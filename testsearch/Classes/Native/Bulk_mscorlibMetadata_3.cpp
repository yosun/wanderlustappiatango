﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// Metadata Definition System.Reflection.AssemblyDescriptionAttribute
extern TypeInfo AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo;
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyDescriptionAttribute_t486_AssemblyDescriptionAttribute__ctor_m2449_ParameterInfos[] = 
{
	{"description", 0, 134221529, 0, &String_t_0_0_0},
};
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyDescriptionAttribute__ctor_m2449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyDescriptionAttribute__ctor_m2449/* method */
	, &AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyDescriptionAttribute_t486_AssemblyDescriptionAttribute__ctor_m2449_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyDescriptionAttribute_t486_MethodInfos[] =
{
	&AssemblyDescriptionAttribute__ctor_m2449_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5279_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5280_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference AssemblyDescriptionAttribute_t486_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyDescriptionAttribute_t486_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t907_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyDescriptionAttribute_t486_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyDescriptionAttribute_t486_0_0_0;
extern const Il2CppType AssemblyDescriptionAttribute_t486_1_0_0;
extern const Il2CppType Attribute_t146_0_0_0;
struct AssemblyDescriptionAttribute_t486;
const Il2CppTypeDefinitionMetadata AssemblyDescriptionAttribute_t486_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyDescriptionAttribute_t486_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyDescriptionAttribute_t486_VTable/* vtableMethods */
	, AssemblyDescriptionAttribute_t486_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1125/* fieldStart */

};
TypeInfo AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyDescriptionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyDescriptionAttribute_t486_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 390/* custom_attributes_cache */
	, &AssemblyDescriptionAttribute_t486_0_0_0/* byval_arg */
	, &AssemblyDescriptionAttribute_t486_1_0_0/* this_arg */
	, &AssemblyDescriptionAttribute_t486_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyDescriptionAttribute_t486)/* instance_size */
	, sizeof (AssemblyDescriptionAttribute_t486)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// Metadata Definition System.Reflection.AssemblyFileVersionAttribute
extern TypeInfo AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo;
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyFileVersionAttribute_t493_AssemblyFileVersionAttribute__ctor_m2456_ParameterInfos[] = 
{
	{"version", 0, 134221530, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyFileVersionAttribute__ctor_m2456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyFileVersionAttribute__ctor_m2456/* method */
	, &AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyFileVersionAttribute_t493_AssemblyFileVersionAttribute__ctor_m2456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyFileVersionAttribute_t493_MethodInfos[] =
{
	&AssemblyFileVersionAttribute__ctor_m2456_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyFileVersionAttribute_t493_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyFileVersionAttribute_t493_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyFileVersionAttribute_t493_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyFileVersionAttribute_t493_0_0_0;
extern const Il2CppType AssemblyFileVersionAttribute_t493_1_0_0;
struct AssemblyFileVersionAttribute_t493;
const Il2CppTypeDefinitionMetadata AssemblyFileVersionAttribute_t493_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyFileVersionAttribute_t493_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyFileVersionAttribute_t493_VTable/* vtableMethods */
	, AssemblyFileVersionAttribute_t493_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1126/* fieldStart */

};
TypeInfo AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyFileVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyFileVersionAttribute_t493_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 391/* custom_attributes_cache */
	, &AssemblyFileVersionAttribute_t493_0_0_0/* byval_arg */
	, &AssemblyFileVersionAttribute_t493_1_0_0/* this_arg */
	, &AssemblyFileVersionAttribute_t493_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyFileVersionAttribute_t493)/* instance_size */
	, sizeof (AssemblyFileVersionAttribute_t493)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// Metadata Definition System.Reflection.AssemblyInformationalVersionAttribute
extern TypeInfo AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo;
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyInformationalVersionAttribute_t1588_AssemblyInformationalVersionAttribute__ctor_m7288_ParameterInfos[] = 
{
	{"informationalVersion", 0, 134221531, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyInformationalVersionAttribute__ctor_m7288_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyInformationalVersionAttribute__ctor_m7288/* method */
	, &AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyInformationalVersionAttribute_t1588_AssemblyInformationalVersionAttribute__ctor_m7288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyInformationalVersionAttribute_t1588_MethodInfos[] =
{
	&AssemblyInformationalVersionAttribute__ctor_m7288_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyInformationalVersionAttribute_t1588_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyInformationalVersionAttribute_t1588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyInformationalVersionAttribute_t1588_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyInformationalVersionAttribute_t1588_0_0_0;
extern const Il2CppType AssemblyInformationalVersionAttribute_t1588_1_0_0;
struct AssemblyInformationalVersionAttribute_t1588;
const Il2CppTypeDefinitionMetadata AssemblyInformationalVersionAttribute_t1588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyInformationalVersionAttribute_t1588_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyInformationalVersionAttribute_t1588_VTable/* vtableMethods */
	, AssemblyInformationalVersionAttribute_t1588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1127/* fieldStart */

};
TypeInfo AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyInformationalVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyInformationalVersionAttribute_t1588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 392/* custom_attributes_cache */
	, &AssemblyInformationalVersionAttribute_t1588_0_0_0/* byval_arg */
	, &AssemblyInformationalVersionAttribute_t1588_1_0_0/* this_arg */
	, &AssemblyInformationalVersionAttribute_t1588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyInformationalVersionAttribute_t1588)/* instance_size */
	, sizeof (AssemblyInformationalVersionAttribute_t1588)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// Metadata Definition System.Reflection.AssemblyKeyFileAttribute
extern TypeInfo AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo;
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyKeyFileAttribute_t1591_AssemblyKeyFileAttribute__ctor_m7292_ParameterInfos[] = 
{
	{"keyFile", 0, 134221532, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern const MethodInfo AssemblyKeyFileAttribute__ctor_m7292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyKeyFileAttribute__ctor_m7292/* method */
	, &AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyKeyFileAttribute_t1591_AssemblyKeyFileAttribute__ctor_m7292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyKeyFileAttribute_t1591_MethodInfos[] =
{
	&AssemblyKeyFileAttribute__ctor_m7292_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyKeyFileAttribute_t1591_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyKeyFileAttribute_t1591_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyKeyFileAttribute_t1591_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyKeyFileAttribute_t1591_0_0_0;
extern const Il2CppType AssemblyKeyFileAttribute_t1591_1_0_0;
struct AssemblyKeyFileAttribute_t1591;
const Il2CppTypeDefinitionMetadata AssemblyKeyFileAttribute_t1591_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyKeyFileAttribute_t1591_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyKeyFileAttribute_t1591_VTable/* vtableMethods */
	, AssemblyKeyFileAttribute_t1591_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1128/* fieldStart */

};
TypeInfo AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyKeyFileAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyKeyFileAttribute_t1591_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 393/* custom_attributes_cache */
	, &AssemblyKeyFileAttribute_t1591_0_0_0/* byval_arg */
	, &AssemblyKeyFileAttribute_t1591_1_0_0/* this_arg */
	, &AssemblyKeyFileAttribute_t1591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyKeyFileAttribute_t1591)/* instance_size */
	, sizeof (AssemblyKeyFileAttribute_t1591)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
// Metadata Definition System.Reflection.AssemblyName
extern TypeInfo AssemblyName_t2244_il2cpp_TypeInfo;
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyNameMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::.ctor()
extern const MethodInfo AssemblyName__ctor_m11821_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyName__ctor_m11821/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName__ctor_m11822_ParameterInfos[] = 
{
	{"si", 0, 134221533, 0, &SerializationInfo_t1388_0_0_0},
	{"sc", 1, 134221534, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo AssemblyName__ctor_m11822_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyName__ctor_m11822/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, AssemblyName_t2244_AssemblyName__ctor_m11822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::get_Name()
extern const MethodInfo AssemblyName_get_Name_m11823_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&AssemblyName_get_Name_m11823/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AssemblyNameFlags_t2245_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t2245 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.AssemblyNameFlags System.Reflection.AssemblyName::get_Flags()
extern const MethodInfo AssemblyName_get_Flags_m11824_MethodInfo = 
{
	"get_Flags"/* name */
	, (methodPointerType)&AssemblyName_get_Flags_m11824/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t2245_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t2245/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::get_FullName()
extern const MethodInfo AssemblyName_get_FullName_m11825_MethodInfo = 
{
	"get_FullName"/* name */
	, (methodPointerType)&AssemblyName_get_FullName_m11825/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Reflection.AssemblyName::get_Version()
extern const MethodInfo AssemblyName_get_Version_m11826_MethodInfo = 
{
	"get_Version"/* name */
	, (methodPointerType)&AssemblyName_get_Version_m11826/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1881_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName_set_Version_m11827_ParameterInfos[] = 
{
	{"value", 0, 134221535, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::set_Version(System.Version)
extern const MethodInfo AssemblyName_set_Version_m11827_MethodInfo = 
{
	"set_Version"/* name */
	, (methodPointerType)&AssemblyName_set_Version_m11827/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyName_t2244_AssemblyName_set_Version_m11827_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::ToString()
extern const MethodInfo AssemblyName_ToString_m11828_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AssemblyName_ToString_m11828/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.AssemblyName::get_IsPublicKeyValid()
extern const MethodInfo AssemblyName_get_IsPublicKeyValid_m11829_MethodInfo = 
{
	"get_IsPublicKeyValid"/* name */
	, (methodPointerType)&AssemblyName_get_IsPublicKeyValid_m11829/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Reflection.AssemblyName::InternalGetPublicKeyToken()
extern const MethodInfo AssemblyName_InternalGetPublicKeyToken_m11830_MethodInfo = 
{
	"InternalGetPublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_InternalGetPublicKeyToken_m11830/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t622_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Reflection.AssemblyName::ComputePublicKeyToken()
extern const MethodInfo AssemblyName_ComputePublicKeyToken_m11831_MethodInfo = 
{
	"ComputePublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_ComputePublicKeyToken_m11831/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t622_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName_SetPublicKey_m11832_ParameterInfos[] = 
{
	{"publicKey", 0, 134221536, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::SetPublicKey(System.Byte[])
extern const MethodInfo AssemblyName_SetPublicKey_m11832_MethodInfo = 
{
	"SetPublicKey"/* name */
	, (methodPointerType)&AssemblyName_SetPublicKey_m11832/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyName_t2244_AssemblyName_SetPublicKey_m11832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName_SetPublicKeyToken_m11833_ParameterInfos[] = 
{
	{"publicKeyToken", 0, 134221537, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::SetPublicKeyToken(System.Byte[])
extern const MethodInfo AssemblyName_SetPublicKeyToken_m11833_MethodInfo = 
{
	"SetPublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_SetPublicKeyToken_m11833/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyName_t2244_AssemblyName_SetPublicKeyToken_m11833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName_GetObjectData_m11834_ParameterInfos[] = 
{
	{"info", 0, 134221538, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221539, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo AssemblyName_GetObjectData_m11834_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&AssemblyName_GetObjectData_m11834/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, AssemblyName_t2244_AssemblyName_GetObjectData_m11834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyName_t2244_AssemblyName_OnDeserialization_m11835_ParameterInfos[] = 
{
	{"sender", 0, 134221540, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::OnDeserialization(System.Object)
extern const MethodInfo AssemblyName_OnDeserialization_m11835_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&AssemblyName_OnDeserialization_m11835/* method */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyName_t2244_AssemblyName_OnDeserialization_m11835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyName_t2244_MethodInfos[] =
{
	&AssemblyName__ctor_m11821_MethodInfo,
	&AssemblyName__ctor_m11822_MethodInfo,
	&AssemblyName_get_Name_m11823_MethodInfo,
	&AssemblyName_get_Flags_m11824_MethodInfo,
	&AssemblyName_get_FullName_m11825_MethodInfo,
	&AssemblyName_get_Version_m11826_MethodInfo,
	&AssemblyName_set_Version_m11827_MethodInfo,
	&AssemblyName_ToString_m11828_MethodInfo,
	&AssemblyName_get_IsPublicKeyValid_m11829_MethodInfo,
	&AssemblyName_InternalGetPublicKeyToken_m11830_MethodInfo,
	&AssemblyName_ComputePublicKeyToken_m11831_MethodInfo,
	&AssemblyName_SetPublicKey_m11832_MethodInfo,
	&AssemblyName_SetPublicKeyToken_m11833_MethodInfo,
	&AssemblyName_GetObjectData_m11834_MethodInfo,
	&AssemblyName_OnDeserialization_m11835_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyName_get_Name_m11823_MethodInfo;
static const PropertyInfo AssemblyName_t2244____Name_PropertyInfo = 
{
	&AssemblyName_t2244_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &AssemblyName_get_Name_m11823_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_Flags_m11824_MethodInfo;
static const PropertyInfo AssemblyName_t2244____Flags_PropertyInfo = 
{
	&AssemblyName_t2244_il2cpp_TypeInfo/* parent */
	, "Flags"/* name */
	, &AssemblyName_get_Flags_m11824_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_FullName_m11825_MethodInfo;
static const PropertyInfo AssemblyName_t2244____FullName_PropertyInfo = 
{
	&AssemblyName_t2244_il2cpp_TypeInfo/* parent */
	, "FullName"/* name */
	, &AssemblyName_get_FullName_m11825_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_Version_m11826_MethodInfo;
extern const MethodInfo AssemblyName_set_Version_m11827_MethodInfo;
static const PropertyInfo AssemblyName_t2244____Version_PropertyInfo = 
{
	&AssemblyName_t2244_il2cpp_TypeInfo/* parent */
	, "Version"/* name */
	, &AssemblyName_get_Version_m11826_MethodInfo/* get */
	, &AssemblyName_set_Version_m11827_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_IsPublicKeyValid_m11829_MethodInfo;
static const PropertyInfo AssemblyName_t2244____IsPublicKeyValid_PropertyInfo = 
{
	&AssemblyName_t2244_il2cpp_TypeInfo/* parent */
	, "IsPublicKeyValid"/* name */
	, &AssemblyName_get_IsPublicKeyValid_m11829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AssemblyName_t2244_PropertyInfos[] =
{
	&AssemblyName_t2244____Name_PropertyInfo,
	&AssemblyName_t2244____Flags_PropertyInfo,
	&AssemblyName_t2244____FullName_PropertyInfo,
	&AssemblyName_t2244____Version_PropertyInfo,
	&AssemblyName_t2244____IsPublicKeyValid_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo AssemblyName_ToString_m11828_MethodInfo;
extern const MethodInfo AssemblyName_GetObjectData_m11834_MethodInfo;
extern const MethodInfo AssemblyName_OnDeserialization_m11835_MethodInfo;
static const Il2CppMethodReference AssemblyName_t2244_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&AssemblyName_ToString_m11828_MethodInfo,
	&AssemblyName_GetObjectData_m11834_MethodInfo,
	&AssemblyName_OnDeserialization_m11835_MethodInfo,
};
static bool AssemblyName_t2244_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
extern const Il2CppType _AssemblyName_t2678_0_0_0;
extern const Il2CppType IDeserializationCallback_t1615_0_0_0;
static const Il2CppType* AssemblyName_t2244_InterfacesTypeInfos[] = 
{
	&ICloneable_t518_0_0_0,
	&ISerializable_t519_0_0_0,
	&_AssemblyName_t2678_0_0_0,
	&IDeserializationCallback_t1615_0_0_0,
};
static Il2CppInterfaceOffsetPair AssemblyName_t2244_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
	{ &_AssemblyName_t2678_0_0_0, 5},
	{ &IDeserializationCallback_t1615_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyName_t2244_0_0_0;
extern const Il2CppType AssemblyName_t2244_1_0_0;
struct AssemblyName_t2244;
const Il2CppTypeDefinitionMetadata AssemblyName_t2244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AssemblyName_t2244_InterfacesTypeInfos/* implementedInterfaces */
	, AssemblyName_t2244_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AssemblyName_t2244_VTable/* vtableMethods */
	, AssemblyName_t2244_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1129/* fieldStart */

};
TypeInfo AssemblyName_t2244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyName"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyName_t2244_MethodInfos/* methods */
	, AssemblyName_t2244_PropertyInfos/* properties */
	, NULL/* events */
	, &AssemblyName_t2244_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 394/* custom_attributes_cache */
	, &AssemblyName_t2244_0_0_0/* byval_arg */
	, &AssemblyName_t2244_1_0_0/* this_arg */
	, &AssemblyName_t2244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyName_t2244)/* instance_size */
	, sizeof (AssemblyName_t2244)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 5/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
// Metadata Definition System.Reflection.AssemblyNameFlags
extern TypeInfo AssemblyNameFlags_t2245_il2cpp_TypeInfo;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlagsMethodDeclarations.h"
static const MethodInfo* AssemblyNameFlags_t2245_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference AssemblyNameFlags_t2245_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool AssemblyNameFlags_t2245_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyNameFlags_t2245_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyNameFlags_t2245_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AssemblyNameFlags_t2245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyNameFlags_t2245_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, AssemblyNameFlags_t2245_VTable/* vtableMethods */
	, AssemblyNameFlags_t2245_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1144/* fieldStart */

};
TypeInfo AssemblyNameFlags_t2245_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyNameFlags"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyNameFlags_t2245_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 395/* custom_attributes_cache */
	, &AssemblyNameFlags_t2245_0_0_0/* byval_arg */
	, &AssemblyNameFlags_t2245_1_0_0/* this_arg */
	, &AssemblyNameFlags_t2245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyNameFlags_t2245)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyNameFlags_t2245)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// Metadata Definition System.Reflection.AssemblyProductAttribute
extern TypeInfo AssemblyProductAttribute_t489_il2cpp_TypeInfo;
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyProductAttribute_t489_AssemblyProductAttribute__ctor_m2452_ParameterInfos[] = 
{
	{"product", 0, 134221541, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
extern const MethodInfo AssemblyProductAttribute__ctor_m2452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyProductAttribute__ctor_m2452/* method */
	, &AssemblyProductAttribute_t489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyProductAttribute_t489_AssemblyProductAttribute__ctor_m2452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyProductAttribute_t489_MethodInfos[] =
{
	&AssemblyProductAttribute__ctor_m2452_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyProductAttribute_t489_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyProductAttribute_t489_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyProductAttribute_t489_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyProductAttribute_t489_0_0_0;
extern const Il2CppType AssemblyProductAttribute_t489_1_0_0;
struct AssemblyProductAttribute_t489;
const Il2CppTypeDefinitionMetadata AssemblyProductAttribute_t489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyProductAttribute_t489_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyProductAttribute_t489_VTable/* vtableMethods */
	, AssemblyProductAttribute_t489_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */

};
TypeInfo AssemblyProductAttribute_t489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyProductAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyProductAttribute_t489_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyProductAttribute_t489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 396/* custom_attributes_cache */
	, &AssemblyProductAttribute_t489_0_0_0/* byval_arg */
	, &AssemblyProductAttribute_t489_1_0_0/* this_arg */
	, &AssemblyProductAttribute_t489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyProductAttribute_t489)/* instance_size */
	, sizeof (AssemblyProductAttribute_t489)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// Metadata Definition System.Reflection.AssemblyTitleAttribute
extern TypeInfo AssemblyTitleAttribute_t492_il2cpp_TypeInfo;
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyTitleAttribute_t492_AssemblyTitleAttribute__ctor_m2455_ParameterInfos[] = 
{
	{"title", 0, 134221542, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
extern const MethodInfo AssemblyTitleAttribute__ctor_m2455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyTitleAttribute__ctor_m2455/* method */
	, &AssemblyTitleAttribute_t492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyTitleAttribute_t492_AssemblyTitleAttribute__ctor_m2455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyTitleAttribute_t492_MethodInfos[] =
{
	&AssemblyTitleAttribute__ctor_m2455_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyTitleAttribute_t492_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyTitleAttribute_t492_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyTitleAttribute_t492_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTitleAttribute_t492_0_0_0;
extern const Il2CppType AssemblyTitleAttribute_t492_1_0_0;
struct AssemblyTitleAttribute_t492;
const Il2CppTypeDefinitionMetadata AssemblyTitleAttribute_t492_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTitleAttribute_t492_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyTitleAttribute_t492_VTable/* vtableMethods */
	, AssemblyTitleAttribute_t492_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1151/* fieldStart */

};
TypeInfo AssemblyTitleAttribute_t492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTitleAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyTitleAttribute_t492_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyTitleAttribute_t492_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 397/* custom_attributes_cache */
	, &AssemblyTitleAttribute_t492_0_0_0/* byval_arg */
	, &AssemblyTitleAttribute_t492_1_0_0/* this_arg */
	, &AssemblyTitleAttribute_t492_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTitleAttribute_t492)/* instance_size */
	, sizeof (AssemblyTitleAttribute_t492)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// Metadata Definition System.Reflection.AssemblyTrademarkAttribute
extern TypeInfo AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo;
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyTrademarkAttribute_t494_AssemblyTrademarkAttribute__ctor_m2457_ParameterInfos[] = 
{
	{"trademark", 0, 134221543, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
extern const MethodInfo AssemblyTrademarkAttribute__ctor_m2457_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyTrademarkAttribute__ctor_m2457/* method */
	, &AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyTrademarkAttribute_t494_AssemblyTrademarkAttribute__ctor_m2457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyTrademarkAttribute_t494_MethodInfos[] =
{
	&AssemblyTrademarkAttribute__ctor_m2457_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyTrademarkAttribute_t494_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyTrademarkAttribute_t494_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyTrademarkAttribute_t494_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTrademarkAttribute_t494_0_0_0;
extern const Il2CppType AssemblyTrademarkAttribute_t494_1_0_0;
struct AssemblyTrademarkAttribute_t494;
const Il2CppTypeDefinitionMetadata AssemblyTrademarkAttribute_t494_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTrademarkAttribute_t494_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyTrademarkAttribute_t494_VTable/* vtableMethods */
	, AssemblyTrademarkAttribute_t494_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1152/* fieldStart */

};
TypeInfo AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTrademarkAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyTrademarkAttribute_t494_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 398/* custom_attributes_cache */
	, &AssemblyTrademarkAttribute_t494_0_0_0/* byval_arg */
	, &AssemblyTrademarkAttribute_t494_1_0_0/* this_arg */
	, &AssemblyTrademarkAttribute_t494_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTrademarkAttribute_t494)/* instance_size */
	, sizeof (AssemblyTrademarkAttribute_t494)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_Default.h"
// Metadata Definition System.Reflection.Binder/Default
extern TypeInfo Default_t2246_il2cpp_TypeInfo;
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_DefaultMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::.ctor()
extern const MethodInfo Default__ctor_m11836_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Default__ctor_m11836/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo Default_t2246_Default_BindToMethod_m11837_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221571, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221572, 0, &MethodBaseU5BU5D_t2597_0_0_0},
	{"args", 2, 134221573, 0, &ObjectU5BU5D_t124_1_0_0},
	{"modifiers", 3, 134221574, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
	{"culture", 4, 134221575, 0, &CultureInfo_t1411_0_0_0},
	{"names", 5, 134221576, 0, &StringU5BU5D_t15_0_0_0},
	{"state", 6, 134221577, 0, &Object_t_1_0_2},
};
extern const Il2CppType MethodBase_t1440_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_ObjectU5BU5DU26_t2703_Object_t_Object_t_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::BindToMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Object[]&,System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[],System.Object&)
extern const MethodInfo Default_BindToMethod_m11837_MethodInfo = 
{
	"BindToMethod"/* name */
	, (methodPointerType)&Default_BindToMethod_m11837/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_ObjectU5BU5DU26_t2703_Object_t_Object_t_Object_t_ObjectU26_t1522/* invoker_method */
	, Default_t2246_Default_BindToMethod_m11837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType MethodBase_t1440_0_0_0;
static const ParameterInfo Default_t2246_Default_ReorderParameters_m11838_ParameterInfos[] = 
{
	{"names", 0, 134221578, 0, &StringU5BU5D_t15_0_0_0},
	{"args", 1, 134221579, 0, &ObjectU5BU5D_t124_1_0_0},
	{"selected", 2, 134221580, 0, &MethodBase_t1440_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_ObjectU5BU5DU26_t2703_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::ReorderParameters(System.String[],System.Object[]&,System.Reflection.MethodBase)
extern const MethodInfo Default_ReorderParameters_m11838_MethodInfo = 
{
	"ReorderParameters"/* name */
	, (methodPointerType)&Default_ReorderParameters_m11838/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_ObjectU5BU5DU26_t2703_Object_t/* invoker_method */
	, Default_t2246_Default_ReorderParameters_m11838_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t2246_Default_IsArrayAssignable_m11839_ParameterInfos[] = 
{
	{"object_type", 0, 134221581, 0, &Type_t_0_0_0},
	{"target_type", 1, 134221582, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::IsArrayAssignable(System.Type,System.Type)
extern const MethodInfo Default_IsArrayAssignable_m11839_MethodInfo = 
{
	"IsArrayAssignable"/* name */
	, (methodPointerType)&Default_IsArrayAssignable_m11839/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_IsArrayAssignable_m11839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo Default_t2246_Default_ChangeType_m11840_ParameterInfos[] = 
{
	{"value", 0, 134221583, 0, &Object_t_0_0_0},
	{"type", 1, 134221584, 0, &Type_t_0_0_0},
	{"culture", 2, 134221585, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.Binder/Default::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo)
extern const MethodInfo Default_ChangeType_m11840_MethodInfo = 
{
	"ChangeType"/* name */
	, (methodPointerType)&Default_ChangeType_m11840/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_ChangeType_m11840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Default_t2246_Default_ReorderArgumentArray_m11841_ParameterInfos[] = 
{
	{"args", 0, 134221586, 0, &ObjectU5BU5D_t124_1_0_0},
	{"state", 1, 134221587, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::ReorderArgumentArray(System.Object[]&,System.Object)
extern const MethodInfo Default_ReorderArgumentArray_m11841_MethodInfo = 
{
	"ReorderArgumentArray"/* name */
	, (methodPointerType)&Default_ReorderArgumentArray_m11841/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t/* invoker_method */
	, Default_t2246_Default_ReorderArgumentArray_m11841_ParameterInfos/* parameters */
	, 400/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t2246_Default_check_type_m11842_ParameterInfos[] = 
{
	{"from", 0, 134221588, 0, &Type_t_0_0_0},
	{"to", 1, 134221589, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::check_type(System.Type,System.Type)
extern const MethodInfo Default_check_type_m11842_MethodInfo = 
{
	"check_type"/* name */
	, (methodPointerType)&Default_check_type_m11842/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_check_type_m11842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t1431_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t1431_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Default_t2246_Default_check_arguments_m11843_ParameterInfos[] = 
{
	{"types", 0, 134221590, 0, &TypeU5BU5D_t884_0_0_0},
	{"args", 1, 134221591, 0, &ParameterInfoU5BU5D_t1431_0_0_0},
	{"allowByRefMatch", 2, 134221592, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::check_arguments(System.Type[],System.Reflection.ParameterInfo[],System.Boolean)
extern const MethodInfo Default_check_arguments_m11843_MethodInfo = 
{
	"check_arguments"/* name */
	, (methodPointerType)&Default_check_arguments_m11843/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t_SByte_t177/* invoker_method */
	, Default_t2246_Default_check_arguments_m11843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo Default_t2246_Default_SelectMethod_m11844_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221593, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221594, 0, &MethodBaseU5BU5D_t2597_0_0_0},
	{"types", 2, 134221595, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 3, 134221596, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Default_SelectMethod_m11844_MethodInfo = 
{
	"SelectMethod"/* name */
	, (methodPointerType)&Default_SelectMethod_m11844/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_SelectMethod_m11844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Default_t2246_Default_SelectMethod_m11845_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221597, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221598, 0, &MethodBaseU5BU5D_t2597_0_0_0},
	{"types", 2, 134221599, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 3, 134221600, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
	{"allowByRefMatch", 4, 134221601, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[],System.Boolean)
extern const MethodInfo Default_SelectMethod_m11845_MethodInfo = 
{
	"SelectMethod"/* name */
	, (methodPointerType)&Default_SelectMethod_m11845/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_SByte_t177/* invoker_method */
	, Default_t2246_Default_SelectMethod_m11845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t1440_0_0_0;
extern const Il2CppType MethodBase_t1440_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo Default_t2246_Default_GetBetterMethod_m11846_ParameterInfos[] = 
{
	{"m1", 0, 134221602, 0, &MethodBase_t1440_0_0_0},
	{"m2", 1, 134221603, 0, &MethodBase_t1440_0_0_0},
	{"types", 2, 134221604, 0, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::GetBetterMethod(System.Reflection.MethodBase,System.Reflection.MethodBase,System.Type[])
extern const MethodInfo Default_GetBetterMethod_m11846_MethodInfo = 
{
	"GetBetterMethod"/* name */
	, (methodPointerType)&Default_GetBetterMethod_m11846/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_GetBetterMethod_m11846_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t2246_Default_CompareCloserType_m11847_ParameterInfos[] = 
{
	{"t1", 0, 134221605, 0, &Type_t_0_0_0},
	{"t2", 1, 134221606, 0, &Type_t_0_0_0},
};
extern const Il2CppType Int32_t135_0_0_0;
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::CompareCloserType(System.Type,System.Type)
extern const MethodInfo Default_CompareCloserType_m11847_MethodInfo = 
{
	"CompareCloserType"/* name */
	, (methodPointerType)&Default_CompareCloserType_m11847/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_CompareCloserType_m11847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t1434_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t1434_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo Default_t2246_Default_SelectProperty_m11848_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221607, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221608, 0, &PropertyInfoU5BU5D_t1434_0_0_0},
	{"returnType", 2, 134221609, 0, &Type_t_0_0_0},
	{"indexes", 3, 134221610, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 4, 134221611, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.Reflection.Binder/Default::SelectProperty(System.Reflection.BindingFlags,System.Reflection.PropertyInfo[],System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Default_SelectProperty_m11848_MethodInfo = 
{
	"SelectProperty"/* name */
	, (methodPointerType)&Default_SelectProperty_m11848/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_SelectProperty_m11848_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t1431_0_0_0;
static const ParameterInfo Default_t2246_Default_check_arguments_with_score_m11849_ParameterInfos[] = 
{
	{"types", 0, 134221612, 0, &TypeU5BU5D_t884_0_0_0},
	{"args", 1, 134221613, 0, &ParameterInfoU5BU5D_t1431_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::check_arguments_with_score(System.Type[],System.Reflection.ParameterInfo[])
extern const MethodInfo Default_check_arguments_with_score_m11849_MethodInfo = 
{
	"check_arguments_with_score"/* name */
	, (methodPointerType)&Default_check_arguments_with_score_m11849/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_check_arguments_with_score_m11849_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t2246_Default_check_type_with_score_m11850_ParameterInfos[] = 
{
	{"from", 0, 134221614, 0, &Type_t_0_0_0},
	{"to", 1, 134221615, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::check_type_with_score(System.Type,System.Type)
extern const MethodInfo Default_check_type_with_score_m11850_MethodInfo = 
{
	"check_type_with_score"/* name */
	, (methodPointerType)&Default_check_type_with_score_m11850/* method */
	, &Default_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, Default_t2246_Default_check_type_with_score_m11850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Default_t2246_MethodInfos[] =
{
	&Default__ctor_m11836_MethodInfo,
	&Default_BindToMethod_m11837_MethodInfo,
	&Default_ReorderParameters_m11838_MethodInfo,
	&Default_IsArrayAssignable_m11839_MethodInfo,
	&Default_ChangeType_m11840_MethodInfo,
	&Default_ReorderArgumentArray_m11841_MethodInfo,
	&Default_check_type_m11842_MethodInfo,
	&Default_check_arguments_m11843_MethodInfo,
	&Default_SelectMethod_m11844_MethodInfo,
	&Default_SelectMethod_m11845_MethodInfo,
	&Default_GetBetterMethod_m11846_MethodInfo,
	&Default_CompareCloserType_m11847_MethodInfo,
	&Default_SelectProperty_m11848_MethodInfo,
	&Default_check_arguments_with_score_m11849_MethodInfo,
	&Default_check_type_with_score_m11850_MethodInfo,
	NULL
};
extern const MethodInfo Default_BindToMethod_m11837_MethodInfo;
extern const MethodInfo Default_ChangeType_m11840_MethodInfo;
extern const MethodInfo Default_ReorderArgumentArray_m11841_MethodInfo;
extern const MethodInfo Default_SelectMethod_m11844_MethodInfo;
extern const MethodInfo Default_SelectProperty_m11848_MethodInfo;
static const Il2CppMethodReference Default_t2246_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Default_BindToMethod_m11837_MethodInfo,
	&Default_ChangeType_m11840_MethodInfo,
	&Default_ReorderArgumentArray_m11841_MethodInfo,
	&Default_SelectMethod_m11844_MethodInfo,
	&Default_SelectProperty_m11848_MethodInfo,
};
static bool Default_t2246_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Default_t2246_0_0_0;
extern const Il2CppType Default_t2246_1_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern TypeInfo Binder_t1437_il2cpp_TypeInfo;
struct Default_t2246;
const Il2CppTypeDefinitionMetadata Default_t2246_DefinitionMetadata = 
{
	&Binder_t1437_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Binder_t1437_0_0_0/* parent */
	, Default_t2246_VTable/* vtableMethods */
	, Default_t2246_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Default_t2246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Default"/* name */
	, ""/* namespaze */
	, Default_t2246_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Default_t2246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Default_t2246_0_0_0/* byval_arg */
	, &Default_t2246_1_0_0/* this_arg */
	, &Default_t2246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Default_t2246)/* instance_size */
	, sizeof (Default_t2246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// Metadata Definition System.Reflection.Binder
// System.Reflection.Binder
#include "mscorlib_System_Reflection_BinderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::.ctor()
extern const MethodInfo Binder__ctor_m11851_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Binder__ctor_m11851/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::.cctor()
extern const MethodInfo Binder__cctor_m11852_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Binder__cctor_m11852/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo Binder_t1437_Binder_BindToMethod_m14532_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221544, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221545, 0, &MethodBaseU5BU5D_t2597_0_0_0},
	{"args", 2, 134221546, 0, &ObjectU5BU5D_t124_1_0_0},
	{"modifiers", 3, 134221547, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
	{"culture", 4, 134221548, 0, &CultureInfo_t1411_0_0_0},
	{"names", 5, 134221549, 0, &StringU5BU5D_t15_0_0_0},
	{"state", 6, 134221550, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_ObjectU5BU5DU26_t2703_Object_t_Object_t_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::BindToMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Object[]&,System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[],System.Object&)
extern const MethodInfo Binder_BindToMethod_m14532_MethodInfo = 
{
	"BindToMethod"/* name */
	, NULL/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_ObjectU5BU5DU26_t2703_Object_t_Object_t_Object_t_ObjectU26_t1522/* invoker_method */
	, Binder_t1437_Binder_BindToMethod_m14532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo Binder_t1437_Binder_ChangeType_m14533_ParameterInfos[] = 
{
	{"value", 0, 134221551, 0, &Object_t_0_0_0},
	{"type", 1, 134221552, 0, &Type_t_0_0_0},
	{"culture", 2, 134221553, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.Binder::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo)
extern const MethodInfo Binder_ChangeType_m14533_MethodInfo = 
{
	"ChangeType"/* name */
	, NULL/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t1437_Binder_ChangeType_m14533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Binder_t1437_Binder_ReorderArgumentArray_m14534_ParameterInfos[] = 
{
	{"args", 0, 134221554, 0, &ObjectU5BU5D_t124_1_0_0},
	{"state", 1, 134221555, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::ReorderArgumentArray(System.Object[]&,System.Object)
extern const MethodInfo Binder_ReorderArgumentArray_m14534_MethodInfo = 
{
	"ReorderArgumentArray"/* name */
	, NULL/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t/* invoker_method */
	, Binder_t1437_Binder_ReorderArgumentArray_m14534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo Binder_t1437_Binder_SelectMethod_m14535_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221556, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221557, 0, &MethodBaseU5BU5D_t2597_0_0_0},
	{"types", 2, 134221558, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 3, 134221559, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Binder_SelectMethod_m14535_MethodInfo = 
{
	"SelectMethod"/* name */
	, NULL/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t1437_Binder_SelectMethod_m14535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t1434_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo Binder_t1437_Binder_SelectProperty_m14536_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221560, 0, &BindingFlags_t2247_0_0_0},
	{"match", 1, 134221561, 0, &PropertyInfoU5BU5D_t1434_0_0_0},
	{"returnType", 2, 134221562, 0, &Type_t_0_0_0},
	{"indexes", 3, 134221563, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 4, 134221564, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.Reflection.Binder::SelectProperty(System.Reflection.BindingFlags,System.Reflection.PropertyInfo[],System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Binder_SelectProperty_m14536_MethodInfo = 
{
	"SelectProperty"/* name */
	, NULL/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t1437_Binder_SelectProperty_m14536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Binder System.Reflection.Binder::get_DefaultBinder()
extern const MethodInfo Binder_get_DefaultBinder_m11853_MethodInfo = 
{
	"get_DefaultBinder"/* name */
	, (methodPointerType)&Binder_get_DefaultBinder_m11853/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Binder_t1437_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2195/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t1431_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo Binder_t1437_Binder_ConvertArgs_m11854_ParameterInfos[] = 
{
	{"binder", 0, 134221565, 0, &Binder_t1437_0_0_0},
	{"args", 1, 134221566, 0, &ObjectU5BU5D_t124_0_0_0},
	{"pinfo", 2, 134221567, 0, &ParameterInfoU5BU5D_t1431_0_0_0},
	{"culture", 3, 134221568, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder::ConvertArgs(System.Reflection.Binder,System.Object[],System.Reflection.ParameterInfo[],System.Globalization.CultureInfo)
extern const MethodInfo Binder_ConvertArgs_m11854_MethodInfo = 
{
	"ConvertArgs"/* name */
	, (methodPointerType)&Binder_ConvertArgs_m11854/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t1437_Binder_ConvertArgs_m11854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Binder_t1437_Binder_GetDerivedLevel_m11855_ParameterInfos[] = 
{
	{"type", 0, 134221569, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder::GetDerivedLevel(System.Type)
extern const MethodInfo Binder_GetDerivedLevel_m11855_MethodInfo = 
{
	"GetDerivedLevel"/* name */
	, (methodPointerType)&Binder_GetDerivedLevel_m11855/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Binder_t1437_Binder_GetDerivedLevel_m11855_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBaseU5BU5D_t2597_0_0_0;
static const ParameterInfo Binder_t1437_Binder_FindMostDerivedMatch_m11856_ParameterInfos[] = 
{
	{"match", 0, 134221570, 0, &MethodBaseU5BU5D_t2597_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::FindMostDerivedMatch(System.Reflection.MethodBase[])
extern const MethodInfo Binder_FindMostDerivedMatch_m11856_MethodInfo = 
{
	"FindMostDerivedMatch"/* name */
	, (methodPointerType)&Binder_FindMostDerivedMatch_m11856/* method */
	, &Binder_t1437_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Binder_t1437_Binder_FindMostDerivedMatch_m11856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Binder_t1437_MethodInfos[] =
{
	&Binder__ctor_m11851_MethodInfo,
	&Binder__cctor_m11852_MethodInfo,
	&Binder_BindToMethod_m14532_MethodInfo,
	&Binder_ChangeType_m14533_MethodInfo,
	&Binder_ReorderArgumentArray_m14534_MethodInfo,
	&Binder_SelectMethod_m14535_MethodInfo,
	&Binder_SelectProperty_m14536_MethodInfo,
	&Binder_get_DefaultBinder_m11853_MethodInfo,
	&Binder_ConvertArgs_m11854_MethodInfo,
	&Binder_GetDerivedLevel_m11855_MethodInfo,
	&Binder_FindMostDerivedMatch_m11856_MethodInfo,
	NULL
};
extern const MethodInfo Binder_get_DefaultBinder_m11853_MethodInfo;
static const PropertyInfo Binder_t1437____DefaultBinder_PropertyInfo = 
{
	&Binder_t1437_il2cpp_TypeInfo/* parent */
	, "DefaultBinder"/* name */
	, &Binder_get_DefaultBinder_m11853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Binder_t1437_PropertyInfos[] =
{
	&Binder_t1437____DefaultBinder_PropertyInfo,
	NULL
};
static const Il2CppType* Binder_t1437_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Default_t2246_0_0_0,
};
static const Il2CppMethodReference Binder_t1437_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool Binder_t1437_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Binder_t1437_1_0_0;
struct Binder_t1437;
const Il2CppTypeDefinitionMetadata Binder_t1437_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Binder_t1437_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Binder_t1437_VTable/* vtableMethods */
	, Binder_t1437_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */

};
TypeInfo Binder_t1437_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Binder"/* name */
	, "System.Reflection"/* namespaze */
	, Binder_t1437_MethodInfos/* methods */
	, Binder_t1437_PropertyInfos/* properties */
	, NULL/* events */
	, &Binder_t1437_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 399/* custom_attributes_cache */
	, &Binder_t1437_0_0_0/* byval_arg */
	, &Binder_t1437_1_0_0/* this_arg */
	, &Binder_t1437_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Binder_t1437)/* instance_size */
	, sizeof (Binder_t1437)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Binder_t1437_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// Metadata Definition System.Reflection.BindingFlags
extern TypeInfo BindingFlags_t2247_il2cpp_TypeInfo;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlagsMethodDeclarations.h"
static const MethodInfo* BindingFlags_t2247_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BindingFlags_t2247_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool BindingFlags_t2247_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BindingFlags_t2247_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BindingFlags_t2247_1_0_0;
const Il2CppTypeDefinitionMetadata BindingFlags_t2247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BindingFlags_t2247_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, BindingFlags_t2247_VTable/* vtableMethods */
	, BindingFlags_t2247_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1154/* fieldStart */

};
TypeInfo BindingFlags_t2247_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BindingFlags"/* name */
	, "System.Reflection"/* namespaze */
	, BindingFlags_t2247_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 401/* custom_attributes_cache */
	, &BindingFlags_t2247_0_0_0/* byval_arg */
	, &BindingFlags_t2247_1_0_0/* this_arg */
	, &BindingFlags_t2247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BindingFlags_t2247)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BindingFlags_t2247)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
// Metadata Definition System.Reflection.CallingConventions
extern TypeInfo CallingConventions_t2248_il2cpp_TypeInfo;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventionsMethodDeclarations.h"
static const MethodInfo* CallingConventions_t2248_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CallingConventions_t2248_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CallingConventions_t2248_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CallingConventions_t2248_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConventions_t2248_0_0_0;
extern const Il2CppType CallingConventions_t2248_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConventions_t2248_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConventions_t2248_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CallingConventions_t2248_VTable/* vtableMethods */
	, CallingConventions_t2248_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1175/* fieldStart */

};
TypeInfo CallingConventions_t2248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConventions"/* name */
	, "System.Reflection"/* namespaze */
	, CallingConventions_t2248_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 402/* custom_attributes_cache */
	, &CallingConventions_t2248_0_0_0/* byval_arg */
	, &CallingConventions_t2248_1_0_0/* this_arg */
	, &CallingConventions_t2248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConventions_t2248)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConventions_t2248)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
// Metadata Definition System.Reflection.ConstructorInfo
extern TypeInfo ConstructorInfo_t1293_il2cpp_TypeInfo;
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ConstructorInfo::.ctor()
extern const MethodInfo ConstructorInfo__ctor_m11857_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructorInfo__ctor_m11857/* method */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ConstructorInfo::.cctor()
extern const MethodInfo ConstructorInfo__cctor_m11858_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructorInfo__cctor_m11858/* method */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberTypes_t2253_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.ConstructorInfo::get_MemberType()
extern const MethodInfo ConstructorInfo_get_MemberType_m11859_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&ConstructorInfo_get_MemberType_m11859/* method */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo ConstructorInfo_t1293_ConstructorInfo_Invoke_m7037_ParameterInfos[] = 
{
	{"parameters", 0, 134221616, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern const MethodInfo ConstructorInfo_Invoke_m7037_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConstructorInfo_Invoke_m7037/* method */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorInfo_t1293_ConstructorInfo_Invoke_m7037_ParameterInfos/* parameters */
	, 406/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo ConstructorInfo_t1293_ConstructorInfo_Invoke_m14537_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221617, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 1, 134221618, 0, &Binder_t1437_0_0_0},
	{"parameters", 2, 134221619, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 3, 134221620, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo ConstructorInfo_Invoke_m14537_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, ConstructorInfo_t1293_ConstructorInfo_Invoke_m14537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructorInfo_t1293_MethodInfos[] =
{
	&ConstructorInfo__ctor_m11857_MethodInfo,
	&ConstructorInfo__cctor_m11858_MethodInfo,
	&ConstructorInfo_get_MemberType_m11859_MethodInfo,
	&ConstructorInfo_Invoke_m7037_MethodInfo,
	&ConstructorInfo_Invoke_m14537_MethodInfo,
	NULL
};
extern const MethodInfo ConstructorInfo_get_MemberType_m11859_MethodInfo;
static const PropertyInfo ConstructorInfo_t1293____MemberType_PropertyInfo = 
{
	&ConstructorInfo_t1293_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &ConstructorInfo_get_MemberType_m11859_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 407/* custom_attributes_cache */

};
static const PropertyInfo* ConstructorInfo_t1293_PropertyInfos[] =
{
	&ConstructorInfo_t1293____MemberType_PropertyInfo,
	NULL
};
extern const MethodInfo MemberInfo_GetCustomAttributes_m14204_MethodInfo;
extern const MethodInfo MemberInfo_IsDefined_m14202_MethodInfo;
extern const MethodInfo MemberInfo_get_Module_m10308_MethodInfo;
extern const MethodInfo MethodBase_Invoke_m11891_MethodInfo;
extern const MethodInfo MethodBase_get_CallingConvention_m11892_MethodInfo;
extern const MethodInfo MethodBase_get_IsPublic_m11893_MethodInfo;
extern const MethodInfo MethodBase_get_IsStatic_m11894_MethodInfo;
extern const MethodInfo MethodBase_get_IsVirtual_m11895_MethodInfo;
extern const MethodInfo MethodBase_GetGenericArguments_m11896_MethodInfo;
extern const MethodInfo MethodBase_get_ContainsGenericParameters_m11897_MethodInfo;
extern const MethodInfo MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo;
extern const MethodInfo MethodBase_get_IsGenericMethod_m11899_MethodInfo;
static const Il2CppMethodReference ConstructorInfo_t1293_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	&ConstructorInfo_get_MemberType_m11859_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m11891_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m11892_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodBase_GetGenericArguments_m11896_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m11897_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo,
	&MethodBase_get_IsGenericMethod_m11899_MethodInfo,
	NULL,
};
static bool ConstructorInfo_t1293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _ConstructorInfo_t2679_0_0_0;
static const Il2CppType* ConstructorInfo_t1293_InterfacesTypeInfos[] = 
{
	&_ConstructorInfo_t2679_0_0_0,
};
extern const Il2CppType _MethodBase_t2682_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType _MemberInfo_t2642_0_0_0;
static Il2CppInterfaceOffsetPair ConstructorInfo_t1293_InterfacesOffsets[] = 
{
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_ConstructorInfo_t2679_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructorInfo_t1293_0_0_0;
extern const Il2CppType ConstructorInfo_t1293_1_0_0;
struct ConstructorInfo_t1293;
const Il2CppTypeDefinitionMetadata ConstructorInfo_t1293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructorInfo_t1293_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructorInfo_t1293_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t1440_0_0_0/* parent */
	, ConstructorInfo_t1293_VTable/* vtableMethods */
	, ConstructorInfo_t1293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1181/* fieldStart */

};
TypeInfo ConstructorInfo_t1293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorInfo"/* name */
	, "System.Reflection"/* namespaze */
	, ConstructorInfo_t1293_MethodInfos/* methods */
	, ConstructorInfo_t1293_PropertyInfos/* properties */
	, NULL/* events */
	, &ConstructorInfo_t1293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 403/* custom_attributes_cache */
	, &ConstructorInfo_t1293_0_0_0/* byval_arg */
	, &ConstructorInfo_t1293_1_0_0/* this_arg */
	, &ConstructorInfo_t1293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorInfo_t1293)/* instance_size */
	, sizeof (ConstructorInfo_t1293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructorInfo_t1293_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
// Metadata Definition System.Reflection.EventAttributes
extern TypeInfo EventAttributes_t2249_il2cpp_TypeInfo;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributesMethodDeclarations.h"
static const MethodInfo* EventAttributes_t2249_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference EventAttributes_t2249_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool EventAttributes_t2249_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventAttributes_t2249_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventAttributes_t2249_0_0_0;
extern const Il2CppType EventAttributes_t2249_1_0_0;
const Il2CppTypeDefinitionMetadata EventAttributes_t2249_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventAttributes_t2249_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, EventAttributes_t2249_VTable/* vtableMethods */
	, EventAttributes_t2249_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1183/* fieldStart */

};
TypeInfo EventAttributes_t2249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, EventAttributes_t2249_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 408/* custom_attributes_cache */
	, &EventAttributes_t2249_0_0_0/* byval_arg */
	, &EventAttributes_t2249_1_0_0/* this_arg */
	, &EventAttributes_t2249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventAttributes_t2249)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventAttributes_t2249)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapter.h"
// Metadata Definition System.Reflection.EventInfo/AddEventAdapter
extern TypeInfo AddEventAdapter_t2250_il2cpp_TypeInfo;
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AddEventAdapter_t2250_AddEventAdapter__ctor_m11860_ParameterInfos[] = 
{
	{"object", 0, 134221622, 0, &Object_t_0_0_0},
	{"method", 1, 134221623, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AddEventAdapter__ctor_m11860_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddEventAdapter__ctor_m11860/* method */
	, &AddEventAdapter_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, AddEventAdapter_t2250_AddEventAdapter__ctor_m11860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Delegate_t151_0_0_0;
extern const Il2CppType Delegate_t151_0_0_0;
static const ParameterInfo AddEventAdapter_t2250_AddEventAdapter_Invoke_m11861_ParameterInfos[] = 
{
	{"_this", 0, 134221624, 0, &Object_t_0_0_0},
	{"dele", 1, 134221625, 0, &Delegate_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::Invoke(System.Object,System.Delegate)
extern const MethodInfo AddEventAdapter_Invoke_m11861_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AddEventAdapter_Invoke_m11861/* method */
	, &AddEventAdapter_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, AddEventAdapter_t2250_AddEventAdapter_Invoke_m11861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Delegate_t151_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AddEventAdapter_t2250_AddEventAdapter_BeginInvoke_m11862_ParameterInfos[] = 
{
	{"_this", 0, 134221626, 0, &Object_t_0_0_0},
	{"dele", 1, 134221627, 0, &Delegate_t151_0_0_0},
	{"callback", 2, 134221628, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134221629, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.EventInfo/AddEventAdapter::BeginInvoke(System.Object,System.Delegate,System.AsyncCallback,System.Object)
extern const MethodInfo AddEventAdapter_BeginInvoke_m11862_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AddEventAdapter_BeginInvoke_m11862/* method */
	, &AddEventAdapter_t2250_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AddEventAdapter_t2250_AddEventAdapter_BeginInvoke_m11862_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo AddEventAdapter_t2250_AddEventAdapter_EndInvoke_m11863_ParameterInfos[] = 
{
	{"result", 0, 134221630, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::EndInvoke(System.IAsyncResult)
extern const MethodInfo AddEventAdapter_EndInvoke_m11863_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AddEventAdapter_EndInvoke_m11863/* method */
	, &AddEventAdapter_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AddEventAdapter_t2250_AddEventAdapter_EndInvoke_m11863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddEventAdapter_t2250_MethodInfos[] =
{
	&AddEventAdapter__ctor_m11860_MethodInfo,
	&AddEventAdapter_Invoke_m11861_MethodInfo,
	&AddEventAdapter_BeginInvoke_m11862_MethodInfo,
	&AddEventAdapter_EndInvoke_m11863_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo AddEventAdapter_Invoke_m11861_MethodInfo;
extern const MethodInfo AddEventAdapter_BeginInvoke_m11862_MethodInfo;
extern const MethodInfo AddEventAdapter_EndInvoke_m11863_MethodInfo;
static const Il2CppMethodReference AddEventAdapter_t2250_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&AddEventAdapter_Invoke_m11861_MethodInfo,
	&AddEventAdapter_BeginInvoke_m11862_MethodInfo,
	&AddEventAdapter_EndInvoke_m11863_MethodInfo,
};
static bool AddEventAdapter_t2250_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddEventAdapter_t2250_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AddEventAdapter_t2250_0_0_0;
extern const Il2CppType AddEventAdapter_t2250_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
extern TypeInfo EventInfo_t_il2cpp_TypeInfo;
extern const Il2CppType EventInfo_t_0_0_0;
struct AddEventAdapter_t2250;
const Il2CppTypeDefinitionMetadata AddEventAdapter_t2250_DefinitionMetadata = 
{
	&EventInfo_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddEventAdapter_t2250_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, AddEventAdapter_t2250_VTable/* vtableMethods */
	, AddEventAdapter_t2250_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AddEventAdapter_t2250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddEventAdapter"/* name */
	, ""/* namespaze */
	, AddEventAdapter_t2250_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddEventAdapter_t2250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddEventAdapter_t2250_0_0_0/* byval_arg */
	, &AddEventAdapter_t2250_1_0_0/* this_arg */
	, &AddEventAdapter_t2250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AddEventAdapter_t2250/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddEventAdapter_t2250)/* instance_size */
	, sizeof (AddEventAdapter_t2250)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
// Metadata Definition System.Reflection.EventInfo
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo::.ctor()
extern const MethodInfo EventInfo__ctor_m11864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventInfo__ctor_m11864/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_EventAttributes_t2249 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventAttributes System.Reflection.EventInfo::get_Attributes()
extern const MethodInfo EventInfo_get_Attributes_m14538_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t2249_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t2249/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.EventInfo::get_EventHandlerType()
extern const MethodInfo EventInfo_get_EventHandlerType_m11865_MethodInfo = 
{
	"get_EventHandlerType"/* name */
	, (methodPointerType)&EventInfo_get_EventHandlerType_m11865/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.EventInfo::get_MemberType()
extern const MethodInfo EventInfo_get_MemberType_m11866_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&EventInfo_get_MemberType_m11866/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo EventInfo_t_EventInfo_GetAddMethod_m14539_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221621, 0, &Boolean_t176_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.EventInfo::GetAddMethod(System.Boolean)
extern const MethodInfo EventInfo_GetAddMethod_m14539_MethodInfo = 
{
	"GetAddMethod"/* name */
	, NULL/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, EventInfo_t_EventInfo_GetAddMethod_m14539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventInfo_t_MethodInfos[] =
{
	&EventInfo__ctor_m11864_MethodInfo,
	&EventInfo_get_Attributes_m14538_MethodInfo,
	&EventInfo_get_EventHandlerType_m11865_MethodInfo,
	&EventInfo_get_MemberType_m11866_MethodInfo,
	&EventInfo_GetAddMethod_m14539_MethodInfo,
	NULL
};
extern const MethodInfo EventInfo_get_Attributes_m14538_MethodInfo;
static const PropertyInfo EventInfo_t____Attributes_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &EventInfo_get_Attributes_m14538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo EventInfo_get_EventHandlerType_m11865_MethodInfo;
static const PropertyInfo EventInfo_t____EventHandlerType_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "EventHandlerType"/* name */
	, &EventInfo_get_EventHandlerType_m11865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo EventInfo_get_MemberType_m11866_MethodInfo;
static const PropertyInfo EventInfo_t____MemberType_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &EventInfo_get_MemberType_m11866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* EventInfo_t_PropertyInfos[] =
{
	&EventInfo_t____Attributes_PropertyInfo,
	&EventInfo_t____EventHandlerType_PropertyInfo,
	&EventInfo_t____MemberType_PropertyInfo,
	NULL
};
static const Il2CppType* EventInfo_t_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AddEventAdapter_t2250_0_0_0,
};
static const Il2CppMethodReference EventInfo_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	&EventInfo_get_MemberType_m11866_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&EventInfo_get_EventHandlerType_m11865_MethodInfo,
	NULL,
};
static bool EventInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _EventInfo_t2680_0_0_0;
static const Il2CppType* EventInfo_t_InterfacesTypeInfos[] = 
{
	&_EventInfo_t2680_0_0_0,
};
static Il2CppInterfaceOffsetPair EventInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_EventInfo_t2680_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventInfo_t_1_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
struct EventInfo_t;
const Il2CppTypeDefinitionMetadata EventInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EventInfo_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EventInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, EventInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, EventInfo_t_VTable/* vtableMethods */
	, EventInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1188/* fieldStart */

};
TypeInfo EventInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, EventInfo_t_MethodInfos/* methods */
	, EventInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &EventInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 409/* custom_attributes_cache */
	, &EventInfo_t_0_0_0/* byval_arg */
	, &EventInfo_t_1_0_0/* this_arg */
	, &EventInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventInfo_t)/* instance_size */
	, sizeof (EventInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 17/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
// Metadata Definition System.Reflection.FieldAttributes
extern TypeInfo FieldAttributes_t2251_il2cpp_TypeInfo;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributesMethodDeclarations.h"
static const MethodInfo* FieldAttributes_t2251_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FieldAttributes_t2251_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool FieldAttributes_t2251_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FieldAttributes_t2251_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAttributes_t2251_0_0_0;
extern const Il2CppType FieldAttributes_t2251_1_0_0;
const Il2CppTypeDefinitionMetadata FieldAttributes_t2251_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAttributes_t2251_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, FieldAttributes_t2251_VTable/* vtableMethods */
	, FieldAttributes_t2251_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1189/* fieldStart */

};
TypeInfo FieldAttributes_t2251_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, FieldAttributes_t2251_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 410/* custom_attributes_cache */
	, &FieldAttributes_t2251_0_0_0/* byval_arg */
	, &FieldAttributes_t2251_1_0_0/* this_arg */
	, &FieldAttributes_t2251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAttributes_t2251)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FieldAttributes_t2251)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
// Metadata Definition System.Reflection.FieldInfo
extern TypeInfo FieldInfo_t_il2cpp_TypeInfo;
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::.ctor()
extern const MethodInfo FieldInfo__ctor_m11867_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldInfo__ctor_m11867/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FieldAttributes_t2251 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes()
extern const MethodInfo FieldInfo_get_Attributes_m14540_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t2251_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t2251/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeFieldHandle_t2054_0_0_0;
extern void* RuntimeInvoker_RuntimeFieldHandle_t2054 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeFieldHandle System.Reflection.FieldInfo::get_FieldHandle()
extern const MethodInfo FieldInfo_get_FieldHandle_m14541_MethodInfo = 
{
	"get_FieldHandle"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeFieldHandle_t2054_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeFieldHandle_t2054/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.FieldInfo::get_FieldType()
extern const MethodInfo FieldInfo_get_FieldType_m14542_MethodInfo = 
{
	"get_FieldType"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_GetValue_m14543_ParameterInfos[] = 
{
	{"obj", 0, 134221631, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.FieldInfo::GetValue(System.Object)
extern const MethodInfo FieldInfo_GetValue_m14543_MethodInfo = 
{
	"GetValue"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_GetValue_m14543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.FieldInfo::get_MemberType()
extern const MethodInfo FieldInfo_get_MemberType_m11868_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&FieldInfo_get_MemberType_m11868/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsLiteral()
extern const MethodInfo FieldInfo_get_IsLiteral_m11869_MethodInfo = 
{
	"get_IsLiteral"/* name */
	, (methodPointerType)&FieldInfo_get_IsLiteral_m11869/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
extern const MethodInfo FieldInfo_get_IsStatic_m11870_MethodInfo = 
{
	"get_IsStatic"/* name */
	, (methodPointerType)&FieldInfo_get_IsStatic_m11870/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsInitOnly()
extern const MethodInfo FieldInfo_get_IsInitOnly_m11871_MethodInfo = 
{
	"get_IsInitOnly"/* name */
	, (methodPointerType)&FieldInfo_get_IsInitOnly_m11871/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsPublic()
extern const MethodInfo FieldInfo_get_IsPublic_m11872_MethodInfo = 
{
	"get_IsPublic"/* name */
	, (methodPointerType)&FieldInfo_get_IsPublic_m11872/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized()
extern const MethodInfo FieldInfo_get_IsNotSerialized_m11873_MethodInfo = 
{
	"get_IsNotSerialized"/* name */
	, (methodPointerType)&FieldInfo_get_IsNotSerialized_m11873/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_SetValue_m14544_ParameterInfos[] = 
{
	{"obj", 0, 134221632, 0, &Object_t_0_0_0},
	{"value", 1, 134221633, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221634, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 3, 134221635, 0, &Binder_t1437_0_0_0},
	{"culture", 4, 134221636, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern const MethodInfo FieldInfo_SetValue_m14544_MethodInfo = 
{
	"SetValue"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_SetValue_m14544_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_SetValue_m11874_ParameterInfos[] = 
{
	{"obj", 0, 134221637, 0, &Object_t_0_0_0},
	{"value", 1, 134221638, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern const MethodInfo FieldInfo_SetValue_m11874_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&FieldInfo_SetValue_m11874/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_SetValue_m11874_ParameterInfos/* parameters */
	, 412/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_internal_from_handle_type_m11875_ParameterInfos[] = 
{
	{"field_handle", 0, 134221639, 0, &IntPtr_t_0_0_0},
	{"type_handle", 1, 134221640, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType FieldInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.Reflection.FieldInfo::internal_from_handle_type(System.IntPtr,System.IntPtr)
extern const MethodInfo FieldInfo_internal_from_handle_type_m11875_MethodInfo = 
{
	"internal_from_handle_type"/* name */
	, (methodPointerType)&FieldInfo_internal_from_handle_type_m11875/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, FieldInfo_t_FieldInfo_internal_from_handle_type_m11875_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeFieldHandle_t2054_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_GetFieldFromHandle_m11876_ParameterInfos[] = 
{
	{"handle", 0, 134221641, 0, &RuntimeFieldHandle_t2054_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t2054 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.Reflection.FieldInfo::GetFieldFromHandle(System.RuntimeFieldHandle)
extern const MethodInfo FieldInfo_GetFieldFromHandle_m11876_MethodInfo = 
{
	"GetFieldFromHandle"/* name */
	, (methodPointerType)&FieldInfo_GetFieldFromHandle_m11876/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeFieldHandle_t2054/* invoker_method */
	, FieldInfo_t_FieldInfo_GetFieldFromHandle_m11876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.FieldInfo::GetFieldOffset()
extern const MethodInfo FieldInfo_GetFieldOffset_m11877_MethodInfo = 
{
	"GetFieldOffset"/* name */
	, (methodPointerType)&FieldInfo_GetFieldOffset_m11877/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnmanagedMarshal_t2226_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::GetUnmanagedMarshal()
extern const MethodInfo FieldInfo_GetUnmanagedMarshal_m11878_MethodInfo = 
{
	"GetUnmanagedMarshal"/* name */
	, (methodPointerType)&FieldInfo_GetUnmanagedMarshal_m11878/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedMarshal_t2226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::get_UMarshal()
extern const MethodInfo FieldInfo_get_UMarshal_m11879_MethodInfo = 
{
	"get_UMarshal"/* name */
	, (methodPointerType)&FieldInfo_get_UMarshal_m11879/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedMarshal_t2226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2499/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.FieldInfo::GetPseudoCustomAttributes()
extern const MethodInfo FieldInfo_GetPseudoCustomAttributes_m11880_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&FieldInfo_GetPseudoCustomAttributes_m11880/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FieldInfo_t_MethodInfos[] =
{
	&FieldInfo__ctor_m11867_MethodInfo,
	&FieldInfo_get_Attributes_m14540_MethodInfo,
	&FieldInfo_get_FieldHandle_m14541_MethodInfo,
	&FieldInfo_get_FieldType_m14542_MethodInfo,
	&FieldInfo_GetValue_m14543_MethodInfo,
	&FieldInfo_get_MemberType_m11868_MethodInfo,
	&FieldInfo_get_IsLiteral_m11869_MethodInfo,
	&FieldInfo_get_IsStatic_m11870_MethodInfo,
	&FieldInfo_get_IsInitOnly_m11871_MethodInfo,
	&FieldInfo_get_IsPublic_m11872_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m11873_MethodInfo,
	&FieldInfo_SetValue_m14544_MethodInfo,
	&FieldInfo_SetValue_m11874_MethodInfo,
	&FieldInfo_internal_from_handle_type_m11875_MethodInfo,
	&FieldInfo_GetFieldFromHandle_m11876_MethodInfo,
	&FieldInfo_GetFieldOffset_m11877_MethodInfo,
	&FieldInfo_GetUnmanagedMarshal_m11878_MethodInfo,
	&FieldInfo_get_UMarshal_m11879_MethodInfo,
	&FieldInfo_GetPseudoCustomAttributes_m11880_MethodInfo,
	NULL
};
extern const MethodInfo FieldInfo_get_Attributes_m14540_MethodInfo;
static const PropertyInfo FieldInfo_t____Attributes_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &FieldInfo_get_Attributes_m14540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_FieldHandle_m14541_MethodInfo;
static const PropertyInfo FieldInfo_t____FieldHandle_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "FieldHandle"/* name */
	, &FieldInfo_get_FieldHandle_m14541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_FieldType_m14542_MethodInfo;
static const PropertyInfo FieldInfo_t____FieldType_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "FieldType"/* name */
	, &FieldInfo_get_FieldType_m14542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_MemberType_m11868_MethodInfo;
static const PropertyInfo FieldInfo_t____MemberType_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &FieldInfo_get_MemberType_m11868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsLiteral_m11869_MethodInfo;
static const PropertyInfo FieldInfo_t____IsLiteral_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsLiteral"/* name */
	, &FieldInfo_get_IsLiteral_m11869_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsStatic_m11870_MethodInfo;
static const PropertyInfo FieldInfo_t____IsStatic_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsStatic"/* name */
	, &FieldInfo_get_IsStatic_m11870_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsInitOnly_m11871_MethodInfo;
static const PropertyInfo FieldInfo_t____IsInitOnly_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsInitOnly"/* name */
	, &FieldInfo_get_IsInitOnly_m11871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsPublic_m11872_MethodInfo;
static const PropertyInfo FieldInfo_t____IsPublic_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsPublic"/* name */
	, &FieldInfo_get_IsPublic_m11872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsNotSerialized_m11873_MethodInfo;
static const PropertyInfo FieldInfo_t____IsNotSerialized_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsNotSerialized"/* name */
	, &FieldInfo_get_IsNotSerialized_m11873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_UMarshal_m11879_MethodInfo;
static const PropertyInfo FieldInfo_t____UMarshal_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "UMarshal"/* name */
	, &FieldInfo_get_UMarshal_m11879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* FieldInfo_t_PropertyInfos[] =
{
	&FieldInfo_t____Attributes_PropertyInfo,
	&FieldInfo_t____FieldHandle_PropertyInfo,
	&FieldInfo_t____FieldType_PropertyInfo,
	&FieldInfo_t____MemberType_PropertyInfo,
	&FieldInfo_t____IsLiteral_PropertyInfo,
	&FieldInfo_t____IsStatic_PropertyInfo,
	&FieldInfo_t____IsInitOnly_PropertyInfo,
	&FieldInfo_t____IsPublic_PropertyInfo,
	&FieldInfo_t____IsNotSerialized_PropertyInfo,
	&FieldInfo_t____UMarshal_PropertyInfo,
	NULL
};
extern const MethodInfo FieldInfo_SetValue_m11874_MethodInfo;
extern const MethodInfo FieldInfo_GetFieldOffset_m11877_MethodInfo;
static const Il2CppMethodReference FieldInfo_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	&FieldInfo_get_MemberType_m11868_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&FieldInfo_get_IsLiteral_m11869_MethodInfo,
	&FieldInfo_get_IsStatic_m11870_MethodInfo,
	&FieldInfo_get_IsInitOnly_m11871_MethodInfo,
	&FieldInfo_get_IsPublic_m11872_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m11873_MethodInfo,
	NULL,
	&FieldInfo_SetValue_m11874_MethodInfo,
	&FieldInfo_GetFieldOffset_m11877_MethodInfo,
	&FieldInfo_get_UMarshal_m11879_MethodInfo,
};
static bool FieldInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _FieldInfo_t2681_0_0_0;
static const Il2CppType* FieldInfo_t_InterfacesTypeInfos[] = 
{
	&_FieldInfo_t2681_0_0_0,
};
static Il2CppInterfaceOffsetPair FieldInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_FieldInfo_t2681_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldInfo_t_1_0_0;
struct FieldInfo_t;
const Il2CppTypeDefinitionMetadata FieldInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FieldInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, FieldInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, FieldInfo_t_VTable/* vtableMethods */
	, FieldInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FieldInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldInfo"/* name */
	, "System.Reflection"/* namespaze */
	, FieldInfo_t_MethodInfos/* methods */
	, FieldInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &FieldInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 411/* custom_attributes_cache */
	, &FieldInfo_t_0_0_0/* byval_arg */
	, &FieldInfo_t_1_0_0/* this_arg */
	, &FieldInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldInfo_t)/* instance_size */
	, sizeof (FieldInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 10/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolder.h"
// Metadata Definition System.Reflection.MemberInfoSerializationHolder
extern TypeInfo MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo;
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder__ctor_m11881_ParameterInfos[] = 
{
	{"info", 0, 134221642, 0, &SerializationInfo_t1388_0_0_0},
	{"ctx", 1, 134221643, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder__ctor_m11881_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder__ctor_m11881/* method */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder__ctor_m11881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberTypes_t2253_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_Serialize_m11882_ParameterInfos[] = 
{
	{"info", 0, 134221644, 0, &SerializationInfo_t1388_0_0_0},
	{"name", 1, 134221645, 0, &String_t_0_0_0},
	{"klass", 2, 134221646, 0, &Type_t_0_0_0},
	{"signature", 3, 134221647, 0, &String_t_0_0_0},
	{"type", 4, 134221648, 0, &MemberTypes_t2253_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes)
extern const MethodInfo MemberInfoSerializationHolder_Serialize_m11882_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_Serialize_m11882/* method */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Int32_t135/* invoker_method */
	, MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_Serialize_m11882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberTypes_t2253_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_Serialize_m11883_ParameterInfos[] = 
{
	{"info", 0, 134221649, 0, &SerializationInfo_t1388_0_0_0},
	{"name", 1, 134221650, 0, &String_t_0_0_0},
	{"klass", 2, 134221651, 0, &Type_t_0_0_0},
	{"signature", 3, 134221652, 0, &String_t_0_0_0},
	{"type", 4, 134221653, 0, &MemberTypes_t2253_0_0_0},
	{"genericArguments", 5, 134221654, 0, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes,System.Type[])
extern const MethodInfo MemberInfoSerializationHolder_Serialize_m11883_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_Serialize_m11883/* method */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Int32_t135_Object_t/* invoker_method */
	, MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_Serialize_m11883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_GetObjectData_m11884_ParameterInfos[] = 
{
	{"info", 0, 134221655, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221656, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder_GetObjectData_m11884_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_GetObjectData_m11884/* method */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_GetObjectData_m11884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_GetRealObject_m11885_ParameterInfos[] = 
{
	{"context", 0, 134221657, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MemberInfoSerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder_GetRealObject_m11885_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_GetRealObject_m11885/* method */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1389/* invoker_method */
	, MemberInfoSerializationHolder_t2252_MemberInfoSerializationHolder_GetRealObject_m11885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberInfoSerializationHolder_t2252_MethodInfos[] =
{
	&MemberInfoSerializationHolder__ctor_m11881_MethodInfo,
	&MemberInfoSerializationHolder_Serialize_m11882_MethodInfo,
	&MemberInfoSerializationHolder_Serialize_m11883_MethodInfo,
	&MemberInfoSerializationHolder_GetObjectData_m11884_MethodInfo,
	&MemberInfoSerializationHolder_GetRealObject_m11885_MethodInfo,
	NULL
};
extern const MethodInfo MemberInfoSerializationHolder_GetObjectData_m11884_MethodInfo;
extern const MethodInfo MemberInfoSerializationHolder_GetRealObject_m11885_MethodInfo;
static const Il2CppMethodReference MemberInfoSerializationHolder_t2252_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfoSerializationHolder_GetObjectData_m11884_MethodInfo,
	&MemberInfoSerializationHolder_GetRealObject_m11885_MethodInfo,
};
static bool MemberInfoSerializationHolder_t2252_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t2621_0_0_0;
static const Il2CppType* MemberInfoSerializationHolder_t2252_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
	&IObjectReference_t2621_0_0_0,
};
static Il2CppInterfaceOffsetPair MemberInfoSerializationHolder_t2252_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &IObjectReference_t2621_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberInfoSerializationHolder_t2252_0_0_0;
extern const Il2CppType MemberInfoSerializationHolder_t2252_1_0_0;
struct MemberInfoSerializationHolder_t2252;
const Il2CppTypeDefinitionMetadata MemberInfoSerializationHolder_t2252_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MemberInfoSerializationHolder_t2252_InterfacesTypeInfos/* implementedInterfaces */
	, MemberInfoSerializationHolder_t2252_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MemberInfoSerializationHolder_t2252_VTable/* vtableMethods */
	, MemberInfoSerializationHolder_t2252_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1209/* fieldStart */

};
TypeInfo MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberInfoSerializationHolder"/* name */
	, "System.Reflection"/* namespaze */
	, MemberInfoSerializationHolder_t2252_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberInfoSerializationHolder_t2252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MemberInfoSerializationHolder_t2252_0_0_0/* byval_arg */
	, &MemberInfoSerializationHolder_t2252_1_0_0/* this_arg */
	, &MemberInfoSerializationHolder_t2252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberInfoSerializationHolder_t2252)/* instance_size */
	, sizeof (MemberInfoSerializationHolder_t2252)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
// Metadata Definition System.Reflection.MemberTypes
extern TypeInfo MemberTypes_t2253_il2cpp_TypeInfo;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypesMethodDeclarations.h"
static const MethodInfo* MemberTypes_t2253_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MemberTypes_t2253_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool MemberTypes_t2253_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberTypes_t2253_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberTypes_t2253_1_0_0;
const Il2CppTypeDefinitionMetadata MemberTypes_t2253_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberTypes_t2253_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, MemberTypes_t2253_VTable/* vtableMethods */
	, MemberTypes_t2253_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1214/* fieldStart */

};
TypeInfo MemberTypes_t2253_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberTypes"/* name */
	, "System.Reflection"/* namespaze */
	, MemberTypes_t2253_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 413/* custom_attributes_cache */
	, &MemberTypes_t2253_0_0_0/* byval_arg */
	, &MemberTypes_t2253_1_0_0/* this_arg */
	, &MemberTypes_t2253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberTypes_t2253)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MemberTypes_t2253)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
// Metadata Definition System.Reflection.MethodAttributes
extern TypeInfo MethodAttributes_t2254_il2cpp_TypeInfo;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributesMethodDeclarations.h"
static const MethodInfo* MethodAttributes_t2254_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodAttributes_t2254_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool MethodAttributes_t2254_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodAttributes_t2254_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAttributes_t2254_0_0_0;
extern const Il2CppType MethodAttributes_t2254_1_0_0;
const Il2CppTypeDefinitionMetadata MethodAttributes_t2254_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAttributes_t2254_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, MethodAttributes_t2254_VTable/* vtableMethods */
	, MethodAttributes_t2254_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1224/* fieldStart */

};
TypeInfo MethodAttributes_t2254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, MethodAttributes_t2254_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 414/* custom_attributes_cache */
	, &MethodAttributes_t2254_0_0_0/* byval_arg */
	, &MethodAttributes_t2254_1_0_0/* this_arg */
	, &MethodAttributes_t2254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAttributes_t2254)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodAttributes_t2254)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// Metadata Definition System.Reflection.MethodBase
extern TypeInfo MethodBase_t1440_il2cpp_TypeInfo;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MethodBase::.ctor()
extern const MethodInfo MethodBase__ctor_m11886_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodBase__ctor_m11886/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeMethodHandle_t2551_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t2551_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_GetMethodFromHandleNoGenericCheck_m11887_ParameterInfos[] = 
{
	{"handle", 0, 134221658, 0, &RuntimeMethodHandle_t2551_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2551 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleNoGenericCheck(System.RuntimeMethodHandle)
extern const MethodInfo MethodBase_GetMethodFromHandleNoGenericCheck_m11887_MethodInfo = 
{
	"GetMethodFromHandleNoGenericCheck"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandleNoGenericCheck_m11887/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeMethodHandle_t2551/* invoker_method */
	, MethodBase_t1440_MethodBase_GetMethodFromHandleNoGenericCheck_m11887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_GetMethodFromIntPtr_m11888_ParameterInfos[] = 
{
	{"handle", 0, 134221659, 0, &IntPtr_t_0_0_0},
	{"declaringType", 1, 134221660, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromIntPtr(System.IntPtr,System.IntPtr)
extern const MethodInfo MethodBase_GetMethodFromIntPtr_m11888_MethodInfo = 
{
	"GetMethodFromIntPtr"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromIntPtr_m11888/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, MethodBase_t1440_MethodBase_GetMethodFromIntPtr_m11888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeMethodHandle_t2551_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_GetMethodFromHandle_m11889_ParameterInfos[] = 
{
	{"handle", 0, 134221661, 0, &RuntimeMethodHandle_t2551_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2551 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandle(System.RuntimeMethodHandle)
extern const MethodInfo MethodBase_GetMethodFromHandle_m11889_MethodInfo = 
{
	"GetMethodFromHandle"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandle_m11889/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeMethodHandle_t2551/* invoker_method */
	, MethodBase_t1440_MethodBase_GetMethodFromHandle_m11889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_GetMethodFromHandleInternalType_m11890_ParameterInfos[] = 
{
	{"method_handle", 0, 134221662, 0, &IntPtr_t_0_0_0},
	{"type_handle", 1, 134221663, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleInternalType(System.IntPtr,System.IntPtr)
extern const MethodInfo MethodBase_GetMethodFromHandleInternalType_m11890_MethodInfo = 
{
	"GetMethodFromHandleInternalType"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandleInternalType_m11890/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, MethodBase_t1440_MethodBase_GetMethodFromHandleInternalType_m11890_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters()
extern const MethodInfo MethodBase_GetParameters_m14545_MethodInfo = 
{
	"GetParameters"/* name */
	, NULL/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_Invoke_m11891_ParameterInfos[] = 
{
	{"obj", 0, 134221664, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221665, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern const MethodInfo MethodBase_Invoke_m11891_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MethodBase_Invoke_m11891/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodBase_t1440_MethodBase_Invoke_m11891_ParameterInfos/* parameters */
	, 416/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MethodBase_t1440_MethodBase_Invoke_m14546_ParameterInfos[] = 
{
	{"obj", 0, 134221666, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221667, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134221668, 0, &Binder_t1437_0_0_0},
	{"parameters", 3, 134221669, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 4, 134221670, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MethodBase_Invoke_m14546_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MethodBase_t1440_MethodBase_Invoke_m14546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t2551 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MethodBase::get_MethodHandle()
extern const MethodInfo MethodBase_get_MethodHandle_m14547_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, NULL/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t2551_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t2551/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t2254 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes()
extern const MethodInfo MethodBase_get_Attributes_m14548_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t2254_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t2254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t2248 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention()
extern const MethodInfo MethodBase_get_CallingConvention_m11892_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MethodBase_get_CallingConvention_m11892/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t2248_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t2248/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsPublic()
extern const MethodInfo MethodBase_get_IsPublic_m11893_MethodInfo = 
{
	"get_IsPublic"/* name */
	, (methodPointerType)&MethodBase_get_IsPublic_m11893/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsStatic()
extern const MethodInfo MethodBase_get_IsStatic_m11894_MethodInfo = 
{
	"get_IsStatic"/* name */
	, (methodPointerType)&MethodBase_get_IsStatic_m11894/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsVirtual()
extern const MethodInfo MethodBase_get_IsVirtual_m11895_MethodInfo = 
{
	"get_IsVirtual"/* name */
	, (methodPointerType)&MethodBase_get_IsVirtual_m11895/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MethodBase::GetGenericArguments()
extern const MethodInfo MethodBase_GetGenericArguments_m11896_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MethodBase_GetGenericArguments_m11896/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 417/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_ContainsGenericParameters()
extern const MethodInfo MethodBase_get_ContainsGenericParameters_m11897_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MethodBase_get_ContainsGenericParameters_m11897/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethodDefinition()
extern const MethodInfo MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MethodBase_get_IsGenericMethodDefinition_m11898/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethod()
extern const MethodInfo MethodBase_get_IsGenericMethod_m11899_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MethodBase_get_IsGenericMethod_m11899/* method */
	, &MethodBase_t1440_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodBase_t1440_MethodInfos[] =
{
	&MethodBase__ctor_m11886_MethodInfo,
	&MethodBase_GetMethodFromHandleNoGenericCheck_m11887_MethodInfo,
	&MethodBase_GetMethodFromIntPtr_m11888_MethodInfo,
	&MethodBase_GetMethodFromHandle_m11889_MethodInfo,
	&MethodBase_GetMethodFromHandleInternalType_m11890_MethodInfo,
	&MethodBase_GetParameters_m14545_MethodInfo,
	&MethodBase_Invoke_m11891_MethodInfo,
	&MethodBase_Invoke_m14546_MethodInfo,
	&MethodBase_get_MethodHandle_m14547_MethodInfo,
	&MethodBase_get_Attributes_m14548_MethodInfo,
	&MethodBase_get_CallingConvention_m11892_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodBase_GetGenericArguments_m11896_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m11897_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo,
	&MethodBase_get_IsGenericMethod_m11899_MethodInfo,
	NULL
};
extern const MethodInfo MethodBase_get_MethodHandle_m14547_MethodInfo;
static const PropertyInfo MethodBase_t1440____MethodHandle_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MethodBase_get_MethodHandle_m14547_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodBase_get_Attributes_m14548_MethodInfo;
static const PropertyInfo MethodBase_t1440____Attributes_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MethodBase_get_Attributes_m14548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____CallingConvention_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MethodBase_get_CallingConvention_m11892_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____IsPublic_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "IsPublic"/* name */
	, &MethodBase_get_IsPublic_m11893_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____IsStatic_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "IsStatic"/* name */
	, &MethodBase_get_IsStatic_m11894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____IsVirtual_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "IsVirtual"/* name */
	, &MethodBase_get_IsVirtual_m11895_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____ContainsGenericParameters_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MethodBase_get_ContainsGenericParameters_m11897_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____IsGenericMethodDefinition_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t1440____IsGenericMethod_PropertyInfo = 
{
	&MethodBase_t1440_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MethodBase_get_IsGenericMethod_m11899_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodBase_t1440_PropertyInfos[] =
{
	&MethodBase_t1440____MethodHandle_PropertyInfo,
	&MethodBase_t1440____Attributes_PropertyInfo,
	&MethodBase_t1440____CallingConvention_PropertyInfo,
	&MethodBase_t1440____IsPublic_PropertyInfo,
	&MethodBase_t1440____IsStatic_PropertyInfo,
	&MethodBase_t1440____IsVirtual_PropertyInfo,
	&MethodBase_t1440____ContainsGenericParameters_PropertyInfo,
	&MethodBase_t1440____IsGenericMethodDefinition_PropertyInfo,
	&MethodBase_t1440____IsGenericMethod_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MethodBase_t1440_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m11891_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m11892_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodBase_GetGenericArguments_m11896_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m11897_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo,
	&MethodBase_get_IsGenericMethod_m11899_MethodInfo,
};
static bool MethodBase_t1440_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodBase_t1440_InterfacesTypeInfos[] = 
{
	&_MethodBase_t2682_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodBase_t1440_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_MethodBase_t2682_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodBase_t1440_1_0_0;
struct MethodBase_t1440;
const Il2CppTypeDefinitionMetadata MethodBase_t1440_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodBase_t1440_InterfacesTypeInfos/* implementedInterfaces */
	, MethodBase_t1440_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, MethodBase_t1440_VTable/* vtableMethods */
	, MethodBase_t1440_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodBase_t1440_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodBase"/* name */
	, "System.Reflection"/* namespaze */
	, MethodBase_t1440_MethodInfos/* methods */
	, MethodBase_t1440_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodBase_t1440_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 415/* custom_attributes_cache */
	, &MethodBase_t1440_0_0_0/* byval_arg */
	, &MethodBase_t1440_1_0_0/* this_arg */
	, &MethodBase_t1440_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodBase_t1440)/* instance_size */
	, sizeof (MethodBase_t1440)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 9/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
// Metadata Definition System.Reflection.MethodImplAttributes
extern TypeInfo MethodImplAttributes_t2255_il2cpp_TypeInfo;
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributesMethodDeclarations.h"
static const MethodInfo* MethodImplAttributes_t2255_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodImplAttributes_t2255_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool MethodImplAttributes_t2255_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodImplAttributes_t2255_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodImplAttributes_t2255_0_0_0;
extern const Il2CppType MethodImplAttributes_t2255_1_0_0;
const Il2CppTypeDefinitionMetadata MethodImplAttributes_t2255_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodImplAttributes_t2255_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, MethodImplAttributes_t2255_VTable/* vtableMethods */
	, MethodImplAttributes_t2255_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1249/* fieldStart */

};
TypeInfo MethodImplAttributes_t2255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodImplAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, MethodImplAttributes_t2255_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 418/* custom_attributes_cache */
	, &MethodImplAttributes_t2255_0_0_0/* byval_arg */
	, &MethodImplAttributes_t2255_1_0_0/* this_arg */
	, &MethodImplAttributes_t2255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodImplAttributes_t2255)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodImplAttributes_t2255)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Metadata Definition System.Reflection.MethodInfo
extern TypeInfo MethodInfo_t_il2cpp_TypeInfo;
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MethodInfo::.ctor()
extern const MethodInfo MethodInfo__ctor_m11900_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodInfo__ctor_m11900/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MethodInfo::GetBaseDefinition()
extern const MethodInfo MethodInfo_GetBaseDefinition_m14549_MethodInfo = 
{
	"GetBaseDefinition"/* name */
	, NULL/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.MethodInfo::get_MemberType()
extern const MethodInfo MethodInfo_get_MemberType_m11901_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&MethodInfo_get_MemberType_m11901/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MethodInfo::get_ReturnType()
extern const MethodInfo MethodInfo_get_ReturnType_m11902_MethodInfo = 
{
	"get_ReturnType"/* name */
	, (methodPointerType)&MethodInfo_get_ReturnType_m11902/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo MethodInfo_t_MethodInfo_MakeGenericMethod_m11903_ParameterInfos[] = 
{
	{"typeArguments", 0, 134221671, 420, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[])
extern const MethodInfo MethodInfo_MakeGenericMethod_m11903_MethodInfo = 
{
	"MakeGenericMethod"/* name */
	, (methodPointerType)&MethodInfo_MakeGenericMethod_m11903/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodInfo_t_MethodInfo_MakeGenericMethod_m11903_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MethodInfo::GetGenericArguments()
extern const MethodInfo MethodInfo_GetGenericArguments_m11904_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MethodInfo_GetGenericArguments_m11904/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 421/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethod()
extern const MethodInfo MethodInfo_get_IsGenericMethod_m11905_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MethodInfo_get_IsGenericMethod_m11905/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethodDefinition()
extern const MethodInfo MethodInfo_get_IsGenericMethodDefinition_m11906_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MethodInfo_get_IsGenericMethodDefinition_m11906/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_ContainsGenericParameters()
extern const MethodInfo MethodInfo_get_ContainsGenericParameters_m11907_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MethodInfo_get_ContainsGenericParameters_m11907/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodInfo_t_MethodInfos[] =
{
	&MethodInfo__ctor_m11900_MethodInfo,
	&MethodInfo_GetBaseDefinition_m14549_MethodInfo,
	&MethodInfo_get_MemberType_m11901_MethodInfo,
	&MethodInfo_get_ReturnType_m11902_MethodInfo,
	&MethodInfo_MakeGenericMethod_m11903_MethodInfo,
	&MethodInfo_GetGenericArguments_m11904_MethodInfo,
	&MethodInfo_get_IsGenericMethod_m11905_MethodInfo,
	&MethodInfo_get_IsGenericMethodDefinition_m11906_MethodInfo,
	&MethodInfo_get_ContainsGenericParameters_m11907_MethodInfo,
	NULL
};
extern const MethodInfo MethodInfo_get_MemberType_m11901_MethodInfo;
static const PropertyInfo MethodInfo_t____MemberType_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &MethodInfo_get_MemberType_m11901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_ReturnType_m11902_MethodInfo;
static const PropertyInfo MethodInfo_t____ReturnType_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "ReturnType"/* name */
	, &MethodInfo_get_ReturnType_m11902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_IsGenericMethod_m11905_MethodInfo;
static const PropertyInfo MethodInfo_t____IsGenericMethod_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MethodInfo_get_IsGenericMethod_m11905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_IsGenericMethodDefinition_m11906_MethodInfo;
static const PropertyInfo MethodInfo_t____IsGenericMethodDefinition_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MethodInfo_get_IsGenericMethodDefinition_m11906_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_ContainsGenericParameters_m11907_MethodInfo;
static const PropertyInfo MethodInfo_t____ContainsGenericParameters_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MethodInfo_get_ContainsGenericParameters_m11907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodInfo_t_PropertyInfos[] =
{
	&MethodInfo_t____MemberType_PropertyInfo,
	&MethodInfo_t____ReturnType_PropertyInfo,
	&MethodInfo_t____IsGenericMethod_PropertyInfo,
	&MethodInfo_t____IsGenericMethodDefinition_PropertyInfo,
	&MethodInfo_t____ContainsGenericParameters_PropertyInfo,
	NULL
};
extern const MethodInfo MethodInfo_GetGenericArguments_m11904_MethodInfo;
extern const MethodInfo MethodInfo_MakeGenericMethod_m11903_MethodInfo;
static const Il2CppMethodReference MethodInfo_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	&MethodInfo_get_MemberType_m11901_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m11891_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m11892_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodInfo_GetGenericArguments_m11904_MethodInfo,
	&MethodInfo_get_ContainsGenericParameters_m11907_MethodInfo,
	&MethodInfo_get_IsGenericMethodDefinition_m11906_MethodInfo,
	&MethodInfo_get_IsGenericMethod_m11905_MethodInfo,
	NULL,
	&MethodInfo_get_ReturnType_m11902_MethodInfo,
	&MethodInfo_MakeGenericMethod_m11903_MethodInfo,
};
static bool MethodInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _MethodInfo_t2683_0_0_0;
static const Il2CppType* MethodInfo_t_InterfacesTypeInfos[] = 
{
	&_MethodInfo_t2683_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodInfo_t_InterfacesOffsets[] = 
{
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_MethodInfo_t2683_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodInfo_t_1_0_0;
struct MethodInfo_t;
const Il2CppTypeDefinitionMetadata MethodInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, MethodInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t1440_0_0_0/* parent */
	, MethodInfo_t_VTable/* vtableMethods */
	, MethodInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MethodInfo_t_MethodInfos/* methods */
	, MethodInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 419/* custom_attributes_cache */
	, &MethodInfo_t_0_0_0/* byval_arg */
	, &MethodInfo_t_1_0_0/* this_arg */
	, &MethodInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodInfo_t)/* instance_size */
	, sizeof (MethodInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 30/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Missing
#include "mscorlib_System_Reflection_Missing.h"
// Metadata Definition System.Reflection.Missing
extern TypeInfo Missing_t2256_il2cpp_TypeInfo;
// System.Reflection.Missing
#include "mscorlib_System_Reflection_MissingMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::.ctor()
extern const MethodInfo Missing__ctor_m11908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Missing__ctor_m11908/* method */
	, &Missing_t2256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::.cctor()
extern const MethodInfo Missing__cctor_m11909_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Missing__cctor_m11909/* method */
	, &Missing_t2256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo Missing_t2256_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_ParameterInfos[] = 
{
	{"info", 0, 134221672, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221673, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910/* method */
	, &Missing_t2256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, Missing_t2256_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_ParameterInfos/* parameters */
	, 423/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Missing_t2256_MethodInfos[] =
{
	&Missing__ctor_m11908_MethodInfo,
	&Missing__cctor_m11909_MethodInfo,
	&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_MethodInfo,
	NULL
};
extern const MethodInfo Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_MethodInfo;
static const Il2CppMethodReference Missing_t2256_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910_MethodInfo,
};
static bool Missing_t2256_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Missing_t2256_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair Missing_t2256_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Missing_t2256_0_0_0;
extern const Il2CppType Missing_t2256_1_0_0;
struct Missing_t2256;
const Il2CppTypeDefinitionMetadata Missing_t2256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Missing_t2256_InterfacesTypeInfos/* implementedInterfaces */
	, Missing_t2256_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Missing_t2256_VTable/* vtableMethods */
	, Missing_t2256_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1264/* fieldStart */

};
TypeInfo Missing_t2256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Missing"/* name */
	, "System.Reflection"/* namespaze */
	, Missing_t2256_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Missing_t2256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 422/* custom_attributes_cache */
	, &Missing_t2256_0_0_0/* byval_arg */
	, &Missing_t2256_1_0_0/* this_arg */
	, &Missing_t2256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Missing_t2256)/* instance_size */
	, sizeof (Missing_t2256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Missing_t2256_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// Metadata Definition System.Reflection.Module
extern TypeInfo Module_t2232_il2cpp_TypeInfo;
// System.Reflection.Module
#include "mscorlib_System_Reflection_ModuleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::.ctor()
extern const MethodInfo Module__ctor_m11911_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Module__ctor_m11911/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::.cctor()
extern const MethodInfo Module__cctor_m11912_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Module__cctor_m11912/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t2009_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.Reflection.Module::get_Assembly()
extern const MethodInfo Module_get_Assembly_m11913_MethodInfo = 
{
	"get_Assembly"/* name */
	, (methodPointerType)&Module_get_Assembly_m11913/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.Module::get_ScopeName()
extern const MethodInfo Module_get_ScopeName_m11914_MethodInfo = 
{
	"get_ScopeName"/* name */
	, (methodPointerType)&Module_get_ScopeName_m11914/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Module_t2232_Module_GetCustomAttributes_m11915_ParameterInfos[] = 
{
	{"attributeType", 0, 134221674, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221675, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.Module::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo Module_GetCustomAttributes_m11915_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&Module_GetCustomAttributes_m11915/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, Module_t2232_Module_GetCustomAttributes_m11915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo Module_t2232_Module_GetObjectData_m11916_ParameterInfos[] = 
{
	{"info", 0, 134221676, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221677, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Module_GetObjectData_m11916_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&Module_GetObjectData_m11916/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, Module_t2232_Module_GetObjectData_m11916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Module_t2232_Module_IsDefined_m11917_ParameterInfos[] = 
{
	{"attributeType", 0, 134221678, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221679, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::IsDefined(System.Type,System.Boolean)
extern const MethodInfo Module_IsDefined_m11917_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&Module_IsDefined_m11917/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, Module_t2232_Module_IsDefined_m11917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::IsResource()
extern const MethodInfo Module_IsResource_m11918_MethodInfo = 
{
	"IsResource"/* name */
	, (methodPointerType)&Module_IsResource_m11918/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.Module::ToString()
extern const MethodInfo Module_ToString_m11919_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Module_ToString_m11919/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Module_t2232_Module_filter_by_type_name_m11920_ParameterInfos[] = 
{
	{"m", 0, 134221680, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134221681, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::filter_by_type_name(System.Type,System.Object)
extern const MethodInfo Module_filter_by_type_name_m11920_MethodInfo = 
{
	"filter_by_type_name"/* name */
	, (methodPointerType)&Module_filter_by_type_name_m11920/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Module_t2232_Module_filter_by_type_name_m11920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Module_t2232_Module_filter_by_type_name_ignore_case_m11921_ParameterInfos[] = 
{
	{"m", 0, 134221682, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134221683, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::filter_by_type_name_ignore_case(System.Type,System.Object)
extern const MethodInfo Module_filter_by_type_name_ignore_case_m11921_MethodInfo = 
{
	"filter_by_type_name_ignore_case"/* name */
	, (methodPointerType)&Module_filter_by_type_name_ignore_case_m11921/* method */
	, &Module_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Module_t2232_Module_filter_by_type_name_ignore_case_m11921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Module_t2232_MethodInfos[] =
{
	&Module__ctor_m11911_MethodInfo,
	&Module__cctor_m11912_MethodInfo,
	&Module_get_Assembly_m11913_MethodInfo,
	&Module_get_ScopeName_m11914_MethodInfo,
	&Module_GetCustomAttributes_m11915_MethodInfo,
	&Module_GetObjectData_m11916_MethodInfo,
	&Module_IsDefined_m11917_MethodInfo,
	&Module_IsResource_m11918_MethodInfo,
	&Module_ToString_m11919_MethodInfo,
	&Module_filter_by_type_name_m11920_MethodInfo,
	&Module_filter_by_type_name_ignore_case_m11921_MethodInfo,
	NULL
};
extern const MethodInfo Module_get_Assembly_m11913_MethodInfo;
static const PropertyInfo Module_t2232____Assembly_PropertyInfo = 
{
	&Module_t2232_il2cpp_TypeInfo/* parent */
	, "Assembly"/* name */
	, &Module_get_Assembly_m11913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Module_get_ScopeName_m11914_MethodInfo;
static const PropertyInfo Module_t2232____ScopeName_PropertyInfo = 
{
	&Module_t2232_il2cpp_TypeInfo/* parent */
	, "ScopeName"/* name */
	, &Module_get_ScopeName_m11914_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Module_t2232_PropertyInfos[] =
{
	&Module_t2232____Assembly_PropertyInfo,
	&Module_t2232____ScopeName_PropertyInfo,
	NULL
};
extern const MethodInfo Module_ToString_m11919_MethodInfo;
extern const MethodInfo Module_GetObjectData_m11916_MethodInfo;
extern const MethodInfo Module_GetCustomAttributes_m11915_MethodInfo;
extern const MethodInfo Module_IsDefined_m11917_MethodInfo;
static const Il2CppMethodReference Module_t2232_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Module_ToString_m11919_MethodInfo,
	&Module_GetObjectData_m11916_MethodInfo,
	&Module_GetCustomAttributes_m11915_MethodInfo,
	&Module_IsDefined_m11917_MethodInfo,
	&Module_GetCustomAttributes_m11915_MethodInfo,
	&Module_GetObjectData_m11916_MethodInfo,
	&Module_IsDefined_m11917_MethodInfo,
};
static bool Module_t2232_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Module_t2684_0_0_0;
static const Il2CppType* Module_t2232_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
	&ICustomAttributeProvider_t2605_0_0_0,
	&_Module_t2684_0_0_0,
};
static Il2CppInterfaceOffsetPair Module_t2232_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &ICustomAttributeProvider_t2605_0_0_0, 5},
	{ &_Module_t2684_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Module_t2232_0_0_0;
extern const Il2CppType Module_t2232_1_0_0;
struct Module_t2232;
const Il2CppTypeDefinitionMetadata Module_t2232_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Module_t2232_InterfacesTypeInfos/* implementedInterfaces */
	, Module_t2232_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Module_t2232_VTable/* vtableMethods */
	, Module_t2232_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1265/* fieldStart */

};
TypeInfo Module_t2232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Module"/* name */
	, "System.Reflection"/* namespaze */
	, Module_t2232_MethodInfos/* methods */
	, Module_t2232_PropertyInfos/* properties */
	, NULL/* events */
	, &Module_t2232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 424/* custom_attributes_cache */
	, &Module_t2232_0_0_0/* byval_arg */
	, &Module_t2232_1_0_0/* this_arg */
	, &Module_t2232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Module_t2232)/* instance_size */
	, sizeof (Module_t2232)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Module_t2232_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
// Metadata Definition System.Reflection.MonoEventInfo
extern TypeInfo MonoEventInfo_t2258_il2cpp_TypeInfo;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfoMethodDeclarations.h"
extern const Il2CppType MonoEvent_t_0_0_0;
extern const Il2CppType MonoEvent_t_0_0_0;
extern const Il2CppType MonoEventInfo_t2258_1_0_2;
extern const Il2CppType MonoEventInfo_t2258_1_0_0;
static const ParameterInfo MonoEventInfo_t2258_MonoEventInfo_get_event_info_m11922_ParameterInfos[] = 
{
	{"ev", 0, 134221684, 0, &MonoEvent_t_0_0_0},
	{"info", 1, 134221685, 0, &MonoEventInfo_t2258_1_0_2},
};
extern void* RuntimeInvoker_Void_t175_Object_t_MonoEventInfoU26_t3053 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)
extern const MethodInfo MonoEventInfo_get_event_info_m11922_MethodInfo = 
{
	"get_event_info"/* name */
	, (methodPointerType)&MonoEventInfo_get_event_info_m11922/* method */
	, &MonoEventInfo_t2258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_MonoEventInfoU26_t3053/* invoker_method */
	, MonoEventInfo_t2258_MonoEventInfo_get_event_info_m11922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoEvent_t_0_0_0;
static const ParameterInfo MonoEventInfo_t2258_MonoEventInfo_GetEventInfo_m11923_ParameterInfos[] = 
{
	{"ev", 0, 134221686, 0, &MonoEvent_t_0_0_0},
};
extern const Il2CppType MonoEventInfo_t2258_0_0_0;
extern void* RuntimeInvoker_MonoEventInfo_t2258_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoEventInfo System.Reflection.MonoEventInfo::GetEventInfo(System.Reflection.MonoEvent)
extern const MethodInfo MonoEventInfo_GetEventInfo_m11923_MethodInfo = 
{
	"GetEventInfo"/* name */
	, (methodPointerType)&MonoEventInfo_GetEventInfo_m11923/* method */
	, &MonoEventInfo_t2258_il2cpp_TypeInfo/* declaring_type */
	, &MonoEventInfo_t2258_0_0_0/* return_type */
	, RuntimeInvoker_MonoEventInfo_t2258_Object_t/* invoker_method */
	, MonoEventInfo_t2258_MonoEventInfo_GetEventInfo_m11923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoEventInfo_t2258_MethodInfos[] =
{
	&MonoEventInfo_get_event_info_m11922_MethodInfo,
	&MonoEventInfo_GetEventInfo_m11923_MethodInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference MonoEventInfo_t2258_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool MonoEventInfo_t2258_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueType_t530_0_0_0;
const Il2CppTypeDefinitionMetadata MonoEventInfo_t2258_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, MonoEventInfo_t2258_VTable/* vtableMethods */
	, MonoEventInfo_t2258_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1275/* fieldStart */

};
TypeInfo MonoEventInfo_t2258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoEventInfo_t2258_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoEventInfo_t2258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEventInfo_t2258_0_0_0/* byval_arg */
	, &MonoEventInfo_t2258_1_0_0/* this_arg */
	, &MonoEventInfo_t2258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEventInfo_t2258)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoEventInfo_t2258)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEvent.h"
// Metadata Definition System.Reflection.MonoEvent
extern TypeInfo MonoEvent_t_il2cpp_TypeInfo;
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEvent::.ctor()
extern const MethodInfo MonoEvent__ctor_m11924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoEvent__ctor_m11924/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_EventAttributes_t2249 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventAttributes System.Reflection.MonoEvent::get_Attributes()
extern const MethodInfo MonoEvent_get_Attributes_m11925_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoEvent_get_Attributes_m11925/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t2249_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t2249/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetAddMethod_m11926_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221687, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoEvent::GetAddMethod(System.Boolean)
extern const MethodInfo MonoEvent_GetAddMethod_m11926_MethodInfo = 
{
	"GetAddMethod"/* name */
	, (methodPointerType)&MonoEvent_GetAddMethod_m11926/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoEvent_t_MonoEvent_GetAddMethod_m11926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoEvent::get_DeclaringType()
extern const MethodInfo MonoEvent_get_DeclaringType_m11927_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoEvent_get_DeclaringType_m11927/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoEvent::get_ReflectedType()
extern const MethodInfo MonoEvent_get_ReflectedType_m11928_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoEvent_get_ReflectedType_m11928/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoEvent::get_Name()
extern const MethodInfo MonoEvent_get_Name_m11929_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoEvent_get_Name_m11929/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoEvent::ToString()
extern const MethodInfo MonoEvent_ToString_m11930_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoEvent_ToString_m11930/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_IsDefined_m11931_ParameterInfos[] = 
{
	{"attributeType", 0, 134221688, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221689, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoEvent::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoEvent_IsDefined_m11931_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoEvent_IsDefined_m11931/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoEvent_t_MonoEvent_IsDefined_m11931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetCustomAttributes_m11932_ParameterInfos[] = 
{
	{"inherit", 0, 134221690, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoEvent_GetCustomAttributes_m11932_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoEvent_GetCustomAttributes_m11932/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoEvent_t_MonoEvent_GetCustomAttributes_m11932_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetCustomAttributes_m11933_ParameterInfos[] = 
{
	{"attributeType", 0, 134221691, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221692, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoEvent_GetCustomAttributes_m11933_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoEvent_GetCustomAttributes_m11933/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoEvent_t_MonoEvent_GetCustomAttributes_m11933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetObjectData_m11934_ParameterInfos[] = 
{
	{"info", 0, 134221693, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221694, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEvent::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoEvent_GetObjectData_m11934_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoEvent_GetObjectData_m11934/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoEvent_t_MonoEvent_GetObjectData_m11934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoEvent_t_MethodInfos[] =
{
	&MonoEvent__ctor_m11924_MethodInfo,
	&MonoEvent_get_Attributes_m11925_MethodInfo,
	&MonoEvent_GetAddMethod_m11926_MethodInfo,
	&MonoEvent_get_DeclaringType_m11927_MethodInfo,
	&MonoEvent_get_ReflectedType_m11928_MethodInfo,
	&MonoEvent_get_Name_m11929_MethodInfo,
	&MonoEvent_ToString_m11930_MethodInfo,
	&MonoEvent_IsDefined_m11931_MethodInfo,
	&MonoEvent_GetCustomAttributes_m11932_MethodInfo,
	&MonoEvent_GetCustomAttributes_m11933_MethodInfo,
	&MonoEvent_GetObjectData_m11934_MethodInfo,
	NULL
};
extern const MethodInfo MonoEvent_get_Attributes_m11925_MethodInfo;
static const PropertyInfo MonoEvent_t____Attributes_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoEvent_get_Attributes_m11925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_DeclaringType_m11927_MethodInfo;
static const PropertyInfo MonoEvent_t____DeclaringType_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoEvent_get_DeclaringType_m11927_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_ReflectedType_m11928_MethodInfo;
static const PropertyInfo MonoEvent_t____ReflectedType_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoEvent_get_ReflectedType_m11928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_Name_m11929_MethodInfo;
static const PropertyInfo MonoEvent_t____Name_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoEvent_get_Name_m11929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoEvent_t_PropertyInfos[] =
{
	&MonoEvent_t____Attributes_PropertyInfo,
	&MonoEvent_t____DeclaringType_PropertyInfo,
	&MonoEvent_t____ReflectedType_PropertyInfo,
	&MonoEvent_t____Name_PropertyInfo,
	NULL
};
extern const MethodInfo MonoEvent_ToString_m11930_MethodInfo;
extern const MethodInfo MonoEvent_GetCustomAttributes_m11933_MethodInfo;
extern const MethodInfo MonoEvent_IsDefined_m11931_MethodInfo;
extern const MethodInfo MonoEvent_GetCustomAttributes_m11932_MethodInfo;
extern const MethodInfo MonoEvent_GetAddMethod_m11926_MethodInfo;
extern const MethodInfo MonoEvent_GetObjectData_m11934_MethodInfo;
static const Il2CppMethodReference MonoEvent_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoEvent_ToString_m11930_MethodInfo,
	&MonoEvent_GetCustomAttributes_m11933_MethodInfo,
	&MonoEvent_IsDefined_m11931_MethodInfo,
	&MonoEvent_get_DeclaringType_m11927_MethodInfo,
	&EventInfo_get_MemberType_m11866_MethodInfo,
	&MonoEvent_get_Name_m11929_MethodInfo,
	&MonoEvent_get_ReflectedType_m11928_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoEvent_IsDefined_m11931_MethodInfo,
	&MonoEvent_GetCustomAttributes_m11932_MethodInfo,
	&MonoEvent_GetCustomAttributes_m11933_MethodInfo,
	&MonoEvent_get_Attributes_m11925_MethodInfo,
	&EventInfo_get_EventHandlerType_m11865_MethodInfo,
	&MonoEvent_GetAddMethod_m11926_MethodInfo,
	&MonoEvent_GetObjectData_m11934_MethodInfo,
};
static bool MonoEvent_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoEvent_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoEvent_t_InterfacesOffsets[] = 
{
	{ &_EventInfo_t2680_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 17},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEvent_t_1_0_0;
struct MonoEvent_t;
const Il2CppTypeDefinitionMetadata MonoEvent_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoEvent_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoEvent_t_InterfacesOffsets/* interfaceOffsets */
	, &EventInfo_t_0_0_0/* parent */
	, MonoEvent_t_VTable/* vtableMethods */
	, MonoEvent_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1283/* fieldStart */

};
TypeInfo MonoEvent_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEvent"/* name */
	, "System.Reflection"/* namespaze */
	, MonoEvent_t_MethodInfos/* methods */
	, MonoEvent_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoEvent_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEvent_t_0_0_0/* byval_arg */
	, &MonoEvent_t_1_0_0/* this_arg */
	, &MonoEvent_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEvent_t)/* instance_size */
	, sizeof (MonoEvent_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoField.h"
// Metadata Definition System.Reflection.MonoField
extern TypeInfo MonoField_t_il2cpp_TypeInfo;
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoFieldMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::.ctor()
extern const MethodInfo MonoField__ctor_m11935_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoField__ctor_m11935/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FieldAttributes_t2251 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldAttributes System.Reflection.MonoField::get_Attributes()
extern const MethodInfo MonoField_get_Attributes_m11936_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoField_get_Attributes_m11936/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t2251_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t2251/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeFieldHandle_t2054 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeFieldHandle System.Reflection.MonoField::get_FieldHandle()
extern const MethodInfo MonoField_get_FieldHandle_m11937_MethodInfo = 
{
	"get_FieldHandle"/* name */
	, (methodPointerType)&MonoField_get_FieldHandle_m11937/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeFieldHandle_t2054_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeFieldHandle_t2054/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_FieldType()
extern const MethodInfo MonoField_get_FieldType_m11938_MethodInfo = 
{
	"get_FieldType"/* name */
	, (methodPointerType)&MonoField_get_FieldType_m11938/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetParentType_m11939_ParameterInfos[] = 
{
	{"declaring", 0, 134221695, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::GetParentType(System.Boolean)
extern const MethodInfo MonoField_GetParentType_m11939_MethodInfo = 
{
	"GetParentType"/* name */
	, (methodPointerType)&MonoField_GetParentType_m11939/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoField_t_MonoField_GetParentType_m11939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_ReflectedType()
extern const MethodInfo MonoField_get_ReflectedType_m11940_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoField_get_ReflectedType_m11940/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_DeclaringType()
extern const MethodInfo MonoField_get_DeclaringType_m11941_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoField_get_DeclaringType_m11941/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoField::get_Name()
extern const MethodInfo MonoField_get_Name_m11942_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoField_get_Name_m11942/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoField_t_MonoField_IsDefined_m11943_ParameterInfos[] = 
{
	{"attributeType", 0, 134221696, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221697, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoField::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoField_IsDefined_m11943_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoField_IsDefined_m11943/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoField_t_MonoField_IsDefined_m11943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetCustomAttributes_m11944_ParameterInfos[] = 
{
	{"inherit", 0, 134221698, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoField_GetCustomAttributes_m11944_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoField_GetCustomAttributes_m11944/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoField_t_MonoField_GetCustomAttributes_m11944_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetCustomAttributes_m11945_ParameterInfos[] = 
{
	{"attributeType", 0, 134221699, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221700, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoField_GetCustomAttributes_m11945_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoField_GetCustomAttributes_m11945/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoField_t_MonoField_GetCustomAttributes_m11945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.MonoField::GetFieldOffset()
extern const MethodInfo MonoField_GetFieldOffset_m11946_MethodInfo = 
{
	"GetFieldOffset"/* name */
	, (methodPointerType)&MonoField_GetFieldOffset_m11946/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 4096/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetValueInternal_m11947_ParameterInfos[] = 
{
	{"obj", 0, 134221701, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoField::GetValueInternal(System.Object)
extern const MethodInfo MonoField_GetValueInternal_m11947_MethodInfo = 
{
	"GetValueInternal"/* name */
	, (methodPointerType)&MonoField_GetValueInternal_m11947/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_GetValueInternal_m11947_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetValue_m11948_ParameterInfos[] = 
{
	{"obj", 0, 134221702, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoField::GetValue(System.Object)
extern const MethodInfo MonoField_GetValue_m11948_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoField_GetValue_m11948/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_GetValue_m11948_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoField::ToString()
extern const MethodInfo MonoField_ToString_m11949_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoField_ToString_m11949/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_SetValueInternal_m11950_ParameterInfos[] = 
{
	{"fi", 0, 134221703, 0, &FieldInfo_t_0_0_0},
	{"obj", 1, 134221704, 0, &Object_t_0_0_0},
	{"value", 2, 134221705, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::SetValueInternal(System.Reflection.FieldInfo,System.Object,System.Object)
extern const MethodInfo MonoField_SetValueInternal_m11950_MethodInfo = 
{
	"SetValueInternal"/* name */
	, (methodPointerType)&MonoField_SetValueInternal_m11950/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_SetValueInternal_m11950_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoField_t_MonoField_SetValue_m11951_ParameterInfos[] = 
{
	{"obj", 0, 134221706, 0, &Object_t_0_0_0},
	{"val", 1, 134221707, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221708, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 3, 134221709, 0, &Binder_t1437_0_0_0},
	{"culture", 4, 134221710, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern const MethodInfo MonoField_SetValue_m11951_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&MonoField_SetValue_m11951/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_SetValue_m11951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetObjectData_m11952_ParameterInfos[] = 
{
	{"info", 0, 134221711, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221712, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoField_GetObjectData_m11952_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoField_GetObjectData_m11952/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoField_t_MonoField_GetObjectData_m11952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::CheckGeneric()
extern const MethodInfo MonoField_CheckGeneric_m11953_MethodInfo = 
{
	"CheckGeneric"/* name */
	, (methodPointerType)&MonoField_CheckGeneric_m11953/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoField_t_MethodInfos[] =
{
	&MonoField__ctor_m11935_MethodInfo,
	&MonoField_get_Attributes_m11936_MethodInfo,
	&MonoField_get_FieldHandle_m11937_MethodInfo,
	&MonoField_get_FieldType_m11938_MethodInfo,
	&MonoField_GetParentType_m11939_MethodInfo,
	&MonoField_get_ReflectedType_m11940_MethodInfo,
	&MonoField_get_DeclaringType_m11941_MethodInfo,
	&MonoField_get_Name_m11942_MethodInfo,
	&MonoField_IsDefined_m11943_MethodInfo,
	&MonoField_GetCustomAttributes_m11944_MethodInfo,
	&MonoField_GetCustomAttributes_m11945_MethodInfo,
	&MonoField_GetFieldOffset_m11946_MethodInfo,
	&MonoField_GetValueInternal_m11947_MethodInfo,
	&MonoField_GetValue_m11948_MethodInfo,
	&MonoField_ToString_m11949_MethodInfo,
	&MonoField_SetValueInternal_m11950_MethodInfo,
	&MonoField_SetValue_m11951_MethodInfo,
	&MonoField_GetObjectData_m11952_MethodInfo,
	&MonoField_CheckGeneric_m11953_MethodInfo,
	NULL
};
extern const MethodInfo MonoField_get_Attributes_m11936_MethodInfo;
static const PropertyInfo MonoField_t____Attributes_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoField_get_Attributes_m11936_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_FieldHandle_m11937_MethodInfo;
static const PropertyInfo MonoField_t____FieldHandle_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "FieldHandle"/* name */
	, &MonoField_get_FieldHandle_m11937_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_FieldType_m11938_MethodInfo;
static const PropertyInfo MonoField_t____FieldType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "FieldType"/* name */
	, &MonoField_get_FieldType_m11938_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_ReflectedType_m11940_MethodInfo;
static const PropertyInfo MonoField_t____ReflectedType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoField_get_ReflectedType_m11940_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_DeclaringType_m11941_MethodInfo;
static const PropertyInfo MonoField_t____DeclaringType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoField_get_DeclaringType_m11941_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_Name_m11942_MethodInfo;
static const PropertyInfo MonoField_t____Name_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoField_get_Name_m11942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoField_t_PropertyInfos[] =
{
	&MonoField_t____Attributes_PropertyInfo,
	&MonoField_t____FieldHandle_PropertyInfo,
	&MonoField_t____FieldType_PropertyInfo,
	&MonoField_t____ReflectedType_PropertyInfo,
	&MonoField_t____DeclaringType_PropertyInfo,
	&MonoField_t____Name_PropertyInfo,
	NULL
};
extern const MethodInfo MonoField_ToString_m11949_MethodInfo;
extern const MethodInfo MonoField_GetCustomAttributes_m11945_MethodInfo;
extern const MethodInfo MonoField_IsDefined_m11943_MethodInfo;
extern const MethodInfo MonoField_GetCustomAttributes_m11944_MethodInfo;
extern const MethodInfo MonoField_GetValue_m11948_MethodInfo;
extern const MethodInfo MonoField_SetValue_m11951_MethodInfo;
extern const MethodInfo MonoField_GetFieldOffset_m11946_MethodInfo;
extern const MethodInfo MonoField_GetObjectData_m11952_MethodInfo;
static const Il2CppMethodReference MonoField_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoField_ToString_m11949_MethodInfo,
	&MonoField_GetCustomAttributes_m11945_MethodInfo,
	&MonoField_IsDefined_m11943_MethodInfo,
	&MonoField_get_DeclaringType_m11941_MethodInfo,
	&FieldInfo_get_MemberType_m11868_MethodInfo,
	&MonoField_get_Name_m11942_MethodInfo,
	&MonoField_get_ReflectedType_m11940_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoField_IsDefined_m11943_MethodInfo,
	&MonoField_GetCustomAttributes_m11944_MethodInfo,
	&MonoField_GetCustomAttributes_m11945_MethodInfo,
	&MonoField_get_Attributes_m11936_MethodInfo,
	&MonoField_get_FieldHandle_m11937_MethodInfo,
	&MonoField_get_FieldType_m11938_MethodInfo,
	&MonoField_GetValue_m11948_MethodInfo,
	&FieldInfo_get_IsLiteral_m11869_MethodInfo,
	&FieldInfo_get_IsStatic_m11870_MethodInfo,
	&FieldInfo_get_IsInitOnly_m11871_MethodInfo,
	&FieldInfo_get_IsPublic_m11872_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m11873_MethodInfo,
	&MonoField_SetValue_m11951_MethodInfo,
	&FieldInfo_SetValue_m11874_MethodInfo,
	&MonoField_GetFieldOffset_m11946_MethodInfo,
	&FieldInfo_get_UMarshal_m11879_MethodInfo,
	&MonoField_GetObjectData_m11952_MethodInfo,
};
static bool MonoField_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoField_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoField_t_InterfacesOffsets[] = 
{
	{ &_FieldInfo_t2681_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoField_t_0_0_0;
extern const Il2CppType MonoField_t_1_0_0;
struct MonoField_t;
const Il2CppTypeDefinitionMetadata MonoField_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoField_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoField_t_InterfacesOffsets/* interfaceOffsets */
	, &FieldInfo_t_0_0_0/* parent */
	, MonoField_t_VTable/* vtableMethods */
	, MonoField_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1285/* fieldStart */

};
TypeInfo MonoField_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoField"/* name */
	, "System.Reflection"/* namespaze */
	, MonoField_t_MethodInfos/* methods */
	, MonoField_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoField_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoField_t_0_0_0/* byval_arg */
	, &MonoField_t_1_0_0/* this_arg */
	, &MonoField_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoField_t)/* instance_size */
	, sizeof (MonoField_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethod.h"
// Metadata Definition System.Reflection.MonoGenericMethod
extern TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoGenericMethod::.ctor()
extern const MethodInfo MonoGenericMethod__ctor_m11954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoGenericMethod__ctor_m11954/* method */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoGenericMethod::get_ReflectedType()
extern const MethodInfo MonoGenericMethod_get_ReflectedType_m11955_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoGenericMethod_get_ReflectedType_m11955/* method */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoGenericMethod_t_MethodInfos[] =
{
	&MonoGenericMethod__ctor_m11954_MethodInfo,
	&MonoGenericMethod_get_ReflectedType_m11955_MethodInfo,
	NULL
};
extern const MethodInfo MonoGenericMethod_get_ReflectedType_m11955_MethodInfo;
static const PropertyInfo MonoGenericMethod_t____ReflectedType_PropertyInfo = 
{
	&MonoGenericMethod_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoGenericMethod_get_ReflectedType_m11955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoGenericMethod_t_PropertyInfos[] =
{
	&MonoGenericMethod_t____ReflectedType_PropertyInfo,
	NULL
};
extern const MethodInfo MonoMethod_ToString_m11986_MethodInfo;
extern const MethodInfo MonoMethod_GetCustomAttributes_m11982_MethodInfo;
extern const MethodInfo MonoMethod_IsDefined_m11980_MethodInfo;
extern const MethodInfo MonoMethod_get_DeclaringType_m11978_MethodInfo;
extern const MethodInfo MonoMethod_get_Name_m11979_MethodInfo;
extern const MethodInfo MonoMethod_GetCustomAttributes_m11981_MethodInfo;
extern const MethodInfo MonoMethod_GetParameters_m11971_MethodInfo;
extern const MethodInfo MonoMethod_Invoke_m11973_MethodInfo;
extern const MethodInfo MonoMethod_get_MethodHandle_m11974_MethodInfo;
extern const MethodInfo MonoMethod_get_Attributes_m11975_MethodInfo;
extern const MethodInfo MonoMethod_get_CallingConvention_m11976_MethodInfo;
extern const MethodInfo MonoMethod_GetGenericArguments_m11990_MethodInfo;
extern const MethodInfo MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo;
extern const MethodInfo MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo;
extern const MethodInfo MonoMethod_get_IsGenericMethod_m11992_MethodInfo;
extern const MethodInfo MonoMethod_GetBaseDefinition_m11969_MethodInfo;
extern const MethodInfo MonoMethod_get_ReturnType_m11970_MethodInfo;
extern const MethodInfo MonoMethod_MakeGenericMethod_m11988_MethodInfo;
extern const MethodInfo MonoMethod_GetObjectData_m11987_MethodInfo;
static const Il2CppMethodReference MonoGenericMethod_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoMethod_ToString_m11986_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11982_MethodInfo,
	&MonoMethod_IsDefined_m11980_MethodInfo,
	&MonoMethod_get_DeclaringType_m11978_MethodInfo,
	&MethodInfo_get_MemberType_m11901_MethodInfo,
	&MonoMethod_get_Name_m11979_MethodInfo,
	&MonoGenericMethod_get_ReflectedType_m11955_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoMethod_IsDefined_m11980_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11981_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11982_MethodInfo,
	&MonoMethod_GetParameters_m11971_MethodInfo,
	&MethodBase_Invoke_m11891_MethodInfo,
	&MonoMethod_Invoke_m11973_MethodInfo,
	&MonoMethod_get_MethodHandle_m11974_MethodInfo,
	&MonoMethod_get_Attributes_m11975_MethodInfo,
	&MonoMethod_get_CallingConvention_m11976_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MonoMethod_GetGenericArguments_m11990_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m11992_MethodInfo,
	&MonoMethod_GetBaseDefinition_m11969_MethodInfo,
	&MonoMethod_get_ReturnType_m11970_MethodInfo,
	&MonoMethod_MakeGenericMethod_m11988_MethodInfo,
	&MonoMethod_GetObjectData_m11987_MethodInfo,
};
static bool MonoGenericMethod_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoGenericMethod_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 30},
	{ &_MethodInfo_t2683_0_0_0, 27},
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericMethod_t_0_0_0;
extern const Il2CppType MonoGenericMethod_t_1_0_0;
extern const Il2CppType MonoMethod_t_0_0_0;
struct MonoGenericMethod_t;
const Il2CppTypeDefinitionMetadata MonoGenericMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MonoMethod_t_0_0_0/* parent */
	, MonoGenericMethod_t_VTable/* vtableMethods */
	, MonoGenericMethod_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoGenericMethod_t_MethodInfos/* methods */
	, MonoGenericMethod_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericMethod_t_0_0_0/* byval_arg */
	, &MonoGenericMethod_t_1_0_0/* this_arg */
	, &MonoGenericMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericMethod_t)/* instance_size */
	, sizeof (MonoGenericMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethod.h"
// Metadata Definition System.Reflection.MonoGenericCMethod
extern TypeInfo MonoGenericCMethod_t2259_il2cpp_TypeInfo;
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoGenericCMethod::.ctor()
extern const MethodInfo MonoGenericCMethod__ctor_m11956_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoGenericCMethod__ctor_m11956/* method */
	, &MonoGenericCMethod_t2259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoGenericCMethod::get_ReflectedType()
extern const MethodInfo MonoGenericCMethod_get_ReflectedType_m11957_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoGenericCMethod_get_ReflectedType_m11957/* method */
	, &MonoGenericCMethod_t2259_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoGenericCMethod_t2259_MethodInfos[] =
{
	&MonoGenericCMethod__ctor_m11956_MethodInfo,
	&MonoGenericCMethod_get_ReflectedType_m11957_MethodInfo,
	NULL
};
extern const MethodInfo MonoGenericCMethod_get_ReflectedType_m11957_MethodInfo;
static const PropertyInfo MonoGenericCMethod_t2259____ReflectedType_PropertyInfo = 
{
	&MonoGenericCMethod_t2259_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoGenericCMethod_get_ReflectedType_m11957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoGenericCMethod_t2259_PropertyInfos[] =
{
	&MonoGenericCMethod_t2259____ReflectedType_PropertyInfo,
	NULL
};
extern const MethodInfo MonoCMethod_ToString_m12008_MethodInfo;
extern const MethodInfo MonoCMethod_GetCustomAttributes_m12007_MethodInfo;
extern const MethodInfo MonoCMethod_IsDefined_m12005_MethodInfo;
extern const MethodInfo MonoCMethod_get_DeclaringType_m12003_MethodInfo;
extern const MethodInfo MonoCMethod_get_Name_m12004_MethodInfo;
extern const MethodInfo MonoCMethod_GetCustomAttributes_m12006_MethodInfo;
extern const MethodInfo MonoCMethod_GetParameters_m11995_MethodInfo;
extern const MethodInfo MonoCMethod_Invoke_m11997_MethodInfo;
extern const MethodInfo MonoCMethod_get_MethodHandle_m11999_MethodInfo;
extern const MethodInfo MonoCMethod_get_Attributes_m12000_MethodInfo;
extern const MethodInfo MonoCMethod_get_CallingConvention_m12001_MethodInfo;
extern const MethodInfo MonoCMethod_Invoke_m11998_MethodInfo;
extern const MethodInfo MonoCMethod_GetObjectData_m12009_MethodInfo;
static const Il2CppMethodReference MonoGenericCMethod_t2259_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoCMethod_ToString_m12008_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12007_MethodInfo,
	&MonoCMethod_IsDefined_m12005_MethodInfo,
	&MonoCMethod_get_DeclaringType_m12003_MethodInfo,
	&ConstructorInfo_get_MemberType_m11859_MethodInfo,
	&MonoCMethod_get_Name_m12004_MethodInfo,
	&MonoGenericCMethod_get_ReflectedType_m11957_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoCMethod_IsDefined_m12005_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12006_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12007_MethodInfo,
	&MonoCMethod_GetParameters_m11995_MethodInfo,
	&MethodBase_Invoke_m11891_MethodInfo,
	&MonoCMethod_Invoke_m11997_MethodInfo,
	&MonoCMethod_get_MethodHandle_m11999_MethodInfo,
	&MonoCMethod_get_Attributes_m12000_MethodInfo,
	&MonoCMethod_get_CallingConvention_m12001_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodBase_GetGenericArguments_m11896_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m11897_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo,
	&MethodBase_get_IsGenericMethod_m11899_MethodInfo,
	&MonoCMethod_Invoke_m11998_MethodInfo,
	&MonoCMethod_GetObjectData_m12009_MethodInfo,
};
static bool MonoGenericCMethod_t2259_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoGenericCMethod_t2259_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 28},
	{ &_ConstructorInfo_t2679_0_0_0, 27},
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericCMethod_t2259_0_0_0;
extern const Il2CppType MonoGenericCMethod_t2259_1_0_0;
extern const Il2CppType MonoCMethod_t2260_0_0_0;
struct MonoGenericCMethod_t2259;
const Il2CppTypeDefinitionMetadata MonoGenericCMethod_t2259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericCMethod_t2259_InterfacesOffsets/* interfaceOffsets */
	, &MonoCMethod_t2260_0_0_0/* parent */
	, MonoGenericCMethod_t2259_VTable/* vtableMethods */
	, MonoGenericCMethod_t2259_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoGenericCMethod_t2259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoGenericCMethod_t2259_MethodInfos/* methods */
	, MonoGenericCMethod_t2259_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoGenericCMethod_t2259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericCMethod_t2259_0_0_0/* byval_arg */
	, &MonoGenericCMethod_t2259_1_0_0/* this_arg */
	, &MonoGenericCMethod_t2259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericCMethod_t2259)/* instance_size */
	, sizeof (MonoGenericCMethod_t2259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
// Metadata Definition System.Reflection.MonoMethodInfo
extern TypeInfo MonoMethodInfo_t2261_il2cpp_TypeInfo;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfoMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MonoMethodInfo_t2261_1_0_2;
extern const Il2CppType MonoMethodInfo_t2261_1_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_get_method_info_m11958_ParameterInfos[] = 
{
	{"handle", 0, 134221713, 0, &IntPtr_t_0_0_0},
	{"info", 1, 134221714, 0, &MonoMethodInfo_t2261_1_0_2},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_MonoMethodInfoU26_t3054 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethodInfo::get_method_info(System.IntPtr,System.Reflection.MonoMethodInfo&)
extern const MethodInfo MonoMethodInfo_get_method_info_m11958_MethodInfo = 
{
	"get_method_info"/* name */
	, (methodPointerType)&MonoMethodInfo_get_method_info_m11958/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_MonoMethodInfoU26_t3054/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_get_method_info_m11958_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetMethodInfo_m11959_ParameterInfos[] = 
{
	{"handle", 0, 134221715, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType MonoMethodInfo_t2261_0_0_0;
extern void* RuntimeInvoker_MonoMethodInfo_t2261_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoMethodInfo System.Reflection.MonoMethodInfo::GetMethodInfo(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetMethodInfo_m11959_MethodInfo = 
{
	"GetMethodInfo"/* name */
	, (methodPointerType)&MonoMethodInfo_GetMethodInfo_m11959/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodInfo_t2261_0_0_0/* return_type */
	, RuntimeInvoker_MonoMethodInfo_t2261_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetMethodInfo_m11959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetDeclaringType_m11960_ParameterInfos[] = 
{
	{"handle", 0, 134221716, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethodInfo::GetDeclaringType(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetDeclaringType_m11960_MethodInfo = 
{
	"GetDeclaringType"/* name */
	, (methodPointerType)&MonoMethodInfo_GetDeclaringType_m11960/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetDeclaringType_m11960_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetReturnType_m11961_ParameterInfos[] = 
{
	{"handle", 0, 134221717, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethodInfo::GetReturnType(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetReturnType_m11961_MethodInfo = 
{
	"GetReturnType"/* name */
	, (methodPointerType)&MonoMethodInfo_GetReturnType_m11961/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetReturnType_m11961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetAttributes_m11962_ParameterInfos[] = 
{
	{"handle", 0, 134221718, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_MethodAttributes_t2254_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoMethodInfo::GetAttributes(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetAttributes_m11962_MethodInfo = 
{
	"GetAttributes"/* name */
	, (methodPointerType)&MonoMethodInfo_GetAttributes_m11962/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t2254_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t2254_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetAttributes_m11962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetCallingConvention_m11963_ParameterInfos[] = 
{
	{"handle", 0, 134221719, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_CallingConventions_t2248_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoMethodInfo::GetCallingConvention(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetCallingConvention_m11963_MethodInfo = 
{
	"GetCallingConvention"/* name */
	, (methodPointerType)&MonoMethodInfo_GetCallingConvention_m11963/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t2248_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t2248_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetCallingConvention_m11963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_get_parameter_info_m11964_ParameterInfos[] = 
{
	{"handle", 0, 134221720, 0, &IntPtr_t_0_0_0},
	{"member", 1, 134221721, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::get_parameter_info(System.IntPtr,System.Reflection.MemberInfo)
extern const MethodInfo MonoMethodInfo_get_parameter_info_m11964_MethodInfo = 
{
	"get_parameter_info"/* name */
	, (methodPointerType)&MonoMethodInfo_get_parameter_info_m11964/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_Object_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_get_parameter_info_m11964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t2261_MonoMethodInfo_GetParametersInfo_m11965_ParameterInfos[] = 
{
	{"handle", 0, 134221722, 0, &IntPtr_t_0_0_0},
	{"member", 1, 134221723, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::GetParametersInfo(System.IntPtr,System.Reflection.MemberInfo)
extern const MethodInfo MonoMethodInfo_GetParametersInfo_m11965_MethodInfo = 
{
	"GetParametersInfo"/* name */
	, (methodPointerType)&MonoMethodInfo_GetParametersInfo_m11965/* method */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_Object_t/* invoker_method */
	, MonoMethodInfo_t2261_MonoMethodInfo_GetParametersInfo_m11965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethodInfo_t2261_MethodInfos[] =
{
	&MonoMethodInfo_get_method_info_m11958_MethodInfo,
	&MonoMethodInfo_GetMethodInfo_m11959_MethodInfo,
	&MonoMethodInfo_GetDeclaringType_m11960_MethodInfo,
	&MonoMethodInfo_GetReturnType_m11961_MethodInfo,
	&MonoMethodInfo_GetAttributes_m11962_MethodInfo,
	&MonoMethodInfo_GetCallingConvention_m11963_MethodInfo,
	&MonoMethodInfo_get_parameter_info_m11964_MethodInfo,
	&MonoMethodInfo_GetParametersInfo_m11965_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoMethodInfo_t2261_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool MonoMethodInfo_t2261_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
const Il2CppTypeDefinitionMetadata MonoMethodInfo_t2261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, MonoMethodInfo_t2261_VTable/* vtableMethods */
	, MonoMethodInfo_t2261_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1290/* fieldStart */

};
TypeInfo MonoMethodInfo_t2261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoMethodInfo_t2261_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoMethodInfo_t2261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodInfo_t2261_0_0_0/* byval_arg */
	, &MonoMethodInfo_t2261_1_0_0/* this_arg */
	, &MonoMethodInfo_t2261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodInfo_t2261)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoMethodInfo_t2261)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethod.h"
// Metadata Definition System.Reflection.MonoMethod
extern TypeInfo MonoMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethod::.ctor()
extern const MethodInfo MonoMethod__ctor_m11966_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoMethod__ctor_m11966/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t1440_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_get_name_m11967_ParameterInfos[] = 
{
	{"method", 0, 134221724, 0, &MethodBase_t1440_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::get_name(System.Reflection.MethodBase)
extern const MethodInfo MonoMethod_get_name_m11967_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&MonoMethod_get_name_m11967/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_get_name_m11967_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethod_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_get_base_definition_m11968_ParameterInfos[] = 
{
	{"method", 0, 134221725, 0, &MonoMethod_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoMethod System.Reflection.MonoMethod::get_base_definition(System.Reflection.MonoMethod)
extern const MethodInfo MonoMethod_get_base_definition_m11968_MethodInfo = 
{
	"get_base_definition"/* name */
	, (methodPointerType)&MonoMethod_get_base_definition_m11968/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethod_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_get_base_definition_m11968_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::GetBaseDefinition()
extern const MethodInfo MonoMethod_GetBaseDefinition_m11969_MethodInfo = 
{
	"GetBaseDefinition"/* name */
	, (methodPointerType)&MonoMethod_GetBaseDefinition_m11969/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_ReturnType()
extern const MethodInfo MonoMethod_get_ReturnType_m11970_MethodInfo = 
{
	"get_ReturnType"/* name */
	, (methodPointerType)&MonoMethod_get_ReturnType_m11970/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethod::GetParameters()
extern const MethodInfo MonoMethod_GetParameters_m11971_MethodInfo = 
{
	"GetParameters"/* name */
	, (methodPointerType)&MonoMethod_GetParameters_m11971/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType Exception_t148_1_0_2;
extern const Il2CppType Exception_t148_1_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_InternalInvoke_m11972_ParameterInfos[] = 
{
	{"obj", 0, 134221726, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221727, 0, &ObjectU5BU5D_t124_0_0_0},
	{"exc", 2, 134221728, 0, &Exception_t148_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2704 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern const MethodInfo MonoMethod_InternalInvoke_m11972_MethodInfo = 
{
	"InternalInvoke"/* name */
	, (methodPointerType)&MonoMethod_InternalInvoke_m11972/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2704/* invoker_method */
	, MonoMethod_t_MonoMethod_InternalInvoke_m11972_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_Invoke_m11973_ParameterInfos[] = 
{
	{"obj", 0, 134221729, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221730, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134221731, 0, &Binder_t1437_0_0_0},
	{"parameters", 3, 134221732, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 4, 134221733, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoMethod_Invoke_m11973_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoMethod_Invoke_m11973/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_Invoke_m11973_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t2551 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MonoMethod::get_MethodHandle()
extern const MethodInfo MonoMethod_get_MethodHandle_m11974_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, (methodPointerType)&MonoMethod_get_MethodHandle_m11974/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t2551_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t2551/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t2254 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoMethod::get_Attributes()
extern const MethodInfo MonoMethod_get_Attributes_m11975_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoMethod_get_Attributes_m11975/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t2254_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t2254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t2248 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoMethod::get_CallingConvention()
extern const MethodInfo MonoMethod_get_CallingConvention_m11976_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MonoMethod_get_CallingConvention_m11976/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t2248_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t2248/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_ReflectedType()
extern const MethodInfo MonoMethod_get_ReflectedType_m11977_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoMethod_get_ReflectedType_m11977/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_DeclaringType()
extern const MethodInfo MonoMethod_get_DeclaringType_m11978_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoMethod_get_DeclaringType_m11978/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::get_Name()
extern const MethodInfo MonoMethod_get_Name_m11979_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoMethod_get_Name_m11979/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_IsDefined_m11980_ParameterInfos[] = 
{
	{"attributeType", 0, 134221734, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221735, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoMethod_IsDefined_m11980_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoMethod_IsDefined_m11980/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoMethod_t_MonoMethod_IsDefined_m11980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetCustomAttributes_m11981_ParameterInfos[] = 
{
	{"inherit", 0, 134221736, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoMethod_GetCustomAttributes_m11981_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetCustomAttributes_m11981/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoMethod_t_MonoMethod_GetCustomAttributes_m11981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetCustomAttributes_m11982_ParameterInfos[] = 
{
	{"attributeType", 0, 134221737, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221738, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoMethod_GetCustomAttributes_m11982_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetCustomAttributes_m11982/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoMethod_t_MonoMethod_GetCustomAttributes_m11982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetDllImportAttribute_m11983_ParameterInfos[] = 
{
	{"mhandle", 0, 134221739, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType DllImportAttribute_t2056_0_0_0;
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.InteropServices.DllImportAttribute System.Reflection.MonoMethod::GetDllImportAttribute(System.IntPtr)
extern const MethodInfo MonoMethod_GetDllImportAttribute_m11983_MethodInfo = 
{
	"GetDllImportAttribute"/* name */
	, (methodPointerType)&MonoMethod_GetDllImportAttribute_m11983/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &DllImportAttribute_t2056_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethod_t_MonoMethod_GetDllImportAttribute_m11983_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetPseudoCustomAttributes()
extern const MethodInfo MonoMethod_GetPseudoCustomAttributes_m11984_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetPseudoCustomAttributes_m11984/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_ShouldPrintFullName_m11985_ParameterInfos[] = 
{
	{"type", 0, 134221740, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::ShouldPrintFullName(System.Type)
extern const MethodInfo MonoMethod_ShouldPrintFullName_m11985_MethodInfo = 
{
	"ShouldPrintFullName"/* name */
	, (methodPointerType)&MonoMethod_ShouldPrintFullName_m11985/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_ShouldPrintFullName_m11985_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::ToString()
extern const MethodInfo MonoMethod_ToString_m11986_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoMethod_ToString_m11986/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetObjectData_m11987_ParameterInfos[] = 
{
	{"info", 0, 134221741, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221742, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoMethod_GetObjectData_m11987_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoMethod_GetObjectData_m11987/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoMethod_t_MonoMethod_GetObjectData_m11987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_MakeGenericMethod_m11988_ParameterInfos[] = 
{
	{"methodInstantiation", 0, 134221743, 0, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod(System.Type[])
extern const MethodInfo MonoMethod_MakeGenericMethod_m11988_MethodInfo = 
{
	"MakeGenericMethod"/* name */
	, (methodPointerType)&MonoMethod_MakeGenericMethod_m11988/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_MakeGenericMethod_m11988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_MakeGenericMethod_impl_m11989_ParameterInfos[] = 
{
	{"types", 0, 134221744, 0, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod_impl(System.Type[])
extern const MethodInfo MonoMethod_MakeGenericMethod_impl_m11989_MethodInfo = 
{
	"MakeGenericMethod_impl"/* name */
	, (methodPointerType)&MonoMethod_MakeGenericMethod_impl_m11989/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_MakeGenericMethod_impl_m11989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoMethod::GetGenericArguments()
extern const MethodInfo MonoMethod_GetGenericArguments_m11990_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MonoMethod_GetGenericArguments_m11990/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethodDefinition()
extern const MethodInfo MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MonoMethod_get_IsGenericMethodDefinition_m11991/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod()
extern const MethodInfo MonoMethod_get_IsGenericMethod_m11992_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MonoMethod_get_IsGenericMethod_m11992/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_ContainsGenericParameters()
extern const MethodInfo MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MonoMethod_get_ContainsGenericParameters_m11993/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethod_t_MethodInfos[] =
{
	&MonoMethod__ctor_m11966_MethodInfo,
	&MonoMethod_get_name_m11967_MethodInfo,
	&MonoMethod_get_base_definition_m11968_MethodInfo,
	&MonoMethod_GetBaseDefinition_m11969_MethodInfo,
	&MonoMethod_get_ReturnType_m11970_MethodInfo,
	&MonoMethod_GetParameters_m11971_MethodInfo,
	&MonoMethod_InternalInvoke_m11972_MethodInfo,
	&MonoMethod_Invoke_m11973_MethodInfo,
	&MonoMethod_get_MethodHandle_m11974_MethodInfo,
	&MonoMethod_get_Attributes_m11975_MethodInfo,
	&MonoMethod_get_CallingConvention_m11976_MethodInfo,
	&MonoMethod_get_ReflectedType_m11977_MethodInfo,
	&MonoMethod_get_DeclaringType_m11978_MethodInfo,
	&MonoMethod_get_Name_m11979_MethodInfo,
	&MonoMethod_IsDefined_m11980_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11981_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11982_MethodInfo,
	&MonoMethod_GetDllImportAttribute_m11983_MethodInfo,
	&MonoMethod_GetPseudoCustomAttributes_m11984_MethodInfo,
	&MonoMethod_ShouldPrintFullName_m11985_MethodInfo,
	&MonoMethod_ToString_m11986_MethodInfo,
	&MonoMethod_GetObjectData_m11987_MethodInfo,
	&MonoMethod_MakeGenericMethod_m11988_MethodInfo,
	&MonoMethod_MakeGenericMethod_impl_m11989_MethodInfo,
	&MonoMethod_GetGenericArguments_m11990_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m11992_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo,
	NULL
};
static const PropertyInfo MonoMethod_t____ReturnType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ReturnType"/* name */
	, &MonoMethod_get_ReturnType_m11970_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____MethodHandle_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MonoMethod_get_MethodHandle_m11974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____Attributes_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoMethod_get_Attributes_m11975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____CallingConvention_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MonoMethod_get_CallingConvention_m11976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethod_get_ReflectedType_m11977_MethodInfo;
static const PropertyInfo MonoMethod_t____ReflectedType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoMethod_get_ReflectedType_m11977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____DeclaringType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoMethod_get_DeclaringType_m11978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____Name_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoMethod_get_Name_m11979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____IsGenericMethodDefinition_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____IsGenericMethod_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MonoMethod_get_IsGenericMethod_m11992_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____ContainsGenericParameters_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoMethod_t_PropertyInfos[] =
{
	&MonoMethod_t____ReturnType_PropertyInfo,
	&MonoMethod_t____MethodHandle_PropertyInfo,
	&MonoMethod_t____Attributes_PropertyInfo,
	&MonoMethod_t____CallingConvention_PropertyInfo,
	&MonoMethod_t____ReflectedType_PropertyInfo,
	&MonoMethod_t____DeclaringType_PropertyInfo,
	&MonoMethod_t____Name_PropertyInfo,
	&MonoMethod_t____IsGenericMethodDefinition_PropertyInfo,
	&MonoMethod_t____IsGenericMethod_PropertyInfo,
	&MonoMethod_t____ContainsGenericParameters_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoMethod_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoMethod_ToString_m11986_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11982_MethodInfo,
	&MonoMethod_IsDefined_m11980_MethodInfo,
	&MonoMethod_get_DeclaringType_m11978_MethodInfo,
	&MethodInfo_get_MemberType_m11901_MethodInfo,
	&MonoMethod_get_Name_m11979_MethodInfo,
	&MonoMethod_get_ReflectedType_m11977_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoMethod_IsDefined_m11980_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11981_MethodInfo,
	&MonoMethod_GetCustomAttributes_m11982_MethodInfo,
	&MonoMethod_GetParameters_m11971_MethodInfo,
	&MethodBase_Invoke_m11891_MethodInfo,
	&MonoMethod_Invoke_m11973_MethodInfo,
	&MonoMethod_get_MethodHandle_m11974_MethodInfo,
	&MonoMethod_get_Attributes_m11975_MethodInfo,
	&MonoMethod_get_CallingConvention_m11976_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MonoMethod_GetGenericArguments_m11990_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m11993_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m11991_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m11992_MethodInfo,
	&MonoMethod_GetBaseDefinition_m11969_MethodInfo,
	&MonoMethod_get_ReturnType_m11970_MethodInfo,
	&MonoMethod_MakeGenericMethod_m11988_MethodInfo,
	&MonoMethod_GetObjectData_m11987_MethodInfo,
};
static bool MonoMethod_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethod_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethod_t_InterfacesOffsets[] = 
{
	{ &_MethodInfo_t2683_0_0_0, 27},
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 30},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethod_t_1_0_0;
struct MonoMethod_t;
const Il2CppTypeDefinitionMetadata MonoMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethod_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodInfo_t_0_0_0/* parent */
	, MonoMethod_t_VTable/* vtableMethods */
	, MonoMethod_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1295/* fieldStart */

};
TypeInfo MonoMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoMethod_t_MethodInfos/* methods */
	, MonoMethod_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethod_t_0_0_0/* byval_arg */
	, &MonoMethod_t_1_0_0/* this_arg */
	, &MonoMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethod_t)/* instance_size */
	, sizeof (MonoMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 10/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethod.h"
// Metadata Definition System.Reflection.MonoCMethod
extern TypeInfo MonoCMethod_t2260_il2cpp_TypeInfo;
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoCMethod::.ctor()
extern const MethodInfo MonoCMethod__ctor_m11994_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoCMethod__ctor_m11994/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoCMethod::GetParameters()
extern const MethodInfo MonoCMethod_GetParameters_m11995_MethodInfo = 
{
	"GetParameters"/* name */
	, (methodPointerType)&MonoCMethod_GetParameters_m11995/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType Exception_t148_1_0_2;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_InternalInvoke_m11996_ParameterInfos[] = 
{
	{"obj", 0, 134221745, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221746, 0, &ObjectU5BU5D_t124_0_0_0},
	{"exc", 2, 134221747, 0, &Exception_t148_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2704 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern const MethodInfo MonoCMethod_InternalInvoke_m11996_MethodInfo = 
{
	"InternalInvoke"/* name */
	, (methodPointerType)&MonoCMethod_InternalInvoke_m11996/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2704/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_InternalInvoke_m11996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_Invoke_m11997_ParameterInfos[] = 
{
	{"obj", 0, 134221748, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221749, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134221750, 0, &Binder_t1437_0_0_0},
	{"parameters", 3, 134221751, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 4, 134221752, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoCMethod_Invoke_m11997_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoCMethod_Invoke_m11997/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_Invoke_m11997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_Invoke_m11998_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221753, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 1, 134221754, 0, &Binder_t1437_0_0_0},
	{"parameters", 2, 134221755, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 3, 134221756, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoCMethod_Invoke_m11998_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoCMethod_Invoke_m11998/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_Invoke_m11998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t2551 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MonoCMethod::get_MethodHandle()
extern const MethodInfo MonoCMethod_get_MethodHandle_m11999_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, (methodPointerType)&MonoCMethod_get_MethodHandle_m11999/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t2551_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t2551/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t2254 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoCMethod::get_Attributes()
extern const MethodInfo MonoCMethod_get_Attributes_m12000_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoCMethod_get_Attributes_m12000/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t2254_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t2254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t2248 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoCMethod::get_CallingConvention()
extern const MethodInfo MonoCMethod_get_CallingConvention_m12001_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MonoCMethod_get_CallingConvention_m12001/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t2248_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t2248/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoCMethod::get_ReflectedType()
extern const MethodInfo MonoCMethod_get_ReflectedType_m12002_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoCMethod_get_ReflectedType_m12002/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoCMethod::get_DeclaringType()
extern const MethodInfo MonoCMethod_get_DeclaringType_m12003_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoCMethod_get_DeclaringType_m12003/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoCMethod::get_Name()
extern const MethodInfo MonoCMethod_get_Name_m12004_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoCMethod_get_Name_m12004/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_IsDefined_m12005_ParameterInfos[] = 
{
	{"attributeType", 0, 134221757, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221758, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoCMethod::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoCMethod_IsDefined_m12005_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoCMethod_IsDefined_m12005/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_IsDefined_m12005_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_GetCustomAttributes_m12006_ParameterInfos[] = 
{
	{"inherit", 0, 134221759, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoCMethod_GetCustomAttributes_m12006_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCMethod_GetCustomAttributes_m12006/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_GetCustomAttributes_m12006_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_GetCustomAttributes_m12007_ParameterInfos[] = 
{
	{"attributeType", 0, 134221760, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221761, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoCMethod_GetCustomAttributes_m12007_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCMethod_GetCustomAttributes_m12007/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_GetCustomAttributes_m12007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoCMethod::ToString()
extern const MethodInfo MonoCMethod_ToString_m12008_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoCMethod_ToString_m12008/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoCMethod_t2260_MonoCMethod_GetObjectData_m12009_ParameterInfos[] = 
{
	{"info", 0, 134221762, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221763, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoCMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoCMethod_GetObjectData_m12009_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoCMethod_GetObjectData_m12009/* method */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoCMethod_t2260_MonoCMethod_GetObjectData_m12009_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoCMethod_t2260_MethodInfos[] =
{
	&MonoCMethod__ctor_m11994_MethodInfo,
	&MonoCMethod_GetParameters_m11995_MethodInfo,
	&MonoCMethod_InternalInvoke_m11996_MethodInfo,
	&MonoCMethod_Invoke_m11997_MethodInfo,
	&MonoCMethod_Invoke_m11998_MethodInfo,
	&MonoCMethod_get_MethodHandle_m11999_MethodInfo,
	&MonoCMethod_get_Attributes_m12000_MethodInfo,
	&MonoCMethod_get_CallingConvention_m12001_MethodInfo,
	&MonoCMethod_get_ReflectedType_m12002_MethodInfo,
	&MonoCMethod_get_DeclaringType_m12003_MethodInfo,
	&MonoCMethod_get_Name_m12004_MethodInfo,
	&MonoCMethod_IsDefined_m12005_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12006_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12007_MethodInfo,
	&MonoCMethod_ToString_m12008_MethodInfo,
	&MonoCMethod_GetObjectData_m12009_MethodInfo,
	NULL
};
static const PropertyInfo MonoCMethod_t2260____MethodHandle_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MonoCMethod_get_MethodHandle_m11999_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t2260____Attributes_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoCMethod_get_Attributes_m12000_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t2260____CallingConvention_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MonoCMethod_get_CallingConvention_m12001_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoCMethod_get_ReflectedType_m12002_MethodInfo;
static const PropertyInfo MonoCMethod_t2260____ReflectedType_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoCMethod_get_ReflectedType_m12002_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t2260____DeclaringType_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoCMethod_get_DeclaringType_m12003_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t2260____Name_PropertyInfo = 
{
	&MonoCMethod_t2260_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoCMethod_get_Name_m12004_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoCMethod_t2260_PropertyInfos[] =
{
	&MonoCMethod_t2260____MethodHandle_PropertyInfo,
	&MonoCMethod_t2260____Attributes_PropertyInfo,
	&MonoCMethod_t2260____CallingConvention_PropertyInfo,
	&MonoCMethod_t2260____ReflectedType_PropertyInfo,
	&MonoCMethod_t2260____DeclaringType_PropertyInfo,
	&MonoCMethod_t2260____Name_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoCMethod_t2260_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoCMethod_ToString_m12008_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12007_MethodInfo,
	&MonoCMethod_IsDefined_m12005_MethodInfo,
	&MonoCMethod_get_DeclaringType_m12003_MethodInfo,
	&ConstructorInfo_get_MemberType_m11859_MethodInfo,
	&MonoCMethod_get_Name_m12004_MethodInfo,
	&MonoCMethod_get_ReflectedType_m12002_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoCMethod_IsDefined_m12005_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12006_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m12007_MethodInfo,
	&MonoCMethod_GetParameters_m11995_MethodInfo,
	&MethodBase_Invoke_m11891_MethodInfo,
	&MonoCMethod_Invoke_m11997_MethodInfo,
	&MonoCMethod_get_MethodHandle_m11999_MethodInfo,
	&MonoCMethod_get_Attributes_m12000_MethodInfo,
	&MonoCMethod_get_CallingConvention_m12001_MethodInfo,
	&MethodBase_get_IsPublic_m11893_MethodInfo,
	&MethodBase_get_IsStatic_m11894_MethodInfo,
	&MethodBase_get_IsVirtual_m11895_MethodInfo,
	&MethodBase_GetGenericArguments_m11896_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m11897_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m11898_MethodInfo,
	&MethodBase_get_IsGenericMethod_m11899_MethodInfo,
	&MonoCMethod_Invoke_m11998_MethodInfo,
	&MonoCMethod_GetObjectData_m12009_MethodInfo,
};
static bool MonoCMethod_t2260_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoCMethod_t2260_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoCMethod_t2260_InterfacesOffsets[] = 
{
	{ &_ConstructorInfo_t2679_0_0_0, 27},
	{ &_MethodBase_t2682_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCMethod_t2260_1_0_0;
struct MonoCMethod_t2260;
const Il2CppTypeDefinitionMetadata MonoCMethod_t2260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoCMethod_t2260_InterfacesTypeInfos/* implementedInterfaces */
	, MonoCMethod_t2260_InterfacesOffsets/* interfaceOffsets */
	, &ConstructorInfo_t1293_0_0_0/* parent */
	, MonoCMethod_t2260_VTable/* vtableMethods */
	, MonoCMethod_t2260_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1298/* fieldStart */

};
TypeInfo MonoCMethod_t2260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoCMethod_t2260_MethodInfos/* methods */
	, MonoCMethod_t2260_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoCMethod_t2260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCMethod_t2260_0_0_0/* byval_arg */
	, &MonoCMethod_t2260_1_0_0/* this_arg */
	, &MonoCMethod_t2260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCMethod_t2260)/* instance_size */
	, sizeof (MonoCMethod_t2260)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 6/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
// Metadata Definition System.Reflection.MonoPropertyInfo
extern TypeInfo MonoPropertyInfo_t2262_il2cpp_TypeInfo;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfoMethodDeclarations.h"
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType MonoPropertyInfo_t2262_1_0_0;
extern const Il2CppType MonoPropertyInfo_t2262_1_0_0;
extern const Il2CppType PInfo_t2263_0_0_0;
extern const Il2CppType PInfo_t2263_0_0_0;
static const ParameterInfo MonoPropertyInfo_t2262_MonoPropertyInfo_get_property_info_m12010_ParameterInfos[] = 
{
	{"prop", 0, 134221764, 0, &MonoProperty_t_0_0_0},
	{"info", 1, 134221765, 0, &MonoPropertyInfo_t2262_1_0_0},
	{"req_info", 2, 134221766, 0, &PInfo_t2263_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_MonoPropertyInfoU26_t3055_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoPropertyInfo::get_property_info(System.Reflection.MonoProperty,System.Reflection.MonoPropertyInfo&,System.Reflection.PInfo)
extern const MethodInfo MonoPropertyInfo_get_property_info_m12010_MethodInfo = 
{
	"get_property_info"/* name */
	, (methodPointerType)&MonoPropertyInfo_get_property_info_m12010/* method */
	, &MonoPropertyInfo_t2262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_MonoPropertyInfoU26_t3055_Int32_t135/* invoker_method */
	, MonoPropertyInfo_t2262_MonoPropertyInfo_get_property_info_m12010_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoPropertyInfo_t2262_MonoPropertyInfo_GetTypeModifiers_m12011_ParameterInfos[] = 
{
	{"prop", 0, 134221767, 0, &MonoProperty_t_0_0_0},
	{"optional", 1, 134221768, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoPropertyInfo::GetTypeModifiers(System.Reflection.MonoProperty,System.Boolean)
extern const MethodInfo MonoPropertyInfo_GetTypeModifiers_m12011_MethodInfo = 
{
	"GetTypeModifiers"/* name */
	, (methodPointerType)&MonoPropertyInfo_GetTypeModifiers_m12011/* method */
	, &MonoPropertyInfo_t2262_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoPropertyInfo_t2262_MonoPropertyInfo_GetTypeModifiers_m12011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoPropertyInfo_t2262_MethodInfos[] =
{
	&MonoPropertyInfo_get_property_info_m12010_MethodInfo,
	&MonoPropertyInfo_GetTypeModifiers_m12011_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoPropertyInfo_t2262_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool MonoPropertyInfo_t2262_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoPropertyInfo_t2262_0_0_0;
const Il2CppTypeDefinitionMetadata MonoPropertyInfo_t2262_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, MonoPropertyInfo_t2262_VTable/* vtableMethods */
	, MonoPropertyInfo_t2262_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1301/* fieldStart */

};
TypeInfo MonoPropertyInfo_t2262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoPropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoPropertyInfo_t2262_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoPropertyInfo_t2262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoPropertyInfo_t2262_0_0_0/* byval_arg */
	, &MonoPropertyInfo_t2262_1_0_0/* this_arg */
	, &MonoPropertyInfo_t2262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoPropertyInfo_t2262)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoPropertyInfo_t2262)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfo.h"
// Metadata Definition System.Reflection.PInfo
extern TypeInfo PInfo_t2263_il2cpp_TypeInfo;
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfoMethodDeclarations.h"
static const MethodInfo* PInfo_t2263_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PInfo_t2263_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool PInfo_t2263_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PInfo_t2263_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PInfo_t2263_1_0_0;
const Il2CppTypeDefinitionMetadata PInfo_t2263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PInfo_t2263_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, PInfo_t2263_VTable/* vtableMethods */
	, PInfo_t2263_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1306/* fieldStart */

};
TypeInfo PInfo_t2263_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PInfo"/* name */
	, "System.Reflection"/* namespaze */
	, PInfo_t2263_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 425/* custom_attributes_cache */
	, &PInfo_t2263_0_0_0/* byval_arg */
	, &PInfo_t2263_1_0_0/* this_arg */
	, &PInfo_t2263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PInfo_t2263)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PInfo_t2263)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapter.h"
// Metadata Definition System.Reflection.MonoProperty/GetterAdapter
extern TypeInfo GetterAdapter_t2264_il2cpp_TypeInfo;
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo GetterAdapter_t2264_GetterAdapter__ctor_m12012_ParameterInfos[] = 
{
	{"object", 0, 134221798, 0, &Object_t_0_0_0},
	{"method", 1, 134221799, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty/GetterAdapter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo GetterAdapter__ctor_m12012_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GetterAdapter__ctor_m12012/* method */
	, &GetterAdapter_t2264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, GetterAdapter_t2264_GetterAdapter__ctor_m12012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetterAdapter_t2264_GetterAdapter_Invoke_m12013_ParameterInfos[] = 
{
	{"_this", 0, 134221800, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty/GetterAdapter::Invoke(System.Object)
extern const MethodInfo GetterAdapter_Invoke_m12013_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&GetterAdapter_Invoke_m12013/* method */
	, &GetterAdapter_t2264_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t2264_GetterAdapter_Invoke_m12013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetterAdapter_t2264_GetterAdapter_BeginInvoke_m12014_ParameterInfos[] = 
{
	{"_this", 0, 134221801, 0, &Object_t_0_0_0},
	{"callback", 1, 134221802, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134221803, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MonoProperty/GetterAdapter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo GetterAdapter_BeginInvoke_m12014_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&GetterAdapter_BeginInvoke_m12014/* method */
	, &GetterAdapter_t2264_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t2264_GetterAdapter_BeginInvoke_m12014_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo GetterAdapter_t2264_GetterAdapter_EndInvoke_m12015_ParameterInfos[] = 
{
	{"result", 0, 134221804, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty/GetterAdapter::EndInvoke(System.IAsyncResult)
extern const MethodInfo GetterAdapter_EndInvoke_m12015_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&GetterAdapter_EndInvoke_m12015/* method */
	, &GetterAdapter_t2264_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t2264_GetterAdapter_EndInvoke_m12015_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GetterAdapter_t2264_MethodInfos[] =
{
	&GetterAdapter__ctor_m12012_MethodInfo,
	&GetterAdapter_Invoke_m12013_MethodInfo,
	&GetterAdapter_BeginInvoke_m12014_MethodInfo,
	&GetterAdapter_EndInvoke_m12015_MethodInfo,
	NULL
};
extern const MethodInfo GetterAdapter_Invoke_m12013_MethodInfo;
extern const MethodInfo GetterAdapter_BeginInvoke_m12014_MethodInfo;
extern const MethodInfo GetterAdapter_EndInvoke_m12015_MethodInfo;
static const Il2CppMethodReference GetterAdapter_t2264_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&GetterAdapter_Invoke_m12013_MethodInfo,
	&GetterAdapter_BeginInvoke_m12014_MethodInfo,
	&GetterAdapter_EndInvoke_m12015_MethodInfo,
};
static bool GetterAdapter_t2264_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair GetterAdapter_t2264_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GetterAdapter_t2264_0_0_0;
extern const Il2CppType GetterAdapter_t2264_1_0_0;
extern TypeInfo MonoProperty_t_il2cpp_TypeInfo;
struct GetterAdapter_t2264;
const Il2CppTypeDefinitionMetadata GetterAdapter_t2264_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetterAdapter_t2264_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, GetterAdapter_t2264_VTable/* vtableMethods */
	, GetterAdapter_t2264_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GetterAdapter_t2264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetterAdapter"/* name */
	, ""/* namespaze */
	, GetterAdapter_t2264_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GetterAdapter_t2264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetterAdapter_t2264_0_0_0/* byval_arg */
	, &GetterAdapter_t2264_1_0_0/* this_arg */
	, &GetterAdapter_t2264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetterAdapter_t2264/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetterAdapter_t2264)/* instance_size */
	, sizeof (GetterAdapter_t2264)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/Getter`2
extern TypeInfo Getter_2_t2610_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Getter_2_t2610_Il2CppGenericContainer;
extern TypeInfo Getter_2_t2610_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Getter_2_t2610_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Getter_2_t2610_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Getter_2_t2610_gp_R_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Getter_2_t2610_gp_R_1_il2cpp_TypeInfo_GenericParamFull = { &Getter_2_t2610_Il2CppGenericContainer, NULL, "R", 1, 0 };
static const Il2CppGenericParameter* Getter_2_t2610_Il2CppGenericParametersArray[2] = 
{
	&Getter_2_t2610_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Getter_2_t2610_gp_R_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Getter_2_t2610_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Getter_2_t2610_il2cpp_TypeInfo, 2, 0, Getter_2_t2610_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Getter_2_t2610_Getter_2__ctor_m14552_ParameterInfos[] = 
{
	{"object", 0, 134221805, 0, &Object_t_0_0_0},
	{"method", 1, 134221806, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Reflection.MonoProperty/Getter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Getter_2__ctor_m14552_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Getter_2_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t2610_Getter_2__ctor_m14552_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t2610_gp_0_0_0_0;
extern const Il2CppType Getter_2_t2610_gp_0_0_0_0;
static const ParameterInfo Getter_2_t2610_Getter_2_Invoke_m14553_ParameterInfos[] = 
{
	{"_this", 0, 134221807, 0, &Getter_2_t2610_gp_0_0_0_0},
};
extern const Il2CppType Getter_2_t2610_gp_1_0_0_0;
// R System.Reflection.MonoProperty/Getter`2::Invoke(T)
extern const MethodInfo Getter_2_Invoke_m14553_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Getter_2_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Getter_2_t2610_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t2610_Getter_2_Invoke_m14553_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t2610_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Getter_2_t2610_Getter_2_BeginInvoke_m14554_ParameterInfos[] = 
{
	{"_this", 0, 134221808, 0, &Getter_2_t2610_gp_0_0_0_0},
	{"callback", 1, 134221809, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134221810, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Getter_2_BeginInvoke_m14554_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Getter_2_t2610_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t2610_Getter_2_BeginInvoke_m14554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Getter_2_t2610_Getter_2_EndInvoke_m14555_ParameterInfos[] = 
{
	{"result", 0, 134221811, 0, &IAsyncResult_t311_0_0_0},
};
// R System.Reflection.MonoProperty/Getter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Getter_2_EndInvoke_m14555_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Getter_2_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Getter_2_t2610_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t2610_Getter_2_EndInvoke_m14555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Getter_2_t2610_MethodInfos[] =
{
	&Getter_2__ctor_m14552_MethodInfo,
	&Getter_2_Invoke_m14553_MethodInfo,
	&Getter_2_BeginInvoke_m14554_MethodInfo,
	&Getter_2_EndInvoke_m14555_MethodInfo,
	NULL
};
extern const MethodInfo Getter_2_Invoke_m14553_MethodInfo;
extern const MethodInfo Getter_2_BeginInvoke_m14554_MethodInfo;
extern const MethodInfo Getter_2_EndInvoke_m14555_MethodInfo;
static const Il2CppMethodReference Getter_2_t2610_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Getter_2_Invoke_m14553_MethodInfo,
	&Getter_2_BeginInvoke_m14554_MethodInfo,
	&Getter_2_EndInvoke_m14555_MethodInfo,
};
static bool Getter_2_t2610_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Getter_2_t2610_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Getter_2_t2610_0_0_0;
extern const Il2CppType Getter_2_t2610_1_0_0;
struct Getter_2_t2610;
const Il2CppTypeDefinitionMetadata Getter_2_t2610_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Getter_2_t2610_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Getter_2_t2610_VTable/* vtableMethods */
	, Getter_2_t2610_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Getter_2_t2610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Getter`2"/* name */
	, ""/* namespaze */
	, Getter_2_t2610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Getter_2_t2610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Getter_2_t2610_0_0_0/* byval_arg */
	, &Getter_2_t2610_1_0_0/* this_arg */
	, &Getter_2_t2610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Getter_2_t2610_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/StaticGetter`1
extern TypeInfo StaticGetter_1_t2609_il2cpp_TypeInfo;
extern const Il2CppGenericContainer StaticGetter_1_t2609_Il2CppGenericContainer;
extern TypeInfo StaticGetter_1_t2609_gp_R_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter StaticGetter_1_t2609_gp_R_0_il2cpp_TypeInfo_GenericParamFull = { &StaticGetter_1_t2609_Il2CppGenericContainer, NULL, "R", 0, 0 };
static const Il2CppGenericParameter* StaticGetter_1_t2609_Il2CppGenericParametersArray[1] = 
{
	&StaticGetter_1_t2609_gp_R_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer StaticGetter_1_t2609_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&StaticGetter_1_t2609_il2cpp_TypeInfo, 1, 0, StaticGetter_1_t2609_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo StaticGetter_1_t2609_StaticGetter_1__ctor_m14556_ParameterInfos[] = 
{
	{"object", 0, 134221812, 0, &Object_t_0_0_0},
	{"method", 1, 134221813, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Reflection.MonoProperty/StaticGetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo StaticGetter_1__ctor_m14556_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &StaticGetter_1_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t2609_StaticGetter_1__ctor_m14556_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StaticGetter_1_t2609_gp_0_0_0_0;
// R System.Reflection.MonoProperty/StaticGetter`1::Invoke()
extern const MethodInfo StaticGetter_1_Invoke_m14557_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t2609_il2cpp_TypeInfo/* declaring_type */
	, &StaticGetter_1_t2609_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StaticGetter_1_t2609_StaticGetter_1_BeginInvoke_m14558_ParameterInfos[] = 
{
	{"callback", 0, 134221814, 0, &AsyncCallback_t312_0_0_0},
	{"object", 1, 134221815, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo StaticGetter_1_BeginInvoke_m14558_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t2609_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t2609_StaticGetter_1_BeginInvoke_m14558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo StaticGetter_1_t2609_StaticGetter_1_EndInvoke_m14559_ParameterInfos[] = 
{
	{"result", 0, 134221816, 0, &IAsyncResult_t311_0_0_0},
};
// R System.Reflection.MonoProperty/StaticGetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo StaticGetter_1_EndInvoke_m14559_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t2609_il2cpp_TypeInfo/* declaring_type */
	, &StaticGetter_1_t2609_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t2609_StaticGetter_1_EndInvoke_m14559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StaticGetter_1_t2609_MethodInfos[] =
{
	&StaticGetter_1__ctor_m14556_MethodInfo,
	&StaticGetter_1_Invoke_m14557_MethodInfo,
	&StaticGetter_1_BeginInvoke_m14558_MethodInfo,
	&StaticGetter_1_EndInvoke_m14559_MethodInfo,
	NULL
};
extern const MethodInfo StaticGetter_1_Invoke_m14557_MethodInfo;
extern const MethodInfo StaticGetter_1_BeginInvoke_m14558_MethodInfo;
extern const MethodInfo StaticGetter_1_EndInvoke_m14559_MethodInfo;
static const Il2CppMethodReference StaticGetter_1_t2609_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&StaticGetter_1_Invoke_m14557_MethodInfo,
	&StaticGetter_1_BeginInvoke_m14558_MethodInfo,
	&StaticGetter_1_EndInvoke_m14559_MethodInfo,
};
static bool StaticGetter_1_t2609_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StaticGetter_1_t2609_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StaticGetter_1_t2609_0_0_0;
extern const Il2CppType StaticGetter_1_t2609_1_0_0;
struct StaticGetter_1_t2609;
const Il2CppTypeDefinitionMetadata StaticGetter_1_t2609_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StaticGetter_1_t2609_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, StaticGetter_1_t2609_VTable/* vtableMethods */
	, StaticGetter_1_t2609_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StaticGetter_1_t2609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StaticGetter`1"/* name */
	, ""/* namespaze */
	, StaticGetter_1_t2609_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StaticGetter_1_t2609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StaticGetter_1_t2609_0_0_0/* byval_arg */
	, &StaticGetter_1_t2609_1_0_0/* this_arg */
	, &StaticGetter_1_t2609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &StaticGetter_1_t2609_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// Metadata Definition System.Reflection.MonoProperty
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoPropertyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::.ctor()
extern const MethodInfo MonoProperty__ctor_m12016_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoProperty__ctor_m12016/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PInfo_t2263_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_CachePropertyInfo_m12017_ParameterInfos[] = 
{
	{"flags", 0, 134221769, 0, &PInfo_t2263_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::CachePropertyInfo(System.Reflection.PInfo)
extern const MethodInfo MonoProperty_CachePropertyInfo_m12017_MethodInfo = 
{
	"CachePropertyInfo"/* name */
	, (methodPointerType)&MonoProperty_CachePropertyInfo_m12017/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, MonoProperty_t_MonoProperty_CachePropertyInfo_m12017_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyAttributes_t2269_0_0_0;
extern void* RuntimeInvoker_PropertyAttributes_t2269 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyAttributes System.Reflection.MonoProperty::get_Attributes()
extern const MethodInfo MonoProperty_get_Attributes_m12018_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoProperty_get_Attributes_m12018/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t2269_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t2269/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::get_CanRead()
extern const MethodInfo MonoProperty_get_CanRead_m12019_MethodInfo = 
{
	"get_CanRead"/* name */
	, (methodPointerType)&MonoProperty_get_CanRead_m12019/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::get_CanWrite()
extern const MethodInfo MonoProperty_get_CanWrite_m12020_MethodInfo = 
{
	"get_CanWrite"/* name */
	, (methodPointerType)&MonoProperty_get_CanWrite_m12020/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_PropertyType()
extern const MethodInfo MonoProperty_get_PropertyType_m12021_MethodInfo = 
{
	"get_PropertyType"/* name */
	, (methodPointerType)&MonoProperty_get_PropertyType_m12021/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_ReflectedType()
extern const MethodInfo MonoProperty_get_ReflectedType_m12022_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoProperty_get_ReflectedType_m12022/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_DeclaringType()
extern const MethodInfo MonoProperty_get_DeclaringType_m12023_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoProperty_get_DeclaringType_m12023/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoProperty::get_Name()
extern const MethodInfo MonoProperty_get_Name_m12024_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoProperty_get_Name_m12024/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetAccessors_m12025_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221770, 0, &Boolean_t176_0_0_0},
};
extern const Il2CppType MethodInfoU5BU5D_t149_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.Reflection.MonoProperty::GetAccessors(System.Boolean)
extern const MethodInfo MonoProperty_GetAccessors_m12025_MethodInfo = 
{
	"GetAccessors"/* name */
	, (methodPointerType)&MonoProperty_GetAccessors_m12025/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_GetAccessors_m12025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetGetMethod_m12026_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221771, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetGetMethod(System.Boolean)
extern const MethodInfo MonoProperty_GetGetMethod_m12026_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&MonoProperty_GetGetMethod_m12026/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_GetGetMethod_m12026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoProperty::GetIndexParameters()
extern const MethodInfo MonoProperty_GetIndexParameters_m12027_MethodInfo = 
{
	"GetIndexParameters"/* name */
	, (methodPointerType)&MonoProperty_GetIndexParameters_m12027/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetSetMethod_m12028_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221772, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetSetMethod(System.Boolean)
extern const MethodInfo MonoProperty_GetSetMethod_m12028_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&MonoProperty_GetSetMethod_m12028/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_GetSetMethod_m12028_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_IsDefined_m12029_ParameterInfos[] = 
{
	{"attributeType", 0, 134221773, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221774, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoProperty_IsDefined_m12029_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoProperty_IsDefined_m12029/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_IsDefined_m12029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetCustomAttributes_m12030_ParameterInfos[] = 
{
	{"inherit", 0, 134221775, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoProperty_GetCustomAttributes_m12030_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoProperty_GetCustomAttributes_m12030/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_GetCustomAttributes_m12030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetCustomAttributes_m12031_ParameterInfos[] = 
{
	{"attributeType", 0, 134221776, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221777, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoProperty_GetCustomAttributes_m12031_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoProperty_GetCustomAttributes_m12031/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoProperty_t_MonoProperty_GetCustomAttributes_m12031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t3059_0_0_0;
extern const Il2CppType Getter_2_t3059_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetterAdapterFrame_m14550_ParameterInfos[] = 
{
	{"getter", 0, 134221778, 0, &Getter_2_t3059_0_0_0},
	{"obj", 1, 134221779, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericContainer;
extern TypeInfo MonoProperty_GetterAdapterFrame_m14550_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_GetterAdapterFrame_m14550_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo MonoProperty_GetterAdapterFrame_m14550_gp_R_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_GetterAdapterFrame_m14550_gp_R_1_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericContainer, NULL, "R", 1, 0 };
static const Il2CppGenericParameter* MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericParametersArray[2] = 
{
	&MonoProperty_GetterAdapterFrame_m14550_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&MonoProperty_GetterAdapterFrame_m14550_gp_R_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo MonoProperty_GetterAdapterFrame_m14550_MethodInfo;
extern const Il2CppGenericContainer MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MonoProperty_GetterAdapterFrame_m14550_MethodInfo, 2, 1, MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericParametersArray };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m14550_gp_0_0_0_0;
extern const Il2CppGenericMethod Getter_2_Invoke_m14879_GenericMethod;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m14550_gp_1_0_0_0;
static Il2CppRGCTXDefinition MonoProperty_GetterAdapterFrame_m14550_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_GetterAdapterFrame_m14550_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Getter_2_Invoke_m14879_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_GetterAdapterFrame_m14550_gp_1_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern const MethodInfo MonoProperty_GetterAdapterFrame_m14550_MethodInfo = 
{
	"GetterAdapterFrame"/* name */
	, NULL/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MonoProperty_t_MonoProperty_GetterAdapterFrame_m14550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 3288/* token */
	, MonoProperty_GetterAdapterFrame_m14550_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &MonoProperty_GetterAdapterFrame_m14550_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType StaticGetter_1_t3062_0_0_0;
extern const Il2CppType StaticGetter_1_t3062_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_StaticGetterAdapterFrame_m14551_ParameterInfos[] = 
{
	{"getter", 0, 134221780, 0, &StaticGetter_1_t3062_0_0_0},
	{"obj", 1, 134221781, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericContainer;
extern TypeInfo MonoProperty_StaticGetterAdapterFrame_m14551_gp_R_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_StaticGetterAdapterFrame_m14551_gp_R_0_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericContainer, NULL, "R", 0, 0 };
static const Il2CppGenericParameter* MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericParametersArray[1] = 
{
	&MonoProperty_StaticGetterAdapterFrame_m14551_gp_R_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo MonoProperty_StaticGetterAdapterFrame_m14551_MethodInfo;
extern const Il2CppGenericContainer MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MonoProperty_StaticGetterAdapterFrame_m14551_MethodInfo, 1, 1, MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod StaticGetter_1_Invoke_m14880_GenericMethod;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m14551_gp_0_0_0_0;
static Il2CppRGCTXDefinition MonoProperty_StaticGetterAdapterFrame_m14551_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &StaticGetter_1_Invoke_m14880_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_StaticGetterAdapterFrame_m14551_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern const MethodInfo MonoProperty_StaticGetterAdapterFrame_m14551_MethodInfo = 
{
	"StaticGetterAdapterFrame"/* name */
	, NULL/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MonoProperty_t_MonoProperty_StaticGetterAdapterFrame_m14551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 3289/* token */
	, MonoProperty_StaticGetterAdapterFrame_m14551_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &MonoProperty_StaticGetterAdapterFrame_m14551_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_CreateGetterDelegate_m12032_ParameterInfos[] = 
{
	{"method", 0, 134221782, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoProperty/GetterAdapter System.Reflection.MonoProperty::CreateGetterDelegate(System.Reflection.MethodInfo)
extern const MethodInfo MonoProperty_CreateGetterDelegate_m12032_MethodInfo = 
{
	"CreateGetterDelegate"/* name */
	, (methodPointerType)&MonoProperty_CreateGetterDelegate_m12032/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &GetterAdapter_t2264_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_CreateGetterDelegate_m12032_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetValue_m12033_ParameterInfos[] = 
{
	{"obj", 0, 134221783, 0, &Object_t_0_0_0},
	{"index", 1, 134221784, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Object[])
extern const MethodInfo MonoProperty_GetValue_m12033_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoProperty_GetValue_m12033/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_GetValue_m12033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetValue_m12034_ParameterInfos[] = 
{
	{"obj", 0, 134221785, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221786, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134221787, 0, &Binder_t1437_0_0_0},
	{"index", 3, 134221788, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 4, 134221789, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoProperty_GetValue_m12034_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoProperty_GetValue_m12034/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_GetValue_m12034_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_SetValue_m12035_ParameterInfos[] = 
{
	{"obj", 0, 134221790, 0, &Object_t_0_0_0},
	{"value", 1, 134221791, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221792, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 3, 134221793, 0, &Binder_t1437_0_0_0},
	{"index", 4, 134221794, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 5, 134221795, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoProperty_SetValue_m12035_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&MonoProperty_SetValue_m12035/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_SetValue_m12035_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoProperty::ToString()
extern const MethodInfo MonoProperty_ToString_m12036_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoProperty_ToString_m12036/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoProperty::GetOptionalCustomModifiers()
extern const MethodInfo MonoProperty_GetOptionalCustomModifiers_m12037_MethodInfo = 
{
	"GetOptionalCustomModifiers"/* name */
	, (methodPointerType)&MonoProperty_GetOptionalCustomModifiers_m12037/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoProperty::GetRequiredCustomModifiers()
extern const MethodInfo MonoProperty_GetRequiredCustomModifiers_m12038_MethodInfo = 
{
	"GetRequiredCustomModifiers"/* name */
	, (methodPointerType)&MonoProperty_GetRequiredCustomModifiers_m12038/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetObjectData_m12039_ParameterInfos[] = 
{
	{"info", 0, 134221796, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221797, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoProperty_GetObjectData_m12039_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoProperty_GetObjectData_m12039/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoProperty_t_MonoProperty_GetObjectData_m12039_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoProperty_t_MethodInfos[] =
{
	&MonoProperty__ctor_m12016_MethodInfo,
	&MonoProperty_CachePropertyInfo_m12017_MethodInfo,
	&MonoProperty_get_Attributes_m12018_MethodInfo,
	&MonoProperty_get_CanRead_m12019_MethodInfo,
	&MonoProperty_get_CanWrite_m12020_MethodInfo,
	&MonoProperty_get_PropertyType_m12021_MethodInfo,
	&MonoProperty_get_ReflectedType_m12022_MethodInfo,
	&MonoProperty_get_DeclaringType_m12023_MethodInfo,
	&MonoProperty_get_Name_m12024_MethodInfo,
	&MonoProperty_GetAccessors_m12025_MethodInfo,
	&MonoProperty_GetGetMethod_m12026_MethodInfo,
	&MonoProperty_GetIndexParameters_m12027_MethodInfo,
	&MonoProperty_GetSetMethod_m12028_MethodInfo,
	&MonoProperty_IsDefined_m12029_MethodInfo,
	&MonoProperty_GetCustomAttributes_m12030_MethodInfo,
	&MonoProperty_GetCustomAttributes_m12031_MethodInfo,
	&MonoProperty_GetterAdapterFrame_m14550_MethodInfo,
	&MonoProperty_StaticGetterAdapterFrame_m14551_MethodInfo,
	&MonoProperty_CreateGetterDelegate_m12032_MethodInfo,
	&MonoProperty_GetValue_m12033_MethodInfo,
	&MonoProperty_GetValue_m12034_MethodInfo,
	&MonoProperty_SetValue_m12035_MethodInfo,
	&MonoProperty_ToString_m12036_MethodInfo,
	&MonoProperty_GetOptionalCustomModifiers_m12037_MethodInfo,
	&MonoProperty_GetRequiredCustomModifiers_m12038_MethodInfo,
	&MonoProperty_GetObjectData_m12039_MethodInfo,
	NULL
};
extern const MethodInfo MonoProperty_get_Attributes_m12018_MethodInfo;
static const PropertyInfo MonoProperty_t____Attributes_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoProperty_get_Attributes_m12018_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_CanRead_m12019_MethodInfo;
static const PropertyInfo MonoProperty_t____CanRead_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "CanRead"/* name */
	, &MonoProperty_get_CanRead_m12019_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_CanWrite_m12020_MethodInfo;
static const PropertyInfo MonoProperty_t____CanWrite_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "CanWrite"/* name */
	, &MonoProperty_get_CanWrite_m12020_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_PropertyType_m12021_MethodInfo;
static const PropertyInfo MonoProperty_t____PropertyType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "PropertyType"/* name */
	, &MonoProperty_get_PropertyType_m12021_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_ReflectedType_m12022_MethodInfo;
static const PropertyInfo MonoProperty_t____ReflectedType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoProperty_get_ReflectedType_m12022_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_DeclaringType_m12023_MethodInfo;
static const PropertyInfo MonoProperty_t____DeclaringType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoProperty_get_DeclaringType_m12023_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_Name_m12024_MethodInfo;
static const PropertyInfo MonoProperty_t____Name_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoProperty_get_Name_m12024_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoProperty_t_PropertyInfos[] =
{
	&MonoProperty_t____Attributes_PropertyInfo,
	&MonoProperty_t____CanRead_PropertyInfo,
	&MonoProperty_t____CanWrite_PropertyInfo,
	&MonoProperty_t____PropertyType_PropertyInfo,
	&MonoProperty_t____ReflectedType_PropertyInfo,
	&MonoProperty_t____DeclaringType_PropertyInfo,
	&MonoProperty_t____Name_PropertyInfo,
	NULL
};
static const Il2CppType* MonoProperty_t_il2cpp_TypeInfo__nestedTypes[3] =
{
	&GetterAdapter_t2264_0_0_0,
	&Getter_2_t2610_0_0_0,
	&StaticGetter_1_t2609_0_0_0,
};
extern const MethodInfo MonoProperty_ToString_m12036_MethodInfo;
extern const MethodInfo MonoProperty_GetCustomAttributes_m12031_MethodInfo;
extern const MethodInfo MonoProperty_IsDefined_m12029_MethodInfo;
extern const MethodInfo PropertyInfo_get_MemberType_m12059_MethodInfo;
extern const MethodInfo MonoProperty_GetCustomAttributes_m12030_MethodInfo;
extern const MethodInfo MonoProperty_GetAccessors_m12025_MethodInfo;
extern const MethodInfo MonoProperty_GetGetMethod_m12026_MethodInfo;
extern const MethodInfo MonoProperty_GetIndexParameters_m12027_MethodInfo;
extern const MethodInfo MonoProperty_GetSetMethod_m12028_MethodInfo;
extern const MethodInfo MonoProperty_GetValue_m12033_MethodInfo;
extern const MethodInfo MonoProperty_GetValue_m12034_MethodInfo;
extern const MethodInfo PropertyInfo_SetValue_m12061_MethodInfo;
extern const MethodInfo MonoProperty_SetValue_m12035_MethodInfo;
extern const MethodInfo MonoProperty_GetOptionalCustomModifiers_m12037_MethodInfo;
extern const MethodInfo MonoProperty_GetRequiredCustomModifiers_m12038_MethodInfo;
extern const MethodInfo MonoProperty_GetObjectData_m12039_MethodInfo;
static const Il2CppMethodReference MonoProperty_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&MonoProperty_ToString_m12036_MethodInfo,
	&MonoProperty_GetCustomAttributes_m12031_MethodInfo,
	&MonoProperty_IsDefined_m12029_MethodInfo,
	&MonoProperty_get_DeclaringType_m12023_MethodInfo,
	&PropertyInfo_get_MemberType_m12059_MethodInfo,
	&MonoProperty_get_Name_m12024_MethodInfo,
	&MonoProperty_get_ReflectedType_m12022_MethodInfo,
	&MemberInfo_get_Module_m10308_MethodInfo,
	&MonoProperty_IsDefined_m12029_MethodInfo,
	&MonoProperty_GetCustomAttributes_m12030_MethodInfo,
	&MonoProperty_GetCustomAttributes_m12031_MethodInfo,
	&MonoProperty_get_Attributes_m12018_MethodInfo,
	&MonoProperty_get_CanRead_m12019_MethodInfo,
	&MonoProperty_get_CanWrite_m12020_MethodInfo,
	&MonoProperty_get_PropertyType_m12021_MethodInfo,
	&MonoProperty_GetAccessors_m12025_MethodInfo,
	&MonoProperty_GetGetMethod_m12026_MethodInfo,
	&MonoProperty_GetIndexParameters_m12027_MethodInfo,
	&MonoProperty_GetSetMethod_m12028_MethodInfo,
	&MonoProperty_GetValue_m12033_MethodInfo,
	&MonoProperty_GetValue_m12034_MethodInfo,
	&PropertyInfo_SetValue_m12061_MethodInfo,
	&MonoProperty_SetValue_m12035_MethodInfo,
	&MonoProperty_GetOptionalCustomModifiers_m12037_MethodInfo,
	&MonoProperty_GetRequiredCustomModifiers_m12038_MethodInfo,
	&MonoProperty_GetObjectData_m12039_MethodInfo,
};
static bool MonoProperty_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoProperty_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
extern const Il2CppType _PropertyInfo_t2686_0_0_0;
static Il2CppInterfaceOffsetPair MonoProperty_t_InterfacesOffsets[] = 
{
	{ &_PropertyInfo_t2686_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoProperty_t_1_0_0;
struct MonoProperty_t;
const Il2CppTypeDefinitionMetadata MonoProperty_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoProperty_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MonoProperty_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoProperty_t_InterfacesOffsets/* interfaceOffsets */
	, &PropertyInfo_t_0_0_0/* parent */
	, MonoProperty_t_VTable/* vtableMethods */
	, MonoProperty_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1313/* fieldStart */

};
TypeInfo MonoProperty_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoProperty"/* name */
	, "System.Reflection"/* namespaze */
	, MonoProperty_t_MethodInfos/* methods */
	, MonoProperty_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoProperty_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoProperty_t_0_0_0/* byval_arg */
	, &MonoProperty_t_1_0_0/* this_arg */
	, &MonoProperty_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoProperty_t)/* instance_size */
	, sizeof (MonoProperty_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 7/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
// Metadata Definition System.Reflection.ParameterAttributes
extern TypeInfo ParameterAttributes_t2265_il2cpp_TypeInfo;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributesMethodDeclarations.h"
static const MethodInfo* ParameterAttributes_t2265_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ParameterAttributes_t2265_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ParameterAttributes_t2265_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ParameterAttributes_t2265_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterAttributes_t2265_0_0_0;
extern const Il2CppType ParameterAttributes_t2265_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterAttributes_t2265_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ParameterAttributes_t2265_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ParameterAttributes_t2265_VTable/* vtableMethods */
	, ParameterAttributes_t2265_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */

};
TypeInfo ParameterAttributes_t2265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterAttributes_t2265_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 426/* custom_attributes_cache */
	, &ParameterAttributes_t2265_0_0_0/* byval_arg */
	, &ParameterAttributes_t2265_1_0_0/* this_arg */
	, &ParameterAttributes_t2265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterAttributes_t2265)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterAttributes_t2265)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
// Metadata Definition System.Reflection.ParameterInfo
extern TypeInfo ParameterInfo_t1432_il2cpp_TypeInfo;
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor()
extern const MethodInfo ParameterInfo__ctor_m12040_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m12040/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ParameterBuilder_t2233_0_0_0;
extern const Il2CppType ParameterBuilder_t2233_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo ParameterInfo_t1432_ParameterInfo__ctor_m12041_ParameterInfos[] = 
{
	{"pb", 0, 134221817, 0, &ParameterBuilder_t2233_0_0_0},
	{"type", 1, 134221818, 0, &Type_t_0_0_0},
	{"member", 2, 134221819, 0, &MemberInfo_t_0_0_0},
	{"position", 3, 134221820, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.Emit.ParameterBuilder,System.Type,System.Reflection.MemberInfo,System.Int32)
extern const MethodInfo ParameterInfo__ctor_m12041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m12041/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Int32_t135/* invoker_method */
	, ParameterInfo_t1432_ParameterInfo__ctor_m12041_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ParameterInfo_t1432_0_0_0;
extern const Il2CppType ParameterInfo_t1432_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo ParameterInfo_t1432_ParameterInfo__ctor_m12042_ParameterInfos[] = 
{
	{"pinfo", 0, 134221821, 0, &ParameterInfo_t1432_0_0_0},
	{"member", 1, 134221822, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.ParameterInfo,System.Reflection.MemberInfo)
extern const MethodInfo ParameterInfo__ctor_m12042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m12042/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, ParameterInfo_t1432_ParameterInfo__ctor_m12042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.ParameterInfo::ToString()
extern const MethodInfo ParameterInfo_ToString_m12043_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ParameterInfo_ToString_m12043/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.ParameterInfo::get_ParameterType()
extern const MethodInfo ParameterInfo_get_ParameterType_m12044_MethodInfo = 
{
	"get_ParameterType"/* name */
	, (methodPointerType)&ParameterInfo_get_ParameterType_m12044/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ParameterAttributes_t2265 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes()
extern const MethodInfo ParameterInfo_get_Attributes_m12045_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&ParameterInfo_get_Attributes_m12045/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &ParameterAttributes_t2265_0_0_0/* return_type */
	, RuntimeInvoker_ParameterAttributes_t2265/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsIn()
extern const MethodInfo ParameterInfo_get_IsIn_m12046_MethodInfo = 
{
	"get_IsIn"/* name */
	, (methodPointerType)&ParameterInfo_get_IsIn_m12046/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsOptional()
extern const MethodInfo ParameterInfo_get_IsOptional_m12047_MethodInfo = 
{
	"get_IsOptional"/* name */
	, (methodPointerType)&ParameterInfo_get_IsOptional_m12047/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsOut()
extern const MethodInfo ParameterInfo_get_IsOut_m12048_MethodInfo = 
{
	"get_IsOut"/* name */
	, (methodPointerType)&ParameterInfo_get_IsOut_m12048/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsRetval()
extern const MethodInfo ParameterInfo_get_IsRetval_m12049_MethodInfo = 
{
	"get_IsRetval"/* name */
	, (methodPointerType)&ParameterInfo_get_IsRetval_m12049/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberInfo System.Reflection.ParameterInfo::get_Member()
extern const MethodInfo ParameterInfo_get_Member_m12050_MethodInfo = 
{
	"get_Member"/* name */
	, (methodPointerType)&ParameterInfo_get_Member_m12050/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &MemberInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.ParameterInfo::get_Name()
extern const MethodInfo ParameterInfo_get_Name_m12051_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ParameterInfo_get_Name_m12051/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.ParameterInfo::get_Position()
extern const MethodInfo ParameterInfo_get_Position_m12052_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&ParameterInfo_get_Position_m12052/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ParameterInfo_t1432_ParameterInfo_GetCustomAttributes_m12053_ParameterInfos[] = 
{
	{"attributeType", 0, 134221823, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221824, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.ParameterInfo::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo ParameterInfo_GetCustomAttributes_m12053_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&ParameterInfo_GetCustomAttributes_m12053/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, ParameterInfo_t1432_ParameterInfo_GetCustomAttributes_m12053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ParameterInfo_t1432_ParameterInfo_IsDefined_m12054_ParameterInfos[] = 
{
	{"attributeType", 0, 134221825, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221826, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::IsDefined(System.Type,System.Boolean)
extern const MethodInfo ParameterInfo_IsDefined_m12054_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&ParameterInfo_IsDefined_m12054/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, ParameterInfo_t1432_ParameterInfo_IsDefined_m12054_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.ParameterInfo::GetPseudoCustomAttributes()
extern const MethodInfo ParameterInfo_GetPseudoCustomAttributes_m12055_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&ParameterInfo_GetPseudoCustomAttributes_m12055/* method */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ParameterInfo_t1432_MethodInfos[] =
{
	&ParameterInfo__ctor_m12040_MethodInfo,
	&ParameterInfo__ctor_m12041_MethodInfo,
	&ParameterInfo__ctor_m12042_MethodInfo,
	&ParameterInfo_ToString_m12043_MethodInfo,
	&ParameterInfo_get_ParameterType_m12044_MethodInfo,
	&ParameterInfo_get_Attributes_m12045_MethodInfo,
	&ParameterInfo_get_IsIn_m12046_MethodInfo,
	&ParameterInfo_get_IsOptional_m12047_MethodInfo,
	&ParameterInfo_get_IsOut_m12048_MethodInfo,
	&ParameterInfo_get_IsRetval_m12049_MethodInfo,
	&ParameterInfo_get_Member_m12050_MethodInfo,
	&ParameterInfo_get_Name_m12051_MethodInfo,
	&ParameterInfo_get_Position_m12052_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m12053_MethodInfo,
	&ParameterInfo_IsDefined_m12054_MethodInfo,
	&ParameterInfo_GetPseudoCustomAttributes_m12055_MethodInfo,
	NULL
};
extern const MethodInfo ParameterInfo_get_ParameterType_m12044_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____ParameterType_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "ParameterType"/* name */
	, &ParameterInfo_get_ParameterType_m12044_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Attributes_m12045_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____Attributes_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &ParameterInfo_get_Attributes_m12045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsIn_m12046_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____IsIn_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "IsIn"/* name */
	, &ParameterInfo_get_IsIn_m12046_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsOptional_m12047_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____IsOptional_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "IsOptional"/* name */
	, &ParameterInfo_get_IsOptional_m12047_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsOut_m12048_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____IsOut_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "IsOut"/* name */
	, &ParameterInfo_get_IsOut_m12048_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsRetval_m12049_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____IsRetval_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "IsRetval"/* name */
	, &ParameterInfo_get_IsRetval_m12049_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Member_m12050_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____Member_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "Member"/* name */
	, &ParameterInfo_get_Member_m12050_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Name_m12051_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____Name_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ParameterInfo_get_Name_m12051_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Position_m12052_MethodInfo;
static const PropertyInfo ParameterInfo_t1432____Position_PropertyInfo = 
{
	&ParameterInfo_t1432_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &ParameterInfo_get_Position_m12052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ParameterInfo_t1432_PropertyInfos[] =
{
	&ParameterInfo_t1432____ParameterType_PropertyInfo,
	&ParameterInfo_t1432____Attributes_PropertyInfo,
	&ParameterInfo_t1432____IsIn_PropertyInfo,
	&ParameterInfo_t1432____IsOptional_PropertyInfo,
	&ParameterInfo_t1432____IsOut_PropertyInfo,
	&ParameterInfo_t1432____IsRetval_PropertyInfo,
	&ParameterInfo_t1432____Member_PropertyInfo,
	&ParameterInfo_t1432____Name_PropertyInfo,
	&ParameterInfo_t1432____Position_PropertyInfo,
	NULL
};
extern const MethodInfo ParameterInfo_ToString_m12043_MethodInfo;
extern const MethodInfo ParameterInfo_GetCustomAttributes_m12053_MethodInfo;
extern const MethodInfo ParameterInfo_IsDefined_m12054_MethodInfo;
static const Il2CppMethodReference ParameterInfo_t1432_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&ParameterInfo_ToString_m12043_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m12053_MethodInfo,
	&ParameterInfo_IsDefined_m12054_MethodInfo,
	&ParameterInfo_get_ParameterType_m12044_MethodInfo,
	&ParameterInfo_get_Attributes_m12045_MethodInfo,
	&ParameterInfo_get_Member_m12050_MethodInfo,
	&ParameterInfo_get_Name_m12051_MethodInfo,
	&ParameterInfo_get_Position_m12052_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m12053_MethodInfo,
	&ParameterInfo_IsDefined_m12054_MethodInfo,
};
static bool ParameterInfo_t1432_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _ParameterInfo_t2685_0_0_0;
static const Il2CppType* ParameterInfo_t1432_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t2605_0_0_0,
	&_ParameterInfo_t2685_0_0_0,
};
static Il2CppInterfaceOffsetPair ParameterInfo_t1432_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_ParameterInfo_t2685_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterInfo_t1432_1_0_0;
struct ParameterInfo_t1432;
const Il2CppTypeDefinitionMetadata ParameterInfo_t1432_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ParameterInfo_t1432_InterfacesTypeInfos/* implementedInterfaces */
	, ParameterInfo_t1432_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ParameterInfo_t1432_VTable/* vtableMethods */
	, ParameterInfo_t1432_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1330/* fieldStart */

};
TypeInfo ParameterInfo_t1432_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterInfo"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterInfo_t1432_MethodInfos/* methods */
	, ParameterInfo_t1432_PropertyInfos/* properties */
	, NULL/* events */
	, &ParameterInfo_t1432_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 427/* custom_attributes_cache */
	, &ParameterInfo_t1432_0_0_0/* byval_arg */
	, &ParameterInfo_t1432_1_0_0/* this_arg */
	, &ParameterInfo_t1432_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterInfo_t1432)/* instance_size */
	, sizeof (ParameterInfo_t1432)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 9/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
// Metadata Definition System.Reflection.ParameterModifier
extern TypeInfo ParameterModifier_t2266_il2cpp_TypeInfo;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifierMethodDeclarations.h"
static const MethodInfo* ParameterModifier_t2266_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ParameterModifier_t2266_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool ParameterModifier_t2266_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterModifier_t2266_0_0_0;
extern const Il2CppType ParameterModifier_t2266_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterModifier_t2266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, ParameterModifier_t2266_VTable/* vtableMethods */
	, ParameterModifier_t2266_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1337/* fieldStart */

};
TypeInfo ParameterModifier_t2266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterModifier"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterModifier_t2266_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ParameterModifier_t2266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 428/* custom_attributes_cache */
	, &ParameterModifier_t2266_0_0_0/* byval_arg */
	, &ParameterModifier_t2266_1_0_0/* this_arg */
	, &ParameterModifier_t2266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ParameterModifier_t2266_marshal/* marshal_to_native_func */
	, (methodPointerType)ParameterModifier_t2266_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ParameterModifier_t2266_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ParameterModifier_t2266)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterModifier_t2266)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ParameterModifier_t2266_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_Pointer.h"
// Metadata Definition System.Reflection.Pointer
extern TypeInfo Pointer_t2267_il2cpp_TypeInfo;
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_PointerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Pointer::.ctor()
extern const MethodInfo Pointer__ctor_m12056_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Pointer__ctor_m12056/* method */
	, &Pointer_t2267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo Pointer_t2267_Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_ParameterInfos[] = 
{
	{"info", 0, 134221827, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221828, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057/* method */
	, &Pointer_t2267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, Pointer_t2267_Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Pointer_t2267_MethodInfos[] =
{
	&Pointer__ctor_m12056_MethodInfo,
	&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_MethodInfo,
	NULL
};
extern const MethodInfo Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_MethodInfo;
static const Il2CppMethodReference Pointer_t2267_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12057_MethodInfo,
};
static bool Pointer_t2267_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Pointer_t2267_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair Pointer_t2267_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Pointer_t2267_0_0_0;
extern const Il2CppType Pointer_t2267_1_0_0;
struct Pointer_t2267;
const Il2CppTypeDefinitionMetadata Pointer_t2267_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Pointer_t2267_InterfacesTypeInfos/* implementedInterfaces */
	, Pointer_t2267_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Pointer_t2267_VTable/* vtableMethods */
	, Pointer_t2267_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1338/* fieldStart */

};
TypeInfo Pointer_t2267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Pointer"/* name */
	, "System.Reflection"/* namespaze */
	, Pointer_t2267_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Pointer_t2267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 429/* custom_attributes_cache */
	, &Pointer_t2267_0_0_0/* byval_arg */
	, &Pointer_t2267_1_0_0/* this_arg */
	, &Pointer_t2267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Pointer_t2267)/* instance_size */
	, sizeof (Pointer_t2267)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"
// Metadata Definition System.Reflection.ProcessorArchitecture
extern TypeInfo ProcessorArchitecture_t2268_il2cpp_TypeInfo;
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitectureMethodDeclarations.h"
static const MethodInfo* ProcessorArchitecture_t2268_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ProcessorArchitecture_t2268_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ProcessorArchitecture_t2268_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ProcessorArchitecture_t2268_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProcessorArchitecture_t2268_0_0_0;
extern const Il2CppType ProcessorArchitecture_t2268_1_0_0;
const Il2CppTypeDefinitionMetadata ProcessorArchitecture_t2268_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ProcessorArchitecture_t2268_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ProcessorArchitecture_t2268_VTable/* vtableMethods */
	, ProcessorArchitecture_t2268_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1340/* fieldStart */

};
TypeInfo ProcessorArchitecture_t2268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProcessorArchitecture"/* name */
	, "System.Reflection"/* namespaze */
	, ProcessorArchitecture_t2268_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 430/* custom_attributes_cache */
	, &ProcessorArchitecture_t2268_0_0_0/* byval_arg */
	, &ProcessorArchitecture_t2268_1_0_0/* this_arg */
	, &ProcessorArchitecture_t2268_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProcessorArchitecture_t2268)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ProcessorArchitecture_t2268)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
// Metadata Definition System.Reflection.PropertyAttributes
extern TypeInfo PropertyAttributes_t2269_il2cpp_TypeInfo;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributesMethodDeclarations.h"
static const MethodInfo* PropertyAttributes_t2269_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PropertyAttributes_t2269_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool PropertyAttributes_t2269_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttributes_t2269_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyAttributes_t2269_1_0_0;
const Il2CppTypeDefinitionMetadata PropertyAttributes_t2269_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttributes_t2269_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, PropertyAttributes_t2269_VTable/* vtableMethods */
	, PropertyAttributes_t2269_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1346/* fieldStart */

};
TypeInfo PropertyAttributes_t2269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, PropertyAttributes_t2269_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 431/* custom_attributes_cache */
	, &PropertyAttributes_t2269_0_0_0/* byval_arg */
	, &PropertyAttributes_t2269_1_0_0/* this_arg */
	, &PropertyAttributes_t2269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttributes_t2269)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PropertyAttributes_t2269)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
// Metadata Definition System.Reflection.PropertyInfo
extern TypeInfo PropertyInfo_t_il2cpp_TypeInfo;
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::.ctor()
extern const MethodInfo PropertyInfo__ctor_m12058_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyInfo__ctor_m12058/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PropertyAttributes_t2269 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyAttributes System.Reflection.PropertyInfo::get_Attributes()
extern const MethodInfo PropertyInfo_get_Attributes_m14560_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t2269_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t2269/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.PropertyInfo::get_CanRead()
extern const MethodInfo PropertyInfo_get_CanRead_m14561_MethodInfo = 
{
	"get_CanRead"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.PropertyInfo::get_CanWrite()
extern const MethodInfo PropertyInfo_get_CanWrite_m14562_MethodInfo = 
{
	"get_CanWrite"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.PropertyInfo::get_MemberType()
extern const MethodInfo PropertyInfo_get_MemberType_m12059_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&PropertyInfo_get_MemberType_m12059/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.PropertyInfo::get_PropertyType()
extern const MethodInfo PropertyInfo_get_PropertyType_m14563_MethodInfo = 
{
	"get_PropertyType"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetAccessors_m14564_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221829, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.Reflection.PropertyInfo::GetAccessors(System.Boolean)
extern const MethodInfo PropertyInfo_GetAccessors_m14564_MethodInfo = 
{
	"GetAccessors"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetAccessors_m14564_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetGetMethod_m14565_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221830, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean)
extern const MethodInfo PropertyInfo_GetGetMethod_m14565_MethodInfo = 
{
	"GetGetMethod"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetGetMethod_m14565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters()
extern const MethodInfo PropertyInfo_GetIndexParameters_m14566_MethodInfo = 
{
	"GetIndexParameters"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetSetMethod_m14567_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221831, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean)
extern const MethodInfo PropertyInfo_GetSetMethod_m14567_MethodInfo = 
{
	"GetSetMethod"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetSetMethod_m14567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetValue_m12060_ParameterInfos[] = 
{
	{"obj", 0, 134221832, 0, &Object_t_0_0_0},
	{"index", 1, 134221833, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[])
extern const MethodInfo PropertyInfo_GetValue_m12060_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&PropertyInfo_GetValue_m12060/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetValue_m12060_ParameterInfos/* parameters */
	, 433/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetValue_m14568_ParameterInfos[] = 
{
	{"obj", 0, 134221834, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221835, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134221836, 0, &Binder_t1437_0_0_0},
	{"index", 3, 134221837, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 4, 134221838, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo PropertyInfo_GetValue_m14568_MethodInfo = 
{
	"GetValue"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetValue_m14568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_SetValue_m12061_ParameterInfos[] = 
{
	{"obj", 0, 134221839, 0, &Object_t_0_0_0},
	{"value", 1, 134221840, 0, &Object_t_0_0_0},
	{"index", 2, 134221841, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[])
extern const MethodInfo PropertyInfo_SetValue_m12061_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&PropertyInfo_SetValue_m12061/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_SetValue_m12061_ParameterInfos/* parameters */
	, 434/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_SetValue_m14569_ParameterInfos[] = 
{
	{"obj", 0, 134221842, 0, &Object_t_0_0_0},
	{"value", 1, 134221843, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221844, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 3, 134221845, 0, &Binder_t1437_0_0_0},
	{"index", 4, 134221846, 0, &ObjectU5BU5D_t124_0_0_0},
	{"culture", 5, 134221847, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo PropertyInfo_SetValue_m14569_MethodInfo = 
{
	"SetValue"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_SetValue_m14569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.PropertyInfo::GetOptionalCustomModifiers()
extern const MethodInfo PropertyInfo_GetOptionalCustomModifiers_m12062_MethodInfo = 
{
	"GetOptionalCustomModifiers"/* name */
	, (methodPointerType)&PropertyInfo_GetOptionalCustomModifiers_m12062/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.PropertyInfo::GetRequiredCustomModifiers()
extern const MethodInfo PropertyInfo_GetRequiredCustomModifiers_m12063_MethodInfo = 
{
	"GetRequiredCustomModifiers"/* name */
	, (methodPointerType)&PropertyInfo_GetRequiredCustomModifiers_m12063/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyInfo_t_MethodInfos[] =
{
	&PropertyInfo__ctor_m12058_MethodInfo,
	&PropertyInfo_get_Attributes_m14560_MethodInfo,
	&PropertyInfo_get_CanRead_m14561_MethodInfo,
	&PropertyInfo_get_CanWrite_m14562_MethodInfo,
	&PropertyInfo_get_MemberType_m12059_MethodInfo,
	&PropertyInfo_get_PropertyType_m14563_MethodInfo,
	&PropertyInfo_GetAccessors_m14564_MethodInfo,
	&PropertyInfo_GetGetMethod_m14565_MethodInfo,
	&PropertyInfo_GetIndexParameters_m14566_MethodInfo,
	&PropertyInfo_GetSetMethod_m14567_MethodInfo,
	&PropertyInfo_GetValue_m12060_MethodInfo,
	&PropertyInfo_GetValue_m14568_MethodInfo,
	&PropertyInfo_SetValue_m12061_MethodInfo,
	&PropertyInfo_SetValue_m14569_MethodInfo,
	&PropertyInfo_GetOptionalCustomModifiers_m12062_MethodInfo,
	&PropertyInfo_GetRequiredCustomModifiers_m12063_MethodInfo,
	NULL
};
extern const MethodInfo PropertyInfo_get_Attributes_m14560_MethodInfo;
static const PropertyInfo PropertyInfo_t____Attributes_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &PropertyInfo_get_Attributes_m14560_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_CanRead_m14561_MethodInfo;
static const PropertyInfo PropertyInfo_t____CanRead_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "CanRead"/* name */
	, &PropertyInfo_get_CanRead_m14561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_CanWrite_m14562_MethodInfo;
static const PropertyInfo PropertyInfo_t____CanWrite_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "CanWrite"/* name */
	, &PropertyInfo_get_CanWrite_m14562_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo PropertyInfo_t____MemberType_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &PropertyInfo_get_MemberType_m12059_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_PropertyType_m14563_MethodInfo;
static const PropertyInfo PropertyInfo_t____PropertyType_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "PropertyType"/* name */
	, &PropertyInfo_get_PropertyType_m14563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PropertyInfo_t_PropertyInfos[] =
{
	&PropertyInfo_t____Attributes_PropertyInfo,
	&PropertyInfo_t____CanRead_PropertyInfo,
	&PropertyInfo_t____CanWrite_PropertyInfo,
	&PropertyInfo_t____MemberType_PropertyInfo,
	&PropertyInfo_t____PropertyType_PropertyInfo,
	NULL
};
extern const MethodInfo PropertyInfo_GetValue_m12060_MethodInfo;
extern const MethodInfo PropertyInfo_GetOptionalCustomModifiers_m12062_MethodInfo;
extern const MethodInfo PropertyInfo_GetRequiredCustomModifiers_m12063_MethodInfo;
static const Il2CppMethodReference PropertyInfo_t_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MemberInfo_GetCustomAttributes_m14204_MethodInfo,
	&MemberInfo_IsDefined_m14202_MethodInfo,
	NULL,
	&PropertyInfo_get_MemberType_m12059_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m10308_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&PropertyInfo_GetValue_m12060_MethodInfo,
	NULL,
	&PropertyInfo_SetValue_m12061_MethodInfo,
	NULL,
	&PropertyInfo_GetOptionalCustomModifiers_m12062_MethodInfo,
	&PropertyInfo_GetRequiredCustomModifiers_m12063_MethodInfo,
};
static bool PropertyInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PropertyInfo_t_InterfacesTypeInfos[] = 
{
	&_PropertyInfo_t2686_0_0_0,
};
static Il2CppInterfaceOffsetPair PropertyInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &_PropertyInfo_t2686_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyInfo_t_1_0_0;
struct PropertyInfo_t;
const Il2CppTypeDefinitionMetadata PropertyInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PropertyInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, PropertyInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, PropertyInfo_t_VTable/* vtableMethods */
	, PropertyInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, PropertyInfo_t_MethodInfos/* methods */
	, PropertyInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &PropertyInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 432/* custom_attributes_cache */
	, &PropertyInfo_t_0_0_0/* byval_arg */
	, &PropertyInfo_t_1_0_0/* this_arg */
	, &PropertyInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyInfo_t)/* instance_size */
	, sizeof (PropertyInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPair.h"
// Metadata Definition System.Reflection.StrongNameKeyPair
extern TypeInfo StrongNameKeyPair_t2243_il2cpp_TypeInfo;
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPairMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo StrongNameKeyPair_t2243_StrongNameKeyPair__ctor_m12064_ParameterInfos[] = 
{
	{"info", 0, 134221848, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221849, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo StrongNameKeyPair__ctor_m12064_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StrongNameKeyPair__ctor_m12064/* method */
	, &StrongNameKeyPair_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, StrongNameKeyPair_t2243_StrongNameKeyPair__ctor_m12064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo StrongNameKeyPair_t2243_StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_ParameterInfos[] = 
{
	{"info", 0, 134221850, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221851, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065/* method */
	, &StrongNameKeyPair_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, StrongNameKeyPair_t2243_StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StrongNameKeyPair_t2243_StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_ParameterInfos[] = 
{
	{"sender", 0, 134221852, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066/* method */
	, &StrongNameKeyPair_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StrongNameKeyPair_t2243_StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StrongNameKeyPair_t2243_MethodInfos[] =
{
	&StrongNameKeyPair__ctor_m12064_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_MethodInfo,
	NULL
};
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_MethodInfo;
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_MethodInfo;
static const Il2CppMethodReference StrongNameKeyPair_t2243_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m12065_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12066_MethodInfo,
};
static bool StrongNameKeyPair_t2243_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* StrongNameKeyPair_t2243_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
	&IDeserializationCallback_t1615_0_0_0,
};
static Il2CppInterfaceOffsetPair StrongNameKeyPair_t2243_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &IDeserializationCallback_t1615_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongNameKeyPair_t2243_0_0_0;
extern const Il2CppType StrongNameKeyPair_t2243_1_0_0;
struct StrongNameKeyPair_t2243;
const Il2CppTypeDefinitionMetadata StrongNameKeyPair_t2243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StrongNameKeyPair_t2243_InterfacesTypeInfos/* implementedInterfaces */
	, StrongNameKeyPair_t2243_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongNameKeyPair_t2243_VTable/* vtableMethods */
	, StrongNameKeyPair_t2243_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1355/* fieldStart */

};
TypeInfo StrongNameKeyPair_t2243_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongNameKeyPair"/* name */
	, "System.Reflection"/* namespaze */
	, StrongNameKeyPair_t2243_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StrongNameKeyPair_t2243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 435/* custom_attributes_cache */
	, &StrongNameKeyPair_t2243_0_0_0/* byval_arg */
	, &StrongNameKeyPair_t2243_1_0_0/* this_arg */
	, &StrongNameKeyPair_t2243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongNameKeyPair_t2243)/* instance_size */
	, sizeof (StrongNameKeyPair_t2243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetException.h"
// Metadata Definition System.Reflection.TargetException
extern TypeInfo TargetException_t2270_il2cpp_TypeInfo;
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor()
extern const MethodInfo TargetException__ctor_m12067_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m12067/* method */
	, &TargetException_t2270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TargetException_t2270_TargetException__ctor_m12068_ParameterInfos[] = 
{
	{"message", 0, 134221853, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor(System.String)
extern const MethodInfo TargetException__ctor_m12068_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m12068/* method */
	, &TargetException_t2270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TargetException_t2270_TargetException__ctor_m12068_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TargetException_t2270_TargetException__ctor_m12069_ParameterInfos[] = 
{
	{"info", 0, 134221854, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221855, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetException__ctor_m12069_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m12069/* method */
	, &TargetException_t2270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TargetException_t2270_TargetException__ctor_m12069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetException_t2270_MethodInfos[] =
{
	&TargetException__ctor_m12067_MethodInfo,
	&TargetException__ctor_m12068_MethodInfo,
	&TargetException__ctor_m12069_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m7194_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7195_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7196_MethodInfo;
extern const MethodInfo Exception_get_Message_m7197_MethodInfo;
extern const MethodInfo Exception_get_Source_m7198_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7199_MethodInfo;
extern const MethodInfo Exception_GetType_m7200_MethodInfo;
static const Il2CppMethodReference TargetException_t2270_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool TargetException_t2270_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t1479_0_0_0;
static Il2CppInterfaceOffsetPair TargetException_t2270_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetException_t2270_0_0_0;
extern const Il2CppType TargetException_t2270_1_0_0;
extern const Il2CppType Exception_t148_0_0_0;
struct TargetException_t2270;
const Il2CppTypeDefinitionMetadata TargetException_t2270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetException_t2270_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t148_0_0_0/* parent */
	, TargetException_t2270_VTable/* vtableMethods */
	, TargetException_t2270_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetException_t2270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetException_t2270_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetException_t2270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 436/* custom_attributes_cache */
	, &TargetException_t2270_0_0_0/* byval_arg */
	, &TargetException_t2270_1_0_0/* this_arg */
	, &TargetException_t2270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetException_t2270)/* instance_size */
	, sizeof (TargetException_t2270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationException.h"
// Metadata Definition System.Reflection.TargetInvocationException
extern TypeInfo TargetInvocationException_t2271_il2cpp_TypeInfo;
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationExceptionMethodDeclarations.h"
extern const Il2CppType Exception_t148_0_0_0;
static const ParameterInfo TargetInvocationException_t2271_TargetInvocationException__ctor_m12070_ParameterInfos[] = 
{
	{"inner", 0, 134221856, 0, &Exception_t148_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Exception)
extern const MethodInfo TargetInvocationException__ctor_m12070_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetInvocationException__ctor_m12070/* method */
	, &TargetInvocationException_t2271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TargetInvocationException_t2271_TargetInvocationException__ctor_m12070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TargetInvocationException_t2271_TargetInvocationException__ctor_m12071_ParameterInfos[] = 
{
	{"info", 0, 134221857, 0, &SerializationInfo_t1388_0_0_0},
	{"sc", 1, 134221858, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetInvocationException__ctor_m12071_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetInvocationException__ctor_m12071/* method */
	, &TargetInvocationException_t2271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TargetInvocationException_t2271_TargetInvocationException__ctor_m12071_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetInvocationException_t2271_MethodInfos[] =
{
	&TargetInvocationException__ctor_m12070_MethodInfo,
	&TargetInvocationException__ctor_m12071_MethodInfo,
	NULL
};
static const Il2CppMethodReference TargetInvocationException_t2271_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool TargetInvocationException_t2271_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TargetInvocationException_t2271_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetInvocationException_t2271_0_0_0;
extern const Il2CppType TargetInvocationException_t2271_1_0_0;
struct TargetInvocationException_t2271;
const Il2CppTypeDefinitionMetadata TargetInvocationException_t2271_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetInvocationException_t2271_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t148_0_0_0/* parent */
	, TargetInvocationException_t2271_VTable/* vtableMethods */
	, TargetInvocationException_t2271_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetInvocationException_t2271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetInvocationException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetInvocationException_t2271_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetInvocationException_t2271_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 437/* custom_attributes_cache */
	, &TargetInvocationException_t2271_0_0_0/* byval_arg */
	, &TargetInvocationException_t2271_1_0_0/* this_arg */
	, &TargetInvocationException_t2271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetInvocationException_t2271)/* instance_size */
	, sizeof (TargetInvocationException_t2271)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountException.h"
// Metadata Definition System.Reflection.TargetParameterCountException
extern TypeInfo TargetParameterCountException_t2272_il2cpp_TypeInfo;
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor()
extern const MethodInfo TargetParameterCountException__ctor_m12072_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m12072/* method */
	, &TargetParameterCountException_t2272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TargetParameterCountException_t2272_TargetParameterCountException__ctor_m12073_ParameterInfos[] = 
{
	{"message", 0, 134221859, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.String)
extern const MethodInfo TargetParameterCountException__ctor_m12073_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m12073/* method */
	, &TargetParameterCountException_t2272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TargetParameterCountException_t2272_TargetParameterCountException__ctor_m12073_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TargetParameterCountException_t2272_TargetParameterCountException__ctor_m12074_ParameterInfos[] = 
{
	{"info", 0, 134221860, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221861, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetParameterCountException__ctor_m12074_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m12074/* method */
	, &TargetParameterCountException_t2272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TargetParameterCountException_t2272_TargetParameterCountException__ctor_m12074_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetParameterCountException_t2272_MethodInfos[] =
{
	&TargetParameterCountException__ctor_m12072_MethodInfo,
	&TargetParameterCountException__ctor_m12073_MethodInfo,
	&TargetParameterCountException__ctor_m12074_MethodInfo,
	NULL
};
static const Il2CppMethodReference TargetParameterCountException_t2272_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool TargetParameterCountException_t2272_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TargetParameterCountException_t2272_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetParameterCountException_t2272_0_0_0;
extern const Il2CppType TargetParameterCountException_t2272_1_0_0;
struct TargetParameterCountException_t2272;
const Il2CppTypeDefinitionMetadata TargetParameterCountException_t2272_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetParameterCountException_t2272_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t148_0_0_0/* parent */
	, TargetParameterCountException_t2272_VTable/* vtableMethods */
	, TargetParameterCountException_t2272_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetParameterCountException_t2272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetParameterCountException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetParameterCountException_t2272_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetParameterCountException_t2272_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 438/* custom_attributes_cache */
	, &TargetParameterCountException_t2272_0_0_0/* byval_arg */
	, &TargetParameterCountException_t2272_1_0_0/* this_arg */
	, &TargetParameterCountException_t2272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetParameterCountException_t2272)/* instance_size */
	, sizeof (TargetParameterCountException_t2272)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// Metadata Definition System.Reflection.TypeAttributes
extern TypeInfo TypeAttributes_t2273_il2cpp_TypeInfo;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributesMethodDeclarations.h"
static const MethodInfo* TypeAttributes_t2273_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeAttributes_t2273_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TypeAttributes_t2273_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeAttributes_t2273_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeAttributes_t2273_0_0_0;
extern const Il2CppType TypeAttributes_t2273_1_0_0;
const Il2CppTypeDefinitionMetadata TypeAttributes_t2273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeAttributes_t2273_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TypeAttributes_t2273_VTable/* vtableMethods */
	, TypeAttributes_t2273_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1359/* fieldStart */

};
TypeInfo TypeAttributes_t2273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, TypeAttributes_t2273_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 439/* custom_attributes_cache */
	, &TypeAttributes_t2273_0_0_0/* byval_arg */
	, &TypeAttributes_t2273_1_0_0/* this_arg */
	, &TypeAttributes_t2273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeAttributes_t2273)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeAttributes_t2273)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 32/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// Metadata Definition System.Resources.NeutralResourcesLanguageAttribute
extern TypeInfo NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo;
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NeutralResourcesLanguageAttribute_t1594_NeutralResourcesLanguageAttribute__ctor_m7295_ParameterInfos[] = 
{
	{"cultureName", 0, 134221862, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern const MethodInfo NeutralResourcesLanguageAttribute__ctor_m7295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NeutralResourcesLanguageAttribute__ctor_m7295/* method */
	, &NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NeutralResourcesLanguageAttribute_t1594_NeutralResourcesLanguageAttribute__ctor_m7295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NeutralResourcesLanguageAttribute_t1594_MethodInfos[] =
{
	&NeutralResourcesLanguageAttribute__ctor_m7295_MethodInfo,
	NULL
};
static const Il2CppMethodReference NeutralResourcesLanguageAttribute_t1594_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool NeutralResourcesLanguageAttribute_t1594_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NeutralResourcesLanguageAttribute_t1594_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NeutralResourcesLanguageAttribute_t1594_0_0_0;
extern const Il2CppType NeutralResourcesLanguageAttribute_t1594_1_0_0;
struct NeutralResourcesLanguageAttribute_t1594;
const Il2CppTypeDefinitionMetadata NeutralResourcesLanguageAttribute_t1594_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NeutralResourcesLanguageAttribute_t1594_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, NeutralResourcesLanguageAttribute_t1594_VTable/* vtableMethods */
	, NeutralResourcesLanguageAttribute_t1594_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1391/* fieldStart */

};
TypeInfo NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NeutralResourcesLanguageAttribute"/* name */
	, "System.Resources"/* namespaze */
	, NeutralResourcesLanguageAttribute_t1594_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 440/* custom_attributes_cache */
	, &NeutralResourcesLanguageAttribute_t1594_0_0_0/* byval_arg */
	, &NeutralResourcesLanguageAttribute_t1594_1_0_0/* this_arg */
	, &NeutralResourcesLanguageAttribute_t1594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NeutralResourcesLanguageAttribute_t1594)/* instance_size */
	, sizeof (NeutralResourcesLanguageAttribute_t1594)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// Metadata Definition System.Resources.SatelliteContractVersionAttribute
extern TypeInfo SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo;
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SatelliteContractVersionAttribute_t1589_SatelliteContractVersionAttribute__ctor_m7289_ParameterInfos[] = 
{
	{"version", 0, 134221863, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern const MethodInfo SatelliteContractVersionAttribute__ctor_m7289_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SatelliteContractVersionAttribute__ctor_m7289/* method */
	, &SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SatelliteContractVersionAttribute_t1589_SatelliteContractVersionAttribute__ctor_m7289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SatelliteContractVersionAttribute_t1589_MethodInfos[] =
{
	&SatelliteContractVersionAttribute__ctor_m7289_MethodInfo,
	NULL
};
static const Il2CppMethodReference SatelliteContractVersionAttribute_t1589_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SatelliteContractVersionAttribute_t1589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SatelliteContractVersionAttribute_t1589_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SatelliteContractVersionAttribute_t1589_0_0_0;
extern const Il2CppType SatelliteContractVersionAttribute_t1589_1_0_0;
struct SatelliteContractVersionAttribute_t1589;
const Il2CppTypeDefinitionMetadata SatelliteContractVersionAttribute_t1589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SatelliteContractVersionAttribute_t1589_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, SatelliteContractVersionAttribute_t1589_VTable/* vtableMethods */
	, SatelliteContractVersionAttribute_t1589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1392/* fieldStart */

};
TypeInfo SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SatelliteContractVersionAttribute"/* name */
	, "System.Resources"/* namespaze */
	, SatelliteContractVersionAttribute_t1589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 441/* custom_attributes_cache */
	, &SatelliteContractVersionAttribute_t1589_0_0_0/* byval_arg */
	, &SatelliteContractVersionAttribute_t1589_1_0_0/* this_arg */
	, &SatelliteContractVersionAttribute_t1589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SatelliteContractVersionAttribute_t1589)/* instance_size */
	, sizeof (SatelliteContractVersionAttribute_t1589)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxations
extern TypeInfo CompilationRelaxations_t2274_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0MethodDeclarations.h"
static const MethodInfo* CompilationRelaxations_t2274_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CompilationRelaxations_t2274_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CompilationRelaxations_t2274_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CompilationRelaxations_t2274_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxations_t2274_0_0_0;
extern const Il2CppType CompilationRelaxations_t2274_1_0_0;
const Il2CppTypeDefinitionMetadata CompilationRelaxations_t2274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxations_t2274_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CompilationRelaxations_t2274_VTable/* vtableMethods */
	, CompilationRelaxations_t2274_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1393/* fieldStart */

};
TypeInfo CompilationRelaxations_t2274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxations"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, CompilationRelaxations_t2274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 442/* custom_attributes_cache */
	, &CompilationRelaxations_t2274_0_0_0/* byval_arg */
	, &CompilationRelaxations_t2274_1_0_0/* this_arg */
	, &CompilationRelaxations_t2274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxations_t2274)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CompilationRelaxations_t2274)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxationsAttribute
extern TypeInfo CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo CompilationRelaxationsAttribute_t904_CompilationRelaxationsAttribute__ctor_m4699_ParameterInfos[] = 
{
	{"relaxations", 0, 134221864, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
extern const MethodInfo CompilationRelaxationsAttribute__ctor_m4699_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompilationRelaxationsAttribute__ctor_m4699/* method */
	, &CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, CompilationRelaxationsAttribute_t904_CompilationRelaxationsAttribute__ctor_m4699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CompilationRelaxations_t2274_0_0_0;
static const ParameterInfo CompilationRelaxationsAttribute_t904_CompilationRelaxationsAttribute__ctor_m7291_ParameterInfos[] = 
{
	{"relaxations", 0, 134221865, 0, &CompilationRelaxations_t2274_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Runtime.CompilerServices.CompilationRelaxations)
extern const MethodInfo CompilationRelaxationsAttribute__ctor_m7291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompilationRelaxationsAttribute__ctor_m7291/* method */
	, &CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, CompilationRelaxationsAttribute_t904_CompilationRelaxationsAttribute__ctor_m7291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CompilationRelaxationsAttribute_t904_MethodInfos[] =
{
	&CompilationRelaxationsAttribute__ctor_m4699_MethodInfo,
	&CompilationRelaxationsAttribute__ctor_m7291_MethodInfo,
	NULL
};
static const Il2CppMethodReference CompilationRelaxationsAttribute_t904_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool CompilationRelaxationsAttribute_t904_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CompilationRelaxationsAttribute_t904_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxationsAttribute_t904_0_0_0;
extern const Il2CppType CompilationRelaxationsAttribute_t904_1_0_0;
struct CompilationRelaxationsAttribute_t904;
const Il2CppTypeDefinitionMetadata CompilationRelaxationsAttribute_t904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxationsAttribute_t904_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, CompilationRelaxationsAttribute_t904_VTable/* vtableMethods */
	, CompilationRelaxationsAttribute_t904_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1395/* fieldStart */

};
TypeInfo CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxationsAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, CompilationRelaxationsAttribute_t904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 443/* custom_attributes_cache */
	, &CompilationRelaxationsAttribute_t904_0_0_0/* byval_arg */
	, &CompilationRelaxationsAttribute_t904_1_0_0/* this_arg */
	, &CompilationRelaxationsAttribute_t904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxationsAttribute_t904)/* instance_size */
	, sizeof (CompilationRelaxationsAttribute_t904)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// Metadata Definition System.Runtime.CompilerServices.DefaultDependencyAttribute
extern TypeInfo DefaultDependencyAttribute_t2275_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
extern const Il2CppType LoadHint_t2277_0_0_0;
extern const Il2CppType LoadHint_t2277_0_0_0;
static const ParameterInfo DefaultDependencyAttribute_t2275_DefaultDependencyAttribute__ctor_m12075_ParameterInfos[] = 
{
	{"loadHintArgument", 0, 134221866, 0, &LoadHint_t2277_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.DefaultDependencyAttribute::.ctor(System.Runtime.CompilerServices.LoadHint)
extern const MethodInfo DefaultDependencyAttribute__ctor_m12075_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultDependencyAttribute__ctor_m12075/* method */
	, &DefaultDependencyAttribute_t2275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DefaultDependencyAttribute_t2275_DefaultDependencyAttribute__ctor_m12075_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultDependencyAttribute_t2275_MethodInfos[] =
{
	&DefaultDependencyAttribute__ctor_m12075_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultDependencyAttribute_t2275_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool DefaultDependencyAttribute_t2275_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultDependencyAttribute_t2275_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultDependencyAttribute_t2275_0_0_0;
extern const Il2CppType DefaultDependencyAttribute_t2275_1_0_0;
struct DefaultDependencyAttribute_t2275;
const Il2CppTypeDefinitionMetadata DefaultDependencyAttribute_t2275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultDependencyAttribute_t2275_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, DefaultDependencyAttribute_t2275_VTable/* vtableMethods */
	, DefaultDependencyAttribute_t2275_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1396/* fieldStart */

};
TypeInfo DefaultDependencyAttribute_t2275_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultDependencyAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, DefaultDependencyAttribute_t2275_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultDependencyAttribute_t2275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 444/* custom_attributes_cache */
	, &DefaultDependencyAttribute_t2275_0_0_0/* byval_arg */
	, &DefaultDependencyAttribute_t2275_1_0_0/* this_arg */
	, &DefaultDependencyAttribute_t2275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultDependencyAttribute_t2275)/* instance_size */
	, sizeof (DefaultDependencyAttribute_t2275)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile.h"
// Metadata Definition System.Runtime.CompilerServices.IsVolatile
extern TypeInfo IsVolatile_t2276_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatileMethodDeclarations.h"
static const MethodInfo* IsVolatile_t2276_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference IsVolatile_t2276_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool IsVolatile_t2276_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IsVolatile_t2276_0_0_0;
extern const Il2CppType IsVolatile_t2276_1_0_0;
struct IsVolatile_t2276;
const Il2CppTypeDefinitionMetadata IsVolatile_t2276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IsVolatile_t2276_VTable/* vtableMethods */
	, IsVolatile_t2276_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IsVolatile_t2276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IsVolatile"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, IsVolatile_t2276_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IsVolatile_t2276_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 445/* custom_attributes_cache */
	, &IsVolatile_t2276_0_0_0/* byval_arg */
	, &IsVolatile_t2276_1_0_0/* this_arg */
	, &IsVolatile_t2276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IsVolatile_t2276)/* instance_size */
	, sizeof (IsVolatile_t2276)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"
// Metadata Definition System.Runtime.CompilerServices.LoadHint
extern TypeInfo LoadHint_t2277_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHintMethodDeclarations.h"
static const MethodInfo* LoadHint_t2277_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LoadHint_t2277_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool LoadHint_t2277_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LoadHint_t2277_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoadHint_t2277_1_0_0;
const Il2CppTypeDefinitionMetadata LoadHint_t2277_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoadHint_t2277_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, LoadHint_t2277_VTable/* vtableMethods */
	, LoadHint_t2277_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1397/* fieldStart */

};
TypeInfo LoadHint_t2277_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoadHint"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, LoadHint_t2277_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoadHint_t2277_0_0_0/* byval_arg */
	, &LoadHint_t2277_1_0_0/* this_arg */
	, &LoadHint_t2277_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoadHint_t2277)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoadHint_t2277)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// Metadata Definition System.Runtime.CompilerServices.StringFreezingAttribute
extern TypeInfo StringFreezingAttribute_t2278_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern const MethodInfo StringFreezingAttribute__ctor_m12076_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringFreezingAttribute__ctor_m12076/* method */
	, &StringFreezingAttribute_t2278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringFreezingAttribute_t2278_MethodInfos[] =
{
	&StringFreezingAttribute__ctor_m12076_MethodInfo,
	NULL
};
static const Il2CppMethodReference StringFreezingAttribute_t2278_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool StringFreezingAttribute_t2278_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringFreezingAttribute_t2278_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringFreezingAttribute_t2278_0_0_0;
extern const Il2CppType StringFreezingAttribute_t2278_1_0_0;
struct StringFreezingAttribute_t2278;
const Il2CppTypeDefinitionMetadata StringFreezingAttribute_t2278_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringFreezingAttribute_t2278_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, StringFreezingAttribute_t2278_VTable/* vtableMethods */
	, StringFreezingAttribute_t2278_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StringFreezingAttribute_t2278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringFreezingAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, StringFreezingAttribute_t2278_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringFreezingAttribute_t2278_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 446/* custom_attributes_cache */
	, &StringFreezingAttribute_t2278_0_0_0/* byval_arg */
	, &StringFreezingAttribute_t2278_1_0_0/* this_arg */
	, &StringFreezingAttribute_t2278_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringFreezingAttribute_t2278)/* instance_size */
	, sizeof (StringFreezingAttribute_t2278)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Cer
extern TypeInfo Cer_t2279_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_CerMethodDeclarations.h"
static const MethodInfo* Cer_t2279_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Cer_t2279_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Cer_t2279_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Cer_t2279_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Cer_t2279_0_0_0;
extern const Il2CppType Cer_t2279_1_0_0;
const Il2CppTypeDefinitionMetadata Cer_t2279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Cer_t2279_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Cer_t2279_VTable/* vtableMethods */
	, Cer_t2279_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1401/* fieldStart */

};
TypeInfo Cer_t2279_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Cer"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, Cer_t2279_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Cer_t2279_0_0_0/* byval_arg */
	, &Cer_t2279_1_0_0/* this_arg */
	, &Cer_t2279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Cer_t2279)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Cer_t2279)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Consistency
extern TypeInfo Consistency_t2280_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_ConsistencyMethodDeclarations.h"
static const MethodInfo* Consistency_t2280_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Consistency_t2280_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Consistency_t2280_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Consistency_t2280_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Consistency_t2280_0_0_0;
extern const Il2CppType Consistency_t2280_1_0_0;
const Il2CppTypeDefinitionMetadata Consistency_t2280_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Consistency_t2280_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Consistency_t2280_VTable/* vtableMethods */
	, Consistency_t2280_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1405/* fieldStart */

};
TypeInfo Consistency_t2280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Consistency"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, Consistency_t2280_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Consistency_t2280_0_0_0/* byval_arg */
	, &Consistency_t2280_1_0_0/* this_arg */
	, &Consistency_t2280_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Consistency_t2280)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Consistency_t2280)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
// Metadata Definition System.Runtime.ConstrainedExecution.CriticalFinalizerObject
extern TypeInfo CriticalFinalizerObject_t2281_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinalizMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
extern const MethodInfo CriticalFinalizerObject__ctor_m12077_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CriticalFinalizerObject__ctor_m12077/* method */
	, &CriticalFinalizerObject_t2281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 448/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
extern const MethodInfo CriticalFinalizerObject_Finalize_m12078_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&CriticalFinalizerObject_Finalize_m12078/* method */
	, &CriticalFinalizerObject_t2281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 449/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CriticalFinalizerObject_t2281_MethodInfos[] =
{
	&CriticalFinalizerObject__ctor_m12077_MethodInfo,
	&CriticalFinalizerObject_Finalize_m12078_MethodInfo,
	NULL
};
extern const MethodInfo CriticalFinalizerObject_Finalize_m12078_MethodInfo;
static const Il2CppMethodReference CriticalFinalizerObject_t2281_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&CriticalFinalizerObject_Finalize_m12078_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool CriticalFinalizerObject_t2281_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CriticalFinalizerObject_t2281_0_0_0;
extern const Il2CppType CriticalFinalizerObject_t2281_1_0_0;
struct CriticalFinalizerObject_t2281;
const Il2CppTypeDefinitionMetadata CriticalFinalizerObject_t2281_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CriticalFinalizerObject_t2281_VTable/* vtableMethods */
	, CriticalFinalizerObject_t2281_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CriticalFinalizerObject_t2281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CriticalFinalizerObject"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, CriticalFinalizerObject_t2281_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CriticalFinalizerObject_t2281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 447/* custom_attributes_cache */
	, &CriticalFinalizerObject_t2281_0_0_0/* byval_arg */
	, &CriticalFinalizerObject_t2281_1_0_0/* this_arg */
	, &CriticalFinalizerObject_t2281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CriticalFinalizerObject_t2281)/* instance_size */
	, sizeof (CriticalFinalizerObject_t2281)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// Metadata Definition System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
extern TypeInfo ReliabilityContractAttribute_t2282_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern const Il2CppType Consistency_t2280_0_0_0;
extern const Il2CppType Cer_t2279_0_0_0;
static const ParameterInfo ReliabilityContractAttribute_t2282_ReliabilityContractAttribute__ctor_m12079_ParameterInfos[] = 
{
	{"consistencyGuarantee", 0, 134221867, 0, &Consistency_t2280_0_0_0},
	{"cer", 1, 134221868, 0, &Cer_t2279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern const MethodInfo ReliabilityContractAttribute__ctor_m12079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReliabilityContractAttribute__ctor_m12079/* method */
	, &ReliabilityContractAttribute_t2282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, ReliabilityContractAttribute_t2282_ReliabilityContractAttribute__ctor_m12079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReliabilityContractAttribute_t2282_MethodInfos[] =
{
	&ReliabilityContractAttribute__ctor_m12079_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReliabilityContractAttribute_t2282_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ReliabilityContractAttribute_t2282_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReliabilityContractAttribute_t2282_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReliabilityContractAttribute_t2282_0_0_0;
extern const Il2CppType ReliabilityContractAttribute_t2282_1_0_0;
struct ReliabilityContractAttribute_t2282;
const Il2CppTypeDefinitionMetadata ReliabilityContractAttribute_t2282_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReliabilityContractAttribute_t2282_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ReliabilityContractAttribute_t2282_VTable/* vtableMethods */
	, ReliabilityContractAttribute_t2282_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1410/* fieldStart */

};
TypeInfo ReliabilityContractAttribute_t2282_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReliabilityContractAttribute"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, ReliabilityContractAttribute_t2282_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReliabilityContractAttribute_t2282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 450/* custom_attributes_cache */
	, &ReliabilityContractAttribute_t2282_0_0_0/* byval_arg */
	, &ReliabilityContractAttribute_t2282_1_0_0/* this_arg */
	, &ReliabilityContractAttribute_t2282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReliabilityContractAttribute_t2282)/* instance_size */
	, sizeof (ReliabilityContractAttribute_t2282)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArguments.h"
// Metadata Definition System.Runtime.Hosting.ActivationArguments
extern TypeInfo ActivationArguments_t2283_il2cpp_TypeInfo;
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArgumentsMethodDeclarations.h"
static const MethodInfo* ActivationArguments_t2283_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ActivationArguments_t2283_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ActivationArguments_t2283_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationArguments_t2283_0_0_0;
extern const Il2CppType ActivationArguments_t2283_1_0_0;
struct ActivationArguments_t2283;
const Il2CppTypeDefinitionMetadata ActivationArguments_t2283_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationArguments_t2283_VTable/* vtableMethods */
	, ActivationArguments_t2283_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ActivationArguments_t2283_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationArguments"/* name */
	, "System.Runtime.Hosting"/* namespaze */
	, ActivationArguments_t2283_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ActivationArguments_t2283_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 451/* custom_attributes_cache */
	, &ActivationArguments_t2283_0_0_0/* byval_arg */
	, &ActivationArguments_t2283_1_0_0/* this_arg */
	, &ActivationArguments_t2283_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationArguments_t2283)/* instance_size */
	, sizeof (ActivationArguments_t2283)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
// Metadata Definition System.Runtime.InteropServices.CallingConvention
extern TypeInfo CallingConvention_t2284_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConventionMethodDeclarations.h"
static const MethodInfo* CallingConvention_t2284_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CallingConvention_t2284_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CallingConvention_t2284_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CallingConvention_t2284_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConvention_t2284_0_0_0;
extern const Il2CppType CallingConvention_t2284_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConvention_t2284_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConvention_t2284_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CallingConvention_t2284_VTable/* vtableMethods */
	, CallingConvention_t2284_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1412/* fieldStart */

};
TypeInfo CallingConvention_t2284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConvention"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, CallingConvention_t2284_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 452/* custom_attributes_cache */
	, &CallingConvention_t2284_0_0_0/* byval_arg */
	, &CallingConvention_t2284_1_0_0/* this_arg */
	, &CallingConvention_t2284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConvention_t2284)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConvention_t2284)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
// Metadata Definition System.Runtime.InteropServices.CharSet
extern TypeInfo CharSet_t2285_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSetMethodDeclarations.h"
static const MethodInfo* CharSet_t2285_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CharSet_t2285_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CharSet_t2285_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CharSet_t2285_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CharSet_t2285_0_0_0;
extern const Il2CppType CharSet_t2285_1_0_0;
const Il2CppTypeDefinitionMetadata CharSet_t2285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CharSet_t2285_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CharSet_t2285_VTable/* vtableMethods */
	, CharSet_t2285_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1418/* fieldStart */

};
TypeInfo CharSet_t2285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharSet"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, CharSet_t2285_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 453/* custom_attributes_cache */
	, &CharSet_t2285_0_0_0/* byval_arg */
	, &CharSet_t2285_1_0_0/* this_arg */
	, &CharSet_t2285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharSet_t2285)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharSet_t2285)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceAttribute
extern TypeInfo ClassInterfaceAttribute_t2286_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern const Il2CppType ClassInterfaceType_t2287_0_0_0;
extern const Il2CppType ClassInterfaceType_t2287_0_0_0;
static const ParameterInfo ClassInterfaceAttribute_t2286_ClassInterfaceAttribute__ctor_m12080_ParameterInfos[] = 
{
	{"classInterfaceType", 0, 134221869, 0, &ClassInterfaceType_t2287_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern const MethodInfo ClassInterfaceAttribute__ctor_m12080_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClassInterfaceAttribute__ctor_m12080/* method */
	, &ClassInterfaceAttribute_t2286_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ClassInterfaceAttribute_t2286_ClassInterfaceAttribute__ctor_m12080_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClassInterfaceAttribute_t2286_MethodInfos[] =
{
	&ClassInterfaceAttribute__ctor_m12080_MethodInfo,
	NULL
};
static const Il2CppMethodReference ClassInterfaceAttribute_t2286_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ClassInterfaceAttribute_t2286_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ClassInterfaceAttribute_t2286_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceAttribute_t2286_0_0_0;
extern const Il2CppType ClassInterfaceAttribute_t2286_1_0_0;
struct ClassInterfaceAttribute_t2286;
const Il2CppTypeDefinitionMetadata ClassInterfaceAttribute_t2286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceAttribute_t2286_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ClassInterfaceAttribute_t2286_VTable/* vtableMethods */
	, ClassInterfaceAttribute_t2286_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1423/* fieldStart */

};
TypeInfo ClassInterfaceAttribute_t2286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ClassInterfaceAttribute_t2286_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ClassInterfaceAttribute_t2286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 454/* custom_attributes_cache */
	, &ClassInterfaceAttribute_t2286_0_0_0/* byval_arg */
	, &ClassInterfaceAttribute_t2286_1_0_0/* this_arg */
	, &ClassInterfaceAttribute_t2286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceAttribute_t2286)/* instance_size */
	, sizeof (ClassInterfaceAttribute_t2286)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceType
extern TypeInfo ClassInterfaceType_t2287_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceTypeMethodDeclarations.h"
static const MethodInfo* ClassInterfaceType_t2287_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ClassInterfaceType_t2287_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ClassInterfaceType_t2287_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ClassInterfaceType_t2287_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceType_t2287_1_0_0;
const Il2CppTypeDefinitionMetadata ClassInterfaceType_t2287_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceType_t2287_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ClassInterfaceType_t2287_VTable/* vtableMethods */
	, ClassInterfaceType_t2287_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1424/* fieldStart */

};
TypeInfo ClassInterfaceType_t2287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ClassInterfaceType_t2287_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 455/* custom_attributes_cache */
	, &ClassInterfaceType_t2287_0_0_0/* byval_arg */
	, &ClassInterfaceType_t2287_1_0_0/* this_arg */
	, &ClassInterfaceType_t2287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceType_t2287)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ClassInterfaceType_t2287)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// Metadata Definition System.Runtime.InteropServices.ComDefaultInterfaceAttribute
extern TypeInfo ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ComDefaultInterfaceAttribute_t2288_ComDefaultInterfaceAttribute__ctor_m12081_ParameterInfos[] = 
{
	{"defaultInterface", 0, 134221870, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern const MethodInfo ComDefaultInterfaceAttribute__ctor_m12081_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ComDefaultInterfaceAttribute__ctor_m12081/* method */
	, &ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ComDefaultInterfaceAttribute_t2288_ComDefaultInterfaceAttribute__ctor_m12081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComDefaultInterfaceAttribute_t2288_MethodInfos[] =
{
	&ComDefaultInterfaceAttribute__ctor_m12081_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComDefaultInterfaceAttribute_t2288_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ComDefaultInterfaceAttribute_t2288_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ComDefaultInterfaceAttribute_t2288_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComDefaultInterfaceAttribute_t2288_0_0_0;
extern const Il2CppType ComDefaultInterfaceAttribute_t2288_1_0_0;
struct ComDefaultInterfaceAttribute_t2288;
const Il2CppTypeDefinitionMetadata ComDefaultInterfaceAttribute_t2288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComDefaultInterfaceAttribute_t2288_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ComDefaultInterfaceAttribute_t2288_VTable/* vtableMethods */
	, ComDefaultInterfaceAttribute_t2288_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1428/* fieldStart */

};
TypeInfo ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComDefaultInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ComDefaultInterfaceAttribute_t2288_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 456/* custom_attributes_cache */
	, &ComDefaultInterfaceAttribute_t2288_0_0_0/* byval_arg */
	, &ComDefaultInterfaceAttribute_t2288_1_0_0/* this_arg */
	, &ComDefaultInterfaceAttribute_t2288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComDefaultInterfaceAttribute_t2288)/* instance_size */
	, sizeof (ComDefaultInterfaceAttribute_t2288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ComInterfaceType
extern TypeInfo ComInterfaceType_t2289_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceTypeMethodDeclarations.h"
static const MethodInfo* ComInterfaceType_t2289_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ComInterfaceType_t2289_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ComInterfaceType_t2289_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ComInterfaceType_t2289_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComInterfaceType_t2289_0_0_0;
extern const Il2CppType ComInterfaceType_t2289_1_0_0;
const Il2CppTypeDefinitionMetadata ComInterfaceType_t2289_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComInterfaceType_t2289_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ComInterfaceType_t2289_VTable/* vtableMethods */
	, ComInterfaceType_t2289_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1429/* fieldStart */

};
TypeInfo ComInterfaceType_t2289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ComInterfaceType_t2289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 457/* custom_attributes_cache */
	, &ComInterfaceType_t2289_0_0_0/* byval_arg */
	, &ComInterfaceType_t2289_1_0_0/* this_arg */
	, &ComInterfaceType_t2289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComInterfaceType_t2289)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ComInterfaceType_t2289)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// Metadata Definition System.Runtime.InteropServices.DispIdAttribute
extern TypeInfo DispIdAttribute_t2290_il2cpp_TypeInfo;
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo DispIdAttribute_t2290_DispIdAttribute__ctor_m12082_ParameterInfos[] = 
{
	{"dispId", 0, 134221871, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern const MethodInfo DispIdAttribute__ctor_m12082_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DispIdAttribute__ctor_m12082/* method */
	, &DispIdAttribute_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DispIdAttribute_t2290_DispIdAttribute__ctor_m12082_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DispIdAttribute_t2290_MethodInfos[] =
{
	&DispIdAttribute__ctor_m12082_MethodInfo,
	NULL
};
static const Il2CppMethodReference DispIdAttribute_t2290_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool DispIdAttribute_t2290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DispIdAttribute_t2290_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DispIdAttribute_t2290_0_0_0;
extern const Il2CppType DispIdAttribute_t2290_1_0_0;
struct DispIdAttribute_t2290;
const Il2CppTypeDefinitionMetadata DispIdAttribute_t2290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DispIdAttribute_t2290_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, DispIdAttribute_t2290_VTable/* vtableMethods */
	, DispIdAttribute_t2290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1433/* fieldStart */

};
TypeInfo DispIdAttribute_t2290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DispIdAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, DispIdAttribute_t2290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DispIdAttribute_t2290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 458/* custom_attributes_cache */
	, &DispIdAttribute_t2290_0_0_0/* byval_arg */
	, &DispIdAttribute_t2290_1_0_0/* this_arg */
	, &DispIdAttribute_t2290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DispIdAttribute_t2290)/* instance_size */
	, sizeof (DispIdAttribute_t2290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// Metadata Definition System.Runtime.InteropServices.GCHandle
extern TypeInfo GCHandle_t811_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t2291_0_0_0;
extern const Il2CppType GCHandleType_t2291_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle__ctor_m12083_ParameterInfos[] = 
{
	{"value", 0, 134221872, 0, &Object_t_0_0_0},
	{"type", 1, 134221873, 0, &GCHandleType_t2291_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle__ctor_m12083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GCHandle__ctor_m12083/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle__ctor_m12083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern const MethodInfo GCHandle_get_IsAllocated_m12084_MethodInfo = 
{
	"get_IsAllocated"/* name */
	, (methodPointerType)&GCHandle_get_IsAllocated_m12084/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern const MethodInfo GCHandle_get_Target_m12085_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&GCHandle_get_Target_m12085/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.GCHandle::AddrOfPinnedObject()
extern const MethodInfo GCHandle_AddrOfPinnedObject_m4394_MethodInfo = 
{
	"AddrOfPinnedObject"/* name */
	, (methodPointerType)&GCHandle_AddrOfPinnedObject_m4394/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t2291_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_Alloc_m4393_ParameterInfos[] = 
{
	{"value", 0, 134221874, 0, &Object_t_0_0_0},
	{"type", 1, 134221875, 0, &GCHandleType_t2291_0_0_0},
};
extern const Il2CppType GCHandle_t811_0_0_0;
extern void* RuntimeInvoker_GCHandle_t811_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_Alloc_m4393_MethodInfo = 
{
	"Alloc"/* name */
	, (methodPointerType)&GCHandle_Alloc_m4393/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &GCHandle_t811_0_0_0/* return_type */
	, RuntimeInvoker_GCHandle_t811_Object_t_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle_Alloc_m4393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern const MethodInfo GCHandle_Free_m4396_MethodInfo = 
{
	"Free"/* name */
	, (methodPointerType)&GCHandle_Free_m4396/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_GetTarget_m12086_ParameterInfos[] = 
{
	{"handle", 0, 134221876, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)
extern const MethodInfo GCHandle_GetTarget_m12086_MethodInfo = 
{
	"GetTarget"/* name */
	, (methodPointerType)&GCHandle_GetTarget_m12086/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle_GetTarget_m12086_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType GCHandleType_t2291_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_GetTargetHandle_m12087_ParameterInfos[] = 
{
	{"obj", 0, 134221877, 0, &Object_t_0_0_0},
	{"handle", 1, 134221878, 0, &Int32_t135_0_0_0},
	{"type", 2, 134221879, 0, &GCHandleType_t2291_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_GetTargetHandle_m12087_MethodInfo = 
{
	"GetTargetHandle"/* name */
	, (methodPointerType)&GCHandle_GetTargetHandle_m12087/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle_GetTargetHandle_m12087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_FreeHandle_m12088_ParameterInfos[] = 
{
	{"handle", 0, 134221880, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)
extern const MethodInfo GCHandle_FreeHandle_m12088_MethodInfo = 
{
	"FreeHandle"/* name */
	, (methodPointerType)&GCHandle_FreeHandle_m12088/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle_FreeHandle_m12088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_GetAddrOfPinnedObject_m12089_ParameterInfos[] = 
{
	{"handle", 0, 134221881, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.GCHandle::GetAddrOfPinnedObject(System.Int32)
extern const MethodInfo GCHandle_GetAddrOfPinnedObject_m12089_MethodInfo = 
{
	"GetAddrOfPinnedObject"/* name */
	, (methodPointerType)&GCHandle_GetAddrOfPinnedObject_m12089/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t_Int32_t135/* invoker_method */
	, GCHandle_t811_GCHandle_GetAddrOfPinnedObject_m12089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GCHandle_t811_GCHandle_Equals_m12090_ParameterInfos[] = 
{
	{"o", 0, 134221882, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::Equals(System.Object)
extern const MethodInfo GCHandle_Equals_m12090_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&GCHandle_Equals_m12090/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, GCHandle_t811_GCHandle_Equals_m12090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetHashCode()
extern const MethodInfo GCHandle_GetHashCode_m12091_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&GCHandle_GetHashCode_m12091/* method */
	, &GCHandle_t811_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GCHandle_t811_MethodInfos[] =
{
	&GCHandle__ctor_m12083_MethodInfo,
	&GCHandle_get_IsAllocated_m12084_MethodInfo,
	&GCHandle_get_Target_m12085_MethodInfo,
	&GCHandle_AddrOfPinnedObject_m4394_MethodInfo,
	&GCHandle_Alloc_m4393_MethodInfo,
	&GCHandle_Free_m4396_MethodInfo,
	&GCHandle_GetTarget_m12086_MethodInfo,
	&GCHandle_GetTargetHandle_m12087_MethodInfo,
	&GCHandle_FreeHandle_m12088_MethodInfo,
	&GCHandle_GetAddrOfPinnedObject_m12089_MethodInfo,
	&GCHandle_Equals_m12090_MethodInfo,
	&GCHandle_GetHashCode_m12091_MethodInfo,
	NULL
};
extern const MethodInfo GCHandle_get_IsAllocated_m12084_MethodInfo;
static const PropertyInfo GCHandle_t811____IsAllocated_PropertyInfo = 
{
	&GCHandle_t811_il2cpp_TypeInfo/* parent */
	, "IsAllocated"/* name */
	, &GCHandle_get_IsAllocated_m12084_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GCHandle_get_Target_m12085_MethodInfo;
static const PropertyInfo GCHandle_t811____Target_PropertyInfo = 
{
	&GCHandle_t811_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &GCHandle_get_Target_m12085_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GCHandle_t811_PropertyInfos[] =
{
	&GCHandle_t811____IsAllocated_PropertyInfo,
	&GCHandle_t811____Target_PropertyInfo,
	NULL
};
extern const MethodInfo GCHandle_Equals_m12090_MethodInfo;
extern const MethodInfo GCHandle_GetHashCode_m12091_MethodInfo;
static const Il2CppMethodReference GCHandle_t811_VTable[] =
{
	&GCHandle_Equals_m12090_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&GCHandle_GetHashCode_m12091_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool GCHandle_t811_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandle_t811_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandle_t811_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, GCHandle_t811_VTable/* vtableMethods */
	, GCHandle_t811_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1434/* fieldStart */

};
TypeInfo GCHandle_t811_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandle_t811_MethodInfos/* methods */
	, GCHandle_t811_PropertyInfos/* properties */
	, NULL/* events */
	, &GCHandle_t811_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 459/* custom_attributes_cache */
	, &GCHandle_t811_0_0_0/* byval_arg */
	, &GCHandle_t811_1_0_0/* this_arg */
	, &GCHandle_t811_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandle_t811)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandle_t811)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GCHandle_t811 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 12/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// Metadata Definition System.Runtime.InteropServices.GCHandleType
extern TypeInfo GCHandleType_t2291_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleTypeMethodDeclarations.h"
static const MethodInfo* GCHandleType_t2291_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference GCHandleType_t2291_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool GCHandleType_t2291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair GCHandleType_t2291_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandleType_t2291_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandleType_t2291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GCHandleType_t2291_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, GCHandleType_t2291_VTable/* vtableMethods */
	, GCHandleType_t2291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1435/* fieldStart */

};
TypeInfo GCHandleType_t2291_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandleType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandleType_t2291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 460/* custom_attributes_cache */
	, &GCHandleType_t2291_0_0_0/* byval_arg */
	, &GCHandleType_t2291_1_0_0/* this_arg */
	, &GCHandleType_t2291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandleType_t2291)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandleType_t2291)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute
extern TypeInfo InterfaceTypeAttribute_t2292_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
extern const Il2CppType ComInterfaceType_t2289_0_0_0;
static const ParameterInfo InterfaceTypeAttribute_t2292_InterfaceTypeAttribute__ctor_m12092_ParameterInfos[] = 
{
	{"interfaceType", 0, 134221883, 0, &ComInterfaceType_t2289_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern const MethodInfo InterfaceTypeAttribute__ctor_m12092_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterfaceTypeAttribute__ctor_m12092/* method */
	, &InterfaceTypeAttribute_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, InterfaceTypeAttribute_t2292_InterfaceTypeAttribute__ctor_m12092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterfaceTypeAttribute_t2292_MethodInfos[] =
{
	&InterfaceTypeAttribute__ctor_m12092_MethodInfo,
	NULL
};
static const Il2CppMethodReference InterfaceTypeAttribute_t2292_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool InterfaceTypeAttribute_t2292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InterfaceTypeAttribute_t2292_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InterfaceTypeAttribute_t2292_0_0_0;
extern const Il2CppType InterfaceTypeAttribute_t2292_1_0_0;
struct InterfaceTypeAttribute_t2292;
const Il2CppTypeDefinitionMetadata InterfaceTypeAttribute_t2292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InterfaceTypeAttribute_t2292_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, InterfaceTypeAttribute_t2292_VTable/* vtableMethods */
	, InterfaceTypeAttribute_t2292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1440/* fieldStart */

};
TypeInfo InterfaceTypeAttribute_t2292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterfaceTypeAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, InterfaceTypeAttribute_t2292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InterfaceTypeAttribute_t2292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 461/* custom_attributes_cache */
	, &InterfaceTypeAttribute_t2292_0_0_0/* byval_arg */
	, &InterfaceTypeAttribute_t2292_1_0_0/* this_arg */
	, &InterfaceTypeAttribute_t2292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterfaceTypeAttribute_t2292)/* instance_size */
	, sizeof (InterfaceTypeAttribute_t2292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
// Metadata Definition System.Runtime.InteropServices.Marshal
extern TypeInfo Marshal_t798_il2cpp_TypeInfo;
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern const MethodInfo Marshal__cctor_m12093_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Marshal__cctor_m12093/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_AllocHGlobal_m12094_ParameterInfos[] = 
{
	{"cb", 0, 134221884, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.IntPtr)
extern const MethodInfo Marshal_AllocHGlobal_m12094_MethodInfo = 
{
	"AllocHGlobal"/* name */
	, (methodPointerType)&Marshal_AllocHGlobal_m12094/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t_IntPtr_t/* invoker_method */
	, Marshal_t798_Marshal_AllocHGlobal_m12094_ParameterInfos/* parameters */
	, 463/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_AllocHGlobal_m4294_ParameterInfos[] = 
{
	{"cb", 0, 134221885, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.Int32)
extern const MethodInfo Marshal_AllocHGlobal_m4294_MethodInfo = 
{
	"AllocHGlobal"/* name */
	, (methodPointerType)&Marshal_AllocHGlobal_m4294/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_AllocHGlobal_m4294_ParameterInfos/* parameters */
	, 464/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_copy_to_unmanaged_m12095_ParameterInfos[] = 
{
	{"source", 0, 134221886, 0, &Array_t_0_0_0},
	{"startIndex", 1, 134221887, 0, &Int32_t135_0_0_0},
	{"destination", 2, 134221888, 0, &IntPtr_t_0_0_0},
	{"length", 3, 134221889, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::copy_to_unmanaged(System.Array,System.Int32,System.IntPtr,System.Int32)
extern const MethodInfo Marshal_copy_to_unmanaged_m12095_MethodInfo = 
{
	"copy_to_unmanaged"/* name */
	, (methodPointerType)&Marshal_copy_to_unmanaged_m12095/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_IntPtr_t_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_copy_to_unmanaged_m12095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_copy_from_unmanaged_m12096_ParameterInfos[] = 
{
	{"source", 0, 134221890, 0, &IntPtr_t_0_0_0},
	{"startIndex", 1, 134221891, 0, &Int32_t135_0_0_0},
	{"destination", 2, 134221892, 0, &Array_t_0_0_0},
	{"length", 3, 134221893, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_Int32_t135_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern const MethodInfo Marshal_copy_from_unmanaged_m12096_MethodInfo = 
{
	"copy_from_unmanaged"/* name */
	, (methodPointerType)&Marshal_copy_from_unmanaged_m12096/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_Int32_t135_Object_t_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_copy_from_unmanaged_m12096_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_Copy_m4395_ParameterInfos[] = 
{
	{"source", 0, 134221894, 0, &ByteU5BU5D_t622_0_0_0},
	{"startIndex", 1, 134221895, 0, &Int32_t135_0_0_0},
	{"destination", 2, 134221896, 0, &IntPtr_t_0_0_0},
	{"length", 3, 134221897, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Byte[],System.Int32,System.IntPtr,System.Int32)
extern const MethodInfo Marshal_Copy_m4395_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m4395/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_IntPtr_t_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_Copy_m4395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_Copy_m4392_ParameterInfos[] = 
{
	{"source", 0, 134221898, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134221899, 0, &ByteU5BU5D_t622_0_0_0},
	{"startIndex", 2, 134221900, 0, &Int32_t135_0_0_0},
	{"length", 3, 134221901, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m4392_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m4392/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_Copy_m4392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType CharU5BU5D_t119_0_0_0;
extern const Il2CppType CharU5BU5D_t119_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_Copy_m12097_ParameterInfos[] = 
{
	{"source", 0, 134221902, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134221903, 0, &CharU5BU5D_t119_0_0_0},
	{"startIndex", 2, 134221904, 0, &Int32_t135_0_0_0},
	{"length", 3, 134221905, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m12097_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m12097/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_Copy_m12097_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType SingleU5BU5D_t591_0_0_0;
extern const Il2CppType SingleU5BU5D_t591_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_Copy_m4295_ParameterInfos[] = 
{
	{"source", 0, 134221906, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134221907, 0, &SingleU5BU5D_t591_0_0_0},
	{"startIndex", 2, 134221908, 0, &Int32_t135_0_0_0},
	{"length", 3, 134221909, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m4295_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m4295/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_Copy_m4295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_FreeHGlobal_m4298_ParameterInfos[] = 
{
	{"hglobal", 0, 134221910, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::FreeHGlobal(System.IntPtr)
extern const MethodInfo Marshal_FreeHGlobal_m4298_MethodInfo = 
{
	"FreeHGlobal"/* name */
	, (methodPointerType)&Marshal_FreeHGlobal_m4298/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t/* invoker_method */
	, Marshal_t798_Marshal_FreeHGlobal_m4298_ParameterInfos/* parameters */
	, 465/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_PtrToStringAnsi_m4597_ParameterInfos[] = 
{
	{"ptr", 0, 134221911, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr)
extern const MethodInfo Marshal_PtrToStringAnsi_m4597_MethodInfo = 
{
	"PtrToStringAnsi"/* name */
	, (methodPointerType)&Marshal_PtrToStringAnsi_m4597/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, Marshal_t798_Marshal_PtrToStringAnsi_m4597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_PtrToStringUni_m4300_ParameterInfos[] = 
{
	{"ptr", 0, 134221912, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr)
extern const MethodInfo Marshal_PtrToStringUni_m4300_MethodInfo = 
{
	"PtrToStringUni"/* name */
	, (methodPointerType)&Marshal_PtrToStringUni_m4300/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, Marshal_t798_Marshal_PtrToStringUni_m4300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_PtrToStructure_m4353_ParameterInfos[] = 
{
	{"ptr", 0, 134221913, 0, &IntPtr_t_0_0_0},
	{"structureType", 1, 134221914, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Type)
extern const MethodInfo Marshal_PtrToStructure_m4353_MethodInfo = 
{
	"PtrToStructure"/* name */
	, (methodPointerType)&Marshal_PtrToStructure_m4353/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_Object_t/* invoker_method */
	, Marshal_t798_Marshal_PtrToStructure_m4353_ParameterInfos/* parameters */
	, 466/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_ReadInt32_m4598_ParameterInfos[] = 
{
	{"ptr", 0, 134221915, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr)
extern const MethodInfo Marshal_ReadInt32_m4598_MethodInfo = 
{
	"ReadInt32"/* name */
	, (methodPointerType)&Marshal_ReadInt32_m4598/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_IntPtr_t/* invoker_method */
	, Marshal_t798_Marshal_ReadInt32_m4598_ParameterInfos/* parameters */
	, 467/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_ReadInt32_m12098_ParameterInfos[] = 
{
	{"ptr", 0, 134221916, 0, &IntPtr_t_0_0_0},
	{"ofs", 1, 134221917, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr,System.Int32)
extern const MethodInfo Marshal_ReadInt32_m12098_MethodInfo = 
{
	"ReadInt32"/* name */
	, (methodPointerType)&Marshal_ReadInt32_m12098/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_IntPtr_t_Int32_t135/* invoker_method */
	, Marshal_t798_Marshal_ReadInt32_m12098_ParameterInfos/* parameters */
	, 468/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_SizeOf_m4293_ParameterInfos[] = 
{
	{"t", 0, 134221918, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
extern const MethodInfo Marshal_SizeOf_m4293_MethodInfo = 
{
	"SizeOf"/* name */
	, (methodPointerType)&Marshal_SizeOf_m4293/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Marshal_t798_Marshal_SizeOf_m4293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_StringToHGlobalUni_m4301_ParameterInfos[] = 
{
	{"s", 0, 134221919, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalUni(System.String)
extern const MethodInfo Marshal_StringToHGlobalUni_m4301_MethodInfo = 
{
	"StringToHGlobalUni"/* name */
	, (methodPointerType)&Marshal_StringToHGlobalUni_m4301/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t_Object_t/* invoker_method */
	, Marshal_t798_Marshal_StringToHGlobalUni_m4301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Marshal_t798_Marshal_StructureToPtr_m4316_ParameterInfos[] = 
{
	{"structure", 0, 134221920, 0, &Object_t_0_0_0},
	{"ptr", 1, 134221921, 0, &IntPtr_t_0_0_0},
	{"fDeleteOld", 2, 134221922, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::StructureToPtr(System.Object,System.IntPtr,System.Boolean)
extern const MethodInfo Marshal_StructureToPtr_m4316_MethodInfo = 
{
	"StructureToPtr"/* name */
	, (methodPointerType)&Marshal_StructureToPtr_m4316/* method */
	, &Marshal_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t_SByte_t177/* invoker_method */
	, Marshal_t798_Marshal_StructureToPtr_m4316_ParameterInfos/* parameters */
	, 469/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Marshal_t798_MethodInfos[] =
{
	&Marshal__cctor_m12093_MethodInfo,
	&Marshal_AllocHGlobal_m12094_MethodInfo,
	&Marshal_AllocHGlobal_m4294_MethodInfo,
	&Marshal_copy_to_unmanaged_m12095_MethodInfo,
	&Marshal_copy_from_unmanaged_m12096_MethodInfo,
	&Marshal_Copy_m4395_MethodInfo,
	&Marshal_Copy_m4392_MethodInfo,
	&Marshal_Copy_m12097_MethodInfo,
	&Marshal_Copy_m4295_MethodInfo,
	&Marshal_FreeHGlobal_m4298_MethodInfo,
	&Marshal_PtrToStringAnsi_m4597_MethodInfo,
	&Marshal_PtrToStringUni_m4300_MethodInfo,
	&Marshal_PtrToStructure_m4353_MethodInfo,
	&Marshal_ReadInt32_m4598_MethodInfo,
	&Marshal_ReadInt32_m12098_MethodInfo,
	&Marshal_SizeOf_m4293_MethodInfo,
	&Marshal_StringToHGlobalUni_m4301_MethodInfo,
	&Marshal_StructureToPtr_m4316_MethodInfo,
	NULL
};
static const Il2CppMethodReference Marshal_t798_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Marshal_t798_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Marshal_t798_0_0_0;
extern const Il2CppType Marshal_t798_1_0_0;
struct Marshal_t798;
const Il2CppTypeDefinitionMetadata Marshal_t798_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Marshal_t798_VTable/* vtableMethods */
	, Marshal_t798_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1441/* fieldStart */

};
TypeInfo Marshal_t798_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Marshal"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, Marshal_t798_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Marshal_t798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 462/* custom_attributes_cache */
	, &Marshal_t798_0_0_0/* byval_arg */
	, &Marshal_t798_1_0_0/* this_arg */
	, &Marshal_t798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Marshal_t798)/* instance_size */
	, sizeof (Marshal_t798)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Marshal_t798_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 262529/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
// Metadata Definition System.Runtime.InteropServices.MarshalDirectiveException
extern TypeInfo MarshalDirectiveException_t2293_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern const MethodInfo MarshalDirectiveException__ctor_m12099_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m12099/* method */
	, &MarshalDirectiveException_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MarshalDirectiveException_t2293_MarshalDirectiveException__ctor_m12100_ParameterInfos[] = 
{
	{"info", 0, 134221923, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134221924, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MarshalDirectiveException__ctor_m12100_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m12100/* method */
	, &MarshalDirectiveException_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MarshalDirectiveException_t2293_MarshalDirectiveException__ctor_m12100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MarshalDirectiveException_t2293_MethodInfos[] =
{
	&MarshalDirectiveException__ctor_m12099_MethodInfo,
	&MarshalDirectiveException__ctor_m12100_MethodInfo,
	NULL
};
static const Il2CppMethodReference MarshalDirectiveException_t2293_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MarshalDirectiveException_t2293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MarshalDirectiveException_t2293_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalDirectiveException_t2293_0_0_0;
extern const Il2CppType MarshalDirectiveException_t2293_1_0_0;
extern const Il2CppType SystemException_t2010_0_0_0;
struct MarshalDirectiveException_t2293;
const Il2CppTypeDefinitionMetadata MarshalDirectiveException_t2293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalDirectiveException_t2293_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, MarshalDirectiveException_t2293_VTable/* vtableMethods */
	, MarshalDirectiveException_t2293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1443/* fieldStart */

};
TypeInfo MarshalDirectiveException_t2293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalDirectiveException"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, MarshalDirectiveException_t2293_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MarshalDirectiveException_t2293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 470/* custom_attributes_cache */
	, &MarshalDirectiveException_t2293_0_0_0/* byval_arg */
	, &MarshalDirectiveException_t2293_1_0_0/* this_arg */
	, &MarshalDirectiveException_t2293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalDirectiveException_t2293)/* instance_size */
	, sizeof (MarshalDirectiveException_t2293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute
extern TypeInfo PreserveSigAttribute_t2294_il2cpp_TypeInfo;
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern const MethodInfo PreserveSigAttribute__ctor_m12101_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PreserveSigAttribute__ctor_m12101/* method */
	, &PreserveSigAttribute_t2294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PreserveSigAttribute_t2294_MethodInfos[] =
{
	&PreserveSigAttribute__ctor_m12101_MethodInfo,
	NULL
};
static const Il2CppMethodReference PreserveSigAttribute_t2294_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PreserveSigAttribute_t2294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PreserveSigAttribute_t2294_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreserveSigAttribute_t2294_0_0_0;
extern const Il2CppType PreserveSigAttribute_t2294_1_0_0;
struct PreserveSigAttribute_t2294;
const Il2CppTypeDefinitionMetadata PreserveSigAttribute_t2294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PreserveSigAttribute_t2294_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, PreserveSigAttribute_t2294_VTable/* vtableMethods */
	, PreserveSigAttribute_t2294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PreserveSigAttribute_t2294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreserveSigAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, PreserveSigAttribute_t2294_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PreserveSigAttribute_t2294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 471/* custom_attributes_cache */
	, &PreserveSigAttribute_t2294_0_0_0/* byval_arg */
	, &PreserveSigAttribute_t2294_1_0_0/* this_arg */
	, &PreserveSigAttribute_t2294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PreserveSigAttribute_t2294)/* instance_size */
	, sizeof (PreserveSigAttribute_t2294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
// Metadata Definition System.Runtime.InteropServices.SafeHandle
extern TypeInfo SafeHandle_t2073_il2cpp_TypeInfo;
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo SafeHandle_t2073_SafeHandle__ctor_m12102_ParameterInfos[] = 
{
	{"invalidHandleValue", 0, 134221925, 0, &IntPtr_t_0_0_0},
	{"ownsHandle", 1, 134221926, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::.ctor(System.IntPtr,System.Boolean)
extern const MethodInfo SafeHandle__ctor_m12102_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SafeHandle__ctor_m12102/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t_SByte_t177/* invoker_method */
	, SafeHandle_t2073_SafeHandle__ctor_m12102_ParameterInfos/* parameters */
	, 472/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Close()
extern const MethodInfo SafeHandle_Close_m12103_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&SafeHandle_Close_m12103/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 473/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_1_0_0;
extern const Il2CppType Boolean_t176_1_0_0;
static const ParameterInfo SafeHandle_t2073_SafeHandle_DangerousAddRef_m12104_ParameterInfos[] = 
{
	{"success", 0, 134221927, 0, &Boolean_t176_1_0_0},
};
extern void* RuntimeInvoker_Void_t175_BooleanU26_t532 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
extern const MethodInfo SafeHandle_DangerousAddRef_m12104_MethodInfo = 
{
	"DangerousAddRef"/* name */
	, (methodPointerType)&SafeHandle_DangerousAddRef_m12104/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_BooleanU26_t532/* invoker_method */
	, SafeHandle_t2073_SafeHandle_DangerousAddRef_m12104_ParameterInfos/* parameters */
	, 474/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern const MethodInfo SafeHandle_DangerousGetHandle_m12105_MethodInfo = 
{
	"DangerousGetHandle"/* name */
	, (methodPointerType)&SafeHandle_DangerousGetHandle_m12105/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 475/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
extern const MethodInfo SafeHandle_DangerousRelease_m12106_MethodInfo = 
{
	"DangerousRelease"/* name */
	, (methodPointerType)&SafeHandle_DangerousRelease_m12106/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 476/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern const MethodInfo SafeHandle_Dispose_m12107_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m12107/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 477/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo SafeHandle_t2073_SafeHandle_Dispose_m12108_ParameterInfos[] = 
{
	{"disposing", 0, 134221928, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose(System.Boolean)
extern const MethodInfo SafeHandle_Dispose_m12108_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m12108/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, SafeHandle_t2073_SafeHandle_Dispose_m12108_ParameterInfos/* parameters */
	, 478/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle()
extern const MethodInfo SafeHandle_ReleaseHandle_m14570_MethodInfo = 
{
	"ReleaseHandle"/* name */
	, NULL/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 479/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SafeHandle_t2073_SafeHandle_SetHandle_m12109_ParameterInfos[] = 
{
	{"handle", 0, 134221929, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandle(System.IntPtr)
extern const MethodInfo SafeHandle_SetHandle_m12109_MethodInfo = 
{
	"SetHandle"/* name */
	, (methodPointerType)&SafeHandle_SetHandle_m12109/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t/* invoker_method */
	, SafeHandle_t2073_SafeHandle_SetHandle_m12109_ParameterInfos/* parameters */
	, 480/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid()
extern const MethodInfo SafeHandle_get_IsInvalid_m14571_MethodInfo = 
{
	"get_IsInvalid"/* name */
	, NULL/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 481/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Finalize()
extern const MethodInfo SafeHandle_Finalize_m12110_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&SafeHandle_Finalize_m12110/* method */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SafeHandle_t2073_MethodInfos[] =
{
	&SafeHandle__ctor_m12102_MethodInfo,
	&SafeHandle_Close_m12103_MethodInfo,
	&SafeHandle_DangerousAddRef_m12104_MethodInfo,
	&SafeHandle_DangerousGetHandle_m12105_MethodInfo,
	&SafeHandle_DangerousRelease_m12106_MethodInfo,
	&SafeHandle_Dispose_m12107_MethodInfo,
	&SafeHandle_Dispose_m12108_MethodInfo,
	&SafeHandle_ReleaseHandle_m14570_MethodInfo,
	&SafeHandle_SetHandle_m12109_MethodInfo,
	&SafeHandle_get_IsInvalid_m14571_MethodInfo,
	&SafeHandle_Finalize_m12110_MethodInfo,
	NULL
};
extern const MethodInfo SafeHandle_get_IsInvalid_m14571_MethodInfo;
static const PropertyInfo SafeHandle_t2073____IsInvalid_PropertyInfo = 
{
	&SafeHandle_t2073_il2cpp_TypeInfo/* parent */
	, "IsInvalid"/* name */
	, &SafeHandle_get_IsInvalid_m14571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SafeHandle_t2073_PropertyInfos[] =
{
	&SafeHandle_t2073____IsInvalid_PropertyInfo,
	NULL
};
extern const MethodInfo SafeHandle_Finalize_m12110_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m12107_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m12108_MethodInfo;
static const Il2CppMethodReference SafeHandle_t2073_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&SafeHandle_Finalize_m12110_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&SafeHandle_Dispose_m12107_MethodInfo,
	&SafeHandle_Dispose_m12108_MethodInfo,
	NULL,
	NULL,
};
static bool SafeHandle_t2073_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t152_0_0_0;
static const Il2CppType* SafeHandle_t2073_InterfacesTypeInfos[] = 
{
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandle_t2073_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandle_t2073_0_0_0;
extern const Il2CppType SafeHandle_t2073_1_0_0;
struct SafeHandle_t2073;
const Il2CppTypeDefinitionMetadata SafeHandle_t2073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandle_t2073_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandle_t2073_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t2281_0_0_0/* parent */
	, SafeHandle_t2073_VTable/* vtableMethods */
	, SafeHandle_t2073_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1444/* fieldStart */

};
TypeInfo SafeHandle_t2073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, SafeHandle_t2073_MethodInfos/* methods */
	, SafeHandle_t2073_PropertyInfos/* properties */
	, NULL/* events */
	, &SafeHandle_t2073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandle_t2073_0_0_0/* byval_arg */
	, &SafeHandle_t2073_1_0_0/* this_arg */
	, &SafeHandle_t2073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandle_t2073)/* instance_size */
	, sizeof (SafeHandle_t2073)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute
extern TypeInfo TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeLibImportClassAttribute_t2295_TypeLibImportClassAttribute__ctor_m12111_ParameterInfos[] = 
{
	{"importClass", 0, 134221930, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern const MethodInfo TypeLibImportClassAttribute__ctor_m12111_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibImportClassAttribute__ctor_m12111/* method */
	, &TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TypeLibImportClassAttribute_t2295_TypeLibImportClassAttribute__ctor_m12111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibImportClassAttribute_t2295_MethodInfos[] =
{
	&TypeLibImportClassAttribute__ctor_m12111_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibImportClassAttribute_t2295_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TypeLibImportClassAttribute_t2295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibImportClassAttribute_t2295_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibImportClassAttribute_t2295_0_0_0;
extern const Il2CppType TypeLibImportClassAttribute_t2295_1_0_0;
struct TypeLibImportClassAttribute_t2295;
const Il2CppTypeDefinitionMetadata TypeLibImportClassAttribute_t2295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibImportClassAttribute_t2295_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, TypeLibImportClassAttribute_t2295_VTable/* vtableMethods */
	, TypeLibImportClassAttribute_t2295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1448/* fieldStart */

};
TypeInfo TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibImportClassAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibImportClassAttribute_t2295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 482/* custom_attributes_cache */
	, &TypeLibImportClassAttribute_t2295_0_0_0/* byval_arg */
	, &TypeLibImportClassAttribute_t2295_1_0_0/* this_arg */
	, &TypeLibImportClassAttribute_t2295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibImportClassAttribute_t2295)/* instance_size */
	, sizeof (TypeLibImportClassAttribute_t2295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute
extern TypeInfo TypeLibVersionAttribute_t2296_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TypeLibVersionAttribute_t2296_TypeLibVersionAttribute__ctor_m12112_ParameterInfos[] = 
{
	{"major", 0, 134221931, 0, &Int32_t135_0_0_0},
	{"minor", 1, 134221932, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TypeLibVersionAttribute__ctor_m12112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibVersionAttribute__ctor_m12112/* method */
	, &TypeLibVersionAttribute_t2296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, TypeLibVersionAttribute_t2296_TypeLibVersionAttribute__ctor_m12112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibVersionAttribute_t2296_MethodInfos[] =
{
	&TypeLibVersionAttribute__ctor_m12112_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibVersionAttribute_t2296_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TypeLibVersionAttribute_t2296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibVersionAttribute_t2296_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibVersionAttribute_t2296_0_0_0;
extern const Il2CppType TypeLibVersionAttribute_t2296_1_0_0;
struct TypeLibVersionAttribute_t2296;
const Il2CppTypeDefinitionMetadata TypeLibVersionAttribute_t2296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibVersionAttribute_t2296_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, TypeLibVersionAttribute_t2296_VTable/* vtableMethods */
	, TypeLibVersionAttribute_t2296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1449/* fieldStart */

};
TypeInfo TypeLibVersionAttribute_t2296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibVersionAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibVersionAttribute_t2296_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibVersionAttribute_t2296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 483/* custom_attributes_cache */
	, &TypeLibVersionAttribute_t2296_0_0_0/* byval_arg */
	, &TypeLibVersionAttribute_t2296_1_0_0/* this_arg */
	, &TypeLibVersionAttribute_t2296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibVersionAttribute_t2296)/* instance_size */
	, sizeof (TypeLibVersionAttribute_t2296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
// Metadata Definition System.Runtime.InteropServices.UnmanagedType
extern TypeInfo UnmanagedType_t2297_il2cpp_TypeInfo;
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTypeMethodDeclarations.h"
static const MethodInfo* UnmanagedType_t2297_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnmanagedType_t2297_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UnmanagedType_t2297_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnmanagedType_t2297_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnmanagedType_t2297_0_0_0;
extern const Il2CppType UnmanagedType_t2297_1_0_0;
const Il2CppTypeDefinitionMetadata UnmanagedType_t2297_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnmanagedType_t2297_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UnmanagedType_t2297_VTable/* vtableMethods */
	, UnmanagedType_t2297_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1451/* fieldStart */

};
TypeInfo UnmanagedType_t2297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnmanagedType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, UnmanagedType_t2297_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 484/* custom_attributes_cache */
	, &UnmanagedType_t2297_0_0_0/* byval_arg */
	, &UnmanagedType_t2297_1_0_0/* this_arg */
	, &UnmanagedType_t2297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnmanagedType_t2297)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnmanagedType_t2297)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 36/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Activator
extern TypeInfo _Activator_t2687_il2cpp_TypeInfo;
static const MethodInfo* _Activator_t2687_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Activator_t2687_0_0_0;
extern const Il2CppType _Activator_t2687_1_0_0;
struct _Activator_t2687;
const Il2CppTypeDefinitionMetadata _Activator_t2687_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Activator_t2687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Activator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Activator_t2687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Activator_t2687_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 485/* custom_attributes_cache */
	, &_Activator_t2687_0_0_0/* byval_arg */
	, &_Activator_t2687_1_0_0/* this_arg */
	, &_Activator_t2687_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Assembly
extern TypeInfo _Assembly_t2677_il2cpp_TypeInfo;
static const MethodInfo* _Assembly_t2677_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Assembly_t2677_0_0_0;
extern const Il2CppType _Assembly_t2677_1_0_0;
struct _Assembly_t2677;
const Il2CppTypeDefinitionMetadata _Assembly_t2677_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Assembly_t2677_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Assembly"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Assembly_t2677_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Assembly_t2677_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 486/* custom_attributes_cache */
	, &_Assembly_t2677_0_0_0/* byval_arg */
	, &_Assembly_t2677_1_0_0/* this_arg */
	, &_Assembly_t2677_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyBuilder
extern TypeInfo _AssemblyBuilder_t2668_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyBuilder_t2668_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyBuilder_t2668_0_0_0;
extern const Il2CppType _AssemblyBuilder_t2668_1_0_0;
struct _AssemblyBuilder_t2668;
const Il2CppTypeDefinitionMetadata _AssemblyBuilder_t2668_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyBuilder_t2668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyBuilder_t2668_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyBuilder_t2668_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 487/* custom_attributes_cache */
	, &_AssemblyBuilder_t2668_0_0_0/* byval_arg */
	, &_AssemblyBuilder_t2668_1_0_0/* this_arg */
	, &_AssemblyBuilder_t2668_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyName
extern TypeInfo _AssemblyName_t2678_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyName_t2678_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyName_t2678_1_0_0;
struct _AssemblyName_t2678;
const Il2CppTypeDefinitionMetadata _AssemblyName_t2678_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyName_t2678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyName"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyName_t2678_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyName_t2678_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 488/* custom_attributes_cache */
	, &_AssemblyName_t2678_0_0_0/* byval_arg */
	, &_AssemblyName_t2678_1_0_0/* this_arg */
	, &_AssemblyName_t2678_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder
extern TypeInfo _ConstructorBuilder_t2669_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorBuilder_t2669_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorBuilder_t2669_0_0_0;
extern const Il2CppType _ConstructorBuilder_t2669_1_0_0;
struct _ConstructorBuilder_t2669;
const Il2CppTypeDefinitionMetadata _ConstructorBuilder_t2669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorBuilder_t2669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorBuilder_t2669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorBuilder_t2669_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 489/* custom_attributes_cache */
	, &_ConstructorBuilder_t2669_0_0_0/* byval_arg */
	, &_ConstructorBuilder_t2669_1_0_0/* this_arg */
	, &_ConstructorBuilder_t2669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorInfo
extern TypeInfo _ConstructorInfo_t2679_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorInfo_t2679_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorInfo_t2679_1_0_0;
struct _ConstructorInfo_t2679;
const Il2CppTypeDefinitionMetadata _ConstructorInfo_t2679_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorInfo_t2679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorInfo_t2679_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorInfo_t2679_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 490/* custom_attributes_cache */
	, &_ConstructorInfo_t2679_0_0_0/* byval_arg */
	, &_ConstructorInfo_t2679_1_0_0/* this_arg */
	, &_ConstructorInfo_t2679_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EnumBuilder
extern TypeInfo _EnumBuilder_t2670_il2cpp_TypeInfo;
static const MethodInfo* _EnumBuilder_t2670_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EnumBuilder_t2670_0_0_0;
extern const Il2CppType _EnumBuilder_t2670_1_0_0;
struct _EnumBuilder_t2670;
const Il2CppTypeDefinitionMetadata _EnumBuilder_t2670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EnumBuilder_t2670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EnumBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EnumBuilder_t2670_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EnumBuilder_t2670_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 491/* custom_attributes_cache */
	, &_EnumBuilder_t2670_0_0_0/* byval_arg */
	, &_EnumBuilder_t2670_1_0_0/* this_arg */
	, &_EnumBuilder_t2670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EventInfo
extern TypeInfo _EventInfo_t2680_il2cpp_TypeInfo;
static const MethodInfo* _EventInfo_t2680_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EventInfo_t2680_1_0_0;
struct _EventInfo_t2680;
const Il2CppTypeDefinitionMetadata _EventInfo_t2680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EventInfo_t2680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EventInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EventInfo_t2680_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EventInfo_t2680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 492/* custom_attributes_cache */
	, &_EventInfo_t2680_0_0_0/* byval_arg */
	, &_EventInfo_t2680_1_0_0/* this_arg */
	, &_EventInfo_t2680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldBuilder
extern TypeInfo _FieldBuilder_t2671_il2cpp_TypeInfo;
static const MethodInfo* _FieldBuilder_t2671_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldBuilder_t2671_0_0_0;
extern const Il2CppType _FieldBuilder_t2671_1_0_0;
struct _FieldBuilder_t2671;
const Il2CppTypeDefinitionMetadata _FieldBuilder_t2671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldBuilder_t2671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldBuilder_t2671_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldBuilder_t2671_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 493/* custom_attributes_cache */
	, &_FieldBuilder_t2671_0_0_0/* byval_arg */
	, &_FieldBuilder_t2671_1_0_0/* this_arg */
	, &_FieldBuilder_t2671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldInfo
extern TypeInfo _FieldInfo_t2681_il2cpp_TypeInfo;
static const MethodInfo* _FieldInfo_t2681_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldInfo_t2681_1_0_0;
struct _FieldInfo_t2681;
const Il2CppTypeDefinitionMetadata _FieldInfo_t2681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldInfo_t2681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldInfo_t2681_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldInfo_t2681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 494/* custom_attributes_cache */
	, &_FieldInfo_t2681_0_0_0/* byval_arg */
	, &_FieldInfo_t2681_1_0_0/* this_arg */
	, &_FieldInfo_t2681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBase
extern TypeInfo _MethodBase_t2682_il2cpp_TypeInfo;
static const MethodInfo* _MethodBase_t2682_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBase_t2682_1_0_0;
struct _MethodBase_t2682;
const Il2CppTypeDefinitionMetadata _MethodBase_t2682_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBase_t2682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBase"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBase_t2682_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBase_t2682_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 495/* custom_attributes_cache */
	, &_MethodBase_t2682_0_0_0/* byval_arg */
	, &_MethodBase_t2682_1_0_0/* this_arg */
	, &_MethodBase_t2682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBuilder
extern TypeInfo _MethodBuilder_t2672_il2cpp_TypeInfo;
static const MethodInfo* _MethodBuilder_t2672_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBuilder_t2672_0_0_0;
extern const Il2CppType _MethodBuilder_t2672_1_0_0;
struct _MethodBuilder_t2672;
const Il2CppTypeDefinitionMetadata _MethodBuilder_t2672_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBuilder_t2672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBuilder_t2672_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBuilder_t2672_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 496/* custom_attributes_cache */
	, &_MethodBuilder_t2672_0_0_0/* byval_arg */
	, &_MethodBuilder_t2672_1_0_0/* this_arg */
	, &_MethodBuilder_t2672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodInfo
extern TypeInfo _MethodInfo_t2683_il2cpp_TypeInfo;
static const MethodInfo* _MethodInfo_t2683_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodInfo_t2683_1_0_0;
struct _MethodInfo_t2683;
const Il2CppTypeDefinitionMetadata _MethodInfo_t2683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodInfo_t2683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodInfo_t2683_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodInfo_t2683_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 497/* custom_attributes_cache */
	, &_MethodInfo_t2683_0_0_0/* byval_arg */
	, &_MethodInfo_t2683_1_0_0/* this_arg */
	, &_MethodInfo_t2683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Module
extern TypeInfo _Module_t2684_il2cpp_TypeInfo;
static const MethodInfo* _Module_t2684_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Module_t2684_1_0_0;
struct _Module_t2684;
const Il2CppTypeDefinitionMetadata _Module_t2684_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Module_t2684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Module"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Module_t2684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Module_t2684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 498/* custom_attributes_cache */
	, &_Module_t2684_0_0_0/* byval_arg */
	, &_Module_t2684_1_0_0/* this_arg */
	, &_Module_t2684_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
extern TypeInfo _ModuleBuilder_t2673_il2cpp_TypeInfo;
static const MethodInfo* _ModuleBuilder_t2673_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ModuleBuilder_t2673_0_0_0;
extern const Il2CppType _ModuleBuilder_t2673_1_0_0;
struct _ModuleBuilder_t2673;
const Il2CppTypeDefinitionMetadata _ModuleBuilder_t2673_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ModuleBuilder_t2673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ModuleBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ModuleBuilder_t2673_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ModuleBuilder_t2673_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 499/* custom_attributes_cache */
	, &_ModuleBuilder_t2673_0_0_0/* byval_arg */
	, &_ModuleBuilder_t2673_1_0_0/* this_arg */
	, &_ModuleBuilder_t2673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
extern TypeInfo _ParameterBuilder_t2674_il2cpp_TypeInfo;
static const MethodInfo* _ParameterBuilder_t2674_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterBuilder_t2674_0_0_0;
extern const Il2CppType _ParameterBuilder_t2674_1_0_0;
struct _ParameterBuilder_t2674;
const Il2CppTypeDefinitionMetadata _ParameterBuilder_t2674_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterBuilder_t2674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterBuilder_t2674_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterBuilder_t2674_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 500/* custom_attributes_cache */
	, &_ParameterBuilder_t2674_0_0_0/* byval_arg */
	, &_ParameterBuilder_t2674_1_0_0/* this_arg */
	, &_ParameterBuilder_t2674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterInfo
extern TypeInfo _ParameterInfo_t2685_il2cpp_TypeInfo;
static const MethodInfo* _ParameterInfo_t2685_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterInfo_t2685_1_0_0;
struct _ParameterInfo_t2685;
const Il2CppTypeDefinitionMetadata _ParameterInfo_t2685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterInfo_t2685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterInfo_t2685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterInfo_t2685_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 501/* custom_attributes_cache */
	, &_ParameterInfo_t2685_0_0_0/* byval_arg */
	, &_ParameterInfo_t2685_1_0_0/* this_arg */
	, &_ParameterInfo_t2685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
