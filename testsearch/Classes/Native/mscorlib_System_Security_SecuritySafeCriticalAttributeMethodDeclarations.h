﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecuritySafeCriticalAttribute
struct SecuritySafeCriticalAttribute_t1449;

// System.Void System.Security.SecuritySafeCriticalAttribute::.ctor()
extern "C" void SecuritySafeCriticalAttribute__ctor_m7076 (SecuritySafeCriticalAttribute_t1449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
