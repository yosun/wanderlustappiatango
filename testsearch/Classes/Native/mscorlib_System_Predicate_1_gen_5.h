﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Reflection.MethodInfo>
struct  Predicate_1_t3157  : public MulticastDelegate_t314
{
};
