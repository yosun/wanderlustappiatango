﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t805;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t737;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t770;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButton>
struct IEnumerator_1_t889;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.VirtualButton>
struct ICollection_1_t4160;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButton>
struct ReadOnlyCollection_1_t3391;
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3389;
// System.Predicate`1<Vuforia.VirtualButton>
struct Predicate_1_t3392;
// System.Comparison`1<Vuforia.VirtualButton>
struct Comparison_1_t3394;
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_44.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4371(__this, method) (( void (*) (List_1_t805 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18796(__this, ___collection, method) (( void (*) (List_1_t805 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Int32)
#define List_1__ctor_m18797(__this, ___capacity, method) (( void (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.cctor()
#define List_1__cctor_m18798(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18799(__this, method) (( Object_t* (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18800(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t805 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18801(__this, method) (( Object_t * (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18802(__this, ___item, method) (( int32_t (*) (List_1_t805 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18803(__this, ___item, method) (( bool (*) (List_1_t805 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18804(__this, ___item, method) (( int32_t (*) (List_1_t805 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18805(__this, ___index, ___item, method) (( void (*) (List_1_t805 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18806(__this, ___item, method) (( void (*) (List_1_t805 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18807(__this, method) (( bool (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18808(__this, method) (( bool (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18809(__this, method) (( Object_t * (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18810(__this, method) (( bool (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18811(__this, method) (( bool (*) (List_1_t805 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18812(__this, ___index, method) (( Object_t * (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18813(__this, ___index, ___value, method) (( void (*) (List_1_t805 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Add(T)
#define List_1_Add_m18814(__this, ___item, method) (( void (*) (List_1_t805 *, VirtualButton_t737 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18815(__this, ___newCount, method) (( void (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18816(__this, ___collection, method) (( void (*) (List_1_t805 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18817(__this, ___enumerable, method) (( void (*) (List_1_t805 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18818(__this, ___collection, method) (( void (*) (List_1_t805 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::AsReadOnly()
#define List_1_AsReadOnly_m18819(__this, method) (( ReadOnlyCollection_1_t3391 * (*) (List_1_t805 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Clear()
#define List_1_Clear_m18820(__this, method) (( void (*) (List_1_t805 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Contains(T)
#define List_1_Contains_m18821(__this, ___item, method) (( bool (*) (List_1_t805 *, VirtualButton_t737 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18822(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t805 *, VirtualButtonU5BU5D_t3389*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::Find(System.Predicate`1<T>)
#define List_1_Find_m18823(__this, ___match, method) (( VirtualButton_t737 * (*) (List_1_t805 *, Predicate_1_t3392 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18824(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3392 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18825(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t805 *, int32_t, int32_t, Predicate_1_t3392 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetEnumerator()
#define List_1_GetEnumerator_m18826(__this, method) (( Enumerator_t3393  (*) (List_1_t805 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::IndexOf(T)
#define List_1_IndexOf_m18827(__this, ___item, method) (( int32_t (*) (List_1_t805 *, VirtualButton_t737 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18828(__this, ___start, ___delta, method) (( void (*) (List_1_t805 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18829(__this, ___index, method) (( void (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Insert(System.Int32,T)
#define List_1_Insert_m18830(__this, ___index, ___item, method) (( void (*) (List_1_t805 *, int32_t, VirtualButton_t737 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18831(__this, ___collection, method) (( void (*) (List_1_t805 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Remove(T)
#define List_1_Remove_m18832(__this, ___item, method) (( bool (*) (List_1_t805 *, VirtualButton_t737 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18833(__this, ___match, method) (( int32_t (*) (List_1_t805 *, Predicate_1_t3392 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18834(__this, ___index, method) (( void (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Reverse()
#define List_1_Reverse_m18835(__this, method) (( void (*) (List_1_t805 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort()
#define List_1_Sort_m18836(__this, method) (( void (*) (List_1_t805 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18837(__this, ___comparison, method) (( void (*) (List_1_t805 *, Comparison_1_t3394 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::ToArray()
#define List_1_ToArray_m18838(__this, method) (( VirtualButtonU5BU5D_t3389* (*) (List_1_t805 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::TrimExcess()
#define List_1_TrimExcess_m18839(__this, method) (( void (*) (List_1_t805 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Capacity()
#define List_1_get_Capacity_m18840(__this, method) (( int32_t (*) (List_1_t805 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18841(__this, ___value, method) (( void (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Count()
#define List_1_get_Count_m18842(__this, method) (( int32_t (*) (List_1_t805 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Item(System.Int32)
#define List_1_get_Item_m18843(__this, ___index, method) (( VirtualButton_t737 * (*) (List_1_t805 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Item(System.Int32,T)
#define List_1_set_Item_m18844(__this, ___index, ___value, method) (( void (*) (List_1_t805 *, int32_t, VirtualButton_t737 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
