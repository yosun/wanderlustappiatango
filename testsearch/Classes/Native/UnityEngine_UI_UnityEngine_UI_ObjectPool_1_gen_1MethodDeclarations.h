﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t386;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t387;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t426;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2440(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t386 *, UnityAction_1_t387 *, UnityAction_1_t387 *, const MethodInfo*))ObjectPool_1__ctor_m15674_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m18385(__this, method) (( int32_t (*) (ObjectPool_1_t386 *, const MethodInfo*))ObjectPool_1_get_countAll_m15676_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18386(__this, ___value, method) (( void (*) (ObjectPool_1_t386 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15678_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m18387(__this, method) (( int32_t (*) (ObjectPool_1_t386 *, const MethodInfo*))ObjectPool_1_get_countActive_m15680_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18388(__this, method) (( int32_t (*) (ObjectPool_1_t386 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15682_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m2441(__this, method) (( List_1_t426 * (*) (ObjectPool_1_t386 *, const MethodInfo*))ObjectPool_1_Get_m15684_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m2442(__this, ___element, method) (( void (*) (ObjectPool_1_t386 *, List_1_t426 *, const MethodInfo*))ObjectPool_1_Release_m15686_gshared)(__this, ___element, method)
