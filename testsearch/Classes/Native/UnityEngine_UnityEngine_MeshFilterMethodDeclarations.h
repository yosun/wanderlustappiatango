﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshFilter
struct MeshFilter_t157;
// UnityEngine.Mesh
struct Mesh_t160;

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t160 * MeshFilter_get_mesh_m4304 (MeshFilter_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m4616 (MeshFilter_t157 * __this, Mesh_t160 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t160 * MeshFilter_get_sharedMesh_m513 (MeshFilter_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m4358 (MeshFilter_t157 * __this, Mesh_t160 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
