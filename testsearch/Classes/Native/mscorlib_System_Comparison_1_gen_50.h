﻿#pragma once
#include <stdint.h>
// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<DG.Tweening.TweenCallback>
struct  Comparison_1_t3681  : public MulticastDelegate_t314
{
};
