﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t3808;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t1258;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_36.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__33MethodDeclarations.h"
#define Enumerator__ctor_m25511(__this, ___dictionary, method) (( void (*) (Enumerator_t3808 *, Dictionary_2_t1258 *, const MethodInfo*))Enumerator__ctor_m25409_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25512(__this, method) (( Object_t * (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25410_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25513(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25514(__this, method) (( Object_t * (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25515(__this, method) (( Object_t * (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m25516(__this, method) (( bool (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_MoveNext_m25414_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m25517(__this, method) (( KeyValuePair_2_t3805  (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_get_Current_m25415_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25518(__this, method) (( String_t* (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_get_CurrentKey_m25416_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25519(__this, method) (( int64_t (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_get_CurrentValue_m25417_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m25520(__this, method) (( void (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_VerifyState_m25418_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25521(__this, method) (( void (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_VerifyCurrent_m25419_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m25522(__this, method) (( void (*) (Enumerator_t3808 *, const MethodInfo*))Enumerator_Dispose_m25420_gshared)(__this, method)
