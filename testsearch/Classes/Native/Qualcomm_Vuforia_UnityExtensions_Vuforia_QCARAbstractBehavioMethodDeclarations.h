﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARAbstractBehaviour
struct QCARAbstractBehaviour_t75;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// UnityEngine.Camera
struct Camera_t3;
// System.String
struct String_t;
// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t136;
// System.Action
struct Action_t147;
// System.Action`1<System.Boolean>
struct Action_1_t753;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t794;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t178;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t187;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"

// System.Boolean Vuforia.QCARAbstractBehaviour::get_AutoAdjustStereoCameraSkewing()
extern "C" bool QCARAbstractBehaviour_get_AutoAdjustStereoCameraSkewing_m4138 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_SceneScaleFactor()
extern "C" float QCARAbstractBehaviour_get_SceneScaleFactor_m4139 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SceneScaleFactor(System.Single)
extern "C" void QCARAbstractBehaviour_set_SceneScaleFactor_m4140 (QCARAbstractBehaviour_t75 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_CameraOffset()
extern "C" float QCARAbstractBehaviour_get_CameraOffset_m4141 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_CameraOffset(System.Single)
extern "C" void QCARAbstractBehaviour_set_CameraOffset_m4142 (QCARAbstractBehaviour_t75 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARAbstractBehaviour::get_WorldCenterModeSetting()
extern "C" int32_t QCARAbstractBehaviour_get_WorldCenterModeSetting_m4143 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableBehaviour Vuforia.QCARAbstractBehaviour::get_WorldCenter()
extern "C" TrackableBehaviour_t52 * QCARAbstractBehaviour_get_WorldCenter_m4144 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_VideoBackGroundMirrored()
extern "C" bool QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4145 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
extern "C" void QCARAbstractBehaviour_set_VideoBackGroundMirrored_m4146 (QCARAbstractBehaviour_t75 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDeviceMode Vuforia.QCARAbstractBehaviour::get_CameraDeviceMode()
extern "C" int32_t QCARAbstractBehaviour_get_CameraDeviceMode_m4147 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_HasStarted()
extern "C" bool QCARAbstractBehaviour_get_HasStarted_m4148 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_IsStereoRendering()
extern "C" bool QCARAbstractBehaviour_get_IsStereoRendering_m4149 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_PrimaryCamera()
extern "C" Camera_t3 * QCARAbstractBehaviour_get_PrimaryCamera_m4150 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_PrimaryCamera(UnityEngine.Camera)
extern "C" void QCARAbstractBehaviour_set_PrimaryCamera_m4151 (QCARAbstractBehaviour_t75 * __this, Camera_t3 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_SecondaryCamera()
extern "C" Camera_t3 * QCARAbstractBehaviour_get_SecondaryCamera_m4152 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SecondaryCamera(UnityEngine.Camera)
extern "C" void QCARAbstractBehaviour_set_SecondaryCamera_m4153 (QCARAbstractBehaviour_t75 * __this, Camera_t3 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.QCARAbstractBehaviour::get_AppLicenseKey()
extern "C" String_t* QCARAbstractBehaviour_get_AppLicenseKey_m4154 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAutoAdjustStereoCameraSkewing(System.Boolean)
extern "C" void QCARAbstractBehaviour_SetAutoAdjustStereoCameraSkewing_m4155 (QCARAbstractBehaviour_t75 * __this, bool ___setSkewing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetSceneScaleFactor(System.Single)
extern "C" void QCARAbstractBehaviour_SetSceneScaleFactor_m4156 (QCARAbstractBehaviour_t75 * __this, float ___Scale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
extern "C" void QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m426 (QCARAbstractBehaviour_t75 * __this, Action_1_t136 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
extern "C" void QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m427 (QCARAbstractBehaviour_t75 * __this, Action_1_t136 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitializedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4157 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitializedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4158 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARStartedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_RegisterQCARStartedCallback_m4159 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARStartedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4160 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackablesUpdatedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4161 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterTrackablesUpdatedCallback(System.Action)
extern "C" void QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4162 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterOnPauseCallback(System.Action`1<System.Boolean>)
extern "C" void QCARAbstractBehaviour_RegisterOnPauseCallback_m4163 (QCARAbstractBehaviour_t75 * __this, Action_1_t753 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterOnPauseCallback(System.Action`1<System.Boolean>)
extern "C" void QCARAbstractBehaviour_UnregisterOnPauseCallback_m4164 (QCARAbstractBehaviour_t75 * __this, Action_1_t753 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetEditorValues(System.Single)
extern "C" void QCARAbstractBehaviour_SetEditorValues_m4165 (QCARAbstractBehaviour_t75 * __this, float ___Offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
extern "C" void QCARAbstractBehaviour_RegisterTrackerEventHandler_m4166 (QCARAbstractBehaviour_t75 * __this, Object_t * ___trackerEventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
extern "C" bool QCARAbstractBehaviour_UnregisterTrackerEventHandler_m4167 (QCARAbstractBehaviour_t75 * __this, Object_t * ___trackerEventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
extern "C" void QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4168 (QCARAbstractBehaviour_t75 * __this, Object_t * ___videoBgEventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
extern "C" bool QCARAbstractBehaviour_UnregisterVideoBgEventHandler_m4169 (QCARAbstractBehaviour_t75 * __this, Object_t * ___videoBgEventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
extern "C" void QCARAbstractBehaviour_SetWorldCenterMode_m4170 (QCARAbstractBehaviour_t75 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenter(Vuforia.TrackableBehaviour)
extern "C" void QCARAbstractBehaviour_SetWorldCenter_m4171 (QCARAbstractBehaviour_t75 * __this, TrackableBehaviour_t52 * ___trackable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAppLicenseKey(System.String)
extern "C" void QCARAbstractBehaviour_SetAppLicenseKey_m4172 (QCARAbstractBehaviour_t75 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.QCARAbstractBehaviour::GetViewportRectangle()
extern "C" Rect_t132  QCARAbstractBehaviour_GetViewportRectangle_m4173 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.QCARAbstractBehaviour::GetSurfaceOrientation()
extern "C" int32_t QCARAbstractBehaviour_GetSurfaceOrientation_m4174 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureVideoBackground(System.Boolean)
extern "C" void QCARAbstractBehaviour_ConfigureVideoBackground_m4175 (QCARAbstractBehaviour_t75 * __this, bool ___forceReflectionSetting, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern "C" void QCARAbstractBehaviour_ResetBackgroundPlane_m4176 (QCARAbstractBehaviour_t75 * __this, bool ___disable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterRenderOnUpdateCallback(System.Action)
extern "C" void QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4177 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterRenderOnUpdateCallback(System.Action)
extern "C" void QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4178 (QCARAbstractBehaviour_t75 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureView()
extern "C" void QCARAbstractBehaviour_ConfigureView_m4179 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::EnableObjectRenderer(UnityEngine.GameObject,System.Boolean)
extern "C" void QCARAbstractBehaviour_EnableObjectRenderer_m4180 (QCARAbstractBehaviour_t75 * __this, GameObject_t2 * ___go, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Start()
extern "C" void QCARAbstractBehaviour_Start_m4181 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnEnable()
extern "C" void QCARAbstractBehaviour_OnEnable_m4182 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateView()
extern "C" void QCARAbstractBehaviour_UpdateView_m4183 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Update()
extern "C" void QCARAbstractBehaviour_Update_m4184 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnApplicationPause(System.Boolean)
extern "C" void QCARAbstractBehaviour_OnApplicationPause_m4185 (QCARAbstractBehaviour_t75 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDisable()
extern "C" void QCARAbstractBehaviour_OnDisable_m4186 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDestroy()
extern "C" void QCARAbstractBehaviour_OnDestroy_m4187 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetUnityPlayerImplementation(Vuforia.IUnityPlayer)
extern "C" void QCARAbstractBehaviour_SetUnityPlayerImplementation_m489 (QCARAbstractBehaviour_t75 * __this, Object_t * ___implementation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StartQCAR(System.Boolean,System.Boolean)
extern "C" bool QCARAbstractBehaviour_StartQCAR_m4188 (QCARAbstractBehaviour_t75 * __this, bool ___startObjectTracker, bool ___startMarkerTracker, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StopQCAR()
extern "C" bool QCARAbstractBehaviour_StopQCAR_m4189 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateStereoDepth()
extern "C" void QCARAbstractBehaviour_UpdateStereoDepth_m4190 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ProjectionMatricesUpdated()
extern "C" void QCARAbstractBehaviour_ProjectionMatricesUpdated_m4191 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ApplyMatrix(UnityEngine.Camera,UnityEngine.Matrix4x4)
extern "C" void QCARAbstractBehaviour_ApplyMatrix_m4192 (QCARAbstractBehaviour_t75 * __this, Camera_t3 * ___cam, Matrix4x4_t163  ___inputMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateProjection(UnityEngine.ScreenOrientation)
extern "C" void QCARAbstractBehaviour_UpdateProjection_m4193 (QCARAbstractBehaviour_t75 * __this, int32_t ___orientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::DeinitRequestedTrackers()
extern "C" void QCARAbstractBehaviour_DeinitRequestedTrackers_m4194 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckSceneScaleFactor()
extern "C" void QCARAbstractBehaviour_CheckSceneScaleFactor_m4195 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckForSurfaceChanges()
extern "C" void QCARAbstractBehaviour_CheckForSurfaceChanges_m4196 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDeviceMode(Vuforia.CameraDevice/CameraDeviceMode)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m693 (QCARAbstractBehaviour_t75 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousImageTargets()
extern "C" int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m694 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousImageTargets(System.Int32)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m695 (QCARAbstractBehaviour_t75 * __this, int32_t ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousObjectTargets()
extern "C" int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m696 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousObjectTargets(System.Int32)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m697 (QCARAbstractBehaviour_t75 * __this, int32_t ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetDelayedLoadingObjectTargets()
extern "C" bool QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m698 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetUseDelayedLoadingObjectTargets(System.Boolean)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m699 (QCARAbstractBehaviour_t75 * __this, bool ___useDelayedLoading, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDirection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetCameraDirection()
extern "C" int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m700 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDirection(Vuforia.CameraDevice/CameraDirection)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m701 (QCARAbstractBehaviour_t75 * __this, int32_t ___cameraDirection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBackgroundReflection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMirrorVideoBackground()
extern "C" int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m702 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMirrorVideoBackground(Vuforia.QCARRenderer/VideoBackgroundReflection)
extern "C" void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m703 (QCARAbstractBehaviour_t75 * __this, int32_t ___reflection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::.ctor()
extern "C" void QCARAbstractBehaviour__ctor_m484 (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
