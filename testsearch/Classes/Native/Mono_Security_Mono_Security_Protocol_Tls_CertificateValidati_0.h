﻿#pragma once
#include <stdint.h>
// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1768;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1700;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t1770  : public MulticastDelegate_t314
{
};
