﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t606;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t68;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t94;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t85;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t58;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t66;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t70;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t44;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t83;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t72;

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t68 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2849 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t94 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2850 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t85 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2851 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t58 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2852 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t66 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2853 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t70 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2854 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t44 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2855 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t101 * NullBehaviourComponentFactory_AddWordBehaviour_m2856 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t83 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2857 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t72 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2858 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m2859 (NullBehaviourComponentFactory_t606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
