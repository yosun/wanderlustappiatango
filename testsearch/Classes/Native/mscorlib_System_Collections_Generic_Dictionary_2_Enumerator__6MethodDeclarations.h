﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>
struct Enumerator_t3134;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
struct Dictionary_2_t9;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m15145(__this, ___dictionary, method) (( void (*) (Enumerator_t3134 *, Dictionary_2_t9 *, const MethodInfo*))Enumerator__ctor_m15040_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15146(__this, method) (( Object_t * (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15147(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15148(__this, method) (( Object_t * (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15149(__this, method) (( Object_t * (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::MoveNext()
#define Enumerator_MoveNext_m15150(__this, method) (( bool (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_MoveNext_m15045_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::get_Current()
#define Enumerator_get_Current_m15151(__this, method) (( KeyValuePair_2_t3131  (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_get_Current_m15046_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15152(__this, method) (( String_t* (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_get_CurrentKey_m15047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15153(__this, method) (( GameObjectU5BU5D_t5* (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_get_CurrentValue_m15048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::VerifyState()
#define Enumerator_VerifyState_m15154(__this, method) (( void (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_VerifyState_m15049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15155(__this, method) (( void (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_VerifyCurrent_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject[]>::Dispose()
#define Enumerator_Dispose_m15156(__this, method) (( void (*) (Enumerator_t3134 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
