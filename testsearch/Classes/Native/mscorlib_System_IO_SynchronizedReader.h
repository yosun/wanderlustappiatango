﻿#pragma once
#include <stdint.h>
// System.IO.TextReader
struct TextReader_t2139;
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// System.IO.SynchronizedReader
struct  SynchronizedReader_t2215  : public TextReader_t2139
{
	// System.IO.TextReader System.IO.SynchronizedReader::reader
	TextReader_t2139 * ___reader_1;
};
