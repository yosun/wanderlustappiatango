﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
struct U3CU3Ec__DisplayClass9a_t967;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::.ctor()
extern "C" void U3CU3Ec__DisplayClass9a__ctor_m5391 (U3CU3Ec__DisplayClass9a_t967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__98()
extern "C" Vector3_t14  U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392 (U3CU3Ec__DisplayClass9a_t967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__99(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393 (U3CU3Ec__DisplayClass9a_t967 * __this, Vector3_t14  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
