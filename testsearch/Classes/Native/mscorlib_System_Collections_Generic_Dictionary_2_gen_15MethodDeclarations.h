﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t716;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4073;
// System.Collections.Generic.ICollection`1<Vuforia.Prop>
struct ICollection_1_t4239;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t105;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.Prop>
struct KeyCollection_t870;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Prop>
struct ValueCollection_t853;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3221;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.Prop>
struct IDictionary_2_t4240;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct KeyValuePair_2U5BU5D_t4241;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>
struct IEnumerator_1_t4242;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_34MethodDeclarations.h"
#define Dictionary_2__ctor_m4571(__this, method) (( void (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2__ctor_m16394_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21423(__this, ___comparer, method) (( void (*) (Dictionary_2_t716 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16396_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m21424(__this, ___dictionary, method) (( void (*) (Dictionary_2_t716 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16398_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor(System.Int32)
#define Dictionary_2__ctor_m21425(__this, ___capacity, method) (( void (*) (Dictionary_2_t716 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16400_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21426(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t716 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16402_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m21427(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t716 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m16404_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21428(__this, method) (( Object_t* (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16406_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21429(__this, method) (( Object_t* (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16408_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21430(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16410_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21431(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t716 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16412_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m21432(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t716 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16414_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m21433(__this, ___key, method) (( bool (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16416_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m21434(__this, ___key, method) (( void (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16418_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21435(__this, method) (( bool (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16420_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21436(__this, method) (( Object_t * (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16422_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21437(__this, method) (( bool (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21438(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t716 *, KeyValuePair_2_t860 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16426_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21439(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t716 *, KeyValuePair_2_t860 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16428_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21440(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t716 *, KeyValuePair_2U5BU5D_t4241*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16430_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21441(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t716 *, KeyValuePair_2_t860 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16432_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21442(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t716 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16434_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21443(__this, method) (( Object_t * (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16436_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21444(__this, method) (( Object_t* (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16438_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21445(__this, method) (( Object_t * (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::get_Count()
#define Dictionary_2_get_Count_m21446(__this, method) (( int32_t (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_get_Count_m16442_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::get_Item(TKey)
#define Dictionary_2_get_Item_m21447(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t716 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16444_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m21448(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t716 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m16446_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m21449(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t716 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16448_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m21450(__this, ___size, method) (( void (*) (Dictionary_2_t716 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16450_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m21451(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t716 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16452_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m21452(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t860  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m16454_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m21453(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m16456_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m21454(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m16458_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m21455(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t716 *, KeyValuePair_2U5BU5D_t4241*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16460_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::Resize()
#define Dictionary_2_Resize_m21456(__this, method) (( void (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_Resize_m16462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::Add(TKey,TValue)
#define Dictionary_2_Add_m21457(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t716 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m16464_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::Clear()
#define Dictionary_2_Clear_m21458(__this, method) (( void (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_Clear_m16466_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m21459(__this, ___key, method) (( bool (*) (Dictionary_2_t716 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16468_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m21460(__this, ___value, method) (( bool (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m16470_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m21461(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t716 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m16472_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m21462(__this, ___sender, method) (( void (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16474_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::Remove(TKey)
#define Dictionary_2_Remove_m21463(__this, ___key, method) (( bool (*) (Dictionary_2_t716 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16476_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m21464(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t716 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m16478_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::get_Keys()
#define Dictionary_2_get_Keys_m4566(__this, method) (( KeyCollection_t870 * (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_get_Keys_m16480_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::get_Values()
#define Dictionary_2_get_Values_m4534(__this, method) (( ValueCollection_t853 * (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_get_Values_m16481_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m21465(__this, ___key, method) (( int32_t (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16483_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m21466(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t716 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16485_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m21467(__this, ___pair, method) (( bool (*) (Dictionary_2_t716 *, KeyValuePair_2_t860 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16487_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m4547(__this, method) (( Enumerator_t862  (*) (Dictionary_2_t716 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16488_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m21468(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16490_gshared)(__this /* static, unused */, ___key, ___value, method)
