﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.StringPluginExtensions
struct StringPluginExtensions_t998;
// System.Char[]
struct CharU5BU5D_t119;
// System.Text.StringBuilder
struct StringBuilder_t429;

// System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern "C" void StringPluginExtensions__cctor_m5486 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern "C" void StringPluginExtensions_ScrambleChars_m5487 (Object_t * __this /* static, unused */, CharU5BU5D_t119* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern "C" StringBuilder_t429 * StringPluginExtensions_AppendScrambledChars_m5488 (Object_t * __this /* static, unused */, StringBuilder_t429 * ___buffer, int32_t ___length, CharU5BU5D_t119* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
