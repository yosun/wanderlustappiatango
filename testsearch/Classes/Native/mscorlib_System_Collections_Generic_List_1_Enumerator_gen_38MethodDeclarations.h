﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>
struct Enumerator_t3302;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t285;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t287;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m17552(__this, ___l, method) (( void (*) (Enumerator_t3302 *, List_1_t287 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17553(__this, method) (( Object_t * (*) (Enumerator_t3302 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::Dispose()
#define Enumerator_Dispose_m17554(__this, method) (( void (*) (Enumerator_t3302 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::VerifyState()
#define Enumerator_VerifyState_m17555(__this, method) (( void (*) (Enumerator_t3302 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::MoveNext()
#define Enumerator_MoveNext_m17556(__this, method) (( bool (*) (Enumerator_t3302 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::get_Current()
#define Enumerator_get_Current_m17557(__this, method) (( Graphic_t285 * (*) (Enumerator_t3302 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
