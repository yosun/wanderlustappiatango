﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>
struct DefaultComparer_t3630;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void DefaultComparer__ctor_m22748_gshared (DefaultComparer_t3630 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22748(__this, method) (( void (*) (DefaultComparer_t3630 *, const MethodInfo*))DefaultComparer__ctor_m22748_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22749_gshared (DefaultComparer_t3630 * __this, ProfileData_t740  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22749(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3630 *, ProfileData_t740 , const MethodInfo*))DefaultComparer_GetHashCode_m22749_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22750_gshared (DefaultComparer_t3630 * __this, ProfileData_t740  ___x, ProfileData_t740  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22750(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3630 *, ProfileData_t740 , ProfileData_t740 , const MethodInfo*))DefaultComparer_Equals_m22750_gshared)(__this, ___x, ___y, method)
