﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t2290;

// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern "C" void DispIdAttribute__ctor_m12082 (DispIdAttribute_t2290 * __this, int32_t ___dispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
