﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t1063;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24133_gshared (DOSetter_1_t1063 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m24133(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1063 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m24133_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24134_gshared (DOSetter_1_t1063 * __this, int64_t ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m24134(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1063 *, int64_t, const MethodInfo*))DOSetter_1_Invoke_m24134_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m24135_gshared (DOSetter_1_t1063 * __this, int64_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m24135(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1063 *, int64_t, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m24135_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24136_gshared (DOSetter_1_t1063 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m24136(__this, ___result, method) (( void (*) (DOSetter_1_t1063 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m24136_gshared)(__this, ___result, method)
