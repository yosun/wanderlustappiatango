﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t3628;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3616;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22737_gshared (ShimEnumerator_t3628 * __this, Dictionary_2_t3616 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22737(__this, ___host, method) (( void (*) (ShimEnumerator_t3628 *, Dictionary_2_t3616 *, const MethodInfo*))ShimEnumerator__ctor_m22737_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22738_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22738(__this, method) (( bool (*) (ShimEnumerator_t3628 *, const MethodInfo*))ShimEnumerator_MoveNext_m22738_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m22739_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22739(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3628 *, const MethodInfo*))ShimEnumerator_get_Entry_m22739_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22740_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22740(__this, method) (( Object_t * (*) (ShimEnumerator_t3628 *, const MethodInfo*))ShimEnumerator_get_Key_m22740_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22741_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22741(__this, method) (( Object_t * (*) (ShimEnumerator_t3628 *, const MethodInfo*))ShimEnumerator_get_Value_m22741_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22742_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22742(__this, method) (( Object_t * (*) (ShimEnumerator_t3628 *, const MethodInfo*))ShimEnumerator_get_Current_m22742_gshared)(__this, method)
