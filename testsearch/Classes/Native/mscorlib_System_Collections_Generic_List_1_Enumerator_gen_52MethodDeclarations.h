﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>
struct Enumerator_t3691;
// System.Object
struct Object_t;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t953;

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23718(__this, ___l, method) (( void (*) (Enumerator_t3691 *, List_1_t953 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23719(__this, method) (( Object_t * (*) (Enumerator_t3691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::Dispose()
#define Enumerator_Dispose_m23720(__this, method) (( void (*) (Enumerator_t3691 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::VerifyState()
#define Enumerator_VerifyState_m23721(__this, method) (( void (*) (Enumerator_t3691 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::MoveNext()
#define Enumerator_MoveNext_m23722(__this, method) (( bool (*) (Enumerator_t3691 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::get_Current()
#define Enumerator_get_Current_m23723(__this, method) (( ABSSequentiable_t949 * (*) (Enumerator_t3691 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
