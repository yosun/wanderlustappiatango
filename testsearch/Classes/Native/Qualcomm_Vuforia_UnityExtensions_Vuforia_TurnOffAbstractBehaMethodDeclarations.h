﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t85;

// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern "C" void TurnOffAbstractBehaviour__ctor_m497 (TurnOffAbstractBehaviour_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
