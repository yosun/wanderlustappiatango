﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
struct Enumerator_t815;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t584;
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t630;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19595(__this, ___l, method) (( void (*) (Enumerator_t815 *, List_1_t630 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19596(__this, method) (( Object_t * (*) (Enumerator_t815 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::Dispose()
#define Enumerator_Dispose_m19597(__this, method) (( void (*) (Enumerator_t815 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::VerifyState()
#define Enumerator_VerifyState_m19598(__this, method) (( void (*) (Enumerator_t815 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::MoveNext()
#define Enumerator_MoveNext_m4409(__this, method) (( bool (*) (Enumerator_t815 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::get_Current()
#define Enumerator_get_Current_m4408(__this, method) (( DataSetImpl_t584 * (*) (Enumerator_t815 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
