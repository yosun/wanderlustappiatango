﻿#pragma once
#include <stdint.h>
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct  List_1_t615  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::_items
	PIXEL_FORMATU5BU5D_t3395* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::_version
	int32_t ____version_3;
};
struct List_1_t615_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::EmptyArray
	PIXEL_FORMATU5BU5D_t3395* ___EmptyArray_4;
};
