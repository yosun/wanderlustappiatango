﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.CodeDom.Compiler.GeneratedCodeAttribute
struct  GeneratedCodeAttribute_t1454  : public Attribute_t146
{
	// System.String System.CodeDom.Compiler.GeneratedCodeAttribute::tool
	String_t* ___tool_0;
	// System.String System.CodeDom.Compiler.GeneratedCodeAttribute::version
	String_t* ___version_1;
};
