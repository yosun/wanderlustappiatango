﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct InternalEnumerator_1_t3639;
// System.Object
struct Object_t;
// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t90;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22899(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3639 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22900(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3639 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m22901(__this, method) (( void (*) (InternalEnumerator_1_t3639 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22902(__this, method) (( bool (*) (InternalEnumerator_1_t3639 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m22903(__this, method) (( VideoBackgroundAbstractBehaviour_t90 * (*) (InternalEnumerator_1_t3639 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
