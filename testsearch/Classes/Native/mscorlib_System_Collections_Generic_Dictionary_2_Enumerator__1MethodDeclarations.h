﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>
struct Enumerator_t862;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t105;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t716;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m21506(__this, ___dictionary, method) (( void (*) (Enumerator_t862 *, Dictionary_2_t716 *, const MethodInfo*))Enumerator__ctor_m16521_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21507(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21508(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21509(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21510(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m4550(__this, method) (( bool (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_MoveNext_m16526_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m4548(__this, method) (( KeyValuePair_2_t860  (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_get_Current_m16527_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21511(__this, method) (( int32_t (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_get_CurrentKey_m16528_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21512(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_get_CurrentValue_m16529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::VerifyState()
#define Enumerator_VerifyState_m21513(__this, method) (( void (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_VerifyState_m16530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21514(__this, method) (( void (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_VerifyCurrent_m16531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m21515(__this, method) (( void (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_Dispose_m16532_gshared)(__this, method)
