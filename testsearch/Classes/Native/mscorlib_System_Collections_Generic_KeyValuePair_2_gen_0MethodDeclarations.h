﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct KeyValuePair_2_t847;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20724(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t847 *, int32_t, WordAbstractBehaviour_t101 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m4509(__this, method) (( int32_t (*) (KeyValuePair_2_t847 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20725(__this, ___value, method) (( void (*) (KeyValuePair_2_t847 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m4510(__this, method) (( WordAbstractBehaviour_t101 * (*) (KeyValuePair_2_t847 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20726(__this, ___value, method) (( void (*) (KeyValuePair_2_t847 *, WordAbstractBehaviour_t101 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m20727(__this, method) (( String_t* (*) (KeyValuePair_2_t847 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
