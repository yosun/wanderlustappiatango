﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction
struct UnityAction_t282;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1346  : public BaseInvokableCall_t1345
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t282 * ___Delegate_0;
};
