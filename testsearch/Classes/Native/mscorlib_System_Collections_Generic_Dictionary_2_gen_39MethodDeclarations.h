﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3827;
// System.Collections.Generic.ICollection`1<System.UInt64>
struct ICollection_1_t4374;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt64,System.Object>
struct KeyCollection_t3831;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt64,System.Object>
struct ValueCollection_t3835;
// System.Collections.Generic.IEqualityComparer`1<System.UInt64>
struct IEqualityComparer_1_t3825;
// System.Collections.Generic.IDictionary`2<System.UInt64,System.Object>
struct IDictionary_2_t4375;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
struct KeyValuePair_2U5BU5D_t4376;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>
struct IEnumerator_1_t4377;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_37.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__35.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m25711_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m25711(__this, method) (( void (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2__ctor_m25711_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25713_gshared (Dictionary_2_t3827 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m25713(__this, ___comparer, method) (( void (*) (Dictionary_2_t3827 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25713_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m25715_gshared (Dictionary_2_t3827 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m25715(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3827 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25715_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m25717_gshared (Dictionary_2_t3827 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m25717(__this, ___capacity, method) (( void (*) (Dictionary_2_t3827 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m25717_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25719_gshared (Dictionary_2_t3827 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m25719(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3827 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25719_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m25721_gshared (Dictionary_2_t3827 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m25721(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3827 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m25721_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25723_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25723(__this, method) (( Object_t* (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25723_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25725_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25725(__this, method) (( Object_t* (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m25727_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25727(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25727_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25729_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25729(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3827 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25729_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25731_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m25731(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3827 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25731_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m25733_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m25733(__this, ___key, method) (( bool (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m25733_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25735_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m25735(__this, ___key, method) (( void (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25735_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25737_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25737(__this, method) (( bool (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25737_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25739_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25739(__this, method) (( Object_t * (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25741_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25741(__this, method) (( bool (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25741_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25743_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2_t3828  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25743(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3827 *, KeyValuePair_2_t3828 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25743_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25745_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2_t3828  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25745(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3827 *, KeyValuePair_2_t3828 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25745_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25747_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2U5BU5D_t4376* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25747(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3827 *, KeyValuePair_2U5BU5D_t4376*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25747_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25749_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2_t3828  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25749(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3827 *, KeyValuePair_2_t3828 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25749_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25751_gshared (Dictionary_2_t3827 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25751(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3827 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25751_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25753_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25753(__this, method) (( Object_t * (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25753_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25755_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25755(__this, method) (( Object_t* (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25755_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25757_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25757(__this, method) (( Object_t * (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25757_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m25759_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m25759(__this, method) (( int32_t (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_get_Count_m25759_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m25761_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m25761(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3827 *, uint64_t, const MethodInfo*))Dictionary_2_get_Item_m25761_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m25763_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m25763(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3827 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m25763_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m25765_gshared (Dictionary_2_t3827 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m25765(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3827 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m25765_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m25767_gshared (Dictionary_2_t3827 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m25767(__this, ___size, method) (( void (*) (Dictionary_2_t3827 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m25767_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m25769_gshared (Dictionary_2_t3827 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m25769(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3827 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m25769_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3828  Dictionary_2_make_pair_m25771_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m25771(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3828  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m25771_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::pick_key(TKey,TValue)
extern "C" uint64_t Dictionary_2_pick_key_m25773_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m25773(__this /* static, unused */, ___key, ___value, method) (( uint64_t (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m25773_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m25775_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m25775(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m25775_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m25777_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2U5BU5D_t4376* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m25777(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3827 *, KeyValuePair_2U5BU5D_t4376*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m25777_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m25779_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m25779(__this, method) (( void (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_Resize_m25779_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m25781_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m25781(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3827 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m25781_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m25783_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m25783(__this, method) (( void (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_Clear_m25783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m25785_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m25785(__this, ___key, method) (( bool (*) (Dictionary_2_t3827 *, uint64_t, const MethodInfo*))Dictionary_2_ContainsKey_m25785_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m25787_gshared (Dictionary_2_t3827 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m25787(__this, ___value, method) (( bool (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m25787_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m25789_gshared (Dictionary_2_t3827 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m25789(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3827 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m25789_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m25791_gshared (Dictionary_2_t3827 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m25791(__this, ___sender, method) (( void (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m25791_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m25793_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m25793(__this, ___key, method) (( bool (*) (Dictionary_2_t3827 *, uint64_t, const MethodInfo*))Dictionary_2_Remove_m25793_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m25795_gshared (Dictionary_2_t3827 * __this, uint64_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m25795(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3827 *, uint64_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m25795_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Keys()
extern "C" KeyCollection_t3831 * Dictionary_2_get_Keys_m25797_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m25797(__this, method) (( KeyCollection_t3831 * (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_get_Keys_m25797_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Values()
extern "C" ValueCollection_t3835 * Dictionary_2_get_Values_m25799_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m25799(__this, method) (( ValueCollection_t3835 * (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_get_Values_m25799_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ToTKey(System.Object)
extern "C" uint64_t Dictionary_2_ToTKey_m25801_gshared (Dictionary_2_t3827 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m25801(__this, ___key, method) (( uint64_t (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m25801_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m25803_gshared (Dictionary_2_t3827 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m25803(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t3827 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m25803_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m25805_gshared (Dictionary_2_t3827 * __this, KeyValuePair_2_t3828  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m25805(__this, ___pair, method) (( bool (*) (Dictionary_2_t3827 *, KeyValuePair_2_t3828 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25805_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::GetEnumerator()
extern "C" Enumerator_t3833  Dictionary_2_GetEnumerator_m25807_gshared (Dictionary_2_t3827 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m25807(__this, method) (( Enumerator_t3833  (*) (Dictionary_2_t3827 *, const MethodInfo*))Dictionary_2_GetEnumerator_m25807_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m25809_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m25809(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25809_gshared)(__this /* static, unused */, ___key, ___value, method)
