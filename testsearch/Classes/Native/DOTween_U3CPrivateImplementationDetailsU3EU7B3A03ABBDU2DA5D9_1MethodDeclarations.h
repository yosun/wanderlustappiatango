﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
struct __StaticArrayInitTypeSizeU3D20_t1021;
struct __StaticArrayInitTypeSizeU3D20_t1021_marshaled;

void __StaticArrayInitTypeSizeU3D20_t1021_marshal(const __StaticArrayInitTypeSizeU3D20_t1021& unmarshaled, __StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled);
void __StaticArrayInitTypeSizeU3D20_t1021_marshal_back(const __StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled, __StaticArrayInitTypeSizeU3D20_t1021& unmarshaled);
void __StaticArrayInitTypeSizeU3D20_t1021_marshal_cleanup(__StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled);
