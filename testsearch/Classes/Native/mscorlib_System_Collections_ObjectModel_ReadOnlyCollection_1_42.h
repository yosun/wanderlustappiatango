﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ITrackerEventHandler>
struct IList_1_t3641;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackerEventHandler>
struct  ReadOnlyCollection_1_t3642  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackerEventHandler>::list
	Object_t* ___list_0;
};
