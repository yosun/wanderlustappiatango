﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t375;

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m1822 (HorizontalOrVerticalLayoutGroup_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m1823 (HorizontalOrVerticalLayoutGroup_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m1824 (HorizontalOrVerticalLayoutGroup_t375 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825 (HorizontalOrVerticalLayoutGroup_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826 (HorizontalOrVerticalLayoutGroup_t375 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827 (HorizontalOrVerticalLayoutGroup_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828 (HorizontalOrVerticalLayoutGroup_t375 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829 (HorizontalOrVerticalLayoutGroup_t375 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830 (HorizontalOrVerticalLayoutGroup_t375 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
