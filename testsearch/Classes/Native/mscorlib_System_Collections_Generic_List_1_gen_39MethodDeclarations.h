﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t729;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t790;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t802;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t4265;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t3600;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3596;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3604;
// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3607;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_49.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void List_1__ctor_m4596_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1__ctor_m4596(__this, method) (( void (*) (List_1_t729 *, const MethodInfo*))List_1__ctor_m4596_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22302_gshared (List_1_t729 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22302(__this, ___collection, method) (( void (*) (List_1_t729 *, Object_t*, const MethodInfo*))List_1__ctor_m22302_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22303_gshared (List_1_t729 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22303(__this, ___capacity, method) (( void (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1__ctor_m22303_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void List_1__cctor_m22304_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22304(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22304_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22305_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22305(__this, method) (( Object_t* (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22305_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22306_gshared (List_1_t729 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22306(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t729 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22306_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22307_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22307(__this, method) (( Object_t * (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22307_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22308_gshared (List_1_t729 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22308(__this, ___item, method) (( int32_t (*) (List_1_t729 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22308_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22309_gshared (List_1_t729 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22309(__this, ___item, method) (( bool (*) (List_1_t729 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22309_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22310_gshared (List_1_t729 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22310(__this, ___item, method) (( int32_t (*) (List_1_t729 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22310_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22311_gshared (List_1_t729 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22311(__this, ___index, ___item, method) (( void (*) (List_1_t729 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22311_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22312_gshared (List_1_t729 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22312(__this, ___item, method) (( void (*) (List_1_t729 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22312_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22313_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22313(__this, method) (( bool (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22313_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22314_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22314(__this, method) (( bool (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22314_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22315_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22315(__this, method) (( Object_t * (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22315_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22316_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22316(__this, method) (( bool (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22317_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22317(__this, method) (( bool (*) (List_1_t729 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22317_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22318_gshared (List_1_t729 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22318(__this, ___index, method) (( Object_t * (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22318_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22319_gshared (List_1_t729 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22319(__this, ___index, ___value, method) (( void (*) (List_1_t729 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22319_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void List_1_Add_m22320_gshared (List_1_t729 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define List_1_Add_m22320(__this, ___item, method) (( void (*) (List_1_t729 *, TargetSearchResult_t726 , const MethodInfo*))List_1_Add_m22320_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22321_gshared (List_1_t729 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22321(__this, ___newCount, method) (( void (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22321_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22322_gshared (List_1_t729 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22322(__this, ___collection, method) (( void (*) (List_1_t729 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22322_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22323_gshared (List_1_t729 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22323(__this, ___enumerable, method) (( void (*) (List_1_t729 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22323_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m22324_gshared (List_1_t729 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m22324(__this, ___collection, method) (( void (*) (List_1_t729 *, Object_t*, const MethodInfo*))List_1_AddRange_m22324_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3600 * List_1_AsReadOnly_m22325_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22325(__this, method) (( ReadOnlyCollection_1_t3600 * (*) (List_1_t729 *, const MethodInfo*))List_1_AsReadOnly_m22325_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void List_1_Clear_m22326_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_Clear_m22326(__this, method) (( void (*) (List_1_t729 *, const MethodInfo*))List_1_Clear_m22326_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool List_1_Contains_m22327_gshared (List_1_t729 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define List_1_Contains_m22327(__this, ___item, method) (( bool (*) (List_1_t729 *, TargetSearchResult_t726 , const MethodInfo*))List_1_Contains_m22327_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22328_gshared (List_1_t729 * __this, TargetSearchResultU5BU5D_t3596* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22328(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t729 *, TargetSearchResultU5BU5D_t3596*, int32_t, const MethodInfo*))List_1_CopyTo_m22328_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Find(System.Predicate`1<T>)
extern "C" TargetSearchResult_t726  List_1_Find_m22329_gshared (List_1_t729 * __this, Predicate_1_t3604 * ___match, const MethodInfo* method);
#define List_1_Find_m22329(__this, ___match, method) (( TargetSearchResult_t726  (*) (List_1_t729 *, Predicate_1_t3604 *, const MethodInfo*))List_1_Find_m22329_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22330_gshared (Object_t * __this /* static, unused */, Predicate_1_t3604 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22330(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3604 *, const MethodInfo*))List_1_CheckMatch_m22330_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22331_gshared (List_1_t729 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3604 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22331(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t729 *, int32_t, int32_t, Predicate_1_t3604 *, const MethodInfo*))List_1_GetIndex_m22331_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Enumerator_t3598  List_1_GetEnumerator_m22332_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22332(__this, method) (( Enumerator_t3598  (*) (List_1_t729 *, const MethodInfo*))List_1_GetEnumerator_m22332_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22333_gshared (List_1_t729 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22333(__this, ___item, method) (( int32_t (*) (List_1_t729 *, TargetSearchResult_t726 , const MethodInfo*))List_1_IndexOf_m22333_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22334_gshared (List_1_t729 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22334(__this, ___start, ___delta, method) (( void (*) (List_1_t729 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22334_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22335_gshared (List_1_t729 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22335(__this, ___index, method) (( void (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22335_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22336_gshared (List_1_t729 * __this, int32_t ___index, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define List_1_Insert_m22336(__this, ___index, ___item, method) (( void (*) (List_1_t729 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))List_1_Insert_m22336_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22337_gshared (List_1_t729 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22337(__this, ___collection, method) (( void (*) (List_1_t729 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22337_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool List_1_Remove_m22338_gshared (List_1_t729 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define List_1_Remove_m22338(__this, ___item, method) (( bool (*) (List_1_t729 *, TargetSearchResult_t726 , const MethodInfo*))List_1_Remove_m22338_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22339_gshared (List_1_t729 * __this, Predicate_1_t3604 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22339(__this, ___match, method) (( int32_t (*) (List_1_t729 *, Predicate_1_t3604 *, const MethodInfo*))List_1_RemoveAll_m22339_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22340_gshared (List_1_t729 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22340(__this, ___index, method) (( void (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22340_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Reverse()
extern "C" void List_1_Reverse_m22341_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_Reverse_m22341(__this, method) (( void (*) (List_1_t729 *, const MethodInfo*))List_1_Reverse_m22341_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort()
extern "C" void List_1_Sort_m22342_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_Sort_m22342(__this, method) (( void (*) (List_1_t729 *, const MethodInfo*))List_1_Sort_m22342_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22343_gshared (List_1_t729 * __this, Comparison_1_t3607 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22343(__this, ___comparison, method) (( void (*) (List_1_t729 *, Comparison_1_t3607 *, const MethodInfo*))List_1_Sort_m22343_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C" TargetSearchResultU5BU5D_t3596* List_1_ToArray_m22344_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_ToArray_m22344(__this, method) (( TargetSearchResultU5BU5D_t3596* (*) (List_1_t729 *, const MethodInfo*))List_1_ToArray_m22344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m22345_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22345(__this, method) (( void (*) (List_1_t729 *, const MethodInfo*))List_1_TrimExcess_m22345_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22346_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22346(__this, method) (( int32_t (*) (List_1_t729 *, const MethodInfo*))List_1_get_Capacity_m22346_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22347_gshared (List_1_t729 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22347(__this, ___value, method) (( void (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22347_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t List_1_get_Count_m22348_gshared (List_1_t729 * __this, const MethodInfo* method);
#define List_1_get_Count_m22348(__this, method) (( int32_t (*) (List_1_t729 *, const MethodInfo*))List_1_get_Count_m22348_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t726  List_1_get_Item_m22349_gshared (List_1_t729 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22349(__this, ___index, method) (( TargetSearchResult_t726  (*) (List_1_t729 *, int32_t, const MethodInfo*))List_1_get_Item_m22349_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22350_gshared (List_1_t729 * __this, int32_t ___index, TargetSearchResult_t726  ___value, const MethodInfo* method);
#define List_1_set_Item_m22350(__this, ___index, ___value, method) (( void (*) (List_1_t729 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))List_1_set_Item_m22350_gshared)(__this, ___index, ___value, method)
