﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.UInt16>
struct GenericComparer_1_t3707;

// System.Void System.Collections.Generic.GenericComparer`1<System.UInt16>::.ctor()
extern "C" void GenericComparer_1__ctor_m24029_gshared (GenericComparer_1_t3707 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m24029(__this, method) (( void (*) (GenericComparer_1_t3707 *, const MethodInfo*))GenericComparer_1__ctor_m24029_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt16>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m24030_gshared (GenericComparer_1_t3707 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m24030(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t3707 *, uint16_t, uint16_t, const MethodInfo*))GenericComparer_1_Compare_m24030_gshared)(__this, ___x, ___y, method)
