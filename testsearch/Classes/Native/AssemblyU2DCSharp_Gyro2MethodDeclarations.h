﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Gyro2
struct Gyro2_t21;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Gyro2::.ctor()
extern "C" void Gyro2__ctor_m25 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::.cctor()
extern "C" void Gyro2__cctor_m26 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Awake()
extern "C" void Gyro2_Awake_m27 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Start()
extern "C" void Gyro2_Start_m28 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern "C" Vector3_t14  Gyro2_ClampPosInRoom_m29 (Object_t * __this /* static, unused */, Vector3_t14  ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void Gyro2_StationParent_m30 (Object_t * __this /* static, unused */, Vector3_t14  ___pos, Quaternion_t22  ___rot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern "C" void Gyro2_StationParentShiftAngle_m31 (Gyro2_t21 * __this, float ___angle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Init(UnityEngine.Transform)
extern "C" void Gyro2_Init_m32 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::ResetStatic()
extern "C" void Gyro2_ResetStatic_m33 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::ResetStaticSmoothly()
extern "C" void Gyro2_ResetStaticSmoothly_m34 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Reset()
extern "C" void Gyro2_Reset_m35 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern "C" void Gyro2_DoSwipeRotation_m36 (Gyro2_t21 * __this, float ___degree, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::DisableSettingsButotn()
extern "C" void Gyro2_DisableSettingsButotn_m37 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Update()
extern "C" void Gyro2_Update_m38 (Gyro2_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::AssignCalibration()
extern "C" void Gyro2_AssignCalibration_m39 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::AssignCamBaseRotation()
extern "C" void Gyro2_AssignCamBaseRotation_m40 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Gyro2::G()
extern "C" Quaternion_t22  Gyro2_G_m41 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t22  Gyro2_ConvertRotation_m42 (Object_t * __this /* static, unused */, Quaternion_t22  ___q, const MethodInfo* method) IL2CPP_METHOD_ATTR;
