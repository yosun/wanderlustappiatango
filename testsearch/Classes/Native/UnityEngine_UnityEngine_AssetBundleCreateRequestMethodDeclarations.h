﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1139;
// UnityEngine.AssetBundle
struct AssetBundle_t1141;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m5665 (AssetBundleCreateRequest_t1139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t1141 * AssetBundleCreateRequest_get_assetBundle_m5666 (AssetBundleCreateRequest_t1139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m5667 (AssetBundleCreateRequest_t1139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
