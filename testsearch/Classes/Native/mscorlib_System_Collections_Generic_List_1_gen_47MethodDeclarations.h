﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t952;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t940;
// System.Collections.Generic.IEnumerable`1<DG.Tweening.Tween>
struct IEnumerable_1_t4306;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.Tween>
struct IEnumerator_1_t4307;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<DG.Tweening.Tween>
struct ICollection_1_t4308;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>
struct ReadOnlyCollection_1_t3683;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t984;
// System.Predicate`1<DG.Tweening.Tween>
struct Predicate_1_t3684;
// System.Comparison`1<DG.Tweening.Tween>
struct Comparison_1_t3686;
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"

// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m5531(__this, method) (( void (*) (List_1_t952 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23546(__this, ___collection, method) (( void (*) (List_1_t952 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.ctor(System.Int32)
#define List_1__ctor_m5560(__this, ___capacity, method) (( void (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.cctor()
#define List_1__cctor_m23547(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23548(__this, method) (( Object_t* (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23549(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t952 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23550(__this, method) (( Object_t * (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23551(__this, ___item, method) (( int32_t (*) (List_1_t952 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23552(__this, ___item, method) (( bool (*) (List_1_t952 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23553(__this, ___item, method) (( int32_t (*) (List_1_t952 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23554(__this, ___index, ___item, method) (( void (*) (List_1_t952 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23555(__this, ___item, method) (( void (*) (List_1_t952 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23556(__this, method) (( bool (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23557(__this, method) (( bool (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23558(__this, method) (( Object_t * (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23559(__this, method) (( bool (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23560(__this, method) (( bool (*) (List_1_t952 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23561(__this, ___index, method) (( Object_t * (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23562(__this, ___index, ___value, method) (( void (*) (List_1_t952 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Add(T)
#define List_1_Add_m23563(__this, ___item, method) (( void (*) (List_1_t952 *, Tween_t940 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23564(__this, ___newCount, method) (( void (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23565(__this, ___collection, method) (( void (*) (List_1_t952 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23566(__this, ___enumerable, method) (( void (*) (List_1_t952 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23567(__this, ___collection, method) (( void (*) (List_1_t952 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<DG.Tweening.Tween>::AsReadOnly()
#define List_1_AsReadOnly_m23568(__this, method) (( ReadOnlyCollection_1_t3683 * (*) (List_1_t952 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Clear()
#define List_1_Clear_m23569(__this, method) (( void (*) (List_1_t952 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::Contains(T)
#define List_1_Contains_m23570(__this, ___item, method) (( bool (*) (List_1_t952 *, Tween_t940 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23571(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t952 *, TweenU5BU5D_t984*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<DG.Tweening.Tween>::Find(System.Predicate`1<T>)
#define List_1_Find_m23572(__this, ___match, method) (( Tween_t940 * (*) (List_1_t952 *, Predicate_1_t3684 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23573(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3684 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23574(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t952 *, int32_t, int32_t, Predicate_1_t3684 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<DG.Tweening.Tween>::GetEnumerator()
#define List_1_GetEnumerator_m23575(__this, method) (( Enumerator_t3685  (*) (List_1_t952 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::IndexOf(T)
#define List_1_IndexOf_m23576(__this, ___item, method) (( int32_t (*) (List_1_t952 *, Tween_t940 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23577(__this, ___start, ___delta, method) (( void (*) (List_1_t952 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23578(__this, ___index, method) (( void (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Insert(System.Int32,T)
#define List_1_Insert_m23579(__this, ___index, ___item, method) (( void (*) (List_1_t952 *, int32_t, Tween_t940 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23580(__this, ___collection, method) (( void (*) (List_1_t952 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Tween>::Remove(T)
#define List_1_Remove_m23581(__this, ___item, method) (( bool (*) (List_1_t952 *, Tween_t940 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23582(__this, ___match, method) (( int32_t (*) (List_1_t952 *, Predicate_1_t3684 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23583(__this, ___index, method) (( void (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Reverse()
#define List_1_Reverse_m23584(__this, method) (( void (*) (List_1_t952 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Sort()
#define List_1_Sort_m23585(__this, method) (( void (*) (List_1_t952 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23586(__this, ___comparison, method) (( void (*) (List_1_t952 *, Comparison_1_t3686 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<DG.Tweening.Tween>::ToArray()
#define List_1_ToArray_m23587(__this, method) (( TweenU5BU5D_t984* (*) (List_1_t952 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::TrimExcess()
#define List_1_TrimExcess_m23588(__this, method) (( void (*) (List_1_t952 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Capacity()
#define List_1_get_Capacity_m5558(__this, method) (( int32_t (*) (List_1_t952 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m5557(__this, ___value, method) (( void (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count()
#define List_1_get_Count_m23589(__this, method) (( int32_t (*) (List_1_t952 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32)
#define List_1_get_Item_m23590(__this, ___index, method) (( Tween_t940 * (*) (List_1_t952 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::set_Item(System.Int32,T)
#define List_1_set_Item_m23591(__this, ___index, ___value, method) (( void (*) (List_1_t952 *, int32_t, Tween_t940 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
