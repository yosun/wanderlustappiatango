﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit
struct RaycastHit_t102;
// UnityEngine.Collider
struct Collider_t164;
// UnityEngine.Rigidbody
struct Rigidbody_t1229;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t14  RaycastHit_get_point_m268 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern "C" void RaycastHit_set_point_m349 (RaycastHit_t102 * __this, Vector3_t14  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t14  RaycastHit_get_normal_m2078 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m2077 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t164 * RaycastHit_get_collider_m2076 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t1229 * RaycastHit_get_rigidbody_m6376 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t11 * RaycastHit_get_transform_m270 (RaycastHit_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
