﻿#pragma once
#include <stdint.h>
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t645;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.PremiumObjectFactory
struct  PremiumObjectFactory_t646  : public Object_t
{
};
struct PremiumObjectFactory_t646_StaticFields{
	// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::sInstance
	Object_t * ___sInstance_0;
};
