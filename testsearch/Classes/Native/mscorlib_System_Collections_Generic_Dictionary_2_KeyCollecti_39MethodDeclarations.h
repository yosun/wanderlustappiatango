﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>
struct KeyCollection_t3745;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1190;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t4042;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.String[]
struct StringU5BU5D_t15;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_79.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5MethodDeclarations.h"
#define KeyCollection__ctor_m24550(__this, ___dictionary, method) (( void (*) (KeyCollection_t3745 *, Dictionary_2_t1190 *, const MethodInfo*))KeyCollection__ctor_m15021_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24551(__this, ___item, method) (( void (*) (KeyCollection_t3745 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15022_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24552(__this, method) (( void (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24553(__this, ___item, method) (( bool (*) (KeyCollection_t3745 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15024_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24554(__this, ___item, method) (( bool (*) (KeyCollection_t3745 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15025_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24555(__this, method) (( Object_t* (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m24556(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3745 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15027_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24557(__this, method) (( Object_t * (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15028_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24558(__this, method) (( bool (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24559(__this, method) (( bool (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15030_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m24560(__this, method) (( Object_t * (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m24561(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3745 *, StringU5BU5D_t15*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15032_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::GetEnumerator()
#define KeyCollection_GetEnumerator_m24562(__this, method) (( Enumerator_t4331  (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_GetEnumerator_m15033_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>::get_Count()
#define KeyCollection_get_Count_m24563(__this, method) (( int32_t (*) (KeyCollection_t3745 *, const MethodInfo*))KeyCollection_get_Count_m15034_gshared)(__this, method)
