﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t95;

// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m236 (WebCamBehaviour_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
