﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Tracker
struct Tracker_t629;

// System.Boolean Vuforia.Tracker::Start()
// System.Void Vuforia.Tracker::Stop()
// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C" bool Tracker_get_IsActive_m2965 (Tracker_t629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C" void Tracker_set_IsActive_m2966 (Tracker_t629 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::.ctor()
extern "C" void Tracker__ctor_m2967 (Tracker_t629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
