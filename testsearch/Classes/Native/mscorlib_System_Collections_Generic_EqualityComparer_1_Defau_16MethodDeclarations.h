﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t4031;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m27844_gshared (DefaultComparer_t4031 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27844(__this, method) (( void (*) (DefaultComparer_t4031 *, const MethodInfo*))DefaultComparer__ctor_m27844_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m27845_gshared (DefaultComparer_t4031 * __this, Guid_t118  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m27845(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4031 *, Guid_t118 , const MethodInfo*))DefaultComparer_GetHashCode_m27845_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m27846_gshared (DefaultComparer_t4031 * __this, Guid_t118  ___x, Guid_t118  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m27846(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4031 *, Guid_t118 , Guid_t118 , const MethodInfo*))DefaultComparer_Equals_m27846_gshared)(__this, ___x, ___y, method)
