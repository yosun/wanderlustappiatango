﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t3290;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct  Comparer_1_t3290  : public Object_t
{
};
struct Comparer_1_t3290_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::_default
	Comparer_1_t3290 * ____default_0;
};
