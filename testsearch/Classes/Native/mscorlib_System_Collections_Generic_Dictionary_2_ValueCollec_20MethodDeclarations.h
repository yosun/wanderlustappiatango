﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>
struct Enumerator_t878;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t720;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define Enumerator__ctor_m22299(__this, ___host, method) (( void (*) (Enumerator_t878 *, Dictionary_2_t720 *, const MethodInfo*))Enumerator__ctor_m16551_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22300(__this, method) (( Object_t * (*) (Enumerator_t878 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m22301(__this, method) (( void (*) (Enumerator_t878 *, const MethodInfo*))Enumerator_Dispose_m16553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4587(__this, method) (( bool (*) (Enumerator_t878 *, const MethodInfo*))Enumerator_MoveNext_m16554_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m4586(__this, method) (( TrackableBehaviour_t52 * (*) (Enumerator_t878 *, const MethodInfo*))Enumerator_get_Current_m16555_gshared)(__this, method)
