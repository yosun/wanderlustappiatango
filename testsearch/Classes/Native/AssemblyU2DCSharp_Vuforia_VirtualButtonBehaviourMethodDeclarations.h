﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t93;

// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m235 (VirtualButtonBehaviour_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
