﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t2112;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Security.Cryptography.RSA
struct RSA_t1697;
// System.Security.Cryptography.DSA
struct DSA_t1703;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m10622 (PrivateKeyInfo_t2112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m10623 (PrivateKeyInfo_t2112 * __this, ByteU5BU5D_t622* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t622* PrivateKeyInfo_get_PrivateKey_m10624 (PrivateKeyInfo_t2112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m10625 (PrivateKeyInfo_t2112 * __this, ByteU5BU5D_t622* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t622* PrivateKeyInfo_RemoveLeadingZero_m10626 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___bigInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t622* PrivateKeyInfo_Normalize_m10627 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___bigInt, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t1697 * PrivateKeyInfo_DecodeRSA_m10628 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___keypair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t1703 * PrivateKeyInfo_DecodeDSA_m10629 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___privateKey, DSAParameters_t1807  ___dsaParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
