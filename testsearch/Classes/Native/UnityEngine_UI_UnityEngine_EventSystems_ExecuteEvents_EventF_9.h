﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t404;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t204;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct  EventFunction_1_t221  : public MulticastDelegate_t314
{
};
