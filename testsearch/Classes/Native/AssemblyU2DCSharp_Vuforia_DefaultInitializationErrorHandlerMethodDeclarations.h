﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t47;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern "C" void DefaultInitializationErrorHandler__ctor_m146 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern "C" void DefaultInitializationErrorHandler_Awake_m147 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern "C" void DefaultInitializationErrorHandler_OnGUI_m148 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m149 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m150 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m151 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m152 (DefaultInitializationErrorHandler_t47 * __this, bool ___errorOccurred, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnQCARInitializationError_m153 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___initError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
