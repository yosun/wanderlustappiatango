﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1895;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1893;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1795;
// System.Security.Cryptography.Oid
struct Oid_t1894;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1705;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Security.Cryptography.DSA
struct DSA_t1703;
// System.Security.Cryptography.RSA
struct RSA_t1697;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m8662 (PublicKey_t1895 * __this, X509Certificate_t1705 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t1893 * PublicKey_get_EncodedKeyValue_m8663 (PublicKey_t1895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t1893 * PublicKey_get_EncodedParameters_m8664 (PublicKey_t1895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1795 * PublicKey_get_Key_m8665 (PublicKey_t1895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t1894 * PublicKey_get_Oid_m8666 (PublicKey_t1895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t622* PublicKey_GetUnsignedBigInteger_m8667 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1703 * PublicKey_DecodeDSA_m8668 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___rawPublicKey, ByteU5BU5D_t622* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1697 * PublicKey_DecodeRSA_m8669 (Object_t * __this /* static, unused */, ByteU5BU5D_t622* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
