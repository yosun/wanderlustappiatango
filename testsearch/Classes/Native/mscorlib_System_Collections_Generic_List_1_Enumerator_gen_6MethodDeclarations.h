﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t822;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t721;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m19082_gshared (Enumerator_t822 * __this, List_1_t721 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m19082(__this, ___l, method) (( void (*) (Enumerator_t822 *, List_1_t721 *, const MethodInfo*))Enumerator__ctor_m19082_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19083_gshared (Enumerator_t822 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19083(__this, method) (( Object_t * (*) (Enumerator_t822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19083_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m19084_gshared (Enumerator_t822 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19084(__this, method) (( void (*) (Enumerator_t822 *, const MethodInfo*))Enumerator_Dispose_m19084_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m19085_gshared (Enumerator_t822 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m19085(__this, method) (( void (*) (Enumerator_t822 *, const MethodInfo*))Enumerator_VerifyState_m19085_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m4443_gshared (Enumerator_t822 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4443(__this, method) (( bool (*) (Enumerator_t822 *, const MethodInfo*))Enumerator_MoveNext_m4443_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m4441_gshared (Enumerator_t822 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4441(__this, method) (( int32_t (*) (Enumerator_t822 *, const MethodInfo*))Enumerator_get_Current_m4441_gshared)(__this, method)
