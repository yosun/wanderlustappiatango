﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Type>
struct Enumerator_t3897;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1436;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m26717(__this, ___t, method) (( void (*) (Enumerator_t3897 *, Stack_1_t1436 *, const MethodInfo*))Enumerator__ctor_m15698_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26718(__this, method) (( Object_t * (*) (Enumerator_t3897 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15699_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m26719(__this, method) (( void (*) (Enumerator_t3897 *, const MethodInfo*))Enumerator_Dispose_m15700_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m26720(__this, method) (( bool (*) (Enumerator_t3897 *, const MethodInfo*))Enumerator_MoveNext_m15701_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m26721(__this, method) (( Type_t * (*) (Enumerator_t3897 *, const MethodInfo*))Enumerator_get_Current_m15702_gshared)(__this, method)
