﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t67;

// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m216 (MaskOutBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MaskOutBehaviour::Start()
extern "C" void MaskOutBehaviour_Start_m217 (MaskOutBehaviour_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
