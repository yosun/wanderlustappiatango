﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t3281;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t4111;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t321;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m17260(__this, method) (( void (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1__ctor_m15687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m17261(__this, method) (( bool (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15688_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m17262(__this, method) (( Object_t * (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15689_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m17263(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3281 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15690_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17264(__this, method) (( Object_t* (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15691_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m17265(__this, method) (( Object_t * (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15692_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m17266(__this, method) (( List_1_t321 * (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_Peek_m15693_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m17267(__this, method) (( List_1_t321 * (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_Pop_m15694_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m17268(__this, ___t, method) (( void (*) (Stack_1_t3281 *, List_1_t321 *, const MethodInfo*))Stack_1_Push_m15695_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m17269(__this, method) (( int32_t (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_get_Count_m15696_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m17270(__this, method) (( Enumerator_t4112  (*) (Stack_1_t3281 *, const MethodInfo*))Stack_1_GetEnumerator_m15697_gshared)(__this, method)
