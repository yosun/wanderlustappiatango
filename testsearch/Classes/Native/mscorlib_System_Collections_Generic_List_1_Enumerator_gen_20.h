﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t760;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t796;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
struct  Enumerator_t899 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::l
	List_1_t760 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::current
	Object_t * ___current_3;
};
