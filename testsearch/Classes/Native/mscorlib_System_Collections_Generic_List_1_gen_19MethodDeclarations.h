﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t581;
// System.Object
struct Object_t;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t767;
// System.Collections.Generic.IEnumerable`1<Vuforia.ICloudRecoEventHandler>
struct IEnumerable_1_t4154;
// System.Collections.Generic.IEnumerator`1<Vuforia.ICloudRecoEventHandler>
struct IEnumerator_1_t4155;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ICloudRecoEventHandler>
struct ICollection_1_t4156;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>
struct ReadOnlyCollection_1_t3377;
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3375;
// System.Predicate`1<Vuforia.ICloudRecoEventHandler>
struct Predicate_1_t3378;
// System.Comparison`1<Vuforia.ICloudRecoEventHandler>
struct Comparison_1_t3379;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4341(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18521(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m18522(__this, ___capacity, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.cctor()
#define List_1__cctor_m18523(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18524(__this, method) (( Object_t* (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18525(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t581 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18526(__this, method) (( Object_t * (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18527(__this, ___item, method) (( int32_t (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18528(__this, ___item, method) (( bool (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18529(__this, ___item, method) (( int32_t (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18530(__this, ___index, ___item, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18531(__this, ___item, method) (( void (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18532(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18533(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18534(__this, method) (( Object_t * (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18535(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18536(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18537(__this, ___index, method) (( Object_t * (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18538(__this, ___index, ___value, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Add(T)
#define List_1_Add_m18539(__this, ___item, method) (( void (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18540(__this, ___newCount, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18541(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18542(__this, ___enumerable, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18543(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m18544(__this, method) (( ReadOnlyCollection_1_t3377 * (*) (List_1_t581 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Clear()
#define List_1_Clear_m18545(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Contains(T)
#define List_1_Contains_m18546(__this, ___item, method) (( bool (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18547(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t581 *, ICloudRecoEventHandlerU5BU5D_t3375*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m18548(__this, ___match, method) (( Object_t * (*) (List_1_t581 *, Predicate_1_t3378 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18549(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3378 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18550(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t581 *, int32_t, int32_t, Predicate_1_t3378 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4337(__this, method) (( Enumerator_t801  (*) (List_1_t581 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::IndexOf(T)
#define List_1_IndexOf_m18551(__this, ___item, method) (( int32_t (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18552(__this, ___start, ___delta, method) (( void (*) (List_1_t581 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18553(__this, ___index, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m18554(__this, ___index, ___item, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18555(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Remove(T)
#define List_1_Remove_m18556(__this, ___item, method) (( bool (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18557(__this, ___match, method) (( int32_t (*) (List_1_t581 *, Predicate_1_t3378 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18558(__this, ___index, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Reverse()
#define List_1_Reverse_m18559(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Sort()
#define List_1_Sort_m18560(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18561(__this, ___comparison, method) (( void (*) (List_1_t581 *, Comparison_1_t3379 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::ToArray()
#define List_1_ToArray_m18562(__this, method) (( ICloudRecoEventHandlerU5BU5D_t3375* (*) (List_1_t581 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::TrimExcess()
#define List_1_TrimExcess_m18563(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Capacity()
#define List_1_get_Capacity_m18564(__this, method) (( int32_t (*) (List_1_t581 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18565(__this, ___value, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Count()
#define List_1_get_Count_m18566(__this, method) (( int32_t (*) (List_1_t581 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m18567(__this, ___index, method) (( Object_t * (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m18568(__this, ___index, ___value, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
