﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceImpl
struct SurfaceImpl_t676;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t595;
// UnityEngine.Mesh
struct Mesh_t160;
// System.Int32[]
struct Int32U5BU5D_t27;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
extern "C" void SurfaceImpl__ctor_m3088 (SurfaceImpl_t676 * __this, int32_t ___id, Object_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
extern "C" void SurfaceImpl_SetID_m3089 (SurfaceImpl_t676 * __this, int32_t ___trackableID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
extern "C" void SurfaceImpl_SetMesh_m3090 (SurfaceImpl_t676 * __this, int32_t ___meshRev, Mesh_t160 * ___mesh, Mesh_t160 * ___navMesh, Int32U5BU5D_t27* ___meshBoundaries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
extern "C" void SurfaceImpl_SetBoundingBox_m3091 (SurfaceImpl_t676 * __this, Rect_t132  ___boundingBox, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
extern "C" Mesh_t160 * SurfaceImpl_GetNavMesh_m3092 (SurfaceImpl_t676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
extern "C" Int32U5BU5D_t27* SurfaceImpl_GetMeshBoundaries_m3093 (SurfaceImpl_t676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
extern "C" Rect_t132  SurfaceImpl_get_BoundingBox_m3094 (SurfaceImpl_t676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SurfaceImpl::GetArea()
extern "C" float SurfaceImpl_GetArea_m3095 (SurfaceImpl_t676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
