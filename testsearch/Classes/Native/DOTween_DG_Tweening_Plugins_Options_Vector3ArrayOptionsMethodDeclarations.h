﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t957;
struct Vector3ArrayOptions_t957_marshaled;

void Vector3ArrayOptions_t957_marshal(const Vector3ArrayOptions_t957& unmarshaled, Vector3ArrayOptions_t957_marshaled& marshaled);
void Vector3ArrayOptions_t957_marshal_back(const Vector3ArrayOptions_t957_marshaled& marshaled, Vector3ArrayOptions_t957& unmarshaled);
void Vector3ArrayOptions_t957_marshal_cleanup(Vector3ArrayOptions_t957_marshaled& marshaled);
