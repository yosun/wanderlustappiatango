﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t510;

// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m2521 (SelectionBaseAttribute_t510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
