﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>
struct ShimEnumerator_t3955;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3944;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m27305_gshared (ShimEnumerator_t3955 * __this, Dictionary_2_t3944 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m27305(__this, ___host, method) (( void (*) (ShimEnumerator_t3955 *, Dictionary_2_t3944 *, const MethodInfo*))ShimEnumerator__ctor_m27305_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m27306_gshared (ShimEnumerator_t3955 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m27306(__this, method) (( bool (*) (ShimEnumerator_t3955 *, const MethodInfo*))ShimEnumerator_MoveNext_m27306_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m27307_gshared (ShimEnumerator_t3955 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m27307(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3955 *, const MethodInfo*))ShimEnumerator_get_Entry_m27307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m27308_gshared (ShimEnumerator_t3955 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m27308(__this, method) (( Object_t * (*) (ShimEnumerator_t3955 *, const MethodInfo*))ShimEnumerator_get_Key_m27308_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m27309_gshared (ShimEnumerator_t3955 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m27309(__this, method) (( Object_t * (*) (ShimEnumerator_t3955 *, const MethodInfo*))ShimEnumerator_get_Value_m27309_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m27310_gshared (ShimEnumerator_t3955 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m27310(__this, method) (( Object_t * (*) (ShimEnumerator_t3955 *, const MethodInfo*))ShimEnumerator_get_Current_m27310_gshared)(__this, method)
