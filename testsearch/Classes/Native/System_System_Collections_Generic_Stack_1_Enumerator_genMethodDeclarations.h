﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct Enumerator_t3177;
// System.Object
struct Object_t;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3174;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m15698_gshared (Enumerator_t3177 * __this, Stack_1_t3174 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m15698(__this, ___t, method) (( void (*) (Enumerator_t3177 *, Stack_1_t3174 *, const MethodInfo*))Enumerator__ctor_m15698_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15699_gshared (Enumerator_t3177 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15699(__this, method) (( Object_t * (*) (Enumerator_t3177 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15699_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15700_gshared (Enumerator_t3177 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15700(__this, method) (( void (*) (Enumerator_t3177 *, const MethodInfo*))Enumerator_Dispose_m15700_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15701_gshared (Enumerator_t3177 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15701(__this, method) (( bool (*) (Enumerator_t3177 *, const MethodInfo*))Enumerator_MoveNext_m15701_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15702_gshared (Enumerator_t3177 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15702(__this, method) (( Object_t * (*) (Enumerator_t3177 *, const MethodInfo*))Enumerator_get_Current_m15702_gshared)(__this, method)
