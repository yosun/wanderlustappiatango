﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1938;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4073;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3969;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t3973;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3221;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t4445;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4446;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t4447;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_47.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__43.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m27387_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m27387(__this, method) (( void (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2__ctor_m27387_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27388_gshared (Dictionary_2_t1938 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27388(__this, ___comparer, method) (( void (*) (Dictionary_2_t1938 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27388_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m27389_gshared (Dictionary_2_t1938 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m27389(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1938 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27389_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m27390_gshared (Dictionary_2_t1938 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m27390(__this, ___capacity, method) (( void (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m27390_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27391_gshared (Dictionary_2_t1938 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27391(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1938 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27391_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m27392_gshared (Dictionary_2_t1938 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m27392(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1938 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m27392_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27393_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27393(__this, method) (( Object_t* (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27393_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27394_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27394(__this, method) (( Object_t* (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27394_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m27395_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m27395(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m27395_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27396_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m27396(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1938 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m27396_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27397_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m27397(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1938 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m27397_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m27398_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m27398(__this, ___key, method) (( bool (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m27398_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27399_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m27399(__this, ___key, method) (( void (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m27399_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27400_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27400(__this, method) (( bool (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27400_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27401_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27401(__this, method) (( Object_t * (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27402_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27402(__this, method) (( bool (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27402_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27403_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2_t3967  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27403(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1938 *, KeyValuePair_2_t3967 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27403_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27404_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2_t3967  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27404(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1938 *, KeyValuePair_2_t3967 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27404_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27405_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2U5BU5D_t4446* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27405(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1938 *, KeyValuePair_2U5BU5D_t4446*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27405_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27406_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2_t3967  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27406(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1938 *, KeyValuePair_2_t3967 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27406_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27407_gshared (Dictionary_2_t1938 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m27407(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1938 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m27407_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27408_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27408(__this, method) (( Object_t * (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27408_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27409_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27409(__this, method) (( Object_t* (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27409_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27410_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27410(__this, method) (( Object_t * (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27410_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m27411_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m27411(__this, method) (( int32_t (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_get_Count_m27411_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m27412_gshared (Dictionary_2_t1938 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m27412(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m27412_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m27413_gshared (Dictionary_2_t1938 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m27413(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1938 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m27413_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m27414_gshared (Dictionary_2_t1938 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m27414(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1938 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m27414_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m27415_gshared (Dictionary_2_t1938 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m27415(__this, ___size, method) (( void (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m27415_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m27416_gshared (Dictionary_2_t1938 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m27416(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1938 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m27416_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3967  Dictionary_2_make_pair_m27417_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m27417(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3967  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m27417_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m27418_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m27418(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m27418_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m27419_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m27419(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m27419_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m27420_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2U5BU5D_t4446* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m27420(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1938 *, KeyValuePair_2U5BU5D_t4446*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m27420_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m27421_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m27421(__this, method) (( void (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_Resize_m27421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m27422_gshared (Dictionary_2_t1938 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m27422(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1938 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m27422_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m27423_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m27423(__this, method) (( void (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_Clear_m27423_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m27424_gshared (Dictionary_2_t1938 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m27424(__this, ___key, method) (( bool (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m27424_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m27425_gshared (Dictionary_2_t1938 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m27425(__this, ___value, method) (( bool (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m27425_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m27426_gshared (Dictionary_2_t1938 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m27426(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1938 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m27426_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m27427_gshared (Dictionary_2_t1938 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m27427(__this, ___sender, method) (( void (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m27427_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m27428_gshared (Dictionary_2_t1938 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m27428(__this, ___key, method) (( bool (*) (Dictionary_2_t1938 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m27428_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m27429_gshared (Dictionary_2_t1938 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m27429(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1938 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m27429_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t3969 * Dictionary_2_get_Keys_m27430_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m27430(__this, method) (( KeyCollection_t3969 * (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_get_Keys_m27430_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t3973 * Dictionary_2_get_Values_m27431_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m27431(__this, method) (( ValueCollection_t3973 * (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_get_Values_m27431_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m27432_gshared (Dictionary_2_t1938 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m27432(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m27432_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m27433_gshared (Dictionary_2_t1938 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m27433(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1938 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m27433_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m27434_gshared (Dictionary_2_t1938 * __this, KeyValuePair_2_t3967  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m27434(__this, ___pair, method) (( bool (*) (Dictionary_2_t1938 *, KeyValuePair_2_t3967 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m27434_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t3971  Dictionary_2_GetEnumerator_m27435_gshared (Dictionary_2_t1938 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m27435(__this, method) (( Enumerator_t3971  (*) (Dictionary_2_t1938 *, const MethodInfo*))Dictionary_2_GetEnumerator_m27435_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m27436_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m27436(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m27436_gshared)(__this /* static, unused */, ___key, ___value, method)
