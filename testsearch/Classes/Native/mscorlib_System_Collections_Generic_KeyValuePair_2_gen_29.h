﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct  KeyValuePair_2_t3617 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::key
	Object_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::value
	ProfileData_t740  ___value_1;
};
