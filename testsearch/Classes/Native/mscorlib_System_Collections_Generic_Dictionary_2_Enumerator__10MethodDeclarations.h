﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Enumerator_t3311;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t281;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t454;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t291;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m17714(__this, ___dictionary, method) (( void (*) (Enumerator_t3311 *, Dictionary_2_t291 *, const MethodInfo*))Enumerator__ctor_m15040_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17715(__this, method) (( Object_t * (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17716(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17717(__this, method) (( Object_t * (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17718(__this, method) (( Object_t * (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m17719(__this, method) (( bool (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_MoveNext_m15045_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m17720(__this, method) (( KeyValuePair_2_t3308  (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_get_Current_m15046_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17721(__this, method) (( Canvas_t281 * (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_get_CurrentKey_m15047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17722(__this, method) (( IndexedSet_1_t454 * (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_get_CurrentValue_m15048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m17723(__this, method) (( void (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_VerifyState_m15049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17724(__this, method) (( void (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_VerifyCurrent_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m17725(__this, method) (( void (*) (Enumerator_t3311 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
