﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m1985_gshared ();
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_Execute_TisObject_t_m1969_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m2041_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m27985_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m27981_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m28010_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m2022_gshared ();
extern "C" void EventFunction_1__ctor_m15573_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_Invoke_m15575_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m15577_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_EndInvoke_m15579_gshared ();
void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m2162_gshared ();
void* RuntimeInvoker_Boolean_t176_ObjectU26_t1522_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisObject_t_m2424_gshared ();
void* RuntimeInvoker_Void_t175_ObjectU26_t1522_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Count_m16763_gshared ();
void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_IsReadOnly_m16765_gshared ();
void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Item_m16773_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_set_Item_m16775_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1__ctor_m16747_gshared ();
void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16749_gshared ();
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Add_m16751_gshared ();
extern "C" void IndexedSet_1_Remove_m16753_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m16755_gshared ();
extern "C" void IndexedSet_1_Clear_m16757_gshared ();
extern "C" void IndexedSet_1_Contains_m16759_gshared ();
extern "C" void IndexedSet_1_CopyTo_m16761_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_IndexOf_m16767_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Insert_m16769_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m16771_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_RemoveAll_m16776_gshared ();
extern "C" void IndexedSet_1_Sort_m16777_gshared ();
extern "C" void ObjectPool_1_get_countAll_m15676_gshared ();
extern "C" void ObjectPool_1_set_countAll_m15678_gshared ();
extern "C" void ObjectPool_1_get_countActive_m15680_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m15682_gshared ();
extern "C" void ObjectPool_1__ctor_m15674_gshared ();
extern "C" void ObjectPool_1_Get_m15684_gshared ();
extern "C" void ObjectPool_1_Release_m15686_gshared ();
extern "C" void NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m28198_gshared ();
extern "C" void SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m28265_gshared ();
extern "C" void TrackerManagerImpl_GetTracker_TisObject_t_m28373_gshared ();
extern "C" void TrackerManagerImpl_InitTracker_TisObject_t_m28374_gshared ();
extern "C" void TrackerManagerImpl_DeinitTracker_TisObject_t_m28375_gshared ();
extern "C" void QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361_gshared ();
extern "C" void TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenSettingsExtensions_OnComplete_TisObject_t_m248_gshared ();
extern "C" void TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared ();
extern "C" void TweenCallback_1__ctor_m23727_gshared ();
extern "C" void TweenCallback_1_Invoke_m23728_gshared ();
extern "C" void TweenCallback_1_BeginInvoke_m23729_gshared ();
extern "C" void TweenCallback_1_EndInvoke_m23730_gshared ();
extern "C" void DOGetter_1__ctor_m23731_gshared ();
extern "C" void DOGetter_1_Invoke_m23732_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23733_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23734_gshared ();
extern "C" void DOSetter_1__ctor_m23735_gshared ();
extern "C" void DOSetter_1_Invoke_m23736_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23737_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23738_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisObject_t_m28495_gshared ();
extern "C" void Object_Instantiate_TisObject_t_m408_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m298_gshared ();
extern "C" void Component_GetComponentInChildren_TisObject_t_m4347_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m372_gshared ();
void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m28126_gshared ();
void* RuntimeInvoker_Void_t175_SByte_t177_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m4459_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m2437_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m1967_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m2060_gshared ();
extern "C" void GameObject_GetComponentInChildren_TisObject_t_m4633_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m27984_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m27936_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m28127_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m506_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m2107_gshared ();
extern "C" void GameObject_AddComponent_TisObject_t_m464_gshared ();
extern "C" void ResponseBase_ParseJSONList_TisObject_t_m6982_gshared ();
extern "C" void NetworkMatch_ProcessMatchResponse_TisObject_t_m6989_gshared ();
extern "C" void ResponseDelegate_1__ctor_m25960_gshared ();
extern "C" void ResponseDelegate_1_Invoke_m25962_gshared ();
extern "C" void ResponseDelegate_1_BeginInvoke_m25964_gshared ();
extern "C" void ResponseDelegate_1_EndInvoke_m25966_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25968_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25969_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25967_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25970_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25971_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Keys_m26111_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Values_m26117_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Item_m26119_gshared ();
extern "C" void ThreadSafeDictionary_2_set_Item_m26121_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Count_m26131_gshared ();
extern "C" void ThreadSafeDictionary_2_get_IsReadOnly_m26133_gshared ();
extern "C" void ThreadSafeDictionary_2__ctor_m26101_gshared ();
extern "C" void ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103_gshared ();
extern "C" void ThreadSafeDictionary_2_Get_m26105_gshared ();
extern "C" void ThreadSafeDictionary_2_AddValue_m26107_gshared ();
extern "C" void ThreadSafeDictionary_2_Add_m26109_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m26113_gshared ();
extern "C" void ThreadSafeDictionary_2_TryGetValue_m26115_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Add_m26123_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Clear_m26125_gshared ();
extern "C" void ThreadSafeDictionary_2_Contains_m26127_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_CopyTo_m26129_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m26135_gshared ();
extern "C" void ThreadSafeDictionary_2_GetEnumerator_m26137_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2__ctor_m26094_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_Invoke_m26096_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_BeginInvoke_m26098_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_EndInvoke_m26100_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m28009_gshared ();
extern "C" void InvokableCall_1__ctor_m16155_gshared ();
extern "C" void InvokableCall_1__ctor_m16156_gshared ();
extern "C" void InvokableCall_1_Invoke_m16157_gshared ();
extern "C" void InvokableCall_1_Find_m16158_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InvokableCall_2__ctor_m26832_gshared ();
extern "C" void InvokableCall_2_Invoke_m26833_gshared ();
extern "C" void InvokableCall_2_Find_m26834_gshared ();
extern "C" void InvokableCall_3__ctor_m26839_gshared ();
extern "C" void InvokableCall_3_Invoke_m26840_gshared ();
extern "C" void InvokableCall_3_Find_m26841_gshared ();
extern "C" void InvokableCall_4__ctor_m26846_gshared ();
extern "C" void InvokableCall_4_Invoke_m26847_gshared ();
extern "C" void InvokableCall_4_Find_m26848_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m26853_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m26854_gshared ();
extern "C" void UnityEvent_1__ctor_m16145_gshared ();
extern "C" void UnityEvent_1_AddListener_m16147_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m16149_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m16150_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16151_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16153_gshared ();
extern "C" void UnityEvent_1_Invoke_m16154_gshared ();
extern "C" void UnityEvent_2__ctor_m27057_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m27058_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m27059_gshared ();
extern "C" void UnityEvent_3__ctor_m27060_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m27061_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m27062_gshared ();
extern "C" void UnityEvent_4__ctor_m27063_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m27064_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m27065_gshared ();
extern "C" void UnityAction_1__ctor_m15703_gshared ();
extern "C" void UnityAction_1_Invoke_m15704_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m15705_gshared ();
extern "C" void UnityAction_1_EndInvoke_m15706_gshared ();
extern "C" void UnityAction_2__ctor_m26835_gshared ();
extern "C" void UnityAction_2_Invoke_m26836_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m26837_gshared ();
extern "C" void UnityAction_2_EndInvoke_m26838_gshared ();
extern "C" void UnityAction_3__ctor_m26842_gshared ();
extern "C" void UnityAction_3_Invoke_m26843_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m26844_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_3_EndInvoke_m26845_gshared ();
extern "C" void UnityAction_4__ctor_m26849_gshared ();
extern "C" void UnityAction_4_Invoke_m26850_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_BeginInvoke_m26851_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_EndInvoke_m26852_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275_gshared ();
extern "C" void HashSet_1_get_Count_m23283_gshared ();
extern "C" void HashSet_1__ctor_m23269_gshared ();
extern "C" void HashSet_1__ctor_m23271_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279_gshared ();
extern "C" void HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281_gshared ();
extern "C" void HashSet_1_Init_m23285_gshared ();
extern "C" void HashSet_1_InitArrays_m23287_gshared ();
extern "C" void HashSet_1_SlotsContainsAt_m23289_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_CopyTo_m23291_gshared ();
extern "C" void HashSet_1_CopyTo_m23293_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_Resize_m23295_gshared ();
extern "C" void HashSet_1_GetLinkHashCode_m23297_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_GetItemHashCode_m23299_gshared ();
extern "C" void HashSet_1_Add_m23300_gshared ();
extern "C" void HashSet_1_Clear_m23302_gshared ();
extern "C" void HashSet_1_Contains_m23304_gshared ();
extern "C" void HashSet_1_Remove_m23306_gshared ();
extern "C" void HashSet_1_GetObjectData_m23308_gshared ();
extern "C" void HashSet_1_OnDeserialization_m23310_gshared ();
extern "C" void HashSet_1_GetEnumerator_m23311_gshared ();
void* RuntimeInvoker_Enumerator_t3669 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared ();
extern "C" void Enumerator_get_Current_m23320_gshared ();
extern "C" void Enumerator__ctor_m23317_gshared ();
extern "C" void Enumerator_MoveNext_m23319_gshared ();
extern "C" void Enumerator_Dispose_m23321_gshared ();
extern "C" void Enumerator_CheckState_m23322_gshared ();
extern "C" void PrimeHelper__cctor_m23323_gshared ();
extern "C" void PrimeHelper_TestPrime_m23324_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void PrimeHelper_CalcPrime_m23325_gshared ();
extern "C" void PrimeHelper_ToPrime_m23326_gshared ();
extern "C" void Enumerable_Any_TisObject_t_m4428_gshared ();
extern "C" void Enumerable_Cast_TisObject_t_m4404_gshared ();
extern "C" void Enumerable_CreateCastIterator_TisObject_t_m28196_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m4325_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m28150_gshared ();
extern "C" void Enumerable_First_TisObject_t_m4480_gshared ();
extern "C" void Enumerable_ToArray_TisObject_t_m4476_gshared ();
extern "C" void Enumerable_ToList_TisObject_t_m455_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m2392_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m28125_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19697_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19698_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19696_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19699_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19700_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19701_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19702_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18276_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18277_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18275_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18278_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18279_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18280_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18281_gshared ();
extern "C" void Func_2__ctor_m27066_gshared ();
extern "C" void Func_2_Invoke_m27067_gshared ();
extern "C" void Func_2_BeginInvoke_m27068_gshared ();
extern "C" void Func_2_EndInvoke_m27069_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27101_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m27102_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m27103_gshared ();
extern "C" void LinkedList_1_get_Count_m27116_gshared ();
extern "C" void LinkedList_1_get_First_m27117_gshared ();
extern "C" void LinkedList_1__ctor_m27095_gshared ();
extern "C" void LinkedList_1__ctor_m27096_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27097_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m27098_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27099_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m27100_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m27104_gshared ();
extern "C" void LinkedList_1_AddLast_m27105_gshared ();
extern "C" void LinkedList_1_Clear_m27106_gshared ();
extern "C" void LinkedList_1_Contains_m27107_gshared ();
extern "C" void LinkedList_1_CopyTo_m27108_gshared ();
extern "C" void LinkedList_1_Find_m27109_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m27110_gshared ();
void* RuntimeInvoker_Enumerator_t3941 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1_GetObjectData_m27111_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m27112_gshared ();
extern "C" void LinkedList_1_Remove_m27113_gshared ();
extern "C" void LinkedList_1_Remove_m27114_gshared ();
extern "C" void LinkedList_1_RemoveLast_m27115_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27125_gshared ();
extern "C" void Enumerator_get_Current_m27126_gshared ();
extern "C" void Enumerator__ctor_m27124_gshared ();
extern "C" void Enumerator_MoveNext_m27127_gshared ();
extern "C" void Enumerator_Dispose_m27128_gshared ();
extern "C" void LinkedListNode_1_get_List_m27121_gshared ();
extern "C" void LinkedListNode_1_get_Next_m27122_gshared ();
extern "C" void LinkedListNode_1_get_Value_m27123_gshared ();
extern "C" void LinkedListNode_1__ctor_m27118_gshared ();
extern "C" void LinkedListNode_1__ctor_m27119_gshared ();
extern "C" void LinkedListNode_1_Detach_m27120_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m15688_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m15689_gshared ();
extern "C" void Stack_1_get_Count_m15696_gshared ();
extern "C" void Stack_1__ctor_m15687_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m15690_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15691_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m15692_gshared ();
extern "C" void Stack_1_Peek_m15693_gshared ();
extern "C" void Stack_1_Pop_m15694_gshared ();
extern "C" void Stack_1_Push_m15695_gshared ();
extern "C" void Stack_1_GetEnumerator_m15697_gshared ();
void* RuntimeInvoker_Enumerator_t3177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15699_gshared ();
extern "C" void Enumerator_get_Current_m15702_gshared ();
extern "C" void Enumerator__ctor_m15698_gshared ();
extern "C" void Enumerator_Dispose_m15700_gshared ();
extern "C" void Enumerator_MoveNext_m15701_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m27874_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m27866_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m27869_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m27867_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m27868_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m27871_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m27870_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m27865_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m27873_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_get_swapper_TisObject_t_m27963_gshared ();
extern "C" void Array_Sort_TisObject_t_m28774_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28775_gshared ();
extern "C" void Array_Sort_TisObject_t_m28776_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28777_gshared ();
extern "C" void Array_Sort_TisObject_t_m14025_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28778_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m27962_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m27961_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m28779_gshared ();
extern "C" void Array_Sort_TisObject_t_m27979_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_TisObject_t_m27964_gshared ();
extern "C" void Array_compare_TisObject_t_m27976_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_m27978_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m27977_gshared ();
extern "C" void Array_swap_TisObject_t_m27980_gshared ();
extern "C" void Array_Resize_TisObject_t_m5554_gshared ();
void* RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisObject_t_m27960_gshared ();
void* RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_TrueForAll_TisObject_t_m28780_gshared ();
extern "C" void Array_ForEach_TisObject_t_m28781_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m28782_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m28784_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m28785_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m28783_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindIndex_TisObject_t_m28787_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m28788_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m28786_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28790_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28791_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28792_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28789_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14027_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m28793_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14024_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_LastIndexOf_TisObject_t_m28795_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m28794_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m28796_gshared ();
extern "C" void Array_FindAll_TisObject_t_m28797_gshared ();
extern "C" void Array_Exists_TisObject_t_m28798_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m28799_gshared ();
extern "C" void Array_Find_TisObject_t_m28800_gshared ();
extern "C" void Array_FindLast_TisObject_t_m28801_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14890_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14882_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14886_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14888_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m27547_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m27548_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m27549_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m27550_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m27545_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27546_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m27551_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m27552_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m27553_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m27554_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m27555_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m27556_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m27557_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m27558_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m27559_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m27560_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27562_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27563_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m27561_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27564_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27565_gshared ();
extern "C" void Comparer_1_get_Default_m15408_gshared ();
extern "C" void Comparer_1__ctor_m15405_gshared ();
extern "C" void Comparer_1__cctor_m15406_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15407_gshared ();
extern "C" void DefaultComparer__ctor_m15409_gshared ();
extern "C" void DefaultComparer_Compare_m15410_gshared ();
extern "C" void GenericComparer_1__ctor_m27591_gshared ();
extern "C" void GenericComparer_1_Compare_m27592_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14908_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14910_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m14912_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m14914_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14922_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14924_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14926_gshared ();
extern "C" void Dictionary_2_get_Count_m14944_gshared ();
extern "C" void Dictionary_2_get_Item_m14946_gshared ();
extern "C" void Dictionary_2_set_Item_m14948_gshared ();
extern "C" void Dictionary_2_get_Keys_m14982_gshared ();
extern "C" void Dictionary_2_get_Values_m14984_gshared ();
extern "C" void Dictionary_2__ctor_m14896_gshared ();
extern "C" void Dictionary_2__ctor_m14898_gshared ();
extern "C" void Dictionary_2__ctor_m14900_gshared ();
extern "C" void Dictionary_2__ctor_m14902_gshared ();
extern "C" void Dictionary_2__ctor_m14904_gshared ();
extern "C" void Dictionary_2__ctor_m14906_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m14916_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m14918_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m14920_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14928_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14930_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14932_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14934_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m14936_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14938_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14940_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14942_gshared ();
extern "C" void Dictionary_2_Init_m14950_gshared ();
extern "C" void Dictionary_2_InitArrays_m14952_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m14954_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27908_gshared ();
extern "C" void Dictionary_2_make_pair_m14956_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3114_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m14958_gshared ();
extern "C" void Dictionary_2_pick_value_m14960_gshared ();
extern "C" void Dictionary_2_CopyTo_m14962_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27909_gshared ();
extern "C" void Dictionary_2_Resize_m14964_gshared ();
extern "C" void Dictionary_2_Add_m14966_gshared ();
extern "C" void Dictionary_2_Clear_m14968_gshared ();
extern "C" void Dictionary_2_ContainsKey_m14970_gshared ();
extern "C" void Dictionary_2_ContainsValue_m14972_gshared ();
extern "C" void Dictionary_2_GetObjectData_m14974_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m14976_gshared ();
extern "C" void Dictionary_2_Remove_m14978_gshared ();
extern "C" void Dictionary_2_TryGetValue_m14980_gshared ();
extern "C" void Dictionary_2_ToTKey_m14986_gshared ();
extern "C" void Dictionary_2_ToTValue_m14988_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m14990_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m14992_gshared ();
void* RuntimeInvoker_Enumerator_t3121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m14994_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Entry_m15090_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002 (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Key_m15091_gshared ();
extern "C" void ShimEnumerator_get_Value_m15092_gshared ();
extern "C" void ShimEnumerator_get_Current_m15093_gshared ();
extern "C" void ShimEnumerator__ctor_m15088_gshared ();
extern "C" void ShimEnumerator_MoveNext_m15089_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared ();
extern "C" void Enumerator_get_Current_m15046_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_CurrentKey_m15047_gshared ();
extern "C" void Enumerator_get_CurrentValue_m15048_gshared ();
extern "C" void Enumerator__ctor_m15040_gshared ();
extern "C" void Enumerator_MoveNext_m15045_gshared ();
extern "C" void Enumerator_VerifyState_m15049_gshared ();
extern "C" void Enumerator_VerifyCurrent_m15050_gshared ();
extern "C" void Enumerator_Dispose_m15051_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15029_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15030_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m15031_gshared ();
extern "C" void KeyCollection_get_Count_m15034_gshared ();
extern "C" void KeyCollection__ctor_m15021_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15022_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15023_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15024_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15025_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15026_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m15027_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15028_gshared ();
extern "C" void KeyCollection_CopyTo_m15032_gshared ();
extern "C" void KeyCollection_GetEnumerator_m15033_gshared ();
void* RuntimeInvoker_Enumerator_t3120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15036_gshared ();
extern "C" void Enumerator_get_Current_m15039_gshared ();
extern "C" void Enumerator__ctor_m15035_gshared ();
extern "C" void Enumerator_Dispose_m15037_gshared ();
extern "C" void Enumerator_MoveNext_m15038_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15064_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15065_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m15066_gshared ();
extern "C" void ValueCollection_get_Count_m15069_gshared ();
extern "C" void ValueCollection__ctor_m15056_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15057_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15058_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15059_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15060_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15061_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m15062_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15063_gshared ();
extern "C" void ValueCollection_CopyTo_m15067_gshared ();
extern "C" void ValueCollection_GetEnumerator_m15068_gshared ();
void* RuntimeInvoker_Enumerator_t3124 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared ();
extern "C" void Enumerator_get_Current_m15074_gshared ();
extern "C" void Enumerator__ctor_m15070_gshared ();
extern "C" void Enumerator_Dispose_m15072_gshared ();
extern "C" void Enumerator_MoveNext_m15073_gshared ();
extern "C" void Transform_1__ctor_m15052_gshared ();
extern "C" void Transform_1_Invoke_m15053_gshared ();
extern "C" void Transform_1_BeginInvoke_m15054_gshared ();
extern "C" void Transform_1_EndInvoke_m15055_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15098_gshared ();
extern "C" void EqualityComparer_1__ctor_m15094_gshared ();
extern "C" void EqualityComparer_1__cctor_m15095_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15096_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15097_gshared ();
extern "C" void DefaultComparer__ctor_m15104_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15105_gshared ();
extern "C" void DefaultComparer_Equals_m15106_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m27593_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27594_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27595_gshared ();
extern "C" void KeyValuePair_2_get_Key_m15001_gshared ();
extern "C" void KeyValuePair_2_set_Key_m15002_gshared ();
extern "C" void KeyValuePair_2_get_Value_m15003_gshared ();
extern "C" void KeyValuePair_2_set_Value_m15004_gshared ();
extern "C" void KeyValuePair_2__ctor_m15000_gshared ();
extern "C" void KeyValuePair_2_ToString_m15005_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m7219_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m7220_gshared ();
extern "C" void List_1_get_Capacity_m15323_gshared ();
extern "C" void List_1_set_Capacity_m15325_gshared ();
extern "C" void List_1_get_Count_m7213_gshared ();
extern "C" void List_1_get_Item_m7236_gshared ();
extern "C" void List_1_set_Item_m7237_gshared ();
extern "C" void List_1__ctor_m6998_gshared ();
extern "C" void List_1__ctor_m15260_gshared ();
extern "C" void List_1__ctor_m15262_gshared ();
extern "C" void List_1__cctor_m15264_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m7216_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m7221_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m7223_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m7224_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m7225_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m7226_gshared ();
extern "C" void List_1_Add_m7229_gshared ();
extern "C" void List_1_GrowIfNeeded_m15282_gshared ();
extern "C" void List_1_AddCollection_m15284_gshared ();
extern "C" void List_1_AddEnumerable_m15286_gshared ();
extern "C" void List_1_AddRange_m15287_gshared ();
extern "C" void List_1_AsReadOnly_m15289_gshared ();
extern "C" void List_1_Clear_m7222_gshared ();
extern "C" void List_1_Contains_m7230_gshared ();
extern "C" void List_1_CopyTo_m7231_gshared ();
extern "C" void List_1_Find_m15294_gshared ();
extern "C" void List_1_CheckMatch_m15296_gshared ();
extern "C" void List_1_GetIndex_m15298_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GetEnumerator_m15299_gshared ();
void* RuntimeInvoker_Enumerator_t3147 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m7234_gshared ();
extern "C" void List_1_Shift_m15302_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckIndex_m15304_gshared ();
extern "C" void List_1_Insert_m7235_gshared ();
extern "C" void List_1_CheckCollection_m15307_gshared ();
extern "C" void List_1_Remove_m7232_gshared ();
extern "C" void List_1_RemoveAll_m15310_gshared ();
extern "C" void List_1_RemoveAt_m7227_gshared ();
extern "C" void List_1_Reverse_m15313_gshared ();
extern "C" void List_1_Sort_m15315_gshared ();
extern "C" void List_1_Sort_m15317_gshared ();
extern "C" void List_1_ToArray_m15319_gshared ();
extern "C" void List_1_TrimExcess_m15321_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared ();
extern "C" void Enumerator_get_Current_m15334_gshared ();
extern "C" void Enumerator__ctor_m15329_gshared ();
extern "C" void Enumerator_Dispose_m15331_gshared ();
extern "C" void Enumerator_VerifyState_m15332_gshared ();
extern "C" void Enumerator_MoveNext_m15333_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15366_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15374_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15375_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15376_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15377_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15378_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15379_gshared ();
extern "C" void Collection_1_get_Count_m15392_gshared ();
extern "C" void Collection_1_get_Item_m15393_gshared ();
extern "C" void Collection_1_set_Item_m15394_gshared ();
extern "C" void Collection_1__ctor_m15365_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15367_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15369_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15370_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15371_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15372_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15373_gshared ();
extern "C" void Collection_1_Add_m15380_gshared ();
extern "C" void Collection_1_Clear_m15381_gshared ();
extern "C" void Collection_1_ClearItems_m15382_gshared ();
extern "C" void Collection_1_Contains_m15383_gshared ();
extern "C" void Collection_1_CopyTo_m15384_gshared ();
extern "C" void Collection_1_GetEnumerator_m15385_gshared ();
extern "C" void Collection_1_IndexOf_m15386_gshared ();
extern "C" void Collection_1_Insert_m15387_gshared ();
extern "C" void Collection_1_InsertItem_m15388_gshared ();
extern "C" void Collection_1_Remove_m15389_gshared ();
extern "C" void Collection_1_RemoveAt_m15390_gshared ();
extern "C" void Collection_1_RemoveItem_m15391_gshared ();
extern "C" void Collection_1_SetItem_m15395_gshared ();
extern "C" void Collection_1_IsValidItem_m15396_gshared ();
extern "C" void Collection_1_ConvertItem_m15397_gshared ();
extern "C" void Collection_1_CheckWritable_m15398_gshared ();
extern "C" void Collection_1_IsSynchronized_m15399_gshared ();
extern "C" void Collection_1_IsFixedSize_m15400_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15363_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15364_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15335_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15359_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15360_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15361_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15362_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836_gshared ();
extern "C" void Getter_2__ctor_m27651_gshared ();
extern "C" void Getter_2_Invoke_m27652_gshared ();
extern "C" void Getter_2_BeginInvoke_m27653_gshared ();
extern "C" void Getter_2_EndInvoke_m27654_gshared ();
extern "C" void StaticGetter_1__ctor_m27655_gshared ();
extern "C" void StaticGetter_1_Invoke_m27656_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m27657_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m27658_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m27982_gshared ();
extern "C" void Action_1__ctor_m15248_gshared ();
extern "C" void Action_1_Invoke_m15250_gshared ();
extern "C" void Action_1_BeginInvoke_m15252_gshared ();
extern "C" void Action_1_EndInvoke_m15254_gshared ();
extern "C" void Comparison_1__ctor_m15416_gshared ();
extern "C" void Comparison_1_Invoke_m15417_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15418_gshared ();
extern "C" void Comparison_1_EndInvoke_m15419_gshared ();
extern "C" void Converter_2__ctor_m27541_gshared ();
extern "C" void Converter_2_Invoke_m27542_gshared ();
extern "C" void Converter_2_BeginInvoke_m27543_gshared ();
extern "C" void Converter_2_EndInvoke_m27544_gshared ();
extern "C" void Predicate_1__ctor_m15401_gshared ();
extern "C" void Predicate_1_Invoke_m15402_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15403_gshared ();
extern "C" void Predicate_1_EndInvoke_m15404_gshared ();
extern "C" void Nullable_1__ctor_m15177_gshared ();
void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1__ctor_m15188_gshared ();
extern "C" void DOGetter_1__ctor_m389_gshared ();
extern "C" void DOSetter_1__ctor_m390_gshared ();
extern "C" void Action_1__ctor_m15241_gshared ();
extern "C" void Comparison_1__ctor_m1972_gshared ();
extern "C" void List_1_Sort_m1976_gshared ();
extern "C" void List_1__ctor_m2016_gshared ();
extern "C" void Dictionary_2__ctor_m16394_gshared ();
extern "C" void Dictionary_2_get_Values_m16481_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16549_gshared ();
void* RuntimeInvoker_Enumerator_t3232 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m16555_gshared ();
extern "C" void Enumerator_MoveNext_m16554_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16488_gshared ();
void* RuntimeInvoker_Enumerator_t3229 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m16527_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3225 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Value_m16499_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16497_gshared ();
extern "C" void Enumerator_MoveNext_m16526_gshared ();
extern "C" void KeyValuePair_2_ToString_m16501_gshared ();
extern "C" void Comparison_1__ctor_m2075_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t102_m2072_gshared ();
extern "C" void UnityEvent_1__ctor_m2079_gshared ();
extern "C" void UnityEvent_1_Invoke_m2081_gshared ();
void* RuntimeInvoker_Void_t175_Color_t98 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m2082_gshared ();
extern "C" void TweenRunner_1__ctor_m2110_gshared ();
extern "C" void TweenRunner_1_Init_m2111_gshared ();
extern "C" void UnityAction_1__ctor_m2138_gshared ();
extern "C" void TweenRunner_1_StartTween_m2139_gshared ();
void* RuntimeInvoker_Void_t175_ColorTween_t260 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_get_Capacity_m2143_gshared ();
extern "C" void List_1_set_Capacity_m2144_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t135_m2164_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32U26_t541_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisByte_t455_m2166_gshared ();
void* RuntimeInvoker_Boolean_t176_ByteU26_t1846_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t112_m2168_gshared ();
void* RuntimeInvoker_Boolean_t176_SingleU26_t930_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m2225_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisUInt16_t460_m2221_gshared ();
void* RuntimeInvoker_Boolean_t176_UInt16U26_t2726_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ToArray_m2280_gshared ();
extern "C" void UnityEvent_1__ctor_m2312_gshared ();
extern "C" void UnityEvent_1_Invoke_m2318_gshared ();
void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m2323_gshared ();
extern "C" void UnityAction_1__ctor_m2324_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m2325_gshared ();
extern "C" void UnityEvent_1_AddListener_m2326_gshared ();
extern "C" void UnityEvent_1_Invoke_m2332_gshared ();
void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t326_m2347_gshared ();
void* RuntimeInvoker_Boolean_t176_NavigationU26_t4560_Navigation_t326 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t272_m2349_gshared ();
void* RuntimeInvoker_Boolean_t176_ColorBlockU26_t4561_ColorBlock_t272 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t345_m2350_gshared ();
void* RuntimeInvoker_Boolean_t176_SpriteStateU26_t4562_SpriteState_t345 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m18151_gshared ();
extern "C" void UnityEvent_1_Invoke_m18160_gshared ();
extern "C" void Func_2__ctor_m18264_gshared ();
extern "C" void LayoutGroup_SetProperty_TisInt32_t135_m2409_gshared ();
void* RuntimeInvoker_Void_t175_Int32U26_t541_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisVector2_t19_m2411_gshared ();
void* RuntimeInvoker_Void_t175_Vector2U26_t929_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisSingle_t112_m2418_gshared ();
void* RuntimeInvoker_Void_t175_SingleU26_t930_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisByte_t455_m2420_gshared ();
void* RuntimeInvoker_Void_t175_ByteU26_t1846_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m18378_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2590_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2591_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2599_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2600_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2601_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2602_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m18156_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18157_gshared ();
extern "C" void Action_1__ctor_m18611_gshared ();
extern "C" void List_1__ctor_m4498_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16470_gshared ();
extern "C" void LinkedList_1__ctor_m4429_gshared ();
extern "C" void LinkedList_1_AddLast_m4438_gshared ();
extern "C" void List_1__ctor_m4439_gshared ();
extern "C" void List_1_GetEnumerator_m4440_gshared ();
void* RuntimeInvoker_Enumerator_t822 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m4441_gshared ();
extern "C" void Predicate_1__ctor_m4442_gshared ();
extern "C" void Array_Exists_TisTrackableResultData_t648_m4426_gshared ();
extern "C" void Enumerator_MoveNext_m4443_gshared ();
extern "C" void LinkedList_1_get_First_m4444_gshared ();
extern "C" void LinkedListNode_1_get_Value_m4445_gshared ();
extern "C" void Dictionary_2__ctor_m20149_gshared ();
extern "C" void Dictionary_2_get_Keys_m16480_gshared ();
extern "C" void Enumerable_ToList_TisInt32_t135_m4478_gshared ();
extern "C" void Enumerable_ToArray_TisInt32_t135_m4574_gshared ();
extern "C" void LinkedListNode_1_get_Next_m4580_gshared ();
extern "C" void LinkedList_1_Remove_m4581_gshared ();
extern "C" void Dictionary_2__ctor_m4582_gshared ();
extern "C" void Dictionary_2__ctor_m4583_gshared ();
extern "C" void List_1__ctor_m4596_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5526_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5527_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m15235_gshared ();
extern "C" void DOGetter_1__ctor_m5540_gshared ();
extern "C" void DOSetter_1__ctor_m5541_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m23739_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5550_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5551_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5552_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5561_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5568_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5569_gshared ();
extern "C" void Nullable_1_get_HasValue_m15178_gshared ();
extern "C" void Nullable_1_get_Value_m15179_gshared ();
void* RuntimeInvoker_Byte_t455 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m15189_gshared ();
extern "C" void Nullable_1_get_Value_m15190_gshared ();
extern "C" void DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_gshared ();
extern "C" void Array_IndexOf_TisUInt16_t460_m5579_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void ABSTweenPlugin_3__ctor_m23829_gshared ();
extern "C" void List_1__ctor_m23850_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5598_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5601_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5602_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5603_gshared ();
extern "C" void List_1__ctor_m6972_gshared ();
extern "C" void List_1__ctor_m6973_gshared ();
extern "C" void List_1__ctor_m6974_gshared ();
extern "C" void Dictionary_2__ctor_m25711_gshared ();
extern "C" void Dictionary_2__ctor_m26445_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m7056_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m7057_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m26870_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m16781_gshared ();
extern "C" void Dictionary_2__ctor_m27131_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t135_m9394_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m14029_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14030_gshared ();
extern "C" void GenericComparer_1__ctor_m14031_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14032_gshared ();
extern "C" void Nullable_1__ctor_m14033_gshared ();
void* RuntimeInvoker_Void_t175_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m14034_gshared ();
extern "C" void Nullable_1_get_Value_m14035_gshared ();
void* RuntimeInvoker_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m14036_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14037_gshared ();
extern "C" void GenericComparer_1__ctor_m14038_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14039_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3114_m27876_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3114_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3114_m27877_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3114_m27878_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3114_m27879_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3114_m27880_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3114_m27881_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3114_m27882_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3114_m27884_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3114_m27885_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t135_m27887_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t135_m27888_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t135_m27889_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t135_m27890_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t135_m27891_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t135_m27892_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t135_m27893_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t135_m27895_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t135_m27896_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t2142_m27898_gshared ();
void* RuntimeInvoker_Link_t2142_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t2142_m27899_gshared ();
void* RuntimeInvoker_Void_t175_Link_t2142 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t2142_m27900_gshared ();
void* RuntimeInvoker_Boolean_t176_Link_t2142 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t2142_m27901_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t2142_m27902_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t2142_m27903_gshared ();
void* RuntimeInvoker_Int32_t135_Link_t2142 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t2142_m27904_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Link_t2142 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t2142_m27906_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t2142_m27907_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t2002_m27911_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t2002_m27912_gshared ();
void* RuntimeInvoker_Void_t175_DictionaryEntry_t2002 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t2002_m27913_gshared ();
void* RuntimeInvoker_Boolean_t176_DictionaryEntry_t2002 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t2002_m27914_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t2002_m27915_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t2002_m27916_gshared ();
void* RuntimeInvoker_Int32_t135_DictionaryEntry_t2002 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t2002_m27917_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_DictionaryEntry_t2002 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t2002_m27919_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t2002_m27920_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m27921_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3114_m27923_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisObject_t_m27922_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisKeyValuePair_2_t3114_m27924_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t460_m27926_gshared ();
void* RuntimeInvoker_UInt16_t460_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t460_m27927_gshared ();
void* RuntimeInvoker_Void_t175_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t460_m27928_gshared ();
void* RuntimeInvoker_Boolean_t176_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t460_m27929_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t460_m27930_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t460_m27931_gshared ();
void* RuntimeInvoker_Int32_t135_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt16_t460_m27932_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt16_t460_m27934_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t460_m27935_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27937_gshared ();
void* RuntimeInvoker_Single_t112_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Tweener_DoStartup_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27940_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27938_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27941_gshared ();
extern "C" void Tweener_DoStartup_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27944_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27942_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27945_gshared ();
extern "C" void Tweener_DoStartup_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27948_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27946_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t14_m27950_gshared ();
void* RuntimeInvoker_Vector3_t14_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t14_m27951_gshared ();
void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t14_m27952_gshared ();
void* RuntimeInvoker_Boolean_t176_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t14_m27953_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t14_m27954_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t14_m27955_gshared ();
void* RuntimeInvoker_Int32_t135_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector3_t14_m27956_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector3_t14_m27958_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t14_m27959_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27947_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27943_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27939_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t1413_m27966_gshared ();
void* RuntimeInvoker_Double_t1413_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t1413_m27967_gshared ();
void* RuntimeInvoker_Void_t175_Double_t1413 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t1413_m27968_gshared ();
void* RuntimeInvoker_Boolean_t176_Double_t1413 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t1413_m27969_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t1413_m27970_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t1413_m27971_gshared ();
void* RuntimeInvoker_Int32_t135_Double_t1413 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDouble_t1413_m27972_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Double_t1413 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDouble_t1413_m27974_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1413_m27975_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t238_m27987_gshared ();
void* RuntimeInvoker_RaycastResult_t238_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t238_m27988_gshared ();
void* RuntimeInvoker_Void_t175_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t238_m27989_gshared ();
void* RuntimeInvoker_Boolean_t176_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t238_m27990_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t238_m27991_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t238_m27992_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t238_m27993_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t238_m27995_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t238_m27996_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t238_m27998_gshared ();
void* RuntimeInvoker_Void_t175_RaycastResultU5BU5DU26_t4563_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisRaycastResult_t238_m27997_gshared ();
void* RuntimeInvoker_Void_t175_RaycastResultU5BU5DU26_t4563_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisRaycastResult_t238_m27999_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_RaycastResult_t238_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisRaycastResult_t238_m28001_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t238_TisRaycastResult_t238_m28000_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t238_m28002_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t238_TisRaycastResult_t238_m28003_gshared ();
extern "C" void Array_compare_TisRaycastResult_t238_m28004_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastResult_t238_RaycastResult_t238_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisRaycastResult_t238_TisRaycastResult_t238_m28005_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t238_m28007_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t238_m28006_gshared ();
extern "C" void Array_swap_TisRaycastResult_t238_m28008_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3225_m28012_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3225_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3225_m28013_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3225_m28014_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3225_m28015_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3225_m28016_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3225_m28017_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3225_m28018_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3225_m28020_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3225_m28021_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28023_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28022_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28024_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28026_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28025_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28027_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3225_m28029_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3225_TisObject_t_m28028_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3225_TisKeyValuePair_2_t3225_m28030_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t438_m28032_gshared ();
void* RuntimeInvoker_RaycastHit2D_t438_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t438_m28033_gshared ();
void* RuntimeInvoker_Void_t175_RaycastHit2D_t438 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t438_m28034_gshared ();
void* RuntimeInvoker_Boolean_t176_RaycastHit2D_t438 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t438_m28035_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t438_m28036_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t438_m28037_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastHit2D_t438 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t438_m28038_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_RaycastHit2D_t438 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t438_m28040_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t438_m28041_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t102_m28043_gshared ();
void* RuntimeInvoker_RaycastHit_t102_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t102_m28044_gshared ();
void* RuntimeInvoker_Void_t175_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t102_m28045_gshared ();
void* RuntimeInvoker_Boolean_t176_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t102_m28046_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t102_m28047_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t102_m28048_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t102_m28049_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t102_m28051_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t102_m28052_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t102_m28053_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t102_m28054_gshared ();
extern "C" void Array_swap_TisRaycastHit_t102_m28055_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t98_m28056_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3253_m28058_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3253_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3253_m28059_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3253_m28060_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3253_m28061_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3253_m28062_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3253_m28063_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3253_m28064_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3253_m28066_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3253_m28067_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28069_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28068_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28071_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28070_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28072_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28073_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3253_m28075_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3253_TisObject_t_m28074_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3253_TisKeyValuePair_2_t3253_m28076_gshared ();
extern "C" void Array_Resize_TisUIVertex_t319_m28078_gshared ();
void* RuntimeInvoker_Void_t175_UIVertexU5BU5DU26_t4564_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUIVertex_t319_m28077_gshared ();
void* RuntimeInvoker_Void_t175_UIVertexU5BU5DU26_t4564_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUIVertex_t319_m28079_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_UIVertex_t319_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUIVertex_t319_m28081_gshared ();
extern "C" void Array_Sort_TisUIVertex_t319_TisUIVertex_t319_m28080_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t319_m28082_gshared ();
extern "C" void Array_qsort_TisUIVertex_t319_TisUIVertex_t319_m28083_gshared ();
extern "C" void Array_compare_TisUIVertex_t319_m28084_gshared ();
void* RuntimeInvoker_Int32_t135_UIVertex_t319_UIVertex_t319_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUIVertex_t319_TisUIVertex_t319_m28085_gshared ();
extern "C" void Array_Sort_TisUIVertex_t319_m28087_gshared ();
extern "C" void Array_qsort_TisUIVertex_t319_m28086_gshared ();
extern "C" void Array_swap_TisUIVertex_t319_m28088_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t19_m28090_gshared ();
void* RuntimeInvoker_Vector2_t19_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t19_m28091_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t19_m28092_gshared ();
void* RuntimeInvoker_Boolean_t176_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t19_m28093_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t19_m28094_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t19_m28095_gshared ();
void* RuntimeInvoker_Int32_t135_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector2_t19_m28096_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector2_t19_m28098_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t19_m28099_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t464_m28101_gshared ();
void* RuntimeInvoker_UILineInfo_t464_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t464_m28102_gshared ();
void* RuntimeInvoker_Void_t175_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t464_m28103_gshared ();
void* RuntimeInvoker_Boolean_t176_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t464_m28104_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t464_m28105_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t464_m28106_gshared ();
void* RuntimeInvoker_Int32_t135_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t464_m28107_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t464_m28109_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t464_m28110_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t466_m28112_gshared ();
void* RuntimeInvoker_UICharInfo_t466_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t466_m28113_gshared ();
void* RuntimeInvoker_Void_t175_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t466_m28114_gshared ();
void* RuntimeInvoker_Boolean_t176_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t466_m28115_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t466_m28116_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t466_m28117_gshared ();
void* RuntimeInvoker_Int32_t135_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t466_m28118_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t466_m28120_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t466_m28121_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t112_m28122_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t19_m28123_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisByte_t455_m28124_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t112_m28129_gshared ();
void* RuntimeInvoker_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t112_m28130_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t112_m28131_gshared ();
void* RuntimeInvoker_Boolean_t176_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t112_m28132_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t112_m28133_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t112_m28134_gshared ();
void* RuntimeInvoker_Int32_t135_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSingle_t112_m28135_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSingle_t112_m28137_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t112_m28138_gshared ();
extern "C" void Array_InternalArray__get_Item_TisEyewearCalibrationReading_t592_m28140_gshared ();
void* RuntimeInvoker_EyewearCalibrationReading_t592_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t592_m28141_gshared ();
void* RuntimeInvoker_Void_t175_EyewearCalibrationReading_t592 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t592_m28142_gshared ();
void* RuntimeInvoker_Boolean_t176_EyewearCalibrationReading_t592 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t592_m28143_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t592_m28144_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t592_m28145_gshared ();
void* RuntimeInvoker_Int32_t135_EyewearCalibrationReading_t592 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisEyewearCalibrationReading_t592_m28146_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_EyewearCalibrationReading_t592 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisEyewearCalibrationReading_t592_m28148_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t592_m28149_gshared ();
extern "C" void Array_Resize_TisInt32_t135_m28152_gshared ();
void* RuntimeInvoker_Void_t175_Int32U5BU5DU26_t4565_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisInt32_t135_m28151_gshared ();
void* RuntimeInvoker_Void_t175_Int32U5BU5DU26_t4565_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisInt32_t135_m28153_gshared ();
extern "C" void Array_Sort_TisInt32_t135_m28155_gshared ();
extern "C" void Array_Sort_TisInt32_t135_TisInt32_t135_m28154_gshared ();
extern "C" void Array_get_swapper_TisInt32_t135_m28156_gshared ();
extern "C" void Array_qsort_TisInt32_t135_TisInt32_t135_m28157_gshared ();
extern "C" void Array_compare_TisInt32_t135_m28158_gshared ();
extern "C" void Array_swap_TisInt32_t135_TisInt32_t135_m28159_gshared ();
extern "C" void Array_Sort_TisInt32_t135_m28161_gshared ();
extern "C" void Array_qsort_TisInt32_t135_m28160_gshared ();
extern "C" void Array_swap_TisInt32_t135_m28162_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t455_m28164_gshared ();
void* RuntimeInvoker_Byte_t455_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t455_m28165_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t455_m28166_gshared ();
void* RuntimeInvoker_Boolean_t176_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t455_m28167_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t455_m28168_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t455_m28169_gshared ();
void* RuntimeInvoker_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisByte_t455_m28170_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisByte_t455_m28172_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t455_m28173_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor32_t427_m28175_gshared ();
void* RuntimeInvoker_Color32_t427_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisColor32_t427_m28176_gshared ();
void* RuntimeInvoker_Void_t175_Color32_t427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisColor32_t427_m28177_gshared ();
void* RuntimeInvoker_Boolean_t176_Color32_t427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor32_t427_m28178_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor32_t427_m28179_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor32_t427_m28180_gshared ();
void* RuntimeInvoker_Int32_t135_Color32_t427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisColor32_t427_m28181_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Color32_t427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisColor32_t427_m28183_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t427_m28184_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor_t98_m28186_gshared ();
void* RuntimeInvoker_Color_t98_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisColor_t98_m28187_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisColor_t98_m28188_gshared ();
void* RuntimeInvoker_Boolean_t176_Color_t98 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor_t98_m28189_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor_t98_m28190_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor_t98_m28191_gshared ();
void* RuntimeInvoker_Int32_t135_Color_t98 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisColor_t98_m28192_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Color_t98 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisColor_t98_m28194_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t98_m28195_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTrackableResultData_t648_m28200_gshared ();
void* RuntimeInvoker_TrackableResultData_t648_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTrackableResultData_t648_m28201_gshared ();
void* RuntimeInvoker_Void_t175_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTrackableResultData_t648_m28202_gshared ();
void* RuntimeInvoker_Boolean_t176_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t648_m28203_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTrackableResultData_t648_m28204_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTrackableResultData_t648_m28205_gshared ();
void* RuntimeInvoker_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTrackableResultData_t648_m28206_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTrackableResultData_t648_m28208_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t648_m28209_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordData_t653_m28211_gshared ();
void* RuntimeInvoker_WordData_t653_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWordData_t653_m28212_gshared ();
void* RuntimeInvoker_Void_t175_WordData_t653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWordData_t653_m28213_gshared ();
void* RuntimeInvoker_Boolean_t176_WordData_t653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordData_t653_m28214_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordData_t653_m28215_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordData_t653_m28216_gshared ();
void* RuntimeInvoker_Int32_t135_WordData_t653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWordData_t653_m28217_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_WordData_t653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWordData_t653_m28219_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t653_m28220_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordResultData_t652_m28222_gshared ();
void* RuntimeInvoker_WordResultData_t652_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWordResultData_t652_m28223_gshared ();
void* RuntimeInvoker_Void_t175_WordResultData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWordResultData_t652_m28224_gshared ();
void* RuntimeInvoker_Boolean_t176_WordResultData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordResultData_t652_m28225_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordResultData_t652_m28226_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordResultData_t652_m28227_gshared ();
void* RuntimeInvoker_Int32_t135_WordResultData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWordResultData_t652_m28228_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_WordResultData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWordResultData_t652_m28230_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t652_m28231_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t656_m28233_gshared ();
void* RuntimeInvoker_SmartTerrainRevisionData_t656_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t656_m28234_gshared ();
void* RuntimeInvoker_Void_t175_SmartTerrainRevisionData_t656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t656_m28235_gshared ();
void* RuntimeInvoker_Boolean_t176_SmartTerrainRevisionData_t656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t656_m28236_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t656_m28237_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t656_m28238_gshared ();
void* RuntimeInvoker_Int32_t135_SmartTerrainRevisionData_t656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSmartTerrainRevisionData_t656_m28239_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_SmartTerrainRevisionData_t656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t656_m28241_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t656_m28242_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSurfaceData_t657_m28244_gshared ();
void* RuntimeInvoker_SurfaceData_t657_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSurfaceData_t657_m28245_gshared ();
void* RuntimeInvoker_Void_t175_SurfaceData_t657 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSurfaceData_t657_m28246_gshared ();
void* RuntimeInvoker_Boolean_t176_SurfaceData_t657 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t657_m28247_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSurfaceData_t657_m28248_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSurfaceData_t657_m28249_gshared ();
void* RuntimeInvoker_Int32_t135_SurfaceData_t657 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSurfaceData_t657_m28250_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_SurfaceData_t657 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSurfaceData_t657_m28252_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t657_m28253_gshared ();
extern "C" void Array_InternalArray__get_Item_TisPropData_t658_m28255_gshared ();
void* RuntimeInvoker_PropData_t658_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisPropData_t658_m28256_gshared ();
void* RuntimeInvoker_Void_t175_PropData_t658 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisPropData_t658_m28257_gshared ();
void* RuntimeInvoker_Boolean_t176_PropData_t658 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisPropData_t658_m28258_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisPropData_t658_m28259_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisPropData_t658_m28260_gshared ();
void* RuntimeInvoker_Int32_t135_PropData_t658 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisPropData_t658_m28261_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_PropData_t658 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisPropData_t658_m28263_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t658_m28264_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3475_m28267_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3475_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3475_m28268_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3475 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3475_m28269_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3475 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3475_m28270_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3475_m28271_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3475_m28272_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3475 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3475_m28273_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3475 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3475_m28275_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3475_m28276_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28278_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28277_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t460_m28280_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t460_TisObject_t_m28279_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t460_TisUInt16_t460_m28281_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28282_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3475_m28284_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3475_TisObject_t_m28283_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3475_TisKeyValuePair_2_t3475_m28285_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRectangleData_t602_m28287_gshared ();
void* RuntimeInvoker_RectangleData_t602_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRectangleData_t602_m28288_gshared ();
void* RuntimeInvoker_Void_t175_RectangleData_t602 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRectangleData_t602_m28289_gshared ();
void* RuntimeInvoker_Boolean_t176_RectangleData_t602 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRectangleData_t602_m28290_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRectangleData_t602_m28291_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRectangleData_t602_m28292_gshared ();
void* RuntimeInvoker_Int32_t135_RectangleData_t602 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRectangleData_t602_m28293_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_RectangleData_t602 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRectangleData_t602_m28295_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t602_m28296_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3563_m28298_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3563_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3563_m28299_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3563 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3563_m28300_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3563 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3563_m28301_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3563_m28302_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3563_m28303_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3563 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3563_m28304_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3563 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3563_m28306_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3563_m28307_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28309_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28308_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28310_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t648_m28312_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t648_TisObject_t_m28311_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t648_TisTrackableResultData_t648_m28313_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28314_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3563_m28316_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3563_TisObject_t_m28315_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3563_TisKeyValuePair_2_t3563_m28317_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3578_m28319_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3578_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3578_m28320_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3578 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3578_m28321_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3578 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3578_m28322_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3578_m28323_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3578_m28324_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3578 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3578_m28325_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3578 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3578_m28327_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3578_m28328_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVirtualButtonData_t649_m28330_gshared ();
void* RuntimeInvoker_VirtualButtonData_t649_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVirtualButtonData_t649_m28331_gshared ();
void* RuntimeInvoker_Void_t175_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t649_m28332_gshared ();
void* RuntimeInvoker_Boolean_t176_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t649_m28333_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t649_m28334_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVirtualButtonData_t649_m28335_gshared ();
void* RuntimeInvoker_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVirtualButtonData_t649_m28336_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVirtualButtonData_t649_m28338_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t649_m28339_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28341_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28340_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28342_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t649_m28344_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t649_TisObject_t_m28343_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t649_TisVirtualButtonData_t649_m28345_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28346_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3578_m28348_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3578_TisObject_t_m28347_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3578_TisKeyValuePair_2_t3578_m28349_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTargetSearchResult_t726_m28351_gshared ();
void* RuntimeInvoker_TargetSearchResult_t726_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTargetSearchResult_t726_m28352_gshared ();
void* RuntimeInvoker_Void_t175_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t726_m28353_gshared ();
void* RuntimeInvoker_Boolean_t176_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t726_m28354_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t726_m28355_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTargetSearchResult_t726_m28356_gshared ();
void* RuntimeInvoker_Int32_t135_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTargetSearchResult_t726_m28357_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTargetSearchResult_t726_m28359_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t726_m28360_gshared ();
extern "C" void Array_Resize_TisTargetSearchResult_t726_m28362_gshared ();
void* RuntimeInvoker_Void_t175_TargetSearchResultU5BU5DU26_t4566_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisTargetSearchResult_t726_m28361_gshared ();
void* RuntimeInvoker_Void_t175_TargetSearchResultU5BU5DU26_t4566_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisTargetSearchResult_t726_m28363_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_TargetSearchResult_t726_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisTargetSearchResult_t726_m28365_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28364_gshared ();
extern "C" void Array_get_swapper_TisTargetSearchResult_t726_m28366_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28367_gshared ();
extern "C" void Array_compare_TisTargetSearchResult_t726_m28368_gshared ();
void* RuntimeInvoker_Int32_t135_TargetSearchResult_t726_TargetSearchResult_t726_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28369_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t726_m28371_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t726_m28370_gshared ();
extern "C" void Array_swap_TisTargetSearchResult_t726_m28372_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWebCamDevice_t885_m28377_gshared ();
void* RuntimeInvoker_WebCamDevice_t885_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWebCamDevice_t885_m28378_gshared ();
void* RuntimeInvoker_Void_t175_WebCamDevice_t885 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWebCamDevice_t885_m28379_gshared ();
void* RuntimeInvoker_Boolean_t176_WebCamDevice_t885 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t885_m28380_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWebCamDevice_t885_m28381_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWebCamDevice_t885_m28382_gshared ();
void* RuntimeInvoker_Int32_t135_WebCamDevice_t885 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWebCamDevice_t885_m28383_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_WebCamDevice_t885 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWebCamDevice_t885_m28385_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t885_m28386_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3617_m28388_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3617_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3617_m28389_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3617 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3617_m28390_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3617 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3617_m28391_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3617_m28392_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3617_m28393_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3617 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3617_m28394_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3617 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3617_m28396_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3617_m28397_gshared ();
extern "C" void Array_InternalArray__get_Item_TisProfileData_t740_m28399_gshared ();
void* RuntimeInvoker_ProfileData_t740_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisProfileData_t740_m28400_gshared ();
void* RuntimeInvoker_Void_t175_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisProfileData_t740_m28401_gshared ();
void* RuntimeInvoker_Boolean_t176_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisProfileData_t740_m28402_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisProfileData_t740_m28403_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisProfileData_t740_m28404_gshared ();
void* RuntimeInvoker_Int32_t135_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisProfileData_t740_m28405_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisProfileData_t740_m28407_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t740_m28408_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28410_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28409_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t740_m28412_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t740_TisObject_t_m28411_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t740_TisProfileData_t740_m28413_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28414_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3617_m28416_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3617_TisObject_t_m28415_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3617_TisKeyValuePair_2_t3617_m28417_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t3666_m28419_gshared ();
void* RuntimeInvoker_Link_t3666_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t3666_m28420_gshared ();
void* RuntimeInvoker_Void_t175_Link_t3666 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t3666_m28421_gshared ();
void* RuntimeInvoker_Boolean_t176_Link_t3666 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t3666_m28422_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t3666_m28423_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t3666_m28424_gshared ();
void* RuntimeInvoker_Int32_t135_Link_t3666 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t3666_m28425_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Link_t3666 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t3666_m28427_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3666_m28428_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28429_gshared ();
extern "C" void Tweener_DoStartup_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28432_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28430_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28431_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28433_gshared ();
extern "C" void Tweener_DoStartup_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28436_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28434_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28435_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t939_m28437_gshared ();
extern "C" void Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t939_m28440_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisNoOptions_t939_m28438_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisNoOptions_t939_m28439_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28441_gshared ();
extern "C" void Tweener_DoStartup_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28444_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28442_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28443_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28445_gshared ();
extern "C" void Tweener_DoStartup_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28448_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28446_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28447_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28449_gshared ();
extern "C" void Tweener_DoStartup_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28452_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28450_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28451_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28453_gshared ();
extern "C" void Tweener_DoStartup_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28456_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28454_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28455_gshared ();
extern "C" void TweenManager_GetTweener_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28457_gshared ();
extern "C" void Tweener_Setup_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28458_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t_Vector3_t14_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void PluginsManager_GetDefaultPlugin_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28459_gshared ();
extern "C" void TweenManager_GetTweener_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28460_gshared ();
extern "C" void Tweener_Setup_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28461_gshared ();
extern "C" void PluginsManager_GetDefaultPlugin_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28462_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1009_m28463_gshared ();
extern "C" void Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1009_m28466_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisStringOptions_t1009_m28464_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisStringOptions_t1009_m28465_gshared ();
extern "C" void Array_Resize_TisUInt16_t460_m28468_gshared ();
void* RuntimeInvoker_Void_t175_UInt16U5BU5DU26_t4567_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUInt16_t460_m28467_gshared ();
void* RuntimeInvoker_Void_t175_UInt16U5BU5DU26_t4567_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUInt16_t460_m28469_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int16_t540_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUInt16_t460_m28471_gshared ();
extern "C" void Array_Sort_TisUInt16_t460_TisUInt16_t460_m28470_gshared ();
extern "C" void Array_get_swapper_TisUInt16_t460_m28472_gshared ();
extern "C" void Array_qsort_TisUInt16_t460_TisUInt16_t460_m28473_gshared ();
extern "C" void Array_compare_TisUInt16_t460_m28474_gshared ();
void* RuntimeInvoker_Int32_t135_Int16_t540_Int16_t540_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUInt16_t460_TisUInt16_t460_m28475_gshared ();
extern "C" void Array_Sort_TisUInt16_t460_m28477_gshared ();
extern "C" void Array_qsort_TisUInt16_t460_m28476_gshared ();
extern "C" void Array_swap_TisUInt16_t460_m28478_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28479_gshared ();
extern "C" void Tweener_DoStartup_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28482_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28480_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28481_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28483_gshared ();
extern "C" void Tweener_DoStartup_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28486_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28484_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28485_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28487_gshared ();
extern "C" void Tweener_DoStartup_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28490_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28488_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28489_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28491_gshared ();
extern "C" void Tweener_DoStartup_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28494_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28492_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28493_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t1311_m28497_gshared ();
void* RuntimeInvoker_GcAchievementData_t1311_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t1311_m28498_gshared ();
void* RuntimeInvoker_Void_t175_GcAchievementData_t1311 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1311_m28499_gshared ();
void* RuntimeInvoker_Boolean_t176_GcAchievementData_t1311 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1311_m28500_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1311_m28501_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t1311_m28502_gshared ();
void* RuntimeInvoker_Int32_t135_GcAchievementData_t1311 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t1311_m28503_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_GcAchievementData_t1311 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t1311_m28505_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1311_m28506_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t1312_m28508_gshared ();
void* RuntimeInvoker_GcScoreData_t1312_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t1312_m28509_gshared ();
void* RuntimeInvoker_Void_t175_GcScoreData_t1312 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t1312_m28510_gshared ();
void* RuntimeInvoker_Boolean_t176_GcScoreData_t1312 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1312_m28511_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t1312_m28512_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t1312_m28513_gshared ();
void* RuntimeInvoker_Int32_t135_GcScoreData_t1312 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t1312_m28514_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_GcScoreData_t1312 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t1312_m28516_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1312_m28517_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m28519_gshared ();
void* RuntimeInvoker_IntPtr_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m28520_gshared ();
void* RuntimeInvoker_Void_t175_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m28521_gshared ();
void* RuntimeInvoker_Boolean_t176_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m28522_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m28523_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m28524_gshared ();
void* RuntimeInvoker_Int32_t135_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m28525_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m28527_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m28528_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t1242_m28530_gshared ();
void* RuntimeInvoker_Keyframe_t1242_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t1242_m28531_gshared ();
void* RuntimeInvoker_Void_t175_Keyframe_t1242 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t1242_m28532_gshared ();
void* RuntimeInvoker_Boolean_t176_Keyframe_t1242 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1242_m28533_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t1242_m28534_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t1242_m28535_gshared ();
void* RuntimeInvoker_Int32_t135_Keyframe_t1242 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyframe_t1242_m28536_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Keyframe_t1242 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t1242_m28538_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1242_m28539_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t466_m28541_gshared ();
void* RuntimeInvoker_Void_t175_UICharInfoU5BU5DU26_t4568_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUICharInfo_t466_m28540_gshared ();
void* RuntimeInvoker_Void_t175_UICharInfoU5BU5DU26_t4568_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUICharInfo_t466_m28542_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_UICharInfo_t466_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUICharInfo_t466_m28544_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t466_TisUICharInfo_t466_m28543_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t466_m28545_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t466_TisUICharInfo_t466_m28546_gshared ();
extern "C" void Array_compare_TisUICharInfo_t466_m28547_gshared ();
void* RuntimeInvoker_Int32_t135_UICharInfo_t466_UICharInfo_t466_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUICharInfo_t466_TisUICharInfo_t466_m28548_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t466_m28550_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t466_m28549_gshared ();
extern "C" void Array_swap_TisUICharInfo_t466_m28551_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t464_m28553_gshared ();
void* RuntimeInvoker_Void_t175_UILineInfoU5BU5DU26_t4569_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUILineInfo_t464_m28552_gshared ();
void* RuntimeInvoker_Void_t175_UILineInfoU5BU5DU26_t4569_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUILineInfo_t464_m28554_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_UILineInfo_t464_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUILineInfo_t464_m28556_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t464_TisUILineInfo_t464_m28555_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t464_m28557_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t464_TisUILineInfo_t464_m28558_gshared ();
extern "C" void Array_compare_TisUILineInfo_t464_m28559_gshared ();
void* RuntimeInvoker_Int32_t135_UILineInfo_t464_UILineInfo_t464_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUILineInfo_t464_TisUILineInfo_t464_m28560_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t464_m28562_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t464_m28561_gshared ();
extern "C" void Array_swap_TisUILineInfo_t464_m28563_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3790_m28565_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3790_m28566_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3790 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3790_m28567_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3790 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3790_m28568_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3790_m28569_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3790_m28570_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3790 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3790_m28571_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3790 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3790_m28573_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3790_m28574_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t1098_m28576_gshared ();
void* RuntimeInvoker_Int64_t1098_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t1098_m28577_gshared ();
void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t1098_m28578_gshared ();
void* RuntimeInvoker_Boolean_t176_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t1098_m28579_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t1098_m28580_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t1098_m28581_gshared ();
void* RuntimeInvoker_Int32_t135_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisInt64_t1098_m28582_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisInt64_t1098_m28584_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1098_m28585_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28587_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28586_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1098_m28589_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1098_TisObject_t_m28588_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1098_TisInt64_t1098_m28590_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28591_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3790_m28593_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisObject_t_m28592_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisKeyValuePair_2_t3790_m28594_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3828_m28596_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3828_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3828_m28597_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3828 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3828_m28598_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3828 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3828_m28599_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3828_m28600_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3828_m28601_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3828 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3828_m28602_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3828 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3828_m28604_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3828_m28605_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t1097_m28607_gshared ();
void* RuntimeInvoker_UInt64_t1097_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t1097_m28608_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t1097_m28609_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t1097_m28610_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t1097_m28611_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t1097_m28612_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt64_t1097_m28613_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt64_t1097_m28615_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1097_m28616_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1097_m28618_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1097_TisObject_t_m28617_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1097_TisUInt64_t1097_m28619_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28621_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28620_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28622_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3828_m28624_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3828_TisObject_t_m28623_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3828_TisKeyValuePair_2_t3828_m28625_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3849_m28627_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3849_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3849_m28628_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3849 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3849_m28629_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3849 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3849_m28630_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3849_m28631_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3849_m28632_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3849 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3849_m28633_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3849 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3849_m28635_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3849_m28636_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28638_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28637_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3114_m28640_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisObject_t_m28639_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisKeyValuePair_2_t3114_m28641_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28642_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3849_m28644_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3849_TisObject_t_m28643_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3849_TisKeyValuePair_2_t3849_m28645_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t2266_m28647_gshared ();
void* RuntimeInvoker_ParameterModifier_t2266_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t2266_m28648_gshared ();
void* RuntimeInvoker_Void_t175_ParameterModifier_t2266 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t2266_m28649_gshared ();
void* RuntimeInvoker_Boolean_t176_ParameterModifier_t2266 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2266_m28650_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t2266_m28651_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t2266_m28652_gshared ();
void* RuntimeInvoker_Int32_t135_ParameterModifier_t2266 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t2266_m28653_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_ParameterModifier_t2266 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t2266_m28655_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2266_m28656_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t1329_m28658_gshared ();
void* RuntimeInvoker_HitInfo_t1329_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t1329_m28659_gshared ();
void* RuntimeInvoker_Void_t175_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t1329_m28660_gshared ();
void* RuntimeInvoker_Boolean_t176_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1329_m28661_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t1329_m28662_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t1329_m28663_gshared ();
void* RuntimeInvoker_Int32_t135_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisHitInfo_t1329_m28664_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t1329_m28666_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1329_m28667_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t135_m28668_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t1081_m28670_gshared ();
void* RuntimeInvoker_UInt32_t1081_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t1081_m28671_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t1081_m28672_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t1081_m28673_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t1081_m28674_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t1081_m28675_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t1081_m28676_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t1081_m28678_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1081_m28679_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3945_m28681_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3945_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3945_m28682_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3945 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3945_m28683_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3945 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3945_m28684_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3945_m28685_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3945_m28686_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3945 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3945_m28687_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3945 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3945_m28689_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3945_m28690_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28692_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28691_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t455_m28694_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t455_TisObject_t_m28693_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t455_TisByte_t455_m28695_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28696_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3945_m28698_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3945_TisObject_t_m28697_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3945_TisKeyValuePair_2_t3945_m28699_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t1908_m28701_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1908_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1908_m28702_gshared ();
void* RuntimeInvoker_Void_t175_X509ChainStatus_t1908 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1908_m28703_gshared ();
void* RuntimeInvoker_Boolean_t176_X509ChainStatus_t1908 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1908_m28704_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1908_m28705_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t1908_m28706_gshared ();
void* RuntimeInvoker_Int32_t135_X509ChainStatus_t1908 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t1908_m28707_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_X509ChainStatus_t1908 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t1908_m28709_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1908_m28710_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3967_m28712_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3967_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3967_m28713_gshared ();
void* RuntimeInvoker_Void_t175_KeyValuePair_2_t3967 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3967_m28714_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3967 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3967_m28715_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3967_m28716_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3967_m28717_gshared ();
void* RuntimeInvoker_Int32_t135_KeyValuePair_2_t3967 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3967_m28718_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3967 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3967_m28720_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3967_m28721_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28723_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28722_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28724_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28725_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3967_m28727_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3967_TisObject_t_m28726_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3967_TisKeyValuePair_2_t3967_m28728_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t135_m28729_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisMark_t1954_m28731_gshared ();
void* RuntimeInvoker_Mark_t1954_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t1954_m28732_gshared ();
void* RuntimeInvoker_Void_t175_Mark_t1954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t1954_m28733_gshared ();
void* RuntimeInvoker_Boolean_t176_Mark_t1954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t1954_m28734_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t1954_m28735_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t1954_m28736_gshared ();
void* RuntimeInvoker_Int32_t135_Mark_t1954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMark_t1954_m28737_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Mark_t1954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMark_t1954_m28739_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1954_m28740_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t1990_m28742_gshared ();
void* RuntimeInvoker_UriScheme_t1990_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t1990_m28743_gshared ();
void* RuntimeInvoker_Void_t175_UriScheme_t1990 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t1990_m28744_gshared ();
void* RuntimeInvoker_Boolean_t176_UriScheme_t1990 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1990_m28745_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t1990_m28746_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t1990_m28747_gshared ();
void* RuntimeInvoker_Int32_t135_UriScheme_t1990 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUriScheme_t1990_m28748_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_UriScheme_t1990 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t1990_m28750_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1990_m28751_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t540_m28753_gshared ();
void* RuntimeInvoker_Int16_t540_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t540_m28754_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t540_m28755_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t540_m28756_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t540_m28757_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t540_m28758_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t540_m28759_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t540_m28761_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t540_m28762_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t177_m28764_gshared ();
void* RuntimeInvoker_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t177_m28765_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t177_m28766_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t177_m28767_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t177_m28768_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t177_m28769_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t177_m28770_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t177_m28772_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t177_m28773_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t2075_m28803_gshared ();
void* RuntimeInvoker_TableRange_t2075_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t2075_m28804_gshared ();
void* RuntimeInvoker_Void_t175_TableRange_t2075 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t2075_m28805_gshared ();
void* RuntimeInvoker_Boolean_t176_TableRange_t2075 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t2075_m28808_gshared ();
void* RuntimeInvoker_Int32_t135_TableRange_t2075 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTableRange_t2075_m28809_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_TableRange_t2075 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTableRange_t2075_m28811_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2152_m28814_gshared ();
void* RuntimeInvoker_Slot_t2152_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815_gshared ();
void* RuntimeInvoker_Void_t175_Slot_t2152 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816_gshared ();
void* RuntimeInvoker_Boolean_t176_Slot_t2152 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2152_m28819_gshared ();
void* RuntimeInvoker_Int32_t135_Slot_t2152 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2152_m28820_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Slot_t2152 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2152_m28822_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2159_m28825_gshared ();
void* RuntimeInvoker_Slot_t2159_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826_gshared ();
void* RuntimeInvoker_Void_t175_Slot_t2159 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827_gshared ();
void* RuntimeInvoker_Boolean_t176_Slot_t2159 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2159_m28830_gshared ();
void* RuntimeInvoker_Int32_t135_Slot_t2159 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2159_m28831_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Slot_t2159 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2159_m28833_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t120_m28838_gshared ();
void* RuntimeInvoker_DateTime_t120_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839_gshared ();
void* RuntimeInvoker_Void_t175_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840_gshared ();
void* RuntimeInvoker_Boolean_t176_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t120_m28843_gshared ();
void* RuntimeInvoker_Int32_t135_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDateTime_t120_m28844_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDateTime_t120_m28846_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t1065_m28849_gshared ();
void* RuntimeInvoker_Decimal_t1065_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850_gshared ();
void* RuntimeInvoker_Void_t175_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851_gshared ();
void* RuntimeInvoker_Boolean_t176_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t1065_m28854_gshared ();
void* RuntimeInvoker_Int32_t135_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDecimal_t1065_m28855_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1065_m28857_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t121_m28860_gshared ();
void* RuntimeInvoker_TimeSpan_t121_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862_gshared ();
void* RuntimeInvoker_Boolean_t176_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865_gshared ();
void* RuntimeInvoker_Int32_t135_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t121_m28866_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t121_m28868_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14995_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14996_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14997_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14998_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14999_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15011_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15012_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15013_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15014_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15015_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15016_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15017_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15018_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15019_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15020_gshared ();
void* RuntimeInvoker_Link_t2142 (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1__ctor_m15075_gshared ();
extern "C" void Transform_1_Invoke_m15076_gshared ();
extern "C" void Transform_1_BeginInvoke_m15077_gshared ();
extern "C" void Transform_1_EndInvoke_m15078_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15079_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15081_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15082_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15083_gshared ();
extern "C" void Transform_1__ctor_m15084_gshared ();
extern "C" void Transform_1_Invoke_m15085_gshared ();
extern "C" void Transform_1_BeginInvoke_m15086_gshared ();
extern "C" void Transform_1_EndInvoke_m15087_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3114_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15158_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15160_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15162_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15164_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15166_gshared ();
void* RuntimeInvoker_UInt16_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_Equals_m15181_gshared ();
extern "C" void Nullable_1_Equals_m15183_gshared ();
void* RuntimeInvoker_Boolean_t176_Nullable_1_t3140 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m15185_gshared ();
extern "C" void Nullable_1_ToString_m15187_gshared ();
extern "C" void Nullable_1_Equals_m15192_gshared ();
extern "C" void Nullable_1_Equals_m15194_gshared ();
void* RuntimeInvoker_Boolean_t176_Nullable_1_t3141 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m15196_gshared ();
extern "C" void Nullable_1_ToString_m15198_gshared ();
extern "C" void DOGetter_1_Invoke_m15199_gshared ();
void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m15200_gshared ();
extern "C" void DOGetter_1_EndInvoke_m15201_gshared ();
void* RuntimeInvoker_Vector3_t14_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_Invoke_m15202_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m15203_gshared ();
void* RuntimeInvoker_Object_t_Vector3_t14_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m15204_gshared ();
extern "C" void TweenerCore_3__ctor_m15205_gshared ();
extern "C" void TweenerCore_3_Reset_m15206_gshared ();
extern "C" void TweenerCore_3_Validate_m15207_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m15208_gshared ();
void* RuntimeInvoker_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3_Startup_m15209_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m15210_gshared ();
void* RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3__ctor_m15211_gshared ();
extern "C" void TweenerCore_3_Reset_m15212_gshared ();
extern "C" void TweenerCore_3_Validate_m15213_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m15214_gshared ();
extern "C" void TweenerCore_3_Startup_m15215_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m15216_gshared ();
extern "C" void DOGetter_1_Invoke_m15217_gshared ();
void* RuntimeInvoker_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m15218_gshared ();
extern "C" void DOGetter_1_EndInvoke_m15219_gshared ();
void* RuntimeInvoker_Quaternion_t22_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_Invoke_m15220_gshared ();
void* RuntimeInvoker_Void_t175_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m15221_gshared ();
void* RuntimeInvoker_Object_t_Quaternion_t22_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m15222_gshared ();
extern "C" void TweenerCore_3__ctor_m15224_gshared ();
extern "C" void TweenerCore_3_Reset_m15226_gshared ();
extern "C" void TweenerCore_3_Validate_m15228_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m15230_gshared ();
extern "C" void TweenerCore_3_Startup_m15232_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m15234_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15236_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15237_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15238_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15239_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15240_gshared ();
extern "C" void Action_1_Invoke_m15243_gshared ();
extern "C" void Action_1_BeginInvoke_m15245_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m15247_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15411_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15412_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15413_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15414_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15415_gshared ();
void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m15570_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastResult_t238_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m15571_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t238_RaycastResult_t238_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15572_gshared ();
extern "C" void List_1__ctor_m15816_gshared ();
extern "C" void List_1__ctor_m15817_gshared ();
extern "C" void List_1__cctor_m15818_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15819_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15820_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15821_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15822_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15823_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15824_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15825_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15826_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15827_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15828_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15829_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15830_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15831_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15832_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15833_gshared ();
extern "C" void List_1_Add_m15834_gshared ();
extern "C" void List_1_GrowIfNeeded_m15835_gshared ();
extern "C" void List_1_AddCollection_m15836_gshared ();
extern "C" void List_1_AddEnumerable_m15837_gshared ();
extern "C" void List_1_AddRange_m15838_gshared ();
extern "C" void List_1_AsReadOnly_m15839_gshared ();
extern "C" void List_1_Clear_m15840_gshared ();
extern "C" void List_1_Contains_m15841_gshared ();
extern "C" void List_1_CopyTo_m15842_gshared ();
extern "C" void List_1_Find_m15843_gshared ();
void* RuntimeInvoker_RaycastResult_t238_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m15844_gshared ();
extern "C" void List_1_GetIndex_m15845_gshared ();
extern "C" void List_1_GetEnumerator_m15846_gshared ();
void* RuntimeInvoker_Enumerator_t3186 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15847_gshared ();
extern "C" void List_1_Shift_m15848_gshared ();
extern "C" void List_1_CheckIndex_m15849_gshared ();
extern "C" void List_1_Insert_m15850_gshared ();
extern "C" void List_1_CheckCollection_m15851_gshared ();
extern "C" void List_1_Remove_m15852_gshared ();
extern "C" void List_1_RemoveAll_m15853_gshared ();
extern "C" void List_1_RemoveAt_m15854_gshared ();
extern "C" void List_1_Reverse_m15855_gshared ();
extern "C" void List_1_Sort_m15856_gshared ();
extern "C" void List_1_ToArray_m15857_gshared ();
extern "C" void List_1_TrimExcess_m15858_gshared ();
extern "C" void List_1_get_Capacity_m15859_gshared ();
extern "C" void List_1_set_Capacity_m15860_gshared ();
extern "C" void List_1_get_Count_m15861_gshared ();
extern "C" void List_1_get_Item_m15862_gshared ();
extern "C" void List_1_set_Item_m15863_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15864_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15865_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15866_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15867_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15868_gshared ();
void* RuntimeInvoker_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m15869_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15870_gshared ();
extern "C" void Enumerator_Dispose_m15871_gshared ();
extern "C" void Enumerator_VerifyState_m15872_gshared ();
extern "C" void Enumerator_MoveNext_m15873_gshared ();
extern "C" void Enumerator_get_Current_m15874_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15875_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15876_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15877_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15878_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15879_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15880_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15881_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15882_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15883_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15884_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15885_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15886_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15887_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15888_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15889_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15890_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15891_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15892_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15893_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15894_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15895_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15896_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15897_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15898_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15899_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15900_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15901_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15902_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15903_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15904_gshared ();
extern "C" void Collection_1__ctor_m15905_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15906_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15907_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15908_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15909_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15910_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15911_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15912_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15913_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15914_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15915_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15916_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15917_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15918_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15919_gshared ();
extern "C" void Collection_1_Add_m15920_gshared ();
extern "C" void Collection_1_Clear_m15921_gshared ();
extern "C" void Collection_1_ClearItems_m15922_gshared ();
extern "C" void Collection_1_Contains_m15923_gshared ();
extern "C" void Collection_1_CopyTo_m15924_gshared ();
extern "C" void Collection_1_GetEnumerator_m15925_gshared ();
extern "C" void Collection_1_IndexOf_m15926_gshared ();
extern "C" void Collection_1_Insert_m15927_gshared ();
extern "C" void Collection_1_InsertItem_m15928_gshared ();
extern "C" void Collection_1_Remove_m15929_gshared ();
extern "C" void Collection_1_RemoveAt_m15930_gshared ();
extern "C" void Collection_1_RemoveItem_m15931_gshared ();
extern "C" void Collection_1_get_Count_m15932_gshared ();
extern "C" void Collection_1_get_Item_m15933_gshared ();
extern "C" void Collection_1_set_Item_m15934_gshared ();
extern "C" void Collection_1_SetItem_m15935_gshared ();
extern "C" void Collection_1_IsValidItem_m15936_gshared ();
extern "C" void Collection_1_ConvertItem_m15937_gshared ();
extern "C" void Collection_1_CheckWritable_m15938_gshared ();
extern "C" void Collection_1_IsSynchronized_m15939_gshared ();
extern "C" void Collection_1_IsFixedSize_m15940_gshared ();
extern "C" void EqualityComparer_1__ctor_m15941_gshared ();
extern "C" void EqualityComparer_1__cctor_m15942_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15943_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15944_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15945_gshared ();
extern "C" void DefaultComparer__ctor_m15946_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15947_gshared ();
extern "C" void DefaultComparer_Equals_m15948_gshared ();
void* RuntimeInvoker_Boolean_t176_RaycastResult_t238_RaycastResult_t238 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m15949_gshared ();
extern "C" void Predicate_1_Invoke_m15950_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15951_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t238_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m15952_gshared ();
extern "C" void Comparer_1__ctor_m15953_gshared ();
extern "C" void Comparer_1__cctor_m15954_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15955_gshared ();
extern "C" void Comparer_1_get_Default_m15956_gshared ();
extern "C" void DefaultComparer__ctor_m15957_gshared ();
extern "C" void DefaultComparer_Compare_m15958_gshared ();
extern "C" void Dictionary_2__ctor_m16396_gshared ();
extern "C" void Dictionary_2__ctor_m16398_gshared ();
extern "C" void Dictionary_2__ctor_m16400_gshared ();
extern "C" void Dictionary_2__ctor_m16402_gshared ();
extern "C" void Dictionary_2__ctor_m16404_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16406_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16408_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16410_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16412_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16414_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16416_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16418_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16420_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16422_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16424_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16426_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16428_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16430_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16432_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16434_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16436_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16438_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16440_gshared ();
extern "C" void Dictionary_2_get_Count_m16442_gshared ();
extern "C" void Dictionary_2_get_Item_m16444_gshared ();
extern "C" void Dictionary_2_set_Item_m16446_gshared ();
extern "C" void Dictionary_2_Init_m16448_gshared ();
extern "C" void Dictionary_2_InitArrays_m16450_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16452_gshared ();
extern "C" void Dictionary_2_make_pair_m16454_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3225_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16456_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16458_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16460_gshared ();
extern "C" void Dictionary_2_Resize_m16462_gshared ();
extern "C" void Dictionary_2_Add_m16464_gshared ();
extern "C" void Dictionary_2_Clear_m16466_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16468_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16472_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16474_gshared ();
extern "C" void Dictionary_2_Remove_m16476_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16478_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ToTKey_m16483_gshared ();
extern "C" void Dictionary_2_ToTValue_m16485_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16487_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16490_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16491_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16492_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16493_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16494_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16495_gshared ();
extern "C" void KeyValuePair_2__ctor_m16496_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16498_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16500_gshared ();
extern "C" void KeyCollection__ctor_m16502_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16503_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16504_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16505_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16506_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16507_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16508_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16509_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16510_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16511_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16512_gshared ();
extern "C" void KeyCollection_CopyTo_m16513_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16514_gshared ();
void* RuntimeInvoker_Enumerator_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16515_gshared ();
extern "C" void Enumerator__ctor_m16516_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16517_gshared ();
extern "C" void Enumerator_Dispose_m16518_gshared ();
extern "C" void Enumerator_MoveNext_m16519_gshared ();
extern "C" void Enumerator_get_Current_m16520_gshared ();
extern "C" void Enumerator__ctor_m16521_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16528_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16529_gshared ();
extern "C" void Enumerator_VerifyState_m16530_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16531_gshared ();
extern "C" void Enumerator_Dispose_m16532_gshared ();
extern "C" void Transform_1__ctor_m16533_gshared ();
extern "C" void Transform_1_Invoke_m16534_gshared ();
extern "C" void Transform_1_BeginInvoke_m16535_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16536_gshared ();
extern "C" void ValueCollection__ctor_m16537_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16538_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16539_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16540_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16541_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16542_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16543_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16544_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16545_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16546_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16547_gshared ();
extern "C" void ValueCollection_CopyTo_m16548_gshared ();
extern "C" void ValueCollection_get_Count_m16550_gshared ();
extern "C" void Enumerator__ctor_m16551_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared ();
extern "C" void Enumerator_Dispose_m16553_gshared ();
extern "C" void Transform_1__ctor_m16556_gshared ();
extern "C" void Transform_1_Invoke_m16557_gshared ();
extern "C" void Transform_1_BeginInvoke_m16558_gshared ();
extern "C" void Transform_1_EndInvoke_m16559_gshared ();
extern "C" void Transform_1__ctor_m16560_gshared ();
extern "C" void Transform_1_Invoke_m16561_gshared ();
extern "C" void Transform_1_BeginInvoke_m16562_gshared ();
extern "C" void Transform_1_EndInvoke_m16563_gshared ();
extern "C" void Transform_1__ctor_m16564_gshared ();
extern "C" void Transform_1_Invoke_m16565_gshared ();
extern "C" void Transform_1_BeginInvoke_m16566_gshared ();
extern "C" void Transform_1_EndInvoke_m16567_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3225_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16568_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16569_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16570_gshared ();
extern "C" void ShimEnumerator_get_Key_m16571_gshared ();
extern "C" void ShimEnumerator_get_Value_m16572_gshared ();
extern "C" void ShimEnumerator_get_Current_m16573_gshared ();
extern "C" void EqualityComparer_1__ctor_m16574_gshared ();
extern "C" void EqualityComparer_1__cctor_m16575_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16576_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16577_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16578_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16579_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16580_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16581_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m16582_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16583_gshared ();
extern "C" void DefaultComparer_Equals_m16584_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16725_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16726_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16727_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16728_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16729_gshared ();
void* RuntimeInvoker_RaycastHit2D_t438 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m16730_gshared ();
void* RuntimeInvoker_Int32_t135_RaycastHit_t102_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m16731_gshared ();
void* RuntimeInvoker_Object_t_RaycastHit_t102_RaycastHit_t102_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m16732_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16733_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16734_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16735_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16736_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16737_gshared ();
void* RuntimeInvoker_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_RemoveListener_m16738_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16739_gshared ();
extern "C" void UnityAction_1_Invoke_m16740_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m16741_gshared ();
void* RuntimeInvoker_Object_t_Color_t98_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m16742_gshared ();
extern "C" void InvokableCall_1__ctor_m16743_gshared ();
extern "C" void InvokableCall_1__ctor_m16744_gshared ();
extern "C" void InvokableCall_1_Invoke_m16745_gshared ();
extern "C" void InvokableCall_1_Find_m16746_gshared ();
extern "C" void Dictionary_2__ctor_m16778_gshared ();
extern "C" void Dictionary_2__ctor_m16779_gshared ();
extern "C" void Dictionary_2__ctor_m16780_gshared ();
extern "C" void Dictionary_2__ctor_m16782_gshared ();
extern "C" void Dictionary_2__ctor_m16783_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16784_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16785_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16786_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16787_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16788_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16789_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16790_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16791_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16792_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16793_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16794_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16795_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16797_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16798_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16799_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16800_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16801_gshared ();
extern "C" void Dictionary_2_get_Count_m16802_gshared ();
extern "C" void Dictionary_2_get_Item_m16803_gshared ();
extern "C" void Dictionary_2_set_Item_m16804_gshared ();
extern "C" void Dictionary_2_Init_m16805_gshared ();
extern "C" void Dictionary_2_InitArrays_m16806_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16807_gshared ();
extern "C" void Dictionary_2_make_pair_m16808_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3253_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16809_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16810_gshared ();
void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16811_gshared ();
extern "C" void Dictionary_2_Resize_m16812_gshared ();
extern "C" void Dictionary_2_Add_m16813_gshared ();
extern "C" void Dictionary_2_Clear_m16814_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16815_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16816_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16817_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16818_gshared ();
extern "C" void Dictionary_2_Remove_m16819_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16820_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_Int32U26_t541 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m16821_gshared ();
extern "C" void Dictionary_2_get_Values_m16822_gshared ();
extern "C" void Dictionary_2_ToTKey_m16823_gshared ();
extern "C" void Dictionary_2_ToTValue_m16824_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16825_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16826_gshared ();
void* RuntimeInvoker_Enumerator_t3257 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16827_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16828_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16829_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16830_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16831_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16832_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3253 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m16833_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16834_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16835_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16836_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16837_gshared ();
extern "C" void KeyValuePair_2_ToString_m16838_gshared ();
extern "C" void KeyCollection__ctor_m16839_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16840_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16841_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16842_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16843_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16844_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16845_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16846_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16847_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16848_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16849_gshared ();
extern "C" void KeyCollection_CopyTo_m16850_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16851_gshared ();
void* RuntimeInvoker_Enumerator_t3256 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16852_gshared ();
extern "C" void Enumerator__ctor_m16853_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16854_gshared ();
extern "C" void Enumerator_Dispose_m16855_gshared ();
extern "C" void Enumerator_MoveNext_m16856_gshared ();
extern "C" void Enumerator_get_Current_m16857_gshared ();
extern "C" void Enumerator__ctor_m16858_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared ();
extern "C" void Enumerator_MoveNext_m16863_gshared ();
extern "C" void Enumerator_get_Current_m16864_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16865_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16866_gshared ();
extern "C" void Enumerator_VerifyState_m16867_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16868_gshared ();
extern "C" void Enumerator_Dispose_m16869_gshared ();
extern "C" void Transform_1__ctor_m16870_gshared ();
extern "C" void Transform_1_Invoke_m16871_gshared ();
extern "C" void Transform_1_BeginInvoke_m16872_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16873_gshared ();
extern "C" void ValueCollection__ctor_m16874_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16875_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16876_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16877_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16878_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16879_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16880_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16881_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16882_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16883_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16884_gshared ();
extern "C" void ValueCollection_CopyTo_m16885_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16886_gshared ();
void* RuntimeInvoker_Enumerator_t3260 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m16887_gshared ();
extern "C" void Enumerator__ctor_m16888_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16889_gshared ();
extern "C" void Enumerator_Dispose_m16890_gshared ();
extern "C" void Enumerator_MoveNext_m16891_gshared ();
extern "C" void Enumerator_get_Current_m16892_gshared ();
extern "C" void Transform_1__ctor_m16893_gshared ();
extern "C" void Transform_1_Invoke_m16894_gshared ();
extern "C" void Transform_1_BeginInvoke_m16895_gshared ();
extern "C" void Transform_1_EndInvoke_m16896_gshared ();
extern "C" void Transform_1__ctor_m16897_gshared ();
extern "C" void Transform_1_Invoke_m16898_gshared ();
extern "C" void Transform_1_BeginInvoke_m16899_gshared ();
extern "C" void Transform_1_EndInvoke_m16900_gshared ();
extern "C" void Transform_1__ctor_m16901_gshared ();
extern "C" void Transform_1_Invoke_m16902_gshared ();
extern "C" void Transform_1_BeginInvoke_m16903_gshared ();
extern "C" void Transform_1_EndInvoke_m16904_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16905_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16906_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16907_gshared ();
extern "C" void ShimEnumerator_get_Key_m16908_gshared ();
extern "C" void ShimEnumerator_get_Value_m16909_gshared ();
extern "C" void ShimEnumerator_get_Current_m16910_gshared ();
extern "C" void List_1__ctor_m17271_gshared ();
extern "C" void List_1__cctor_m17272_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17273_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17274_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m17275_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m17276_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m17277_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m17278_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m17279_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m17280_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17281_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m17282_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m17283_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m17284_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m17285_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m17286_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m17287_gshared ();
extern "C" void List_1_Add_m17288_gshared ();
void* RuntimeInvoker_Void_t175_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GrowIfNeeded_m17289_gshared ();
extern "C" void List_1_AddCollection_m17290_gshared ();
extern "C" void List_1_AddEnumerable_m17291_gshared ();
extern "C" void List_1_AddRange_m17292_gshared ();
extern "C" void List_1_AsReadOnly_m17293_gshared ();
extern "C" void List_1_Clear_m17294_gshared ();
extern "C" void List_1_Contains_m17295_gshared ();
void* RuntimeInvoker_Boolean_t176_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CopyTo_m17296_gshared ();
extern "C" void List_1_Find_m17297_gshared ();
void* RuntimeInvoker_UIVertex_t319_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m17298_gshared ();
extern "C" void List_1_GetIndex_m17299_gshared ();
extern "C" void List_1_GetEnumerator_m17300_gshared ();
void* RuntimeInvoker_Enumerator_t3284 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m17301_gshared ();
void* RuntimeInvoker_Int32_t135_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_Shift_m17302_gshared ();
extern "C" void List_1_CheckIndex_m17303_gshared ();
extern "C" void List_1_Insert_m17304_gshared ();
void* RuntimeInvoker_Void_t175_Int32_t135_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckCollection_m17305_gshared ();
extern "C" void List_1_Remove_m17306_gshared ();
extern "C" void List_1_RemoveAll_m17307_gshared ();
extern "C" void List_1_RemoveAt_m17308_gshared ();
extern "C" void List_1_Reverse_m17309_gshared ();
extern "C" void List_1_Sort_m17310_gshared ();
extern "C" void List_1_Sort_m17311_gshared ();
extern "C" void List_1_TrimExcess_m17312_gshared ();
extern "C" void List_1_get_Count_m17313_gshared ();
extern "C" void List_1_get_Item_m17314_gshared ();
void* RuntimeInvoker_UIVertex_t319_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_set_Item_m17315_gshared ();
extern "C" void Enumerator__ctor_m17250_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17251_gshared ();
extern "C" void Enumerator_Dispose_m17252_gshared ();
extern "C" void Enumerator_VerifyState_m17253_gshared ();
extern "C" void Enumerator_MoveNext_m17254_gshared ();
extern "C" void Enumerator_get_Current_m17255_gshared ();
void* RuntimeInvoker_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void ReadOnlyCollection_1__ctor_m17216_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17217_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17218_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17219_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17220_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17221_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17222_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17223_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17224_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17225_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17226_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m17227_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17228_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m17229_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17230_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17231_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17232_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17233_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17234_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17235_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17236_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17237_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m17238_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17239_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m17240_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m17241_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m17242_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m17243_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m17244_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m17245_gshared ();
extern "C" void Collection_1__ctor_m17319_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17320_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17321_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m17322_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m17323_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m17324_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m17325_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m17326_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m17327_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m17328_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m17329_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m17330_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m17331_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m17332_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m17333_gshared ();
extern "C" void Collection_1_Add_m17334_gshared ();
extern "C" void Collection_1_Clear_m17335_gshared ();
extern "C" void Collection_1_ClearItems_m17336_gshared ();
extern "C" void Collection_1_Contains_m17337_gshared ();
extern "C" void Collection_1_CopyTo_m17338_gshared ();
extern "C" void Collection_1_GetEnumerator_m17339_gshared ();
extern "C" void Collection_1_IndexOf_m17340_gshared ();
extern "C" void Collection_1_Insert_m17341_gshared ();
extern "C" void Collection_1_InsertItem_m17342_gshared ();
extern "C" void Collection_1_Remove_m17343_gshared ();
extern "C" void Collection_1_RemoveAt_m17344_gshared ();
extern "C" void Collection_1_RemoveItem_m17345_gshared ();
extern "C" void Collection_1_get_Count_m17346_gshared ();
extern "C" void Collection_1_get_Item_m17347_gshared ();
extern "C" void Collection_1_set_Item_m17348_gshared ();
extern "C" void Collection_1_SetItem_m17349_gshared ();
extern "C" void Collection_1_IsValidItem_m17350_gshared ();
extern "C" void Collection_1_ConvertItem_m17351_gshared ();
extern "C" void Collection_1_CheckWritable_m17352_gshared ();
extern "C" void Collection_1_IsSynchronized_m17353_gshared ();
extern "C" void Collection_1_IsFixedSize_m17354_gshared ();
extern "C" void EqualityComparer_1__ctor_m17355_gshared ();
extern "C" void EqualityComparer_1__cctor_m17356_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17357_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17358_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17359_gshared ();
extern "C" void DefaultComparer__ctor_m17360_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17361_gshared ();
extern "C" void DefaultComparer_Equals_m17362_gshared ();
void* RuntimeInvoker_Boolean_t176_UIVertex_t319_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m17246_gshared ();
extern "C" void Predicate_1_Invoke_m17247_gshared ();
extern "C" void Predicate_1_BeginInvoke_m17248_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t319_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m17249_gshared ();
extern "C" void Comparer_1__ctor_m17363_gshared ();
extern "C" void Comparer_1__cctor_m17364_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m17365_gshared ();
extern "C" void Comparer_1_get_Default_m17366_gshared ();
extern "C" void DefaultComparer__ctor_m17367_gshared ();
extern "C" void DefaultComparer_Compare_m17368_gshared ();
void* RuntimeInvoker_Int32_t135_UIVertex_t319_UIVertex_t319 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m17256_gshared ();
extern "C" void Comparison_1_Invoke_m17257_gshared ();
extern "C" void Comparison_1_BeginInvoke_m17258_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t319_UIVertex_t319_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m17259_gshared ();
extern "C" void TweenRunner_1_Start_m17369_gshared ();
void* RuntimeInvoker_Object_t_ColorTween_t260 (const MethodInfo* method, void* obj, void** args);
extern "C" void U3CStartU3Ec__Iterator0__ctor_m17370_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17371_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17372_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m17373_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m17374_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m17375_gshared ();
extern "C" void InternalEnumerator_1__ctor_m17826_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17827_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17828_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17829_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17830_gshared ();
void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17836_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17837_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17838_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17839_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17840_gshared ();
void* RuntimeInvoker_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17841_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17842_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17843_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17844_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17845_gshared ();
void* RuntimeInvoker_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_GetDelegate_m17853_gshared ();
extern "C" void UnityAction_1_Invoke_m17854_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17855_gshared ();
void* RuntimeInvoker_Object_t_Single_t112_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17856_gshared ();
extern "C" void InvokableCall_1__ctor_m17857_gshared ();
extern "C" void InvokableCall_1__ctor_m17858_gshared ();
extern "C" void InvokableCall_1_Invoke_m17859_gshared ();
extern "C" void InvokableCall_1_Find_m17860_gshared ();
extern "C" void UnityEvent_1_AddListener_m17861_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m17862_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m17863_gshared ();
extern "C" void UnityAction_1__ctor_m17864_gshared ();
extern "C" void UnityAction_1_Invoke_m17865_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17866_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t19_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17867_gshared ();
extern "C" void InvokableCall_1__ctor_m17868_gshared ();
extern "C" void InvokableCall_1__ctor_m17869_gshared ();
extern "C" void InvokableCall_1_Invoke_m17870_gshared ();
extern "C" void InvokableCall_1_Find_m17871_gshared ();
extern "C" void UnityEvent_1_AddListener_m18153_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m18155_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18159_gshared ();
extern "C" void UnityAction_1__ctor_m18161_gshared ();
extern "C" void UnityAction_1_Invoke_m18162_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m18163_gshared ();
void* RuntimeInvoker_Object_t_SByte_t177_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m18164_gshared ();
extern "C" void InvokableCall_1__ctor_m18165_gshared ();
extern "C" void InvokableCall_1__ctor_m18166_gshared ();
extern "C" void InvokableCall_1_Invoke_m18167_gshared ();
extern "C" void InvokableCall_1_Find_m18168_gshared ();
extern "C" void Func_2_Invoke_m18266_gshared ();
void* RuntimeInvoker_Byte_t455_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18268_gshared ();
extern "C" void Func_2_EndInvoke_m18270_gshared ();
extern "C" void Func_2_Invoke_m18380_gshared ();
void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18382_gshared ();
extern "C" void Func_2_EndInvoke_m18384_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18421_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18422_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18423_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18424_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18425_gshared ();
void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18426_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18427_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18428_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18429_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18430_gshared ();
void* RuntimeInvoker_EyewearCalibrationReading_t592 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_Invoke_m18613_gshared ();
extern "C" void Action_1_BeginInvoke_m18615_gshared ();
extern "C" void Action_1_EndInvoke_m18617_gshared ();
extern "C" void List_1__ctor_m18988_gshared ();
extern "C" void List_1__cctor_m18990_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18994_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m18996_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m18998_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m19000_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m19002_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m19004_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m19006_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m19010_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m19012_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m19014_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m19016_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m19018_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m19020_gshared ();
extern "C" void List_1_Add_m19022_gshared ();
extern "C" void List_1_GrowIfNeeded_m19024_gshared ();
extern "C" void List_1_AddCollection_m19026_gshared ();
extern "C" void List_1_AddEnumerable_m19028_gshared ();
extern "C" void List_1_AddRange_m19030_gshared ();
extern "C" void List_1_AsReadOnly_m19032_gshared ();
extern "C" void List_1_Clear_m19034_gshared ();
extern "C" void List_1_Contains_m19036_gshared ();
extern "C" void List_1_CopyTo_m19038_gshared ();
extern "C" void List_1_Find_m19040_gshared ();
extern "C" void List_1_CheckMatch_m19042_gshared ();
extern "C" void List_1_GetIndex_m19044_gshared ();
extern "C" void List_1_IndexOf_m19047_gshared ();
extern "C" void List_1_Shift_m19049_gshared ();
extern "C" void List_1_CheckIndex_m19051_gshared ();
extern "C" void List_1_Insert_m19053_gshared ();
extern "C" void List_1_CheckCollection_m19055_gshared ();
extern "C" void List_1_Remove_m19057_gshared ();
extern "C" void List_1_RemoveAll_m19059_gshared ();
extern "C" void List_1_RemoveAt_m19061_gshared ();
extern "C" void List_1_Reverse_m19063_gshared ();
extern "C" void List_1_Sort_m19065_gshared ();
extern "C" void List_1_Sort_m19067_gshared ();
extern "C" void List_1_ToArray_m19069_gshared ();
extern "C" void List_1_TrimExcess_m19071_gshared ();
extern "C" void List_1_get_Capacity_m19073_gshared ();
extern "C" void List_1_set_Capacity_m19075_gshared ();
extern "C" void List_1_get_Count_m19077_gshared ();
extern "C" void List_1_get_Item_m19079_gshared ();
extern "C" void List_1_set_Item_m19081_gshared ();
extern "C" void Enumerator__ctor_m19082_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19083_gshared ();
extern "C" void Enumerator_Dispose_m19084_gshared ();
extern "C" void Enumerator_VerifyState_m19085_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m19086_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19087_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19088_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19089_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19090_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19091_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19092_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19093_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19094_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19095_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19096_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m19097_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m19098_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m19099_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19100_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m19101_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m19102_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19103_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19104_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19105_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19106_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19107_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m19108_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m19109_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m19110_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m19111_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m19112_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m19113_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m19114_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m19115_gshared ();
extern "C" void Collection_1__ctor_m19116_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19117_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19118_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m19119_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m19120_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m19121_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m19122_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m19123_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m19124_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m19125_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m19126_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m19127_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m19128_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m19129_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m19130_gshared ();
extern "C" void Collection_1_Add_m19131_gshared ();
extern "C" void Collection_1_Clear_m19132_gshared ();
extern "C" void Collection_1_ClearItems_m19133_gshared ();
extern "C" void Collection_1_Contains_m19134_gshared ();
extern "C" void Collection_1_CopyTo_m19135_gshared ();
extern "C" void Collection_1_GetEnumerator_m19136_gshared ();
extern "C" void Collection_1_IndexOf_m19137_gshared ();
extern "C" void Collection_1_Insert_m19138_gshared ();
extern "C" void Collection_1_InsertItem_m19139_gshared ();
extern "C" void Collection_1_Remove_m19140_gshared ();
extern "C" void Collection_1_RemoveAt_m19141_gshared ();
extern "C" void Collection_1_RemoveItem_m19142_gshared ();
extern "C" void Collection_1_get_Count_m19143_gshared ();
extern "C" void Collection_1_get_Item_m19144_gshared ();
extern "C" void Collection_1_set_Item_m19145_gshared ();
extern "C" void Collection_1_SetItem_m19146_gshared ();
extern "C" void Collection_1_IsValidItem_m19147_gshared ();
extern "C" void Collection_1_ConvertItem_m19148_gshared ();
extern "C" void Collection_1_CheckWritable_m19149_gshared ();
extern "C" void Collection_1_IsSynchronized_m19150_gshared ();
extern "C" void Collection_1_IsFixedSize_m19151_gshared ();
extern "C" void Predicate_1__ctor_m19152_gshared ();
extern "C" void Predicate_1_Invoke_m19153_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19154_gshared ();
extern "C" void Predicate_1_EndInvoke_m19155_gshared ();
extern "C" void Comparer_1__ctor_m19156_gshared ();
extern "C" void Comparer_1__cctor_m19157_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m19158_gshared ();
extern "C" void Comparer_1_get_Default_m19159_gshared ();
extern "C" void GenericComparer_1__ctor_m19160_gshared ();
extern "C" void GenericComparer_1_Compare_m19161_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m19162_gshared ();
extern "C" void DefaultComparer_Compare_m19163_gshared ();
extern "C" void Comparison_1__ctor_m19164_gshared ();
extern "C" void Comparison_1_Invoke_m19165_gshared ();
extern "C" void Comparison_1_BeginInvoke_m19166_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m19167_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19399_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19401_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19402_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19403_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19404_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19405_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19406_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19407_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19408_gshared ();
void* RuntimeInvoker_Color32_t427 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19409_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19410_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19411_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19412_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19413_gshared ();
void* RuntimeInvoker_Color_t98 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19893_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19894_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19895_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19896_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19897_gshared ();
void* RuntimeInvoker_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19898_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19899_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19900_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19901_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19902_gshared ();
void* RuntimeInvoker_WordData_t653 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19903_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19904_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19905_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19906_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19907_gshared ();
void* RuntimeInvoker_WordResultData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1__ctor_m19908_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19909_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m19910_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19911_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19912_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19913_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19914_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19915_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m19916_gshared ();
extern "C" void LinkedList_1_Clear_m19917_gshared ();
extern "C" void LinkedList_1_Contains_m19918_gshared ();
extern "C" void LinkedList_1_CopyTo_m19919_gshared ();
extern "C" void LinkedList_1_Find_m19920_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m19921_gshared ();
void* RuntimeInvoker_Enumerator_t3455 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1_GetObjectData_m19922_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m19923_gshared ();
extern "C" void LinkedList_1_Remove_m19924_gshared ();
extern "C" void LinkedList_1_RemoveLast_m19925_gshared ();
extern "C" void LinkedList_1_get_Count_m19926_gshared ();
extern "C" void LinkedListNode_1__ctor_m19927_gshared ();
extern "C" void LinkedListNode_1__ctor_m19928_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedListNode_1_Detach_m19929_gshared ();
extern "C" void LinkedListNode_1_get_List_m19930_gshared ();
extern "C" void Enumerator__ctor_m19931_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19932_gshared ();
extern "C" void Enumerator_get_Current_m19933_gshared ();
extern "C" void Enumerator_MoveNext_m19934_gshared ();
extern "C" void Enumerator_Dispose_m19935_gshared ();
extern "C" void Predicate_1_Invoke_m19936_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19937_gshared ();
void* RuntimeInvoker_Object_t_TrackableResultData_t648_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m19938_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19939_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19941_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19942_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19943_gshared ();
void* RuntimeInvoker_SmartTerrainRevisionData_t656 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19944_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19945_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19946_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19947_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19948_gshared ();
void* RuntimeInvoker_SurfaceData_t657 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19949_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19950_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19951_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19952_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19953_gshared ();
void* RuntimeInvoker_PropData_t658 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m20151_gshared ();
extern "C" void Dictionary_2__ctor_m20153_gshared ();
extern "C" void Dictionary_2__ctor_m20155_gshared ();
extern "C" void Dictionary_2__ctor_m20157_gshared ();
extern "C" void Dictionary_2__ctor_m20159_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m20165_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20167_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20169_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m20171_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20173_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20189_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195_gshared ();
extern "C" void Dictionary_2_get_Count_m20197_gshared ();
extern "C" void Dictionary_2_get_Item_m20199_gshared ();
void* RuntimeInvoker_UInt16_t460_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m20201_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m20203_gshared ();
extern "C" void Dictionary_2_InitArrays_m20205_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m20207_gshared ();
extern "C" void Dictionary_2_make_pair_m20209_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3475_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m20211_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m20213_gshared ();
void* RuntimeInvoker_UInt16_t460_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m20215_gshared ();
extern "C" void Dictionary_2_Resize_m20217_gshared ();
extern "C" void Dictionary_2_Add_m20219_gshared ();
extern "C" void Dictionary_2_Clear_m20221_gshared ();
extern "C" void Dictionary_2_ContainsKey_m20223_gshared ();
extern "C" void Dictionary_2_ContainsValue_m20225_gshared ();
extern "C" void Dictionary_2_GetObjectData_m20227_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m20229_gshared ();
extern "C" void Dictionary_2_Remove_m20231_gshared ();
extern "C" void Dictionary_2_TryGetValue_m20233_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_UInt16U26_t2726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m20235_gshared ();
extern "C" void Dictionary_2_get_Values_m20237_gshared ();
extern "C" void Dictionary_2_ToTKey_m20239_gshared ();
extern "C" void Dictionary_2_ToTValue_m20241_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m20243_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m20245_gshared ();
void* RuntimeInvoker_Enumerator_t3479 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m20247_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m20248_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20249_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20250_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20251_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20252_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3475 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m20253_gshared ();
extern "C" void KeyValuePair_2_get_Key_m20254_gshared ();
extern "C" void KeyValuePair_2_set_Key_m20255_gshared ();
extern "C" void KeyValuePair_2_get_Value_m20256_gshared ();
extern "C" void KeyValuePair_2_set_Value_m20257_gshared ();
extern "C" void KeyValuePair_2_ToString_m20258_gshared ();
extern "C" void KeyCollection__ctor_m20259_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20260_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20261_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20262_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20263_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20264_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m20265_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20266_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20267_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20268_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m20269_gshared ();
extern "C" void KeyCollection_CopyTo_m20270_gshared ();
extern "C" void KeyCollection_GetEnumerator_m20271_gshared ();
void* RuntimeInvoker_Enumerator_t3478 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m20272_gshared ();
extern "C" void Enumerator__ctor_m20273_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20274_gshared ();
extern "C" void Enumerator_Dispose_m20275_gshared ();
extern "C" void Enumerator_MoveNext_m20276_gshared ();
extern "C" void Enumerator_get_Current_m20277_gshared ();
extern "C" void Enumerator__ctor_m20278_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282_gshared ();
extern "C" void Enumerator_MoveNext_m20283_gshared ();
extern "C" void Enumerator_get_Current_m20284_gshared ();
extern "C" void Enumerator_get_CurrentKey_m20285_gshared ();
extern "C" void Enumerator_get_CurrentValue_m20286_gshared ();
extern "C" void Enumerator_VerifyState_m20287_gshared ();
extern "C" void Enumerator_VerifyCurrent_m20288_gshared ();
extern "C" void Enumerator_Dispose_m20289_gshared ();
extern "C" void Transform_1__ctor_m20290_gshared ();
extern "C" void Transform_1_Invoke_m20291_gshared ();
extern "C" void Transform_1_BeginInvoke_m20292_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m20293_gshared ();
extern "C" void ValueCollection__ctor_m20294_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20295_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20296_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20297_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20298_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20299_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20300_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20301_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20302_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20303_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m20304_gshared ();
extern "C" void ValueCollection_CopyTo_m20305_gshared ();
extern "C" void ValueCollection_GetEnumerator_m20306_gshared ();
void* RuntimeInvoker_Enumerator_t3482 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m20307_gshared ();
extern "C" void Enumerator__ctor_m20308_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20309_gshared ();
extern "C" void Enumerator_Dispose_m20310_gshared ();
extern "C" void Enumerator_MoveNext_m20311_gshared ();
extern "C" void Enumerator_get_Current_m20312_gshared ();
extern "C" void Transform_1__ctor_m20313_gshared ();
extern "C" void Transform_1_Invoke_m20314_gshared ();
extern "C" void Transform_1_BeginInvoke_m20315_gshared ();
extern "C" void Transform_1_EndInvoke_m20316_gshared ();
extern "C" void Transform_1__ctor_m20317_gshared ();
extern "C" void Transform_1_Invoke_m20318_gshared ();
extern "C" void Transform_1_BeginInvoke_m20319_gshared ();
extern "C" void Transform_1_EndInvoke_m20320_gshared ();
extern "C" void Transform_1__ctor_m20321_gshared ();
extern "C" void Transform_1_Invoke_m20322_gshared ();
extern "C" void Transform_1_BeginInvoke_m20323_gshared ();
extern "C" void Transform_1_EndInvoke_m20324_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3475_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m20325_gshared ();
extern "C" void ShimEnumerator_MoveNext_m20326_gshared ();
extern "C" void ShimEnumerator_get_Entry_m20327_gshared ();
extern "C" void ShimEnumerator_get_Key_m20328_gshared ();
extern "C" void ShimEnumerator_get_Value_m20329_gshared ();
extern "C" void ShimEnumerator_get_Current_m20330_gshared ();
extern "C" void EqualityComparer_1__ctor_m20331_gshared ();
extern "C" void EqualityComparer_1__cctor_m20332_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20333_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20334_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20335_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m20336_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m20337_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m20338_gshared ();
void* RuntimeInvoker_Boolean_t176_Int16_t540_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m20339_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20340_gshared ();
extern "C" void DefaultComparer_Equals_m20341_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20392_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20393_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20394_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20395_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20396_gshared ();
void* RuntimeInvoker_RectangleData_t602 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1__ctor_m21229_gshared ();
extern "C" void Action_1_Invoke_m21230_gshared ();
void* RuntimeInvoker_Void_t175_SmartTerrainInitializationInfo_t594 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_BeginInvoke_m21231_gshared ();
void* RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t594_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m21232_gshared ();
extern "C" void Dictionary_2__ctor_m21925_gshared ();
extern "C" void Dictionary_2__ctor_m21926_gshared ();
extern "C" void Dictionary_2__ctor_m21927_gshared ();
extern "C" void Dictionary_2__ctor_m21928_gshared ();
extern "C" void Dictionary_2__ctor_m21929_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21930_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21931_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21932_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21933_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21934_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m21935_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21936_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21937_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21938_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21939_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21940_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21941_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21942_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21943_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21944_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21945_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21946_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21947_gshared ();
extern "C" void Dictionary_2_get_Count_m21948_gshared ();
extern "C" void Dictionary_2_get_Item_m21949_gshared ();
extern "C" void Dictionary_2_set_Item_m21950_gshared ();
extern "C" void Dictionary_2_Init_m21951_gshared ();
extern "C" void Dictionary_2_InitArrays_m21952_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m21953_gshared ();
extern "C" void Dictionary_2_make_pair_m21954_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3563_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m21955_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m21956_gshared ();
void* RuntimeInvoker_TrackableResultData_t648_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m21957_gshared ();
extern "C" void Dictionary_2_Resize_m21958_gshared ();
extern "C" void Dictionary_2_Add_m21959_gshared ();
extern "C" void Dictionary_2_Clear_m21960_gshared ();
extern "C" void Dictionary_2_ContainsKey_m21961_gshared ();
extern "C" void Dictionary_2_ContainsValue_m21962_gshared ();
extern "C" void Dictionary_2_GetObjectData_m21963_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m21964_gshared ();
extern "C" void Dictionary_2_Remove_m21965_gshared ();
extern "C" void Dictionary_2_TryGetValue_m21966_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_TrackableResultDataU26_t4570 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m21967_gshared ();
extern "C" void Dictionary_2_get_Values_m21968_gshared ();
extern "C" void Dictionary_2_ToTKey_m21969_gshared ();
extern "C" void Dictionary_2_ToTValue_m21970_gshared ();
void* RuntimeInvoker_TrackableResultData_t648_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ContainsKeyValuePair_m21971_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m21972_gshared ();
void* RuntimeInvoker_Enumerator_t3567 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m21973_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21974_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21975_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21976_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21977_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21978_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3563 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m21979_gshared ();
extern "C" void KeyValuePair_2_get_Key_m21980_gshared ();
extern "C" void KeyValuePair_2_set_Key_m21981_gshared ();
extern "C" void KeyValuePair_2_get_Value_m21982_gshared ();
extern "C" void KeyValuePair_2_set_Value_m21983_gshared ();
extern "C" void KeyValuePair_2_ToString_m21984_gshared ();
extern "C" void KeyCollection__ctor_m21985_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21986_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21987_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21988_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21989_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21990_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m21991_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21992_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21993_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21994_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m21995_gshared ();
extern "C" void KeyCollection_CopyTo_m21996_gshared ();
extern "C" void KeyCollection_GetEnumerator_m21997_gshared ();
void* RuntimeInvoker_Enumerator_t3566 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m21998_gshared ();
extern "C" void Enumerator__ctor_m21999_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22000_gshared ();
extern "C" void Enumerator_Dispose_m22001_gshared ();
extern "C" void Enumerator_MoveNext_m22002_gshared ();
extern "C" void Enumerator_get_Current_m22003_gshared ();
extern "C" void Enumerator__ctor_m22004_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22005_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22006_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22007_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22008_gshared ();
extern "C" void Enumerator_MoveNext_m22009_gshared ();
extern "C" void Enumerator_get_Current_m22010_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22011_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22012_gshared ();
extern "C" void Enumerator_VerifyState_m22013_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22014_gshared ();
extern "C" void Enumerator_Dispose_m22015_gshared ();
extern "C" void Transform_1__ctor_m22016_gshared ();
extern "C" void Transform_1_Invoke_m22017_gshared ();
extern "C" void Transform_1_BeginInvoke_m22018_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_TrackableResultData_t648_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22019_gshared ();
extern "C" void ValueCollection__ctor_m22020_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22021_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22022_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22023_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22024_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22025_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22026_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22027_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22028_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22029_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22030_gshared ();
extern "C" void ValueCollection_CopyTo_m22031_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22032_gshared ();
void* RuntimeInvoker_Enumerator_t3570 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22033_gshared ();
extern "C" void Enumerator__ctor_m22034_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22035_gshared ();
extern "C" void Enumerator_Dispose_m22036_gshared ();
extern "C" void Enumerator_MoveNext_m22037_gshared ();
extern "C" void Enumerator_get_Current_m22038_gshared ();
extern "C" void Transform_1__ctor_m22039_gshared ();
extern "C" void Transform_1_Invoke_m22040_gshared ();
extern "C" void Transform_1_BeginInvoke_m22041_gshared ();
extern "C" void Transform_1_EndInvoke_m22042_gshared ();
extern "C" void Transform_1__ctor_m22043_gshared ();
extern "C" void Transform_1_Invoke_m22044_gshared ();
extern "C" void Transform_1_BeginInvoke_m22045_gshared ();
extern "C" void Transform_1_EndInvoke_m22046_gshared ();
extern "C" void Transform_1__ctor_m22047_gshared ();
extern "C" void Transform_1_Invoke_m22048_gshared ();
extern "C" void Transform_1_BeginInvoke_m22049_gshared ();
extern "C" void Transform_1_EndInvoke_m22050_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3563_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22051_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22052_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22053_gshared ();
extern "C" void ShimEnumerator_get_Key_m22054_gshared ();
extern "C" void ShimEnumerator_get_Value_m22055_gshared ();
extern "C" void ShimEnumerator_get_Current_m22056_gshared ();
extern "C" void EqualityComparer_1__ctor_m22057_gshared ();
extern "C" void EqualityComparer_1__cctor_m22058_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22059_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22060_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22061_gshared ();
extern "C" void DefaultComparer__ctor_m22062_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22063_gshared ();
extern "C" void DefaultComparer_Equals_m22064_gshared ();
void* RuntimeInvoker_Boolean_t176_TrackableResultData_t648_TrackableResultData_t648 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m22065_gshared ();
extern "C" void Dictionary_2__ctor_m22066_gshared ();
extern "C" void Dictionary_2__ctor_m22067_gshared ();
extern "C" void Dictionary_2__ctor_m22068_gshared ();
extern "C" void Dictionary_2__ctor_m22069_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22070_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22071_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m22072_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22073_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22074_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m22075_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22076_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22077_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22078_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22079_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22080_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22081_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22082_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22083_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22084_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22085_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22086_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22087_gshared ();
extern "C" void Dictionary_2_get_Count_m22088_gshared ();
extern "C" void Dictionary_2_get_Item_m22089_gshared ();
extern "C" void Dictionary_2_set_Item_m22090_gshared ();
extern "C" void Dictionary_2_Init_m22091_gshared ();
extern "C" void Dictionary_2_InitArrays_m22092_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22093_gshared ();
extern "C" void Dictionary_2_make_pair_m22094_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3578_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m22095_gshared ();
void* RuntimeInvoker_Int32_t135_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m22096_gshared ();
void* RuntimeInvoker_VirtualButtonData_t649_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m22097_gshared ();
extern "C" void Dictionary_2_Resize_m22098_gshared ();
extern "C" void Dictionary_2_Add_m22099_gshared ();
extern "C" void Dictionary_2_Clear_m22100_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22101_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22102_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22103_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22104_gshared ();
extern "C" void Dictionary_2_Remove_m22105_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22106_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_VirtualButtonDataU26_t4571 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m22107_gshared ();
extern "C" void Dictionary_2_get_Values_m22108_gshared ();
extern "C" void Dictionary_2_ToTKey_m22109_gshared ();
extern "C" void Dictionary_2_ToTValue_m22110_gshared ();
void* RuntimeInvoker_VirtualButtonData_t649_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ContainsKeyValuePair_m22111_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22112_gshared ();
void* RuntimeInvoker_Enumerator_t3583 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22113_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22114_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22115_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22116_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22117_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22118_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3578 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m22119_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22120_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22121_gshared ();
extern "C" void KeyValuePair_2_get_Value_m22122_gshared ();
void* RuntimeInvoker_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m22123_gshared ();
extern "C" void KeyValuePair_2_ToString_m22124_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22125_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22126_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22127_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22128_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22129_gshared ();
extern "C" void KeyCollection__ctor_m22130_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22131_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22132_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22133_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22134_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22135_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22136_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22137_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22138_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22139_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22140_gshared ();
extern "C" void KeyCollection_CopyTo_m22141_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22142_gshared ();
void* RuntimeInvoker_Enumerator_t3582 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22143_gshared ();
extern "C" void Enumerator__ctor_m22144_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22145_gshared ();
extern "C" void Enumerator_Dispose_m22146_gshared ();
extern "C" void Enumerator_MoveNext_m22147_gshared ();
extern "C" void Enumerator_get_Current_m22148_gshared ();
extern "C" void Enumerator__ctor_m22149_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22150_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22151_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22152_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22153_gshared ();
extern "C" void Enumerator_MoveNext_m22154_gshared ();
extern "C" void Enumerator_get_Current_m22155_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22156_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22157_gshared ();
extern "C" void Enumerator_VerifyState_m22158_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22159_gshared ();
extern "C" void Enumerator_Dispose_m22160_gshared ();
extern "C" void Transform_1__ctor_m22161_gshared ();
extern "C" void Transform_1_Invoke_m22162_gshared ();
extern "C" void Transform_1_BeginInvoke_m22163_gshared ();
void* RuntimeInvoker_Object_t_Int32_t135_VirtualButtonData_t649_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22164_gshared ();
extern "C" void ValueCollection__ctor_m22165_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22166_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22167_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22168_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22169_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22170_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22171_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22172_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22173_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22174_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22175_gshared ();
extern "C" void ValueCollection_CopyTo_m22176_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22177_gshared ();
void* RuntimeInvoker_Enumerator_t3586 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22178_gshared ();
extern "C" void Enumerator__ctor_m22179_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22180_gshared ();
extern "C" void Enumerator_Dispose_m22181_gshared ();
extern "C" void Enumerator_MoveNext_m22182_gshared ();
extern "C" void Enumerator_get_Current_m22183_gshared ();
extern "C" void Transform_1__ctor_m22184_gshared ();
extern "C" void Transform_1_Invoke_m22185_gshared ();
extern "C" void Transform_1_BeginInvoke_m22186_gshared ();
extern "C" void Transform_1_EndInvoke_m22187_gshared ();
extern "C" void Transform_1__ctor_m22188_gshared ();
extern "C" void Transform_1_Invoke_m22189_gshared ();
extern "C" void Transform_1_BeginInvoke_m22190_gshared ();
extern "C" void Transform_1_EndInvoke_m22191_gshared ();
extern "C" void Transform_1__ctor_m22192_gshared ();
extern "C" void Transform_1_Invoke_m22193_gshared ();
extern "C" void Transform_1_BeginInvoke_m22194_gshared ();
extern "C" void Transform_1_EndInvoke_m22195_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3578_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22196_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22197_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22198_gshared ();
extern "C" void ShimEnumerator_get_Key_m22199_gshared ();
extern "C" void ShimEnumerator_get_Value_m22200_gshared ();
extern "C" void ShimEnumerator_get_Current_m22201_gshared ();
extern "C" void EqualityComparer_1__ctor_m22202_gshared ();
extern "C" void EqualityComparer_1__cctor_m22203_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22204_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22205_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22206_gshared ();
extern "C" void DefaultComparer__ctor_m22207_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22208_gshared ();
extern "C" void DefaultComparer_Equals_m22209_gshared ();
void* RuntimeInvoker_Boolean_t176_VirtualButtonData_t649_VirtualButtonData_t649 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m22302_gshared ();
extern "C" void List_1__ctor_m22303_gshared ();
extern "C" void List_1__cctor_m22304_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22305_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22306_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m22307_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m22308_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m22309_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m22310_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m22311_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m22312_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22313_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m22314_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m22315_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m22316_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m22317_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m22318_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m22319_gshared ();
extern "C" void List_1_Add_m22320_gshared ();
extern "C" void List_1_GrowIfNeeded_m22321_gshared ();
extern "C" void List_1_AddCollection_m22322_gshared ();
extern "C" void List_1_AddEnumerable_m22323_gshared ();
extern "C" void List_1_AddRange_m22324_gshared ();
extern "C" void List_1_AsReadOnly_m22325_gshared ();
extern "C" void List_1_Clear_m22326_gshared ();
extern "C" void List_1_Contains_m22327_gshared ();
extern "C" void List_1_CopyTo_m22328_gshared ();
extern "C" void List_1_Find_m22329_gshared ();
void* RuntimeInvoker_TargetSearchResult_t726_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m22330_gshared ();
extern "C" void List_1_GetIndex_m22331_gshared ();
extern "C" void List_1_GetEnumerator_m22332_gshared ();
void* RuntimeInvoker_Enumerator_t3598 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m22333_gshared ();
extern "C" void List_1_Shift_m22334_gshared ();
extern "C" void List_1_CheckIndex_m22335_gshared ();
extern "C" void List_1_Insert_m22336_gshared ();
extern "C" void List_1_CheckCollection_m22337_gshared ();
extern "C" void List_1_Remove_m22338_gshared ();
extern "C" void List_1_RemoveAll_m22339_gshared ();
extern "C" void List_1_RemoveAt_m22340_gshared ();
extern "C" void List_1_Reverse_m22341_gshared ();
extern "C" void List_1_Sort_m22342_gshared ();
extern "C" void List_1_Sort_m22343_gshared ();
extern "C" void List_1_ToArray_m22344_gshared ();
extern "C" void List_1_TrimExcess_m22345_gshared ();
extern "C" void List_1_get_Capacity_m22346_gshared ();
extern "C" void List_1_set_Capacity_m22347_gshared ();
extern "C" void List_1_get_Count_m22348_gshared ();
extern "C" void List_1_get_Item_m22349_gshared ();
extern "C" void List_1_set_Item_m22350_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22351_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22352_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22353_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22354_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22355_gshared ();
void* RuntimeInvoker_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m22356_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22357_gshared ();
extern "C" void Enumerator_Dispose_m22358_gshared ();
extern "C" void Enumerator_VerifyState_m22359_gshared ();
extern "C" void Enumerator_MoveNext_m22360_gshared ();
extern "C" void Enumerator_get_Current_m22361_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m22362_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22363_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22364_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22365_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22366_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22367_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22368_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22369_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22370_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22371_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22372_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m22373_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m22374_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m22375_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22376_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m22377_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m22378_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22379_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22380_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22381_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22382_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22383_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m22384_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m22385_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m22386_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m22387_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m22388_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m22389_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m22390_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m22391_gshared ();
extern "C" void Collection_1__ctor_m22392_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22393_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22394_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m22395_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m22396_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m22397_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m22398_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m22399_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m22400_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m22401_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m22402_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m22403_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m22404_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m22405_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m22406_gshared ();
extern "C" void Collection_1_Add_m22407_gshared ();
extern "C" void Collection_1_Clear_m22408_gshared ();
extern "C" void Collection_1_ClearItems_m22409_gshared ();
extern "C" void Collection_1_Contains_m22410_gshared ();
extern "C" void Collection_1_CopyTo_m22411_gshared ();
extern "C" void Collection_1_GetEnumerator_m22412_gshared ();
extern "C" void Collection_1_IndexOf_m22413_gshared ();
extern "C" void Collection_1_Insert_m22414_gshared ();
extern "C" void Collection_1_InsertItem_m22415_gshared ();
extern "C" void Collection_1_Remove_m22416_gshared ();
extern "C" void Collection_1_RemoveAt_m22417_gshared ();
extern "C" void Collection_1_RemoveItem_m22418_gshared ();
extern "C" void Collection_1_get_Count_m22419_gshared ();
extern "C" void Collection_1_get_Item_m22420_gshared ();
extern "C" void Collection_1_set_Item_m22421_gshared ();
extern "C" void Collection_1_SetItem_m22422_gshared ();
extern "C" void Collection_1_IsValidItem_m22423_gshared ();
extern "C" void Collection_1_ConvertItem_m22424_gshared ();
extern "C" void Collection_1_CheckWritable_m22425_gshared ();
extern "C" void Collection_1_IsSynchronized_m22426_gshared ();
extern "C" void Collection_1_IsFixedSize_m22427_gshared ();
extern "C" void EqualityComparer_1__ctor_m22428_gshared ();
extern "C" void EqualityComparer_1__cctor_m22429_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22430_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22431_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22432_gshared ();
extern "C" void DefaultComparer__ctor_m22433_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22434_gshared ();
extern "C" void DefaultComparer_Equals_m22435_gshared ();
void* RuntimeInvoker_Boolean_t176_TargetSearchResult_t726_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m22436_gshared ();
extern "C" void Predicate_1_Invoke_m22437_gshared ();
extern "C" void Predicate_1_BeginInvoke_m22438_gshared ();
void* RuntimeInvoker_Object_t_TargetSearchResult_t726_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m22439_gshared ();
extern "C" void Comparer_1__ctor_m22440_gshared ();
extern "C" void Comparer_1__cctor_m22441_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22442_gshared ();
extern "C" void Comparer_1_get_Default_m22443_gshared ();
extern "C" void DefaultComparer__ctor_m22444_gshared ();
extern "C" void DefaultComparer_Compare_m22445_gshared ();
void* RuntimeInvoker_Int32_t135_TargetSearchResult_t726_TargetSearchResult_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m22446_gshared ();
extern "C" void Comparison_1_Invoke_m22447_gshared ();
extern "C" void Comparison_1_BeginInvoke_m22448_gshared ();
void* RuntimeInvoker_Object_t_TargetSearchResult_t726_TargetSearchResult_t726_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m22449_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22550_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22551_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22552_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22553_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22554_gshared ();
void* RuntimeInvoker_WebCamDevice_t885 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m22556_gshared ();
extern "C" void Dictionary_2__ctor_m22558_gshared ();
extern "C" void Dictionary_2__ctor_m22560_gshared ();
extern "C" void Dictionary_2__ctor_m22562_gshared ();
extern "C" void Dictionary_2__ctor_m22564_gshared ();
extern "C" void Dictionary_2__ctor_m22566_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m22572_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22574_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22576_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m22578_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22580_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22596_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602_gshared ();
extern "C" void Dictionary_2_get_Count_m22604_gshared ();
extern "C" void Dictionary_2_get_Item_m22606_gshared ();
void* RuntimeInvoker_ProfileData_t740_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m22608_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m22610_gshared ();
extern "C" void Dictionary_2_InitArrays_m22612_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22614_gshared ();
extern "C" void Dictionary_2_make_pair_m22616_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3617_Object_t_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m22618_gshared ();
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m22620_gshared ();
void* RuntimeInvoker_ProfileData_t740_Object_t_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m22622_gshared ();
extern "C" void Dictionary_2_Resize_m22624_gshared ();
extern "C" void Dictionary_2_Add_m22626_gshared ();
extern "C" void Dictionary_2_Clear_m22628_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22630_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22632_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22634_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22636_gshared ();
extern "C" void Dictionary_2_Remove_m22638_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22640_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_ProfileDataU26_t4572 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m22642_gshared ();
extern "C" void Dictionary_2_get_Values_m22644_gshared ();
extern "C" void Dictionary_2_ToTKey_m22646_gshared ();
extern "C" void Dictionary_2_ToTValue_m22648_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m22650_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22652_gshared ();
void* RuntimeInvoker_Enumerator_t3622 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22654_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22655_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22656_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22657_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22658_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22659_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3617 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m22660_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22661_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22662_gshared ();
extern "C" void KeyValuePair_2_get_Value_m22663_gshared ();
void* RuntimeInvoker_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m22664_gshared ();
extern "C" void KeyValuePair_2_ToString_m22665_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22666_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22667_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22668_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22669_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22670_gshared ();
extern "C" void KeyCollection__ctor_m22671_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22672_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22673_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22674_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22675_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22676_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22677_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22678_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22679_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22680_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22681_gshared ();
extern "C" void KeyCollection_CopyTo_m22682_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22683_gshared ();
void* RuntimeInvoker_Enumerator_t3621 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22684_gshared ();
extern "C" void Enumerator__ctor_m22685_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22686_gshared ();
extern "C" void Enumerator_Dispose_m22687_gshared ();
extern "C" void Enumerator_MoveNext_m22688_gshared ();
extern "C" void Enumerator_get_Current_m22689_gshared ();
extern "C" void Enumerator__ctor_m22690_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22691_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694_gshared ();
extern "C" void Enumerator_MoveNext_m22695_gshared ();
extern "C" void Enumerator_get_Current_m22696_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22697_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22698_gshared ();
extern "C" void Enumerator_VerifyState_m22699_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22700_gshared ();
extern "C" void Enumerator_Dispose_m22701_gshared ();
extern "C" void Transform_1__ctor_m22702_gshared ();
extern "C" void Transform_1_Invoke_m22703_gshared ();
extern "C" void Transform_1_BeginInvoke_m22704_gshared ();
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t740_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22705_gshared ();
extern "C" void ValueCollection__ctor_m22706_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22707_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22708_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22709_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22710_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22711_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22712_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22713_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22714_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22715_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22716_gshared ();
extern "C" void ValueCollection_CopyTo_m22717_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22718_gshared ();
void* RuntimeInvoker_Enumerator_t3625 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22719_gshared ();
extern "C" void Enumerator__ctor_m22720_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22721_gshared ();
extern "C" void Enumerator_Dispose_m22722_gshared ();
extern "C" void Enumerator_MoveNext_m22723_gshared ();
extern "C" void Enumerator_get_Current_m22724_gshared ();
extern "C" void Transform_1__ctor_m22725_gshared ();
extern "C" void Transform_1_Invoke_m22726_gshared ();
extern "C" void Transform_1_BeginInvoke_m22727_gshared ();
extern "C" void Transform_1_EndInvoke_m22728_gshared ();
extern "C" void Transform_1__ctor_m22729_gshared ();
extern "C" void Transform_1_Invoke_m22730_gshared ();
extern "C" void Transform_1_BeginInvoke_m22731_gshared ();
extern "C" void Transform_1_EndInvoke_m22732_gshared ();
extern "C" void Transform_1__ctor_m22733_gshared ();
extern "C" void Transform_1_Invoke_m22734_gshared ();
extern "C" void Transform_1_BeginInvoke_m22735_gshared ();
extern "C" void Transform_1_EndInvoke_m22736_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3617_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22737_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22738_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22739_gshared ();
extern "C" void ShimEnumerator_get_Key_m22740_gshared ();
extern "C" void ShimEnumerator_get_Value_m22741_gshared ();
extern "C" void ShimEnumerator_get_Current_m22742_gshared ();
extern "C" void EqualityComparer_1__ctor_m22743_gshared ();
extern "C" void EqualityComparer_1__cctor_m22744_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22745_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22746_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22747_gshared ();
extern "C" void DefaultComparer__ctor_m22748_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22749_gshared ();
extern "C" void DefaultComparer_Equals_m22750_gshared ();
void* RuntimeInvoker_Boolean_t176_ProfileData_t740_ProfileData_t740 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m23312_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23313_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23314_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23315_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23316_gshared ();
void* RuntimeInvoker_Link_t3666 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3__ctor_m23421_gshared ();
extern "C" void TweenerCore_3_Reset_m23422_gshared ();
extern "C" void TweenerCore_3_Validate_m23423_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23424_gshared ();
extern "C" void TweenerCore_3_Startup_m23425_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23426_gshared ();
extern "C" void DOGetter_1__ctor_m23427_gshared ();
extern "C" void DOGetter_1_Invoke_m23428_gshared ();
void* RuntimeInvoker_UInt32_t1081 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23429_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23430_gshared ();
void* RuntimeInvoker_UInt32_t1081_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23431_gshared ();
extern "C" void DOSetter_1_Invoke_m23432_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23433_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23434_gshared ();
extern "C" void TweenerCore_3__ctor_m23435_gshared ();
extern "C" void TweenerCore_3_Reset_m23436_gshared ();
extern "C" void TweenerCore_3_Validate_m23437_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23438_gshared ();
extern "C" void TweenerCore_3_Startup_m23439_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23440_gshared ();
extern "C" void DOGetter_1__ctor_m23441_gshared ();
extern "C" void DOGetter_1_Invoke_m23442_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23443_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23444_gshared ();
void* RuntimeInvoker_Vector2_t19_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23445_gshared ();
extern "C" void DOSetter_1_Invoke_m23446_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23447_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23448_gshared ();
extern "C" void TweenCallback_1__ctor_m23542_gshared ();
extern "C" void TweenCallback_1_Invoke_m23543_gshared ();
extern "C" void TweenCallback_1_BeginInvoke_m23544_gshared ();
extern "C" void TweenCallback_1_EndInvoke_m23545_gshared ();
extern "C" void TweenerCore_3__ctor_m23740_gshared ();
extern "C" void TweenerCore_3_Reset_m23741_gshared ();
extern "C" void TweenerCore_3_Validate_m23742_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23743_gshared ();
extern "C" void TweenerCore_3_Startup_m23744_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23745_gshared ();
extern "C" void TweenerCore_3__ctor_m23760_gshared ();
extern "C" void TweenerCore_3_Reset_m23761_gshared ();
extern "C" void TweenerCore_3_Validate_m23762_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23763_gshared ();
extern "C" void TweenerCore_3_Startup_m23764_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23765_gshared ();
extern "C" void DOGetter_1__ctor_m23766_gshared ();
extern "C" void DOGetter_1_Invoke_m23767_gshared ();
void* RuntimeInvoker_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23768_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23769_gshared ();
void* RuntimeInvoker_Color2_t1006_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23770_gshared ();
extern "C" void DOSetter_1_Invoke_m23771_gshared ();
void* RuntimeInvoker_Void_t175_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m23772_gshared ();
void* RuntimeInvoker_Object_t_Color2_t1006_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23773_gshared ();
extern "C" void TweenerCore_3__ctor_m23787_gshared ();
extern "C" void TweenerCore_3_Reset_m23788_gshared ();
extern "C" void TweenerCore_3_Validate_m23789_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23790_gshared ();
extern "C" void TweenerCore_3_Startup_m23791_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23792_gshared ();
extern "C" void DOGetter_1__ctor_m23793_gshared ();
extern "C" void DOGetter_1_Invoke_m23794_gshared ();
void* RuntimeInvoker_Rect_t132 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23795_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23796_gshared ();
void* RuntimeInvoker_Rect_t132_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23797_gshared ();
extern "C" void DOSetter_1_Invoke_m23798_gshared ();
void* RuntimeInvoker_Void_t175_Rect_t132 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m23799_gshared ();
void* RuntimeInvoker_Object_t_Rect_t132_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23800_gshared ();
extern "C" void TweenerCore_3__ctor_m23801_gshared ();
extern "C" void TweenerCore_3_Reset_m23802_gshared ();
extern "C" void TweenerCore_3_Validate_m23803_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23804_gshared ();
extern "C" void TweenerCore_3_Startup_m23805_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23806_gshared ();
extern "C" void DOGetter_1__ctor_m23807_gshared ();
extern "C" void DOGetter_1_Invoke_m23808_gshared ();
void* RuntimeInvoker_UInt64_t1097 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23809_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23810_gshared ();
void* RuntimeInvoker_UInt64_t1097_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23811_gshared ();
extern "C" void DOSetter_1_Invoke_m23812_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23813_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1098_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23814_gshared ();
extern "C" void TweenerCore_3__ctor_m23815_gshared ();
extern "C" void TweenerCore_3_Reset_m23816_gshared ();
extern "C" void TweenerCore_3_Validate_m23817_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23818_gshared ();
extern "C" void TweenerCore_3_Startup_m23819_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23820_gshared ();
extern "C" void DOGetter_1__ctor_m23821_gshared ();
extern "C" void DOGetter_1_Invoke_m23822_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23823_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23824_gshared ();
extern "C" void DOSetter_1__ctor_m23825_gshared ();
extern "C" void DOSetter_1_Invoke_m23826_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23827_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23828_gshared ();
extern "C" void TweenerCore_3__ctor_m23830_gshared ();
extern "C" void TweenerCore_3_Reset_m23831_gshared ();
extern "C" void TweenerCore_3_Validate_m23832_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23833_gshared ();
extern "C" void TweenerCore_3_Startup_m23834_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23835_gshared ();
extern "C" void List_1__ctor_m23852_gshared ();
extern "C" void List_1__ctor_m23854_gshared ();
extern "C" void List_1__cctor_m23856_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23860_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m23864_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m23866_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m23868_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m23870_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m23872_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m23884_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m23886_gshared ();
extern "C" void List_1_Add_m23888_gshared ();
extern "C" void List_1_GrowIfNeeded_m23890_gshared ();
extern "C" void List_1_AddCollection_m23892_gshared ();
extern "C" void List_1_AddEnumerable_m23894_gshared ();
extern "C" void List_1_AddRange_m23896_gshared ();
extern "C" void List_1_AsReadOnly_m23898_gshared ();
extern "C" void List_1_Clear_m23900_gshared ();
extern "C" void List_1_Contains_m23902_gshared ();
extern "C" void List_1_CopyTo_m23904_gshared ();
extern "C" void List_1_Find_m23906_gshared ();
extern "C" void List_1_CheckMatch_m23908_gshared ();
extern "C" void List_1_GetIndex_m23910_gshared ();
extern "C" void List_1_GetEnumerator_m23912_gshared ();
void* RuntimeInvoker_Enumerator_t3701 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m23914_gshared ();
extern "C" void List_1_Shift_m23916_gshared ();
extern "C" void List_1_CheckIndex_m23918_gshared ();
extern "C" void List_1_Insert_m23920_gshared ();
extern "C" void List_1_CheckCollection_m23922_gshared ();
extern "C" void List_1_Remove_m23924_gshared ();
extern "C" void List_1_RemoveAll_m23926_gshared ();
extern "C" void List_1_RemoveAt_m23928_gshared ();
extern "C" void List_1_Reverse_m23930_gshared ();
extern "C" void List_1_Sort_m23932_gshared ();
extern "C" void List_1_Sort_m23934_gshared ();
extern "C" void List_1_ToArray_m23936_gshared ();
extern "C" void List_1_TrimExcess_m23938_gshared ();
extern "C" void List_1_get_Capacity_m23940_gshared ();
extern "C" void List_1_set_Capacity_m23942_gshared ();
extern "C" void List_1_get_Count_m23944_gshared ();
extern "C" void List_1_get_Item_m23946_gshared ();
extern "C" void List_1_set_Item_m23948_gshared ();
extern "C" void Enumerator__ctor_m23949_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared ();
extern "C" void Enumerator_Dispose_m23951_gshared ();
extern "C" void Enumerator_VerifyState_m23952_gshared ();
extern "C" void Enumerator_MoveNext_m23953_gshared ();
extern "C" void Enumerator_get_Current_m23954_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m23955_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m23966_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23967_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m23968_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23970_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23971_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m23979_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m23980_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m23981_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m23982_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m23983_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m23984_gshared ();
extern "C" void Collection_1__ctor_m23985_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23987_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m23989_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m23990_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m23991_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m23992_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m23993_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m23995_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m23996_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m23997_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m23998_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m23999_gshared ();
extern "C" void Collection_1_Add_m24000_gshared ();
extern "C" void Collection_1_Clear_m24001_gshared ();
extern "C" void Collection_1_ClearItems_m24002_gshared ();
extern "C" void Collection_1_Contains_m24003_gshared ();
extern "C" void Collection_1_CopyTo_m24004_gshared ();
extern "C" void Collection_1_GetEnumerator_m24005_gshared ();
extern "C" void Collection_1_IndexOf_m24006_gshared ();
extern "C" void Collection_1_Insert_m24007_gshared ();
extern "C" void Collection_1_InsertItem_m24008_gshared ();
extern "C" void Collection_1_Remove_m24009_gshared ();
extern "C" void Collection_1_RemoveAt_m24010_gshared ();
extern "C" void Collection_1_RemoveItem_m24011_gshared ();
extern "C" void Collection_1_get_Count_m24012_gshared ();
extern "C" void Collection_1_get_Item_m24013_gshared ();
extern "C" void Collection_1_set_Item_m24014_gshared ();
extern "C" void Collection_1_SetItem_m24015_gshared ();
extern "C" void Collection_1_IsValidItem_m24016_gshared ();
extern "C" void Collection_1_ConvertItem_m24017_gshared ();
extern "C" void Collection_1_CheckWritable_m24018_gshared ();
extern "C" void Collection_1_IsSynchronized_m24019_gshared ();
extern "C" void Collection_1_IsFixedSize_m24020_gshared ();
extern "C" void Predicate_1__ctor_m24021_gshared ();
extern "C" void Predicate_1_Invoke_m24022_gshared ();
extern "C" void Predicate_1_BeginInvoke_m24023_gshared ();
void* RuntimeInvoker_Object_t_Int16_t540_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m24024_gshared ();
extern "C" void Comparer_1__ctor_m24025_gshared ();
extern "C" void Comparer_1__cctor_m24026_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m24027_gshared ();
extern "C" void Comparer_1_get_Default_m24028_gshared ();
extern "C" void GenericComparer_1__ctor_m24029_gshared ();
extern "C" void GenericComparer_1_Compare_m24030_gshared ();
void* RuntimeInvoker_Int32_t135_Int16_t540_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m24031_gshared ();
extern "C" void DefaultComparer_Compare_m24032_gshared ();
extern "C" void Comparison_1__ctor_m24033_gshared ();
extern "C" void Comparison_1_Invoke_m24034_gshared ();
extern "C" void Comparison_1_BeginInvoke_m24035_gshared ();
void* RuntimeInvoker_Object_t_Int16_t540_Int16_t540_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m24036_gshared ();
extern "C" void TweenerCore_3__ctor_m24081_gshared ();
extern "C" void TweenerCore_3_Reset_m24082_gshared ();
extern "C" void TweenerCore_3_Validate_m24083_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m24084_gshared ();
extern "C" void TweenerCore_3_Startup_m24085_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m24086_gshared ();
extern "C" void DOGetter_1__ctor_m24087_gshared ();
extern "C" void DOGetter_1_Invoke_m24088_gshared ();
void* RuntimeInvoker_Vector4_t419 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m24089_gshared ();
extern "C" void DOGetter_1_EndInvoke_m24090_gshared ();
void* RuntimeInvoker_Vector4_t419_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m24091_gshared ();
extern "C" void DOSetter_1_Invoke_m24092_gshared ();
void* RuntimeInvoker_Void_t175_Vector4_t419 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m24093_gshared ();
void* RuntimeInvoker_Object_t_Vector4_t419_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m24094_gshared ();
extern "C" void TweenerCore_3__ctor_m24095_gshared ();
extern "C" void TweenerCore_3_Reset_m24096_gshared ();
extern "C" void TweenerCore_3_Validate_m24097_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m24098_gshared ();
extern "C" void TweenerCore_3_Startup_m24099_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m24100_gshared ();
extern "C" void DOGetter_1__ctor_m24101_gshared ();
extern "C" void DOGetter_1_Invoke_m24102_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m24103_gshared ();
extern "C" void DOGetter_1_EndInvoke_m24104_gshared ();
void* RuntimeInvoker_Color_t98_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m24105_gshared ();
extern "C" void DOSetter_1_Invoke_m24106_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m24107_gshared ();
extern "C" void DOSetter_1_EndInvoke_m24108_gshared ();
extern "C" void TweenerCore_3__ctor_m24109_gshared ();
extern "C" void TweenerCore_3_Reset_m24110_gshared ();
extern "C" void TweenerCore_3_Validate_m24111_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m24112_gshared ();
extern "C" void TweenerCore_3_Startup_m24113_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m24114_gshared ();
extern "C" void DOGetter_1__ctor_m24115_gshared ();
extern "C" void DOGetter_1_Invoke_m24116_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m24117_gshared ();
extern "C" void DOGetter_1_EndInvoke_m24118_gshared ();
extern "C" void DOSetter_1__ctor_m24119_gshared ();
extern "C" void DOSetter_1_Invoke_m24120_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m24121_gshared ();
extern "C" void DOSetter_1_EndInvoke_m24122_gshared ();
extern "C" void TweenerCore_3__ctor_m24123_gshared ();
extern "C" void TweenerCore_3_Reset_m24124_gshared ();
extern "C" void TweenerCore_3_Validate_m24125_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m24126_gshared ();
extern "C" void TweenerCore_3_Startup_m24127_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m24128_gshared ();
extern "C" void DOGetter_1__ctor_m24129_gshared ();
extern "C" void DOGetter_1_Invoke_m24130_gshared ();
void* RuntimeInvoker_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m24131_gshared ();
extern "C" void DOGetter_1_EndInvoke_m24132_gshared ();
void* RuntimeInvoker_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m24133_gshared ();
extern "C" void DOSetter_1_Invoke_m24134_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m24135_gshared ();
extern "C" void DOSetter_1_EndInvoke_m24136_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24273_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24274_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24275_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24276_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24277_gshared ();
void* RuntimeInvoker_GcAchievementData_t1311 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24283_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24285_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24286_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24287_gshared ();
void* RuntimeInvoker_GcScoreData_t1312 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24885_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24887_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24888_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24889_gshared ();
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24983_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24984_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24985_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24986_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24987_gshared ();
void* RuntimeInvoker_Keyframe_t1242 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m24988_gshared ();
extern "C" void List_1__ctor_m24989_gshared ();
extern "C" void List_1__cctor_m24990_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24991_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24992_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m24993_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m24994_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m24995_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m24996_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m24997_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m24998_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24999_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m25000_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m25001_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m25002_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m25003_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m25004_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m25005_gshared ();
extern "C" void List_1_Add_m25006_gshared ();
extern "C" void List_1_GrowIfNeeded_m25007_gshared ();
extern "C" void List_1_AddCollection_m25008_gshared ();
extern "C" void List_1_AddEnumerable_m25009_gshared ();
extern "C" void List_1_AddRange_m25010_gshared ();
extern "C" void List_1_AsReadOnly_m25011_gshared ();
extern "C" void List_1_Clear_m25012_gshared ();
extern "C" void List_1_Contains_m25013_gshared ();
extern "C" void List_1_CopyTo_m25014_gshared ();
extern "C" void List_1_Find_m25015_gshared ();
void* RuntimeInvoker_UICharInfo_t466_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m25016_gshared ();
extern "C" void List_1_GetIndex_m25017_gshared ();
extern "C" void List_1_GetEnumerator_m25018_gshared ();
void* RuntimeInvoker_Enumerator_t3769 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m25019_gshared ();
extern "C" void List_1_Shift_m25020_gshared ();
extern "C" void List_1_CheckIndex_m25021_gshared ();
extern "C" void List_1_Insert_m25022_gshared ();
extern "C" void List_1_CheckCollection_m25023_gshared ();
extern "C" void List_1_Remove_m25024_gshared ();
extern "C" void List_1_RemoveAll_m25025_gshared ();
extern "C" void List_1_RemoveAt_m25026_gshared ();
extern "C" void List_1_Reverse_m25027_gshared ();
extern "C" void List_1_Sort_m25028_gshared ();
extern "C" void List_1_Sort_m25029_gshared ();
extern "C" void List_1_ToArray_m25030_gshared ();
extern "C" void List_1_TrimExcess_m25031_gshared ();
extern "C" void List_1_get_Capacity_m25032_gshared ();
extern "C" void List_1_set_Capacity_m25033_gshared ();
extern "C" void List_1_get_Count_m25034_gshared ();
extern "C" void List_1_get_Item_m25035_gshared ();
extern "C" void List_1_set_Item_m25036_gshared ();
extern "C" void Enumerator__ctor_m25037_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25038_gshared ();
extern "C" void Enumerator_Dispose_m25039_gshared ();
extern "C" void Enumerator_VerifyState_m25040_gshared ();
extern "C" void Enumerator_MoveNext_m25041_gshared ();
extern "C" void Enumerator_get_Current_m25042_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m25043_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25044_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25045_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25046_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25047_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25048_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25049_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25050_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25051_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25052_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25053_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m25054_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m25055_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m25056_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25057_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m25058_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m25059_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25060_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25061_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25062_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25063_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25064_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m25065_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m25066_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m25067_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m25068_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m25069_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m25070_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m25071_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m25072_gshared ();
extern "C" void Collection_1__ctor_m25073_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25074_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25075_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m25076_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m25077_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m25078_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m25079_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m25080_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m25081_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m25082_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m25083_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m25084_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m25085_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m25086_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m25087_gshared ();
extern "C" void Collection_1_Add_m25088_gshared ();
extern "C" void Collection_1_Clear_m25089_gshared ();
extern "C" void Collection_1_ClearItems_m25090_gshared ();
extern "C" void Collection_1_Contains_m25091_gshared ();
extern "C" void Collection_1_CopyTo_m25092_gshared ();
extern "C" void Collection_1_GetEnumerator_m25093_gshared ();
extern "C" void Collection_1_IndexOf_m25094_gshared ();
extern "C" void Collection_1_Insert_m25095_gshared ();
extern "C" void Collection_1_InsertItem_m25096_gshared ();
extern "C" void Collection_1_Remove_m25097_gshared ();
extern "C" void Collection_1_RemoveAt_m25098_gshared ();
extern "C" void Collection_1_RemoveItem_m25099_gshared ();
extern "C" void Collection_1_get_Count_m25100_gshared ();
extern "C" void Collection_1_get_Item_m25101_gshared ();
extern "C" void Collection_1_set_Item_m25102_gshared ();
extern "C" void Collection_1_SetItem_m25103_gshared ();
extern "C" void Collection_1_IsValidItem_m25104_gshared ();
extern "C" void Collection_1_ConvertItem_m25105_gshared ();
extern "C" void Collection_1_CheckWritable_m25106_gshared ();
extern "C" void Collection_1_IsSynchronized_m25107_gshared ();
extern "C" void Collection_1_IsFixedSize_m25108_gshared ();
extern "C" void EqualityComparer_1__ctor_m25109_gshared ();
extern "C" void EqualityComparer_1__cctor_m25110_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25111_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25112_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25113_gshared ();
extern "C" void DefaultComparer__ctor_m25114_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25115_gshared ();
extern "C" void DefaultComparer_Equals_m25116_gshared ();
void* RuntimeInvoker_Boolean_t176_UICharInfo_t466_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m25117_gshared ();
extern "C" void Predicate_1_Invoke_m25118_gshared ();
extern "C" void Predicate_1_BeginInvoke_m25119_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m25120_gshared ();
extern "C" void Comparer_1__ctor_m25121_gshared ();
extern "C" void Comparer_1__cctor_m25122_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m25123_gshared ();
extern "C" void Comparer_1_get_Default_m25124_gshared ();
extern "C" void DefaultComparer__ctor_m25125_gshared ();
extern "C" void DefaultComparer_Compare_m25126_gshared ();
void* RuntimeInvoker_Int32_t135_UICharInfo_t466_UICharInfo_t466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m25127_gshared ();
extern "C" void Comparison_1_Invoke_m25128_gshared ();
extern "C" void Comparison_1_BeginInvoke_m25129_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t466_UICharInfo_t466_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m25130_gshared ();
extern "C" void List_1__ctor_m25131_gshared ();
extern "C" void List_1__ctor_m25132_gshared ();
extern "C" void List_1__cctor_m25133_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25134_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25135_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m25136_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m25137_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m25138_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m25139_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m25140_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m25141_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25142_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m25143_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m25144_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m25145_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m25146_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m25147_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m25148_gshared ();
extern "C" void List_1_Add_m25149_gshared ();
extern "C" void List_1_GrowIfNeeded_m25150_gshared ();
extern "C" void List_1_AddCollection_m25151_gshared ();
extern "C" void List_1_AddEnumerable_m25152_gshared ();
extern "C" void List_1_AddRange_m25153_gshared ();
extern "C" void List_1_AsReadOnly_m25154_gshared ();
extern "C" void List_1_Clear_m25155_gshared ();
extern "C" void List_1_Contains_m25156_gshared ();
extern "C" void List_1_CopyTo_m25157_gshared ();
extern "C" void List_1_Find_m25158_gshared ();
void* RuntimeInvoker_UILineInfo_t464_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m25159_gshared ();
extern "C" void List_1_GetIndex_m25160_gshared ();
extern "C" void List_1_GetEnumerator_m25161_gshared ();
void* RuntimeInvoker_Enumerator_t3778 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m25162_gshared ();
extern "C" void List_1_Shift_m25163_gshared ();
extern "C" void List_1_CheckIndex_m25164_gshared ();
extern "C" void List_1_Insert_m25165_gshared ();
extern "C" void List_1_CheckCollection_m25166_gshared ();
extern "C" void List_1_Remove_m25167_gshared ();
extern "C" void List_1_RemoveAll_m25168_gshared ();
extern "C" void List_1_RemoveAt_m25169_gshared ();
extern "C" void List_1_Reverse_m25170_gshared ();
extern "C" void List_1_Sort_m25171_gshared ();
extern "C" void List_1_Sort_m25172_gshared ();
extern "C" void List_1_ToArray_m25173_gshared ();
extern "C" void List_1_TrimExcess_m25174_gshared ();
extern "C" void List_1_get_Capacity_m25175_gshared ();
extern "C" void List_1_set_Capacity_m25176_gshared ();
extern "C" void List_1_get_Count_m25177_gshared ();
extern "C" void List_1_get_Item_m25178_gshared ();
extern "C" void List_1_set_Item_m25179_gshared ();
extern "C" void Enumerator__ctor_m25180_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25181_gshared ();
extern "C" void Enumerator_Dispose_m25182_gshared ();
extern "C" void Enumerator_VerifyState_m25183_gshared ();
extern "C" void Enumerator_MoveNext_m25184_gshared ();
extern "C" void Enumerator_get_Current_m25185_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m25186_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25187_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25188_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25189_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25190_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25191_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25192_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25193_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25194_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25195_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25196_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m25197_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m25198_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m25199_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25200_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m25201_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m25202_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25203_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25204_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25205_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25206_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25207_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m25208_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m25209_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m25210_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m25211_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m25212_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m25213_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m25214_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m25215_gshared ();
extern "C" void Collection_1__ctor_m25216_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25217_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25218_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m25219_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m25220_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m25221_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m25222_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m25223_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m25224_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m25225_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m25226_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m25227_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m25228_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m25229_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m25230_gshared ();
extern "C" void Collection_1_Add_m25231_gshared ();
extern "C" void Collection_1_Clear_m25232_gshared ();
extern "C" void Collection_1_ClearItems_m25233_gshared ();
extern "C" void Collection_1_Contains_m25234_gshared ();
extern "C" void Collection_1_CopyTo_m25235_gshared ();
extern "C" void Collection_1_GetEnumerator_m25236_gshared ();
extern "C" void Collection_1_IndexOf_m25237_gshared ();
extern "C" void Collection_1_Insert_m25238_gshared ();
extern "C" void Collection_1_InsertItem_m25239_gshared ();
extern "C" void Collection_1_Remove_m25240_gshared ();
extern "C" void Collection_1_RemoveAt_m25241_gshared ();
extern "C" void Collection_1_RemoveItem_m25242_gshared ();
extern "C" void Collection_1_get_Count_m25243_gshared ();
extern "C" void Collection_1_get_Item_m25244_gshared ();
extern "C" void Collection_1_set_Item_m25245_gshared ();
extern "C" void Collection_1_SetItem_m25246_gshared ();
extern "C" void Collection_1_IsValidItem_m25247_gshared ();
extern "C" void Collection_1_ConvertItem_m25248_gshared ();
extern "C" void Collection_1_CheckWritable_m25249_gshared ();
extern "C" void Collection_1_IsSynchronized_m25250_gshared ();
extern "C" void Collection_1_IsFixedSize_m25251_gshared ();
extern "C" void EqualityComparer_1__ctor_m25252_gshared ();
extern "C" void EqualityComparer_1__cctor_m25253_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25254_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25255_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25256_gshared ();
extern "C" void DefaultComparer__ctor_m25257_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25258_gshared ();
extern "C" void DefaultComparer_Equals_m25259_gshared ();
void* RuntimeInvoker_Boolean_t176_UILineInfo_t464_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m25260_gshared ();
extern "C" void Predicate_1_Invoke_m25261_gshared ();
extern "C" void Predicate_1_BeginInvoke_m25262_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t464_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m25263_gshared ();
extern "C" void Comparer_1__ctor_m25264_gshared ();
extern "C" void Comparer_1__cctor_m25265_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m25266_gshared ();
extern "C" void Comparer_1_get_Default_m25267_gshared ();
extern "C" void DefaultComparer__ctor_m25268_gshared ();
extern "C" void DefaultComparer_Compare_m25269_gshared ();
void* RuntimeInvoker_Int32_t135_UILineInfo_t464_UILineInfo_t464 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m25270_gshared ();
extern "C" void Comparison_1_Invoke_m25271_gshared ();
extern "C" void Comparison_1_BeginInvoke_m25272_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t464_UILineInfo_t464_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m25273_gshared ();
extern "C" void Dictionary_2__ctor_m25275_gshared ();
extern "C" void Dictionary_2__ctor_m25277_gshared ();
extern "C" void Dictionary_2__ctor_m25279_gshared ();
extern "C" void Dictionary_2__ctor_m25281_gshared ();
extern "C" void Dictionary_2__ctor_m25283_gshared ();
extern "C" void Dictionary_2__ctor_m25285_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25287_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25289_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25291_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25293_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25295_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m25297_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25299_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25301_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25303_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25305_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25307_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25309_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25311_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25313_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25315_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25317_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25319_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25321_gshared ();
extern "C" void Dictionary_2_get_Count_m25323_gshared ();
extern "C" void Dictionary_2_get_Item_m25325_gshared ();
extern "C" void Dictionary_2_set_Item_m25327_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m25329_gshared ();
extern "C" void Dictionary_2_InitArrays_m25331_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25333_gshared ();
extern "C" void Dictionary_2_make_pair_m25335_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m25337_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m25339_gshared ();
void* RuntimeInvoker_Int64_t1098_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m25341_gshared ();
extern "C" void Dictionary_2_Resize_m25343_gshared ();
extern "C" void Dictionary_2_Add_m25345_gshared ();
extern "C" void Dictionary_2_Clear_m25347_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25349_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25351_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25353_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25355_gshared ();
extern "C" void Dictionary_2_Remove_m25357_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25359_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_Int64U26_t2709 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m25361_gshared ();
extern "C" void Dictionary_2_get_Values_m25363_gshared ();
extern "C" void Dictionary_2_ToTKey_m25365_gshared ();
extern "C" void Dictionary_2_ToTValue_m25367_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25369_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25371_gshared ();
void* RuntimeInvoker_Enumerator_t3795 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25373_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25374_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25375_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25376_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25377_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25378_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25379_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25380_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25381_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25382_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25383_gshared ();
extern "C" void KeyValuePair_2_ToString_m25384_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25385_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25386_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25387_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25388_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25389_gshared ();
extern "C" void KeyCollection__ctor_m25390_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25391_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25392_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25393_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25394_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25395_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25396_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25397_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25398_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25399_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25400_gshared ();
extern "C" void KeyCollection_CopyTo_m25401_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25402_gshared ();
void* RuntimeInvoker_Enumerator_t3794 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m25403_gshared ();
extern "C" void Enumerator__ctor_m25404_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25405_gshared ();
extern "C" void Enumerator_Dispose_m25406_gshared ();
extern "C" void Enumerator_MoveNext_m25407_gshared ();
extern "C" void Enumerator_get_Current_m25408_gshared ();
extern "C" void Enumerator__ctor_m25409_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25410_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413_gshared ();
extern "C" void Enumerator_MoveNext_m25414_gshared ();
extern "C" void Enumerator_get_Current_m25415_gshared ();
extern "C" void Enumerator_get_CurrentKey_m25416_gshared ();
extern "C" void Enumerator_get_CurrentValue_m25417_gshared ();
extern "C" void Enumerator_VerifyState_m25418_gshared ();
extern "C" void Enumerator_VerifyCurrent_m25419_gshared ();
extern "C" void Enumerator_Dispose_m25420_gshared ();
extern "C" void Transform_1__ctor_m25421_gshared ();
extern "C" void Transform_1_Invoke_m25422_gshared ();
extern "C" void Transform_1_BeginInvoke_m25423_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m25424_gshared ();
extern "C" void ValueCollection__ctor_m25425_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25426_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25427_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25428_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25429_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25430_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25431_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25432_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25433_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25434_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m25435_gshared ();
extern "C" void ValueCollection_CopyTo_m25436_gshared ();
extern "C" void ValueCollection_GetEnumerator_m25437_gshared ();
void* RuntimeInvoker_Enumerator_t3798 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m25438_gshared ();
extern "C" void Enumerator__ctor_m25439_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25440_gshared ();
extern "C" void Enumerator_Dispose_m25441_gshared ();
extern "C" void Enumerator_MoveNext_m25442_gshared ();
extern "C" void Enumerator_get_Current_m25443_gshared ();
extern "C" void Transform_1__ctor_m25444_gshared ();
extern "C" void Transform_1_Invoke_m25445_gshared ();
extern "C" void Transform_1_BeginInvoke_m25446_gshared ();
extern "C" void Transform_1_EndInvoke_m25447_gshared ();
extern "C" void Transform_1__ctor_m25448_gshared ();
extern "C" void Transform_1_Invoke_m25449_gshared ();
extern "C" void Transform_1_BeginInvoke_m25450_gshared ();
extern "C" void Transform_1_EndInvoke_m25451_gshared ();
extern "C" void Transform_1__ctor_m25452_gshared ();
extern "C" void Transform_1_Invoke_m25453_gshared ();
extern "C" void Transform_1_BeginInvoke_m25454_gshared ();
extern "C" void Transform_1_EndInvoke_m25455_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m25456_gshared ();
extern "C" void ShimEnumerator_MoveNext_m25457_gshared ();
extern "C" void ShimEnumerator_get_Entry_m25458_gshared ();
extern "C" void ShimEnumerator_get_Key_m25459_gshared ();
extern "C" void ShimEnumerator_get_Value_m25460_gshared ();
extern "C" void ShimEnumerator_get_Current_m25461_gshared ();
extern "C" void EqualityComparer_1__ctor_m25462_gshared ();
extern "C" void EqualityComparer_1__cctor_m25463_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25464_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25465_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25466_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25467_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25468_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25469_gshared ();
void* RuntimeInvoker_Boolean_t176_Int64_t1098_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m25470_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25471_gshared ();
extern "C" void DefaultComparer_Equals_m25472_gshared ();
extern "C" void Dictionary_2__ctor_m25713_gshared ();
extern "C" void Dictionary_2__ctor_m25715_gshared ();
extern "C" void Dictionary_2__ctor_m25717_gshared ();
extern "C" void Dictionary_2__ctor_m25719_gshared ();
extern "C" void Dictionary_2__ctor_m25721_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25723_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25725_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25727_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25729_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25731_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m25733_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25735_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25737_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25739_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25741_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25743_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25745_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25747_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25749_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25751_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25753_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25755_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25757_gshared ();
extern "C" void Dictionary_2_get_Count_m25759_gshared ();
extern "C" void Dictionary_2_get_Item_m25761_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m25763_gshared ();
void* RuntimeInvoker_Void_t175_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m25765_gshared ();
extern "C" void Dictionary_2_InitArrays_m25767_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25769_gshared ();
extern "C" void Dictionary_2_make_pair_m25771_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3828_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m25773_gshared ();
void* RuntimeInvoker_UInt64_t1097_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m25775_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m25777_gshared ();
extern "C" void Dictionary_2_Resize_m25779_gshared ();
extern "C" void Dictionary_2_Add_m25781_gshared ();
extern "C" void Dictionary_2_Clear_m25783_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25785_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25787_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25789_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25791_gshared ();
extern "C" void Dictionary_2_Remove_m25793_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25795_gshared ();
void* RuntimeInvoker_Boolean_t176_Int64_t1098_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m25797_gshared ();
extern "C" void Dictionary_2_get_Values_m25799_gshared ();
extern "C" void Dictionary_2_ToTKey_m25801_gshared ();
extern "C" void Dictionary_2_ToTValue_m25803_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25805_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25807_gshared ();
void* RuntimeInvoker_Enumerator_t3833 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25809_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25810_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25811_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25812_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25813_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25814_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3828 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25815_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25816_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25817_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25818_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25819_gshared ();
extern "C" void KeyValuePair_2_ToString_m25820_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25821_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25822_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25823_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25824_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25825_gshared ();
extern "C" void KeyCollection__ctor_m25826_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25827_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25828_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25829_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25830_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25831_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25832_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25833_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25834_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25835_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25836_gshared ();
extern "C" void KeyCollection_CopyTo_m25837_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25838_gshared ();
void* RuntimeInvoker_Enumerator_t3832 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m25839_gshared ();
extern "C" void Enumerator__ctor_m25840_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25841_gshared ();
extern "C" void Enumerator_Dispose_m25842_gshared ();
extern "C" void Enumerator_MoveNext_m25843_gshared ();
extern "C" void Enumerator_get_Current_m25844_gshared ();
extern "C" void Enumerator__ctor_m25845_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25846_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25847_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25848_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25849_gshared ();
extern "C" void Enumerator_MoveNext_m25850_gshared ();
extern "C" void Enumerator_get_Current_m25851_gshared ();
extern "C" void Enumerator_get_CurrentKey_m25852_gshared ();
extern "C" void Enumerator_get_CurrentValue_m25853_gshared ();
extern "C" void Enumerator_VerifyState_m25854_gshared ();
extern "C" void Enumerator_VerifyCurrent_m25855_gshared ();
extern "C" void Enumerator_Dispose_m25856_gshared ();
extern "C" void Transform_1__ctor_m25857_gshared ();
extern "C" void Transform_1_Invoke_m25858_gshared ();
extern "C" void Transform_1_BeginInvoke_m25859_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1098_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m25860_gshared ();
extern "C" void ValueCollection__ctor_m25861_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25862_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25863_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25864_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25865_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25866_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25867_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25868_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25869_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25870_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m25871_gshared ();
extern "C" void ValueCollection_CopyTo_m25872_gshared ();
extern "C" void ValueCollection_GetEnumerator_m25873_gshared ();
void* RuntimeInvoker_Enumerator_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m25874_gshared ();
extern "C" void Enumerator__ctor_m25875_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25876_gshared ();
extern "C" void Enumerator_Dispose_m25877_gshared ();
extern "C" void Enumerator_MoveNext_m25878_gshared ();
extern "C" void Enumerator_get_Current_m25879_gshared ();
extern "C" void Transform_1__ctor_m25880_gshared ();
extern "C" void Transform_1_Invoke_m25881_gshared ();
extern "C" void Transform_1_BeginInvoke_m25882_gshared ();
extern "C" void Transform_1_EndInvoke_m25883_gshared ();
extern "C" void Transform_1__ctor_m25884_gshared ();
extern "C" void Transform_1_Invoke_m25885_gshared ();
extern "C" void Transform_1_BeginInvoke_m25886_gshared ();
extern "C" void Transform_1_EndInvoke_m25887_gshared ();
extern "C" void Transform_1__ctor_m25888_gshared ();
extern "C" void Transform_1_Invoke_m25889_gshared ();
extern "C" void Transform_1_BeginInvoke_m25890_gshared ();
extern "C" void Transform_1_EndInvoke_m25891_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3828_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m25892_gshared ();
extern "C" void ShimEnumerator_MoveNext_m25893_gshared ();
extern "C" void ShimEnumerator_get_Entry_m25894_gshared ();
extern "C" void ShimEnumerator_get_Key_m25895_gshared ();
extern "C" void ShimEnumerator_get_Value_m25896_gshared ();
extern "C" void ShimEnumerator_get_Current_m25897_gshared ();
extern "C" void EqualityComparer_1__ctor_m25898_gshared ();
extern "C" void EqualityComparer_1__cctor_m25899_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25900_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25901_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25902_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25903_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25904_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25905_gshared ();
extern "C" void DefaultComparer__ctor_m25906_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25907_gshared ();
extern "C" void DefaultComparer_Equals_m25908_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26083_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26084_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26085_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26086_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26087_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3849 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m26088_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Key_m26089_gshared ();
extern "C" void KeyValuePair_2_set_Key_m26090_gshared ();
extern "C" void KeyValuePair_2_get_Value_m26091_gshared ();
extern "C" void KeyValuePair_2_set_Value_m26092_gshared ();
extern "C" void KeyValuePair_2_ToString_m26093_gshared ();
extern "C" void Dictionary_2__ctor_m26447_gshared ();
extern "C" void Dictionary_2__ctor_m26449_gshared ();
extern "C" void Dictionary_2__ctor_m26451_gshared ();
extern "C" void Dictionary_2__ctor_m26453_gshared ();
extern "C" void Dictionary_2__ctor_m26455_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26457_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26459_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m26461_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26463_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26465_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m26467_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26469_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26471_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26473_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26475_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26477_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26479_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26481_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26483_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26485_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26487_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26489_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26491_gshared ();
extern "C" void Dictionary_2_get_Count_m26493_gshared ();
extern "C" void Dictionary_2_get_Item_m26495_gshared ();
extern "C" void Dictionary_2_set_Item_m26497_gshared ();
extern "C" void Dictionary_2_Init_m26499_gshared ();
extern "C" void Dictionary_2_InitArrays_m26501_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m26503_gshared ();
extern "C" void Dictionary_2_make_pair_m26505_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3849_Object_t_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m26507_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m26509_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3114_Object_t_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m26511_gshared ();
extern "C" void Dictionary_2_Resize_m26513_gshared ();
extern "C" void Dictionary_2_Add_m26515_gshared ();
extern "C" void Dictionary_2_Clear_m26517_gshared ();
extern "C" void Dictionary_2_ContainsKey_m26519_gshared ();
extern "C" void Dictionary_2_ContainsValue_m26521_gshared ();
extern "C" void Dictionary_2_GetObjectData_m26523_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m26525_gshared ();
extern "C" void Dictionary_2_Remove_m26527_gshared ();
extern "C" void Dictionary_2_TryGetValue_m26529_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_KeyValuePair_2U26_t4573 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m26531_gshared ();
extern "C" void Dictionary_2_get_Values_m26533_gshared ();
extern "C" void Dictionary_2_ToTKey_m26535_gshared ();
extern "C" void Dictionary_2_ToTValue_m26537_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m26539_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m26541_gshared ();
void* RuntimeInvoker_Enumerator_t3877 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m26543_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m26544_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26545_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26546_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26547_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26548_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26549_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m26550_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26551_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26552_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26553_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m26554_gshared ();
extern "C" void KeyCollection_CopyTo_m26555_gshared ();
extern "C" void KeyCollection_GetEnumerator_m26556_gshared ();
void* RuntimeInvoker_Enumerator_t3876 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m26557_gshared ();
extern "C" void Enumerator__ctor_m26558_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26559_gshared ();
extern "C" void Enumerator_Dispose_m26560_gshared ();
extern "C" void Enumerator_MoveNext_m26561_gshared ();
extern "C" void Enumerator_get_Current_m26562_gshared ();
extern "C" void Enumerator__ctor_m26563_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26564_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26565_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26566_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26567_gshared ();
extern "C" void Enumerator_MoveNext_m26568_gshared ();
extern "C" void Enumerator_get_Current_m26569_gshared ();
extern "C" void Enumerator_get_CurrentKey_m26570_gshared ();
extern "C" void Enumerator_get_CurrentValue_m26571_gshared ();
extern "C" void Enumerator_VerifyState_m26572_gshared ();
extern "C" void Enumerator_VerifyCurrent_m26573_gshared ();
extern "C" void Enumerator_Dispose_m26574_gshared ();
extern "C" void Transform_1__ctor_m26575_gshared ();
extern "C" void Transform_1_Invoke_m26576_gshared ();
extern "C" void Transform_1_BeginInvoke_m26577_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3114_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m26578_gshared ();
extern "C" void ValueCollection__ctor_m26579_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26580_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26581_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26582_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26583_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26584_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m26585_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26586_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26587_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26588_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m26589_gshared ();
extern "C" void ValueCollection_CopyTo_m26590_gshared ();
extern "C" void ValueCollection_GetEnumerator_m26591_gshared ();
void* RuntimeInvoker_Enumerator_t3880 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m26592_gshared ();
extern "C" void Enumerator__ctor_m26593_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26594_gshared ();
extern "C" void Enumerator_Dispose_m26595_gshared ();
extern "C" void Enumerator_MoveNext_m26596_gshared ();
extern "C" void Enumerator_get_Current_m26597_gshared ();
extern "C" void Transform_1__ctor_m26598_gshared ();
extern "C" void Transform_1_Invoke_m26599_gshared ();
extern "C" void Transform_1_BeginInvoke_m26600_gshared ();
extern "C" void Transform_1_EndInvoke_m26601_gshared ();
extern "C" void Transform_1__ctor_m26602_gshared ();
extern "C" void Transform_1_Invoke_m26603_gshared ();
extern "C" void Transform_1_BeginInvoke_m26604_gshared ();
extern "C" void Transform_1_EndInvoke_m26605_gshared ();
extern "C" void Transform_1__ctor_m26606_gshared ();
extern "C" void Transform_1_Invoke_m26607_gshared ();
extern "C" void Transform_1_BeginInvoke_m26608_gshared ();
extern "C" void Transform_1_EndInvoke_m26609_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3849_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m26610_gshared ();
extern "C" void ShimEnumerator_MoveNext_m26611_gshared ();
extern "C" void ShimEnumerator_get_Entry_m26612_gshared ();
extern "C" void ShimEnumerator_get_Key_m26613_gshared ();
extern "C" void ShimEnumerator_get_Value_m26614_gshared ();
extern "C" void ShimEnumerator_get_Current_m26615_gshared ();
extern "C" void EqualityComparer_1__ctor_m26616_gshared ();
extern "C" void EqualityComparer_1__cctor_m26617_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26618_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26619_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26620_gshared ();
extern "C" void DefaultComparer__ctor_m26621_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26622_gshared ();
extern "C" void DefaultComparer_Equals_m26623_gshared ();
void* RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3114_KeyValuePair_2_t3114 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m26722_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26723_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26724_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26725_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26726_gshared ();
void* RuntimeInvoker_ParameterModifier_t2266 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m26727_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26728_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26729_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26730_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26731_gshared ();
void* RuntimeInvoker_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m26855_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m26856_gshared ();
extern "C" void InvokableCall_1__ctor_m26857_gshared ();
extern "C" void InvokableCall_1__ctor_m26858_gshared ();
extern "C" void InvokableCall_1_Invoke_m26859_gshared ();
extern "C" void InvokableCall_1_Find_m26860_gshared ();
extern "C" void UnityAction_1__ctor_m26861_gshared ();
extern "C" void UnityAction_1_Invoke_m26862_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m26863_gshared ();
extern "C" void UnityAction_1_EndInvoke_m26864_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m26872_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27070_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27071_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27072_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27073_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27074_gshared ();
extern "C" void Dictionary_2__ctor_m27130_gshared ();
extern "C" void Dictionary_2__ctor_m27133_gshared ();
extern "C" void Dictionary_2__ctor_m27135_gshared ();
extern "C" void Dictionary_2__ctor_m27137_gshared ();
extern "C" void Dictionary_2__ctor_m27139_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27141_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27143_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m27145_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27147_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27149_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m27151_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27153_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27155_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27157_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27159_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27161_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27163_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27165_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27167_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27169_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27171_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27173_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27175_gshared ();
extern "C" void Dictionary_2_get_Count_m27177_gshared ();
extern "C" void Dictionary_2_get_Item_m27179_gshared ();
extern "C" void Dictionary_2_set_Item_m27181_gshared ();
void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m27183_gshared ();
extern "C" void Dictionary_2_InitArrays_m27185_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m27187_gshared ();
extern "C" void Dictionary_2_make_pair_m27189_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3945_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m27191_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m27193_gshared ();
void* RuntimeInvoker_Byte_t455_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m27195_gshared ();
extern "C" void Dictionary_2_Resize_m27197_gshared ();
extern "C" void Dictionary_2_Add_m27199_gshared ();
extern "C" void Dictionary_2_Clear_m27201_gshared ();
extern "C" void Dictionary_2_ContainsKey_m27203_gshared ();
extern "C" void Dictionary_2_ContainsValue_m27205_gshared ();
extern "C" void Dictionary_2_GetObjectData_m27207_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m27209_gshared ();
extern "C" void Dictionary_2_Remove_m27211_gshared ();
extern "C" void Dictionary_2_TryGetValue_m27213_gshared ();
void* RuntimeInvoker_Boolean_t176_Object_t_ByteU26_t1846 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m27215_gshared ();
extern "C" void Dictionary_2_get_Values_m27217_gshared ();
extern "C" void Dictionary_2_ToTKey_m27219_gshared ();
extern "C" void Dictionary_2_ToTValue_m27221_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m27223_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m27225_gshared ();
void* RuntimeInvoker_Enumerator_t3949 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m27227_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27228_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27229_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27230_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27231_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27232_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3945 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m27233_gshared ();
extern "C" void KeyValuePair_2_get_Key_m27234_gshared ();
extern "C" void KeyValuePair_2_set_Key_m27235_gshared ();
extern "C" void KeyValuePair_2_get_Value_m27236_gshared ();
extern "C" void KeyValuePair_2_set_Value_m27237_gshared ();
extern "C" void KeyValuePair_2_ToString_m27238_gshared ();
extern "C" void KeyCollection__ctor_m27239_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27240_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27241_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27242_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27243_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27244_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m27245_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27246_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27247_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27248_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m27249_gshared ();
extern "C" void KeyCollection_CopyTo_m27250_gshared ();
extern "C" void KeyCollection_GetEnumerator_m27251_gshared ();
void* RuntimeInvoker_Enumerator_t3948 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m27252_gshared ();
extern "C" void Enumerator__ctor_m27253_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27254_gshared ();
extern "C" void Enumerator_Dispose_m27255_gshared ();
extern "C" void Enumerator_MoveNext_m27256_gshared ();
extern "C" void Enumerator_get_Current_m27257_gshared ();
extern "C" void Enumerator__ctor_m27258_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27259_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27260_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27261_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27262_gshared ();
extern "C" void Enumerator_MoveNext_m27263_gshared ();
extern "C" void Enumerator_get_Current_m27264_gshared ();
extern "C" void Enumerator_get_CurrentKey_m27265_gshared ();
extern "C" void Enumerator_get_CurrentValue_m27266_gshared ();
extern "C" void Enumerator_VerifyState_m27267_gshared ();
extern "C" void Enumerator_VerifyCurrent_m27268_gshared ();
extern "C" void Enumerator_Dispose_m27269_gshared ();
extern "C" void Transform_1__ctor_m27270_gshared ();
extern "C" void Transform_1_Invoke_m27271_gshared ();
extern "C" void Transform_1_BeginInvoke_m27272_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m27273_gshared ();
extern "C" void ValueCollection__ctor_m27274_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27275_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27276_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27277_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27278_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27279_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m27280_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27281_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27282_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27283_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m27284_gshared ();
extern "C" void ValueCollection_CopyTo_m27285_gshared ();
extern "C" void ValueCollection_GetEnumerator_m27286_gshared ();
void* RuntimeInvoker_Enumerator_t3952 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m27287_gshared ();
extern "C" void Enumerator__ctor_m27288_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27289_gshared ();
extern "C" void Enumerator_Dispose_m27290_gshared ();
extern "C" void Enumerator_MoveNext_m27291_gshared ();
extern "C" void Enumerator_get_Current_m27292_gshared ();
extern "C" void Transform_1__ctor_m27293_gshared ();
extern "C" void Transform_1_Invoke_m27294_gshared ();
extern "C" void Transform_1_BeginInvoke_m27295_gshared ();
extern "C" void Transform_1_EndInvoke_m27296_gshared ();
extern "C" void Transform_1__ctor_m27297_gshared ();
extern "C" void Transform_1_Invoke_m27298_gshared ();
extern "C" void Transform_1_BeginInvoke_m27299_gshared ();
extern "C" void Transform_1_EndInvoke_m27300_gshared ();
extern "C" void Transform_1__ctor_m27301_gshared ();
extern "C" void Transform_1_Invoke_m27302_gshared ();
extern "C" void Transform_1_BeginInvoke_m27303_gshared ();
extern "C" void Transform_1_EndInvoke_m27304_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3945_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m27305_gshared ();
extern "C" void ShimEnumerator_MoveNext_m27306_gshared ();
extern "C" void ShimEnumerator_get_Entry_m27307_gshared ();
extern "C" void ShimEnumerator_get_Key_m27308_gshared ();
extern "C" void ShimEnumerator_get_Value_m27309_gshared ();
extern "C" void ShimEnumerator_get_Current_m27310_gshared ();
extern "C" void EqualityComparer_1__ctor_m27311_gshared ();
extern "C" void EqualityComparer_1__cctor_m27312_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27313_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27314_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27315_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m27316_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27317_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27318_gshared ();
void* RuntimeInvoker_Boolean_t176_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m27319_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27320_gshared ();
extern "C" void DefaultComparer_Equals_m27321_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27372_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27373_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27374_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27375_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27376_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1908 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m27387_gshared ();
extern "C" void Dictionary_2__ctor_m27388_gshared ();
extern "C" void Dictionary_2__ctor_m27389_gshared ();
extern "C" void Dictionary_2__ctor_m27390_gshared ();
extern "C" void Dictionary_2__ctor_m27391_gshared ();
extern "C" void Dictionary_2__ctor_m27392_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27393_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27394_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m27395_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27396_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27397_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m27398_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27399_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27400_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27401_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27402_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27403_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27404_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27405_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27406_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27407_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27408_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27409_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27410_gshared ();
extern "C" void Dictionary_2_get_Count_m27411_gshared ();
extern "C" void Dictionary_2_get_Item_m27412_gshared ();
extern "C" void Dictionary_2_set_Item_m27413_gshared ();
extern "C" void Dictionary_2_Init_m27414_gshared ();
extern "C" void Dictionary_2_InitArrays_m27415_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m27416_gshared ();
extern "C" void Dictionary_2_make_pair_m27417_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3967_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m27418_gshared ();
extern "C" void Dictionary_2_pick_value_m27419_gshared ();
extern "C" void Dictionary_2_CopyTo_m27420_gshared ();
extern "C" void Dictionary_2_Resize_m27421_gshared ();
extern "C" void Dictionary_2_Add_m27422_gshared ();
extern "C" void Dictionary_2_Clear_m27423_gshared ();
extern "C" void Dictionary_2_ContainsKey_m27424_gshared ();
extern "C" void Dictionary_2_ContainsValue_m27425_gshared ();
extern "C" void Dictionary_2_GetObjectData_m27426_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m27427_gshared ();
extern "C" void Dictionary_2_Remove_m27428_gshared ();
extern "C" void Dictionary_2_TryGetValue_m27429_gshared ();
void* RuntimeInvoker_Boolean_t176_Int32_t135_Int32U26_t541 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m27430_gshared ();
extern "C" void Dictionary_2_get_Values_m27431_gshared ();
extern "C" void Dictionary_2_ToTKey_m27432_gshared ();
extern "C" void Dictionary_2_ToTValue_m27433_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m27434_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m27435_gshared ();
void* RuntimeInvoker_Enumerator_t3971 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m27436_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27437_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27438_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27439_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27440_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27441_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3967 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m27442_gshared ();
extern "C" void KeyValuePair_2_get_Key_m27443_gshared ();
extern "C" void KeyValuePair_2_set_Key_m27444_gshared ();
extern "C" void KeyValuePair_2_get_Value_m27445_gshared ();
extern "C" void KeyValuePair_2_set_Value_m27446_gshared ();
extern "C" void KeyValuePair_2_ToString_m27447_gshared ();
extern "C" void KeyCollection__ctor_m27448_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27449_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27450_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27451_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27452_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27453_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m27454_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27455_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27456_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27457_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m27458_gshared ();
extern "C" void KeyCollection_CopyTo_m27459_gshared ();
extern "C" void KeyCollection_GetEnumerator_m27460_gshared ();
void* RuntimeInvoker_Enumerator_t3970 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m27461_gshared ();
extern "C" void Enumerator__ctor_m27462_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27463_gshared ();
extern "C" void Enumerator_Dispose_m27464_gshared ();
extern "C" void Enumerator_MoveNext_m27465_gshared ();
extern "C" void Enumerator_get_Current_m27466_gshared ();
extern "C" void Enumerator__ctor_m27467_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27468_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27469_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27470_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27471_gshared ();
extern "C" void Enumerator_MoveNext_m27472_gshared ();
extern "C" void Enumerator_get_Current_m27473_gshared ();
extern "C" void Enumerator_get_CurrentKey_m27474_gshared ();
extern "C" void Enumerator_get_CurrentValue_m27475_gshared ();
extern "C" void Enumerator_VerifyState_m27476_gshared ();
extern "C" void Enumerator_VerifyCurrent_m27477_gshared ();
extern "C" void Enumerator_Dispose_m27478_gshared ();
extern "C" void Transform_1__ctor_m27479_gshared ();
extern "C" void Transform_1_Invoke_m27480_gshared ();
extern "C" void Transform_1_BeginInvoke_m27481_gshared ();
extern "C" void Transform_1_EndInvoke_m27482_gshared ();
extern "C" void ValueCollection__ctor_m27483_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27484_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27485_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27486_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27487_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27488_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m27489_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27490_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27491_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27492_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m27493_gshared ();
extern "C" void ValueCollection_CopyTo_m27494_gshared ();
extern "C" void ValueCollection_GetEnumerator_m27495_gshared ();
void* RuntimeInvoker_Enumerator_t3974 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m27496_gshared ();
extern "C" void Enumerator__ctor_m27497_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27498_gshared ();
extern "C" void Enumerator_Dispose_m27499_gshared ();
extern "C" void Enumerator_MoveNext_m27500_gshared ();
extern "C" void Enumerator_get_Current_m27501_gshared ();
extern "C" void Transform_1__ctor_m27502_gshared ();
extern "C" void Transform_1_Invoke_m27503_gshared ();
extern "C" void Transform_1_BeginInvoke_m27504_gshared ();
extern "C" void Transform_1_EndInvoke_m27505_gshared ();
extern "C" void Transform_1__ctor_m27506_gshared ();
extern "C" void Transform_1_Invoke_m27507_gshared ();
extern "C" void Transform_1_BeginInvoke_m27508_gshared ();
extern "C" void Transform_1_EndInvoke_m27509_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3967_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m27510_gshared ();
extern "C" void ShimEnumerator_MoveNext_m27511_gshared ();
extern "C" void ShimEnumerator_get_Entry_m27512_gshared ();
extern "C" void ShimEnumerator_get_Key_m27513_gshared ();
extern "C" void ShimEnumerator_get_Value_m27514_gshared ();
extern "C" void ShimEnumerator_get_Current_m27515_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27516_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27517_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27518_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27519_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27520_gshared ();
void* RuntimeInvoker_Mark_t1954 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27521_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27522_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27523_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27524_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27525_gshared ();
void* RuntimeInvoker_UriScheme_t1990 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27531_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27532_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27533_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27534_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27535_gshared ();
void* RuntimeInvoker_Int16_t540 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27536_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27537_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27538_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27539_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27540_gshared ();
void* RuntimeInvoker_SByte_t177 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27566_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27567_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27568_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27569_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27570_gshared ();
void* RuntimeInvoker_TableRange_t2075 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27596_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27597_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27598_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27599_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27600_gshared ();
void* RuntimeInvoker_Slot_t2152 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27601_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27602_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27603_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27604_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27605_gshared ();
void* RuntimeInvoker_Slot_t2159 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27674_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27675_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27676_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27677_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27678_gshared ();
void* RuntimeInvoker_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27679_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27680_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27681_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27682_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27683_gshared ();
void* RuntimeInvoker_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27684_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27685_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27686_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27687_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27688_gshared ();
extern "C" void GenericComparer_1_Compare_m27792_gshared ();
void* RuntimeInvoker_Int32_t135_DateTime_t120_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27793_gshared ();
extern "C" void Comparer_1__cctor_m27794_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27795_gshared ();
extern "C" void Comparer_1_get_Default_m27796_gshared ();
extern "C" void DefaultComparer__ctor_m27797_gshared ();
extern "C" void DefaultComparer_Compare_m27798_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27799_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27800_gshared ();
void* RuntimeInvoker_Boolean_t176_DateTime_t120_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27801_gshared ();
extern "C" void EqualityComparer_1__cctor_m27802_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27803_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27804_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27805_gshared ();
extern "C" void DefaultComparer__ctor_m27806_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27807_gshared ();
extern "C" void DefaultComparer_Equals_m27808_gshared ();
extern "C" void GenericComparer_1_Compare_m27809_gshared ();
void* RuntimeInvoker_Int32_t135_DateTimeOffset_t1426_DateTimeOffset_t1426 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27810_gshared ();
extern "C" void Comparer_1__cctor_m27811_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27812_gshared ();
extern "C" void Comparer_1_get_Default_m27813_gshared ();
extern "C" void DefaultComparer__ctor_m27814_gshared ();
extern "C" void DefaultComparer_Compare_m27815_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27816_gshared ();
void* RuntimeInvoker_Int32_t135_DateTimeOffset_t1426 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m27817_gshared ();
void* RuntimeInvoker_Boolean_t176_DateTimeOffset_t1426_DateTimeOffset_t1426 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27818_gshared ();
extern "C" void EqualityComparer_1__cctor_m27819_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27820_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27821_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27822_gshared ();
extern "C" void DefaultComparer__ctor_m27823_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27824_gshared ();
extern "C" void DefaultComparer_Equals_m27825_gshared ();
extern "C" void Nullable_1_Equals_m27826_gshared ();
extern "C" void Nullable_1_Equals_m27827_gshared ();
void* RuntimeInvoker_Boolean_t176_Nullable_1_t2604 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m27828_gshared ();
extern "C" void Nullable_1_ToString_m27829_gshared ();
extern "C" void GenericComparer_1_Compare_m27830_gshared ();
void* RuntimeInvoker_Int32_t135_Guid_t118_Guid_t118 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27831_gshared ();
extern "C" void Comparer_1__cctor_m27832_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27833_gshared ();
extern "C" void Comparer_1_get_Default_m27834_gshared ();
extern "C" void DefaultComparer__ctor_m27835_gshared ();
extern "C" void DefaultComparer_Compare_m27836_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27837_gshared ();
void* RuntimeInvoker_Int32_t135_Guid_t118 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m27838_gshared ();
void* RuntimeInvoker_Boolean_t176_Guid_t118_Guid_t118 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27839_gshared ();
extern "C" void EqualityComparer_1__cctor_m27840_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27841_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27842_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27843_gshared ();
extern "C" void DefaultComparer__ctor_m27844_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27845_gshared ();
extern "C" void DefaultComparer_Equals_m27846_gshared ();
extern "C" void GenericComparer_1_Compare_m27847_gshared ();
void* RuntimeInvoker_Int32_t135_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27848_gshared ();
extern "C" void Comparer_1__cctor_m27849_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27850_gshared ();
extern "C" void Comparer_1_get_Default_m27851_gshared ();
extern "C" void DefaultComparer__ctor_m27852_gshared ();
extern "C" void DefaultComparer_Compare_m27853_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27854_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27855_gshared ();
void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27856_gshared ();
extern "C" void EqualityComparer_1__cctor_m27857_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27858_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27859_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27860_gshared ();
extern "C" void DefaultComparer__ctor_m27861_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27862_gshared ();
extern "C" void DefaultComparer_Equals_m27863_gshared ();
extern const Il2CppGenericInst GenInst_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_t107_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppGenericInst GenInst_Renderer_t17_0_0_0;
extern const Il2CppGenericInst GenInst_Transform_t11_0_0_0;
extern const Il2CppGenericInst GenInst_Raycaster_t26_0_0_0;
extern const Il2CppGenericInst GenInst_Camera_t3_0_0_0;
extern const Il2CppGenericInst GenInst_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_LogBehaviour_t962_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t14_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t125_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t14_0_0_0_Vector3_t14_0_0_0_VectorOptions_t1008_0_0_0;
extern const Il2CppGenericInst GenInst_Sequence_t131_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_t2_0_0_0;
extern const Il2CppGenericInst GenInst_InitError_t183_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t48_0_0_0;
extern const Il2CppGenericInst GenInst_Prop_t105_0_0_0;
extern const Il2CppGenericInst GenInst_Surface_t106_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t52_0_0_0;
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t67_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t93_0_0_0;
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t84_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t57_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t65_0_0_0;
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t69_0_0_0;
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t43_0_0_0;
extern const Il2CppGenericInst GenInst_WordBehaviour_t100_0_0_0;
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t82_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t71_0_0_0;
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t60_0_0_0;
extern const Il2CppGenericInst GenInst_MeshRenderer_t156_0_0_0;
extern const Il2CppGenericInst GenInst_MeshFilter_t157_0_0_0;
extern const Il2CppGenericInst GenInst_Collider_t164_0_0_0;
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t97_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInputModule_t203_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t238_0_0_0;
extern const Il2CppGenericInst GenInst_IDeselectHandler_t409_0_0_0;
extern const Il2CppGenericInst GenInst_ISelectHandler_t408_0_0_0;
extern const Il2CppGenericInst GenInst_BaseEventData_t204_0_0_0;
extern const Il2CppGenericInst GenInst_Entry_t209_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t396_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t397_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t398_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t399_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t400_0_0_0;
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t401_0_0_0;
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t402_0_0_0;
extern const Il2CppGenericInst GenInst_IDragHandler_t403_0_0_0;
extern const Il2CppGenericInst GenInst_IEndDragHandler_t404_0_0_0;
extern const Il2CppGenericInst GenInst_IDropHandler_t405_0_0_0;
extern const Il2CppGenericInst GenInst_IScrollHandler_t406_0_0_0;
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t407_0_0_0;
extern const Il2CppGenericInst GenInst_IMoveHandler_t410_0_0_0;
extern const Il2CppGenericInst GenInst_ISubmitHandler_t411_0_0_0;
extern const Il2CppGenericInst GenInst_ICancelHandler_t412_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t414_0_0_0;
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t496_0_0_0;
extern const Il2CppGenericInst GenInst_PointerEventData_t243_0_0_0;
extern const Il2CppGenericInst GenInst_AxisEventData_t239_0_0_0;
extern const Il2CppGenericInst GenInst_BaseRaycaster_t237_0_0_0;
extern const Il2CppGenericInst GenInst_EventSystem_t116_0_0_0;
extern const Il2CppGenericInst GenInst_ButtonState_t246_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_PointerEventData_t243_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteRenderer_t435_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit_t102_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t98_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t417_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t273_0_0_0_List_1_t443_0_0_0;
extern const Il2CppGenericInst GenInst_Text_t31_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t273_0_0_0;
extern const Il2CppGenericInst GenInst_ColorTween_t260_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t321_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t319_0_0_0;
extern const Il2CppGenericInst GenInst_RectTransform_t279_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t281_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasRenderer_t280_0_0_0;
extern const Il2CppGenericInst GenInst_Component_t113_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t285_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t281_0_0_0_IndexedSet_1_t454_0_0_0;
extern const Il2CppGenericInst GenInst_Sprite_t299_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t292_0_0_0;
extern const Il2CppGenericInst GenInst_FillMethod_t293_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t112_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_SubmitEvent_t307_0_0_0;
extern const Il2CppGenericInst GenInst_OnChangeEvent_t309_0_0_0;
extern const Il2CppGenericInst GenInst_OnValidateInput_t313_0_0_0;
extern const Il2CppGenericInst GenInst_ContentType_t303_0_0_0;
extern const Il2CppGenericInst GenInst_LineType_t306_0_0_0;
extern const Il2CppGenericInst GenInst_InputType_t304_0_0_0;
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t459_0_0_0;
extern const Il2CppGenericInst GenInst_CharacterValidation_t305_0_0_0;
extern const Il2CppGenericInst GenInst_Char_t457_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t464_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t466_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutElement_t376_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t329_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t19_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasGroup_t448_0_0_0;
extern const Il2CppGenericInst GenInst_Selectable_t266_0_0_0;
extern const Il2CppGenericInst GenInst_Navigation_t326_0_0_0;
extern const Il2CppGenericInst GenInst_Transition_t341_0_0_0;
extern const Il2CppGenericInst GenInst_ColorBlock_t272_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteState_t345_0_0_0;
extern const Il2CppGenericInst GenInst_AnimationTriggers_t261_0_0_0;
extern const Il2CppGenericInst GenInst_Animator_t421_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t347_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t301_0_0_0;
extern const Il2CppGenericInst GenInst_MatEntry_t350_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t357_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t357_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_AspectMode_t361_0_0_0;
extern const Il2CppGenericInst GenInst_FitMode_t367_0_0_0;
extern const Il2CppGenericInst GenInst_Corner_t369_0_0_0;
extern const Il2CppGenericInst GenInst_Axis_t370_0_0_0;
extern const Il2CppGenericInst GenInst_Constraint_t371_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t377_0_0_0;
extern const Il2CppGenericInst GenInst_TextAnchor_t477_0_0_0;
extern const Il2CppGenericInst GenInst_ILayoutElement_t425_0_0_0_Single_t112_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t426_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t424_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m2481_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2482_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2484_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m2485_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m2486_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenRunner_1_t502_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t381_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t513_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t513_gp_0_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectPool_1_t515_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t185_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t680_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t76_0_0_0;
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t767_0_0_0;
extern const Il2CppGenericInst GenInst_TargetSearchResult_t726_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectTracker_t580_0_0_0;
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t40_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t588_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButton_t737_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t620_0_0_0_Image_t621_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t620_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Trackable_t571_0_0_0;
extern const Il2CppGenericInst GenInst_Trackable_t571_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButton_t737_0_0_0;
extern const Il2CppGenericInst GenInst_DataSet_t600_0_0_0;
extern const Il2CppGenericInst GenInst_DataSetImpl_t584_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Marker_t745_0_0_0;
extern const Il2CppGenericInst GenInst_Marker_t745_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t648_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t595_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t597_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t460_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_WordResult_t701_0_0_0;
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t101_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t698_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_WordAbstractBehaviour_t101_0_0_0;
extern const Il2CppGenericInst GenInst_WordData_t653_0_0_0;
extern const Il2CppGenericInst GenInst_WordResultData_t652_0_0_0;
extern const Il2CppGenericInst GenInst_Word_t702_0_0_0;
extern const Il2CppGenericInst GenInst_WordResult_t701_0_0_0;
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t782_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t594_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Prop_t105_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Surface_t106_0_0_0;
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t783_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_PropAbstractBehaviour_t73_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_SurfaceAbstractBehaviour_t81_0_0_0;
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t73_0_0_0;
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t81_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableBehaviour_t52_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerTracker_t635_0_0_0;
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t573_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableResultData_t648_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonData_t649_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t94_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_ImageTarget_t738_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTarget_t738_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t58_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonAbstractBehaviour_t94_0_0_0;
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t178_0_0_0;
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t90_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t794_0_0_0;
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t96_0_0_0;
extern const Il2CppGenericInst GenInst_TextTracker_t682_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0;
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t795_0_0_0;
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t796_0_0_0;
extern const Il2CppGenericInst GenInst_QCARAbstractBehaviour_t75_0_0_0;
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t797_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m5032_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t740_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t1081_0_0_0_UInt32_t1081_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t19_0_0_0_Vector2_t19_0_0_0_VectorOptions_t1008_0_0_0;
extern const Il2CppGenericInst GenInst_DOTweenComponent_t941_0_0_0;
extern const Il2CppGenericInst GenInst_Tween_t940_0_0_0;
extern const Il2CppGenericInst GenInst_ABSSequentiable_t949_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t14_0_0_0_Vector3U5BU5D_t161_0_0_0_Vector3ArrayOptions_t957_0_0_0;
extern const Il2CppGenericInst GenInst_Quaternion_t22_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1034_0_0_0;
extern const Il2CppGenericInst GenInst_Quaternion_t22_0_0_0_Vector3_t14_0_0_0_QuaternionOptions_t977_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t377_0_0_0_RectOffset_t377_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_Color2_t1006_0_0_0_Color2_t1006_0_0_0_ColorOptions_t1017_0_0_0;
extern const Il2CppGenericInst GenInst_Rect_t132_0_0_0_Rect_t132_0_0_0_RectOptions_t1010_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_UInt64_t1097_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Int32_t135_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_TweenCallback_t109_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t1009_0_0_0;
extern const Il2CppGenericInst GenInst_Vector4_t419_0_0_0_Vector4_t419_0_0_0_VectorOptions_t1008_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t98_0_0_0_Color_t98_0_0_0_ColorOptions_t1017_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t112_0_0_0_Single_t112_0_0_0_FloatOptions_t1002_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1098_0_0_0_Int64_t1098_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t1070_gp_0_0_0_0_ABSTweenPlugin_3_t1070_gp_1_0_0_0_ABSTweenPlugin_3_t1070_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t1070_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t1081_0_0_0;
extern const Il2CppGenericInst GenInst_PluginsManager_GetDefaultPlugin_m5630_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m5630_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m5630_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Color2_t1006_0_0_0;
extern const Il2CppGenericInst GenInst_TweenManager_GetTweener_m5631_gp_0_0_0_0_TweenManager_GetTweener_m5631_gp_1_0_0_0_TweenManager_GetTweener_m5631_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Rect_t132_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0;
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m5632_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m5632_gp_0_0_0_0_DOTween_ApplyTo_m5632_gp_1_0_0_0_DOTween_ApplyTo_m5632_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Vector4_t419_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_Setup_m5633_gp_0_0_0_0_Tweener_Setup_m5633_gp_1_0_0_0_Tweener_Setup_m5633_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_Setup_m5633_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoUpdateDelay_m5634_gp_0_0_0_0_Tweener_DoUpdateDelay_m5634_gp_1_0_0_0_Tweener_DoUpdateDelay_m5634_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m5635_gp_0_0_0_0_Tweener_DoStartup_m5635_gp_1_0_0_0_Tweener_DoStartup_m5635_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m5635_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DOStartupSpecials_m5636_gp_0_0_0_0_Tweener_DOStartupSpecials_m5636_gp_1_0_0_0_Tweener_DOStartupSpecials_m5636_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DOStartupDurationBased_m5637_gp_0_0_0_0_Tweener_DOStartupDurationBased_m5637_gp_1_0_0_0_Tweener_DOStartupDurationBased_m5637_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1074_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1074_gp_0_0_0_0_TweenerCore_3_t1074_gp_1_0_0_0_TweenerCore_3_t1074_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1098_0_0_0;
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1161_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_LayoutCache_t1176_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1180_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1178_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t622_0_0_0;
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1231_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t1266_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDesc_t1268_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1273_0_0_0_NetworkAccessToken_t1275_0_0_0;
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t1260_0_0_0;
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t1262_0_0_0;
extern const Il2CppGenericInst GenInst_BasicResponse_t1257_0_0_0;
extern const Il2CppGenericInst GenInst_ListMatchResponse_t1270_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1380_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t1292_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1382_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t1290_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1383_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1425_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t1291_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1427_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1293_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayer_t1167_0_0_0;
extern const Il2CppGenericInst GenInst_PersistentCall_t1348_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1345_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1390_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1392_0_0_0;
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t1327_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1321_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7078_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7079_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7080_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7081_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponents_m7082_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m7085_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m7087_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m7088_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m7089_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m7093_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1098_0_0_0;
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m7095_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1456_gp_0_0_0_0_ThreadSafeDictionary_2_t1456_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1456_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1530_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t323_0_0_0_TextEditOp_t1341_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_1_t1463_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1464_gp_0_0_0_0_InvokableCall_2_t1464_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1464_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1464_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1465_gp_0_0_0_0_InvokableCall_3_t1465_gp_1_0_0_0_InvokableCall_3_t1465_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1465_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1465_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1465_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1466_gp_0_0_0_0_InvokableCall_4_t1466_gp_1_0_0_0_InvokableCall_4_t1466_gp_2_0_0_0_InvokableCall_4_t1466_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1466_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1466_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1466_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1466_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1445_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1467_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1468_gp_0_0_0_0_UnityEvent_2_t1468_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1469_gp_0_0_0_0_UnityEvent_3_t1469_gp_1_0_0_0_UnityEvent_3_t1469_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1470_gp_0_0_0_0_UnityEvent_4_t1470_gp_1_0_0_0_UnityEvent_4_t1470_gp_2_0_0_0_UnityEvent_4_t1470_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1597_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_PrimeHelper_t1598_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_HashSet_1_t1595_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_0_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Any_m7329_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m7330_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m7331_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m7332_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m7333_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_First_m7334_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m7335_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m7336_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m7337_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m7337_gp_0_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7338_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7338_gp_0_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2014_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LinkedList_1_t2013_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2015_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2017_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Stack_1_t2016_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_StrongName_t2444_0_0_0;
extern const Il2CppGenericInst GenInst_DateTime_t120_0_0_0;
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1426_0_0_0;
extern const Il2CppGenericInst GenInst_TimeSpan_t121_0_0_0;
extern const Il2CppGenericInst GenInst_Guid_t118_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t455_0_0_0;
extern const Il2CppGenericInst GenInst_SByte_t177_0_0_0;
extern const Il2CppGenericInst GenInst_Int16_t540_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t460_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2635_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Double_t1413_0_0_0;
extern const Il2CppGenericInst GenInst_Decimal_t1065_0_0_0;
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2636_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2638_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2637_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m14067_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14079_gp_0_0_0_0_Array_Sort_m14079_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14080_gp_0_0_0_0_Array_Sort_m14080_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14081_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14081_gp_0_0_0_0_Array_Sort_m14081_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14082_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14082_gp_0_0_0_0_Array_Sort_m14082_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14083_gp_0_0_0_0_Array_Sort_m14083_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14084_gp_0_0_0_0_Array_Sort_m14084_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14085_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14085_gp_0_0_0_0_Array_Sort_m14085_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14086_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14086_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14086_gp_0_0_0_0_Array_Sort_m14086_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14087_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14088_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14089_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14089_gp_0_0_0_0_Array_qsort_m14089_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_compare_m14090_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14091_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Resize_m14094_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m14096_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ForEach_m14097_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m14098_gp_0_0_0_0_Array_ConvertAll_m14098_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14099_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14100_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14101_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14102_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14103_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14104_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14105_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14106_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14107_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14108_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14109_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14110_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14111_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14112_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14113_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14114_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindAll_m14115_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Exists_m14116_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m14117_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Find_m14118_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLast_m14119_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IList_1_t2639_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ICollection_1_t2640_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Nullable_1_t2608_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2647_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Comparer_1_t2646_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2648_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2650_gp_0_0_0_0_ShimEnumerator_t2650_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2651_gp_0_0_0_0_Enumerator_t2651_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2941_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2653_gp_0_0_0_0_Enumerator_t2653_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2653_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2652_gp_0_0_0_0_KeyCollection_t2652_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2652_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2652_gp_0_0_0_0_KeyCollection_t2652_gp_1_0_0_0_KeyCollection_t2652_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2652_gp_0_0_0_0_KeyCollection_t2652_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2655_gp_0_0_0_0_Enumerator_t2655_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2655_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2654_gp_0_0_0_0_ValueCollection_t2654_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2654_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2654_gp_0_0_0_0_ValueCollection_t2654_gp_1_0_0_0_ValueCollection_t2654_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2654_gp_1_0_0_0_ValueCollection_t2654_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0_Dictionary_2_t2649_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2978_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0_Dictionary_2_t2649_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m14268_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0_Dictionary_2_t2649_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m14273_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m14273_gp_0_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0_Dictionary_2_t2649_gp_1_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2002_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2649_gp_0_0_0_0_Dictionary_2_t2649_gp_1_0_0_0_KeyValuePair_2_t2978_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2978_0_0_0_KeyValuePair_2_t2978_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2658_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2657_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2659_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2661_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2661_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4574_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2661_gp_0_0_0_0_IDictionary_2_t2661_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2663_gp_0_0_0_0_KeyValuePair_2_t2663_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2665_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t2664_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Collection_1_t2666_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2667_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m14550_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m14550_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m14551_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Version_t1881_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t111_0_0_0;
extern const Il2CppGenericInst GenInst_EachSearchableObject_t16_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3114_0_0_0;
extern const Il2CppGenericInst GenInst_ValueType_t530_0_0_0;
extern const Il2CppGenericInst GenInst_IConvertible_t172_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_t173_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_t556_0_0_0;
extern const Il2CppGenericInst GenInst_ICloneable_t518_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2739_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2740_0_0_0;
extern const Il2CppGenericInst GenInst_IFormattable_t171_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2705_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2706_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t2142_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0;
extern const Il2CppGenericInst GenInst_IReflect_t2643_0_0_0;
extern const Il2CppGenericInst GenInst__Type_t2641_0_0_0;
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppGenericInst GenInst__MemberInfo_t2642_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObjectU5BU5D_t5_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3131_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2727_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2728_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2735_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2736_0_0_0;
extern const Il2CppGenericInst GenInst_Material_t4_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t14_0_0_0_Object_t_0_0_0_Vector3ArrayOptions_t957_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2747_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2748_0_0_0;
extern const Il2CppGenericInst GenInst_Behaviour_t483_0_0_0;
extern const Il2CppGenericInst GenInst_MonoBehaviour_t7_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3225_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Object_t_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Object_t_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3225_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_PointerEventData_t243_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t433_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit2D_t438_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t135_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t135_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t135_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t135_0_0_0_KeyValuePair_2_t3253_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t417_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t417_0_0_0_Int32_t135_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t273_0_0_0_List_1_t443_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t443_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t285_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t285_0_0_0_Int32_t135_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t281_0_0_0_IndexedSet_1_t454_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t454_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3308_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3316_0_0_0;
extern const Il2CppGenericInst GenInst_Enum_t174_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t455_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t176_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t112_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2744_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2745_0_0_0;
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t592_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t620_0_0_0_Image_t621_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t621_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3399_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Trackable_t571_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3417_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2718_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2719_0_0_0;
extern const Il2CppGenericInst GenInst_Color32_t427_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButton_t737_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3428_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Marker_t745_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3445_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t656_0_0_0;
extern const Il2CppGenericInst GenInst_SurfaceData_t657_0_0_0;
extern const Il2CppGenericInst GenInst_PropData_t658_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t181_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t460_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3475_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t460_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t460_0_0_0_UInt16_t460_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t460_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t460_0_0_0_KeyValuePair_2_t3475_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t460_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3489_0_0_0;
extern const Il2CppGenericInst GenInst_RectangleData_t602_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_WordResult_t701_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3496_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_WordAbstractBehaviour_t101_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t847_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t698_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t698_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3515_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Surface_t106_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t861_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_SurfaceAbstractBehaviour_t81_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3536_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Prop_t105_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t860_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_PropAbstractBehaviour_t73_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3542_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableBehaviour_t52_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3557_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t66_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t189_0_0_0;
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t182_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t180_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t198_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3563_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableResultData_t648_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableResultData_t648_0_0_0_TrackableResultData_t648_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableResultData_t648_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_TrackableResultData_t648_0_0_0_KeyValuePair_2_t3563_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3578_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t649_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonData_t649_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonData_t649_0_0_0_VirtualButtonData_t649_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonData_t649_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonData_t649_0_0_0_KeyValuePair_2_t3578_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_ImageTarget_t738_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3609_0_0_0;
extern const Il2CppGenericInst GenInst_WebCamDevice_t885_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t740_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t740_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t740_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t740_0_0_0_ProfileData_t740_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t740_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t740_0_0_0_KeyValuePair_2_t3617_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t740_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3631_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_VirtualButtonAbstractBehaviour_t94_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3636_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t3666_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_NoOptions_t939_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_StringOptions_t1009_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1462_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievement_t1364_0_0_0;
extern const Il2CppGenericInst GenInst_IScore_t1326_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfile_t1461_0_0_0;
extern const Il2CppGenericInst GenInst_AchievementDescription_t1324_0_0_0;
extern const Il2CppGenericInst GenInst_UserProfile_t1322_0_0_0;
extern const Il2CppGenericInst GenInst_GcAchievementData_t1311_0_0_0;
extern const Il2CppGenericInst GenInst_Achievement_t1323_0_0_0;
extern const Il2CppGenericInst GenInst_GcScoreData_t1312_0_0_0;
extern const Il2CppGenericInst GenInst_Score_t1325_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutOption_t1184_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_LayoutCache_t1176_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutCache_t1176_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3733_0_0_0;
extern const Il2CppGenericInst GenInst_GUIStyle_t1178_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1178_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3744_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t135_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3748_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1401_0_0_0;
extern const Il2CppGenericInst GenInst_Display_t1219_0_0_0;
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0;
extern const Il2CppGenericInst GenInst_ISerializable_t519_0_0_0;
extern const Il2CppGenericInst GenInst_Keyframe_t1242_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1098_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3790_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2710_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2711_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1098_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1098_0_0_0_Int64_t1098_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1098_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1098_0_0_0_KeyValuePair_2_t3790_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1098_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3805_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3828_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2716_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2717_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_Object_t_0_0_0_UInt64_t1097_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_Object_t_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3828_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1273_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1273_0_0_0_NetworkAccessToken_t1275_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t1275_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3843_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t1292_0_0_0;
extern const Il2CppGenericInst GenInst_GetDelegate_t1290_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1382_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1425_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1383_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3849_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t1292_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3856_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1382_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3860_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1383_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3864_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t1290_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0_KeyValuePair_2_t3114_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3114_0_0_0_KeyValuePair_2_t3849_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1425_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3886_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2679_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBase_t1440_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBase_t2682_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterInfo_t1432_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterInfo_t2685_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyInfo_t2686_0_0_0;
extern const Il2CppGenericInst GenInst__FieldInfo_t2681_0_0_0;
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t507_0_0_0;
extern const Il2CppGenericInst GenInst_Attribute_t146_0_0_0;
extern const Il2CppGenericInst GenInst__Attribute_t907_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t506_0_0_0;
extern const Il2CppGenericInst GenInst_RequireComponent_t170_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterModifier_t2266_0_0_0;
extern const Il2CppGenericInst GenInst_HitInfo_t1329_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t323_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t323_0_0_0_TextEditOp_t1341_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_TextEditOp_t1341_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3904_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2713_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2714_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1665_0_0_0;
extern const Il2CppGenericInst GenInst_KeySizes_t1814_0_0_0;
extern const Il2CppGenericInst GenInst_X509Certificate_t1777_0_0_0;
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1615_0_0_0;
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1781_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3945_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t455_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t455_0_0_0_Byte_t455_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t455_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t455_0_0_0_KeyValuePair_2_t3945_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t176_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3959_0_0_0;
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1908_0_0_0;
extern const Il2CppGenericInst GenInst_Capture_t1929_0_0_0;
extern const Il2CppGenericInst GenInst_Group_t1069_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3967_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Int32_t135_0_0_0_Int32_t135_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Int32_t135_0_0_0_DictionaryEntry_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t135_0_0_0_Int32_t135_0_0_0_KeyValuePair_2_t3967_0_0_0;
extern const Il2CppGenericInst GenInst_Mark_t1954_0_0_0;
extern const Il2CppGenericInst GenInst_UriScheme_t1990_0_0_0;
extern const Il2CppGenericInst GenInst_Delegate_t151_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2724_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2725_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2721_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2722_0_0_0;
extern const Il2CppGenericInst GenInst_TableRange_t2075_0_0_0;
extern const Il2CppGenericInst GenInst_TailoringInfo_t2078_0_0_0;
extern const Il2CppGenericInst GenInst_Contraction_t2079_0_0_0;
extern const Il2CppGenericInst GenInst_Level2Map_t2081_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t2101_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t2152_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t2159_0_0_0;
extern const Il2CppGenericInst GenInst_StackFrame_t1439_0_0_0;
extern const Il2CppGenericInst GenInst_Calendar_t2171_0_0_0;
extern const Il2CppGenericInst GenInst_ModuleBuilder_t2231_0_0_0;
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2673_0_0_0;
extern const Il2CppGenericInst GenInst_Module_t2232_0_0_0;
extern const Il2CppGenericInst GenInst__Module_t2684_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterBuilder_t2233_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2674_0_0_0;
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2229_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBuilder_t2228_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBuilder_t2672_0_0_0;
extern const Il2CppGenericInst GenInst__MethodInfo_t2683_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2224_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2669_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyBuilder_t2234_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2675_0_0_0;
extern const Il2CppGenericInst GenInst_FieldBuilder_t2227_0_0_0;
extern const Il2CppGenericInst GenInst__FieldBuilder_t2671_0_0_0;
extern const Il2CppGenericInst GenInst_Header_t2328_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2616_0_0_0;
extern const Il2CppGenericInst GenInst_IContextAttribute_t2611_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t3074_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t3075_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2750_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2751_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t3094_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t3095_0_0_0;
extern const Il2CppGenericInst GenInst_TypeTag_t2366_0_0_0;
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3114_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3114_0_0_0_KeyValuePair_2_t3114_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t238_0_0_0_RaycastResult_t238_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3225_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3225_0_0_0_KeyValuePair_2_t3225_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253_0_0_0_KeyValuePair_2_t3253_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t319_0_0_0_UIVertex_t319_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t460_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t460_0_0_0_UInt16_t460_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3475_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3475_0_0_0_KeyValuePair_2_t3475_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t648_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t648_0_0_0_TrackableResultData_t648_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3563_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3563_0_0_0_KeyValuePair_2_t3563_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t649_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t649_0_0_0_VirtualButtonData_t649_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3578_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3578_0_0_0_KeyValuePair_2_t3578_0_0_0;
extern const Il2CppGenericInst GenInst_TargetSearchResult_t726_0_0_0_TargetSearchResult_t726_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t740_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t740_0_0_0_ProfileData_t740_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617_0_0_0_KeyValuePair_2_t3617_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t466_0_0_0_UICharInfo_t466_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t464_0_0_0_UILineInfo_t464_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1098_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1098_0_0_0_Int64_t1098_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3790_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3790_0_0_0_KeyValuePair_2_t3790_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1097_0_0_0_UInt64_t1097_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3828_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3828_0_0_0_KeyValuePair_2_t3828_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3849_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3849_0_0_0_KeyValuePair_2_t3849_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t455_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t455_0_0_0_Byte_t455_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3945_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3945_0_0_0_KeyValuePair_2_t3945_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3967_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3967_0_0_0_KeyValuePair_2_t3967_0_0_0;
const methodPointerType g_Il2CppMethodPointers[4711] = 
{
	NULL/* 0*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m1985_gshared/* 1*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m1969_gshared/* 2*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m2041_gshared/* 3*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m27985_gshared/* 4*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m27981_gshared/* 5*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m28010_gshared/* 6*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m2022_gshared/* 7*/,
	(methodPointerType)&EventFunction_1__ctor_m15573_gshared/* 8*/,
	(methodPointerType)&EventFunction_1_Invoke_m15575_gshared/* 9*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m15577_gshared/* 10*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m15579_gshared/* 11*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m2162_gshared/* 12*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m2424_gshared/* 13*/,
	(methodPointerType)&IndexedSet_1_get_Count_m16763_gshared/* 14*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m16765_gshared/* 15*/,
	(methodPointerType)&IndexedSet_1_get_Item_m16773_gshared/* 16*/,
	(methodPointerType)&IndexedSet_1_set_Item_m16775_gshared/* 17*/,
	(methodPointerType)&IndexedSet_1__ctor_m16747_gshared/* 18*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16749_gshared/* 19*/,
	(methodPointerType)&IndexedSet_1_Add_m16751_gshared/* 20*/,
	(methodPointerType)&IndexedSet_1_Remove_m16753_gshared/* 21*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m16755_gshared/* 22*/,
	(methodPointerType)&IndexedSet_1_Clear_m16757_gshared/* 23*/,
	(methodPointerType)&IndexedSet_1_Contains_m16759_gshared/* 24*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m16761_gshared/* 25*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m16767_gshared/* 26*/,
	(methodPointerType)&IndexedSet_1_Insert_m16769_gshared/* 27*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m16771_gshared/* 28*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m16776_gshared/* 29*/,
	(methodPointerType)&IndexedSet_1_Sort_m16777_gshared/* 30*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m15676_gshared/* 31*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m15678_gshared/* 32*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m15680_gshared/* 33*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m15682_gshared/* 34*/,
	(methodPointerType)&ObjectPool_1__ctor_m15674_gshared/* 35*/,
	(methodPointerType)&ObjectPool_1_Get_m15684_gshared/* 36*/,
	(methodPointerType)&ObjectPool_1_Release_m15686_gshared/* 37*/,
	(methodPointerType)&NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m28198_gshared/* 38*/,
	(methodPointerType)&SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m28265_gshared/* 39*/,
	(methodPointerType)&TrackerManagerImpl_GetTracker_TisObject_t_m28373_gshared/* 40*/,
	(methodPointerType)&TrackerManagerImpl_InitTracker_TisObject_t_m28374_gshared/* 41*/,
	(methodPointerType)&TrackerManagerImpl_DeinitTracker_TisObject_t_m28375_gshared/* 42*/,
	(methodPointerType)&QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361_gshared/* 43*/,
	(methodPointerType)&TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared/* 44*/,
	(methodPointerType)&TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared/* 45*/,
	(methodPointerType)&TweenSettingsExtensions_OnComplete_TisObject_t_m248_gshared/* 46*/,
	(methodPointerType)&TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared/* 47*/,
	(methodPointerType)&TweenCallback_1__ctor_m23727_gshared/* 48*/,
	(methodPointerType)&TweenCallback_1_Invoke_m23728_gshared/* 49*/,
	(methodPointerType)&TweenCallback_1_BeginInvoke_m23729_gshared/* 50*/,
	(methodPointerType)&TweenCallback_1_EndInvoke_m23730_gshared/* 51*/,
	(methodPointerType)&DOGetter_1__ctor_m23731_gshared/* 52*/,
	(methodPointerType)&DOGetter_1_Invoke_m23732_gshared/* 53*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23733_gshared/* 54*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23734_gshared/* 55*/,
	(methodPointerType)&DOSetter_1__ctor_m23735_gshared/* 56*/,
	(methodPointerType)&DOSetter_1_Invoke_m23736_gshared/* 57*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23737_gshared/* 58*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23738_gshared/* 59*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisObject_t_m28495_gshared/* 60*/,
	(methodPointerType)&Object_Instantiate_TisObject_t_m408_gshared/* 61*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m298_gshared/* 62*/,
	(methodPointerType)&Component_GetComponentInChildren_TisObject_t_m4347_gshared/* 63*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m372_gshared/* 64*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m28126_gshared/* 65*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m4459_gshared/* 66*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m2437_gshared/* 67*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m1967_gshared/* 68*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m2060_gshared/* 69*/,
	(methodPointerType)&GameObject_GetComponentInChildren_TisObject_t_m4633_gshared/* 70*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m27984_gshared/* 71*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m27936_gshared/* 72*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m28127_gshared/* 73*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m506_gshared/* 74*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m2107_gshared/* 75*/,
	(methodPointerType)&GameObject_AddComponent_TisObject_t_m464_gshared/* 76*/,
	(methodPointerType)&ResponseBase_ParseJSONList_TisObject_t_m6982_gshared/* 77*/,
	(methodPointerType)&NetworkMatch_ProcessMatchResponse_TisObject_t_m6989_gshared/* 78*/,
	(methodPointerType)&ResponseDelegate_1__ctor_m25960_gshared/* 79*/,
	(methodPointerType)&ResponseDelegate_1_Invoke_m25962_gshared/* 80*/,
	(methodPointerType)&ResponseDelegate_1_BeginInvoke_m25964_gshared/* 81*/,
	(methodPointerType)&ResponseDelegate_1_EndInvoke_m25966_gshared/* 82*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25968_gshared/* 83*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25969_gshared/* 84*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25967_gshared/* 85*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25970_gshared/* 86*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25971_gshared/* 87*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Keys_m26111_gshared/* 88*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Values_m26117_gshared/* 89*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Item_m26119_gshared/* 90*/,
	(methodPointerType)&ThreadSafeDictionary_2_set_Item_m26121_gshared/* 91*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Count_m26131_gshared/* 92*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_IsReadOnly_m26133_gshared/* 93*/,
	(methodPointerType)&ThreadSafeDictionary_2__ctor_m26101_gshared/* 94*/,
	(methodPointerType)&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103_gshared/* 95*/,
	(methodPointerType)&ThreadSafeDictionary_2_Get_m26105_gshared/* 96*/,
	(methodPointerType)&ThreadSafeDictionary_2_AddValue_m26107_gshared/* 97*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m26109_gshared/* 98*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m26113_gshared/* 99*/,
	(methodPointerType)&ThreadSafeDictionary_2_TryGetValue_m26115_gshared/* 100*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m26123_gshared/* 101*/,
	(methodPointerType)&ThreadSafeDictionary_2_Clear_m26125_gshared/* 102*/,
	(methodPointerType)&ThreadSafeDictionary_2_Contains_m26127_gshared/* 103*/,
	(methodPointerType)&ThreadSafeDictionary_2_CopyTo_m26129_gshared/* 104*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m26135_gshared/* 105*/,
	(methodPointerType)&ThreadSafeDictionary_2_GetEnumerator_m26137_gshared/* 106*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2__ctor_m26094_gshared/* 107*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_Invoke_m26096_gshared/* 108*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m26098_gshared/* 109*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_EndInvoke_m26100_gshared/* 110*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m28009_gshared/* 111*/,
	(methodPointerType)&InvokableCall_1__ctor_m16155_gshared/* 112*/,
	(methodPointerType)&InvokableCall_1__ctor_m16156_gshared/* 113*/,
	(methodPointerType)&InvokableCall_1_Invoke_m16157_gshared/* 114*/,
	(methodPointerType)&InvokableCall_1_Find_m16158_gshared/* 115*/,
	(methodPointerType)&InvokableCall_2__ctor_m26832_gshared/* 116*/,
	(methodPointerType)&InvokableCall_2_Invoke_m26833_gshared/* 117*/,
	(methodPointerType)&InvokableCall_2_Find_m26834_gshared/* 118*/,
	(methodPointerType)&InvokableCall_3__ctor_m26839_gshared/* 119*/,
	(methodPointerType)&InvokableCall_3_Invoke_m26840_gshared/* 120*/,
	(methodPointerType)&InvokableCall_3_Find_m26841_gshared/* 121*/,
	(methodPointerType)&InvokableCall_4__ctor_m26846_gshared/* 122*/,
	(methodPointerType)&InvokableCall_4_Invoke_m26847_gshared/* 123*/,
	(methodPointerType)&InvokableCall_4_Find_m26848_gshared/* 124*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m26853_gshared/* 125*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26854_gshared/* 126*/,
	(methodPointerType)&UnityEvent_1__ctor_m16145_gshared/* 127*/,
	(methodPointerType)&UnityEvent_1_AddListener_m16147_gshared/* 128*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m16149_gshared/* 129*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m16150_gshared/* 130*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16151_gshared/* 131*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16153_gshared/* 132*/,
	(methodPointerType)&UnityEvent_1_Invoke_m16154_gshared/* 133*/,
	(methodPointerType)&UnityEvent_2__ctor_m27057_gshared/* 134*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m27058_gshared/* 135*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m27059_gshared/* 136*/,
	(methodPointerType)&UnityEvent_3__ctor_m27060_gshared/* 137*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m27061_gshared/* 138*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m27062_gshared/* 139*/,
	(methodPointerType)&UnityEvent_4__ctor_m27063_gshared/* 140*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m27064_gshared/* 141*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m27065_gshared/* 142*/,
	(methodPointerType)&UnityAction_1__ctor_m15703_gshared/* 143*/,
	(methodPointerType)&UnityAction_1_Invoke_m15704_gshared/* 144*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m15705_gshared/* 145*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m15706_gshared/* 146*/,
	(methodPointerType)&UnityAction_2__ctor_m26835_gshared/* 147*/,
	(methodPointerType)&UnityAction_2_Invoke_m26836_gshared/* 148*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m26837_gshared/* 149*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m26838_gshared/* 150*/,
	(methodPointerType)&UnityAction_3__ctor_m26842_gshared/* 151*/,
	(methodPointerType)&UnityAction_3_Invoke_m26843_gshared/* 152*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m26844_gshared/* 153*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m26845_gshared/* 154*/,
	(methodPointerType)&UnityAction_4__ctor_m26849_gshared/* 155*/,
	(methodPointerType)&UnityAction_4_Invoke_m26850_gshared/* 156*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m26851_gshared/* 157*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m26852_gshared/* 158*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275_gshared/* 159*/,
	(methodPointerType)&HashSet_1_get_Count_m23283_gshared/* 160*/,
	(methodPointerType)&HashSet_1__ctor_m23269_gshared/* 161*/,
	(methodPointerType)&HashSet_1__ctor_m23271_gshared/* 162*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273_gshared/* 163*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277_gshared/* 164*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279_gshared/* 165*/,
	(methodPointerType)&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281_gshared/* 166*/,
	(methodPointerType)&HashSet_1_Init_m23285_gshared/* 167*/,
	(methodPointerType)&HashSet_1_InitArrays_m23287_gshared/* 168*/,
	(methodPointerType)&HashSet_1_SlotsContainsAt_m23289_gshared/* 169*/,
	(methodPointerType)&HashSet_1_CopyTo_m23291_gshared/* 170*/,
	(methodPointerType)&HashSet_1_CopyTo_m23293_gshared/* 171*/,
	(methodPointerType)&HashSet_1_Resize_m23295_gshared/* 172*/,
	(methodPointerType)&HashSet_1_GetLinkHashCode_m23297_gshared/* 173*/,
	(methodPointerType)&HashSet_1_GetItemHashCode_m23299_gshared/* 174*/,
	(methodPointerType)&HashSet_1_Add_m23300_gshared/* 175*/,
	(methodPointerType)&HashSet_1_Clear_m23302_gshared/* 176*/,
	(methodPointerType)&HashSet_1_Contains_m23304_gshared/* 177*/,
	(methodPointerType)&HashSet_1_Remove_m23306_gshared/* 178*/,
	(methodPointerType)&HashSet_1_GetObjectData_m23308_gshared/* 179*/,
	(methodPointerType)&HashSet_1_OnDeserialization_m23310_gshared/* 180*/,
	(methodPointerType)&HashSet_1_GetEnumerator_m23311_gshared/* 181*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared/* 182*/,
	(methodPointerType)&Enumerator_get_Current_m23320_gshared/* 183*/,
	(methodPointerType)&Enumerator__ctor_m23317_gshared/* 184*/,
	(methodPointerType)&Enumerator_MoveNext_m23319_gshared/* 185*/,
	(methodPointerType)&Enumerator_Dispose_m23321_gshared/* 186*/,
	(methodPointerType)&Enumerator_CheckState_m23322_gshared/* 187*/,
	(methodPointerType)&PrimeHelper__cctor_m23323_gshared/* 188*/,
	(methodPointerType)&PrimeHelper_TestPrime_m23324_gshared/* 189*/,
	(methodPointerType)&PrimeHelper_CalcPrime_m23325_gshared/* 190*/,
	(methodPointerType)&PrimeHelper_ToPrime_m23326_gshared/* 191*/,
	(methodPointerType)&Enumerable_Any_TisObject_t_m4428_gshared/* 192*/,
	(methodPointerType)&Enumerable_Cast_TisObject_t_m4404_gshared/* 193*/,
	(methodPointerType)&Enumerable_CreateCastIterator_TisObject_t_m28196_gshared/* 194*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m4325_gshared/* 195*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m28150_gshared/* 196*/,
	(methodPointerType)&Enumerable_First_TisObject_t_m4480_gshared/* 197*/,
	(methodPointerType)&Enumerable_ToArray_TisObject_t_m4476_gshared/* 198*/,
	(methodPointerType)&Enumerable_ToList_TisObject_t_m455_gshared/* 199*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m2392_gshared/* 200*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m28125_gshared/* 201*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19697_gshared/* 202*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19698_gshared/* 203*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19696_gshared/* 204*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19699_gshared/* 205*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19700_gshared/* 206*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19701_gshared/* 207*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19702_gshared/* 208*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18276_gshared/* 209*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18277_gshared/* 210*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18275_gshared/* 211*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18278_gshared/* 212*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18279_gshared/* 213*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18280_gshared/* 214*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18281_gshared/* 215*/,
	(methodPointerType)&Func_2__ctor_m27066_gshared/* 216*/,
	(methodPointerType)&Func_2_Invoke_m27067_gshared/* 217*/,
	(methodPointerType)&Func_2_BeginInvoke_m27068_gshared/* 218*/,
	(methodPointerType)&Func_2_EndInvoke_m27069_gshared/* 219*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27101_gshared/* 220*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m27102_gshared/* 221*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m27103_gshared/* 222*/,
	(methodPointerType)&LinkedList_1_get_Count_m27116_gshared/* 223*/,
	(methodPointerType)&LinkedList_1_get_First_m27117_gshared/* 224*/,
	(methodPointerType)&LinkedList_1__ctor_m27095_gshared/* 225*/,
	(methodPointerType)&LinkedList_1__ctor_m27096_gshared/* 226*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27097_gshared/* 227*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m27098_gshared/* 228*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27099_gshared/* 229*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m27100_gshared/* 230*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m27104_gshared/* 231*/,
	(methodPointerType)&LinkedList_1_AddLast_m27105_gshared/* 232*/,
	(methodPointerType)&LinkedList_1_Clear_m27106_gshared/* 233*/,
	(methodPointerType)&LinkedList_1_Contains_m27107_gshared/* 234*/,
	(methodPointerType)&LinkedList_1_CopyTo_m27108_gshared/* 235*/,
	(methodPointerType)&LinkedList_1_Find_m27109_gshared/* 236*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m27110_gshared/* 237*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m27111_gshared/* 238*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m27112_gshared/* 239*/,
	(methodPointerType)&LinkedList_1_Remove_m27113_gshared/* 240*/,
	(methodPointerType)&LinkedList_1_Remove_m27114_gshared/* 241*/,
	(methodPointerType)&LinkedList_1_RemoveLast_m27115_gshared/* 242*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27125_gshared/* 243*/,
	(methodPointerType)&Enumerator_get_Current_m27126_gshared/* 244*/,
	(methodPointerType)&Enumerator__ctor_m27124_gshared/* 245*/,
	(methodPointerType)&Enumerator_MoveNext_m27127_gshared/* 246*/,
	(methodPointerType)&Enumerator_Dispose_m27128_gshared/* 247*/,
	(methodPointerType)&LinkedListNode_1_get_List_m27121_gshared/* 248*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m27122_gshared/* 249*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m27123_gshared/* 250*/,
	(methodPointerType)&LinkedListNode_1__ctor_m27118_gshared/* 251*/,
	(methodPointerType)&LinkedListNode_1__ctor_m27119_gshared/* 252*/,
	(methodPointerType)&LinkedListNode_1_Detach_m27120_gshared/* 253*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m15688_gshared/* 254*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m15689_gshared/* 255*/,
	(methodPointerType)&Stack_1_get_Count_m15696_gshared/* 256*/,
	(methodPointerType)&Stack_1__ctor_m15687_gshared/* 257*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m15690_gshared/* 258*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15691_gshared/* 259*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m15692_gshared/* 260*/,
	(methodPointerType)&Stack_1_Peek_m15693_gshared/* 261*/,
	(methodPointerType)&Stack_1_Pop_m15694_gshared/* 262*/,
	(methodPointerType)&Stack_1_Push_m15695_gshared/* 263*/,
	(methodPointerType)&Stack_1_GetEnumerator_m15697_gshared/* 264*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15699_gshared/* 265*/,
	(methodPointerType)&Enumerator_get_Current_m15702_gshared/* 266*/,
	(methodPointerType)&Enumerator__ctor_m15698_gshared/* 267*/,
	(methodPointerType)&Enumerator_Dispose_m15700_gshared/* 268*/,
	(methodPointerType)&Enumerator_MoveNext_m15701_gshared/* 269*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m27874_gshared/* 270*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m27866_gshared/* 271*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m27869_gshared/* 272*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m27867_gshared/* 273*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m27868_gshared/* 274*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m27871_gshared/* 275*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m27870_gshared/* 276*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m27865_gshared/* 277*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m27873_gshared/* 278*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m27963_gshared/* 279*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28774_gshared/* 280*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28775_gshared/* 281*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28776_gshared/* 282*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28777_gshared/* 283*/,
	(methodPointerType)&Array_Sort_TisObject_t_m14025_gshared/* 284*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28778_gshared/* 285*/,
	(methodPointerType)&Array_Sort_TisObject_t_m27962_gshared/* 286*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m27961_gshared/* 287*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28779_gshared/* 288*/,
	(methodPointerType)&Array_Sort_TisObject_t_m27979_gshared/* 289*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m27964_gshared/* 290*/,
	(methodPointerType)&Array_compare_TisObject_t_m27976_gshared/* 291*/,
	(methodPointerType)&Array_qsort_TisObject_t_m27978_gshared/* 292*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m27977_gshared/* 293*/,
	(methodPointerType)&Array_swap_TisObject_t_m27980_gshared/* 294*/,
	(methodPointerType)&Array_Resize_TisObject_t_m5554_gshared/* 295*/,
	(methodPointerType)&Array_Resize_TisObject_t_m27960_gshared/* 296*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m28780_gshared/* 297*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m28781_gshared/* 298*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m28782_gshared/* 299*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28784_gshared/* 300*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28785_gshared/* 301*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28783_gshared/* 302*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28787_gshared/* 303*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28788_gshared/* 304*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28786_gshared/* 305*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28790_gshared/* 306*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28791_gshared/* 307*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28792_gshared/* 308*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28789_gshared/* 309*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14027_gshared/* 310*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m28793_gshared/* 311*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14024_gshared/* 312*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28795_gshared/* 313*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28794_gshared/* 314*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28796_gshared/* 315*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m28797_gshared/* 316*/,
	(methodPointerType)&Array_Exists_TisObject_t_m28798_gshared/* 317*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m28799_gshared/* 318*/,
	(methodPointerType)&Array_Find_TisObject_t_m28800_gshared/* 319*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m28801_gshared/* 320*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared/* 321*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14890_gshared/* 322*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14882_gshared/* 323*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14886_gshared/* 324*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14888_gshared/* 325*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m27547_gshared/* 326*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m27548_gshared/* 327*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m27549_gshared/* 328*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m27550_gshared/* 329*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m27545_gshared/* 330*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27546_gshared/* 331*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m27551_gshared/* 332*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m27552_gshared/* 333*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m27553_gshared/* 334*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m27554_gshared/* 335*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m27555_gshared/* 336*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m27556_gshared/* 337*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m27557_gshared/* 338*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m27558_gshared/* 339*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m27559_gshared/* 340*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m27560_gshared/* 341*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27562_gshared/* 342*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27563_gshared/* 343*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m27561_gshared/* 344*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27564_gshared/* 345*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27565_gshared/* 346*/,
	(methodPointerType)&Comparer_1_get_Default_m15408_gshared/* 347*/,
	(methodPointerType)&Comparer_1__ctor_m15405_gshared/* 348*/,
	(methodPointerType)&Comparer_1__cctor_m15406_gshared/* 349*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15407_gshared/* 350*/,
	(methodPointerType)&DefaultComparer__ctor_m15409_gshared/* 351*/,
	(methodPointerType)&DefaultComparer_Compare_m15410_gshared/* 352*/,
	(methodPointerType)&GenericComparer_1__ctor_m27591_gshared/* 353*/,
	(methodPointerType)&GenericComparer_1_Compare_m27592_gshared/* 354*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14908_gshared/* 355*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14910_gshared/* 356*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m14912_gshared/* 357*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m14914_gshared/* 358*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14922_gshared/* 359*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14924_gshared/* 360*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14926_gshared/* 361*/,
	(methodPointerType)&Dictionary_2_get_Count_m14944_gshared/* 362*/,
	(methodPointerType)&Dictionary_2_get_Item_m14946_gshared/* 363*/,
	(methodPointerType)&Dictionary_2_set_Item_m14948_gshared/* 364*/,
	(methodPointerType)&Dictionary_2_get_Keys_m14982_gshared/* 365*/,
	(methodPointerType)&Dictionary_2_get_Values_m14984_gshared/* 366*/,
	(methodPointerType)&Dictionary_2__ctor_m14896_gshared/* 367*/,
	(methodPointerType)&Dictionary_2__ctor_m14898_gshared/* 368*/,
	(methodPointerType)&Dictionary_2__ctor_m14900_gshared/* 369*/,
	(methodPointerType)&Dictionary_2__ctor_m14902_gshared/* 370*/,
	(methodPointerType)&Dictionary_2__ctor_m14904_gshared/* 371*/,
	(methodPointerType)&Dictionary_2__ctor_m14906_gshared/* 372*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m14916_gshared/* 373*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m14918_gshared/* 374*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m14920_gshared/* 375*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14928_gshared/* 376*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14930_gshared/* 377*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14932_gshared/* 378*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14934_gshared/* 379*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m14936_gshared/* 380*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14938_gshared/* 381*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14940_gshared/* 382*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14942_gshared/* 383*/,
	(methodPointerType)&Dictionary_2_Init_m14950_gshared/* 384*/,
	(methodPointerType)&Dictionary_2_InitArrays_m14952_gshared/* 385*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m14954_gshared/* 386*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27908_gshared/* 387*/,
	(methodPointerType)&Dictionary_2_make_pair_m14956_gshared/* 388*/,
	(methodPointerType)&Dictionary_2_pick_key_m14958_gshared/* 389*/,
	(methodPointerType)&Dictionary_2_pick_value_m14960_gshared/* 390*/,
	(methodPointerType)&Dictionary_2_CopyTo_m14962_gshared/* 391*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27909_gshared/* 392*/,
	(methodPointerType)&Dictionary_2_Resize_m14964_gshared/* 393*/,
	(methodPointerType)&Dictionary_2_Add_m14966_gshared/* 394*/,
	(methodPointerType)&Dictionary_2_Clear_m14968_gshared/* 395*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m14970_gshared/* 396*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m14972_gshared/* 397*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m14974_gshared/* 398*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m14976_gshared/* 399*/,
	(methodPointerType)&Dictionary_2_Remove_m14978_gshared/* 400*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m14980_gshared/* 401*/,
	(methodPointerType)&Dictionary_2_ToTKey_m14986_gshared/* 402*/,
	(methodPointerType)&Dictionary_2_ToTValue_m14988_gshared/* 403*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m14990_gshared/* 404*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m14992_gshared/* 405*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m14994_gshared/* 406*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m15090_gshared/* 407*/,
	(methodPointerType)&ShimEnumerator_get_Key_m15091_gshared/* 408*/,
	(methodPointerType)&ShimEnumerator_get_Value_m15092_gshared/* 409*/,
	(methodPointerType)&ShimEnumerator_get_Current_m15093_gshared/* 410*/,
	(methodPointerType)&ShimEnumerator__ctor_m15088_gshared/* 411*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m15089_gshared/* 412*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared/* 413*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared/* 414*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared/* 415*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared/* 416*/,
	(methodPointerType)&Enumerator_get_Current_m15046_gshared/* 417*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m15047_gshared/* 418*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m15048_gshared/* 419*/,
	(methodPointerType)&Enumerator__ctor_m15040_gshared/* 420*/,
	(methodPointerType)&Enumerator_MoveNext_m15045_gshared/* 421*/,
	(methodPointerType)&Enumerator_VerifyState_m15049_gshared/* 422*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m15050_gshared/* 423*/,
	(methodPointerType)&Enumerator_Dispose_m15051_gshared/* 424*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15029_gshared/* 425*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15030_gshared/* 426*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m15031_gshared/* 427*/,
	(methodPointerType)&KeyCollection_get_Count_m15034_gshared/* 428*/,
	(methodPointerType)&KeyCollection__ctor_m15021_gshared/* 429*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15022_gshared/* 430*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15023_gshared/* 431*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15024_gshared/* 432*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15025_gshared/* 433*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15026_gshared/* 434*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m15027_gshared/* 435*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15028_gshared/* 436*/,
	(methodPointerType)&KeyCollection_CopyTo_m15032_gshared/* 437*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m15033_gshared/* 438*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15036_gshared/* 439*/,
	(methodPointerType)&Enumerator_get_Current_m15039_gshared/* 440*/,
	(methodPointerType)&Enumerator__ctor_m15035_gshared/* 441*/,
	(methodPointerType)&Enumerator_Dispose_m15037_gshared/* 442*/,
	(methodPointerType)&Enumerator_MoveNext_m15038_gshared/* 443*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15064_gshared/* 444*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15065_gshared/* 445*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m15066_gshared/* 446*/,
	(methodPointerType)&ValueCollection_get_Count_m15069_gshared/* 447*/,
	(methodPointerType)&ValueCollection__ctor_m15056_gshared/* 448*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15057_gshared/* 449*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15058_gshared/* 450*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15059_gshared/* 451*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15060_gshared/* 452*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15061_gshared/* 453*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m15062_gshared/* 454*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15063_gshared/* 455*/,
	(methodPointerType)&ValueCollection_CopyTo_m15067_gshared/* 456*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m15068_gshared/* 457*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared/* 458*/,
	(methodPointerType)&Enumerator_get_Current_m15074_gshared/* 459*/,
	(methodPointerType)&Enumerator__ctor_m15070_gshared/* 460*/,
	(methodPointerType)&Enumerator_Dispose_m15072_gshared/* 461*/,
	(methodPointerType)&Enumerator_MoveNext_m15073_gshared/* 462*/,
	(methodPointerType)&Transform_1__ctor_m15052_gshared/* 463*/,
	(methodPointerType)&Transform_1_Invoke_m15053_gshared/* 464*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15054_gshared/* 465*/,
	(methodPointerType)&Transform_1_EndInvoke_m15055_gshared/* 466*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15098_gshared/* 467*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15094_gshared/* 468*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15095_gshared/* 469*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15096_gshared/* 470*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15097_gshared/* 471*/,
	(methodPointerType)&DefaultComparer__ctor_m15104_gshared/* 472*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15105_gshared/* 473*/,
	(methodPointerType)&DefaultComparer_Equals_m15106_gshared/* 474*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m27593_gshared/* 475*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27594_gshared/* 476*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27595_gshared/* 477*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m15001_gshared/* 478*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m15002_gshared/* 479*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m15003_gshared/* 480*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m15004_gshared/* 481*/,
	(methodPointerType)&KeyValuePair_2__ctor_m15000_gshared/* 482*/,
	(methodPointerType)&KeyValuePair_2_ToString_m15005_gshared/* 483*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared/* 484*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared/* 485*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared/* 486*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared/* 487*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared/* 488*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m7219_gshared/* 489*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m7220_gshared/* 490*/,
	(methodPointerType)&List_1_get_Capacity_m15323_gshared/* 491*/,
	(methodPointerType)&List_1_set_Capacity_m15325_gshared/* 492*/,
	(methodPointerType)&List_1_get_Count_m7213_gshared/* 493*/,
	(methodPointerType)&List_1_get_Item_m7236_gshared/* 494*/,
	(methodPointerType)&List_1_set_Item_m7237_gshared/* 495*/,
	(methodPointerType)&List_1__ctor_m6998_gshared/* 496*/,
	(methodPointerType)&List_1__ctor_m15260_gshared/* 497*/,
	(methodPointerType)&List_1__ctor_m15262_gshared/* 498*/,
	(methodPointerType)&List_1__cctor_m15264_gshared/* 499*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared/* 500*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m7216_gshared/* 501*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared/* 502*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m7221_gshared/* 503*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m7223_gshared/* 504*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m7224_gshared/* 505*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m7225_gshared/* 506*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m7226_gshared/* 507*/,
	(methodPointerType)&List_1_Add_m7229_gshared/* 508*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15282_gshared/* 509*/,
	(methodPointerType)&List_1_AddCollection_m15284_gshared/* 510*/,
	(methodPointerType)&List_1_AddEnumerable_m15286_gshared/* 511*/,
	(methodPointerType)&List_1_AddRange_m15287_gshared/* 512*/,
	(methodPointerType)&List_1_AsReadOnly_m15289_gshared/* 513*/,
	(methodPointerType)&List_1_Clear_m7222_gshared/* 514*/,
	(methodPointerType)&List_1_Contains_m7230_gshared/* 515*/,
	(methodPointerType)&List_1_CopyTo_m7231_gshared/* 516*/,
	(methodPointerType)&List_1_Find_m15294_gshared/* 517*/,
	(methodPointerType)&List_1_CheckMatch_m15296_gshared/* 518*/,
	(methodPointerType)&List_1_GetIndex_m15298_gshared/* 519*/,
	(methodPointerType)&List_1_GetEnumerator_m15299_gshared/* 520*/,
	(methodPointerType)&List_1_IndexOf_m7234_gshared/* 521*/,
	(methodPointerType)&List_1_Shift_m15302_gshared/* 522*/,
	(methodPointerType)&List_1_CheckIndex_m15304_gshared/* 523*/,
	(methodPointerType)&List_1_Insert_m7235_gshared/* 524*/,
	(methodPointerType)&List_1_CheckCollection_m15307_gshared/* 525*/,
	(methodPointerType)&List_1_Remove_m7232_gshared/* 526*/,
	(methodPointerType)&List_1_RemoveAll_m15310_gshared/* 527*/,
	(methodPointerType)&List_1_RemoveAt_m7227_gshared/* 528*/,
	(methodPointerType)&List_1_Reverse_m15313_gshared/* 529*/,
	(methodPointerType)&List_1_Sort_m15315_gshared/* 530*/,
	(methodPointerType)&List_1_Sort_m15317_gshared/* 531*/,
	(methodPointerType)&List_1_ToArray_m15319_gshared/* 532*/,
	(methodPointerType)&List_1_TrimExcess_m15321_gshared/* 533*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared/* 534*/,
	(methodPointerType)&Enumerator_get_Current_m15334_gshared/* 535*/,
	(methodPointerType)&Enumerator__ctor_m15329_gshared/* 536*/,
	(methodPointerType)&Enumerator_Dispose_m15331_gshared/* 537*/,
	(methodPointerType)&Enumerator_VerifyState_m15332_gshared/* 538*/,
	(methodPointerType)&Enumerator_MoveNext_m15333_gshared/* 539*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15366_gshared/* 540*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15374_gshared/* 541*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15375_gshared/* 542*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15376_gshared/* 543*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15377_gshared/* 544*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15378_gshared/* 545*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15379_gshared/* 546*/,
	(methodPointerType)&Collection_1_get_Count_m15392_gshared/* 547*/,
	(methodPointerType)&Collection_1_get_Item_m15393_gshared/* 548*/,
	(methodPointerType)&Collection_1_set_Item_m15394_gshared/* 549*/,
	(methodPointerType)&Collection_1__ctor_m15365_gshared/* 550*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15367_gshared/* 551*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15368_gshared/* 552*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15369_gshared/* 553*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15370_gshared/* 554*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15371_gshared/* 555*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15372_gshared/* 556*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15373_gshared/* 557*/,
	(methodPointerType)&Collection_1_Add_m15380_gshared/* 558*/,
	(methodPointerType)&Collection_1_Clear_m15381_gshared/* 559*/,
	(methodPointerType)&Collection_1_ClearItems_m15382_gshared/* 560*/,
	(methodPointerType)&Collection_1_Contains_m15383_gshared/* 561*/,
	(methodPointerType)&Collection_1_CopyTo_m15384_gshared/* 562*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15385_gshared/* 563*/,
	(methodPointerType)&Collection_1_IndexOf_m15386_gshared/* 564*/,
	(methodPointerType)&Collection_1_Insert_m15387_gshared/* 565*/,
	(methodPointerType)&Collection_1_InsertItem_m15388_gshared/* 566*/,
	(methodPointerType)&Collection_1_Remove_m15389_gshared/* 567*/,
	(methodPointerType)&Collection_1_RemoveAt_m15390_gshared/* 568*/,
	(methodPointerType)&Collection_1_RemoveItem_m15391_gshared/* 569*/,
	(methodPointerType)&Collection_1_SetItem_m15395_gshared/* 570*/,
	(methodPointerType)&Collection_1_IsValidItem_m15396_gshared/* 571*/,
	(methodPointerType)&Collection_1_ConvertItem_m15397_gshared/* 572*/,
	(methodPointerType)&Collection_1_CheckWritable_m15398_gshared/* 573*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15399_gshared/* 574*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15400_gshared/* 575*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared/* 576*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared/* 577*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared/* 578*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared/* 579*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared/* 580*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared/* 581*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared/* 582*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared/* 583*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared/* 584*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15363_gshared/* 585*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15364_gshared/* 586*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15335_gshared/* 587*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared/* 588*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared/* 589*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared/* 590*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared/* 591*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared/* 592*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared/* 593*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared/* 594*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared/* 595*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared/* 596*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared/* 597*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared/* 598*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared/* 599*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared/* 600*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared/* 601*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15359_gshared/* 602*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15360_gshared/* 603*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15361_gshared/* 604*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15362_gshared/* 605*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835_gshared/* 606*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836_gshared/* 607*/,
	(methodPointerType)&Getter_2__ctor_m27651_gshared/* 608*/,
	(methodPointerType)&Getter_2_Invoke_m27652_gshared/* 609*/,
	(methodPointerType)&Getter_2_BeginInvoke_m27653_gshared/* 610*/,
	(methodPointerType)&Getter_2_EndInvoke_m27654_gshared/* 611*/,
	(methodPointerType)&StaticGetter_1__ctor_m27655_gshared/* 612*/,
	(methodPointerType)&StaticGetter_1_Invoke_m27656_gshared/* 613*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m27657_gshared/* 614*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m27658_gshared/* 615*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m27982_gshared/* 616*/,
	(methodPointerType)&Action_1__ctor_m15248_gshared/* 617*/,
	(methodPointerType)&Action_1_Invoke_m15250_gshared/* 618*/,
	(methodPointerType)&Action_1_BeginInvoke_m15252_gshared/* 619*/,
	(methodPointerType)&Action_1_EndInvoke_m15254_gshared/* 620*/,
	(methodPointerType)&Comparison_1__ctor_m15416_gshared/* 621*/,
	(methodPointerType)&Comparison_1_Invoke_m15417_gshared/* 622*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15418_gshared/* 623*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15419_gshared/* 624*/,
	(methodPointerType)&Converter_2__ctor_m27541_gshared/* 625*/,
	(methodPointerType)&Converter_2_Invoke_m27542_gshared/* 626*/,
	(methodPointerType)&Converter_2_BeginInvoke_m27543_gshared/* 627*/,
	(methodPointerType)&Converter_2_EndInvoke_m27544_gshared/* 628*/,
	(methodPointerType)&Predicate_1__ctor_m15401_gshared/* 629*/,
	(methodPointerType)&Predicate_1_Invoke_m15402_gshared/* 630*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15403_gshared/* 631*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15404_gshared/* 632*/,
	(methodPointerType)&Nullable_1__ctor_m15177_gshared/* 633*/,
	(methodPointerType)&Nullable_1__ctor_m15188_gshared/* 634*/,
	(methodPointerType)&DOGetter_1__ctor_m389_gshared/* 635*/,
	(methodPointerType)&DOSetter_1__ctor_m390_gshared/* 636*/,
	(methodPointerType)&Action_1__ctor_m15241_gshared/* 637*/,
	(methodPointerType)&Comparison_1__ctor_m1972_gshared/* 638*/,
	(methodPointerType)&List_1_Sort_m1976_gshared/* 639*/,
	(methodPointerType)&List_1__ctor_m2016_gshared/* 640*/,
	(methodPointerType)&Dictionary_2__ctor_m16394_gshared/* 641*/,
	(methodPointerType)&Dictionary_2_get_Values_m16481_gshared/* 642*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16549_gshared/* 643*/,
	(methodPointerType)&Enumerator_get_Current_m16555_gshared/* 644*/,
	(methodPointerType)&Enumerator_MoveNext_m16554_gshared/* 645*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16488_gshared/* 646*/,
	(methodPointerType)&Enumerator_get_Current_m16527_gshared/* 647*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16499_gshared/* 648*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16497_gshared/* 649*/,
	(methodPointerType)&Enumerator_MoveNext_m16526_gshared/* 650*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16501_gshared/* 651*/,
	(methodPointerType)&Comparison_1__ctor_m2075_gshared/* 652*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t102_m2072_gshared/* 653*/,
	(methodPointerType)&UnityEvent_1__ctor_m2079_gshared/* 654*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2081_gshared/* 655*/,
	(methodPointerType)&UnityEvent_1_AddListener_m2082_gshared/* 656*/,
	(methodPointerType)&TweenRunner_1__ctor_m2110_gshared/* 657*/,
	(methodPointerType)&TweenRunner_1_Init_m2111_gshared/* 658*/,
	(methodPointerType)&UnityAction_1__ctor_m2138_gshared/* 659*/,
	(methodPointerType)&TweenRunner_1_StartTween_m2139_gshared/* 660*/,
	(methodPointerType)&List_1_get_Capacity_m2143_gshared/* 661*/,
	(methodPointerType)&List_1_set_Capacity_m2144_gshared/* 662*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t135_m2164_gshared/* 663*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisByte_t455_m2166_gshared/* 664*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t112_m2168_gshared/* 665*/,
	(methodPointerType)&List_1__ctor_m2225_gshared/* 666*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisUInt16_t460_m2221_gshared/* 667*/,
	(methodPointerType)&List_1_ToArray_m2280_gshared/* 668*/,
	(methodPointerType)&UnityEvent_1__ctor_m2312_gshared/* 669*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2318_gshared/* 670*/,
	(methodPointerType)&UnityEvent_1__ctor_m2323_gshared/* 671*/,
	(methodPointerType)&UnityAction_1__ctor_m2324_gshared/* 672*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m2325_gshared/* 673*/,
	(methodPointerType)&UnityEvent_1_AddListener_m2326_gshared/* 674*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2332_gshared/* 675*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t326_m2347_gshared/* 676*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t272_m2349_gshared/* 677*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t345_m2350_gshared/* 678*/,
	(methodPointerType)&UnityEvent_1__ctor_m18151_gshared/* 679*/,
	(methodPointerType)&UnityEvent_1_Invoke_m18160_gshared/* 680*/,
	(methodPointerType)&Func_2__ctor_m18264_gshared/* 681*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t135_m2409_gshared/* 682*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t19_m2411_gshared/* 683*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t112_m2418_gshared/* 684*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisByte_t455_m2420_gshared/* 685*/,
	(methodPointerType)&Func_2__ctor_m18378_gshared/* 686*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2590_gshared/* 687*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2591_gshared/* 688*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2599_gshared/* 689*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2600_gshared/* 690*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2601_gshared/* 691*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2602_gshared/* 692*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m18156_gshared/* 693*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18157_gshared/* 694*/,
	(methodPointerType)&Action_1__ctor_m18611_gshared/* 695*/,
	(methodPointerType)&List_1__ctor_m4498_gshared/* 696*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16470_gshared/* 697*/,
	(methodPointerType)&LinkedList_1__ctor_m4429_gshared/* 698*/,
	(methodPointerType)&LinkedList_1_AddLast_m4438_gshared/* 699*/,
	(methodPointerType)&List_1__ctor_m4439_gshared/* 700*/,
	(methodPointerType)&List_1_GetEnumerator_m4440_gshared/* 701*/,
	(methodPointerType)&Enumerator_get_Current_m4441_gshared/* 702*/,
	(methodPointerType)&Predicate_1__ctor_m4442_gshared/* 703*/,
	(methodPointerType)&Array_Exists_TisTrackableResultData_t648_m4426_gshared/* 704*/,
	(methodPointerType)&Enumerator_MoveNext_m4443_gshared/* 705*/,
	(methodPointerType)&LinkedList_1_get_First_m4444_gshared/* 706*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m4445_gshared/* 707*/,
	(methodPointerType)&Dictionary_2__ctor_m20149_gshared/* 708*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16480_gshared/* 709*/,
	(methodPointerType)&Enumerable_ToList_TisInt32_t135_m4478_gshared/* 710*/,
	(methodPointerType)&Enumerable_ToArray_TisInt32_t135_m4574_gshared/* 711*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m4580_gshared/* 712*/,
	(methodPointerType)&LinkedList_1_Remove_m4581_gshared/* 713*/,
	(methodPointerType)&Dictionary_2__ctor_m4582_gshared/* 714*/,
	(methodPointerType)&Dictionary_2__ctor_m4583_gshared/* 715*/,
	(methodPointerType)&List_1__ctor_m4596_gshared/* 716*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5526_gshared/* 717*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5527_gshared/* 718*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m15235_gshared/* 719*/,
	(methodPointerType)&DOGetter_1__ctor_m5540_gshared/* 720*/,
	(methodPointerType)&DOSetter_1__ctor_m5541_gshared/* 721*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m23739_gshared/* 722*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5550_gshared/* 723*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5551_gshared/* 724*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5552_gshared/* 725*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5561_gshared/* 726*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5568_gshared/* 727*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5569_gshared/* 728*/,
	(methodPointerType)&Nullable_1_get_HasValue_m15178_gshared/* 729*/,
	(methodPointerType)&Nullable_1_get_Value_m15179_gshared/* 730*/,
	(methodPointerType)&Nullable_1_get_HasValue_m15189_gshared/* 731*/,
	(methodPointerType)&Nullable_1_get_Value_m15190_gshared/* 732*/,
	(methodPointerType)&DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_gshared/* 733*/,
	(methodPointerType)&DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_gshared/* 734*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t460_m5579_gshared/* 735*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m23829_gshared/* 736*/,
	(methodPointerType)&List_1__ctor_m23850_gshared/* 737*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5598_gshared/* 738*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5601_gshared/* 739*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5602_gshared/* 740*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5603_gshared/* 741*/,
	(methodPointerType)&List_1__ctor_m6972_gshared/* 742*/,
	(methodPointerType)&List_1__ctor_m6973_gshared/* 743*/,
	(methodPointerType)&List_1__ctor_m6974_gshared/* 744*/,
	(methodPointerType)&Dictionary_2__ctor_m25711_gshared/* 745*/,
	(methodPointerType)&Dictionary_2__ctor_m26445_gshared/* 746*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7056_gshared/* 747*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7057_gshared/* 748*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m26870_gshared/* 749*/,
	(methodPointerType)&Dictionary_2__ctor_m16781_gshared/* 750*/,
	(methodPointerType)&Dictionary_2__ctor_m27131_gshared/* 751*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t135_m9394_gshared/* 752*/,
	(methodPointerType)&GenericComparer_1__ctor_m14029_gshared/* 753*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14030_gshared/* 754*/,
	(methodPointerType)&GenericComparer_1__ctor_m14031_gshared/* 755*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14032_gshared/* 756*/,
	(methodPointerType)&Nullable_1__ctor_m14033_gshared/* 757*/,
	(methodPointerType)&Nullable_1_get_HasValue_m14034_gshared/* 758*/,
	(methodPointerType)&Nullable_1_get_Value_m14035_gshared/* 759*/,
	(methodPointerType)&GenericComparer_1__ctor_m14036_gshared/* 760*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14037_gshared/* 761*/,
	(methodPointerType)&GenericComparer_1__ctor_m14038_gshared/* 762*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14039_gshared/* 763*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3114_m27876_gshared/* 764*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3114_m27877_gshared/* 765*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3114_m27878_gshared/* 766*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3114_m27879_gshared/* 767*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3114_m27880_gshared/* 768*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3114_m27881_gshared/* 769*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3114_m27882_gshared/* 770*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3114_m27884_gshared/* 771*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3114_m27885_gshared/* 772*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t135_m27887_gshared/* 773*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t135_m27888_gshared/* 774*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t135_m27889_gshared/* 775*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t135_m27890_gshared/* 776*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t135_m27891_gshared/* 777*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t135_m27892_gshared/* 778*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t135_m27893_gshared/* 779*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t135_m27895_gshared/* 780*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t135_m27896_gshared/* 781*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t2142_m27898_gshared/* 782*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t2142_m27899_gshared/* 783*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t2142_m27900_gshared/* 784*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t2142_m27901_gshared/* 785*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t2142_m27902_gshared/* 786*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t2142_m27903_gshared/* 787*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t2142_m27904_gshared/* 788*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t2142_m27906_gshared/* 789*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t2142_m27907_gshared/* 790*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t2002_m27911_gshared/* 791*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t2002_m27912_gshared/* 792*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t2002_m27913_gshared/* 793*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t2002_m27914_gshared/* 794*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t2002_m27915_gshared/* 795*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t2002_m27916_gshared/* 796*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t2002_m27917_gshared/* 797*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t2002_m27919_gshared/* 798*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t2002_m27920_gshared/* 799*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m27921_gshared/* 800*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3114_m27923_gshared/* 801*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisObject_t_m27922_gshared/* 802*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisKeyValuePair_2_t3114_m27924_gshared/* 803*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t460_m27926_gshared/* 804*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t460_m27927_gshared/* 805*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t460_m27928_gshared/* 806*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t460_m27929_gshared/* 807*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t460_m27930_gshared/* 808*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t460_m27931_gshared/* 809*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t460_m27932_gshared/* 810*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t460_m27934_gshared/* 811*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t460_m27935_gshared/* 812*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27937_gshared/* 813*/,
	(methodPointerType)&Tweener_DoStartup_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27940_gshared/* 814*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27938_gshared/* 815*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27941_gshared/* 816*/,
	(methodPointerType)&Tweener_DoStartup_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27944_gshared/* 817*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27942_gshared/* 818*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27945_gshared/* 819*/,
	(methodPointerType)&Tweener_DoStartup_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27948_gshared/* 820*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27946_gshared/* 821*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t14_m27950_gshared/* 822*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t14_m27951_gshared/* 823*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t14_m27952_gshared/* 824*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t14_m27953_gshared/* 825*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t14_m27954_gshared/* 826*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t14_m27955_gshared/* 827*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t14_m27956_gshared/* 828*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t14_m27958_gshared/* 829*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t14_m27959_gshared/* 830*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector3_t14_TisObject_t_TisVector3ArrayOptions_t957_m27947_gshared/* 831*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m27943_gshared/* 832*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m27939_gshared/* 833*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t1413_m27966_gshared/* 834*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t1413_m27967_gshared/* 835*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t1413_m27968_gshared/* 836*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t1413_m27969_gshared/* 837*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t1413_m27970_gshared/* 838*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t1413_m27971_gshared/* 839*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t1413_m27972_gshared/* 840*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t1413_m27974_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1413_m27975_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t238_m27987_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t238_m27988_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t238_m27989_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t238_m27990_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t238_m27991_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t238_m27992_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t238_m27993_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t238_m27995_gshared/* 850*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t238_m27996_gshared/* 851*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t238_m27998_gshared/* 852*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t238_m27997_gshared/* 853*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t238_m27999_gshared/* 854*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t238_m28001_gshared/* 855*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t238_TisRaycastResult_t238_m28000_gshared/* 856*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t238_m28002_gshared/* 857*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t238_TisRaycastResult_t238_m28003_gshared/* 858*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t238_m28004_gshared/* 859*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t238_TisRaycastResult_t238_m28005_gshared/* 860*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t238_m28007_gshared/* 861*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t238_m28006_gshared/* 862*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t238_m28008_gshared/* 863*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3225_m28012_gshared/* 864*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3225_m28013_gshared/* 865*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3225_m28014_gshared/* 866*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3225_m28015_gshared/* 867*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3225_m28016_gshared/* 868*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3225_m28017_gshared/* 869*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3225_m28018_gshared/* 870*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3225_m28020_gshared/* 871*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3225_m28021_gshared/* 872*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28023_gshared/* 873*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28022_gshared/* 874*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28024_gshared/* 875*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28026_gshared/* 876*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28025_gshared/* 877*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28027_gshared/* 878*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3225_m28029_gshared/* 879*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3225_TisObject_t_m28028_gshared/* 880*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3225_TisKeyValuePair_2_t3225_m28030_gshared/* 881*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t438_m28032_gshared/* 882*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t438_m28033_gshared/* 883*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t438_m28034_gshared/* 884*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t438_m28035_gshared/* 885*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t438_m28036_gshared/* 886*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t438_m28037_gshared/* 887*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t438_m28038_gshared/* 888*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t438_m28040_gshared/* 889*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t438_m28041_gshared/* 890*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t102_m28043_gshared/* 891*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t102_m28044_gshared/* 892*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t102_m28045_gshared/* 893*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t102_m28046_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t102_m28047_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t102_m28048_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t102_m28049_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t102_m28051_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t102_m28052_gshared/* 899*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t102_m28053_gshared/* 900*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t102_m28054_gshared/* 901*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t102_m28055_gshared/* 902*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t98_m28056_gshared/* 903*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3253_m28058_gshared/* 904*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3253_m28059_gshared/* 905*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3253_m28060_gshared/* 906*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3253_m28061_gshared/* 907*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3253_m28062_gshared/* 908*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3253_m28063_gshared/* 909*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3253_m28064_gshared/* 910*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3253_m28066_gshared/* 911*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3253_m28067_gshared/* 912*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28069_gshared/* 913*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28068_gshared/* 914*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28071_gshared/* 915*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28070_gshared/* 916*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28072_gshared/* 917*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28073_gshared/* 918*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3253_m28075_gshared/* 919*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3253_TisObject_t_m28074_gshared/* 920*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3253_TisKeyValuePair_2_t3253_m28076_gshared/* 921*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t319_m28078_gshared/* 922*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t319_m28077_gshared/* 923*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t319_m28079_gshared/* 924*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t319_m28081_gshared/* 925*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t319_TisUIVertex_t319_m28080_gshared/* 926*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t319_m28082_gshared/* 927*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t319_TisUIVertex_t319_m28083_gshared/* 928*/,
	(methodPointerType)&Array_compare_TisUIVertex_t319_m28084_gshared/* 929*/,
	(methodPointerType)&Array_swap_TisUIVertex_t319_TisUIVertex_t319_m28085_gshared/* 930*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t319_m28087_gshared/* 931*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t319_m28086_gshared/* 932*/,
	(methodPointerType)&Array_swap_TisUIVertex_t319_m28088_gshared/* 933*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t19_m28090_gshared/* 934*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t19_m28091_gshared/* 935*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t19_m28092_gshared/* 936*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t19_m28093_gshared/* 937*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t19_m28094_gshared/* 938*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t19_m28095_gshared/* 939*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t19_m28096_gshared/* 940*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t19_m28098_gshared/* 941*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t19_m28099_gshared/* 942*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t464_m28101_gshared/* 943*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t464_m28102_gshared/* 944*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t464_m28103_gshared/* 945*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t464_m28104_gshared/* 946*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t464_m28105_gshared/* 947*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t464_m28106_gshared/* 948*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t464_m28107_gshared/* 949*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t464_m28109_gshared/* 950*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t464_m28110_gshared/* 951*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t466_m28112_gshared/* 952*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t466_m28113_gshared/* 953*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t466_m28114_gshared/* 954*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t466_m28115_gshared/* 955*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t466_m28116_gshared/* 956*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t466_m28117_gshared/* 957*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t466_m28118_gshared/* 958*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t466_m28120_gshared/* 959*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t466_m28121_gshared/* 960*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t112_m28122_gshared/* 961*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t19_m28123_gshared/* 962*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisByte_t455_m28124_gshared/* 963*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t112_m28129_gshared/* 964*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t112_m28130_gshared/* 965*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t112_m28131_gshared/* 966*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t112_m28132_gshared/* 967*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t112_m28133_gshared/* 968*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t112_m28134_gshared/* 969*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t112_m28135_gshared/* 970*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t112_m28137_gshared/* 971*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t112_m28138_gshared/* 972*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisEyewearCalibrationReading_t592_m28140_gshared/* 973*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t592_m28141_gshared/* 974*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t592_m28142_gshared/* 975*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t592_m28143_gshared/* 976*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t592_m28144_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t592_m28145_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__Insert_TisEyewearCalibrationReading_t592_m28146_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisEyewearCalibrationReading_t592_m28148_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t592_m28149_gshared/* 981*/,
	(methodPointerType)&Array_Resize_TisInt32_t135_m28152_gshared/* 982*/,
	(methodPointerType)&Array_Resize_TisInt32_t135_m28151_gshared/* 983*/,
	(methodPointerType)&Array_IndexOf_TisInt32_t135_m28153_gshared/* 984*/,
	(methodPointerType)&Array_Sort_TisInt32_t135_m28155_gshared/* 985*/,
	(methodPointerType)&Array_Sort_TisInt32_t135_TisInt32_t135_m28154_gshared/* 986*/,
	(methodPointerType)&Array_get_swapper_TisInt32_t135_m28156_gshared/* 987*/,
	(methodPointerType)&Array_qsort_TisInt32_t135_TisInt32_t135_m28157_gshared/* 988*/,
	(methodPointerType)&Array_compare_TisInt32_t135_m28158_gshared/* 989*/,
	(methodPointerType)&Array_swap_TisInt32_t135_TisInt32_t135_m28159_gshared/* 990*/,
	(methodPointerType)&Array_Sort_TisInt32_t135_m28161_gshared/* 991*/,
	(methodPointerType)&Array_qsort_TisInt32_t135_m28160_gshared/* 992*/,
	(methodPointerType)&Array_swap_TisInt32_t135_m28162_gshared/* 993*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t455_m28164_gshared/* 994*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t455_m28165_gshared/* 995*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t455_m28166_gshared/* 996*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t455_m28167_gshared/* 997*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t455_m28168_gshared/* 998*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t455_m28169_gshared/* 999*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t455_m28170_gshared/* 1000*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t455_m28172_gshared/* 1001*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t455_m28173_gshared/* 1002*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor32_t427_m28175_gshared/* 1003*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor32_t427_m28176_gshared/* 1004*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor32_t427_m28177_gshared/* 1005*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor32_t427_m28178_gshared/* 1006*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor32_t427_m28179_gshared/* 1007*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor32_t427_m28180_gshared/* 1008*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor32_t427_m28181_gshared/* 1009*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor32_t427_m28183_gshared/* 1010*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t427_m28184_gshared/* 1011*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor_t98_m28186_gshared/* 1012*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor_t98_m28187_gshared/* 1013*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor_t98_m28188_gshared/* 1014*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor_t98_m28189_gshared/* 1015*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor_t98_m28190_gshared/* 1016*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor_t98_m28191_gshared/* 1017*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor_t98_m28192_gshared/* 1018*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor_t98_m28194_gshared/* 1019*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t98_m28195_gshared/* 1020*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTrackableResultData_t648_m28200_gshared/* 1021*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTrackableResultData_t648_m28201_gshared/* 1022*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTrackableResultData_t648_m28202_gshared/* 1023*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t648_m28203_gshared/* 1024*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTrackableResultData_t648_m28204_gshared/* 1025*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTrackableResultData_t648_m28205_gshared/* 1026*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTrackableResultData_t648_m28206_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTrackableResultData_t648_m28208_gshared/* 1028*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t648_m28209_gshared/* 1029*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordData_t653_m28211_gshared/* 1030*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordData_t653_m28212_gshared/* 1031*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordData_t653_m28213_gshared/* 1032*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordData_t653_m28214_gshared/* 1033*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordData_t653_m28215_gshared/* 1034*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordData_t653_m28216_gshared/* 1035*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordData_t653_m28217_gshared/* 1036*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordData_t653_m28219_gshared/* 1037*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t653_m28220_gshared/* 1038*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordResultData_t652_m28222_gshared/* 1039*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordResultData_t652_m28223_gshared/* 1040*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordResultData_t652_m28224_gshared/* 1041*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordResultData_t652_m28225_gshared/* 1042*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordResultData_t652_m28226_gshared/* 1043*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordResultData_t652_m28227_gshared/* 1044*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordResultData_t652_m28228_gshared/* 1045*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordResultData_t652_m28230_gshared/* 1046*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t652_m28231_gshared/* 1047*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t656_m28233_gshared/* 1048*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t656_m28234_gshared/* 1049*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t656_m28235_gshared/* 1050*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t656_m28236_gshared/* 1051*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t656_m28237_gshared/* 1052*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t656_m28238_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSmartTerrainRevisionData_t656_m28239_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t656_m28241_gshared/* 1055*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t656_m28242_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSurfaceData_t657_m28244_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSurfaceData_t657_m28245_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSurfaceData_t657_m28246_gshared/* 1059*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t657_m28247_gshared/* 1060*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSurfaceData_t657_m28248_gshared/* 1061*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSurfaceData_t657_m28249_gshared/* 1062*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSurfaceData_t657_m28250_gshared/* 1063*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSurfaceData_t657_m28252_gshared/* 1064*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t657_m28253_gshared/* 1065*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisPropData_t658_m28255_gshared/* 1066*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisPropData_t658_m28256_gshared/* 1067*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisPropData_t658_m28257_gshared/* 1068*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisPropData_t658_m28258_gshared/* 1069*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisPropData_t658_m28259_gshared/* 1070*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisPropData_t658_m28260_gshared/* 1071*/,
	(methodPointerType)&Array_InternalArray__Insert_TisPropData_t658_m28261_gshared/* 1072*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisPropData_t658_m28263_gshared/* 1073*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t658_m28264_gshared/* 1074*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3475_m28267_gshared/* 1075*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3475_m28268_gshared/* 1076*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3475_m28269_gshared/* 1077*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3475_m28270_gshared/* 1078*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3475_m28271_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3475_m28272_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3475_m28273_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3475_m28275_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3475_m28276_gshared/* 1083*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28278_gshared/* 1084*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28277_gshared/* 1085*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t460_m28280_gshared/* 1086*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t460_TisObject_t_m28279_gshared/* 1087*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t460_TisUInt16_t460_m28281_gshared/* 1088*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28282_gshared/* 1089*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3475_m28284_gshared/* 1090*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3475_TisObject_t_m28283_gshared/* 1091*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3475_TisKeyValuePair_2_t3475_m28285_gshared/* 1092*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRectangleData_t602_m28287_gshared/* 1093*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRectangleData_t602_m28288_gshared/* 1094*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRectangleData_t602_m28289_gshared/* 1095*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRectangleData_t602_m28290_gshared/* 1096*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRectangleData_t602_m28291_gshared/* 1097*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRectangleData_t602_m28292_gshared/* 1098*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRectangleData_t602_m28293_gshared/* 1099*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRectangleData_t602_m28295_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t602_m28296_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3563_m28298_gshared/* 1102*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3563_m28299_gshared/* 1103*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3563_m28300_gshared/* 1104*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3563_m28301_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3563_m28302_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3563_m28303_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3563_m28304_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3563_m28306_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3563_m28307_gshared/* 1110*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28309_gshared/* 1111*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28308_gshared/* 1112*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28310_gshared/* 1113*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t648_m28312_gshared/* 1114*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t648_TisObject_t_m28311_gshared/* 1115*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t648_TisTrackableResultData_t648_m28313_gshared/* 1116*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28314_gshared/* 1117*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3563_m28316_gshared/* 1118*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3563_TisObject_t_m28315_gshared/* 1119*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3563_TisKeyValuePair_2_t3563_m28317_gshared/* 1120*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3578_m28319_gshared/* 1121*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3578_m28320_gshared/* 1122*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3578_m28321_gshared/* 1123*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3578_m28322_gshared/* 1124*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3578_m28323_gshared/* 1125*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3578_m28324_gshared/* 1126*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3578_m28325_gshared/* 1127*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3578_m28327_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3578_m28328_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVirtualButtonData_t649_m28330_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVirtualButtonData_t649_m28331_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t649_m28332_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t649_m28333_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t649_m28334_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVirtualButtonData_t649_m28335_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVirtualButtonData_t649_m28336_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVirtualButtonData_t649_m28338_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t649_m28339_gshared/* 1138*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28341_gshared/* 1139*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28340_gshared/* 1140*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28342_gshared/* 1141*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t649_m28344_gshared/* 1142*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t649_TisObject_t_m28343_gshared/* 1143*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t649_TisVirtualButtonData_t649_m28345_gshared/* 1144*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28346_gshared/* 1145*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3578_m28348_gshared/* 1146*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3578_TisObject_t_m28347_gshared/* 1147*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3578_TisKeyValuePair_2_t3578_m28349_gshared/* 1148*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTargetSearchResult_t726_m28351_gshared/* 1149*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTargetSearchResult_t726_m28352_gshared/* 1150*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t726_m28353_gshared/* 1151*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t726_m28354_gshared/* 1152*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t726_m28355_gshared/* 1153*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTargetSearchResult_t726_m28356_gshared/* 1154*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTargetSearchResult_t726_m28357_gshared/* 1155*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTargetSearchResult_t726_m28359_gshared/* 1156*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t726_m28360_gshared/* 1157*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t726_m28362_gshared/* 1158*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t726_m28361_gshared/* 1159*/,
	(methodPointerType)&Array_IndexOf_TisTargetSearchResult_t726_m28363_gshared/* 1160*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t726_m28365_gshared/* 1161*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28364_gshared/* 1162*/,
	(methodPointerType)&Array_get_swapper_TisTargetSearchResult_t726_m28366_gshared/* 1163*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28367_gshared/* 1164*/,
	(methodPointerType)&Array_compare_TisTargetSearchResult_t726_m28368_gshared/* 1165*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t726_TisTargetSearchResult_t726_m28369_gshared/* 1166*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t726_m28371_gshared/* 1167*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t726_m28370_gshared/* 1168*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t726_m28372_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWebCamDevice_t885_m28377_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWebCamDevice_t885_m28378_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWebCamDevice_t885_m28379_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t885_m28380_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWebCamDevice_t885_m28381_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWebCamDevice_t885_m28382_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWebCamDevice_t885_m28383_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWebCamDevice_t885_m28385_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t885_m28386_gshared/* 1178*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3617_m28388_gshared/* 1179*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3617_m28389_gshared/* 1180*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3617_m28390_gshared/* 1181*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3617_m28391_gshared/* 1182*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3617_m28392_gshared/* 1183*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3617_m28393_gshared/* 1184*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3617_m28394_gshared/* 1185*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3617_m28396_gshared/* 1186*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3617_m28397_gshared/* 1187*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisProfileData_t740_m28399_gshared/* 1188*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisProfileData_t740_m28400_gshared/* 1189*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisProfileData_t740_m28401_gshared/* 1190*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisProfileData_t740_m28402_gshared/* 1191*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisProfileData_t740_m28403_gshared/* 1192*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisProfileData_t740_m28404_gshared/* 1193*/,
	(methodPointerType)&Array_InternalArray__Insert_TisProfileData_t740_m28405_gshared/* 1194*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisProfileData_t740_m28407_gshared/* 1195*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t740_m28408_gshared/* 1196*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28410_gshared/* 1197*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28409_gshared/* 1198*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t740_m28412_gshared/* 1199*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t740_TisObject_t_m28411_gshared/* 1200*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t740_TisProfileData_t740_m28413_gshared/* 1201*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28414_gshared/* 1202*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3617_m28416_gshared/* 1203*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3617_TisObject_t_m28415_gshared/* 1204*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3617_TisKeyValuePair_2_t3617_m28417_gshared/* 1205*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t3666_m28419_gshared/* 1206*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t3666_m28420_gshared/* 1207*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t3666_m28421_gshared/* 1208*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t3666_m28422_gshared/* 1209*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t3666_m28423_gshared/* 1210*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t3666_m28424_gshared/* 1211*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t3666_m28425_gshared/* 1212*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t3666_m28427_gshared/* 1213*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3666_m28428_gshared/* 1214*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28429_gshared/* 1215*/,
	(methodPointerType)&Tweener_DoStartup_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28432_gshared/* 1216*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28430_gshared/* 1217*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28431_gshared/* 1218*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28433_gshared/* 1219*/,
	(methodPointerType)&Tweener_DoStartup_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28436_gshared/* 1220*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28434_gshared/* 1221*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28435_gshared/* 1222*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t939_m28437_gshared/* 1223*/,
	(methodPointerType)&Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t939_m28440_gshared/* 1224*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisNoOptions_t939_m28438_gshared/* 1225*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisNoOptions_t939_m28439_gshared/* 1226*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28441_gshared/* 1227*/,
	(methodPointerType)&Tweener_DoStartup_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28444_gshared/* 1228*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28442_gshared/* 1229*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28443_gshared/* 1230*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28445_gshared/* 1231*/,
	(methodPointerType)&Tweener_DoStartup_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28448_gshared/* 1232*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28446_gshared/* 1233*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28447_gshared/* 1234*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28449_gshared/* 1235*/,
	(methodPointerType)&Tweener_DoStartup_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28452_gshared/* 1236*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28450_gshared/* 1237*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28451_gshared/* 1238*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28453_gshared/* 1239*/,
	(methodPointerType)&Tweener_DoStartup_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28456_gshared/* 1240*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28454_gshared/* 1241*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28455_gshared/* 1242*/,
	(methodPointerType)&TweenManager_GetTweener_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28457_gshared/* 1243*/,
	(methodPointerType)&Tweener_Setup_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28458_gshared/* 1244*/,
	(methodPointerType)&PluginsManager_GetDefaultPlugin_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m28459_gshared/* 1245*/,
	(methodPointerType)&TweenManager_GetTweener_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28460_gshared/* 1246*/,
	(methodPointerType)&Tweener_Setup_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28461_gshared/* 1247*/,
	(methodPointerType)&PluginsManager_GetDefaultPlugin_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m28462_gshared/* 1248*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1009_m28463_gshared/* 1249*/,
	(methodPointerType)&Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1009_m28466_gshared/* 1250*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisStringOptions_t1009_m28464_gshared/* 1251*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisStringOptions_t1009_m28465_gshared/* 1252*/,
	(methodPointerType)&Array_Resize_TisUInt16_t460_m28468_gshared/* 1253*/,
	(methodPointerType)&Array_Resize_TisUInt16_t460_m28467_gshared/* 1254*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t460_m28469_gshared/* 1255*/,
	(methodPointerType)&Array_Sort_TisUInt16_t460_m28471_gshared/* 1256*/,
	(methodPointerType)&Array_Sort_TisUInt16_t460_TisUInt16_t460_m28470_gshared/* 1257*/,
	(methodPointerType)&Array_get_swapper_TisUInt16_t460_m28472_gshared/* 1258*/,
	(methodPointerType)&Array_qsort_TisUInt16_t460_TisUInt16_t460_m28473_gshared/* 1259*/,
	(methodPointerType)&Array_compare_TisUInt16_t460_m28474_gshared/* 1260*/,
	(methodPointerType)&Array_swap_TisUInt16_t460_TisUInt16_t460_m28475_gshared/* 1261*/,
	(methodPointerType)&Array_Sort_TisUInt16_t460_m28477_gshared/* 1262*/,
	(methodPointerType)&Array_qsort_TisUInt16_t460_m28476_gshared/* 1263*/,
	(methodPointerType)&Array_swap_TisUInt16_t460_m28478_gshared/* 1264*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28479_gshared/* 1265*/,
	(methodPointerType)&Tweener_DoStartup_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28482_gshared/* 1266*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28480_gshared/* 1267*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28481_gshared/* 1268*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28483_gshared/* 1269*/,
	(methodPointerType)&Tweener_DoStartup_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28486_gshared/* 1270*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28484_gshared/* 1271*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28485_gshared/* 1272*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28487_gshared/* 1273*/,
	(methodPointerType)&Tweener_DoStartup_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28490_gshared/* 1274*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28488_gshared/* 1275*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28489_gshared/* 1276*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28491_gshared/* 1277*/,
	(methodPointerType)&Tweener_DoStartup_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28494_gshared/* 1278*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28492_gshared/* 1279*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28493_gshared/* 1280*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t1311_m28497_gshared/* 1281*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t1311_m28498_gshared/* 1282*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1311_m28499_gshared/* 1283*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1311_m28500_gshared/* 1284*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1311_m28501_gshared/* 1285*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t1311_m28502_gshared/* 1286*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t1311_m28503_gshared/* 1287*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t1311_m28505_gshared/* 1288*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1311_m28506_gshared/* 1289*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t1312_m28508_gshared/* 1290*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t1312_m28509_gshared/* 1291*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t1312_m28510_gshared/* 1292*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1312_m28511_gshared/* 1293*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t1312_m28512_gshared/* 1294*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t1312_m28513_gshared/* 1295*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t1312_m28514_gshared/* 1296*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t1312_m28516_gshared/* 1297*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1312_m28517_gshared/* 1298*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m28519_gshared/* 1299*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m28520_gshared/* 1300*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m28521_gshared/* 1301*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m28522_gshared/* 1302*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m28523_gshared/* 1303*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m28524_gshared/* 1304*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m28525_gshared/* 1305*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m28527_gshared/* 1306*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m28528_gshared/* 1307*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t1242_m28530_gshared/* 1308*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t1242_m28531_gshared/* 1309*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t1242_m28532_gshared/* 1310*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1242_m28533_gshared/* 1311*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t1242_m28534_gshared/* 1312*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t1242_m28535_gshared/* 1313*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t1242_m28536_gshared/* 1314*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t1242_m28538_gshared/* 1315*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1242_m28539_gshared/* 1316*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t466_m28541_gshared/* 1317*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t466_m28540_gshared/* 1318*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t466_m28542_gshared/* 1319*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t466_m28544_gshared/* 1320*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t466_TisUICharInfo_t466_m28543_gshared/* 1321*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t466_m28545_gshared/* 1322*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t466_TisUICharInfo_t466_m28546_gshared/* 1323*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t466_m28547_gshared/* 1324*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t466_TisUICharInfo_t466_m28548_gshared/* 1325*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t466_m28550_gshared/* 1326*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t466_m28549_gshared/* 1327*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t466_m28551_gshared/* 1328*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t464_m28553_gshared/* 1329*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t464_m28552_gshared/* 1330*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t464_m28554_gshared/* 1331*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t464_m28556_gshared/* 1332*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t464_TisUILineInfo_t464_m28555_gshared/* 1333*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t464_m28557_gshared/* 1334*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t464_TisUILineInfo_t464_m28558_gshared/* 1335*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t464_m28559_gshared/* 1336*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t464_TisUILineInfo_t464_m28560_gshared/* 1337*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t464_m28562_gshared/* 1338*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t464_m28561_gshared/* 1339*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t464_m28563_gshared/* 1340*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3790_m28565_gshared/* 1341*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3790_m28566_gshared/* 1342*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3790_m28567_gshared/* 1343*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3790_m28568_gshared/* 1344*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3790_m28569_gshared/* 1345*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3790_m28570_gshared/* 1346*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3790_m28571_gshared/* 1347*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3790_m28573_gshared/* 1348*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3790_m28574_gshared/* 1349*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t1098_m28576_gshared/* 1350*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t1098_m28577_gshared/* 1351*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t1098_m28578_gshared/* 1352*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t1098_m28579_gshared/* 1353*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t1098_m28580_gshared/* 1354*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t1098_m28581_gshared/* 1355*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t1098_m28582_gshared/* 1356*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t1098_m28584_gshared/* 1357*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1098_m28585_gshared/* 1358*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28587_gshared/* 1359*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28586_gshared/* 1360*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1098_m28589_gshared/* 1361*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1098_TisObject_t_m28588_gshared/* 1362*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1098_TisInt64_t1098_m28590_gshared/* 1363*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28591_gshared/* 1364*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3790_m28593_gshared/* 1365*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisObject_t_m28592_gshared/* 1366*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisKeyValuePair_2_t3790_m28594_gshared/* 1367*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3828_m28596_gshared/* 1368*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3828_m28597_gshared/* 1369*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3828_m28598_gshared/* 1370*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3828_m28599_gshared/* 1371*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3828_m28600_gshared/* 1372*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3828_m28601_gshared/* 1373*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3828_m28602_gshared/* 1374*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3828_m28604_gshared/* 1375*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3828_m28605_gshared/* 1376*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t1097_m28607_gshared/* 1377*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t1097_m28608_gshared/* 1378*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t1097_m28609_gshared/* 1379*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t1097_m28610_gshared/* 1380*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t1097_m28611_gshared/* 1381*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t1097_m28612_gshared/* 1382*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t1097_m28613_gshared/* 1383*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t1097_m28615_gshared/* 1384*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1097_m28616_gshared/* 1385*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1097_m28618_gshared/* 1386*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1097_TisObject_t_m28617_gshared/* 1387*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1097_TisUInt64_t1097_m28619_gshared/* 1388*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28621_gshared/* 1389*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28620_gshared/* 1390*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28622_gshared/* 1391*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3828_m28624_gshared/* 1392*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3828_TisObject_t_m28623_gshared/* 1393*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3828_TisKeyValuePair_2_t3828_m28625_gshared/* 1394*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3849_m28627_gshared/* 1395*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3849_m28628_gshared/* 1396*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3849_m28629_gshared/* 1397*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3849_m28630_gshared/* 1398*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3849_m28631_gshared/* 1399*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3849_m28632_gshared/* 1400*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3849_m28633_gshared/* 1401*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3849_m28635_gshared/* 1402*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3849_m28636_gshared/* 1403*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28638_gshared/* 1404*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28637_gshared/* 1405*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3114_m28640_gshared/* 1406*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisObject_t_m28639_gshared/* 1407*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3114_TisKeyValuePair_2_t3114_m28641_gshared/* 1408*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28642_gshared/* 1409*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3849_m28644_gshared/* 1410*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3849_TisObject_t_m28643_gshared/* 1411*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3849_TisKeyValuePair_2_t3849_m28645_gshared/* 1412*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t2266_m28647_gshared/* 1413*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t2266_m28648_gshared/* 1414*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t2266_m28649_gshared/* 1415*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2266_m28650_gshared/* 1416*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t2266_m28651_gshared/* 1417*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t2266_m28652_gshared/* 1418*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t2266_m28653_gshared/* 1419*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t2266_m28655_gshared/* 1420*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2266_m28656_gshared/* 1421*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t1329_m28658_gshared/* 1422*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t1329_m28659_gshared/* 1423*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t1329_m28660_gshared/* 1424*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1329_m28661_gshared/* 1425*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t1329_m28662_gshared/* 1426*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t1329_m28663_gshared/* 1427*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t1329_m28664_gshared/* 1428*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t1329_m28666_gshared/* 1429*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1329_m28667_gshared/* 1430*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t135_m28668_gshared/* 1431*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t1081_m28670_gshared/* 1432*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t1081_m28671_gshared/* 1433*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t1081_m28672_gshared/* 1434*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t1081_m28673_gshared/* 1435*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t1081_m28674_gshared/* 1436*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t1081_m28675_gshared/* 1437*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t1081_m28676_gshared/* 1438*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t1081_m28678_gshared/* 1439*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1081_m28679_gshared/* 1440*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3945_m28681_gshared/* 1441*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3945_m28682_gshared/* 1442*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3945_m28683_gshared/* 1443*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3945_m28684_gshared/* 1444*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3945_m28685_gshared/* 1445*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3945_m28686_gshared/* 1446*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3945_m28687_gshared/* 1447*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3945_m28689_gshared/* 1448*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3945_m28690_gshared/* 1449*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28692_gshared/* 1450*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28691_gshared/* 1451*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t455_m28694_gshared/* 1452*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t455_TisObject_t_m28693_gshared/* 1453*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t455_TisByte_t455_m28695_gshared/* 1454*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28696_gshared/* 1455*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3945_m28698_gshared/* 1456*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3945_TisObject_t_m28697_gshared/* 1457*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3945_TisKeyValuePair_2_t3945_m28699_gshared/* 1458*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t1908_m28701_gshared/* 1459*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1908_m28702_gshared/* 1460*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1908_m28703_gshared/* 1461*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1908_m28704_gshared/* 1462*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1908_m28705_gshared/* 1463*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t1908_m28706_gshared/* 1464*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t1908_m28707_gshared/* 1465*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t1908_m28709_gshared/* 1466*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1908_m28710_gshared/* 1467*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3967_m28712_gshared/* 1468*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3967_m28713_gshared/* 1469*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3967_m28714_gshared/* 1470*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3967_m28715_gshared/* 1471*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3967_m28716_gshared/* 1472*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3967_m28717_gshared/* 1473*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3967_m28718_gshared/* 1474*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3967_m28720_gshared/* 1475*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3967_m28721_gshared/* 1476*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t135_m28723_gshared/* 1477*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisObject_t_m28722_gshared/* 1478*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t135_TisInt32_t135_m28724_gshared/* 1479*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2002_TisDictionaryEntry_t2002_m28725_gshared/* 1480*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3967_m28727_gshared/* 1481*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3967_TisObject_t_m28726_gshared/* 1482*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3967_TisKeyValuePair_2_t3967_m28728_gshared/* 1483*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t135_m28729_gshared/* 1484*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t1954_m28731_gshared/* 1485*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t1954_m28732_gshared/* 1486*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t1954_m28733_gshared/* 1487*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t1954_m28734_gshared/* 1488*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t1954_m28735_gshared/* 1489*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t1954_m28736_gshared/* 1490*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t1954_m28737_gshared/* 1491*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t1954_m28739_gshared/* 1492*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1954_m28740_gshared/* 1493*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t1990_m28742_gshared/* 1494*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t1990_m28743_gshared/* 1495*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t1990_m28744_gshared/* 1496*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1990_m28745_gshared/* 1497*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t1990_m28746_gshared/* 1498*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t1990_m28747_gshared/* 1499*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t1990_m28748_gshared/* 1500*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t1990_m28750_gshared/* 1501*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1990_m28751_gshared/* 1502*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t540_m28753_gshared/* 1503*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t540_m28754_gshared/* 1504*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t540_m28755_gshared/* 1505*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t540_m28756_gshared/* 1506*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t540_m28757_gshared/* 1507*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t540_m28758_gshared/* 1508*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t540_m28759_gshared/* 1509*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t540_m28761_gshared/* 1510*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t540_m28762_gshared/* 1511*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t177_m28764_gshared/* 1512*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t177_m28765_gshared/* 1513*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t177_m28766_gshared/* 1514*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t177_m28767_gshared/* 1515*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t177_m28768_gshared/* 1516*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t177_m28769_gshared/* 1517*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t177_m28770_gshared/* 1518*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t177_m28772_gshared/* 1519*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t177_m28773_gshared/* 1520*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t2075_m28803_gshared/* 1521*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t2075_m28804_gshared/* 1522*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t2075_m28805_gshared/* 1523*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806_gshared/* 1524*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807_gshared/* 1525*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t2075_m28808_gshared/* 1526*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t2075_m28809_gshared/* 1527*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t2075_m28811_gshared/* 1528*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812_gshared/* 1529*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2152_m28814_gshared/* 1530*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815_gshared/* 1531*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816_gshared/* 1532*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817_gshared/* 1533*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818_gshared/* 1534*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2152_m28819_gshared/* 1535*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2152_m28820_gshared/* 1536*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2152_m28822_gshared/* 1537*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823_gshared/* 1538*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2159_m28825_gshared/* 1539*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826_gshared/* 1540*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827_gshared/* 1541*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828_gshared/* 1542*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829_gshared/* 1543*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2159_m28830_gshared/* 1544*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2159_m28831_gshared/* 1545*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2159_m28833_gshared/* 1546*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834_gshared/* 1547*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t120_m28838_gshared/* 1548*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839_gshared/* 1549*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840_gshared/* 1550*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841_gshared/* 1551*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842_gshared/* 1552*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t120_m28843_gshared/* 1553*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t120_m28844_gshared/* 1554*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t120_m28846_gshared/* 1555*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847_gshared/* 1556*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t1065_m28849_gshared/* 1557*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850_gshared/* 1558*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851_gshared/* 1559*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852_gshared/* 1560*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853_gshared/* 1561*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t1065_m28854_gshared/* 1562*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t1065_m28855_gshared/* 1563*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t1065_m28857_gshared/* 1564*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858_gshared/* 1565*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t121_m28860_gshared/* 1566*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861_gshared/* 1567*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862_gshared/* 1568*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863_gshared/* 1569*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864_gshared/* 1570*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865_gshared/* 1571*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t121_m28866_gshared/* 1572*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t121_m28868_gshared/* 1573*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869_gshared/* 1574*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14995_gshared/* 1575*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14996_gshared/* 1576*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14997_gshared/* 1577*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14998_gshared/* 1578*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14999_gshared/* 1579*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15011_gshared/* 1580*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15012_gshared/* 1581*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15013_gshared/* 1582*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15014_gshared/* 1583*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15015_gshared/* 1584*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15016_gshared/* 1585*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15017_gshared/* 1586*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15018_gshared/* 1587*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15019_gshared/* 1588*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15020_gshared/* 1589*/,
	(methodPointerType)&Transform_1__ctor_m15075_gshared/* 1590*/,
	(methodPointerType)&Transform_1_Invoke_m15076_gshared/* 1591*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15077_gshared/* 1592*/,
	(methodPointerType)&Transform_1_EndInvoke_m15078_gshared/* 1593*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15079_gshared/* 1594*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_gshared/* 1595*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15081_gshared/* 1596*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15082_gshared/* 1597*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15083_gshared/* 1598*/,
	(methodPointerType)&Transform_1__ctor_m15084_gshared/* 1599*/,
	(methodPointerType)&Transform_1_Invoke_m15085_gshared/* 1600*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15086_gshared/* 1601*/,
	(methodPointerType)&Transform_1_EndInvoke_m15087_gshared/* 1602*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15158_gshared/* 1603*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15160_gshared/* 1604*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15162_gshared/* 1605*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15164_gshared/* 1606*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15166_gshared/* 1607*/,
	(methodPointerType)&Nullable_1_Equals_m15181_gshared/* 1608*/,
	(methodPointerType)&Nullable_1_Equals_m15183_gshared/* 1609*/,
	(methodPointerType)&Nullable_1_GetHashCode_m15185_gshared/* 1610*/,
	(methodPointerType)&Nullable_1_ToString_m15187_gshared/* 1611*/,
	(methodPointerType)&Nullable_1_Equals_m15192_gshared/* 1612*/,
	(methodPointerType)&Nullable_1_Equals_m15194_gshared/* 1613*/,
	(methodPointerType)&Nullable_1_GetHashCode_m15196_gshared/* 1614*/,
	(methodPointerType)&Nullable_1_ToString_m15198_gshared/* 1615*/,
	(methodPointerType)&DOGetter_1_Invoke_m15199_gshared/* 1616*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m15200_gshared/* 1617*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m15201_gshared/* 1618*/,
	(methodPointerType)&DOSetter_1_Invoke_m15202_gshared/* 1619*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m15203_gshared/* 1620*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m15204_gshared/* 1621*/,
	(methodPointerType)&TweenerCore_3__ctor_m15205_gshared/* 1622*/,
	(methodPointerType)&TweenerCore_3_Reset_m15206_gshared/* 1623*/,
	(methodPointerType)&TweenerCore_3_Validate_m15207_gshared/* 1624*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m15208_gshared/* 1625*/,
	(methodPointerType)&TweenerCore_3_Startup_m15209_gshared/* 1626*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m15210_gshared/* 1627*/,
	(methodPointerType)&TweenerCore_3__ctor_m15211_gshared/* 1628*/,
	(methodPointerType)&TweenerCore_3_Reset_m15212_gshared/* 1629*/,
	(methodPointerType)&TweenerCore_3_Validate_m15213_gshared/* 1630*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m15214_gshared/* 1631*/,
	(methodPointerType)&TweenerCore_3_Startup_m15215_gshared/* 1632*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m15216_gshared/* 1633*/,
	(methodPointerType)&DOGetter_1_Invoke_m15217_gshared/* 1634*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m15218_gshared/* 1635*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m15219_gshared/* 1636*/,
	(methodPointerType)&DOSetter_1_Invoke_m15220_gshared/* 1637*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m15221_gshared/* 1638*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m15222_gshared/* 1639*/,
	(methodPointerType)&TweenerCore_3__ctor_m15224_gshared/* 1640*/,
	(methodPointerType)&TweenerCore_3_Reset_m15226_gshared/* 1641*/,
	(methodPointerType)&TweenerCore_3_Validate_m15228_gshared/* 1642*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m15230_gshared/* 1643*/,
	(methodPointerType)&TweenerCore_3_Startup_m15232_gshared/* 1644*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m15234_gshared/* 1645*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15236_gshared/* 1646*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15237_gshared/* 1647*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15238_gshared/* 1648*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15239_gshared/* 1649*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15240_gshared/* 1650*/,
	(methodPointerType)&Action_1_Invoke_m15243_gshared/* 1651*/,
	(methodPointerType)&Action_1_BeginInvoke_m15245_gshared/* 1652*/,
	(methodPointerType)&Action_1_EndInvoke_m15247_gshared/* 1653*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15411_gshared/* 1654*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15412_gshared/* 1655*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15413_gshared/* 1656*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15414_gshared/* 1657*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15415_gshared/* 1658*/,
	(methodPointerType)&Comparison_1_Invoke_m15570_gshared/* 1659*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15571_gshared/* 1660*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15572_gshared/* 1661*/,
	(methodPointerType)&List_1__ctor_m15816_gshared/* 1662*/,
	(methodPointerType)&List_1__ctor_m15817_gshared/* 1663*/,
	(methodPointerType)&List_1__cctor_m15818_gshared/* 1664*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15819_gshared/* 1665*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15820_gshared/* 1666*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15821_gshared/* 1667*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15822_gshared/* 1668*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15823_gshared/* 1669*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15824_gshared/* 1670*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15825_gshared/* 1671*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15826_gshared/* 1672*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15827_gshared/* 1673*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15828_gshared/* 1674*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15829_gshared/* 1675*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15830_gshared/* 1676*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15831_gshared/* 1677*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15832_gshared/* 1678*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15833_gshared/* 1679*/,
	(methodPointerType)&List_1_Add_m15834_gshared/* 1680*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15835_gshared/* 1681*/,
	(methodPointerType)&List_1_AddCollection_m15836_gshared/* 1682*/,
	(methodPointerType)&List_1_AddEnumerable_m15837_gshared/* 1683*/,
	(methodPointerType)&List_1_AddRange_m15838_gshared/* 1684*/,
	(methodPointerType)&List_1_AsReadOnly_m15839_gshared/* 1685*/,
	(methodPointerType)&List_1_Clear_m15840_gshared/* 1686*/,
	(methodPointerType)&List_1_Contains_m15841_gshared/* 1687*/,
	(methodPointerType)&List_1_CopyTo_m15842_gshared/* 1688*/,
	(methodPointerType)&List_1_Find_m15843_gshared/* 1689*/,
	(methodPointerType)&List_1_CheckMatch_m15844_gshared/* 1690*/,
	(methodPointerType)&List_1_GetIndex_m15845_gshared/* 1691*/,
	(methodPointerType)&List_1_GetEnumerator_m15846_gshared/* 1692*/,
	(methodPointerType)&List_1_IndexOf_m15847_gshared/* 1693*/,
	(methodPointerType)&List_1_Shift_m15848_gshared/* 1694*/,
	(methodPointerType)&List_1_CheckIndex_m15849_gshared/* 1695*/,
	(methodPointerType)&List_1_Insert_m15850_gshared/* 1696*/,
	(methodPointerType)&List_1_CheckCollection_m15851_gshared/* 1697*/,
	(methodPointerType)&List_1_Remove_m15852_gshared/* 1698*/,
	(methodPointerType)&List_1_RemoveAll_m15853_gshared/* 1699*/,
	(methodPointerType)&List_1_RemoveAt_m15854_gshared/* 1700*/,
	(methodPointerType)&List_1_Reverse_m15855_gshared/* 1701*/,
	(methodPointerType)&List_1_Sort_m15856_gshared/* 1702*/,
	(methodPointerType)&List_1_ToArray_m15857_gshared/* 1703*/,
	(methodPointerType)&List_1_TrimExcess_m15858_gshared/* 1704*/,
	(methodPointerType)&List_1_get_Capacity_m15859_gshared/* 1705*/,
	(methodPointerType)&List_1_set_Capacity_m15860_gshared/* 1706*/,
	(methodPointerType)&List_1_get_Count_m15861_gshared/* 1707*/,
	(methodPointerType)&List_1_get_Item_m15862_gshared/* 1708*/,
	(methodPointerType)&List_1_set_Item_m15863_gshared/* 1709*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15864_gshared/* 1710*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15865_gshared/* 1711*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15866_gshared/* 1712*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15867_gshared/* 1713*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15868_gshared/* 1714*/,
	(methodPointerType)&Enumerator__ctor_m15869_gshared/* 1715*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15870_gshared/* 1716*/,
	(methodPointerType)&Enumerator_Dispose_m15871_gshared/* 1717*/,
	(methodPointerType)&Enumerator_VerifyState_m15872_gshared/* 1718*/,
	(methodPointerType)&Enumerator_MoveNext_m15873_gshared/* 1719*/,
	(methodPointerType)&Enumerator_get_Current_m15874_gshared/* 1720*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15875_gshared/* 1721*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15876_gshared/* 1722*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15877_gshared/* 1723*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15878_gshared/* 1724*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15879_gshared/* 1725*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15880_gshared/* 1726*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15881_gshared/* 1727*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15882_gshared/* 1728*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15883_gshared/* 1729*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15884_gshared/* 1730*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15885_gshared/* 1731*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15886_gshared/* 1732*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15887_gshared/* 1733*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15888_gshared/* 1734*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15889_gshared/* 1735*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15890_gshared/* 1736*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15891_gshared/* 1737*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15892_gshared/* 1738*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15893_gshared/* 1739*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15894_gshared/* 1740*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15895_gshared/* 1741*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15896_gshared/* 1742*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15897_gshared/* 1743*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15898_gshared/* 1744*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15899_gshared/* 1745*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15900_gshared/* 1746*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15901_gshared/* 1747*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15902_gshared/* 1748*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15903_gshared/* 1749*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15904_gshared/* 1750*/,
	(methodPointerType)&Collection_1__ctor_m15905_gshared/* 1751*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15906_gshared/* 1752*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15907_gshared/* 1753*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15908_gshared/* 1754*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15909_gshared/* 1755*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15910_gshared/* 1756*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15911_gshared/* 1757*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15912_gshared/* 1758*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15913_gshared/* 1759*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15914_gshared/* 1760*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15915_gshared/* 1761*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15916_gshared/* 1762*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15917_gshared/* 1763*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15918_gshared/* 1764*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15919_gshared/* 1765*/,
	(methodPointerType)&Collection_1_Add_m15920_gshared/* 1766*/,
	(methodPointerType)&Collection_1_Clear_m15921_gshared/* 1767*/,
	(methodPointerType)&Collection_1_ClearItems_m15922_gshared/* 1768*/,
	(methodPointerType)&Collection_1_Contains_m15923_gshared/* 1769*/,
	(methodPointerType)&Collection_1_CopyTo_m15924_gshared/* 1770*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15925_gshared/* 1771*/,
	(methodPointerType)&Collection_1_IndexOf_m15926_gshared/* 1772*/,
	(methodPointerType)&Collection_1_Insert_m15927_gshared/* 1773*/,
	(methodPointerType)&Collection_1_InsertItem_m15928_gshared/* 1774*/,
	(methodPointerType)&Collection_1_Remove_m15929_gshared/* 1775*/,
	(methodPointerType)&Collection_1_RemoveAt_m15930_gshared/* 1776*/,
	(methodPointerType)&Collection_1_RemoveItem_m15931_gshared/* 1777*/,
	(methodPointerType)&Collection_1_get_Count_m15932_gshared/* 1778*/,
	(methodPointerType)&Collection_1_get_Item_m15933_gshared/* 1779*/,
	(methodPointerType)&Collection_1_set_Item_m15934_gshared/* 1780*/,
	(methodPointerType)&Collection_1_SetItem_m15935_gshared/* 1781*/,
	(methodPointerType)&Collection_1_IsValidItem_m15936_gshared/* 1782*/,
	(methodPointerType)&Collection_1_ConvertItem_m15937_gshared/* 1783*/,
	(methodPointerType)&Collection_1_CheckWritable_m15938_gshared/* 1784*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15939_gshared/* 1785*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15940_gshared/* 1786*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15941_gshared/* 1787*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15942_gshared/* 1788*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15943_gshared/* 1789*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15944_gshared/* 1790*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15945_gshared/* 1791*/,
	(methodPointerType)&DefaultComparer__ctor_m15946_gshared/* 1792*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15947_gshared/* 1793*/,
	(methodPointerType)&DefaultComparer_Equals_m15948_gshared/* 1794*/,
	(methodPointerType)&Predicate_1__ctor_m15949_gshared/* 1795*/,
	(methodPointerType)&Predicate_1_Invoke_m15950_gshared/* 1796*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15951_gshared/* 1797*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15952_gshared/* 1798*/,
	(methodPointerType)&Comparer_1__ctor_m15953_gshared/* 1799*/,
	(methodPointerType)&Comparer_1__cctor_m15954_gshared/* 1800*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15955_gshared/* 1801*/,
	(methodPointerType)&Comparer_1_get_Default_m15956_gshared/* 1802*/,
	(methodPointerType)&DefaultComparer__ctor_m15957_gshared/* 1803*/,
	(methodPointerType)&DefaultComparer_Compare_m15958_gshared/* 1804*/,
	(methodPointerType)&Dictionary_2__ctor_m16396_gshared/* 1805*/,
	(methodPointerType)&Dictionary_2__ctor_m16398_gshared/* 1806*/,
	(methodPointerType)&Dictionary_2__ctor_m16400_gshared/* 1807*/,
	(methodPointerType)&Dictionary_2__ctor_m16402_gshared/* 1808*/,
	(methodPointerType)&Dictionary_2__ctor_m16404_gshared/* 1809*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16406_gshared/* 1810*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16408_gshared/* 1811*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16410_gshared/* 1812*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16412_gshared/* 1813*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16414_gshared/* 1814*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16416_gshared/* 1815*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16418_gshared/* 1816*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16420_gshared/* 1817*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16422_gshared/* 1818*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16424_gshared/* 1819*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16426_gshared/* 1820*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16428_gshared/* 1821*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16430_gshared/* 1822*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16432_gshared/* 1823*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16434_gshared/* 1824*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16436_gshared/* 1825*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16438_gshared/* 1826*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16440_gshared/* 1827*/,
	(methodPointerType)&Dictionary_2_get_Count_m16442_gshared/* 1828*/,
	(methodPointerType)&Dictionary_2_get_Item_m16444_gshared/* 1829*/,
	(methodPointerType)&Dictionary_2_set_Item_m16446_gshared/* 1830*/,
	(methodPointerType)&Dictionary_2_Init_m16448_gshared/* 1831*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16450_gshared/* 1832*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16452_gshared/* 1833*/,
	(methodPointerType)&Dictionary_2_make_pair_m16454_gshared/* 1834*/,
	(methodPointerType)&Dictionary_2_pick_key_m16456_gshared/* 1835*/,
	(methodPointerType)&Dictionary_2_pick_value_m16458_gshared/* 1836*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16460_gshared/* 1837*/,
	(methodPointerType)&Dictionary_2_Resize_m16462_gshared/* 1838*/,
	(methodPointerType)&Dictionary_2_Add_m16464_gshared/* 1839*/,
	(methodPointerType)&Dictionary_2_Clear_m16466_gshared/* 1840*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16468_gshared/* 1841*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16472_gshared/* 1842*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16474_gshared/* 1843*/,
	(methodPointerType)&Dictionary_2_Remove_m16476_gshared/* 1844*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16478_gshared/* 1845*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16483_gshared/* 1846*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16485_gshared/* 1847*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16487_gshared/* 1848*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16490_gshared/* 1849*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16491_gshared/* 1850*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16492_gshared/* 1851*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16493_gshared/* 1852*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16494_gshared/* 1853*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16495_gshared/* 1854*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16496_gshared/* 1855*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16498_gshared/* 1856*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16500_gshared/* 1857*/,
	(methodPointerType)&KeyCollection__ctor_m16502_gshared/* 1858*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16503_gshared/* 1859*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16504_gshared/* 1860*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16505_gshared/* 1861*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16506_gshared/* 1862*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16507_gshared/* 1863*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16508_gshared/* 1864*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16509_gshared/* 1865*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16510_gshared/* 1866*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16511_gshared/* 1867*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16512_gshared/* 1868*/,
	(methodPointerType)&KeyCollection_CopyTo_m16513_gshared/* 1869*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16514_gshared/* 1870*/,
	(methodPointerType)&KeyCollection_get_Count_m16515_gshared/* 1871*/,
	(methodPointerType)&Enumerator__ctor_m16516_gshared/* 1872*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16517_gshared/* 1873*/,
	(methodPointerType)&Enumerator_Dispose_m16518_gshared/* 1874*/,
	(methodPointerType)&Enumerator_MoveNext_m16519_gshared/* 1875*/,
	(methodPointerType)&Enumerator_get_Current_m16520_gshared/* 1876*/,
	(methodPointerType)&Enumerator__ctor_m16521_gshared/* 1877*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared/* 1878*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared/* 1879*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared/* 1880*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared/* 1881*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16528_gshared/* 1882*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16529_gshared/* 1883*/,
	(methodPointerType)&Enumerator_VerifyState_m16530_gshared/* 1884*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16531_gshared/* 1885*/,
	(methodPointerType)&Enumerator_Dispose_m16532_gshared/* 1886*/,
	(methodPointerType)&Transform_1__ctor_m16533_gshared/* 1887*/,
	(methodPointerType)&Transform_1_Invoke_m16534_gshared/* 1888*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16535_gshared/* 1889*/,
	(methodPointerType)&Transform_1_EndInvoke_m16536_gshared/* 1890*/,
	(methodPointerType)&ValueCollection__ctor_m16537_gshared/* 1891*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16538_gshared/* 1892*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16539_gshared/* 1893*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16540_gshared/* 1894*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16541_gshared/* 1895*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16542_gshared/* 1896*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16543_gshared/* 1897*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16544_gshared/* 1898*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16545_gshared/* 1899*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16546_gshared/* 1900*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16547_gshared/* 1901*/,
	(methodPointerType)&ValueCollection_CopyTo_m16548_gshared/* 1902*/,
	(methodPointerType)&ValueCollection_get_Count_m16550_gshared/* 1903*/,
	(methodPointerType)&Enumerator__ctor_m16551_gshared/* 1904*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared/* 1905*/,
	(methodPointerType)&Enumerator_Dispose_m16553_gshared/* 1906*/,
	(methodPointerType)&Transform_1__ctor_m16556_gshared/* 1907*/,
	(methodPointerType)&Transform_1_Invoke_m16557_gshared/* 1908*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16558_gshared/* 1909*/,
	(methodPointerType)&Transform_1_EndInvoke_m16559_gshared/* 1910*/,
	(methodPointerType)&Transform_1__ctor_m16560_gshared/* 1911*/,
	(methodPointerType)&Transform_1_Invoke_m16561_gshared/* 1912*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16562_gshared/* 1913*/,
	(methodPointerType)&Transform_1_EndInvoke_m16563_gshared/* 1914*/,
	(methodPointerType)&Transform_1__ctor_m16564_gshared/* 1915*/,
	(methodPointerType)&Transform_1_Invoke_m16565_gshared/* 1916*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16566_gshared/* 1917*/,
	(methodPointerType)&Transform_1_EndInvoke_m16567_gshared/* 1918*/,
	(methodPointerType)&ShimEnumerator__ctor_m16568_gshared/* 1919*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16569_gshared/* 1920*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16570_gshared/* 1921*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16571_gshared/* 1922*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16572_gshared/* 1923*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16573_gshared/* 1924*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16574_gshared/* 1925*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16575_gshared/* 1926*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16576_gshared/* 1927*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16577_gshared/* 1928*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16578_gshared/* 1929*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16579_gshared/* 1930*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16580_gshared/* 1931*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16581_gshared/* 1932*/,
	(methodPointerType)&DefaultComparer__ctor_m16582_gshared/* 1933*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16583_gshared/* 1934*/,
	(methodPointerType)&DefaultComparer_Equals_m16584_gshared/* 1935*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16725_gshared/* 1936*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16726_gshared/* 1937*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16727_gshared/* 1938*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16728_gshared/* 1939*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16729_gshared/* 1940*/,
	(methodPointerType)&Comparison_1_Invoke_m16730_gshared/* 1941*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m16731_gshared/* 1942*/,
	(methodPointerType)&Comparison_1_EndInvoke_m16732_gshared/* 1943*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16733_gshared/* 1944*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16734_gshared/* 1945*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16735_gshared/* 1946*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16736_gshared/* 1947*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16737_gshared/* 1948*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m16738_gshared/* 1949*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16739_gshared/* 1950*/,
	(methodPointerType)&UnityAction_1_Invoke_m16740_gshared/* 1951*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m16741_gshared/* 1952*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m16742_gshared/* 1953*/,
	(methodPointerType)&InvokableCall_1__ctor_m16743_gshared/* 1954*/,
	(methodPointerType)&InvokableCall_1__ctor_m16744_gshared/* 1955*/,
	(methodPointerType)&InvokableCall_1_Invoke_m16745_gshared/* 1956*/,
	(methodPointerType)&InvokableCall_1_Find_m16746_gshared/* 1957*/,
	(methodPointerType)&Dictionary_2__ctor_m16778_gshared/* 1958*/,
	(methodPointerType)&Dictionary_2__ctor_m16779_gshared/* 1959*/,
	(methodPointerType)&Dictionary_2__ctor_m16780_gshared/* 1960*/,
	(methodPointerType)&Dictionary_2__ctor_m16782_gshared/* 1961*/,
	(methodPointerType)&Dictionary_2__ctor_m16783_gshared/* 1962*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16784_gshared/* 1963*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16785_gshared/* 1964*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16786_gshared/* 1965*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16787_gshared/* 1966*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16788_gshared/* 1967*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16789_gshared/* 1968*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16790_gshared/* 1969*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16791_gshared/* 1970*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16792_gshared/* 1971*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16793_gshared/* 1972*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16794_gshared/* 1973*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16795_gshared/* 1974*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared/* 1975*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16797_gshared/* 1976*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16798_gshared/* 1977*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16799_gshared/* 1978*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16800_gshared/* 1979*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16801_gshared/* 1980*/,
	(methodPointerType)&Dictionary_2_get_Count_m16802_gshared/* 1981*/,
	(methodPointerType)&Dictionary_2_get_Item_m16803_gshared/* 1982*/,
	(methodPointerType)&Dictionary_2_set_Item_m16804_gshared/* 1983*/,
	(methodPointerType)&Dictionary_2_Init_m16805_gshared/* 1984*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16806_gshared/* 1985*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16807_gshared/* 1986*/,
	(methodPointerType)&Dictionary_2_make_pair_m16808_gshared/* 1987*/,
	(methodPointerType)&Dictionary_2_pick_key_m16809_gshared/* 1988*/,
	(methodPointerType)&Dictionary_2_pick_value_m16810_gshared/* 1989*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16811_gshared/* 1990*/,
	(methodPointerType)&Dictionary_2_Resize_m16812_gshared/* 1991*/,
	(methodPointerType)&Dictionary_2_Add_m16813_gshared/* 1992*/,
	(methodPointerType)&Dictionary_2_Clear_m16814_gshared/* 1993*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16815_gshared/* 1994*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16816_gshared/* 1995*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16817_gshared/* 1996*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16818_gshared/* 1997*/,
	(methodPointerType)&Dictionary_2_Remove_m16819_gshared/* 1998*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16820_gshared/* 1999*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16821_gshared/* 2000*/,
	(methodPointerType)&Dictionary_2_get_Values_m16822_gshared/* 2001*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16823_gshared/* 2002*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16824_gshared/* 2003*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16825_gshared/* 2004*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16826_gshared/* 2005*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16827_gshared/* 2006*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16828_gshared/* 2007*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16829_gshared/* 2008*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16830_gshared/* 2009*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16831_gshared/* 2010*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16832_gshared/* 2011*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16833_gshared/* 2012*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16834_gshared/* 2013*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16835_gshared/* 2014*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16836_gshared/* 2015*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16837_gshared/* 2016*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16838_gshared/* 2017*/,
	(methodPointerType)&KeyCollection__ctor_m16839_gshared/* 2018*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16840_gshared/* 2019*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16841_gshared/* 2020*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16842_gshared/* 2021*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16843_gshared/* 2022*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16844_gshared/* 2023*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16845_gshared/* 2024*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16846_gshared/* 2025*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16847_gshared/* 2026*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16848_gshared/* 2027*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16849_gshared/* 2028*/,
	(methodPointerType)&KeyCollection_CopyTo_m16850_gshared/* 2029*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16851_gshared/* 2030*/,
	(methodPointerType)&KeyCollection_get_Count_m16852_gshared/* 2031*/,
	(methodPointerType)&Enumerator__ctor_m16853_gshared/* 2032*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16854_gshared/* 2033*/,
	(methodPointerType)&Enumerator_Dispose_m16855_gshared/* 2034*/,
	(methodPointerType)&Enumerator_MoveNext_m16856_gshared/* 2035*/,
	(methodPointerType)&Enumerator_get_Current_m16857_gshared/* 2036*/,
	(methodPointerType)&Enumerator__ctor_m16858_gshared/* 2037*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared/* 2038*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared/* 2039*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared/* 2040*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared/* 2041*/,
	(methodPointerType)&Enumerator_MoveNext_m16863_gshared/* 2042*/,
	(methodPointerType)&Enumerator_get_Current_m16864_gshared/* 2043*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16865_gshared/* 2044*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16866_gshared/* 2045*/,
	(methodPointerType)&Enumerator_VerifyState_m16867_gshared/* 2046*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16868_gshared/* 2047*/,
	(methodPointerType)&Enumerator_Dispose_m16869_gshared/* 2048*/,
	(methodPointerType)&Transform_1__ctor_m16870_gshared/* 2049*/,
	(methodPointerType)&Transform_1_Invoke_m16871_gshared/* 2050*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16872_gshared/* 2051*/,
	(methodPointerType)&Transform_1_EndInvoke_m16873_gshared/* 2052*/,
	(methodPointerType)&ValueCollection__ctor_m16874_gshared/* 2053*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16875_gshared/* 2054*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16876_gshared/* 2055*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16877_gshared/* 2056*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16878_gshared/* 2057*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16879_gshared/* 2058*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16880_gshared/* 2059*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16881_gshared/* 2060*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16882_gshared/* 2061*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16883_gshared/* 2062*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16884_gshared/* 2063*/,
	(methodPointerType)&ValueCollection_CopyTo_m16885_gshared/* 2064*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16886_gshared/* 2065*/,
	(methodPointerType)&ValueCollection_get_Count_m16887_gshared/* 2066*/,
	(methodPointerType)&Enumerator__ctor_m16888_gshared/* 2067*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16889_gshared/* 2068*/,
	(methodPointerType)&Enumerator_Dispose_m16890_gshared/* 2069*/,
	(methodPointerType)&Enumerator_MoveNext_m16891_gshared/* 2070*/,
	(methodPointerType)&Enumerator_get_Current_m16892_gshared/* 2071*/,
	(methodPointerType)&Transform_1__ctor_m16893_gshared/* 2072*/,
	(methodPointerType)&Transform_1_Invoke_m16894_gshared/* 2073*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16895_gshared/* 2074*/,
	(methodPointerType)&Transform_1_EndInvoke_m16896_gshared/* 2075*/,
	(methodPointerType)&Transform_1__ctor_m16897_gshared/* 2076*/,
	(methodPointerType)&Transform_1_Invoke_m16898_gshared/* 2077*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16899_gshared/* 2078*/,
	(methodPointerType)&Transform_1_EndInvoke_m16900_gshared/* 2079*/,
	(methodPointerType)&Transform_1__ctor_m16901_gshared/* 2080*/,
	(methodPointerType)&Transform_1_Invoke_m16902_gshared/* 2081*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16903_gshared/* 2082*/,
	(methodPointerType)&Transform_1_EndInvoke_m16904_gshared/* 2083*/,
	(methodPointerType)&ShimEnumerator__ctor_m16905_gshared/* 2084*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16906_gshared/* 2085*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16907_gshared/* 2086*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16908_gshared/* 2087*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16909_gshared/* 2088*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16910_gshared/* 2089*/,
	(methodPointerType)&List_1__ctor_m17271_gshared/* 2090*/,
	(methodPointerType)&List_1__cctor_m17272_gshared/* 2091*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17273_gshared/* 2092*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m17274_gshared/* 2093*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m17275_gshared/* 2094*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m17276_gshared/* 2095*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m17277_gshared/* 2096*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m17278_gshared/* 2097*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m17279_gshared/* 2098*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m17280_gshared/* 2099*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17281_gshared/* 2100*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m17282_gshared/* 2101*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m17283_gshared/* 2102*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m17284_gshared/* 2103*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m17285_gshared/* 2104*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m17286_gshared/* 2105*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m17287_gshared/* 2106*/,
	(methodPointerType)&List_1_Add_m17288_gshared/* 2107*/,
	(methodPointerType)&List_1_GrowIfNeeded_m17289_gshared/* 2108*/,
	(methodPointerType)&List_1_AddCollection_m17290_gshared/* 2109*/,
	(methodPointerType)&List_1_AddEnumerable_m17291_gshared/* 2110*/,
	(methodPointerType)&List_1_AddRange_m17292_gshared/* 2111*/,
	(methodPointerType)&List_1_AsReadOnly_m17293_gshared/* 2112*/,
	(methodPointerType)&List_1_Clear_m17294_gshared/* 2113*/,
	(methodPointerType)&List_1_Contains_m17295_gshared/* 2114*/,
	(methodPointerType)&List_1_CopyTo_m17296_gshared/* 2115*/,
	(methodPointerType)&List_1_Find_m17297_gshared/* 2116*/,
	(methodPointerType)&List_1_CheckMatch_m17298_gshared/* 2117*/,
	(methodPointerType)&List_1_GetIndex_m17299_gshared/* 2118*/,
	(methodPointerType)&List_1_GetEnumerator_m17300_gshared/* 2119*/,
	(methodPointerType)&List_1_IndexOf_m17301_gshared/* 2120*/,
	(methodPointerType)&List_1_Shift_m17302_gshared/* 2121*/,
	(methodPointerType)&List_1_CheckIndex_m17303_gshared/* 2122*/,
	(methodPointerType)&List_1_Insert_m17304_gshared/* 2123*/,
	(methodPointerType)&List_1_CheckCollection_m17305_gshared/* 2124*/,
	(methodPointerType)&List_1_Remove_m17306_gshared/* 2125*/,
	(methodPointerType)&List_1_RemoveAll_m17307_gshared/* 2126*/,
	(methodPointerType)&List_1_RemoveAt_m17308_gshared/* 2127*/,
	(methodPointerType)&List_1_Reverse_m17309_gshared/* 2128*/,
	(methodPointerType)&List_1_Sort_m17310_gshared/* 2129*/,
	(methodPointerType)&List_1_Sort_m17311_gshared/* 2130*/,
	(methodPointerType)&List_1_TrimExcess_m17312_gshared/* 2131*/,
	(methodPointerType)&List_1_get_Count_m17313_gshared/* 2132*/,
	(methodPointerType)&List_1_get_Item_m17314_gshared/* 2133*/,
	(methodPointerType)&List_1_set_Item_m17315_gshared/* 2134*/,
	(methodPointerType)&Enumerator__ctor_m17250_gshared/* 2135*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17251_gshared/* 2136*/,
	(methodPointerType)&Enumerator_Dispose_m17252_gshared/* 2137*/,
	(methodPointerType)&Enumerator_VerifyState_m17253_gshared/* 2138*/,
	(methodPointerType)&Enumerator_MoveNext_m17254_gshared/* 2139*/,
	(methodPointerType)&Enumerator_get_Current_m17255_gshared/* 2140*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m17216_gshared/* 2141*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17217_gshared/* 2142*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17218_gshared/* 2143*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17219_gshared/* 2144*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17220_gshared/* 2145*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17221_gshared/* 2146*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17222_gshared/* 2147*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17223_gshared/* 2148*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17224_gshared/* 2149*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17225_gshared/* 2150*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17226_gshared/* 2151*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m17227_gshared/* 2152*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m17228_gshared/* 2153*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m17229_gshared/* 2154*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17230_gshared/* 2155*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m17231_gshared/* 2156*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m17232_gshared/* 2157*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17233_gshared/* 2158*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17234_gshared/* 2159*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17235_gshared/* 2160*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17236_gshared/* 2161*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17237_gshared/* 2162*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m17238_gshared/* 2163*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m17239_gshared/* 2164*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m17240_gshared/* 2165*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m17241_gshared/* 2166*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m17242_gshared/* 2167*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m17243_gshared/* 2168*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m17244_gshared/* 2169*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m17245_gshared/* 2170*/,
	(methodPointerType)&Collection_1__ctor_m17319_gshared/* 2171*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17320_gshared/* 2172*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m17321_gshared/* 2173*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m17322_gshared/* 2174*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m17323_gshared/* 2175*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m17324_gshared/* 2176*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m17325_gshared/* 2177*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m17326_gshared/* 2178*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m17327_gshared/* 2179*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m17328_gshared/* 2180*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m17329_gshared/* 2181*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m17330_gshared/* 2182*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m17331_gshared/* 2183*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m17332_gshared/* 2184*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m17333_gshared/* 2185*/,
	(methodPointerType)&Collection_1_Add_m17334_gshared/* 2186*/,
	(methodPointerType)&Collection_1_Clear_m17335_gshared/* 2187*/,
	(methodPointerType)&Collection_1_ClearItems_m17336_gshared/* 2188*/,
	(methodPointerType)&Collection_1_Contains_m17337_gshared/* 2189*/,
	(methodPointerType)&Collection_1_CopyTo_m17338_gshared/* 2190*/,
	(methodPointerType)&Collection_1_GetEnumerator_m17339_gshared/* 2191*/,
	(methodPointerType)&Collection_1_IndexOf_m17340_gshared/* 2192*/,
	(methodPointerType)&Collection_1_Insert_m17341_gshared/* 2193*/,
	(methodPointerType)&Collection_1_InsertItem_m17342_gshared/* 2194*/,
	(methodPointerType)&Collection_1_Remove_m17343_gshared/* 2195*/,
	(methodPointerType)&Collection_1_RemoveAt_m17344_gshared/* 2196*/,
	(methodPointerType)&Collection_1_RemoveItem_m17345_gshared/* 2197*/,
	(methodPointerType)&Collection_1_get_Count_m17346_gshared/* 2198*/,
	(methodPointerType)&Collection_1_get_Item_m17347_gshared/* 2199*/,
	(methodPointerType)&Collection_1_set_Item_m17348_gshared/* 2200*/,
	(methodPointerType)&Collection_1_SetItem_m17349_gshared/* 2201*/,
	(methodPointerType)&Collection_1_IsValidItem_m17350_gshared/* 2202*/,
	(methodPointerType)&Collection_1_ConvertItem_m17351_gshared/* 2203*/,
	(methodPointerType)&Collection_1_CheckWritable_m17352_gshared/* 2204*/,
	(methodPointerType)&Collection_1_IsSynchronized_m17353_gshared/* 2205*/,
	(methodPointerType)&Collection_1_IsFixedSize_m17354_gshared/* 2206*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17355_gshared/* 2207*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17356_gshared/* 2208*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17357_gshared/* 2209*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17358_gshared/* 2210*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17359_gshared/* 2211*/,
	(methodPointerType)&DefaultComparer__ctor_m17360_gshared/* 2212*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17361_gshared/* 2213*/,
	(methodPointerType)&DefaultComparer_Equals_m17362_gshared/* 2214*/,
	(methodPointerType)&Predicate_1__ctor_m17246_gshared/* 2215*/,
	(methodPointerType)&Predicate_1_Invoke_m17247_gshared/* 2216*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m17248_gshared/* 2217*/,
	(methodPointerType)&Predicate_1_EndInvoke_m17249_gshared/* 2218*/,
	(methodPointerType)&Comparer_1__ctor_m17363_gshared/* 2219*/,
	(methodPointerType)&Comparer_1__cctor_m17364_gshared/* 2220*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m17365_gshared/* 2221*/,
	(methodPointerType)&Comparer_1_get_Default_m17366_gshared/* 2222*/,
	(methodPointerType)&DefaultComparer__ctor_m17367_gshared/* 2223*/,
	(methodPointerType)&DefaultComparer_Compare_m17368_gshared/* 2224*/,
	(methodPointerType)&Comparison_1__ctor_m17256_gshared/* 2225*/,
	(methodPointerType)&Comparison_1_Invoke_m17257_gshared/* 2226*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m17258_gshared/* 2227*/,
	(methodPointerType)&Comparison_1_EndInvoke_m17259_gshared/* 2228*/,
	(methodPointerType)&TweenRunner_1_Start_m17369_gshared/* 2229*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m17370_gshared/* 2230*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17371_gshared/* 2231*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17372_gshared/* 2232*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m17373_gshared/* 2233*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m17374_gshared/* 2234*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m17375_gshared/* 2235*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17826_gshared/* 2236*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17827_gshared/* 2237*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17828_gshared/* 2238*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17829_gshared/* 2239*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17830_gshared/* 2240*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17836_gshared/* 2241*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17837_gshared/* 2242*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17838_gshared/* 2243*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17839_gshared/* 2244*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17840_gshared/* 2245*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17841_gshared/* 2246*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17842_gshared/* 2247*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17843_gshared/* 2248*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17844_gshared/* 2249*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17845_gshared/* 2250*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m17853_gshared/* 2251*/,
	(methodPointerType)&UnityAction_1_Invoke_m17854_gshared/* 2252*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17855_gshared/* 2253*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17856_gshared/* 2254*/,
	(methodPointerType)&InvokableCall_1__ctor_m17857_gshared/* 2255*/,
	(methodPointerType)&InvokableCall_1__ctor_m17858_gshared/* 2256*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17859_gshared/* 2257*/,
	(methodPointerType)&InvokableCall_1_Find_m17860_gshared/* 2258*/,
	(methodPointerType)&UnityEvent_1_AddListener_m17861_gshared/* 2259*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m17862_gshared/* 2260*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m17863_gshared/* 2261*/,
	(methodPointerType)&UnityAction_1__ctor_m17864_gshared/* 2262*/,
	(methodPointerType)&UnityAction_1_Invoke_m17865_gshared/* 2263*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17866_gshared/* 2264*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17867_gshared/* 2265*/,
	(methodPointerType)&InvokableCall_1__ctor_m17868_gshared/* 2266*/,
	(methodPointerType)&InvokableCall_1__ctor_m17869_gshared/* 2267*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17870_gshared/* 2268*/,
	(methodPointerType)&InvokableCall_1_Find_m17871_gshared/* 2269*/,
	(methodPointerType)&UnityEvent_1_AddListener_m18153_gshared/* 2270*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m18155_gshared/* 2271*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18159_gshared/* 2272*/,
	(methodPointerType)&UnityAction_1__ctor_m18161_gshared/* 2273*/,
	(methodPointerType)&UnityAction_1_Invoke_m18162_gshared/* 2274*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m18163_gshared/* 2275*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m18164_gshared/* 2276*/,
	(methodPointerType)&InvokableCall_1__ctor_m18165_gshared/* 2277*/,
	(methodPointerType)&InvokableCall_1__ctor_m18166_gshared/* 2278*/,
	(methodPointerType)&InvokableCall_1_Invoke_m18167_gshared/* 2279*/,
	(methodPointerType)&InvokableCall_1_Find_m18168_gshared/* 2280*/,
	(methodPointerType)&Func_2_Invoke_m18266_gshared/* 2281*/,
	(methodPointerType)&Func_2_BeginInvoke_m18268_gshared/* 2282*/,
	(methodPointerType)&Func_2_EndInvoke_m18270_gshared/* 2283*/,
	(methodPointerType)&Func_2_Invoke_m18380_gshared/* 2284*/,
	(methodPointerType)&Func_2_BeginInvoke_m18382_gshared/* 2285*/,
	(methodPointerType)&Func_2_EndInvoke_m18384_gshared/* 2286*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18421_gshared/* 2287*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18422_gshared/* 2288*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18423_gshared/* 2289*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18424_gshared/* 2290*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18425_gshared/* 2291*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18426_gshared/* 2292*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18427_gshared/* 2293*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18428_gshared/* 2294*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18429_gshared/* 2295*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18430_gshared/* 2296*/,
	(methodPointerType)&Action_1_Invoke_m18613_gshared/* 2297*/,
	(methodPointerType)&Action_1_BeginInvoke_m18615_gshared/* 2298*/,
	(methodPointerType)&Action_1_EndInvoke_m18617_gshared/* 2299*/,
	(methodPointerType)&List_1__ctor_m18988_gshared/* 2300*/,
	(methodPointerType)&List_1__cctor_m18990_gshared/* 2301*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992_gshared/* 2302*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m18994_gshared/* 2303*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m18996_gshared/* 2304*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m18998_gshared/* 2305*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m19000_gshared/* 2306*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m19002_gshared/* 2307*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m19004_gshared/* 2308*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m19006_gshared/* 2309*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008_gshared/* 2310*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m19010_gshared/* 2311*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m19012_gshared/* 2312*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m19014_gshared/* 2313*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m19016_gshared/* 2314*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m19018_gshared/* 2315*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m19020_gshared/* 2316*/,
	(methodPointerType)&List_1_Add_m19022_gshared/* 2317*/,
	(methodPointerType)&List_1_GrowIfNeeded_m19024_gshared/* 2318*/,
	(methodPointerType)&List_1_AddCollection_m19026_gshared/* 2319*/,
	(methodPointerType)&List_1_AddEnumerable_m19028_gshared/* 2320*/,
	(methodPointerType)&List_1_AddRange_m19030_gshared/* 2321*/,
	(methodPointerType)&List_1_AsReadOnly_m19032_gshared/* 2322*/,
	(methodPointerType)&List_1_Clear_m19034_gshared/* 2323*/,
	(methodPointerType)&List_1_Contains_m19036_gshared/* 2324*/,
	(methodPointerType)&List_1_CopyTo_m19038_gshared/* 2325*/,
	(methodPointerType)&List_1_Find_m19040_gshared/* 2326*/,
	(methodPointerType)&List_1_CheckMatch_m19042_gshared/* 2327*/,
	(methodPointerType)&List_1_GetIndex_m19044_gshared/* 2328*/,
	(methodPointerType)&List_1_IndexOf_m19047_gshared/* 2329*/,
	(methodPointerType)&List_1_Shift_m19049_gshared/* 2330*/,
	(methodPointerType)&List_1_CheckIndex_m19051_gshared/* 2331*/,
	(methodPointerType)&List_1_Insert_m19053_gshared/* 2332*/,
	(methodPointerType)&List_1_CheckCollection_m19055_gshared/* 2333*/,
	(methodPointerType)&List_1_Remove_m19057_gshared/* 2334*/,
	(methodPointerType)&List_1_RemoveAll_m19059_gshared/* 2335*/,
	(methodPointerType)&List_1_RemoveAt_m19061_gshared/* 2336*/,
	(methodPointerType)&List_1_Reverse_m19063_gshared/* 2337*/,
	(methodPointerType)&List_1_Sort_m19065_gshared/* 2338*/,
	(methodPointerType)&List_1_Sort_m19067_gshared/* 2339*/,
	(methodPointerType)&List_1_ToArray_m19069_gshared/* 2340*/,
	(methodPointerType)&List_1_TrimExcess_m19071_gshared/* 2341*/,
	(methodPointerType)&List_1_get_Capacity_m19073_gshared/* 2342*/,
	(methodPointerType)&List_1_set_Capacity_m19075_gshared/* 2343*/,
	(methodPointerType)&List_1_get_Count_m19077_gshared/* 2344*/,
	(methodPointerType)&List_1_get_Item_m19079_gshared/* 2345*/,
	(methodPointerType)&List_1_set_Item_m19081_gshared/* 2346*/,
	(methodPointerType)&Enumerator__ctor_m19082_gshared/* 2347*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19083_gshared/* 2348*/,
	(methodPointerType)&Enumerator_Dispose_m19084_gshared/* 2349*/,
	(methodPointerType)&Enumerator_VerifyState_m19085_gshared/* 2350*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m19086_gshared/* 2351*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19087_gshared/* 2352*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19088_gshared/* 2353*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19089_gshared/* 2354*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19090_gshared/* 2355*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19091_gshared/* 2356*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19092_gshared/* 2357*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19093_gshared/* 2358*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19094_gshared/* 2359*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19095_gshared/* 2360*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19096_gshared/* 2361*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m19097_gshared/* 2362*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m19098_gshared/* 2363*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m19099_gshared/* 2364*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19100_gshared/* 2365*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m19101_gshared/* 2366*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m19102_gshared/* 2367*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19103_gshared/* 2368*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19104_gshared/* 2369*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19105_gshared/* 2370*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19106_gshared/* 2371*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19107_gshared/* 2372*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m19108_gshared/* 2373*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m19109_gshared/* 2374*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m19110_gshared/* 2375*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m19111_gshared/* 2376*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m19112_gshared/* 2377*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m19113_gshared/* 2378*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m19114_gshared/* 2379*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m19115_gshared/* 2380*/,
	(methodPointerType)&Collection_1__ctor_m19116_gshared/* 2381*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19117_gshared/* 2382*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m19118_gshared/* 2383*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m19119_gshared/* 2384*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m19120_gshared/* 2385*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m19121_gshared/* 2386*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m19122_gshared/* 2387*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m19123_gshared/* 2388*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m19124_gshared/* 2389*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m19125_gshared/* 2390*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m19126_gshared/* 2391*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m19127_gshared/* 2392*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m19128_gshared/* 2393*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m19129_gshared/* 2394*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m19130_gshared/* 2395*/,
	(methodPointerType)&Collection_1_Add_m19131_gshared/* 2396*/,
	(methodPointerType)&Collection_1_Clear_m19132_gshared/* 2397*/,
	(methodPointerType)&Collection_1_ClearItems_m19133_gshared/* 2398*/,
	(methodPointerType)&Collection_1_Contains_m19134_gshared/* 2399*/,
	(methodPointerType)&Collection_1_CopyTo_m19135_gshared/* 2400*/,
	(methodPointerType)&Collection_1_GetEnumerator_m19136_gshared/* 2401*/,
	(methodPointerType)&Collection_1_IndexOf_m19137_gshared/* 2402*/,
	(methodPointerType)&Collection_1_Insert_m19138_gshared/* 2403*/,
	(methodPointerType)&Collection_1_InsertItem_m19139_gshared/* 2404*/,
	(methodPointerType)&Collection_1_Remove_m19140_gshared/* 2405*/,
	(methodPointerType)&Collection_1_RemoveAt_m19141_gshared/* 2406*/,
	(methodPointerType)&Collection_1_RemoveItem_m19142_gshared/* 2407*/,
	(methodPointerType)&Collection_1_get_Count_m19143_gshared/* 2408*/,
	(methodPointerType)&Collection_1_get_Item_m19144_gshared/* 2409*/,
	(methodPointerType)&Collection_1_set_Item_m19145_gshared/* 2410*/,
	(methodPointerType)&Collection_1_SetItem_m19146_gshared/* 2411*/,
	(methodPointerType)&Collection_1_IsValidItem_m19147_gshared/* 2412*/,
	(methodPointerType)&Collection_1_ConvertItem_m19148_gshared/* 2413*/,
	(methodPointerType)&Collection_1_CheckWritable_m19149_gshared/* 2414*/,
	(methodPointerType)&Collection_1_IsSynchronized_m19150_gshared/* 2415*/,
	(methodPointerType)&Collection_1_IsFixedSize_m19151_gshared/* 2416*/,
	(methodPointerType)&Predicate_1__ctor_m19152_gshared/* 2417*/,
	(methodPointerType)&Predicate_1_Invoke_m19153_gshared/* 2418*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19154_gshared/* 2419*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19155_gshared/* 2420*/,
	(methodPointerType)&Comparer_1__ctor_m19156_gshared/* 2421*/,
	(methodPointerType)&Comparer_1__cctor_m19157_gshared/* 2422*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m19158_gshared/* 2423*/,
	(methodPointerType)&Comparer_1_get_Default_m19159_gshared/* 2424*/,
	(methodPointerType)&GenericComparer_1__ctor_m19160_gshared/* 2425*/,
	(methodPointerType)&GenericComparer_1_Compare_m19161_gshared/* 2426*/,
	(methodPointerType)&DefaultComparer__ctor_m19162_gshared/* 2427*/,
	(methodPointerType)&DefaultComparer_Compare_m19163_gshared/* 2428*/,
	(methodPointerType)&Comparison_1__ctor_m19164_gshared/* 2429*/,
	(methodPointerType)&Comparison_1_Invoke_m19165_gshared/* 2430*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m19166_gshared/* 2431*/,
	(methodPointerType)&Comparison_1_EndInvoke_m19167_gshared/* 2432*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19399_gshared/* 2433*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400_gshared/* 2434*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19401_gshared/* 2435*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19402_gshared/* 2436*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19403_gshared/* 2437*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19404_gshared/* 2438*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19405_gshared/* 2439*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19406_gshared/* 2440*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19407_gshared/* 2441*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19408_gshared/* 2442*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19409_gshared/* 2443*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19410_gshared/* 2444*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19411_gshared/* 2445*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19412_gshared/* 2446*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19413_gshared/* 2447*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19893_gshared/* 2448*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19894_gshared/* 2449*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19895_gshared/* 2450*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19896_gshared/* 2451*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19897_gshared/* 2452*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19898_gshared/* 2453*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19899_gshared/* 2454*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19900_gshared/* 2455*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19901_gshared/* 2456*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19902_gshared/* 2457*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19903_gshared/* 2458*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19904_gshared/* 2459*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19905_gshared/* 2460*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19906_gshared/* 2461*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19907_gshared/* 2462*/,
	(methodPointerType)&LinkedList_1__ctor_m19908_gshared/* 2463*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19909_gshared/* 2464*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m19910_gshared/* 2465*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19911_gshared/* 2466*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19912_gshared/* 2467*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19913_gshared/* 2468*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19914_gshared/* 2469*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19915_gshared/* 2470*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m19916_gshared/* 2471*/,
	(methodPointerType)&LinkedList_1_Clear_m19917_gshared/* 2472*/,
	(methodPointerType)&LinkedList_1_Contains_m19918_gshared/* 2473*/,
	(methodPointerType)&LinkedList_1_CopyTo_m19919_gshared/* 2474*/,
	(methodPointerType)&LinkedList_1_Find_m19920_gshared/* 2475*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m19921_gshared/* 2476*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m19922_gshared/* 2477*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m19923_gshared/* 2478*/,
	(methodPointerType)&LinkedList_1_Remove_m19924_gshared/* 2479*/,
	(methodPointerType)&LinkedList_1_RemoveLast_m19925_gshared/* 2480*/,
	(methodPointerType)&LinkedList_1_get_Count_m19926_gshared/* 2481*/,
	(methodPointerType)&LinkedListNode_1__ctor_m19927_gshared/* 2482*/,
	(methodPointerType)&LinkedListNode_1__ctor_m19928_gshared/* 2483*/,
	(methodPointerType)&LinkedListNode_1_Detach_m19929_gshared/* 2484*/,
	(methodPointerType)&LinkedListNode_1_get_List_m19930_gshared/* 2485*/,
	(methodPointerType)&Enumerator__ctor_m19931_gshared/* 2486*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19932_gshared/* 2487*/,
	(methodPointerType)&Enumerator_get_Current_m19933_gshared/* 2488*/,
	(methodPointerType)&Enumerator_MoveNext_m19934_gshared/* 2489*/,
	(methodPointerType)&Enumerator_Dispose_m19935_gshared/* 2490*/,
	(methodPointerType)&Predicate_1_Invoke_m19936_gshared/* 2491*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19937_gshared/* 2492*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19938_gshared/* 2493*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19939_gshared/* 2494*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared/* 2495*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19941_gshared/* 2496*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19942_gshared/* 2497*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19943_gshared/* 2498*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19944_gshared/* 2499*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19945_gshared/* 2500*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19946_gshared/* 2501*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19947_gshared/* 2502*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19948_gshared/* 2503*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19949_gshared/* 2504*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19950_gshared/* 2505*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19951_gshared/* 2506*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19952_gshared/* 2507*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19953_gshared/* 2508*/,
	(methodPointerType)&Dictionary_2__ctor_m20151_gshared/* 2509*/,
	(methodPointerType)&Dictionary_2__ctor_m20153_gshared/* 2510*/,
	(methodPointerType)&Dictionary_2__ctor_m20155_gshared/* 2511*/,
	(methodPointerType)&Dictionary_2__ctor_m20157_gshared/* 2512*/,
	(methodPointerType)&Dictionary_2__ctor_m20159_gshared/* 2513*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161_gshared/* 2514*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163_gshared/* 2515*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m20165_gshared/* 2516*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m20167_gshared/* 2517*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m20169_gshared/* 2518*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m20171_gshared/* 2519*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m20173_gshared/* 2520*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175_gshared/* 2521*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177_gshared/* 2522*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179_gshared/* 2523*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181_gshared/* 2524*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183_gshared/* 2525*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185_gshared/* 2526*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187_gshared/* 2527*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m20189_gshared/* 2528*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191_gshared/* 2529*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193_gshared/* 2530*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195_gshared/* 2531*/,
	(methodPointerType)&Dictionary_2_get_Count_m20197_gshared/* 2532*/,
	(methodPointerType)&Dictionary_2_get_Item_m20199_gshared/* 2533*/,
	(methodPointerType)&Dictionary_2_set_Item_m20201_gshared/* 2534*/,
	(methodPointerType)&Dictionary_2_Init_m20203_gshared/* 2535*/,
	(methodPointerType)&Dictionary_2_InitArrays_m20205_gshared/* 2536*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m20207_gshared/* 2537*/,
	(methodPointerType)&Dictionary_2_make_pair_m20209_gshared/* 2538*/,
	(methodPointerType)&Dictionary_2_pick_key_m20211_gshared/* 2539*/,
	(methodPointerType)&Dictionary_2_pick_value_m20213_gshared/* 2540*/,
	(methodPointerType)&Dictionary_2_CopyTo_m20215_gshared/* 2541*/,
	(methodPointerType)&Dictionary_2_Resize_m20217_gshared/* 2542*/,
	(methodPointerType)&Dictionary_2_Add_m20219_gshared/* 2543*/,
	(methodPointerType)&Dictionary_2_Clear_m20221_gshared/* 2544*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m20223_gshared/* 2545*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m20225_gshared/* 2546*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m20227_gshared/* 2547*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m20229_gshared/* 2548*/,
	(methodPointerType)&Dictionary_2_Remove_m20231_gshared/* 2549*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m20233_gshared/* 2550*/,
	(methodPointerType)&Dictionary_2_get_Keys_m20235_gshared/* 2551*/,
	(methodPointerType)&Dictionary_2_get_Values_m20237_gshared/* 2552*/,
	(methodPointerType)&Dictionary_2_ToTKey_m20239_gshared/* 2553*/,
	(methodPointerType)&Dictionary_2_ToTValue_m20241_gshared/* 2554*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m20243_gshared/* 2555*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m20245_gshared/* 2556*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m20247_gshared/* 2557*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20248_gshared/* 2558*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20249_gshared/* 2559*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20250_gshared/* 2560*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20251_gshared/* 2561*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20252_gshared/* 2562*/,
	(methodPointerType)&KeyValuePair_2__ctor_m20253_gshared/* 2563*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m20254_gshared/* 2564*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m20255_gshared/* 2565*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m20256_gshared/* 2566*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m20257_gshared/* 2567*/,
	(methodPointerType)&KeyValuePair_2_ToString_m20258_gshared/* 2568*/,
	(methodPointerType)&KeyCollection__ctor_m20259_gshared/* 2569*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20260_gshared/* 2570*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20261_gshared/* 2571*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20262_gshared/* 2572*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20263_gshared/* 2573*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20264_gshared/* 2574*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m20265_gshared/* 2575*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20266_gshared/* 2576*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20267_gshared/* 2577*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20268_gshared/* 2578*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m20269_gshared/* 2579*/,
	(methodPointerType)&KeyCollection_CopyTo_m20270_gshared/* 2580*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m20271_gshared/* 2581*/,
	(methodPointerType)&KeyCollection_get_Count_m20272_gshared/* 2582*/,
	(methodPointerType)&Enumerator__ctor_m20273_gshared/* 2583*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20274_gshared/* 2584*/,
	(methodPointerType)&Enumerator_Dispose_m20275_gshared/* 2585*/,
	(methodPointerType)&Enumerator_MoveNext_m20276_gshared/* 2586*/,
	(methodPointerType)&Enumerator_get_Current_m20277_gshared/* 2587*/,
	(methodPointerType)&Enumerator__ctor_m20278_gshared/* 2588*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared/* 2589*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280_gshared/* 2590*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281_gshared/* 2591*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282_gshared/* 2592*/,
	(methodPointerType)&Enumerator_MoveNext_m20283_gshared/* 2593*/,
	(methodPointerType)&Enumerator_get_Current_m20284_gshared/* 2594*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m20285_gshared/* 2595*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m20286_gshared/* 2596*/,
	(methodPointerType)&Enumerator_VerifyState_m20287_gshared/* 2597*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m20288_gshared/* 2598*/,
	(methodPointerType)&Enumerator_Dispose_m20289_gshared/* 2599*/,
	(methodPointerType)&Transform_1__ctor_m20290_gshared/* 2600*/,
	(methodPointerType)&Transform_1_Invoke_m20291_gshared/* 2601*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20292_gshared/* 2602*/,
	(methodPointerType)&Transform_1_EndInvoke_m20293_gshared/* 2603*/,
	(methodPointerType)&ValueCollection__ctor_m20294_gshared/* 2604*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20295_gshared/* 2605*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20296_gshared/* 2606*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20297_gshared/* 2607*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20298_gshared/* 2608*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20299_gshared/* 2609*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m20300_gshared/* 2610*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20301_gshared/* 2611*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20302_gshared/* 2612*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20303_gshared/* 2613*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m20304_gshared/* 2614*/,
	(methodPointerType)&ValueCollection_CopyTo_m20305_gshared/* 2615*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m20306_gshared/* 2616*/,
	(methodPointerType)&ValueCollection_get_Count_m20307_gshared/* 2617*/,
	(methodPointerType)&Enumerator__ctor_m20308_gshared/* 2618*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20309_gshared/* 2619*/,
	(methodPointerType)&Enumerator_Dispose_m20310_gshared/* 2620*/,
	(methodPointerType)&Enumerator_MoveNext_m20311_gshared/* 2621*/,
	(methodPointerType)&Enumerator_get_Current_m20312_gshared/* 2622*/,
	(methodPointerType)&Transform_1__ctor_m20313_gshared/* 2623*/,
	(methodPointerType)&Transform_1_Invoke_m20314_gshared/* 2624*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20315_gshared/* 2625*/,
	(methodPointerType)&Transform_1_EndInvoke_m20316_gshared/* 2626*/,
	(methodPointerType)&Transform_1__ctor_m20317_gshared/* 2627*/,
	(methodPointerType)&Transform_1_Invoke_m20318_gshared/* 2628*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20319_gshared/* 2629*/,
	(methodPointerType)&Transform_1_EndInvoke_m20320_gshared/* 2630*/,
	(methodPointerType)&Transform_1__ctor_m20321_gshared/* 2631*/,
	(methodPointerType)&Transform_1_Invoke_m20322_gshared/* 2632*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20323_gshared/* 2633*/,
	(methodPointerType)&Transform_1_EndInvoke_m20324_gshared/* 2634*/,
	(methodPointerType)&ShimEnumerator__ctor_m20325_gshared/* 2635*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m20326_gshared/* 2636*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m20327_gshared/* 2637*/,
	(methodPointerType)&ShimEnumerator_get_Key_m20328_gshared/* 2638*/,
	(methodPointerType)&ShimEnumerator_get_Value_m20329_gshared/* 2639*/,
	(methodPointerType)&ShimEnumerator_get_Current_m20330_gshared/* 2640*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20331_gshared/* 2641*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20332_gshared/* 2642*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20333_gshared/* 2643*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20334_gshared/* 2644*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20335_gshared/* 2645*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m20336_gshared/* 2646*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m20337_gshared/* 2647*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m20338_gshared/* 2648*/,
	(methodPointerType)&DefaultComparer__ctor_m20339_gshared/* 2649*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20340_gshared/* 2650*/,
	(methodPointerType)&DefaultComparer_Equals_m20341_gshared/* 2651*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20392_gshared/* 2652*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20393_gshared/* 2653*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20394_gshared/* 2654*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20395_gshared/* 2655*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20396_gshared/* 2656*/,
	(methodPointerType)&Action_1__ctor_m21229_gshared/* 2657*/,
	(methodPointerType)&Action_1_Invoke_m21230_gshared/* 2658*/,
	(methodPointerType)&Action_1_BeginInvoke_m21231_gshared/* 2659*/,
	(methodPointerType)&Action_1_EndInvoke_m21232_gshared/* 2660*/,
	(methodPointerType)&Dictionary_2__ctor_m21925_gshared/* 2661*/,
	(methodPointerType)&Dictionary_2__ctor_m21926_gshared/* 2662*/,
	(methodPointerType)&Dictionary_2__ctor_m21927_gshared/* 2663*/,
	(methodPointerType)&Dictionary_2__ctor_m21928_gshared/* 2664*/,
	(methodPointerType)&Dictionary_2__ctor_m21929_gshared/* 2665*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21930_gshared/* 2666*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21931_gshared/* 2667*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21932_gshared/* 2668*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m21933_gshared/* 2669*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m21934_gshared/* 2670*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m21935_gshared/* 2671*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m21936_gshared/* 2672*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21937_gshared/* 2673*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21938_gshared/* 2674*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21939_gshared/* 2675*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21940_gshared/* 2676*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21941_gshared/* 2677*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21942_gshared/* 2678*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21943_gshared/* 2679*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m21944_gshared/* 2680*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21945_gshared/* 2681*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21946_gshared/* 2682*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21947_gshared/* 2683*/,
	(methodPointerType)&Dictionary_2_get_Count_m21948_gshared/* 2684*/,
	(methodPointerType)&Dictionary_2_get_Item_m21949_gshared/* 2685*/,
	(methodPointerType)&Dictionary_2_set_Item_m21950_gshared/* 2686*/,
	(methodPointerType)&Dictionary_2_Init_m21951_gshared/* 2687*/,
	(methodPointerType)&Dictionary_2_InitArrays_m21952_gshared/* 2688*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m21953_gshared/* 2689*/,
	(methodPointerType)&Dictionary_2_make_pair_m21954_gshared/* 2690*/,
	(methodPointerType)&Dictionary_2_pick_key_m21955_gshared/* 2691*/,
	(methodPointerType)&Dictionary_2_pick_value_m21956_gshared/* 2692*/,
	(methodPointerType)&Dictionary_2_CopyTo_m21957_gshared/* 2693*/,
	(methodPointerType)&Dictionary_2_Resize_m21958_gshared/* 2694*/,
	(methodPointerType)&Dictionary_2_Add_m21959_gshared/* 2695*/,
	(methodPointerType)&Dictionary_2_Clear_m21960_gshared/* 2696*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m21961_gshared/* 2697*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m21962_gshared/* 2698*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m21963_gshared/* 2699*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m21964_gshared/* 2700*/,
	(methodPointerType)&Dictionary_2_Remove_m21965_gshared/* 2701*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m21966_gshared/* 2702*/,
	(methodPointerType)&Dictionary_2_get_Keys_m21967_gshared/* 2703*/,
	(methodPointerType)&Dictionary_2_get_Values_m21968_gshared/* 2704*/,
	(methodPointerType)&Dictionary_2_ToTKey_m21969_gshared/* 2705*/,
	(methodPointerType)&Dictionary_2_ToTValue_m21970_gshared/* 2706*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m21971_gshared/* 2707*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m21972_gshared/* 2708*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m21973_gshared/* 2709*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21974_gshared/* 2710*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21975_gshared/* 2711*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21976_gshared/* 2712*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21977_gshared/* 2713*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21978_gshared/* 2714*/,
	(methodPointerType)&KeyValuePair_2__ctor_m21979_gshared/* 2715*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m21980_gshared/* 2716*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m21981_gshared/* 2717*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m21982_gshared/* 2718*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m21983_gshared/* 2719*/,
	(methodPointerType)&KeyValuePair_2_ToString_m21984_gshared/* 2720*/,
	(methodPointerType)&KeyCollection__ctor_m21985_gshared/* 2721*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21986_gshared/* 2722*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21987_gshared/* 2723*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21988_gshared/* 2724*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21989_gshared/* 2725*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21990_gshared/* 2726*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m21991_gshared/* 2727*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21992_gshared/* 2728*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21993_gshared/* 2729*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21994_gshared/* 2730*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m21995_gshared/* 2731*/,
	(methodPointerType)&KeyCollection_CopyTo_m21996_gshared/* 2732*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m21997_gshared/* 2733*/,
	(methodPointerType)&KeyCollection_get_Count_m21998_gshared/* 2734*/,
	(methodPointerType)&Enumerator__ctor_m21999_gshared/* 2735*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22000_gshared/* 2736*/,
	(methodPointerType)&Enumerator_Dispose_m22001_gshared/* 2737*/,
	(methodPointerType)&Enumerator_MoveNext_m22002_gshared/* 2738*/,
	(methodPointerType)&Enumerator_get_Current_m22003_gshared/* 2739*/,
	(methodPointerType)&Enumerator__ctor_m22004_gshared/* 2740*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22005_gshared/* 2741*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22006_gshared/* 2742*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22007_gshared/* 2743*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22008_gshared/* 2744*/,
	(methodPointerType)&Enumerator_MoveNext_m22009_gshared/* 2745*/,
	(methodPointerType)&Enumerator_get_Current_m22010_gshared/* 2746*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22011_gshared/* 2747*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22012_gshared/* 2748*/,
	(methodPointerType)&Enumerator_VerifyState_m22013_gshared/* 2749*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22014_gshared/* 2750*/,
	(methodPointerType)&Enumerator_Dispose_m22015_gshared/* 2751*/,
	(methodPointerType)&Transform_1__ctor_m22016_gshared/* 2752*/,
	(methodPointerType)&Transform_1_Invoke_m22017_gshared/* 2753*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22018_gshared/* 2754*/,
	(methodPointerType)&Transform_1_EndInvoke_m22019_gshared/* 2755*/,
	(methodPointerType)&ValueCollection__ctor_m22020_gshared/* 2756*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22021_gshared/* 2757*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22022_gshared/* 2758*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22023_gshared/* 2759*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22024_gshared/* 2760*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22025_gshared/* 2761*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22026_gshared/* 2762*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22027_gshared/* 2763*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22028_gshared/* 2764*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22029_gshared/* 2765*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22030_gshared/* 2766*/,
	(methodPointerType)&ValueCollection_CopyTo_m22031_gshared/* 2767*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22032_gshared/* 2768*/,
	(methodPointerType)&ValueCollection_get_Count_m22033_gshared/* 2769*/,
	(methodPointerType)&Enumerator__ctor_m22034_gshared/* 2770*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22035_gshared/* 2771*/,
	(methodPointerType)&Enumerator_Dispose_m22036_gshared/* 2772*/,
	(methodPointerType)&Enumerator_MoveNext_m22037_gshared/* 2773*/,
	(methodPointerType)&Enumerator_get_Current_m22038_gshared/* 2774*/,
	(methodPointerType)&Transform_1__ctor_m22039_gshared/* 2775*/,
	(methodPointerType)&Transform_1_Invoke_m22040_gshared/* 2776*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22041_gshared/* 2777*/,
	(methodPointerType)&Transform_1_EndInvoke_m22042_gshared/* 2778*/,
	(methodPointerType)&Transform_1__ctor_m22043_gshared/* 2779*/,
	(methodPointerType)&Transform_1_Invoke_m22044_gshared/* 2780*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22045_gshared/* 2781*/,
	(methodPointerType)&Transform_1_EndInvoke_m22046_gshared/* 2782*/,
	(methodPointerType)&Transform_1__ctor_m22047_gshared/* 2783*/,
	(methodPointerType)&Transform_1_Invoke_m22048_gshared/* 2784*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22049_gshared/* 2785*/,
	(methodPointerType)&Transform_1_EndInvoke_m22050_gshared/* 2786*/,
	(methodPointerType)&ShimEnumerator__ctor_m22051_gshared/* 2787*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22052_gshared/* 2788*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22053_gshared/* 2789*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22054_gshared/* 2790*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22055_gshared/* 2791*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22056_gshared/* 2792*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22057_gshared/* 2793*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22058_gshared/* 2794*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22059_gshared/* 2795*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22060_gshared/* 2796*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22061_gshared/* 2797*/,
	(methodPointerType)&DefaultComparer__ctor_m22062_gshared/* 2798*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22063_gshared/* 2799*/,
	(methodPointerType)&DefaultComparer_Equals_m22064_gshared/* 2800*/,
	(methodPointerType)&Dictionary_2__ctor_m22065_gshared/* 2801*/,
	(methodPointerType)&Dictionary_2__ctor_m22066_gshared/* 2802*/,
	(methodPointerType)&Dictionary_2__ctor_m22067_gshared/* 2803*/,
	(methodPointerType)&Dictionary_2__ctor_m22068_gshared/* 2804*/,
	(methodPointerType)&Dictionary_2__ctor_m22069_gshared/* 2805*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22070_gshared/* 2806*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22071_gshared/* 2807*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m22072_gshared/* 2808*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22073_gshared/* 2809*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22074_gshared/* 2810*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m22075_gshared/* 2811*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22076_gshared/* 2812*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22077_gshared/* 2813*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22078_gshared/* 2814*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22079_gshared/* 2815*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22080_gshared/* 2816*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22081_gshared/* 2817*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22082_gshared/* 2818*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22083_gshared/* 2819*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22084_gshared/* 2820*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22085_gshared/* 2821*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22086_gshared/* 2822*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22087_gshared/* 2823*/,
	(methodPointerType)&Dictionary_2_get_Count_m22088_gshared/* 2824*/,
	(methodPointerType)&Dictionary_2_get_Item_m22089_gshared/* 2825*/,
	(methodPointerType)&Dictionary_2_set_Item_m22090_gshared/* 2826*/,
	(methodPointerType)&Dictionary_2_Init_m22091_gshared/* 2827*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22092_gshared/* 2828*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22093_gshared/* 2829*/,
	(methodPointerType)&Dictionary_2_make_pair_m22094_gshared/* 2830*/,
	(methodPointerType)&Dictionary_2_pick_key_m22095_gshared/* 2831*/,
	(methodPointerType)&Dictionary_2_pick_value_m22096_gshared/* 2832*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22097_gshared/* 2833*/,
	(methodPointerType)&Dictionary_2_Resize_m22098_gshared/* 2834*/,
	(methodPointerType)&Dictionary_2_Add_m22099_gshared/* 2835*/,
	(methodPointerType)&Dictionary_2_Clear_m22100_gshared/* 2836*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22101_gshared/* 2837*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22102_gshared/* 2838*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22103_gshared/* 2839*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22104_gshared/* 2840*/,
	(methodPointerType)&Dictionary_2_Remove_m22105_gshared/* 2841*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22106_gshared/* 2842*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22107_gshared/* 2843*/,
	(methodPointerType)&Dictionary_2_get_Values_m22108_gshared/* 2844*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22109_gshared/* 2845*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22110_gshared/* 2846*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22111_gshared/* 2847*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22112_gshared/* 2848*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22113_gshared/* 2849*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22114_gshared/* 2850*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22115_gshared/* 2851*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22116_gshared/* 2852*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22117_gshared/* 2853*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22118_gshared/* 2854*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22119_gshared/* 2855*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22120_gshared/* 2856*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22121_gshared/* 2857*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22122_gshared/* 2858*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22123_gshared/* 2859*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22124_gshared/* 2860*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22125_gshared/* 2861*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22126_gshared/* 2862*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22127_gshared/* 2863*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22128_gshared/* 2864*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22129_gshared/* 2865*/,
	(methodPointerType)&KeyCollection__ctor_m22130_gshared/* 2866*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22131_gshared/* 2867*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22132_gshared/* 2868*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22133_gshared/* 2869*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22134_gshared/* 2870*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22135_gshared/* 2871*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22136_gshared/* 2872*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22137_gshared/* 2873*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22138_gshared/* 2874*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22139_gshared/* 2875*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22140_gshared/* 2876*/,
	(methodPointerType)&KeyCollection_CopyTo_m22141_gshared/* 2877*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22142_gshared/* 2878*/,
	(methodPointerType)&KeyCollection_get_Count_m22143_gshared/* 2879*/,
	(methodPointerType)&Enumerator__ctor_m22144_gshared/* 2880*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22145_gshared/* 2881*/,
	(methodPointerType)&Enumerator_Dispose_m22146_gshared/* 2882*/,
	(methodPointerType)&Enumerator_MoveNext_m22147_gshared/* 2883*/,
	(methodPointerType)&Enumerator_get_Current_m22148_gshared/* 2884*/,
	(methodPointerType)&Enumerator__ctor_m22149_gshared/* 2885*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22150_gshared/* 2886*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22151_gshared/* 2887*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22152_gshared/* 2888*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22153_gshared/* 2889*/,
	(methodPointerType)&Enumerator_MoveNext_m22154_gshared/* 2890*/,
	(methodPointerType)&Enumerator_get_Current_m22155_gshared/* 2891*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22156_gshared/* 2892*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22157_gshared/* 2893*/,
	(methodPointerType)&Enumerator_VerifyState_m22158_gshared/* 2894*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22159_gshared/* 2895*/,
	(methodPointerType)&Enumerator_Dispose_m22160_gshared/* 2896*/,
	(methodPointerType)&Transform_1__ctor_m22161_gshared/* 2897*/,
	(methodPointerType)&Transform_1_Invoke_m22162_gshared/* 2898*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22163_gshared/* 2899*/,
	(methodPointerType)&Transform_1_EndInvoke_m22164_gshared/* 2900*/,
	(methodPointerType)&ValueCollection__ctor_m22165_gshared/* 2901*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22166_gshared/* 2902*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22167_gshared/* 2903*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22168_gshared/* 2904*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22169_gshared/* 2905*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22170_gshared/* 2906*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22171_gshared/* 2907*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22172_gshared/* 2908*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22173_gshared/* 2909*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22174_gshared/* 2910*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22175_gshared/* 2911*/,
	(methodPointerType)&ValueCollection_CopyTo_m22176_gshared/* 2912*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22177_gshared/* 2913*/,
	(methodPointerType)&ValueCollection_get_Count_m22178_gshared/* 2914*/,
	(methodPointerType)&Enumerator__ctor_m22179_gshared/* 2915*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22180_gshared/* 2916*/,
	(methodPointerType)&Enumerator_Dispose_m22181_gshared/* 2917*/,
	(methodPointerType)&Enumerator_MoveNext_m22182_gshared/* 2918*/,
	(methodPointerType)&Enumerator_get_Current_m22183_gshared/* 2919*/,
	(methodPointerType)&Transform_1__ctor_m22184_gshared/* 2920*/,
	(methodPointerType)&Transform_1_Invoke_m22185_gshared/* 2921*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22186_gshared/* 2922*/,
	(methodPointerType)&Transform_1_EndInvoke_m22187_gshared/* 2923*/,
	(methodPointerType)&Transform_1__ctor_m22188_gshared/* 2924*/,
	(methodPointerType)&Transform_1_Invoke_m22189_gshared/* 2925*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22190_gshared/* 2926*/,
	(methodPointerType)&Transform_1_EndInvoke_m22191_gshared/* 2927*/,
	(methodPointerType)&Transform_1__ctor_m22192_gshared/* 2928*/,
	(methodPointerType)&Transform_1_Invoke_m22193_gshared/* 2929*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22194_gshared/* 2930*/,
	(methodPointerType)&Transform_1_EndInvoke_m22195_gshared/* 2931*/,
	(methodPointerType)&ShimEnumerator__ctor_m22196_gshared/* 2932*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22197_gshared/* 2933*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22198_gshared/* 2934*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22199_gshared/* 2935*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22200_gshared/* 2936*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22201_gshared/* 2937*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22202_gshared/* 2938*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22203_gshared/* 2939*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22204_gshared/* 2940*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22205_gshared/* 2941*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22206_gshared/* 2942*/,
	(methodPointerType)&DefaultComparer__ctor_m22207_gshared/* 2943*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22208_gshared/* 2944*/,
	(methodPointerType)&DefaultComparer_Equals_m22209_gshared/* 2945*/,
	(methodPointerType)&List_1__ctor_m22302_gshared/* 2946*/,
	(methodPointerType)&List_1__ctor_m22303_gshared/* 2947*/,
	(methodPointerType)&List_1__cctor_m22304_gshared/* 2948*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22305_gshared/* 2949*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m22306_gshared/* 2950*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m22307_gshared/* 2951*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m22308_gshared/* 2952*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m22309_gshared/* 2953*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m22310_gshared/* 2954*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m22311_gshared/* 2955*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m22312_gshared/* 2956*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22313_gshared/* 2957*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m22314_gshared/* 2958*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m22315_gshared/* 2959*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m22316_gshared/* 2960*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m22317_gshared/* 2961*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m22318_gshared/* 2962*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m22319_gshared/* 2963*/,
	(methodPointerType)&List_1_Add_m22320_gshared/* 2964*/,
	(methodPointerType)&List_1_GrowIfNeeded_m22321_gshared/* 2965*/,
	(methodPointerType)&List_1_AddCollection_m22322_gshared/* 2966*/,
	(methodPointerType)&List_1_AddEnumerable_m22323_gshared/* 2967*/,
	(methodPointerType)&List_1_AddRange_m22324_gshared/* 2968*/,
	(methodPointerType)&List_1_AsReadOnly_m22325_gshared/* 2969*/,
	(methodPointerType)&List_1_Clear_m22326_gshared/* 2970*/,
	(methodPointerType)&List_1_Contains_m22327_gshared/* 2971*/,
	(methodPointerType)&List_1_CopyTo_m22328_gshared/* 2972*/,
	(methodPointerType)&List_1_Find_m22329_gshared/* 2973*/,
	(methodPointerType)&List_1_CheckMatch_m22330_gshared/* 2974*/,
	(methodPointerType)&List_1_GetIndex_m22331_gshared/* 2975*/,
	(methodPointerType)&List_1_GetEnumerator_m22332_gshared/* 2976*/,
	(methodPointerType)&List_1_IndexOf_m22333_gshared/* 2977*/,
	(methodPointerType)&List_1_Shift_m22334_gshared/* 2978*/,
	(methodPointerType)&List_1_CheckIndex_m22335_gshared/* 2979*/,
	(methodPointerType)&List_1_Insert_m22336_gshared/* 2980*/,
	(methodPointerType)&List_1_CheckCollection_m22337_gshared/* 2981*/,
	(methodPointerType)&List_1_Remove_m22338_gshared/* 2982*/,
	(methodPointerType)&List_1_RemoveAll_m22339_gshared/* 2983*/,
	(methodPointerType)&List_1_RemoveAt_m22340_gshared/* 2984*/,
	(methodPointerType)&List_1_Reverse_m22341_gshared/* 2985*/,
	(methodPointerType)&List_1_Sort_m22342_gshared/* 2986*/,
	(methodPointerType)&List_1_Sort_m22343_gshared/* 2987*/,
	(methodPointerType)&List_1_ToArray_m22344_gshared/* 2988*/,
	(methodPointerType)&List_1_TrimExcess_m22345_gshared/* 2989*/,
	(methodPointerType)&List_1_get_Capacity_m22346_gshared/* 2990*/,
	(methodPointerType)&List_1_set_Capacity_m22347_gshared/* 2991*/,
	(methodPointerType)&List_1_get_Count_m22348_gshared/* 2992*/,
	(methodPointerType)&List_1_get_Item_m22349_gshared/* 2993*/,
	(methodPointerType)&List_1_set_Item_m22350_gshared/* 2994*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22351_gshared/* 2995*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22352_gshared/* 2996*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22353_gshared/* 2997*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22354_gshared/* 2998*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22355_gshared/* 2999*/,
	(methodPointerType)&Enumerator__ctor_m22356_gshared/* 3000*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22357_gshared/* 3001*/,
	(methodPointerType)&Enumerator_Dispose_m22358_gshared/* 3002*/,
	(methodPointerType)&Enumerator_VerifyState_m22359_gshared/* 3003*/,
	(methodPointerType)&Enumerator_MoveNext_m22360_gshared/* 3004*/,
	(methodPointerType)&Enumerator_get_Current_m22361_gshared/* 3005*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m22362_gshared/* 3006*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22363_gshared/* 3007*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22364_gshared/* 3008*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22365_gshared/* 3009*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22366_gshared/* 3010*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22367_gshared/* 3011*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22368_gshared/* 3012*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22369_gshared/* 3013*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22370_gshared/* 3014*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22371_gshared/* 3015*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22372_gshared/* 3016*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m22373_gshared/* 3017*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m22374_gshared/* 3018*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m22375_gshared/* 3019*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22376_gshared/* 3020*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m22377_gshared/* 3021*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m22378_gshared/* 3022*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22379_gshared/* 3023*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22380_gshared/* 3024*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22381_gshared/* 3025*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22382_gshared/* 3026*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22383_gshared/* 3027*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m22384_gshared/* 3028*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m22385_gshared/* 3029*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m22386_gshared/* 3030*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m22387_gshared/* 3031*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m22388_gshared/* 3032*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m22389_gshared/* 3033*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m22390_gshared/* 3034*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m22391_gshared/* 3035*/,
	(methodPointerType)&Collection_1__ctor_m22392_gshared/* 3036*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22393_gshared/* 3037*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m22394_gshared/* 3038*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m22395_gshared/* 3039*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m22396_gshared/* 3040*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m22397_gshared/* 3041*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m22398_gshared/* 3042*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m22399_gshared/* 3043*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m22400_gshared/* 3044*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m22401_gshared/* 3045*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m22402_gshared/* 3046*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m22403_gshared/* 3047*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m22404_gshared/* 3048*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m22405_gshared/* 3049*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m22406_gshared/* 3050*/,
	(methodPointerType)&Collection_1_Add_m22407_gshared/* 3051*/,
	(methodPointerType)&Collection_1_Clear_m22408_gshared/* 3052*/,
	(methodPointerType)&Collection_1_ClearItems_m22409_gshared/* 3053*/,
	(methodPointerType)&Collection_1_Contains_m22410_gshared/* 3054*/,
	(methodPointerType)&Collection_1_CopyTo_m22411_gshared/* 3055*/,
	(methodPointerType)&Collection_1_GetEnumerator_m22412_gshared/* 3056*/,
	(methodPointerType)&Collection_1_IndexOf_m22413_gshared/* 3057*/,
	(methodPointerType)&Collection_1_Insert_m22414_gshared/* 3058*/,
	(methodPointerType)&Collection_1_InsertItem_m22415_gshared/* 3059*/,
	(methodPointerType)&Collection_1_Remove_m22416_gshared/* 3060*/,
	(methodPointerType)&Collection_1_RemoveAt_m22417_gshared/* 3061*/,
	(methodPointerType)&Collection_1_RemoveItem_m22418_gshared/* 3062*/,
	(methodPointerType)&Collection_1_get_Count_m22419_gshared/* 3063*/,
	(methodPointerType)&Collection_1_get_Item_m22420_gshared/* 3064*/,
	(methodPointerType)&Collection_1_set_Item_m22421_gshared/* 3065*/,
	(methodPointerType)&Collection_1_SetItem_m22422_gshared/* 3066*/,
	(methodPointerType)&Collection_1_IsValidItem_m22423_gshared/* 3067*/,
	(methodPointerType)&Collection_1_ConvertItem_m22424_gshared/* 3068*/,
	(methodPointerType)&Collection_1_CheckWritable_m22425_gshared/* 3069*/,
	(methodPointerType)&Collection_1_IsSynchronized_m22426_gshared/* 3070*/,
	(methodPointerType)&Collection_1_IsFixedSize_m22427_gshared/* 3071*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22428_gshared/* 3072*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22429_gshared/* 3073*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22430_gshared/* 3074*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22431_gshared/* 3075*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22432_gshared/* 3076*/,
	(methodPointerType)&DefaultComparer__ctor_m22433_gshared/* 3077*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22434_gshared/* 3078*/,
	(methodPointerType)&DefaultComparer_Equals_m22435_gshared/* 3079*/,
	(methodPointerType)&Predicate_1__ctor_m22436_gshared/* 3080*/,
	(methodPointerType)&Predicate_1_Invoke_m22437_gshared/* 3081*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m22438_gshared/* 3082*/,
	(methodPointerType)&Predicate_1_EndInvoke_m22439_gshared/* 3083*/,
	(methodPointerType)&Comparer_1__ctor_m22440_gshared/* 3084*/,
	(methodPointerType)&Comparer_1__cctor_m22441_gshared/* 3085*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22442_gshared/* 3086*/,
	(methodPointerType)&Comparer_1_get_Default_m22443_gshared/* 3087*/,
	(methodPointerType)&DefaultComparer__ctor_m22444_gshared/* 3088*/,
	(methodPointerType)&DefaultComparer_Compare_m22445_gshared/* 3089*/,
	(methodPointerType)&Comparison_1__ctor_m22446_gshared/* 3090*/,
	(methodPointerType)&Comparison_1_Invoke_m22447_gshared/* 3091*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m22448_gshared/* 3092*/,
	(methodPointerType)&Comparison_1_EndInvoke_m22449_gshared/* 3093*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22550_gshared/* 3094*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22551_gshared/* 3095*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22552_gshared/* 3096*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22553_gshared/* 3097*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22554_gshared/* 3098*/,
	(methodPointerType)&Dictionary_2__ctor_m22556_gshared/* 3099*/,
	(methodPointerType)&Dictionary_2__ctor_m22558_gshared/* 3100*/,
	(methodPointerType)&Dictionary_2__ctor_m22560_gshared/* 3101*/,
	(methodPointerType)&Dictionary_2__ctor_m22562_gshared/* 3102*/,
	(methodPointerType)&Dictionary_2__ctor_m22564_gshared/* 3103*/,
	(methodPointerType)&Dictionary_2__ctor_m22566_gshared/* 3104*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568_gshared/* 3105*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570_gshared/* 3106*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m22572_gshared/* 3107*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22574_gshared/* 3108*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22576_gshared/* 3109*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m22578_gshared/* 3110*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22580_gshared/* 3111*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582_gshared/* 3112*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584_gshared/* 3113*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586_gshared/* 3114*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588_gshared/* 3115*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590_gshared/* 3116*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592_gshared/* 3117*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594_gshared/* 3118*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22596_gshared/* 3119*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598_gshared/* 3120*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600_gshared/* 3121*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602_gshared/* 3122*/,
	(methodPointerType)&Dictionary_2_get_Count_m22604_gshared/* 3123*/,
	(methodPointerType)&Dictionary_2_get_Item_m22606_gshared/* 3124*/,
	(methodPointerType)&Dictionary_2_set_Item_m22608_gshared/* 3125*/,
	(methodPointerType)&Dictionary_2_Init_m22610_gshared/* 3126*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22612_gshared/* 3127*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22614_gshared/* 3128*/,
	(methodPointerType)&Dictionary_2_make_pair_m22616_gshared/* 3129*/,
	(methodPointerType)&Dictionary_2_pick_key_m22618_gshared/* 3130*/,
	(methodPointerType)&Dictionary_2_pick_value_m22620_gshared/* 3131*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22622_gshared/* 3132*/,
	(methodPointerType)&Dictionary_2_Resize_m22624_gshared/* 3133*/,
	(methodPointerType)&Dictionary_2_Add_m22626_gshared/* 3134*/,
	(methodPointerType)&Dictionary_2_Clear_m22628_gshared/* 3135*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22630_gshared/* 3136*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22632_gshared/* 3137*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22634_gshared/* 3138*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22636_gshared/* 3139*/,
	(methodPointerType)&Dictionary_2_Remove_m22638_gshared/* 3140*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22640_gshared/* 3141*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22642_gshared/* 3142*/,
	(methodPointerType)&Dictionary_2_get_Values_m22644_gshared/* 3143*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22646_gshared/* 3144*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22648_gshared/* 3145*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22650_gshared/* 3146*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22652_gshared/* 3147*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22654_gshared/* 3148*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22655_gshared/* 3149*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22656_gshared/* 3150*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22657_gshared/* 3151*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22658_gshared/* 3152*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22659_gshared/* 3153*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22660_gshared/* 3154*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22661_gshared/* 3155*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22662_gshared/* 3156*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22663_gshared/* 3157*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22664_gshared/* 3158*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22665_gshared/* 3159*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22666_gshared/* 3160*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22667_gshared/* 3161*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22668_gshared/* 3162*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22669_gshared/* 3163*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22670_gshared/* 3164*/,
	(methodPointerType)&KeyCollection__ctor_m22671_gshared/* 3165*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22672_gshared/* 3166*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22673_gshared/* 3167*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22674_gshared/* 3168*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22675_gshared/* 3169*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22676_gshared/* 3170*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22677_gshared/* 3171*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22678_gshared/* 3172*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22679_gshared/* 3173*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22680_gshared/* 3174*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22681_gshared/* 3175*/,
	(methodPointerType)&KeyCollection_CopyTo_m22682_gshared/* 3176*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22683_gshared/* 3177*/,
	(methodPointerType)&KeyCollection_get_Count_m22684_gshared/* 3178*/,
	(methodPointerType)&Enumerator__ctor_m22685_gshared/* 3179*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22686_gshared/* 3180*/,
	(methodPointerType)&Enumerator_Dispose_m22687_gshared/* 3181*/,
	(methodPointerType)&Enumerator_MoveNext_m22688_gshared/* 3182*/,
	(methodPointerType)&Enumerator_get_Current_m22689_gshared/* 3183*/,
	(methodPointerType)&Enumerator__ctor_m22690_gshared/* 3184*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22691_gshared/* 3185*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692_gshared/* 3186*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693_gshared/* 3187*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694_gshared/* 3188*/,
	(methodPointerType)&Enumerator_MoveNext_m22695_gshared/* 3189*/,
	(methodPointerType)&Enumerator_get_Current_m22696_gshared/* 3190*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22697_gshared/* 3191*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22698_gshared/* 3192*/,
	(methodPointerType)&Enumerator_VerifyState_m22699_gshared/* 3193*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22700_gshared/* 3194*/,
	(methodPointerType)&Enumerator_Dispose_m22701_gshared/* 3195*/,
	(methodPointerType)&Transform_1__ctor_m22702_gshared/* 3196*/,
	(methodPointerType)&Transform_1_Invoke_m22703_gshared/* 3197*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22704_gshared/* 3198*/,
	(methodPointerType)&Transform_1_EndInvoke_m22705_gshared/* 3199*/,
	(methodPointerType)&ValueCollection__ctor_m22706_gshared/* 3200*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22707_gshared/* 3201*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22708_gshared/* 3202*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22709_gshared/* 3203*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22710_gshared/* 3204*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22711_gshared/* 3205*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22712_gshared/* 3206*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22713_gshared/* 3207*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22714_gshared/* 3208*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22715_gshared/* 3209*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22716_gshared/* 3210*/,
	(methodPointerType)&ValueCollection_CopyTo_m22717_gshared/* 3211*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22718_gshared/* 3212*/,
	(methodPointerType)&ValueCollection_get_Count_m22719_gshared/* 3213*/,
	(methodPointerType)&Enumerator__ctor_m22720_gshared/* 3214*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22721_gshared/* 3215*/,
	(methodPointerType)&Enumerator_Dispose_m22722_gshared/* 3216*/,
	(methodPointerType)&Enumerator_MoveNext_m22723_gshared/* 3217*/,
	(methodPointerType)&Enumerator_get_Current_m22724_gshared/* 3218*/,
	(methodPointerType)&Transform_1__ctor_m22725_gshared/* 3219*/,
	(methodPointerType)&Transform_1_Invoke_m22726_gshared/* 3220*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22727_gshared/* 3221*/,
	(methodPointerType)&Transform_1_EndInvoke_m22728_gshared/* 3222*/,
	(methodPointerType)&Transform_1__ctor_m22729_gshared/* 3223*/,
	(methodPointerType)&Transform_1_Invoke_m22730_gshared/* 3224*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22731_gshared/* 3225*/,
	(methodPointerType)&Transform_1_EndInvoke_m22732_gshared/* 3226*/,
	(methodPointerType)&Transform_1__ctor_m22733_gshared/* 3227*/,
	(methodPointerType)&Transform_1_Invoke_m22734_gshared/* 3228*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22735_gshared/* 3229*/,
	(methodPointerType)&Transform_1_EndInvoke_m22736_gshared/* 3230*/,
	(methodPointerType)&ShimEnumerator__ctor_m22737_gshared/* 3231*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22738_gshared/* 3232*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22739_gshared/* 3233*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22740_gshared/* 3234*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22741_gshared/* 3235*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22742_gshared/* 3236*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22743_gshared/* 3237*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22744_gshared/* 3238*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22745_gshared/* 3239*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22746_gshared/* 3240*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22747_gshared/* 3241*/,
	(methodPointerType)&DefaultComparer__ctor_m22748_gshared/* 3242*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22749_gshared/* 3243*/,
	(methodPointerType)&DefaultComparer_Equals_m22750_gshared/* 3244*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23312_gshared/* 3245*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23313_gshared/* 3246*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23314_gshared/* 3247*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23315_gshared/* 3248*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23316_gshared/* 3249*/,
	(methodPointerType)&TweenerCore_3__ctor_m23421_gshared/* 3250*/,
	(methodPointerType)&TweenerCore_3_Reset_m23422_gshared/* 3251*/,
	(methodPointerType)&TweenerCore_3_Validate_m23423_gshared/* 3252*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23424_gshared/* 3253*/,
	(methodPointerType)&TweenerCore_3_Startup_m23425_gshared/* 3254*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23426_gshared/* 3255*/,
	(methodPointerType)&DOGetter_1__ctor_m23427_gshared/* 3256*/,
	(methodPointerType)&DOGetter_1_Invoke_m23428_gshared/* 3257*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23429_gshared/* 3258*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23430_gshared/* 3259*/,
	(methodPointerType)&DOSetter_1__ctor_m23431_gshared/* 3260*/,
	(methodPointerType)&DOSetter_1_Invoke_m23432_gshared/* 3261*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23433_gshared/* 3262*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23434_gshared/* 3263*/,
	(methodPointerType)&TweenerCore_3__ctor_m23435_gshared/* 3264*/,
	(methodPointerType)&TweenerCore_3_Reset_m23436_gshared/* 3265*/,
	(methodPointerType)&TweenerCore_3_Validate_m23437_gshared/* 3266*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23438_gshared/* 3267*/,
	(methodPointerType)&TweenerCore_3_Startup_m23439_gshared/* 3268*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23440_gshared/* 3269*/,
	(methodPointerType)&DOGetter_1__ctor_m23441_gshared/* 3270*/,
	(methodPointerType)&DOGetter_1_Invoke_m23442_gshared/* 3271*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23443_gshared/* 3272*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23444_gshared/* 3273*/,
	(methodPointerType)&DOSetter_1__ctor_m23445_gshared/* 3274*/,
	(methodPointerType)&DOSetter_1_Invoke_m23446_gshared/* 3275*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23447_gshared/* 3276*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23448_gshared/* 3277*/,
	(methodPointerType)&TweenCallback_1__ctor_m23542_gshared/* 3278*/,
	(methodPointerType)&TweenCallback_1_Invoke_m23543_gshared/* 3279*/,
	(methodPointerType)&TweenCallback_1_BeginInvoke_m23544_gshared/* 3280*/,
	(methodPointerType)&TweenCallback_1_EndInvoke_m23545_gshared/* 3281*/,
	(methodPointerType)&TweenerCore_3__ctor_m23740_gshared/* 3282*/,
	(methodPointerType)&TweenerCore_3_Reset_m23741_gshared/* 3283*/,
	(methodPointerType)&TweenerCore_3_Validate_m23742_gshared/* 3284*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23743_gshared/* 3285*/,
	(methodPointerType)&TweenerCore_3_Startup_m23744_gshared/* 3286*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23745_gshared/* 3287*/,
	(methodPointerType)&TweenerCore_3__ctor_m23760_gshared/* 3288*/,
	(methodPointerType)&TweenerCore_3_Reset_m23761_gshared/* 3289*/,
	(methodPointerType)&TweenerCore_3_Validate_m23762_gshared/* 3290*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23763_gshared/* 3291*/,
	(methodPointerType)&TweenerCore_3_Startup_m23764_gshared/* 3292*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23765_gshared/* 3293*/,
	(methodPointerType)&DOGetter_1__ctor_m23766_gshared/* 3294*/,
	(methodPointerType)&DOGetter_1_Invoke_m23767_gshared/* 3295*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23768_gshared/* 3296*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23769_gshared/* 3297*/,
	(methodPointerType)&DOSetter_1__ctor_m23770_gshared/* 3298*/,
	(methodPointerType)&DOSetter_1_Invoke_m23771_gshared/* 3299*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23772_gshared/* 3300*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23773_gshared/* 3301*/,
	(methodPointerType)&TweenerCore_3__ctor_m23787_gshared/* 3302*/,
	(methodPointerType)&TweenerCore_3_Reset_m23788_gshared/* 3303*/,
	(methodPointerType)&TweenerCore_3_Validate_m23789_gshared/* 3304*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23790_gshared/* 3305*/,
	(methodPointerType)&TweenerCore_3_Startup_m23791_gshared/* 3306*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23792_gshared/* 3307*/,
	(methodPointerType)&DOGetter_1__ctor_m23793_gshared/* 3308*/,
	(methodPointerType)&DOGetter_1_Invoke_m23794_gshared/* 3309*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23795_gshared/* 3310*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23796_gshared/* 3311*/,
	(methodPointerType)&DOSetter_1__ctor_m23797_gshared/* 3312*/,
	(methodPointerType)&DOSetter_1_Invoke_m23798_gshared/* 3313*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23799_gshared/* 3314*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23800_gshared/* 3315*/,
	(methodPointerType)&TweenerCore_3__ctor_m23801_gshared/* 3316*/,
	(methodPointerType)&TweenerCore_3_Reset_m23802_gshared/* 3317*/,
	(methodPointerType)&TweenerCore_3_Validate_m23803_gshared/* 3318*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23804_gshared/* 3319*/,
	(methodPointerType)&TweenerCore_3_Startup_m23805_gshared/* 3320*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23806_gshared/* 3321*/,
	(methodPointerType)&DOGetter_1__ctor_m23807_gshared/* 3322*/,
	(methodPointerType)&DOGetter_1_Invoke_m23808_gshared/* 3323*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23809_gshared/* 3324*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23810_gshared/* 3325*/,
	(methodPointerType)&DOSetter_1__ctor_m23811_gshared/* 3326*/,
	(methodPointerType)&DOSetter_1_Invoke_m23812_gshared/* 3327*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23813_gshared/* 3328*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23814_gshared/* 3329*/,
	(methodPointerType)&TweenerCore_3__ctor_m23815_gshared/* 3330*/,
	(methodPointerType)&TweenerCore_3_Reset_m23816_gshared/* 3331*/,
	(methodPointerType)&TweenerCore_3_Validate_m23817_gshared/* 3332*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23818_gshared/* 3333*/,
	(methodPointerType)&TweenerCore_3_Startup_m23819_gshared/* 3334*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23820_gshared/* 3335*/,
	(methodPointerType)&DOGetter_1__ctor_m23821_gshared/* 3336*/,
	(methodPointerType)&DOGetter_1_Invoke_m23822_gshared/* 3337*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23823_gshared/* 3338*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23824_gshared/* 3339*/,
	(methodPointerType)&DOSetter_1__ctor_m23825_gshared/* 3340*/,
	(methodPointerType)&DOSetter_1_Invoke_m23826_gshared/* 3341*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23827_gshared/* 3342*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23828_gshared/* 3343*/,
	(methodPointerType)&TweenerCore_3__ctor_m23830_gshared/* 3344*/,
	(methodPointerType)&TweenerCore_3_Reset_m23831_gshared/* 3345*/,
	(methodPointerType)&TweenerCore_3_Validate_m23832_gshared/* 3346*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23833_gshared/* 3347*/,
	(methodPointerType)&TweenerCore_3_Startup_m23834_gshared/* 3348*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23835_gshared/* 3349*/,
	(methodPointerType)&List_1__ctor_m23852_gshared/* 3350*/,
	(methodPointerType)&List_1__ctor_m23854_gshared/* 3351*/,
	(methodPointerType)&List_1__cctor_m23856_gshared/* 3352*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared/* 3353*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m23860_gshared/* 3354*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared/* 3355*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m23864_gshared/* 3356*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m23866_gshared/* 3357*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m23868_gshared/* 3358*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m23870_gshared/* 3359*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m23872_gshared/* 3360*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared/* 3361*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared/* 3362*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared/* 3363*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared/* 3364*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared/* 3365*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m23884_gshared/* 3366*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m23886_gshared/* 3367*/,
	(methodPointerType)&List_1_Add_m23888_gshared/* 3368*/,
	(methodPointerType)&List_1_GrowIfNeeded_m23890_gshared/* 3369*/,
	(methodPointerType)&List_1_AddCollection_m23892_gshared/* 3370*/,
	(methodPointerType)&List_1_AddEnumerable_m23894_gshared/* 3371*/,
	(methodPointerType)&List_1_AddRange_m23896_gshared/* 3372*/,
	(methodPointerType)&List_1_AsReadOnly_m23898_gshared/* 3373*/,
	(methodPointerType)&List_1_Clear_m23900_gshared/* 3374*/,
	(methodPointerType)&List_1_Contains_m23902_gshared/* 3375*/,
	(methodPointerType)&List_1_CopyTo_m23904_gshared/* 3376*/,
	(methodPointerType)&List_1_Find_m23906_gshared/* 3377*/,
	(methodPointerType)&List_1_CheckMatch_m23908_gshared/* 3378*/,
	(methodPointerType)&List_1_GetIndex_m23910_gshared/* 3379*/,
	(methodPointerType)&List_1_GetEnumerator_m23912_gshared/* 3380*/,
	(methodPointerType)&List_1_IndexOf_m23914_gshared/* 3381*/,
	(methodPointerType)&List_1_Shift_m23916_gshared/* 3382*/,
	(methodPointerType)&List_1_CheckIndex_m23918_gshared/* 3383*/,
	(methodPointerType)&List_1_Insert_m23920_gshared/* 3384*/,
	(methodPointerType)&List_1_CheckCollection_m23922_gshared/* 3385*/,
	(methodPointerType)&List_1_Remove_m23924_gshared/* 3386*/,
	(methodPointerType)&List_1_RemoveAll_m23926_gshared/* 3387*/,
	(methodPointerType)&List_1_RemoveAt_m23928_gshared/* 3388*/,
	(methodPointerType)&List_1_Reverse_m23930_gshared/* 3389*/,
	(methodPointerType)&List_1_Sort_m23932_gshared/* 3390*/,
	(methodPointerType)&List_1_Sort_m23934_gshared/* 3391*/,
	(methodPointerType)&List_1_ToArray_m23936_gshared/* 3392*/,
	(methodPointerType)&List_1_TrimExcess_m23938_gshared/* 3393*/,
	(methodPointerType)&List_1_get_Capacity_m23940_gshared/* 3394*/,
	(methodPointerType)&List_1_set_Capacity_m23942_gshared/* 3395*/,
	(methodPointerType)&List_1_get_Count_m23944_gshared/* 3396*/,
	(methodPointerType)&List_1_get_Item_m23946_gshared/* 3397*/,
	(methodPointerType)&List_1_set_Item_m23948_gshared/* 3398*/,
	(methodPointerType)&Enumerator__ctor_m23949_gshared/* 3399*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared/* 3400*/,
	(methodPointerType)&Enumerator_Dispose_m23951_gshared/* 3401*/,
	(methodPointerType)&Enumerator_VerifyState_m23952_gshared/* 3402*/,
	(methodPointerType)&Enumerator_MoveNext_m23953_gshared/* 3403*/,
	(methodPointerType)&Enumerator_get_Current_m23954_gshared/* 3404*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m23955_gshared/* 3405*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956_gshared/* 3406*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957_gshared/* 3407*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958_gshared/* 3408*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959_gshared/* 3409*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960_gshared/* 3410*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961_gshared/* 3411*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962_gshared/* 3412*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963_gshared/* 3413*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964_gshared/* 3414*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965_gshared/* 3415*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m23966_gshared/* 3416*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m23967_gshared/* 3417*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m23968_gshared/* 3418*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969_gshared/* 3419*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m23970_gshared/* 3420*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m23971_gshared/* 3421*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972_gshared/* 3422*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973_gshared/* 3423*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974_gshared/* 3424*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975_gshared/* 3425*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976_gshared/* 3426*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977_gshared/* 3427*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978_gshared/* 3428*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m23979_gshared/* 3429*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m23980_gshared/* 3430*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m23981_gshared/* 3431*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m23982_gshared/* 3432*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m23983_gshared/* 3433*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m23984_gshared/* 3434*/,
	(methodPointerType)&Collection_1__ctor_m23985_gshared/* 3435*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986_gshared/* 3436*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m23987_gshared/* 3437*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988_gshared/* 3438*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m23989_gshared/* 3439*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m23990_gshared/* 3440*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m23991_gshared/* 3441*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m23992_gshared/* 3442*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m23993_gshared/* 3443*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994_gshared/* 3444*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m23995_gshared/* 3445*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m23996_gshared/* 3446*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m23997_gshared/* 3447*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m23998_gshared/* 3448*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m23999_gshared/* 3449*/,
	(methodPointerType)&Collection_1_Add_m24000_gshared/* 3450*/,
	(methodPointerType)&Collection_1_Clear_m24001_gshared/* 3451*/,
	(methodPointerType)&Collection_1_ClearItems_m24002_gshared/* 3452*/,
	(methodPointerType)&Collection_1_Contains_m24003_gshared/* 3453*/,
	(methodPointerType)&Collection_1_CopyTo_m24004_gshared/* 3454*/,
	(methodPointerType)&Collection_1_GetEnumerator_m24005_gshared/* 3455*/,
	(methodPointerType)&Collection_1_IndexOf_m24006_gshared/* 3456*/,
	(methodPointerType)&Collection_1_Insert_m24007_gshared/* 3457*/,
	(methodPointerType)&Collection_1_InsertItem_m24008_gshared/* 3458*/,
	(methodPointerType)&Collection_1_Remove_m24009_gshared/* 3459*/,
	(methodPointerType)&Collection_1_RemoveAt_m24010_gshared/* 3460*/,
	(methodPointerType)&Collection_1_RemoveItem_m24011_gshared/* 3461*/,
	(methodPointerType)&Collection_1_get_Count_m24012_gshared/* 3462*/,
	(methodPointerType)&Collection_1_get_Item_m24013_gshared/* 3463*/,
	(methodPointerType)&Collection_1_set_Item_m24014_gshared/* 3464*/,
	(methodPointerType)&Collection_1_SetItem_m24015_gshared/* 3465*/,
	(methodPointerType)&Collection_1_IsValidItem_m24016_gshared/* 3466*/,
	(methodPointerType)&Collection_1_ConvertItem_m24017_gshared/* 3467*/,
	(methodPointerType)&Collection_1_CheckWritable_m24018_gshared/* 3468*/,
	(methodPointerType)&Collection_1_IsSynchronized_m24019_gshared/* 3469*/,
	(methodPointerType)&Collection_1_IsFixedSize_m24020_gshared/* 3470*/,
	(methodPointerType)&Predicate_1__ctor_m24021_gshared/* 3471*/,
	(methodPointerType)&Predicate_1_Invoke_m24022_gshared/* 3472*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m24023_gshared/* 3473*/,
	(methodPointerType)&Predicate_1_EndInvoke_m24024_gshared/* 3474*/,
	(methodPointerType)&Comparer_1__ctor_m24025_gshared/* 3475*/,
	(methodPointerType)&Comparer_1__cctor_m24026_gshared/* 3476*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m24027_gshared/* 3477*/,
	(methodPointerType)&Comparer_1_get_Default_m24028_gshared/* 3478*/,
	(methodPointerType)&GenericComparer_1__ctor_m24029_gshared/* 3479*/,
	(methodPointerType)&GenericComparer_1_Compare_m24030_gshared/* 3480*/,
	(methodPointerType)&DefaultComparer__ctor_m24031_gshared/* 3481*/,
	(methodPointerType)&DefaultComparer_Compare_m24032_gshared/* 3482*/,
	(methodPointerType)&Comparison_1__ctor_m24033_gshared/* 3483*/,
	(methodPointerType)&Comparison_1_Invoke_m24034_gshared/* 3484*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m24035_gshared/* 3485*/,
	(methodPointerType)&Comparison_1_EndInvoke_m24036_gshared/* 3486*/,
	(methodPointerType)&TweenerCore_3__ctor_m24081_gshared/* 3487*/,
	(methodPointerType)&TweenerCore_3_Reset_m24082_gshared/* 3488*/,
	(methodPointerType)&TweenerCore_3_Validate_m24083_gshared/* 3489*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m24084_gshared/* 3490*/,
	(methodPointerType)&TweenerCore_3_Startup_m24085_gshared/* 3491*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m24086_gshared/* 3492*/,
	(methodPointerType)&DOGetter_1__ctor_m24087_gshared/* 3493*/,
	(methodPointerType)&DOGetter_1_Invoke_m24088_gshared/* 3494*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m24089_gshared/* 3495*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m24090_gshared/* 3496*/,
	(methodPointerType)&DOSetter_1__ctor_m24091_gshared/* 3497*/,
	(methodPointerType)&DOSetter_1_Invoke_m24092_gshared/* 3498*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m24093_gshared/* 3499*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m24094_gshared/* 3500*/,
	(methodPointerType)&TweenerCore_3__ctor_m24095_gshared/* 3501*/,
	(methodPointerType)&TweenerCore_3_Reset_m24096_gshared/* 3502*/,
	(methodPointerType)&TweenerCore_3_Validate_m24097_gshared/* 3503*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m24098_gshared/* 3504*/,
	(methodPointerType)&TweenerCore_3_Startup_m24099_gshared/* 3505*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m24100_gshared/* 3506*/,
	(methodPointerType)&DOGetter_1__ctor_m24101_gshared/* 3507*/,
	(methodPointerType)&DOGetter_1_Invoke_m24102_gshared/* 3508*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m24103_gshared/* 3509*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m24104_gshared/* 3510*/,
	(methodPointerType)&DOSetter_1__ctor_m24105_gshared/* 3511*/,
	(methodPointerType)&DOSetter_1_Invoke_m24106_gshared/* 3512*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m24107_gshared/* 3513*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m24108_gshared/* 3514*/,
	(methodPointerType)&TweenerCore_3__ctor_m24109_gshared/* 3515*/,
	(methodPointerType)&TweenerCore_3_Reset_m24110_gshared/* 3516*/,
	(methodPointerType)&TweenerCore_3_Validate_m24111_gshared/* 3517*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m24112_gshared/* 3518*/,
	(methodPointerType)&TweenerCore_3_Startup_m24113_gshared/* 3519*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m24114_gshared/* 3520*/,
	(methodPointerType)&DOGetter_1__ctor_m24115_gshared/* 3521*/,
	(methodPointerType)&DOGetter_1_Invoke_m24116_gshared/* 3522*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m24117_gshared/* 3523*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m24118_gshared/* 3524*/,
	(methodPointerType)&DOSetter_1__ctor_m24119_gshared/* 3525*/,
	(methodPointerType)&DOSetter_1_Invoke_m24120_gshared/* 3526*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m24121_gshared/* 3527*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m24122_gshared/* 3528*/,
	(methodPointerType)&TweenerCore_3__ctor_m24123_gshared/* 3529*/,
	(methodPointerType)&TweenerCore_3_Reset_m24124_gshared/* 3530*/,
	(methodPointerType)&TweenerCore_3_Validate_m24125_gshared/* 3531*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m24126_gshared/* 3532*/,
	(methodPointerType)&TweenerCore_3_Startup_m24127_gshared/* 3533*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m24128_gshared/* 3534*/,
	(methodPointerType)&DOGetter_1__ctor_m24129_gshared/* 3535*/,
	(methodPointerType)&DOGetter_1_Invoke_m24130_gshared/* 3536*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m24131_gshared/* 3537*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m24132_gshared/* 3538*/,
	(methodPointerType)&DOSetter_1__ctor_m24133_gshared/* 3539*/,
	(methodPointerType)&DOSetter_1_Invoke_m24134_gshared/* 3540*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m24135_gshared/* 3541*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m24136_gshared/* 3542*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24273_gshared/* 3543*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24274_gshared/* 3544*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24275_gshared/* 3545*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24276_gshared/* 3546*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24277_gshared/* 3547*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24283_gshared/* 3548*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284_gshared/* 3549*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24285_gshared/* 3550*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24286_gshared/* 3551*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24287_gshared/* 3552*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24885_gshared/* 3553*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886_gshared/* 3554*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24887_gshared/* 3555*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24888_gshared/* 3556*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24889_gshared/* 3557*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24983_gshared/* 3558*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24984_gshared/* 3559*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24985_gshared/* 3560*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24986_gshared/* 3561*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24987_gshared/* 3562*/,
	(methodPointerType)&List_1__ctor_m24988_gshared/* 3563*/,
	(methodPointerType)&List_1__ctor_m24989_gshared/* 3564*/,
	(methodPointerType)&List_1__cctor_m24990_gshared/* 3565*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24991_gshared/* 3566*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m24992_gshared/* 3567*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m24993_gshared/* 3568*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m24994_gshared/* 3569*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m24995_gshared/* 3570*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m24996_gshared/* 3571*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m24997_gshared/* 3572*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m24998_gshared/* 3573*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24999_gshared/* 3574*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m25000_gshared/* 3575*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m25001_gshared/* 3576*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m25002_gshared/* 3577*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m25003_gshared/* 3578*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m25004_gshared/* 3579*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m25005_gshared/* 3580*/,
	(methodPointerType)&List_1_Add_m25006_gshared/* 3581*/,
	(methodPointerType)&List_1_GrowIfNeeded_m25007_gshared/* 3582*/,
	(methodPointerType)&List_1_AddCollection_m25008_gshared/* 3583*/,
	(methodPointerType)&List_1_AddEnumerable_m25009_gshared/* 3584*/,
	(methodPointerType)&List_1_AddRange_m25010_gshared/* 3585*/,
	(methodPointerType)&List_1_AsReadOnly_m25011_gshared/* 3586*/,
	(methodPointerType)&List_1_Clear_m25012_gshared/* 3587*/,
	(methodPointerType)&List_1_Contains_m25013_gshared/* 3588*/,
	(methodPointerType)&List_1_CopyTo_m25014_gshared/* 3589*/,
	(methodPointerType)&List_1_Find_m25015_gshared/* 3590*/,
	(methodPointerType)&List_1_CheckMatch_m25016_gshared/* 3591*/,
	(methodPointerType)&List_1_GetIndex_m25017_gshared/* 3592*/,
	(methodPointerType)&List_1_GetEnumerator_m25018_gshared/* 3593*/,
	(methodPointerType)&List_1_IndexOf_m25019_gshared/* 3594*/,
	(methodPointerType)&List_1_Shift_m25020_gshared/* 3595*/,
	(methodPointerType)&List_1_CheckIndex_m25021_gshared/* 3596*/,
	(methodPointerType)&List_1_Insert_m25022_gshared/* 3597*/,
	(methodPointerType)&List_1_CheckCollection_m25023_gshared/* 3598*/,
	(methodPointerType)&List_1_Remove_m25024_gshared/* 3599*/,
	(methodPointerType)&List_1_RemoveAll_m25025_gshared/* 3600*/,
	(methodPointerType)&List_1_RemoveAt_m25026_gshared/* 3601*/,
	(methodPointerType)&List_1_Reverse_m25027_gshared/* 3602*/,
	(methodPointerType)&List_1_Sort_m25028_gshared/* 3603*/,
	(methodPointerType)&List_1_Sort_m25029_gshared/* 3604*/,
	(methodPointerType)&List_1_ToArray_m25030_gshared/* 3605*/,
	(methodPointerType)&List_1_TrimExcess_m25031_gshared/* 3606*/,
	(methodPointerType)&List_1_get_Capacity_m25032_gshared/* 3607*/,
	(methodPointerType)&List_1_set_Capacity_m25033_gshared/* 3608*/,
	(methodPointerType)&List_1_get_Count_m25034_gshared/* 3609*/,
	(methodPointerType)&List_1_get_Item_m25035_gshared/* 3610*/,
	(methodPointerType)&List_1_set_Item_m25036_gshared/* 3611*/,
	(methodPointerType)&Enumerator__ctor_m25037_gshared/* 3612*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25038_gshared/* 3613*/,
	(methodPointerType)&Enumerator_Dispose_m25039_gshared/* 3614*/,
	(methodPointerType)&Enumerator_VerifyState_m25040_gshared/* 3615*/,
	(methodPointerType)&Enumerator_MoveNext_m25041_gshared/* 3616*/,
	(methodPointerType)&Enumerator_get_Current_m25042_gshared/* 3617*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m25043_gshared/* 3618*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25044_gshared/* 3619*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25045_gshared/* 3620*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25046_gshared/* 3621*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25047_gshared/* 3622*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25048_gshared/* 3623*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25049_gshared/* 3624*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25050_gshared/* 3625*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25051_gshared/* 3626*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25052_gshared/* 3627*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25053_gshared/* 3628*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m25054_gshared/* 3629*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m25055_gshared/* 3630*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m25056_gshared/* 3631*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25057_gshared/* 3632*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m25058_gshared/* 3633*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m25059_gshared/* 3634*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25060_gshared/* 3635*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25061_gshared/* 3636*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25062_gshared/* 3637*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25063_gshared/* 3638*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25064_gshared/* 3639*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m25065_gshared/* 3640*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m25066_gshared/* 3641*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m25067_gshared/* 3642*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m25068_gshared/* 3643*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m25069_gshared/* 3644*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m25070_gshared/* 3645*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m25071_gshared/* 3646*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m25072_gshared/* 3647*/,
	(methodPointerType)&Collection_1__ctor_m25073_gshared/* 3648*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25074_gshared/* 3649*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m25075_gshared/* 3650*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m25076_gshared/* 3651*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m25077_gshared/* 3652*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m25078_gshared/* 3653*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m25079_gshared/* 3654*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m25080_gshared/* 3655*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m25081_gshared/* 3656*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m25082_gshared/* 3657*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m25083_gshared/* 3658*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m25084_gshared/* 3659*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m25085_gshared/* 3660*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m25086_gshared/* 3661*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m25087_gshared/* 3662*/,
	(methodPointerType)&Collection_1_Add_m25088_gshared/* 3663*/,
	(methodPointerType)&Collection_1_Clear_m25089_gshared/* 3664*/,
	(methodPointerType)&Collection_1_ClearItems_m25090_gshared/* 3665*/,
	(methodPointerType)&Collection_1_Contains_m25091_gshared/* 3666*/,
	(methodPointerType)&Collection_1_CopyTo_m25092_gshared/* 3667*/,
	(methodPointerType)&Collection_1_GetEnumerator_m25093_gshared/* 3668*/,
	(methodPointerType)&Collection_1_IndexOf_m25094_gshared/* 3669*/,
	(methodPointerType)&Collection_1_Insert_m25095_gshared/* 3670*/,
	(methodPointerType)&Collection_1_InsertItem_m25096_gshared/* 3671*/,
	(methodPointerType)&Collection_1_Remove_m25097_gshared/* 3672*/,
	(methodPointerType)&Collection_1_RemoveAt_m25098_gshared/* 3673*/,
	(methodPointerType)&Collection_1_RemoveItem_m25099_gshared/* 3674*/,
	(methodPointerType)&Collection_1_get_Count_m25100_gshared/* 3675*/,
	(methodPointerType)&Collection_1_get_Item_m25101_gshared/* 3676*/,
	(methodPointerType)&Collection_1_set_Item_m25102_gshared/* 3677*/,
	(methodPointerType)&Collection_1_SetItem_m25103_gshared/* 3678*/,
	(methodPointerType)&Collection_1_IsValidItem_m25104_gshared/* 3679*/,
	(methodPointerType)&Collection_1_ConvertItem_m25105_gshared/* 3680*/,
	(methodPointerType)&Collection_1_CheckWritable_m25106_gshared/* 3681*/,
	(methodPointerType)&Collection_1_IsSynchronized_m25107_gshared/* 3682*/,
	(methodPointerType)&Collection_1_IsFixedSize_m25108_gshared/* 3683*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25109_gshared/* 3684*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25110_gshared/* 3685*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25111_gshared/* 3686*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25112_gshared/* 3687*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25113_gshared/* 3688*/,
	(methodPointerType)&DefaultComparer__ctor_m25114_gshared/* 3689*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25115_gshared/* 3690*/,
	(methodPointerType)&DefaultComparer_Equals_m25116_gshared/* 3691*/,
	(methodPointerType)&Predicate_1__ctor_m25117_gshared/* 3692*/,
	(methodPointerType)&Predicate_1_Invoke_m25118_gshared/* 3693*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m25119_gshared/* 3694*/,
	(methodPointerType)&Predicate_1_EndInvoke_m25120_gshared/* 3695*/,
	(methodPointerType)&Comparer_1__ctor_m25121_gshared/* 3696*/,
	(methodPointerType)&Comparer_1__cctor_m25122_gshared/* 3697*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m25123_gshared/* 3698*/,
	(methodPointerType)&Comparer_1_get_Default_m25124_gshared/* 3699*/,
	(methodPointerType)&DefaultComparer__ctor_m25125_gshared/* 3700*/,
	(methodPointerType)&DefaultComparer_Compare_m25126_gshared/* 3701*/,
	(methodPointerType)&Comparison_1__ctor_m25127_gshared/* 3702*/,
	(methodPointerType)&Comparison_1_Invoke_m25128_gshared/* 3703*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m25129_gshared/* 3704*/,
	(methodPointerType)&Comparison_1_EndInvoke_m25130_gshared/* 3705*/,
	(methodPointerType)&List_1__ctor_m25131_gshared/* 3706*/,
	(methodPointerType)&List_1__ctor_m25132_gshared/* 3707*/,
	(methodPointerType)&List_1__cctor_m25133_gshared/* 3708*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25134_gshared/* 3709*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m25135_gshared/* 3710*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m25136_gshared/* 3711*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m25137_gshared/* 3712*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m25138_gshared/* 3713*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m25139_gshared/* 3714*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m25140_gshared/* 3715*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m25141_gshared/* 3716*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25142_gshared/* 3717*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m25143_gshared/* 3718*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m25144_gshared/* 3719*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m25145_gshared/* 3720*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m25146_gshared/* 3721*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m25147_gshared/* 3722*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m25148_gshared/* 3723*/,
	(methodPointerType)&List_1_Add_m25149_gshared/* 3724*/,
	(methodPointerType)&List_1_GrowIfNeeded_m25150_gshared/* 3725*/,
	(methodPointerType)&List_1_AddCollection_m25151_gshared/* 3726*/,
	(methodPointerType)&List_1_AddEnumerable_m25152_gshared/* 3727*/,
	(methodPointerType)&List_1_AddRange_m25153_gshared/* 3728*/,
	(methodPointerType)&List_1_AsReadOnly_m25154_gshared/* 3729*/,
	(methodPointerType)&List_1_Clear_m25155_gshared/* 3730*/,
	(methodPointerType)&List_1_Contains_m25156_gshared/* 3731*/,
	(methodPointerType)&List_1_CopyTo_m25157_gshared/* 3732*/,
	(methodPointerType)&List_1_Find_m25158_gshared/* 3733*/,
	(methodPointerType)&List_1_CheckMatch_m25159_gshared/* 3734*/,
	(methodPointerType)&List_1_GetIndex_m25160_gshared/* 3735*/,
	(methodPointerType)&List_1_GetEnumerator_m25161_gshared/* 3736*/,
	(methodPointerType)&List_1_IndexOf_m25162_gshared/* 3737*/,
	(methodPointerType)&List_1_Shift_m25163_gshared/* 3738*/,
	(methodPointerType)&List_1_CheckIndex_m25164_gshared/* 3739*/,
	(methodPointerType)&List_1_Insert_m25165_gshared/* 3740*/,
	(methodPointerType)&List_1_CheckCollection_m25166_gshared/* 3741*/,
	(methodPointerType)&List_1_Remove_m25167_gshared/* 3742*/,
	(methodPointerType)&List_1_RemoveAll_m25168_gshared/* 3743*/,
	(methodPointerType)&List_1_RemoveAt_m25169_gshared/* 3744*/,
	(methodPointerType)&List_1_Reverse_m25170_gshared/* 3745*/,
	(methodPointerType)&List_1_Sort_m25171_gshared/* 3746*/,
	(methodPointerType)&List_1_Sort_m25172_gshared/* 3747*/,
	(methodPointerType)&List_1_ToArray_m25173_gshared/* 3748*/,
	(methodPointerType)&List_1_TrimExcess_m25174_gshared/* 3749*/,
	(methodPointerType)&List_1_get_Capacity_m25175_gshared/* 3750*/,
	(methodPointerType)&List_1_set_Capacity_m25176_gshared/* 3751*/,
	(methodPointerType)&List_1_get_Count_m25177_gshared/* 3752*/,
	(methodPointerType)&List_1_get_Item_m25178_gshared/* 3753*/,
	(methodPointerType)&List_1_set_Item_m25179_gshared/* 3754*/,
	(methodPointerType)&Enumerator__ctor_m25180_gshared/* 3755*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25181_gshared/* 3756*/,
	(methodPointerType)&Enumerator_Dispose_m25182_gshared/* 3757*/,
	(methodPointerType)&Enumerator_VerifyState_m25183_gshared/* 3758*/,
	(methodPointerType)&Enumerator_MoveNext_m25184_gshared/* 3759*/,
	(methodPointerType)&Enumerator_get_Current_m25185_gshared/* 3760*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m25186_gshared/* 3761*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25187_gshared/* 3762*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25188_gshared/* 3763*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25189_gshared/* 3764*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25190_gshared/* 3765*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25191_gshared/* 3766*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25192_gshared/* 3767*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25193_gshared/* 3768*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25194_gshared/* 3769*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25195_gshared/* 3770*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25196_gshared/* 3771*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m25197_gshared/* 3772*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m25198_gshared/* 3773*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m25199_gshared/* 3774*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25200_gshared/* 3775*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m25201_gshared/* 3776*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m25202_gshared/* 3777*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25203_gshared/* 3778*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25204_gshared/* 3779*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25205_gshared/* 3780*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25206_gshared/* 3781*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25207_gshared/* 3782*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m25208_gshared/* 3783*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m25209_gshared/* 3784*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m25210_gshared/* 3785*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m25211_gshared/* 3786*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m25212_gshared/* 3787*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m25213_gshared/* 3788*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m25214_gshared/* 3789*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m25215_gshared/* 3790*/,
	(methodPointerType)&Collection_1__ctor_m25216_gshared/* 3791*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25217_gshared/* 3792*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m25218_gshared/* 3793*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m25219_gshared/* 3794*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m25220_gshared/* 3795*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m25221_gshared/* 3796*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m25222_gshared/* 3797*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m25223_gshared/* 3798*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m25224_gshared/* 3799*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m25225_gshared/* 3800*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m25226_gshared/* 3801*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m25227_gshared/* 3802*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m25228_gshared/* 3803*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m25229_gshared/* 3804*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m25230_gshared/* 3805*/,
	(methodPointerType)&Collection_1_Add_m25231_gshared/* 3806*/,
	(methodPointerType)&Collection_1_Clear_m25232_gshared/* 3807*/,
	(methodPointerType)&Collection_1_ClearItems_m25233_gshared/* 3808*/,
	(methodPointerType)&Collection_1_Contains_m25234_gshared/* 3809*/,
	(methodPointerType)&Collection_1_CopyTo_m25235_gshared/* 3810*/,
	(methodPointerType)&Collection_1_GetEnumerator_m25236_gshared/* 3811*/,
	(methodPointerType)&Collection_1_IndexOf_m25237_gshared/* 3812*/,
	(methodPointerType)&Collection_1_Insert_m25238_gshared/* 3813*/,
	(methodPointerType)&Collection_1_InsertItem_m25239_gshared/* 3814*/,
	(methodPointerType)&Collection_1_Remove_m25240_gshared/* 3815*/,
	(methodPointerType)&Collection_1_RemoveAt_m25241_gshared/* 3816*/,
	(methodPointerType)&Collection_1_RemoveItem_m25242_gshared/* 3817*/,
	(methodPointerType)&Collection_1_get_Count_m25243_gshared/* 3818*/,
	(methodPointerType)&Collection_1_get_Item_m25244_gshared/* 3819*/,
	(methodPointerType)&Collection_1_set_Item_m25245_gshared/* 3820*/,
	(methodPointerType)&Collection_1_SetItem_m25246_gshared/* 3821*/,
	(methodPointerType)&Collection_1_IsValidItem_m25247_gshared/* 3822*/,
	(methodPointerType)&Collection_1_ConvertItem_m25248_gshared/* 3823*/,
	(methodPointerType)&Collection_1_CheckWritable_m25249_gshared/* 3824*/,
	(methodPointerType)&Collection_1_IsSynchronized_m25250_gshared/* 3825*/,
	(methodPointerType)&Collection_1_IsFixedSize_m25251_gshared/* 3826*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25252_gshared/* 3827*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25253_gshared/* 3828*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25254_gshared/* 3829*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25255_gshared/* 3830*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25256_gshared/* 3831*/,
	(methodPointerType)&DefaultComparer__ctor_m25257_gshared/* 3832*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25258_gshared/* 3833*/,
	(methodPointerType)&DefaultComparer_Equals_m25259_gshared/* 3834*/,
	(methodPointerType)&Predicate_1__ctor_m25260_gshared/* 3835*/,
	(methodPointerType)&Predicate_1_Invoke_m25261_gshared/* 3836*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m25262_gshared/* 3837*/,
	(methodPointerType)&Predicate_1_EndInvoke_m25263_gshared/* 3838*/,
	(methodPointerType)&Comparer_1__ctor_m25264_gshared/* 3839*/,
	(methodPointerType)&Comparer_1__cctor_m25265_gshared/* 3840*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m25266_gshared/* 3841*/,
	(methodPointerType)&Comparer_1_get_Default_m25267_gshared/* 3842*/,
	(methodPointerType)&DefaultComparer__ctor_m25268_gshared/* 3843*/,
	(methodPointerType)&DefaultComparer_Compare_m25269_gshared/* 3844*/,
	(methodPointerType)&Comparison_1__ctor_m25270_gshared/* 3845*/,
	(methodPointerType)&Comparison_1_Invoke_m25271_gshared/* 3846*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m25272_gshared/* 3847*/,
	(methodPointerType)&Comparison_1_EndInvoke_m25273_gshared/* 3848*/,
	(methodPointerType)&Dictionary_2__ctor_m25275_gshared/* 3849*/,
	(methodPointerType)&Dictionary_2__ctor_m25277_gshared/* 3850*/,
	(methodPointerType)&Dictionary_2__ctor_m25279_gshared/* 3851*/,
	(methodPointerType)&Dictionary_2__ctor_m25281_gshared/* 3852*/,
	(methodPointerType)&Dictionary_2__ctor_m25283_gshared/* 3853*/,
	(methodPointerType)&Dictionary_2__ctor_m25285_gshared/* 3854*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25287_gshared/* 3855*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25289_gshared/* 3856*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25291_gshared/* 3857*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25293_gshared/* 3858*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25295_gshared/* 3859*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m25297_gshared/* 3860*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25299_gshared/* 3861*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25301_gshared/* 3862*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25303_gshared/* 3863*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25305_gshared/* 3864*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25307_gshared/* 3865*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25309_gshared/* 3866*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25311_gshared/* 3867*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25313_gshared/* 3868*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25315_gshared/* 3869*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25317_gshared/* 3870*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25319_gshared/* 3871*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25321_gshared/* 3872*/,
	(methodPointerType)&Dictionary_2_get_Count_m25323_gshared/* 3873*/,
	(methodPointerType)&Dictionary_2_get_Item_m25325_gshared/* 3874*/,
	(methodPointerType)&Dictionary_2_set_Item_m25327_gshared/* 3875*/,
	(methodPointerType)&Dictionary_2_Init_m25329_gshared/* 3876*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25331_gshared/* 3877*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25333_gshared/* 3878*/,
	(methodPointerType)&Dictionary_2_make_pair_m25335_gshared/* 3879*/,
	(methodPointerType)&Dictionary_2_pick_key_m25337_gshared/* 3880*/,
	(methodPointerType)&Dictionary_2_pick_value_m25339_gshared/* 3881*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25341_gshared/* 3882*/,
	(methodPointerType)&Dictionary_2_Resize_m25343_gshared/* 3883*/,
	(methodPointerType)&Dictionary_2_Add_m25345_gshared/* 3884*/,
	(methodPointerType)&Dictionary_2_Clear_m25347_gshared/* 3885*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25349_gshared/* 3886*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25351_gshared/* 3887*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25353_gshared/* 3888*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25355_gshared/* 3889*/,
	(methodPointerType)&Dictionary_2_Remove_m25357_gshared/* 3890*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25359_gshared/* 3891*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25361_gshared/* 3892*/,
	(methodPointerType)&Dictionary_2_get_Values_m25363_gshared/* 3893*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25365_gshared/* 3894*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25367_gshared/* 3895*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25369_gshared/* 3896*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25371_gshared/* 3897*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25373_gshared/* 3898*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25374_gshared/* 3899*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25375_gshared/* 3900*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25376_gshared/* 3901*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25377_gshared/* 3902*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25378_gshared/* 3903*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25379_gshared/* 3904*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25380_gshared/* 3905*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25381_gshared/* 3906*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25382_gshared/* 3907*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25383_gshared/* 3908*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25384_gshared/* 3909*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25385_gshared/* 3910*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25386_gshared/* 3911*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25387_gshared/* 3912*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25388_gshared/* 3913*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25389_gshared/* 3914*/,
	(methodPointerType)&KeyCollection__ctor_m25390_gshared/* 3915*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25391_gshared/* 3916*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25392_gshared/* 3917*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25393_gshared/* 3918*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25394_gshared/* 3919*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25395_gshared/* 3920*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25396_gshared/* 3921*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25397_gshared/* 3922*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25398_gshared/* 3923*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25399_gshared/* 3924*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25400_gshared/* 3925*/,
	(methodPointerType)&KeyCollection_CopyTo_m25401_gshared/* 3926*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25402_gshared/* 3927*/,
	(methodPointerType)&KeyCollection_get_Count_m25403_gshared/* 3928*/,
	(methodPointerType)&Enumerator__ctor_m25404_gshared/* 3929*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25405_gshared/* 3930*/,
	(methodPointerType)&Enumerator_Dispose_m25406_gshared/* 3931*/,
	(methodPointerType)&Enumerator_MoveNext_m25407_gshared/* 3932*/,
	(methodPointerType)&Enumerator_get_Current_m25408_gshared/* 3933*/,
	(methodPointerType)&Enumerator__ctor_m25409_gshared/* 3934*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25410_gshared/* 3935*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411_gshared/* 3936*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412_gshared/* 3937*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413_gshared/* 3938*/,
	(methodPointerType)&Enumerator_MoveNext_m25414_gshared/* 3939*/,
	(methodPointerType)&Enumerator_get_Current_m25415_gshared/* 3940*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m25416_gshared/* 3941*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m25417_gshared/* 3942*/,
	(methodPointerType)&Enumerator_VerifyState_m25418_gshared/* 3943*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m25419_gshared/* 3944*/,
	(methodPointerType)&Enumerator_Dispose_m25420_gshared/* 3945*/,
	(methodPointerType)&Transform_1__ctor_m25421_gshared/* 3946*/,
	(methodPointerType)&Transform_1_Invoke_m25422_gshared/* 3947*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25423_gshared/* 3948*/,
	(methodPointerType)&Transform_1_EndInvoke_m25424_gshared/* 3949*/,
	(methodPointerType)&ValueCollection__ctor_m25425_gshared/* 3950*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25426_gshared/* 3951*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25427_gshared/* 3952*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25428_gshared/* 3953*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25429_gshared/* 3954*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25430_gshared/* 3955*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m25431_gshared/* 3956*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25432_gshared/* 3957*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25433_gshared/* 3958*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25434_gshared/* 3959*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m25435_gshared/* 3960*/,
	(methodPointerType)&ValueCollection_CopyTo_m25436_gshared/* 3961*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m25437_gshared/* 3962*/,
	(methodPointerType)&ValueCollection_get_Count_m25438_gshared/* 3963*/,
	(methodPointerType)&Enumerator__ctor_m25439_gshared/* 3964*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25440_gshared/* 3965*/,
	(methodPointerType)&Enumerator_Dispose_m25441_gshared/* 3966*/,
	(methodPointerType)&Enumerator_MoveNext_m25442_gshared/* 3967*/,
	(methodPointerType)&Enumerator_get_Current_m25443_gshared/* 3968*/,
	(methodPointerType)&Transform_1__ctor_m25444_gshared/* 3969*/,
	(methodPointerType)&Transform_1_Invoke_m25445_gshared/* 3970*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25446_gshared/* 3971*/,
	(methodPointerType)&Transform_1_EndInvoke_m25447_gshared/* 3972*/,
	(methodPointerType)&Transform_1__ctor_m25448_gshared/* 3973*/,
	(methodPointerType)&Transform_1_Invoke_m25449_gshared/* 3974*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25450_gshared/* 3975*/,
	(methodPointerType)&Transform_1_EndInvoke_m25451_gshared/* 3976*/,
	(methodPointerType)&Transform_1__ctor_m25452_gshared/* 3977*/,
	(methodPointerType)&Transform_1_Invoke_m25453_gshared/* 3978*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25454_gshared/* 3979*/,
	(methodPointerType)&Transform_1_EndInvoke_m25455_gshared/* 3980*/,
	(methodPointerType)&ShimEnumerator__ctor_m25456_gshared/* 3981*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m25457_gshared/* 3982*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m25458_gshared/* 3983*/,
	(methodPointerType)&ShimEnumerator_get_Key_m25459_gshared/* 3984*/,
	(methodPointerType)&ShimEnumerator_get_Value_m25460_gshared/* 3985*/,
	(methodPointerType)&ShimEnumerator_get_Current_m25461_gshared/* 3986*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25462_gshared/* 3987*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25463_gshared/* 3988*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25464_gshared/* 3989*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25465_gshared/* 3990*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25466_gshared/* 3991*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25467_gshared/* 3992*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25468_gshared/* 3993*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25469_gshared/* 3994*/,
	(methodPointerType)&DefaultComparer__ctor_m25470_gshared/* 3995*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25471_gshared/* 3996*/,
	(methodPointerType)&DefaultComparer_Equals_m25472_gshared/* 3997*/,
	(methodPointerType)&Dictionary_2__ctor_m25713_gshared/* 3998*/,
	(methodPointerType)&Dictionary_2__ctor_m25715_gshared/* 3999*/,
	(methodPointerType)&Dictionary_2__ctor_m25717_gshared/* 4000*/,
	(methodPointerType)&Dictionary_2__ctor_m25719_gshared/* 4001*/,
	(methodPointerType)&Dictionary_2__ctor_m25721_gshared/* 4002*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25723_gshared/* 4003*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25725_gshared/* 4004*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25727_gshared/* 4005*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25729_gshared/* 4006*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25731_gshared/* 4007*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m25733_gshared/* 4008*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25735_gshared/* 4009*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25737_gshared/* 4010*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25739_gshared/* 4011*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25741_gshared/* 4012*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25743_gshared/* 4013*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25745_gshared/* 4014*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25747_gshared/* 4015*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25749_gshared/* 4016*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25751_gshared/* 4017*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25753_gshared/* 4018*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25755_gshared/* 4019*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25757_gshared/* 4020*/,
	(methodPointerType)&Dictionary_2_get_Count_m25759_gshared/* 4021*/,
	(methodPointerType)&Dictionary_2_get_Item_m25761_gshared/* 4022*/,
	(methodPointerType)&Dictionary_2_set_Item_m25763_gshared/* 4023*/,
	(methodPointerType)&Dictionary_2_Init_m25765_gshared/* 4024*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25767_gshared/* 4025*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25769_gshared/* 4026*/,
	(methodPointerType)&Dictionary_2_make_pair_m25771_gshared/* 4027*/,
	(methodPointerType)&Dictionary_2_pick_key_m25773_gshared/* 4028*/,
	(methodPointerType)&Dictionary_2_pick_value_m25775_gshared/* 4029*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25777_gshared/* 4030*/,
	(methodPointerType)&Dictionary_2_Resize_m25779_gshared/* 4031*/,
	(methodPointerType)&Dictionary_2_Add_m25781_gshared/* 4032*/,
	(methodPointerType)&Dictionary_2_Clear_m25783_gshared/* 4033*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25785_gshared/* 4034*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25787_gshared/* 4035*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25789_gshared/* 4036*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25791_gshared/* 4037*/,
	(methodPointerType)&Dictionary_2_Remove_m25793_gshared/* 4038*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25795_gshared/* 4039*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25797_gshared/* 4040*/,
	(methodPointerType)&Dictionary_2_get_Values_m25799_gshared/* 4041*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25801_gshared/* 4042*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25803_gshared/* 4043*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25805_gshared/* 4044*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25807_gshared/* 4045*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25809_gshared/* 4046*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25810_gshared/* 4047*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25811_gshared/* 4048*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25812_gshared/* 4049*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25813_gshared/* 4050*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25814_gshared/* 4051*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25815_gshared/* 4052*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25816_gshared/* 4053*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25817_gshared/* 4054*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25818_gshared/* 4055*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25819_gshared/* 4056*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25820_gshared/* 4057*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25821_gshared/* 4058*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25822_gshared/* 4059*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25823_gshared/* 4060*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25824_gshared/* 4061*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25825_gshared/* 4062*/,
	(methodPointerType)&KeyCollection__ctor_m25826_gshared/* 4063*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25827_gshared/* 4064*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25828_gshared/* 4065*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25829_gshared/* 4066*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25830_gshared/* 4067*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25831_gshared/* 4068*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25832_gshared/* 4069*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25833_gshared/* 4070*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25834_gshared/* 4071*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25835_gshared/* 4072*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25836_gshared/* 4073*/,
	(methodPointerType)&KeyCollection_CopyTo_m25837_gshared/* 4074*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25838_gshared/* 4075*/,
	(methodPointerType)&KeyCollection_get_Count_m25839_gshared/* 4076*/,
	(methodPointerType)&Enumerator__ctor_m25840_gshared/* 4077*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25841_gshared/* 4078*/,
	(methodPointerType)&Enumerator_Dispose_m25842_gshared/* 4079*/,
	(methodPointerType)&Enumerator_MoveNext_m25843_gshared/* 4080*/,
	(methodPointerType)&Enumerator_get_Current_m25844_gshared/* 4081*/,
	(methodPointerType)&Enumerator__ctor_m25845_gshared/* 4082*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25846_gshared/* 4083*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25847_gshared/* 4084*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25848_gshared/* 4085*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25849_gshared/* 4086*/,
	(methodPointerType)&Enumerator_MoveNext_m25850_gshared/* 4087*/,
	(methodPointerType)&Enumerator_get_Current_m25851_gshared/* 4088*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m25852_gshared/* 4089*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m25853_gshared/* 4090*/,
	(methodPointerType)&Enumerator_VerifyState_m25854_gshared/* 4091*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m25855_gshared/* 4092*/,
	(methodPointerType)&Enumerator_Dispose_m25856_gshared/* 4093*/,
	(methodPointerType)&Transform_1__ctor_m25857_gshared/* 4094*/,
	(methodPointerType)&Transform_1_Invoke_m25858_gshared/* 4095*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25859_gshared/* 4096*/,
	(methodPointerType)&Transform_1_EndInvoke_m25860_gshared/* 4097*/,
	(methodPointerType)&ValueCollection__ctor_m25861_gshared/* 4098*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25862_gshared/* 4099*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25863_gshared/* 4100*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25864_gshared/* 4101*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25865_gshared/* 4102*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25866_gshared/* 4103*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m25867_gshared/* 4104*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25868_gshared/* 4105*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25869_gshared/* 4106*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25870_gshared/* 4107*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m25871_gshared/* 4108*/,
	(methodPointerType)&ValueCollection_CopyTo_m25872_gshared/* 4109*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m25873_gshared/* 4110*/,
	(methodPointerType)&ValueCollection_get_Count_m25874_gshared/* 4111*/,
	(methodPointerType)&Enumerator__ctor_m25875_gshared/* 4112*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25876_gshared/* 4113*/,
	(methodPointerType)&Enumerator_Dispose_m25877_gshared/* 4114*/,
	(methodPointerType)&Enumerator_MoveNext_m25878_gshared/* 4115*/,
	(methodPointerType)&Enumerator_get_Current_m25879_gshared/* 4116*/,
	(methodPointerType)&Transform_1__ctor_m25880_gshared/* 4117*/,
	(methodPointerType)&Transform_1_Invoke_m25881_gshared/* 4118*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25882_gshared/* 4119*/,
	(methodPointerType)&Transform_1_EndInvoke_m25883_gshared/* 4120*/,
	(methodPointerType)&Transform_1__ctor_m25884_gshared/* 4121*/,
	(methodPointerType)&Transform_1_Invoke_m25885_gshared/* 4122*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25886_gshared/* 4123*/,
	(methodPointerType)&Transform_1_EndInvoke_m25887_gshared/* 4124*/,
	(methodPointerType)&Transform_1__ctor_m25888_gshared/* 4125*/,
	(methodPointerType)&Transform_1_Invoke_m25889_gshared/* 4126*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25890_gshared/* 4127*/,
	(methodPointerType)&Transform_1_EndInvoke_m25891_gshared/* 4128*/,
	(methodPointerType)&ShimEnumerator__ctor_m25892_gshared/* 4129*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m25893_gshared/* 4130*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m25894_gshared/* 4131*/,
	(methodPointerType)&ShimEnumerator_get_Key_m25895_gshared/* 4132*/,
	(methodPointerType)&ShimEnumerator_get_Value_m25896_gshared/* 4133*/,
	(methodPointerType)&ShimEnumerator_get_Current_m25897_gshared/* 4134*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25898_gshared/* 4135*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25899_gshared/* 4136*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25900_gshared/* 4137*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25901_gshared/* 4138*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25902_gshared/* 4139*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25903_gshared/* 4140*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25904_gshared/* 4141*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25905_gshared/* 4142*/,
	(methodPointerType)&DefaultComparer__ctor_m25906_gshared/* 4143*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25907_gshared/* 4144*/,
	(methodPointerType)&DefaultComparer_Equals_m25908_gshared/* 4145*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26083_gshared/* 4146*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26084_gshared/* 4147*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26085_gshared/* 4148*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26086_gshared/* 4149*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26087_gshared/* 4150*/,
	(methodPointerType)&KeyValuePair_2__ctor_m26088_gshared/* 4151*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m26089_gshared/* 4152*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m26090_gshared/* 4153*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m26091_gshared/* 4154*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m26092_gshared/* 4155*/,
	(methodPointerType)&KeyValuePair_2_ToString_m26093_gshared/* 4156*/,
	(methodPointerType)&Dictionary_2__ctor_m26447_gshared/* 4157*/,
	(methodPointerType)&Dictionary_2__ctor_m26449_gshared/* 4158*/,
	(methodPointerType)&Dictionary_2__ctor_m26451_gshared/* 4159*/,
	(methodPointerType)&Dictionary_2__ctor_m26453_gshared/* 4160*/,
	(methodPointerType)&Dictionary_2__ctor_m26455_gshared/* 4161*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26457_gshared/* 4162*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26459_gshared/* 4163*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m26461_gshared/* 4164*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m26463_gshared/* 4165*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m26465_gshared/* 4166*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m26467_gshared/* 4167*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m26469_gshared/* 4168*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26471_gshared/* 4169*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26473_gshared/* 4170*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26475_gshared/* 4171*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26477_gshared/* 4172*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26479_gshared/* 4173*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26481_gshared/* 4174*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26483_gshared/* 4175*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m26485_gshared/* 4176*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26487_gshared/* 4177*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26489_gshared/* 4178*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26491_gshared/* 4179*/,
	(methodPointerType)&Dictionary_2_get_Count_m26493_gshared/* 4180*/,
	(methodPointerType)&Dictionary_2_get_Item_m26495_gshared/* 4181*/,
	(methodPointerType)&Dictionary_2_set_Item_m26497_gshared/* 4182*/,
	(methodPointerType)&Dictionary_2_Init_m26499_gshared/* 4183*/,
	(methodPointerType)&Dictionary_2_InitArrays_m26501_gshared/* 4184*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m26503_gshared/* 4185*/,
	(methodPointerType)&Dictionary_2_make_pair_m26505_gshared/* 4186*/,
	(methodPointerType)&Dictionary_2_pick_key_m26507_gshared/* 4187*/,
	(methodPointerType)&Dictionary_2_pick_value_m26509_gshared/* 4188*/,
	(methodPointerType)&Dictionary_2_CopyTo_m26511_gshared/* 4189*/,
	(methodPointerType)&Dictionary_2_Resize_m26513_gshared/* 4190*/,
	(methodPointerType)&Dictionary_2_Add_m26515_gshared/* 4191*/,
	(methodPointerType)&Dictionary_2_Clear_m26517_gshared/* 4192*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m26519_gshared/* 4193*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m26521_gshared/* 4194*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m26523_gshared/* 4195*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m26525_gshared/* 4196*/,
	(methodPointerType)&Dictionary_2_Remove_m26527_gshared/* 4197*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m26529_gshared/* 4198*/,
	(methodPointerType)&Dictionary_2_get_Keys_m26531_gshared/* 4199*/,
	(methodPointerType)&Dictionary_2_get_Values_m26533_gshared/* 4200*/,
	(methodPointerType)&Dictionary_2_ToTKey_m26535_gshared/* 4201*/,
	(methodPointerType)&Dictionary_2_ToTValue_m26537_gshared/* 4202*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m26539_gshared/* 4203*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m26541_gshared/* 4204*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m26543_gshared/* 4205*/,
	(methodPointerType)&KeyCollection__ctor_m26544_gshared/* 4206*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26545_gshared/* 4207*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26546_gshared/* 4208*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26547_gshared/* 4209*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26548_gshared/* 4210*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26549_gshared/* 4211*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m26550_gshared/* 4212*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26551_gshared/* 4213*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26552_gshared/* 4214*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26553_gshared/* 4215*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m26554_gshared/* 4216*/,
	(methodPointerType)&KeyCollection_CopyTo_m26555_gshared/* 4217*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m26556_gshared/* 4218*/,
	(methodPointerType)&KeyCollection_get_Count_m26557_gshared/* 4219*/,
	(methodPointerType)&Enumerator__ctor_m26558_gshared/* 4220*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26559_gshared/* 4221*/,
	(methodPointerType)&Enumerator_Dispose_m26560_gshared/* 4222*/,
	(methodPointerType)&Enumerator_MoveNext_m26561_gshared/* 4223*/,
	(methodPointerType)&Enumerator_get_Current_m26562_gshared/* 4224*/,
	(methodPointerType)&Enumerator__ctor_m26563_gshared/* 4225*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26564_gshared/* 4226*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26565_gshared/* 4227*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26566_gshared/* 4228*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26567_gshared/* 4229*/,
	(methodPointerType)&Enumerator_MoveNext_m26568_gshared/* 4230*/,
	(methodPointerType)&Enumerator_get_Current_m26569_gshared/* 4231*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m26570_gshared/* 4232*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m26571_gshared/* 4233*/,
	(methodPointerType)&Enumerator_VerifyState_m26572_gshared/* 4234*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m26573_gshared/* 4235*/,
	(methodPointerType)&Enumerator_Dispose_m26574_gshared/* 4236*/,
	(methodPointerType)&Transform_1__ctor_m26575_gshared/* 4237*/,
	(methodPointerType)&Transform_1_Invoke_m26576_gshared/* 4238*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26577_gshared/* 4239*/,
	(methodPointerType)&Transform_1_EndInvoke_m26578_gshared/* 4240*/,
	(methodPointerType)&ValueCollection__ctor_m26579_gshared/* 4241*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26580_gshared/* 4242*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26581_gshared/* 4243*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26582_gshared/* 4244*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26583_gshared/* 4245*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26584_gshared/* 4246*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m26585_gshared/* 4247*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26586_gshared/* 4248*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26587_gshared/* 4249*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26588_gshared/* 4250*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m26589_gshared/* 4251*/,
	(methodPointerType)&ValueCollection_CopyTo_m26590_gshared/* 4252*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m26591_gshared/* 4253*/,
	(methodPointerType)&ValueCollection_get_Count_m26592_gshared/* 4254*/,
	(methodPointerType)&Enumerator__ctor_m26593_gshared/* 4255*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26594_gshared/* 4256*/,
	(methodPointerType)&Enumerator_Dispose_m26595_gshared/* 4257*/,
	(methodPointerType)&Enumerator_MoveNext_m26596_gshared/* 4258*/,
	(methodPointerType)&Enumerator_get_Current_m26597_gshared/* 4259*/,
	(methodPointerType)&Transform_1__ctor_m26598_gshared/* 4260*/,
	(methodPointerType)&Transform_1_Invoke_m26599_gshared/* 4261*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26600_gshared/* 4262*/,
	(methodPointerType)&Transform_1_EndInvoke_m26601_gshared/* 4263*/,
	(methodPointerType)&Transform_1__ctor_m26602_gshared/* 4264*/,
	(methodPointerType)&Transform_1_Invoke_m26603_gshared/* 4265*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26604_gshared/* 4266*/,
	(methodPointerType)&Transform_1_EndInvoke_m26605_gshared/* 4267*/,
	(methodPointerType)&Transform_1__ctor_m26606_gshared/* 4268*/,
	(methodPointerType)&Transform_1_Invoke_m26607_gshared/* 4269*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26608_gshared/* 4270*/,
	(methodPointerType)&Transform_1_EndInvoke_m26609_gshared/* 4271*/,
	(methodPointerType)&ShimEnumerator__ctor_m26610_gshared/* 4272*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m26611_gshared/* 4273*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m26612_gshared/* 4274*/,
	(methodPointerType)&ShimEnumerator_get_Key_m26613_gshared/* 4275*/,
	(methodPointerType)&ShimEnumerator_get_Value_m26614_gshared/* 4276*/,
	(methodPointerType)&ShimEnumerator_get_Current_m26615_gshared/* 4277*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26616_gshared/* 4278*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26617_gshared/* 4279*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26618_gshared/* 4280*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26619_gshared/* 4281*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26620_gshared/* 4282*/,
	(methodPointerType)&DefaultComparer__ctor_m26621_gshared/* 4283*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26622_gshared/* 4284*/,
	(methodPointerType)&DefaultComparer_Equals_m26623_gshared/* 4285*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26722_gshared/* 4286*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26723_gshared/* 4287*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26724_gshared/* 4288*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26725_gshared/* 4289*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26726_gshared/* 4290*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26727_gshared/* 4291*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26728_gshared/* 4292*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26729_gshared/* 4293*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26730_gshared/* 4294*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26731_gshared/* 4295*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26855_gshared/* 4296*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26856_gshared/* 4297*/,
	(methodPointerType)&InvokableCall_1__ctor_m26857_gshared/* 4298*/,
	(methodPointerType)&InvokableCall_1__ctor_m26858_gshared/* 4299*/,
	(methodPointerType)&InvokableCall_1_Invoke_m26859_gshared/* 4300*/,
	(methodPointerType)&InvokableCall_1_Find_m26860_gshared/* 4301*/,
	(methodPointerType)&UnityAction_1__ctor_m26861_gshared/* 4302*/,
	(methodPointerType)&UnityAction_1_Invoke_m26862_gshared/* 4303*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m26863_gshared/* 4304*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m26864_gshared/* 4305*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26872_gshared/* 4306*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27070_gshared/* 4307*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27071_gshared/* 4308*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27072_gshared/* 4309*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27073_gshared/* 4310*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27074_gshared/* 4311*/,
	(methodPointerType)&Dictionary_2__ctor_m27130_gshared/* 4312*/,
	(methodPointerType)&Dictionary_2__ctor_m27133_gshared/* 4313*/,
	(methodPointerType)&Dictionary_2__ctor_m27135_gshared/* 4314*/,
	(methodPointerType)&Dictionary_2__ctor_m27137_gshared/* 4315*/,
	(methodPointerType)&Dictionary_2__ctor_m27139_gshared/* 4316*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27141_gshared/* 4317*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27143_gshared/* 4318*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m27145_gshared/* 4319*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m27147_gshared/* 4320*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m27149_gshared/* 4321*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m27151_gshared/* 4322*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m27153_gshared/* 4323*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27155_gshared/* 4324*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27157_gshared/* 4325*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27159_gshared/* 4326*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27161_gshared/* 4327*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27163_gshared/* 4328*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27165_gshared/* 4329*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27167_gshared/* 4330*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m27169_gshared/* 4331*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27171_gshared/* 4332*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27173_gshared/* 4333*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27175_gshared/* 4334*/,
	(methodPointerType)&Dictionary_2_get_Count_m27177_gshared/* 4335*/,
	(methodPointerType)&Dictionary_2_get_Item_m27179_gshared/* 4336*/,
	(methodPointerType)&Dictionary_2_set_Item_m27181_gshared/* 4337*/,
	(methodPointerType)&Dictionary_2_Init_m27183_gshared/* 4338*/,
	(methodPointerType)&Dictionary_2_InitArrays_m27185_gshared/* 4339*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m27187_gshared/* 4340*/,
	(methodPointerType)&Dictionary_2_make_pair_m27189_gshared/* 4341*/,
	(methodPointerType)&Dictionary_2_pick_key_m27191_gshared/* 4342*/,
	(methodPointerType)&Dictionary_2_pick_value_m27193_gshared/* 4343*/,
	(methodPointerType)&Dictionary_2_CopyTo_m27195_gshared/* 4344*/,
	(methodPointerType)&Dictionary_2_Resize_m27197_gshared/* 4345*/,
	(methodPointerType)&Dictionary_2_Add_m27199_gshared/* 4346*/,
	(methodPointerType)&Dictionary_2_Clear_m27201_gshared/* 4347*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m27203_gshared/* 4348*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m27205_gshared/* 4349*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m27207_gshared/* 4350*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m27209_gshared/* 4351*/,
	(methodPointerType)&Dictionary_2_Remove_m27211_gshared/* 4352*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m27213_gshared/* 4353*/,
	(methodPointerType)&Dictionary_2_get_Keys_m27215_gshared/* 4354*/,
	(methodPointerType)&Dictionary_2_get_Values_m27217_gshared/* 4355*/,
	(methodPointerType)&Dictionary_2_ToTKey_m27219_gshared/* 4356*/,
	(methodPointerType)&Dictionary_2_ToTValue_m27221_gshared/* 4357*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m27223_gshared/* 4358*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m27225_gshared/* 4359*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m27227_gshared/* 4360*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27228_gshared/* 4361*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27229_gshared/* 4362*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27230_gshared/* 4363*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27231_gshared/* 4364*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27232_gshared/* 4365*/,
	(methodPointerType)&KeyValuePair_2__ctor_m27233_gshared/* 4366*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m27234_gshared/* 4367*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m27235_gshared/* 4368*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m27236_gshared/* 4369*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m27237_gshared/* 4370*/,
	(methodPointerType)&KeyValuePair_2_ToString_m27238_gshared/* 4371*/,
	(methodPointerType)&KeyCollection__ctor_m27239_gshared/* 4372*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27240_gshared/* 4373*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27241_gshared/* 4374*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27242_gshared/* 4375*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27243_gshared/* 4376*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27244_gshared/* 4377*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m27245_gshared/* 4378*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27246_gshared/* 4379*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27247_gshared/* 4380*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27248_gshared/* 4381*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m27249_gshared/* 4382*/,
	(methodPointerType)&KeyCollection_CopyTo_m27250_gshared/* 4383*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m27251_gshared/* 4384*/,
	(methodPointerType)&KeyCollection_get_Count_m27252_gshared/* 4385*/,
	(methodPointerType)&Enumerator__ctor_m27253_gshared/* 4386*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27254_gshared/* 4387*/,
	(methodPointerType)&Enumerator_Dispose_m27255_gshared/* 4388*/,
	(methodPointerType)&Enumerator_MoveNext_m27256_gshared/* 4389*/,
	(methodPointerType)&Enumerator_get_Current_m27257_gshared/* 4390*/,
	(methodPointerType)&Enumerator__ctor_m27258_gshared/* 4391*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27259_gshared/* 4392*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27260_gshared/* 4393*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27261_gshared/* 4394*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27262_gshared/* 4395*/,
	(methodPointerType)&Enumerator_MoveNext_m27263_gshared/* 4396*/,
	(methodPointerType)&Enumerator_get_Current_m27264_gshared/* 4397*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m27265_gshared/* 4398*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m27266_gshared/* 4399*/,
	(methodPointerType)&Enumerator_VerifyState_m27267_gshared/* 4400*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m27268_gshared/* 4401*/,
	(methodPointerType)&Enumerator_Dispose_m27269_gshared/* 4402*/,
	(methodPointerType)&Transform_1__ctor_m27270_gshared/* 4403*/,
	(methodPointerType)&Transform_1_Invoke_m27271_gshared/* 4404*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27272_gshared/* 4405*/,
	(methodPointerType)&Transform_1_EndInvoke_m27273_gshared/* 4406*/,
	(methodPointerType)&ValueCollection__ctor_m27274_gshared/* 4407*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27275_gshared/* 4408*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27276_gshared/* 4409*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27277_gshared/* 4410*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27278_gshared/* 4411*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27279_gshared/* 4412*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m27280_gshared/* 4413*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27281_gshared/* 4414*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27282_gshared/* 4415*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27283_gshared/* 4416*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m27284_gshared/* 4417*/,
	(methodPointerType)&ValueCollection_CopyTo_m27285_gshared/* 4418*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m27286_gshared/* 4419*/,
	(methodPointerType)&ValueCollection_get_Count_m27287_gshared/* 4420*/,
	(methodPointerType)&Enumerator__ctor_m27288_gshared/* 4421*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27289_gshared/* 4422*/,
	(methodPointerType)&Enumerator_Dispose_m27290_gshared/* 4423*/,
	(methodPointerType)&Enumerator_MoveNext_m27291_gshared/* 4424*/,
	(methodPointerType)&Enumerator_get_Current_m27292_gshared/* 4425*/,
	(methodPointerType)&Transform_1__ctor_m27293_gshared/* 4426*/,
	(methodPointerType)&Transform_1_Invoke_m27294_gshared/* 4427*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27295_gshared/* 4428*/,
	(methodPointerType)&Transform_1_EndInvoke_m27296_gshared/* 4429*/,
	(methodPointerType)&Transform_1__ctor_m27297_gshared/* 4430*/,
	(methodPointerType)&Transform_1_Invoke_m27298_gshared/* 4431*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27299_gshared/* 4432*/,
	(methodPointerType)&Transform_1_EndInvoke_m27300_gshared/* 4433*/,
	(methodPointerType)&Transform_1__ctor_m27301_gshared/* 4434*/,
	(methodPointerType)&Transform_1_Invoke_m27302_gshared/* 4435*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27303_gshared/* 4436*/,
	(methodPointerType)&Transform_1_EndInvoke_m27304_gshared/* 4437*/,
	(methodPointerType)&ShimEnumerator__ctor_m27305_gshared/* 4438*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m27306_gshared/* 4439*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m27307_gshared/* 4440*/,
	(methodPointerType)&ShimEnumerator_get_Key_m27308_gshared/* 4441*/,
	(methodPointerType)&ShimEnumerator_get_Value_m27309_gshared/* 4442*/,
	(methodPointerType)&ShimEnumerator_get_Current_m27310_gshared/* 4443*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27311_gshared/* 4444*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27312_gshared/* 4445*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27313_gshared/* 4446*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27314_gshared/* 4447*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27315_gshared/* 4448*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m27316_gshared/* 4449*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27317_gshared/* 4450*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27318_gshared/* 4451*/,
	(methodPointerType)&DefaultComparer__ctor_m27319_gshared/* 4452*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27320_gshared/* 4453*/,
	(methodPointerType)&DefaultComparer_Equals_m27321_gshared/* 4454*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27372_gshared/* 4455*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27373_gshared/* 4456*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27374_gshared/* 4457*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27375_gshared/* 4458*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27376_gshared/* 4459*/,
	(methodPointerType)&Dictionary_2__ctor_m27387_gshared/* 4460*/,
	(methodPointerType)&Dictionary_2__ctor_m27388_gshared/* 4461*/,
	(methodPointerType)&Dictionary_2__ctor_m27389_gshared/* 4462*/,
	(methodPointerType)&Dictionary_2__ctor_m27390_gshared/* 4463*/,
	(methodPointerType)&Dictionary_2__ctor_m27391_gshared/* 4464*/,
	(methodPointerType)&Dictionary_2__ctor_m27392_gshared/* 4465*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27393_gshared/* 4466*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27394_gshared/* 4467*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m27395_gshared/* 4468*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m27396_gshared/* 4469*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m27397_gshared/* 4470*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m27398_gshared/* 4471*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m27399_gshared/* 4472*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27400_gshared/* 4473*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27401_gshared/* 4474*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27402_gshared/* 4475*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27403_gshared/* 4476*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27404_gshared/* 4477*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27405_gshared/* 4478*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27406_gshared/* 4479*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m27407_gshared/* 4480*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27408_gshared/* 4481*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27409_gshared/* 4482*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27410_gshared/* 4483*/,
	(methodPointerType)&Dictionary_2_get_Count_m27411_gshared/* 4484*/,
	(methodPointerType)&Dictionary_2_get_Item_m27412_gshared/* 4485*/,
	(methodPointerType)&Dictionary_2_set_Item_m27413_gshared/* 4486*/,
	(methodPointerType)&Dictionary_2_Init_m27414_gshared/* 4487*/,
	(methodPointerType)&Dictionary_2_InitArrays_m27415_gshared/* 4488*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m27416_gshared/* 4489*/,
	(methodPointerType)&Dictionary_2_make_pair_m27417_gshared/* 4490*/,
	(methodPointerType)&Dictionary_2_pick_key_m27418_gshared/* 4491*/,
	(methodPointerType)&Dictionary_2_pick_value_m27419_gshared/* 4492*/,
	(methodPointerType)&Dictionary_2_CopyTo_m27420_gshared/* 4493*/,
	(methodPointerType)&Dictionary_2_Resize_m27421_gshared/* 4494*/,
	(methodPointerType)&Dictionary_2_Add_m27422_gshared/* 4495*/,
	(methodPointerType)&Dictionary_2_Clear_m27423_gshared/* 4496*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m27424_gshared/* 4497*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m27425_gshared/* 4498*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m27426_gshared/* 4499*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m27427_gshared/* 4500*/,
	(methodPointerType)&Dictionary_2_Remove_m27428_gshared/* 4501*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m27429_gshared/* 4502*/,
	(methodPointerType)&Dictionary_2_get_Keys_m27430_gshared/* 4503*/,
	(methodPointerType)&Dictionary_2_get_Values_m27431_gshared/* 4504*/,
	(methodPointerType)&Dictionary_2_ToTKey_m27432_gshared/* 4505*/,
	(methodPointerType)&Dictionary_2_ToTValue_m27433_gshared/* 4506*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m27434_gshared/* 4507*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m27435_gshared/* 4508*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m27436_gshared/* 4509*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27437_gshared/* 4510*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27438_gshared/* 4511*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27439_gshared/* 4512*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27440_gshared/* 4513*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27441_gshared/* 4514*/,
	(methodPointerType)&KeyValuePair_2__ctor_m27442_gshared/* 4515*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m27443_gshared/* 4516*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m27444_gshared/* 4517*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m27445_gshared/* 4518*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m27446_gshared/* 4519*/,
	(methodPointerType)&KeyValuePair_2_ToString_m27447_gshared/* 4520*/,
	(methodPointerType)&KeyCollection__ctor_m27448_gshared/* 4521*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27449_gshared/* 4522*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27450_gshared/* 4523*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27451_gshared/* 4524*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27452_gshared/* 4525*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27453_gshared/* 4526*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m27454_gshared/* 4527*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27455_gshared/* 4528*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27456_gshared/* 4529*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27457_gshared/* 4530*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m27458_gshared/* 4531*/,
	(methodPointerType)&KeyCollection_CopyTo_m27459_gshared/* 4532*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m27460_gshared/* 4533*/,
	(methodPointerType)&KeyCollection_get_Count_m27461_gshared/* 4534*/,
	(methodPointerType)&Enumerator__ctor_m27462_gshared/* 4535*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27463_gshared/* 4536*/,
	(methodPointerType)&Enumerator_Dispose_m27464_gshared/* 4537*/,
	(methodPointerType)&Enumerator_MoveNext_m27465_gshared/* 4538*/,
	(methodPointerType)&Enumerator_get_Current_m27466_gshared/* 4539*/,
	(methodPointerType)&Enumerator__ctor_m27467_gshared/* 4540*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27468_gshared/* 4541*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27469_gshared/* 4542*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27470_gshared/* 4543*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27471_gshared/* 4544*/,
	(methodPointerType)&Enumerator_MoveNext_m27472_gshared/* 4545*/,
	(methodPointerType)&Enumerator_get_Current_m27473_gshared/* 4546*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m27474_gshared/* 4547*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m27475_gshared/* 4548*/,
	(methodPointerType)&Enumerator_VerifyState_m27476_gshared/* 4549*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m27477_gshared/* 4550*/,
	(methodPointerType)&Enumerator_Dispose_m27478_gshared/* 4551*/,
	(methodPointerType)&Transform_1__ctor_m27479_gshared/* 4552*/,
	(methodPointerType)&Transform_1_Invoke_m27480_gshared/* 4553*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27481_gshared/* 4554*/,
	(methodPointerType)&Transform_1_EndInvoke_m27482_gshared/* 4555*/,
	(methodPointerType)&ValueCollection__ctor_m27483_gshared/* 4556*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27484_gshared/* 4557*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27485_gshared/* 4558*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27486_gshared/* 4559*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27487_gshared/* 4560*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27488_gshared/* 4561*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m27489_gshared/* 4562*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27490_gshared/* 4563*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27491_gshared/* 4564*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27492_gshared/* 4565*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m27493_gshared/* 4566*/,
	(methodPointerType)&ValueCollection_CopyTo_m27494_gshared/* 4567*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m27495_gshared/* 4568*/,
	(methodPointerType)&ValueCollection_get_Count_m27496_gshared/* 4569*/,
	(methodPointerType)&Enumerator__ctor_m27497_gshared/* 4570*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27498_gshared/* 4571*/,
	(methodPointerType)&Enumerator_Dispose_m27499_gshared/* 4572*/,
	(methodPointerType)&Enumerator_MoveNext_m27500_gshared/* 4573*/,
	(methodPointerType)&Enumerator_get_Current_m27501_gshared/* 4574*/,
	(methodPointerType)&Transform_1__ctor_m27502_gshared/* 4575*/,
	(methodPointerType)&Transform_1_Invoke_m27503_gshared/* 4576*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27504_gshared/* 4577*/,
	(methodPointerType)&Transform_1_EndInvoke_m27505_gshared/* 4578*/,
	(methodPointerType)&Transform_1__ctor_m27506_gshared/* 4579*/,
	(methodPointerType)&Transform_1_Invoke_m27507_gshared/* 4580*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27508_gshared/* 4581*/,
	(methodPointerType)&Transform_1_EndInvoke_m27509_gshared/* 4582*/,
	(methodPointerType)&ShimEnumerator__ctor_m27510_gshared/* 4583*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m27511_gshared/* 4584*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m27512_gshared/* 4585*/,
	(methodPointerType)&ShimEnumerator_get_Key_m27513_gshared/* 4586*/,
	(methodPointerType)&ShimEnumerator_get_Value_m27514_gshared/* 4587*/,
	(methodPointerType)&ShimEnumerator_get_Current_m27515_gshared/* 4588*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27516_gshared/* 4589*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27517_gshared/* 4590*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27518_gshared/* 4591*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27519_gshared/* 4592*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27520_gshared/* 4593*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27521_gshared/* 4594*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27522_gshared/* 4595*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27523_gshared/* 4596*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27524_gshared/* 4597*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27525_gshared/* 4598*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27531_gshared/* 4599*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27532_gshared/* 4600*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27533_gshared/* 4601*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27534_gshared/* 4602*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27535_gshared/* 4603*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27536_gshared/* 4604*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27537_gshared/* 4605*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27538_gshared/* 4606*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27539_gshared/* 4607*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27540_gshared/* 4608*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27566_gshared/* 4609*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27567_gshared/* 4610*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27568_gshared/* 4611*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27569_gshared/* 4612*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27570_gshared/* 4613*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27596_gshared/* 4614*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27597_gshared/* 4615*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27598_gshared/* 4616*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27599_gshared/* 4617*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27600_gshared/* 4618*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27601_gshared/* 4619*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27602_gshared/* 4620*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27603_gshared/* 4621*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27604_gshared/* 4622*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27605_gshared/* 4623*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27674_gshared/* 4624*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27675_gshared/* 4625*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27676_gshared/* 4626*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27677_gshared/* 4627*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27678_gshared/* 4628*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27679_gshared/* 4629*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27680_gshared/* 4630*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27681_gshared/* 4631*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27682_gshared/* 4632*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27683_gshared/* 4633*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27684_gshared/* 4634*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27685_gshared/* 4635*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27686_gshared/* 4636*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27687_gshared/* 4637*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27688_gshared/* 4638*/,
	(methodPointerType)&GenericComparer_1_Compare_m27792_gshared/* 4639*/,
	(methodPointerType)&Comparer_1__ctor_m27793_gshared/* 4640*/,
	(methodPointerType)&Comparer_1__cctor_m27794_gshared/* 4641*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27795_gshared/* 4642*/,
	(methodPointerType)&Comparer_1_get_Default_m27796_gshared/* 4643*/,
	(methodPointerType)&DefaultComparer__ctor_m27797_gshared/* 4644*/,
	(methodPointerType)&DefaultComparer_Compare_m27798_gshared/* 4645*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27799_gshared/* 4646*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27800_gshared/* 4647*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27801_gshared/* 4648*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27802_gshared/* 4649*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27803_gshared/* 4650*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27804_gshared/* 4651*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27805_gshared/* 4652*/,
	(methodPointerType)&DefaultComparer__ctor_m27806_gshared/* 4653*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27807_gshared/* 4654*/,
	(methodPointerType)&DefaultComparer_Equals_m27808_gshared/* 4655*/,
	(methodPointerType)&GenericComparer_1_Compare_m27809_gshared/* 4656*/,
	(methodPointerType)&Comparer_1__ctor_m27810_gshared/* 4657*/,
	(methodPointerType)&Comparer_1__cctor_m27811_gshared/* 4658*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27812_gshared/* 4659*/,
	(methodPointerType)&Comparer_1_get_Default_m27813_gshared/* 4660*/,
	(methodPointerType)&DefaultComparer__ctor_m27814_gshared/* 4661*/,
	(methodPointerType)&DefaultComparer_Compare_m27815_gshared/* 4662*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27816_gshared/* 4663*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27817_gshared/* 4664*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27818_gshared/* 4665*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27819_gshared/* 4666*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27820_gshared/* 4667*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27821_gshared/* 4668*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27822_gshared/* 4669*/,
	(methodPointerType)&DefaultComparer__ctor_m27823_gshared/* 4670*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27824_gshared/* 4671*/,
	(methodPointerType)&DefaultComparer_Equals_m27825_gshared/* 4672*/,
	(methodPointerType)&Nullable_1_Equals_m27826_gshared/* 4673*/,
	(methodPointerType)&Nullable_1_Equals_m27827_gshared/* 4674*/,
	(methodPointerType)&Nullable_1_GetHashCode_m27828_gshared/* 4675*/,
	(methodPointerType)&Nullable_1_ToString_m27829_gshared/* 4676*/,
	(methodPointerType)&GenericComparer_1_Compare_m27830_gshared/* 4677*/,
	(methodPointerType)&Comparer_1__ctor_m27831_gshared/* 4678*/,
	(methodPointerType)&Comparer_1__cctor_m27832_gshared/* 4679*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27833_gshared/* 4680*/,
	(methodPointerType)&Comparer_1_get_Default_m27834_gshared/* 4681*/,
	(methodPointerType)&DefaultComparer__ctor_m27835_gshared/* 4682*/,
	(methodPointerType)&DefaultComparer_Compare_m27836_gshared/* 4683*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27837_gshared/* 4684*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27838_gshared/* 4685*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27839_gshared/* 4686*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27840_gshared/* 4687*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27841_gshared/* 4688*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27842_gshared/* 4689*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27843_gshared/* 4690*/,
	(methodPointerType)&DefaultComparer__ctor_m27844_gshared/* 4691*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27845_gshared/* 4692*/,
	(methodPointerType)&DefaultComparer_Equals_m27846_gshared/* 4693*/,
	(methodPointerType)&GenericComparer_1_Compare_m27847_gshared/* 4694*/,
	(methodPointerType)&Comparer_1__ctor_m27848_gshared/* 4695*/,
	(methodPointerType)&Comparer_1__cctor_m27849_gshared/* 4696*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27850_gshared/* 4697*/,
	(methodPointerType)&Comparer_1_get_Default_m27851_gshared/* 4698*/,
	(methodPointerType)&DefaultComparer__ctor_m27852_gshared/* 4699*/,
	(methodPointerType)&DefaultComparer_Compare_m27853_gshared/* 4700*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27854_gshared/* 4701*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27855_gshared/* 4702*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27856_gshared/* 4703*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27857_gshared/* 4704*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27858_gshared/* 4705*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27859_gshared/* 4706*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27860_gshared/* 4707*/,
	(methodPointerType)&DefaultComparer__ctor_m27861_gshared/* 4708*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27862_gshared/* 4709*/,
	(methodPointerType)&DefaultComparer_Equals_m27863_gshared/* 4710*/,
};
const InvokerMethod g_Il2CppInvokerPointers[676] = 
{
	NULL/* 0*/,
	RuntimeInvoker_Object_t_Object_t/* 1*/,
	RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t/* 2*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* 3*/,
	RuntimeInvoker_Boolean_t176_Object_t/* 4*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t/* 5*/,
	RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* 6*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* 7*/,
	RuntimeInvoker_Void_t175_Object_t/* 8*/,
	RuntimeInvoker_Boolean_t176_ObjectU26_t1522_Object_t/* 9*/,
	RuntimeInvoker_Void_t175_ObjectU26_t1522_Object_t/* 10*/,
	RuntimeInvoker_Int32_t135/* 11*/,
	RuntimeInvoker_Boolean_t176/* 12*/,
	RuntimeInvoker_Object_t_Int32_t135/* 13*/,
	RuntimeInvoker_Void_t175_Int32_t135_Object_t/* 14*/,
	RuntimeInvoker_Void_t175/* 15*/,
	RuntimeInvoker_Object_t/* 16*/,
	RuntimeInvoker_Void_t175_Object_t_Int32_t135/* 17*/,
	RuntimeInvoker_Int32_t135_Object_t/* 18*/,
	RuntimeInvoker_Void_t175_Int32_t135/* 19*/,
	RuntimeInvoker_Object_t_Object_t_Object_t/* 20*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135/* 21*/,
	RuntimeInvoker_Object_t_SByte_t177/* 22*/,
	RuntimeInvoker_Void_t175_SByte_t177_Object_t/* 23*/,
	RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522/* 24*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3114/* 25*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3114/* 26*/,
	RuntimeInvoker_Boolean_t176_Object_t_Object_t/* 27*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t/* 28*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 29*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t/* 30*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 31*/,
	RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* 32*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_Int32_t135_Object_t/* 33*/,
	RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135/* 34*/,
	RuntimeInvoker_Int32_t135_Int32_t135/* 35*/,
	RuntimeInvoker_Enumerator_t3669/* 36*/,
	RuntimeInvoker_Boolean_t176_Int32_t135/* 37*/,
	RuntimeInvoker_Enumerator_t3941/* 38*/,
	RuntimeInvoker_Enumerator_t3177/* 39*/,
	RuntimeInvoker_Void_t175_Int32_t135_ObjectU26_t1522/* 40*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Int32_t135/* 41*/,
	RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135_Object_t/* 42*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135_Int32_t135_Object_t/* 43*/,
	RuntimeInvoker_Void_t175_Object_t_Int32_t135_Object_t/* 44*/,
	RuntimeInvoker_Int32_t135_Object_t_Object_t_Object_t/* 45*/,
	RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Int32_t135/* 46*/,
	RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Int32_t135_Int32_t135/* 47*/,
	RuntimeInvoker_Int32_t135_Object_t_Object_t/* 48*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Object_t/* 49*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Object_t/* 50*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Object_t_Object_t/* 51*/,
	RuntimeInvoker_Int32_t135_Object_t_Object_t_Int32_t135/* 52*/,
	RuntimeInvoker_Int32_t135_Object_t_Object_t_Int32_t135_Int32_t135/* 53*/,
	RuntimeInvoker_KeyValuePair_2_t3114_Object_t_Object_t/* 54*/,
	RuntimeInvoker_Enumerator_t3121/* 55*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_Object_t/* 56*/,
	RuntimeInvoker_DictionaryEntry_t2002/* 57*/,
	RuntimeInvoker_KeyValuePair_2_t3114/* 58*/,
	RuntimeInvoker_Enumerator_t3120/* 59*/,
	RuntimeInvoker_Enumerator_t3124/* 60*/,
	RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135_Object_t/* 61*/,
	RuntimeInvoker_Enumerator_t3147/* 62*/,
	RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* 63*/,
	RuntimeInvoker_Void_t175_SByte_t177/* 64*/,
	RuntimeInvoker_Enumerator_t3232/* 65*/,
	RuntimeInvoker_Enumerator_t3229/* 66*/,
	RuntimeInvoker_KeyValuePair_2_t3225/* 67*/,
	RuntimeInvoker_Void_t175_Color_t98/* 68*/,
	RuntimeInvoker_Void_t175_ColorTween_t260/* 69*/,
	RuntimeInvoker_Boolean_t176_Int32U26_t541_Int32_t135/* 70*/,
	RuntimeInvoker_Boolean_t176_ByteU26_t1846_SByte_t177/* 71*/,
	RuntimeInvoker_Boolean_t176_SingleU26_t930_Single_t112/* 72*/,
	RuntimeInvoker_Boolean_t176_UInt16U26_t2726_Int16_t540/* 73*/,
	RuntimeInvoker_Void_t175_Single_t112/* 74*/,
	RuntimeInvoker_Void_t175_Vector2_t19/* 75*/,
	RuntimeInvoker_Boolean_t176_NavigationU26_t4560_Navigation_t326/* 76*/,
	RuntimeInvoker_Boolean_t176_ColorBlockU26_t4561_ColorBlock_t272/* 77*/,
	RuntimeInvoker_Boolean_t176_SpriteStateU26_t4562_SpriteState_t345/* 78*/,
	RuntimeInvoker_Void_t175_Int32U26_t541_Int32_t135/* 79*/,
	RuntimeInvoker_Void_t175_Vector2U26_t929_Vector2_t19/* 80*/,
	RuntimeInvoker_Void_t175_SingleU26_t930_Single_t112/* 81*/,
	RuntimeInvoker_Void_t175_ByteU26_t1846_SByte_t177/* 82*/,
	RuntimeInvoker_Enumerator_t822/* 83*/,
	RuntimeInvoker_Byte_t455/* 84*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112_Object_t/* 85*/,
	RuntimeInvoker_Int32_t135_Object_t_Int16_t540/* 86*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Single_t112/* 87*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_Int32_t135/* 88*/,
	RuntimeInvoker_Void_t175_Object_t_Object_t_SByte_t177/* 89*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Int32_t135/* 90*/,
	RuntimeInvoker_Void_t175_TimeSpan_t121/* 91*/,
	RuntimeInvoker_TimeSpan_t121/* 92*/,
	RuntimeInvoker_KeyValuePair_2_t3114_Int32_t135/* 93*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3114/* 94*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3114/* 95*/,
	RuntimeInvoker_Link_t2142_Int32_t135/* 96*/,
	RuntimeInvoker_Void_t175_Link_t2142/* 97*/,
	RuntimeInvoker_Boolean_t176_Link_t2142/* 98*/,
	RuntimeInvoker_Int32_t135_Link_t2142/* 99*/,
	RuntimeInvoker_Void_t175_Int32_t135_Link_t2142/* 100*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int32_t135/* 101*/,
	RuntimeInvoker_Void_t175_DictionaryEntry_t2002/* 102*/,
	RuntimeInvoker_Boolean_t176_DictionaryEntry_t2002/* 103*/,
	RuntimeInvoker_Int32_t135_DictionaryEntry_t2002/* 104*/,
	RuntimeInvoker_Void_t175_Int32_t135_DictionaryEntry_t2002/* 105*/,
	RuntimeInvoker_UInt16_t460_Int32_t135/* 106*/,
	RuntimeInvoker_Void_t175_Int16_t540/* 107*/,
	RuntimeInvoker_Boolean_t176_Int16_t540/* 108*/,
	RuntimeInvoker_Int32_t135_Int16_t540/* 109*/,
	RuntimeInvoker_Void_t175_Int32_t135_Int16_t540/* 110*/,
	RuntimeInvoker_Single_t112_Object_t_Single_t112/* 111*/,
	RuntimeInvoker_Vector3_t14_Int32_t135/* 112*/,
	RuntimeInvoker_Void_t175_Vector3_t14/* 113*/,
	RuntimeInvoker_Boolean_t176_Vector3_t14/* 114*/,
	RuntimeInvoker_Int32_t135_Vector3_t14/* 115*/,
	RuntimeInvoker_Void_t175_Int32_t135_Vector3_t14/* 116*/,
	RuntimeInvoker_Double_t1413_Int32_t135/* 117*/,
	RuntimeInvoker_Void_t175_Double_t1413/* 118*/,
	RuntimeInvoker_Boolean_t176_Double_t1413/* 119*/,
	RuntimeInvoker_Int32_t135_Double_t1413/* 120*/,
	RuntimeInvoker_Void_t175_Int32_t135_Double_t1413/* 121*/,
	RuntimeInvoker_RaycastResult_t238_Int32_t135/* 122*/,
	RuntimeInvoker_Void_t175_RaycastResult_t238/* 123*/,
	RuntimeInvoker_Boolean_t176_RaycastResult_t238/* 124*/,
	RuntimeInvoker_Int32_t135_RaycastResult_t238/* 125*/,
	RuntimeInvoker_Void_t175_Int32_t135_RaycastResult_t238/* 126*/,
	RuntimeInvoker_Void_t175_RaycastResultU5BU5DU26_t4563_Int32_t135/* 127*/,
	RuntimeInvoker_Void_t175_RaycastResultU5BU5DU26_t4563_Int32_t135_Int32_t135/* 128*/,
	RuntimeInvoker_Int32_t135_Object_t_RaycastResult_t238_Int32_t135_Int32_t135/* 129*/,
	RuntimeInvoker_Int32_t135_RaycastResult_t238_RaycastResult_t238_Object_t/* 130*/,
	RuntimeInvoker_KeyValuePair_2_t3225_Int32_t135/* 131*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3225/* 132*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3225/* 133*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3225/* 134*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3225/* 135*/,
	RuntimeInvoker_RaycastHit2D_t438_Int32_t135/* 136*/,
	RuntimeInvoker_Void_t175_RaycastHit2D_t438/* 137*/,
	RuntimeInvoker_Boolean_t176_RaycastHit2D_t438/* 138*/,
	RuntimeInvoker_Int32_t135_RaycastHit2D_t438/* 139*/,
	RuntimeInvoker_Void_t175_Int32_t135_RaycastHit2D_t438/* 140*/,
	RuntimeInvoker_RaycastHit_t102_Int32_t135/* 141*/,
	RuntimeInvoker_Void_t175_RaycastHit_t102/* 142*/,
	RuntimeInvoker_Boolean_t176_RaycastHit_t102/* 143*/,
	RuntimeInvoker_Int32_t135_RaycastHit_t102/* 144*/,
	RuntimeInvoker_Void_t175_Int32_t135_RaycastHit_t102/* 145*/,
	RuntimeInvoker_KeyValuePair_2_t3253_Int32_t135/* 146*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3253/* 147*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3253/* 148*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3253/* 149*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3253/* 150*/,
	RuntimeInvoker_Void_t175_UIVertexU5BU5DU26_t4564_Int32_t135/* 151*/,
	RuntimeInvoker_Void_t175_UIVertexU5BU5DU26_t4564_Int32_t135_Int32_t135/* 152*/,
	RuntimeInvoker_Int32_t135_Object_t_UIVertex_t319_Int32_t135_Int32_t135/* 153*/,
	RuntimeInvoker_Int32_t135_UIVertex_t319_UIVertex_t319_Object_t/* 154*/,
	RuntimeInvoker_Vector2_t19_Int32_t135/* 155*/,
	RuntimeInvoker_Boolean_t176_Vector2_t19/* 156*/,
	RuntimeInvoker_Int32_t135_Vector2_t19/* 157*/,
	RuntimeInvoker_Void_t175_Int32_t135_Vector2_t19/* 158*/,
	RuntimeInvoker_UILineInfo_t464_Int32_t135/* 159*/,
	RuntimeInvoker_Void_t175_UILineInfo_t464/* 160*/,
	RuntimeInvoker_Boolean_t176_UILineInfo_t464/* 161*/,
	RuntimeInvoker_Int32_t135_UILineInfo_t464/* 162*/,
	RuntimeInvoker_Void_t175_Int32_t135_UILineInfo_t464/* 163*/,
	RuntimeInvoker_UICharInfo_t466_Int32_t135/* 164*/,
	RuntimeInvoker_Void_t175_UICharInfo_t466/* 165*/,
	RuntimeInvoker_Boolean_t176_UICharInfo_t466/* 166*/,
	RuntimeInvoker_Int32_t135_UICharInfo_t466/* 167*/,
	RuntimeInvoker_Void_t175_Int32_t135_UICharInfo_t466/* 168*/,
	RuntimeInvoker_Single_t112_Int32_t135/* 169*/,
	RuntimeInvoker_Boolean_t176_Single_t112/* 170*/,
	RuntimeInvoker_Int32_t135_Single_t112/* 171*/,
	RuntimeInvoker_Void_t175_Int32_t135_Single_t112/* 172*/,
	RuntimeInvoker_EyewearCalibrationReading_t592_Int32_t135/* 173*/,
	RuntimeInvoker_Void_t175_EyewearCalibrationReading_t592/* 174*/,
	RuntimeInvoker_Boolean_t176_EyewearCalibrationReading_t592/* 175*/,
	RuntimeInvoker_Int32_t135_EyewearCalibrationReading_t592/* 176*/,
	RuntimeInvoker_Void_t175_Int32_t135_EyewearCalibrationReading_t592/* 177*/,
	RuntimeInvoker_Void_t175_Int32U5BU5DU26_t4565_Int32_t135/* 178*/,
	RuntimeInvoker_Void_t175_Int32U5BU5DU26_t4565_Int32_t135_Int32_t135/* 179*/,
	RuntimeInvoker_Byte_t455_Int32_t135/* 180*/,
	RuntimeInvoker_Boolean_t176_SByte_t177/* 181*/,
	RuntimeInvoker_Int32_t135_SByte_t177/* 182*/,
	RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* 183*/,
	RuntimeInvoker_Color32_t427_Int32_t135/* 184*/,
	RuntimeInvoker_Void_t175_Color32_t427/* 185*/,
	RuntimeInvoker_Boolean_t176_Color32_t427/* 186*/,
	RuntimeInvoker_Int32_t135_Color32_t427/* 187*/,
	RuntimeInvoker_Void_t175_Int32_t135_Color32_t427/* 188*/,
	RuntimeInvoker_Color_t98_Int32_t135/* 189*/,
	RuntimeInvoker_Boolean_t176_Color_t98/* 190*/,
	RuntimeInvoker_Int32_t135_Color_t98/* 191*/,
	RuntimeInvoker_Void_t175_Int32_t135_Color_t98/* 192*/,
	RuntimeInvoker_TrackableResultData_t648_Int32_t135/* 193*/,
	RuntimeInvoker_Void_t175_TrackableResultData_t648/* 194*/,
	RuntimeInvoker_Boolean_t176_TrackableResultData_t648/* 195*/,
	RuntimeInvoker_Int32_t135_TrackableResultData_t648/* 196*/,
	RuntimeInvoker_Void_t175_Int32_t135_TrackableResultData_t648/* 197*/,
	RuntimeInvoker_WordData_t653_Int32_t135/* 198*/,
	RuntimeInvoker_Void_t175_WordData_t653/* 199*/,
	RuntimeInvoker_Boolean_t176_WordData_t653/* 200*/,
	RuntimeInvoker_Int32_t135_WordData_t653/* 201*/,
	RuntimeInvoker_Void_t175_Int32_t135_WordData_t653/* 202*/,
	RuntimeInvoker_WordResultData_t652_Int32_t135/* 203*/,
	RuntimeInvoker_Void_t175_WordResultData_t652/* 204*/,
	RuntimeInvoker_Boolean_t176_WordResultData_t652/* 205*/,
	RuntimeInvoker_Int32_t135_WordResultData_t652/* 206*/,
	RuntimeInvoker_Void_t175_Int32_t135_WordResultData_t652/* 207*/,
	RuntimeInvoker_SmartTerrainRevisionData_t656_Int32_t135/* 208*/,
	RuntimeInvoker_Void_t175_SmartTerrainRevisionData_t656/* 209*/,
	RuntimeInvoker_Boolean_t176_SmartTerrainRevisionData_t656/* 210*/,
	RuntimeInvoker_Int32_t135_SmartTerrainRevisionData_t656/* 211*/,
	RuntimeInvoker_Void_t175_Int32_t135_SmartTerrainRevisionData_t656/* 212*/,
	RuntimeInvoker_SurfaceData_t657_Int32_t135/* 213*/,
	RuntimeInvoker_Void_t175_SurfaceData_t657/* 214*/,
	RuntimeInvoker_Boolean_t176_SurfaceData_t657/* 215*/,
	RuntimeInvoker_Int32_t135_SurfaceData_t657/* 216*/,
	RuntimeInvoker_Void_t175_Int32_t135_SurfaceData_t657/* 217*/,
	RuntimeInvoker_PropData_t658_Int32_t135/* 218*/,
	RuntimeInvoker_Void_t175_PropData_t658/* 219*/,
	RuntimeInvoker_Boolean_t176_PropData_t658/* 220*/,
	RuntimeInvoker_Int32_t135_PropData_t658/* 221*/,
	RuntimeInvoker_Void_t175_Int32_t135_PropData_t658/* 222*/,
	RuntimeInvoker_KeyValuePair_2_t3475_Int32_t135/* 223*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3475/* 224*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3475/* 225*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3475/* 226*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3475/* 227*/,
	RuntimeInvoker_RectangleData_t602_Int32_t135/* 228*/,
	RuntimeInvoker_Void_t175_RectangleData_t602/* 229*/,
	RuntimeInvoker_Boolean_t176_RectangleData_t602/* 230*/,
	RuntimeInvoker_Int32_t135_RectangleData_t602/* 231*/,
	RuntimeInvoker_Void_t175_Int32_t135_RectangleData_t602/* 232*/,
	RuntimeInvoker_KeyValuePair_2_t3563_Int32_t135/* 233*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3563/* 234*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3563/* 235*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3563/* 236*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3563/* 237*/,
	RuntimeInvoker_KeyValuePair_2_t3578_Int32_t135/* 238*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3578/* 239*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3578/* 240*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3578/* 241*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3578/* 242*/,
	RuntimeInvoker_VirtualButtonData_t649_Int32_t135/* 243*/,
	RuntimeInvoker_Void_t175_VirtualButtonData_t649/* 244*/,
	RuntimeInvoker_Boolean_t176_VirtualButtonData_t649/* 245*/,
	RuntimeInvoker_Int32_t135_VirtualButtonData_t649/* 246*/,
	RuntimeInvoker_Void_t175_Int32_t135_VirtualButtonData_t649/* 247*/,
	RuntimeInvoker_TargetSearchResult_t726_Int32_t135/* 248*/,
	RuntimeInvoker_Void_t175_TargetSearchResult_t726/* 249*/,
	RuntimeInvoker_Boolean_t176_TargetSearchResult_t726/* 250*/,
	RuntimeInvoker_Int32_t135_TargetSearchResult_t726/* 251*/,
	RuntimeInvoker_Void_t175_Int32_t135_TargetSearchResult_t726/* 252*/,
	RuntimeInvoker_Void_t175_TargetSearchResultU5BU5DU26_t4566_Int32_t135/* 253*/,
	RuntimeInvoker_Void_t175_TargetSearchResultU5BU5DU26_t4566_Int32_t135_Int32_t135/* 254*/,
	RuntimeInvoker_Int32_t135_Object_t_TargetSearchResult_t726_Int32_t135_Int32_t135/* 255*/,
	RuntimeInvoker_Int32_t135_TargetSearchResult_t726_TargetSearchResult_t726_Object_t/* 256*/,
	RuntimeInvoker_WebCamDevice_t885_Int32_t135/* 257*/,
	RuntimeInvoker_Void_t175_WebCamDevice_t885/* 258*/,
	RuntimeInvoker_Boolean_t176_WebCamDevice_t885/* 259*/,
	RuntimeInvoker_Int32_t135_WebCamDevice_t885/* 260*/,
	RuntimeInvoker_Void_t175_Int32_t135_WebCamDevice_t885/* 261*/,
	RuntimeInvoker_KeyValuePair_2_t3617_Int32_t135/* 262*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3617/* 263*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3617/* 264*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3617/* 265*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3617/* 266*/,
	RuntimeInvoker_ProfileData_t740_Int32_t135/* 267*/,
	RuntimeInvoker_Void_t175_ProfileData_t740/* 268*/,
	RuntimeInvoker_Boolean_t176_ProfileData_t740/* 269*/,
	RuntimeInvoker_Int32_t135_ProfileData_t740/* 270*/,
	RuntimeInvoker_Void_t175_Int32_t135_ProfileData_t740/* 271*/,
	RuntimeInvoker_Link_t3666_Int32_t135/* 272*/,
	RuntimeInvoker_Void_t175_Link_t3666/* 273*/,
	RuntimeInvoker_Boolean_t176_Link_t3666/* 274*/,
	RuntimeInvoker_Int32_t135_Link_t3666/* 275*/,
	RuntimeInvoker_Void_t175_Int32_t135_Link_t3666/* 276*/,
	RuntimeInvoker_Boolean_t176_Object_t_Object_t_Object_t_Vector3_t14_Single_t112_Object_t/* 277*/,
	RuntimeInvoker_Void_t175_UInt16U5BU5DU26_t4567_Int32_t135/* 278*/,
	RuntimeInvoker_Void_t175_UInt16U5BU5DU26_t4567_Int32_t135_Int32_t135/* 279*/,
	RuntimeInvoker_Int32_t135_Object_t_Int16_t540_Int32_t135_Int32_t135/* 280*/,
	RuntimeInvoker_Int32_t135_Int16_t540_Int16_t540_Object_t/* 281*/,
	RuntimeInvoker_GcAchievementData_t1311_Int32_t135/* 282*/,
	RuntimeInvoker_Void_t175_GcAchievementData_t1311/* 283*/,
	RuntimeInvoker_Boolean_t176_GcAchievementData_t1311/* 284*/,
	RuntimeInvoker_Int32_t135_GcAchievementData_t1311/* 285*/,
	RuntimeInvoker_Void_t175_Int32_t135_GcAchievementData_t1311/* 286*/,
	RuntimeInvoker_GcScoreData_t1312_Int32_t135/* 287*/,
	RuntimeInvoker_Void_t175_GcScoreData_t1312/* 288*/,
	RuntimeInvoker_Boolean_t176_GcScoreData_t1312/* 289*/,
	RuntimeInvoker_Int32_t135_GcScoreData_t1312/* 290*/,
	RuntimeInvoker_Void_t175_Int32_t135_GcScoreData_t1312/* 291*/,
	RuntimeInvoker_IntPtr_t_Int32_t135/* 292*/,
	RuntimeInvoker_Void_t175_IntPtr_t/* 293*/,
	RuntimeInvoker_Boolean_t176_IntPtr_t/* 294*/,
	RuntimeInvoker_Int32_t135_IntPtr_t/* 295*/,
	RuntimeInvoker_Void_t175_Int32_t135_IntPtr_t/* 296*/,
	RuntimeInvoker_Keyframe_t1242_Int32_t135/* 297*/,
	RuntimeInvoker_Void_t175_Keyframe_t1242/* 298*/,
	RuntimeInvoker_Boolean_t176_Keyframe_t1242/* 299*/,
	RuntimeInvoker_Int32_t135_Keyframe_t1242/* 300*/,
	RuntimeInvoker_Void_t175_Int32_t135_Keyframe_t1242/* 301*/,
	RuntimeInvoker_Void_t175_UICharInfoU5BU5DU26_t4568_Int32_t135/* 302*/,
	RuntimeInvoker_Void_t175_UICharInfoU5BU5DU26_t4568_Int32_t135_Int32_t135/* 303*/,
	RuntimeInvoker_Int32_t135_Object_t_UICharInfo_t466_Int32_t135_Int32_t135/* 304*/,
	RuntimeInvoker_Int32_t135_UICharInfo_t466_UICharInfo_t466_Object_t/* 305*/,
	RuntimeInvoker_Void_t175_UILineInfoU5BU5DU26_t4569_Int32_t135/* 306*/,
	RuntimeInvoker_Void_t175_UILineInfoU5BU5DU26_t4569_Int32_t135_Int32_t135/* 307*/,
	RuntimeInvoker_Int32_t135_Object_t_UILineInfo_t464_Int32_t135_Int32_t135/* 308*/,
	RuntimeInvoker_Int32_t135_UILineInfo_t464_UILineInfo_t464_Object_t/* 309*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Int32_t135/* 310*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3790/* 311*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3790/* 312*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3790/* 313*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3790/* 314*/,
	RuntimeInvoker_Int64_t1098_Int32_t135/* 315*/,
	RuntimeInvoker_Void_t175_Int64_t1098/* 316*/,
	RuntimeInvoker_Boolean_t176_Int64_t1098/* 317*/,
	RuntimeInvoker_Int32_t135_Int64_t1098/* 318*/,
	RuntimeInvoker_Void_t175_Int32_t135_Int64_t1098/* 319*/,
	RuntimeInvoker_KeyValuePair_2_t3828_Int32_t135/* 320*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3828/* 321*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3828/* 322*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3828/* 323*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3828/* 324*/,
	RuntimeInvoker_UInt64_t1097_Int32_t135/* 325*/,
	RuntimeInvoker_KeyValuePair_2_t3849_Int32_t135/* 326*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3849/* 327*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3849/* 328*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3849/* 329*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3849/* 330*/,
	RuntimeInvoker_ParameterModifier_t2266_Int32_t135/* 331*/,
	RuntimeInvoker_Void_t175_ParameterModifier_t2266/* 332*/,
	RuntimeInvoker_Boolean_t176_ParameterModifier_t2266/* 333*/,
	RuntimeInvoker_Int32_t135_ParameterModifier_t2266/* 334*/,
	RuntimeInvoker_Void_t175_Int32_t135_ParameterModifier_t2266/* 335*/,
	RuntimeInvoker_HitInfo_t1329_Int32_t135/* 336*/,
	RuntimeInvoker_Void_t175_HitInfo_t1329/* 337*/,
	RuntimeInvoker_Boolean_t176_HitInfo_t1329/* 338*/,
	RuntimeInvoker_Int32_t135_HitInfo_t1329/* 339*/,
	RuntimeInvoker_Void_t175_Int32_t135_HitInfo_t1329/* 340*/,
	RuntimeInvoker_UInt32_t1081_Int32_t135/* 341*/,
	RuntimeInvoker_KeyValuePair_2_t3945_Int32_t135/* 342*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3945/* 343*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3945/* 344*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3945/* 345*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3945/* 346*/,
	RuntimeInvoker_X509ChainStatus_t1908_Int32_t135/* 347*/,
	RuntimeInvoker_Void_t175_X509ChainStatus_t1908/* 348*/,
	RuntimeInvoker_Boolean_t176_X509ChainStatus_t1908/* 349*/,
	RuntimeInvoker_Int32_t135_X509ChainStatus_t1908/* 350*/,
	RuntimeInvoker_Void_t175_Int32_t135_X509ChainStatus_t1908/* 351*/,
	RuntimeInvoker_KeyValuePair_2_t3967_Int32_t135/* 352*/,
	RuntimeInvoker_Void_t175_KeyValuePair_2_t3967/* 353*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3967/* 354*/,
	RuntimeInvoker_Int32_t135_KeyValuePair_2_t3967/* 355*/,
	RuntimeInvoker_Void_t175_Int32_t135_KeyValuePair_2_t3967/* 356*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135_Int32_t135_Int32_t135_Object_t/* 357*/,
	RuntimeInvoker_Mark_t1954_Int32_t135/* 358*/,
	RuntimeInvoker_Void_t175_Mark_t1954/* 359*/,
	RuntimeInvoker_Boolean_t176_Mark_t1954/* 360*/,
	RuntimeInvoker_Int32_t135_Mark_t1954/* 361*/,
	RuntimeInvoker_Void_t175_Int32_t135_Mark_t1954/* 362*/,
	RuntimeInvoker_UriScheme_t1990_Int32_t135/* 363*/,
	RuntimeInvoker_Void_t175_UriScheme_t1990/* 364*/,
	RuntimeInvoker_Boolean_t176_UriScheme_t1990/* 365*/,
	RuntimeInvoker_Int32_t135_UriScheme_t1990/* 366*/,
	RuntimeInvoker_Void_t175_Int32_t135_UriScheme_t1990/* 367*/,
	RuntimeInvoker_Int16_t540_Int32_t135/* 368*/,
	RuntimeInvoker_SByte_t177_Int32_t135/* 369*/,
	RuntimeInvoker_TableRange_t2075_Int32_t135/* 370*/,
	RuntimeInvoker_Void_t175_TableRange_t2075/* 371*/,
	RuntimeInvoker_Boolean_t176_TableRange_t2075/* 372*/,
	RuntimeInvoker_Int32_t135_TableRange_t2075/* 373*/,
	RuntimeInvoker_Void_t175_Int32_t135_TableRange_t2075/* 374*/,
	RuntimeInvoker_Slot_t2152_Int32_t135/* 375*/,
	RuntimeInvoker_Void_t175_Slot_t2152/* 376*/,
	RuntimeInvoker_Boolean_t176_Slot_t2152/* 377*/,
	RuntimeInvoker_Int32_t135_Slot_t2152/* 378*/,
	RuntimeInvoker_Void_t175_Int32_t135_Slot_t2152/* 379*/,
	RuntimeInvoker_Slot_t2159_Int32_t135/* 380*/,
	RuntimeInvoker_Void_t175_Slot_t2159/* 381*/,
	RuntimeInvoker_Boolean_t176_Slot_t2159/* 382*/,
	RuntimeInvoker_Int32_t135_Slot_t2159/* 383*/,
	RuntimeInvoker_Void_t175_Int32_t135_Slot_t2159/* 384*/,
	RuntimeInvoker_DateTime_t120_Int32_t135/* 385*/,
	RuntimeInvoker_Void_t175_DateTime_t120/* 386*/,
	RuntimeInvoker_Boolean_t176_DateTime_t120/* 387*/,
	RuntimeInvoker_Int32_t135_DateTime_t120/* 388*/,
	RuntimeInvoker_Void_t175_Int32_t135_DateTime_t120/* 389*/,
	RuntimeInvoker_Decimal_t1065_Int32_t135/* 390*/,
	RuntimeInvoker_Void_t175_Decimal_t1065/* 391*/,
	RuntimeInvoker_Boolean_t176_Decimal_t1065/* 392*/,
	RuntimeInvoker_Int32_t135_Decimal_t1065/* 393*/,
	RuntimeInvoker_Void_t175_Int32_t135_Decimal_t1065/* 394*/,
	RuntimeInvoker_TimeSpan_t121_Int32_t135/* 395*/,
	RuntimeInvoker_Boolean_t176_TimeSpan_t121/* 396*/,
	RuntimeInvoker_Int32_t135_TimeSpan_t121/* 397*/,
	RuntimeInvoker_Void_t175_Int32_t135_TimeSpan_t121/* 398*/,
	RuntimeInvoker_Link_t2142/* 399*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t/* 400*/,
	RuntimeInvoker_KeyValuePair_2_t3114_Object_t/* 401*/,
	RuntimeInvoker_UInt16_t460/* 402*/,
	RuntimeInvoker_Boolean_t176_Nullable_1_t3140/* 403*/,
	RuntimeInvoker_Boolean_t176_Nullable_1_t3141/* 404*/,
	RuntimeInvoker_Vector3_t14/* 405*/,
	RuntimeInvoker_Vector3_t14_Object_t/* 406*/,
	RuntimeInvoker_Object_t_Vector3_t14_Object_t_Object_t/* 407*/,
	RuntimeInvoker_Single_t112_Single_t112/* 408*/,
	RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135/* 409*/,
	RuntimeInvoker_Quaternion_t22/* 410*/,
	RuntimeInvoker_Quaternion_t22_Object_t/* 411*/,
	RuntimeInvoker_Void_t175_Quaternion_t22/* 412*/,
	RuntimeInvoker_Object_t_Quaternion_t22_Object_t_Object_t/* 413*/,
	RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t/* 414*/,
	RuntimeInvoker_Double_t1413/* 415*/,
	RuntimeInvoker_Int32_t135_RaycastResult_t238_RaycastResult_t238/* 416*/,
	RuntimeInvoker_Object_t_RaycastResult_t238_RaycastResult_t238_Object_t_Object_t/* 417*/,
	RuntimeInvoker_RaycastResult_t238_Object_t/* 418*/,
	RuntimeInvoker_Enumerator_t3186/* 419*/,
	RuntimeInvoker_RaycastResult_t238/* 420*/,
	RuntimeInvoker_Boolean_t176_RaycastResult_t238_RaycastResult_t238/* 421*/,
	RuntimeInvoker_Object_t_RaycastResult_t238_Object_t_Object_t/* 422*/,
	RuntimeInvoker_KeyValuePair_2_t3225_Int32_t135_Object_t/* 423*/,
	RuntimeInvoker_Int32_t135_Int32_t135_Object_t/* 424*/,
	RuntimeInvoker_Object_t_Int32_t135_Object_t/* 425*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_ObjectU26_t1522/* 426*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_Object_t/* 427*/,
	RuntimeInvoker_Enumerator_t3228/* 428*/,
	RuntimeInvoker_Object_t_Int32_t135_Object_t_Object_t_Object_t/* 429*/,
	RuntimeInvoker_KeyValuePair_2_t3225_Object_t/* 430*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_Int32_t135/* 431*/,
	RuntimeInvoker_RaycastHit2D_t438/* 432*/,
	RuntimeInvoker_Int32_t135_RaycastHit_t102_RaycastHit_t102/* 433*/,
	RuntimeInvoker_Object_t_RaycastHit_t102_RaycastHit_t102_Object_t_Object_t/* 434*/,
	RuntimeInvoker_RaycastHit_t102/* 435*/,
	RuntimeInvoker_Object_t_Color_t98_Object_t_Object_t/* 436*/,
	RuntimeInvoker_KeyValuePair_2_t3253_Object_t_Int32_t135/* 437*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t135/* 438*/,
	RuntimeInvoker_Int32_t135_Object_t_Int32_t135/* 439*/,
	RuntimeInvoker_Boolean_t176_Object_t_Int32U26_t541/* 440*/,
	RuntimeInvoker_Enumerator_t3257/* 441*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int32_t135/* 442*/,
	RuntimeInvoker_KeyValuePair_2_t3253/* 443*/,
	RuntimeInvoker_Enumerator_t3256/* 444*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t/* 445*/,
	RuntimeInvoker_Enumerator_t3260/* 446*/,
	RuntimeInvoker_KeyValuePair_2_t3253_Object_t/* 447*/,
	RuntimeInvoker_Void_t175_UIVertex_t319/* 448*/,
	RuntimeInvoker_Boolean_t176_UIVertex_t319/* 449*/,
	RuntimeInvoker_UIVertex_t319_Object_t/* 450*/,
	RuntimeInvoker_Enumerator_t3284/* 451*/,
	RuntimeInvoker_Int32_t135_UIVertex_t319/* 452*/,
	RuntimeInvoker_Void_t175_Int32_t135_UIVertex_t319/* 453*/,
	RuntimeInvoker_UIVertex_t319_Int32_t135/* 454*/,
	RuntimeInvoker_UIVertex_t319/* 455*/,
	RuntimeInvoker_Boolean_t176_UIVertex_t319_UIVertex_t319/* 456*/,
	RuntimeInvoker_Object_t_UIVertex_t319_Object_t_Object_t/* 457*/,
	RuntimeInvoker_Int32_t135_UIVertex_t319_UIVertex_t319/* 458*/,
	RuntimeInvoker_Object_t_UIVertex_t319_UIVertex_t319_Object_t_Object_t/* 459*/,
	RuntimeInvoker_Object_t_ColorTween_t260/* 460*/,
	RuntimeInvoker_Vector2_t19/* 461*/,
	RuntimeInvoker_UILineInfo_t464/* 462*/,
	RuntimeInvoker_UICharInfo_t466/* 463*/,
	RuntimeInvoker_Object_t_Single_t112_Object_t_Object_t/* 464*/,
	RuntimeInvoker_Object_t_Vector2_t19_Object_t_Object_t/* 465*/,
	RuntimeInvoker_Object_t_SByte_t177_Object_t_Object_t/* 466*/,
	RuntimeInvoker_Byte_t455_Object_t/* 467*/,
	RuntimeInvoker_Single_t112_Object_t/* 468*/,
	RuntimeInvoker_Single_t112/* 469*/,
	RuntimeInvoker_EyewearCalibrationReading_t592/* 470*/,
	RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135/* 471*/,
	RuntimeInvoker_Object_t_Int32_t135_Int32_t135_Object_t_Object_t/* 472*/,
	RuntimeInvoker_Color32_t427/* 473*/,
	RuntimeInvoker_Color_t98/* 474*/,
	RuntimeInvoker_TrackableResultData_t648/* 475*/,
	RuntimeInvoker_WordData_t653/* 476*/,
	RuntimeInvoker_WordResultData_t652/* 477*/,
	RuntimeInvoker_Enumerator_t3455/* 478*/,
	RuntimeInvoker_Void_t175_Object_t_Int32_t135_Object_t_Object_t/* 479*/,
	RuntimeInvoker_Object_t_TrackableResultData_t648_Object_t_Object_t/* 480*/,
	RuntimeInvoker_SmartTerrainRevisionData_t656/* 481*/,
	RuntimeInvoker_SurfaceData_t657/* 482*/,
	RuntimeInvoker_PropData_t658/* 483*/,
	RuntimeInvoker_UInt16_t460_Object_t/* 484*/,
	RuntimeInvoker_Void_t175_Object_t_Int16_t540/* 485*/,
	RuntimeInvoker_KeyValuePair_2_t3475_Object_t_Int16_t540/* 486*/,
	RuntimeInvoker_Object_t_Object_t_Int16_t540/* 487*/,
	RuntimeInvoker_UInt16_t460_Object_t_Int16_t540/* 488*/,
	RuntimeInvoker_Boolean_t176_Object_t_UInt16U26_t2726/* 489*/,
	RuntimeInvoker_Enumerator_t3479/* 490*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int16_t540/* 491*/,
	RuntimeInvoker_KeyValuePair_2_t3475/* 492*/,
	RuntimeInvoker_Enumerator_t3478/* 493*/,
	RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t_Object_t/* 494*/,
	RuntimeInvoker_Enumerator_t3482/* 495*/,
	RuntimeInvoker_KeyValuePair_2_t3475_Object_t/* 496*/,
	RuntimeInvoker_Boolean_t176_Int16_t540_Int16_t540/* 497*/,
	RuntimeInvoker_RectangleData_t602/* 498*/,
	RuntimeInvoker_Void_t175_SmartTerrainInitializationInfo_t594/* 499*/,
	RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t594_Object_t_Object_t/* 500*/,
	RuntimeInvoker_KeyValuePair_2_t3563_Int32_t135_TrackableResultData_t648/* 501*/,
	RuntimeInvoker_Int32_t135_Int32_t135_TrackableResultData_t648/* 502*/,
	RuntimeInvoker_TrackableResultData_t648_Int32_t135_TrackableResultData_t648/* 503*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_TrackableResultDataU26_t4570/* 504*/,
	RuntimeInvoker_TrackableResultData_t648_Object_t/* 505*/,
	RuntimeInvoker_Enumerator_t3567/* 506*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_TrackableResultData_t648/* 507*/,
	RuntimeInvoker_KeyValuePair_2_t3563/* 508*/,
	RuntimeInvoker_Enumerator_t3566/* 509*/,
	RuntimeInvoker_Object_t_Int32_t135_TrackableResultData_t648_Object_t_Object_t/* 510*/,
	RuntimeInvoker_Enumerator_t3570/* 511*/,
	RuntimeInvoker_KeyValuePair_2_t3563_Object_t/* 512*/,
	RuntimeInvoker_Boolean_t176_TrackableResultData_t648_TrackableResultData_t648/* 513*/,
	RuntimeInvoker_KeyValuePair_2_t3578_Int32_t135_VirtualButtonData_t649/* 514*/,
	RuntimeInvoker_Int32_t135_Int32_t135_VirtualButtonData_t649/* 515*/,
	RuntimeInvoker_VirtualButtonData_t649_Int32_t135_VirtualButtonData_t649/* 516*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_VirtualButtonDataU26_t4571/* 517*/,
	RuntimeInvoker_VirtualButtonData_t649_Object_t/* 518*/,
	RuntimeInvoker_Enumerator_t3583/* 519*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_VirtualButtonData_t649/* 520*/,
	RuntimeInvoker_KeyValuePair_2_t3578/* 521*/,
	RuntimeInvoker_VirtualButtonData_t649/* 522*/,
	RuntimeInvoker_Enumerator_t3582/* 523*/,
	RuntimeInvoker_Object_t_Int32_t135_VirtualButtonData_t649_Object_t_Object_t/* 524*/,
	RuntimeInvoker_Enumerator_t3586/* 525*/,
	RuntimeInvoker_KeyValuePair_2_t3578_Object_t/* 526*/,
	RuntimeInvoker_Boolean_t176_VirtualButtonData_t649_VirtualButtonData_t649/* 527*/,
	RuntimeInvoker_TargetSearchResult_t726_Object_t/* 528*/,
	RuntimeInvoker_Enumerator_t3598/* 529*/,
	RuntimeInvoker_TargetSearchResult_t726/* 530*/,
	RuntimeInvoker_Boolean_t176_TargetSearchResult_t726_TargetSearchResult_t726/* 531*/,
	RuntimeInvoker_Object_t_TargetSearchResult_t726_Object_t_Object_t/* 532*/,
	RuntimeInvoker_Int32_t135_TargetSearchResult_t726_TargetSearchResult_t726/* 533*/,
	RuntimeInvoker_Object_t_TargetSearchResult_t726_TargetSearchResult_t726_Object_t_Object_t/* 534*/,
	RuntimeInvoker_WebCamDevice_t885/* 535*/,
	RuntimeInvoker_ProfileData_t740_Object_t/* 536*/,
	RuntimeInvoker_Void_t175_Object_t_ProfileData_t740/* 537*/,
	RuntimeInvoker_KeyValuePair_2_t3617_Object_t_ProfileData_t740/* 538*/,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t740/* 539*/,
	RuntimeInvoker_ProfileData_t740_Object_t_ProfileData_t740/* 540*/,
	RuntimeInvoker_Boolean_t176_Object_t_ProfileDataU26_t4572/* 541*/,
	RuntimeInvoker_Enumerator_t3622/* 542*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_ProfileData_t740/* 543*/,
	RuntimeInvoker_KeyValuePair_2_t3617/* 544*/,
	RuntimeInvoker_ProfileData_t740/* 545*/,
	RuntimeInvoker_Enumerator_t3621/* 546*/,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t740_Object_t_Object_t/* 547*/,
	RuntimeInvoker_Enumerator_t3625/* 548*/,
	RuntimeInvoker_KeyValuePair_2_t3617_Object_t/* 549*/,
	RuntimeInvoker_Boolean_t176_ProfileData_t740_ProfileData_t740/* 550*/,
	RuntimeInvoker_Link_t3666/* 551*/,
	RuntimeInvoker_UInt32_t1081/* 552*/,
	RuntimeInvoker_UInt32_t1081_Object_t/* 553*/,
	RuntimeInvoker_Vector2_t19_Object_t/* 554*/,
	RuntimeInvoker_Color2_t1006/* 555*/,
	RuntimeInvoker_Color2_t1006_Object_t/* 556*/,
	RuntimeInvoker_Void_t175_Color2_t1006/* 557*/,
	RuntimeInvoker_Object_t_Color2_t1006_Object_t_Object_t/* 558*/,
	RuntimeInvoker_Rect_t132/* 559*/,
	RuntimeInvoker_Rect_t132_Object_t/* 560*/,
	RuntimeInvoker_Void_t175_Rect_t132/* 561*/,
	RuntimeInvoker_Object_t_Rect_t132_Object_t_Object_t/* 562*/,
	RuntimeInvoker_UInt64_t1097/* 563*/,
	RuntimeInvoker_UInt64_t1097_Object_t/* 564*/,
	RuntimeInvoker_Object_t_Int64_t1098_Object_t_Object_t/* 565*/,
	RuntimeInvoker_Enumerator_t3701/* 566*/,
	RuntimeInvoker_Object_t_Int16_t540_Object_t_Object_t/* 567*/,
	RuntimeInvoker_Int32_t135_Int16_t540_Int16_t540/* 568*/,
	RuntimeInvoker_Object_t_Int16_t540_Int16_t540_Object_t_Object_t/* 569*/,
	RuntimeInvoker_Vector4_t419/* 570*/,
	RuntimeInvoker_Vector4_t419_Object_t/* 571*/,
	RuntimeInvoker_Void_t175_Vector4_t419/* 572*/,
	RuntimeInvoker_Object_t_Vector4_t419_Object_t_Object_t/* 573*/,
	RuntimeInvoker_Color_t98_Object_t/* 574*/,
	RuntimeInvoker_Int64_t1098/* 575*/,
	RuntimeInvoker_Int64_t1098_Object_t/* 576*/,
	RuntimeInvoker_GcAchievementData_t1311/* 577*/,
	RuntimeInvoker_GcScoreData_t1312/* 578*/,
	RuntimeInvoker_IntPtr_t/* 579*/,
	RuntimeInvoker_Keyframe_t1242/* 580*/,
	RuntimeInvoker_UICharInfo_t466_Object_t/* 581*/,
	RuntimeInvoker_Enumerator_t3769/* 582*/,
	RuntimeInvoker_Boolean_t176_UICharInfo_t466_UICharInfo_t466/* 583*/,
	RuntimeInvoker_Object_t_UICharInfo_t466_Object_t_Object_t/* 584*/,
	RuntimeInvoker_Int32_t135_UICharInfo_t466_UICharInfo_t466/* 585*/,
	RuntimeInvoker_Object_t_UICharInfo_t466_UICharInfo_t466_Object_t_Object_t/* 586*/,
	RuntimeInvoker_UILineInfo_t464_Object_t/* 587*/,
	RuntimeInvoker_Enumerator_t3778/* 588*/,
	RuntimeInvoker_Boolean_t176_UILineInfo_t464_UILineInfo_t464/* 589*/,
	RuntimeInvoker_Object_t_UILineInfo_t464_Object_t_Object_t/* 590*/,
	RuntimeInvoker_Int32_t135_UILineInfo_t464_UILineInfo_t464/* 591*/,
	RuntimeInvoker_Object_t_UILineInfo_t464_UILineInfo_t464_Object_t_Object_t/* 592*/,
	RuntimeInvoker_Void_t175_Object_t_Int64_t1098/* 593*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Object_t_Int64_t1098/* 594*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1098/* 595*/,
	RuntimeInvoker_Int64_t1098_Object_t_Int64_t1098/* 596*/,
	RuntimeInvoker_Boolean_t176_Object_t_Int64U26_t2709/* 597*/,
	RuntimeInvoker_Enumerator_t3795/* 598*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_Int64_t1098/* 599*/,
	RuntimeInvoker_KeyValuePair_2_t3790/* 600*/,
	RuntimeInvoker_Enumerator_t3794/* 601*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t_Object_t/* 602*/,
	RuntimeInvoker_Enumerator_t3798/* 603*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Object_t/* 604*/,
	RuntimeInvoker_Boolean_t176_Int64_t1098_Int64_t1098/* 605*/,
	RuntimeInvoker_Object_t_Int64_t1098/* 606*/,
	RuntimeInvoker_Void_t175_Int64_t1098_Object_t/* 607*/,
	RuntimeInvoker_KeyValuePair_2_t3828_Int64_t1098_Object_t/* 608*/,
	RuntimeInvoker_UInt64_t1097_Int64_t1098_Object_t/* 609*/,
	RuntimeInvoker_Object_t_Int64_t1098_Object_t/* 610*/,
	RuntimeInvoker_Boolean_t176_Int64_t1098_ObjectU26_t1522/* 611*/,
	RuntimeInvoker_Enumerator_t3833/* 612*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int64_t1098_Object_t/* 613*/,
	RuntimeInvoker_KeyValuePair_2_t3828/* 614*/,
	RuntimeInvoker_Enumerator_t3832/* 615*/,
	RuntimeInvoker_Object_t_Int64_t1098_Object_t_Object_t_Object_t/* 616*/,
	RuntimeInvoker_Enumerator_t3836/* 617*/,
	RuntimeInvoker_KeyValuePair_2_t3828_Object_t/* 618*/,
	RuntimeInvoker_KeyValuePair_2_t3849/* 619*/,
	RuntimeInvoker_Void_t175_Object_t_KeyValuePair_2_t3114/* 620*/,
	RuntimeInvoker_KeyValuePair_2_t3849_Object_t_KeyValuePair_2_t3114/* 621*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3114/* 622*/,
	RuntimeInvoker_KeyValuePair_2_t3114_Object_t_KeyValuePair_2_t3114/* 623*/,
	RuntimeInvoker_Boolean_t176_Object_t_KeyValuePair_2U26_t4573/* 624*/,
	RuntimeInvoker_Enumerator_t3877/* 625*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_KeyValuePair_2_t3114/* 626*/,
	RuntimeInvoker_Enumerator_t3876/* 627*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3114_Object_t_Object_t/* 628*/,
	RuntimeInvoker_Enumerator_t3880/* 629*/,
	RuntimeInvoker_KeyValuePair_2_t3849_Object_t/* 630*/,
	RuntimeInvoker_Boolean_t176_KeyValuePair_2_t3114_KeyValuePair_2_t3114/* 631*/,
	RuntimeInvoker_ParameterModifier_t2266/* 632*/,
	RuntimeInvoker_HitInfo_t1329/* 633*/,
	RuntimeInvoker_Void_t175_Object_t_SByte_t177/* 634*/,
	RuntimeInvoker_KeyValuePair_2_t3945_Object_t_SByte_t177/* 635*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t177/* 636*/,
	RuntimeInvoker_Byte_t455_Object_t_SByte_t177/* 637*/,
	RuntimeInvoker_Boolean_t176_Object_t_ByteU26_t1846/* 638*/,
	RuntimeInvoker_Enumerator_t3949/* 639*/,
	RuntimeInvoker_DictionaryEntry_t2002_Object_t_SByte_t177/* 640*/,
	RuntimeInvoker_KeyValuePair_2_t3945/* 641*/,
	RuntimeInvoker_Enumerator_t3948/* 642*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t_Object_t/* 643*/,
	RuntimeInvoker_Enumerator_t3952/* 644*/,
	RuntimeInvoker_KeyValuePair_2_t3945_Object_t/* 645*/,
	RuntimeInvoker_Boolean_t176_SByte_t177_SByte_t177/* 646*/,
	RuntimeInvoker_X509ChainStatus_t1908/* 647*/,
	RuntimeInvoker_KeyValuePair_2_t3967_Int32_t135_Int32_t135/* 648*/,
	RuntimeInvoker_Boolean_t176_Int32_t135_Int32U26_t541/* 649*/,
	RuntimeInvoker_Enumerator_t3971/* 650*/,
	RuntimeInvoker_DictionaryEntry_t2002_Int32_t135_Int32_t135/* 651*/,
	RuntimeInvoker_KeyValuePair_2_t3967/* 652*/,
	RuntimeInvoker_Enumerator_t3970/* 653*/,
	RuntimeInvoker_Enumerator_t3974/* 654*/,
	RuntimeInvoker_KeyValuePair_2_t3967_Object_t/* 655*/,
	RuntimeInvoker_Mark_t1954/* 656*/,
	RuntimeInvoker_UriScheme_t1990/* 657*/,
	RuntimeInvoker_Int16_t540/* 658*/,
	RuntimeInvoker_SByte_t177/* 659*/,
	RuntimeInvoker_TableRange_t2075/* 660*/,
	RuntimeInvoker_Slot_t2152/* 661*/,
	RuntimeInvoker_Slot_t2159/* 662*/,
	RuntimeInvoker_DateTime_t120/* 663*/,
	RuntimeInvoker_Decimal_t1065/* 664*/,
	RuntimeInvoker_Int32_t135_DateTime_t120_DateTime_t120/* 665*/,
	RuntimeInvoker_Boolean_t176_DateTime_t120_DateTime_t120/* 666*/,
	RuntimeInvoker_Int32_t135_DateTimeOffset_t1426_DateTimeOffset_t1426/* 667*/,
	RuntimeInvoker_Int32_t135_DateTimeOffset_t1426/* 668*/,
	RuntimeInvoker_Boolean_t176_DateTimeOffset_t1426_DateTimeOffset_t1426/* 669*/,
	RuntimeInvoker_Boolean_t176_Nullable_1_t2604/* 670*/,
	RuntimeInvoker_Int32_t135_Guid_t118_Guid_t118/* 671*/,
	RuntimeInvoker_Int32_t135_Guid_t118/* 672*/,
	RuntimeInvoker_Boolean_t176_Guid_t118_Guid_t118/* 673*/,
	RuntimeInvoker_Int32_t135_TimeSpan_t121_TimeSpan_t121/* 674*/,
	RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* 675*/,
};
const Il2CppCodeRegistration g_CodeRegistration = 
{
	4711,
	g_Il2CppMethodPointers,
	676,
	g_Il2CppInvokerPointers,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
