﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t3776;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25125_gshared (DefaultComparer_t3776 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25125(__this, method) (( void (*) (DefaultComparer_t3776 *, const MethodInfo*))DefaultComparer__ctor_m25125_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m25126_gshared (DefaultComparer_t3776 * __this, UICharInfo_t466  ___x, UICharInfo_t466  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m25126(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3776 *, UICharInfo_t466 , UICharInfo_t466 , const MethodInfo*))DefaultComparer_Compare_m25126_gshared)(__this, ___x, ___y, method)
