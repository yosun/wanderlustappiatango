﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t66;
// Vuforia.Marker
struct Marker_t745;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.GameObject
struct GameObject_t2;

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
extern "C" Object_t * MarkerAbstractBehaviour_get_Marker_m4131 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
extern "C" void MarkerAbstractBehaviour__ctor_m475 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void MarkerAbstractBehaviour_InternalUnregisterTrackable_m644 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
extern "C" bool MarkerAbstractBehaviour_CorrectScaleImpl_m645 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
extern "C" int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m646 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
extern "C" bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m647 (MarkerAbstractBehaviour_t66 * __this, int32_t ___markerID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
extern "C" void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m648 (MarkerAbstractBehaviour_t66 * __this, Object_t * ___marker, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m639 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m640 (MarkerAbstractBehaviour_t66 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m641 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m642 (MarkerAbstractBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
