﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t235;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t236  : public Object_t
{
};
struct RaycasterManager_t236_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t235 * ___s_Raycasters_0;
};
