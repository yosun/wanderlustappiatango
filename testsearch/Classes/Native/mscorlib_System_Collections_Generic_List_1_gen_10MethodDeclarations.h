﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t321;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t4113;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4110;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t474;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t3282;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t318;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3283;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t3285;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_36.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m2225_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1__ctor_m2225(__this, method) (( void (*) (List_1_t321 *, const MethodInfo*))List_1__ctor_m2225_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17271_gshared (List_1_t321 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17271(__this, ___collection, method) (( void (*) (List_1_t321 *, Object_t*, const MethodInfo*))List_1__ctor_m17271_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m6972_gshared (List_1_t321 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m6972(__this, ___capacity, method) (( void (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1__ctor_m6972_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m17272_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17272(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17272_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17273_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17273(__this, method) (( Object_t* (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17273_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17274_gshared (List_1_t321 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17274(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t321 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17274_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17275_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17275(__this, method) (( Object_t * (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17275_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17276_gshared (List_1_t321 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17276(__this, ___item, method) (( int32_t (*) (List_1_t321 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17276_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17277_gshared (List_1_t321 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17277(__this, ___item, method) (( bool (*) (List_1_t321 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17277_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17278_gshared (List_1_t321 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17278(__this, ___item, method) (( int32_t (*) (List_1_t321 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17278_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17279_gshared (List_1_t321 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17279(__this, ___index, ___item, method) (( void (*) (List_1_t321 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17279_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17280_gshared (List_1_t321 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17280(__this, ___item, method) (( void (*) (List_1_t321 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17280_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17281_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17281(__this, method) (( bool (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17281_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17282_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17282(__this, method) (( bool (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17282_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17283_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17283(__this, method) (( Object_t * (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17284_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17284(__this, method) (( bool (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17284_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17285_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17285(__this, method) (( bool (*) (List_1_t321 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17285_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17286_gshared (List_1_t321 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17286(__this, ___index, method) (( Object_t * (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17286_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17287_gshared (List_1_t321 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17287(__this, ___index, ___value, method) (( void (*) (List_1_t321 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17287_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m17288_gshared (List_1_t321 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define List_1_Add_m17288(__this, ___item, method) (( void (*) (List_1_t321 *, UIVertex_t319 , const MethodInfo*))List_1_Add_m17288_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17289_gshared (List_1_t321 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17289(__this, ___newCount, method) (( void (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17289_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17290_gshared (List_1_t321 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17290(__this, ___collection, method) (( void (*) (List_1_t321 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17290_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17291_gshared (List_1_t321 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17291(__this, ___enumerable, method) (( void (*) (List_1_t321 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17291_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17292_gshared (List_1_t321 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17292(__this, ___collection, method) (( void (*) (List_1_t321 *, Object_t*, const MethodInfo*))List_1_AddRange_m17292_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3282 * List_1_AsReadOnly_m17293_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17293(__this, method) (( ReadOnlyCollection_1_t3282 * (*) (List_1_t321 *, const MethodInfo*))List_1_AsReadOnly_m17293_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m17294_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_Clear_m17294(__this, method) (( void (*) (List_1_t321 *, const MethodInfo*))List_1_Clear_m17294_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m17295_gshared (List_1_t321 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define List_1_Contains_m17295(__this, ___item, method) (( bool (*) (List_1_t321 *, UIVertex_t319 , const MethodInfo*))List_1_Contains_m17295_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17296_gshared (List_1_t321 * __this, UIVertexU5BU5D_t318* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17296(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t321 *, UIVertexU5BU5D_t318*, int32_t, const MethodInfo*))List_1_CopyTo_m17296_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t319  List_1_Find_m17297_gshared (List_1_t321 * __this, Predicate_1_t3283 * ___match, const MethodInfo* method);
#define List_1_Find_m17297(__this, ___match, method) (( UIVertex_t319  (*) (List_1_t321 *, Predicate_1_t3283 *, const MethodInfo*))List_1_Find_m17297_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17298_gshared (Object_t * __this /* static, unused */, Predicate_1_t3283 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17298(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3283 *, const MethodInfo*))List_1_CheckMatch_m17298_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17299_gshared (List_1_t321 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3283 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17299(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t321 *, int32_t, int32_t, Predicate_1_t3283 *, const MethodInfo*))List_1_GetIndex_m17299_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t3284  List_1_GetEnumerator_m17300_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17300(__this, method) (( Enumerator_t3284  (*) (List_1_t321 *, const MethodInfo*))List_1_GetEnumerator_m17300_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17301_gshared (List_1_t321 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17301(__this, ___item, method) (( int32_t (*) (List_1_t321 *, UIVertex_t319 , const MethodInfo*))List_1_IndexOf_m17301_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17302_gshared (List_1_t321 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17302(__this, ___start, ___delta, method) (( void (*) (List_1_t321 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17303_gshared (List_1_t321 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17303(__this, ___index, method) (( void (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17303_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17304_gshared (List_1_t321 * __this, int32_t ___index, UIVertex_t319  ___item, const MethodInfo* method);
#define List_1_Insert_m17304(__this, ___index, ___item, method) (( void (*) (List_1_t321 *, int32_t, UIVertex_t319 , const MethodInfo*))List_1_Insert_m17304_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17305_gshared (List_1_t321 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17305(__this, ___collection, method) (( void (*) (List_1_t321 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17305_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m17306_gshared (List_1_t321 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define List_1_Remove_m17306(__this, ___item, method) (( bool (*) (List_1_t321 *, UIVertex_t319 , const MethodInfo*))List_1_Remove_m17306_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17307_gshared (List_1_t321 * __this, Predicate_1_t3283 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17307(__this, ___match, method) (( int32_t (*) (List_1_t321 *, Predicate_1_t3283 *, const MethodInfo*))List_1_RemoveAll_m17307_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17308_gshared (List_1_t321 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17308(__this, ___index, method) (( void (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17308_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m17309_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_Reverse_m17309(__this, method) (( void (*) (List_1_t321 *, const MethodInfo*))List_1_Reverse_m17309_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m17310_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_Sort_m17310(__this, method) (( void (*) (List_1_t321 *, const MethodInfo*))List_1_Sort_m17310_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17311_gshared (List_1_t321 * __this, Comparison_1_t3285 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17311(__this, ___comparison, method) (( void (*) (List_1_t321 *, Comparison_1_t3285 *, const MethodInfo*))List_1_Sort_m17311_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t318* List_1_ToArray_m2280_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_ToArray_m2280(__this, method) (( UIVertexU5BU5D_t318* (*) (List_1_t321 *, const MethodInfo*))List_1_ToArray_m2280_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m17312_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17312(__this, method) (( void (*) (List_1_t321 *, const MethodInfo*))List_1_TrimExcess_m17312_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m2143_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2143(__this, method) (( int32_t (*) (List_1_t321 *, const MethodInfo*))List_1_get_Capacity_m2143_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m2144_gshared (List_1_t321 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m2144(__this, ___value, method) (( void (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2144_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m17313_gshared (List_1_t321 * __this, const MethodInfo* method);
#define List_1_get_Count_m17313(__this, method) (( int32_t (*) (List_1_t321 *, const MethodInfo*))List_1_get_Count_m17313_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t319  List_1_get_Item_m17314_gshared (List_1_t321 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17314(__this, ___index, method) (( UIVertex_t319  (*) (List_1_t321 *, int32_t, const MethodInfo*))List_1_get_Item_m17314_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17315_gshared (List_1_t321 * __this, int32_t ___index, UIVertex_t319  ___value, const MethodInfo* method);
#define List_1_set_Item_m17315(__this, ___index, ___value, method) (( void (*) (List_1_t321 *, int32_t, UIVertex_t319 , const MethodInfo*))List_1_set_Item_m17315_gshared)(__this, ___index, ___value, method)
