﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t3801;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3789;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m25456_gshared (ShimEnumerator_t3801 * __this, Dictionary_2_t3789 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m25456(__this, ___host, method) (( void (*) (ShimEnumerator_t3801 *, Dictionary_2_t3789 *, const MethodInfo*))ShimEnumerator__ctor_m25456_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m25457_gshared (ShimEnumerator_t3801 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m25457(__this, method) (( bool (*) (ShimEnumerator_t3801 *, const MethodInfo*))ShimEnumerator_MoveNext_m25457_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m25458_gshared (ShimEnumerator_t3801 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m25458(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3801 *, const MethodInfo*))ShimEnumerator_get_Entry_m25458_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m25459_gshared (ShimEnumerator_t3801 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m25459(__this, method) (( Object_t * (*) (ShimEnumerator_t3801 *, const MethodInfo*))ShimEnumerator_get_Key_m25459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m25460_gshared (ShimEnumerator_t3801 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m25460(__this, method) (( Object_t * (*) (ShimEnumerator_t3801 *, const MethodInfo*))ShimEnumerator_get_Value_m25460_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m25461_gshared (ShimEnumerator_t3801 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m25461(__this, method) (( Object_t * (*) (ShimEnumerator_t3801 *, const MethodInfo*))ShimEnumerator_get_Current_m25461_gshared)(__this, method)
