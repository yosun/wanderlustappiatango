﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AnimateTexture
struct AnimateTexture_t18;

// System.Void AnimateTexture::.ctor()
extern "C" void AnimateTexture__ctor_m20 (AnimateTexture_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Awake()
extern "C" void AnimateTexture_Awake_m21 (AnimateTexture_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Update()
extern "C" void AnimateTexture_Update_m22 (AnimateTexture_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
