﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t1054;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24091_gshared (DOSetter_1_t1054 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m24091(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1054 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m24091_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24092_gshared (DOSetter_1_t1054 * __this, Vector4_t419  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m24092(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1054 *, Vector4_t419 , const MethodInfo*))DOSetter_1_Invoke_m24092_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m24093_gshared (DOSetter_1_t1054 * __this, Vector4_t419  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m24093(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1054 *, Vector4_t419 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m24093_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24094_gshared (DOSetter_1_t1054 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m24094(__this, ___result, method) (( void (*) (DOSetter_1_t1054 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m24094_gshared)(__this, ___result, method)
