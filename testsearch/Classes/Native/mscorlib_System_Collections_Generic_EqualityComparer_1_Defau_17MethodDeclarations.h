﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t4035;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m27861_gshared (DefaultComparer_t4035 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27861(__this, method) (( void (*) (DefaultComparer_t4035 *, const MethodInfo*))DefaultComparer__ctor_m27861_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m27862_gshared (DefaultComparer_t4035 * __this, TimeSpan_t121  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m27862(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4035 *, TimeSpan_t121 , const MethodInfo*))DefaultComparer_GetHashCode_m27862_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m27863_gshared (DefaultComparer_t4035 * __this, TimeSpan_t121  ___x, TimeSpan_t121  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m27863(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4035 *, TimeSpan_t121 , TimeSpan_t121 , const MethodInfo*))DefaultComparer_Equals_m27863_gshared)(__this, ___x, ___y, method)
