﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t622;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.IO.FileStream/ReadDelegate
struct  ReadDelegate_t2200  : public MulticastDelegate_t314
{
};
