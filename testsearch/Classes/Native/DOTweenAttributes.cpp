﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t903_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var;
void g_DOTween_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1284);
		DebuggableAttribute_t903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1283);
		AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(498);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(497);
		AssemblyProductAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(492);
		AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(499);
		AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 19;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyCopyrightAttribute_t490 * tmp;
		tmp = (AssemblyCopyrightAttribute_t490 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2453(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Daniele Giardini, 2014"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t904 * tmp;
		tmp = (CompilationRelaxationsAttribute_t904 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m4699(tmp, 8, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t903 * tmp;
		tmp = (DebuggableAttribute_t903 *)il2cpp_codegen_object_new (DebuggableAttribute_t903_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4698(tmp, 2, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t493 * tmp;
		tmp = (AssemblyFileVersionAttribute_t493 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("DOTween46"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("807e068c-2a0e-4c81-a303-4b4fd3924511"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t492 * tmp;
		tmp = (AssemblyTitleAttribute_t492 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper("DOTween"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t489 * tmp;
		tmp = (AssemblyProductAttribute_t489 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t489_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2452(tmp, il2cpp_codegen_string_new_wrapper("DOTween"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t486 * tmp;
		tmp = (AssemblyDescriptionAttribute_t486 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2449(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t487 * tmp;
		tmp = (AssemblyConfigurationAttribute_t487 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m2450(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t494 * tmp;
		tmp = (AssemblyTrademarkAttribute_t494 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t488 * tmp;
		tmp = (AssemblyCompanyAttribute_t488 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2451(tmp, il2cpp_codegen_string_new_wrapper("Demigiant"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("DOTweenProEditor"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("DOTweenPro"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("DOTween43"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("DOTweenEditor"), NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void DOTweenComponent_t941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0__ctor_m5300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2__ctor_m5305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4__ctor_m5310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8__ctor_m5320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a__ctor_m5325(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_m5614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m5615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_m5616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m395(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m5617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenExtensions_t963_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void TweenExtensions_t963_CustomAttributesCacheGenerator_TweenExtensions_Duration_m398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_m388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m262(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_m397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass92_t965_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass96_t966_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass9a_t967_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClassb2_t968_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClassc6_t969_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void StringPluginExtensions_t998_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void StringPluginExtensions_t998_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m5487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void StringPluginExtensions_t998_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m5488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void AxisConstraint_t1007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_DOTween_Assembly_AttributeGenerators[55] = 
{
	NULL,
	g_DOTween_Assembly_CustomAttributesCacheGenerator,
	DOTweenComponent_t941_CustomAttributesCacheGenerator,
	U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator,
	U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297,
	U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299,
	U3CWaitForCompletionU3Ed__0_t942_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0__ctor_m5300,
	U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator,
	U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302,
	U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304,
	U3CWaitForRewindU3Ed__2_t943_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2__ctor_m5305,
	U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator,
	U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307,
	U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309,
	U3CWaitForKillU3Ed__4_t944_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4__ctor_m5310,
	U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator,
	U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312,
	U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314,
	U3CWaitForElapsedLoopsU3Ed__6_t945_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315,
	U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator,
	U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317,
	U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319,
	U3CWaitForPositionU3Ed__8_t946_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8__ctor_m5320,
	U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator,
	U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322,
	U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324,
	U3CWaitForStartU3Ed__a_t947_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a__ctor_m5325,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_m5614,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m5615,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_m5616,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m395,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m396,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m400,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m5617,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5376,
	TweenSettingsExtensions_t108_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5377,
	TweenExtensions_t963_CustomAttributesCacheGenerator,
	TweenExtensions_t963_CustomAttributesCacheGenerator_TweenExtensions_Duration_m398,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_m388,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m399,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m394,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m262,
	ShortcutExtensions_t970_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_m397,
	U3CU3Ec__DisplayClass92_t965_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass96_t966_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9a_t967_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClassb2_t968_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClassc6_t969_CustomAttributesCacheGenerator,
	StringPluginExtensions_t998_CustomAttributesCacheGenerator,
	StringPluginExtensions_t998_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m5487,
	StringPluginExtensions_t998_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m5488,
	AxisConstraint_t1007_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_CustomAttributesCacheGenerator,
};
