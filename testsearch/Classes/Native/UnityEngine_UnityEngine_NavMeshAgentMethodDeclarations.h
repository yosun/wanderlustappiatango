﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t10;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Boolean UnityEngine.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C" bool NavMeshAgent_SetDestination_m284 (NavMeshAgent_t10 * __this, Vector3_t14  ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C" bool NavMeshAgent_INTERNAL_CALL_SetDestination_m6384 (Object_t * __this /* static, unused */, NavMeshAgent_t10 * ___self, Vector3_t14 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_get_destination(UnityEngine.Vector3&)
extern "C" void NavMeshAgent_INTERNAL_get_destination_m6385 (NavMeshAgent_t10 * __this, Vector3_t14 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshAgent::get_destination()
extern "C" Vector3_t14  NavMeshAgent_get_destination_m290 (NavMeshAgent_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
