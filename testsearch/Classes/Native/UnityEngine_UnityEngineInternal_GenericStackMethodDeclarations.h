﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.GenericStack
struct GenericStack_t1173;

// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m6925 (GenericStack_t1173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
