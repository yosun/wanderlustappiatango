﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyValuePair_2_t3849;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m26088_gshared (KeyValuePair_2_t3849 * __this, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m26088(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3849 *, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))KeyValuePair_2__ctor_m26088_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m26089_gshared (KeyValuePair_2_t3849 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m26089(__this, method) (( Object_t * (*) (KeyValuePair_2_t3849 *, const MethodInfo*))KeyValuePair_2_get_Key_m26089_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m26090_gshared (KeyValuePair_2_t3849 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m26090(__this, ___value, method) (( void (*) (KeyValuePair_2_t3849 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m26090_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" KeyValuePair_2_t3114  KeyValuePair_2_get_Value_m26091_gshared (KeyValuePair_2_t3849 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m26091(__this, method) (( KeyValuePair_2_t3114  (*) (KeyValuePair_2_t3849 *, const MethodInfo*))KeyValuePair_2_get_Value_m26091_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m26092_gshared (KeyValuePair_2_t3849 * __this, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m26092(__this, ___value, method) (( void (*) (KeyValuePair_2_t3849 *, KeyValuePair_2_t3114 , const MethodInfo*))KeyValuePair_2_set_Value_m26092_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m26093_gshared (KeyValuePair_2_t3849 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m26093(__this, method) (( String_t* (*) (KeyValuePair_2_t3849 *, const MethodInfo*))KeyValuePair_2_ToString_m26093_gshared)(__this, method)
