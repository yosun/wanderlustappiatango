﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>
struct Enumerator_t840;
// System.Object
struct Object_t;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct Dictionary_2_t697;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define Enumerator__ctor_m20953(__this, ___host, method) (( void (*) (Enumerator_t840 *, Dictionary_2_t697 *, const MethodInfo*))Enumerator__ctor_m16551_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20954(__this, method) (( Object_t * (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m20955(__this, method) (( void (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_Dispose_m16553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4497(__this, method) (( bool (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_MoveNext_m16554_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m4496(__this, method) (( WordAbstractBehaviour_t101 * (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_get_Current_m16555_gshared)(__this, method)
