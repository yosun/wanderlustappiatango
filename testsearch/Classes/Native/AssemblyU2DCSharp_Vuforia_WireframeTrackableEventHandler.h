﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t99  : public MonoBehaviour_t7
{
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t52 * ___mTrackableBehaviour_2;
};
