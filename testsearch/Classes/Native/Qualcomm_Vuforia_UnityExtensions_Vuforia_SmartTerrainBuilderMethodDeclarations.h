﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t589;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerable_1_t768;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t76;
// Vuforia.Reconstruction
struct Reconstruction_t713;

// System.Boolean Vuforia.SmartTerrainBuilder::Init()
// System.Boolean Vuforia.SmartTerrainBuilder::Deinit()
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions()
// System.Boolean Vuforia.SmartTerrainBuilder::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::DestroyReconstruction(Vuforia.Reconstruction)
// System.Void Vuforia.SmartTerrainBuilder::.ctor()
extern "C" void SmartTerrainBuilder__ctor_m2784 (SmartTerrainBuilder_t589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
