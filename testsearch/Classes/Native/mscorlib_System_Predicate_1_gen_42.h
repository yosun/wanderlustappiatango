﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t106;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Surface>
struct  Predicate_1_t3551  : public MulticastDelegate_t314
{
};
