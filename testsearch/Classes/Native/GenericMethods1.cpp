﻿#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"
#include "codegen/il2cpp-codegen.h"

// System.Array
#include "mscorlib_System_Array.h"
// System.Void
#include "mscorlib_System_Void.h"
#include "mscorlib_ArrayTypes.h"
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.RankException
#include "mscorlib_System_RankException.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// Locale
#include "mscorlib_LocaleMethodDeclarations.h"
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
struct Array_t;
struct TableRangeU5BU5D_t2076;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806_gshared (Array_t * __this, TableRangeU5BU5D_t2076* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806(__this, ___array, ___index, method) (( void (*) (Array_t *, TableRangeU5BU5D_t2076*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t2075_m28806_gshared (Array_t * __this, TableRangeU5BU5D_t2076* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		TableRangeU5BU5D_t2076* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t2076* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t2076* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		TableRangeU5BU5D_t2076* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TableRangeU5BU5D_t2076* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807_gshared (Array_t * __this, TableRange_t2075  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807(__this, ___item, method) (( bool (*) (Array_t *, TableRange_t2075 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTableRange_t2075_m28807_gshared (Array_t * __this, TableRange_t2075  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t2075_m28808_gshared (Array_t * __this, TableRange_t2075  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTableRange_t2075_m28808(__this, ___item, method) (( int32_t (*) (Array_t *, TableRange_t2075 , const MethodInfo*))Array_InternalArray__IndexOf_TisTableRange_t2075_m28808_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisTableRange_t2075_m28808_gshared (Array_t * __this, TableRange_t2075  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TableRange_t2075  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TableRange_t2075 *)(&V_2));
		TableRange_t2075  L_5 = ___item;
		TableRange_t2075  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		TableRange_t2075  L_8 = V_2;
		TableRange_t2075  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		TableRange_t2075  L_14 = ___item;
		TableRange_t2075  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTableRange_t2075_m28809_gshared (Array_t * __this, int32_t ___index, TableRange_t2075  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTableRange_t2075_m28809(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t2075 , const MethodInfo*))Array_InternalArray__Insert_TisTableRange_t2075_m28809_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisTableRange_t2075_m28809_gshared (Array_t * __this, int32_t ___index, TableRange_t2075  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTableRange_t2075_m28811_gshared (Array_t * __this, int32_t ___index, TableRange_t2075  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTableRange_t2075_m28811(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TableRange_t2075 , const MethodInfo*))Array_InternalArray__set_Item_TisTableRange_t2075_m28811_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisTableRange_t2075_m28811_gshared (Array_t * __this, int32_t ___index, TableRange_t2075  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		TableRange_t2075  L_6 = ___item;
		TableRange_t2075  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TableRange_t2075 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99.h"
// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
struct Array_t;
struct IEnumerator_1_t4554;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<Mono.Globalization.Unicode.CodePointIndexer/TableRange>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2075_m28812_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3985  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3985 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3985  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C" Slot_t2152  Array_InternalArray__get_Item_TisSlot_t2152_m28814_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2152_m28814(__this, ___index, method) (( Slot_t2152  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2152_m28814_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" Slot_t2152  Array_InternalArray__get_Item_TisSlot_t2152_m28814_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t2152  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t2152 *)(&V_0));
		Slot_t2152  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815(__this, ___item, method) (( void (*) (Array_t *, Slot_t2152 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2152_m28815_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816(__this, ___item, method) (( bool (*) (Array_t *, Slot_t2152 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t2152_m28816_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t2152  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_005c;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t2152 *)(&V_2));
		Slot_t2152  L_5 = ___item;
		Slot_t2152  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		Slot_t2152  L_8 = V_2;
		Slot_t2152  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		Slot_t2152  L_11 = V_2;
		Slot_t2152  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_12);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_14 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_13);
		if (!L_14)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
struct Array_t;
struct SlotU5BU5D_t2158;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817_gshared (Array_t * __this, SlotU5BU5D_t2158* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t2158*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.Hashtable/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2152_m28817_gshared (Array_t * __this, SlotU5BU5D_t2158* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t2158* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2158* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2158* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		SlotU5BU5D_t2158* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2158* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818(__this, ___item, method) (( bool (*) (Array_t *, Slot_t2152 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t2152_m28818_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t2152_m28819_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t2152_m28819(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t2152 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t2152_m28819_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.Hashtable/Slot>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t2152_m28819_gshared (Array_t * __this, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t2152  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t2152 *)(&V_2));
		Slot_t2152  L_5 = ___item;
		Slot_t2152  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		Slot_t2152  L_8 = V_2;
		Slot_t2152  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		Slot_t2152  L_14 = ___item;
		Slot_t2152  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t2152_m28820_gshared (Array_t * __this, int32_t ___index, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t2152_m28820(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t2152 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t2152_m28820_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisSlot_t2152_m28820_gshared (Array_t * __this, int32_t ___index, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t2152_m28822_gshared (Array_t * __this, int32_t ___index, Slot_t2152  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t2152_m28822(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t2152 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t2152_m28822_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.Hashtable/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisSlot_t2152_m28822_gshared (Array_t * __this, int32_t ___index, Slot_t2152  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t2152  L_6 = ___item;
		Slot_t2152  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t2152 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104.h"
// System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
struct Array_t;
struct IEnumerator_1_t4555;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.Hashtable/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2152_m28823_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3994  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3994 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3994  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C" Slot_t2159  Array_InternalArray__get_Item_TisSlot_t2159_m28825_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2159_m28825(__this, ___index, method) (( Slot_t2159  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2159_m28825_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" Slot_t2159  Array_InternalArray__get_Item_TisSlot_t2159_m28825_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t2159  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Slot_t2159 *)(&V_0));
		Slot_t2159  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826(__this, ___item, method) (( void (*) (Array_t *, Slot_t2159 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2159_m28826_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827(__this, ___item, method) (( bool (*) (Array_t *, Slot_t2159 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Contains_TisSlot_t2159_m28827_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t2159  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_005c;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t2159 *)(&V_2));
		Slot_t2159  L_5 = ___item;
		Slot_t2159  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		Slot_t2159  L_8 = V_2;
		Slot_t2159  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		Slot_t2159  L_11 = V_2;
		Slot_t2159  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_12);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_14 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_13);
		if (!L_14)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
struct Array_t;
struct SlotU5BU5D_t2162;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828_gshared (Array_t * __this, SlotU5BU5D_t2162* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828(__this, ___array, ___index, method) (( void (*) (Array_t *, SlotU5BU5D_t2162*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Collections.SortedList/Slot>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2159_m28828_gshared (Array_t * __this, SlotU5BU5D_t2162* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlotU5BU5D_t2162* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2162* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2162* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		SlotU5BU5D_t2162* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		SlotU5BU5D_t2162* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829(__this, ___item, method) (( bool (*) (Array_t *, Slot_t2159 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Collections.SortedList/Slot>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisSlot_t2159_m28829_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t2159_m28830_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisSlot_t2159_m28830(__this, ___item, method) (( int32_t (*) (Array_t *, Slot_t2159 , const MethodInfo*))Array_InternalArray__IndexOf_TisSlot_t2159_m28830_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Collections.SortedList/Slot>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisSlot_t2159_m28830_gshared (Array_t * __this, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Slot_t2159  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Slot_t2159 *)(&V_2));
		Slot_t2159  L_5 = ___item;
		Slot_t2159  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		Slot_t2159  L_8 = V_2;
		Slot_t2159  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		Slot_t2159  L_14 = ___item;
		Slot_t2159  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisSlot_t2159_m28831_gshared (Array_t * __this, int32_t ___index, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisSlot_t2159_m28831(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t2159 , const MethodInfo*))Array_InternalArray__Insert_TisSlot_t2159_m28831_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisSlot_t2159_m28831_gshared (Array_t * __this, int32_t ___index, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisSlot_t2159_m28833_gshared (Array_t * __this, int32_t ___index, Slot_t2159  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisSlot_t2159_m28833(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Slot_t2159 , const MethodInfo*))Array_InternalArray__set_Item_TisSlot_t2159_m28833_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Collections.SortedList/Slot>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisSlot_t2159_m28833_gshared (Array_t * __this, int32_t ___index, Slot_t2159  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		Slot_t2159  L_6 = ___item;
		Slot_t2159  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Slot_t2159 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
struct Array_t;
struct IEnumerator_1_t4556;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Collections.SortedList/Slot>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2159_m28834_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t3995  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t3995 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t3995  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_gen.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_genMethodDeclarations.h"
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
struct MonoProperty_t;
struct Object_t;
struct Getter_2_t4005;
// Declaration System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835_gshared (Object_t * __this /* static, unused */, Getter_2_t4005 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, Getter_2_t4005 *, Object_t *, const MethodInfo*))MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28835_gshared (Object_t * __this /* static, unused */, Getter_2_t4005 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		Getter_2_t4005 * L_0 = ___getter;
		Object_t * L_1 = ___obj;
		NullCheck((Getter_2_t4005 *)L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(10 /* R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T) */, (Getter_2_t4005 *)L_0, (Object_t *)((Object_t *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))));
		Object_t * L_3 = L_2;
		return ((Object_t *)L_3);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_gen.h"
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_genMethodDeclarations.h"
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
struct MonoProperty_t;
struct Object_t;
struct StaticGetter_1_t4006;
// Declaration System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t4006 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, StaticGetter_1_t4006 *, Object_t *, const MethodInfo*))MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28836_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t4006 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		StaticGetter_1_t4006 * L_0 = ___getter;
		NullCheck((StaticGetter_1_t4006 *)L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(10 /* R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke() */, (StaticGetter_1_t4006 *)L_0);
		Object_t * L_2 = L_1;
		return ((Object_t *)L_2);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.DateTime
#include "mscorlib_System_DateTime.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C" DateTime_t120  Array_InternalArray__get_Item_TisDateTime_t120_m28838_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t120_m28838(__this, ___index, method) (( DateTime_t120  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t120_m28838_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" DateTime_t120  Array_InternalArray__get_Item_TisDateTime_t120_m28838_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t120  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (DateTime_t120 *)(&V_0));
		DateTime_t120  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839(__this, ___item, method) (( void (*) (Array_t *, DateTime_t120 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t120_m28839_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t120 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t120_m28840_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t120  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_005c;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t120 *)(&V_2));
		DateTime_t120  L_5 = ___item;
		DateTime_t120  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		DateTime_t120  L_8 = V_2;
		DateTime_t120  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		DateTime_t120  L_11 = V_2;
		DateTime_t120  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_12);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_14 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_13);
		if (!L_14)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
struct Array_t;
struct DateTimeU5BU5D_t2617;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841_gshared (Array_t * __this, DateTimeU5BU5D_t2617* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841(__this, ___array, ___index, method) (( void (*) (Array_t *, DateTimeU5BU5D_t2617*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t120_m28841_gshared (Array_t * __this, DateTimeU5BU5D_t2617* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeU5BU5D_t2617* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2617* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2617* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		DateTimeU5BU5D_t2617* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2617* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t120 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t120_m28842_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t120_m28843_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDateTime_t120_m28843(__this, ___item, method) (( int32_t (*) (Array_t *, DateTime_t120 , const MethodInfo*))Array_InternalArray__IndexOf_TisDateTime_t120_m28843_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t120_m28843_gshared (Array_t * __this, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t120  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t120 *)(&V_2));
		DateTime_t120  L_5 = ___item;
		DateTime_t120  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		DateTime_t120  L_8 = V_2;
		DateTime_t120  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		DateTime_t120  L_14 = ___item;
		DateTime_t120  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDateTime_t120_m28844_gshared (Array_t * __this, int32_t ___index, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDateTime_t120_m28844(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t120 , const MethodInfo*))Array_InternalArray__Insert_TisDateTime_t120_m28844_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisDateTime_t120_m28844_gshared (Array_t * __this, int32_t ___index, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDateTime_t120_m28846_gshared (Array_t * __this, int32_t ___index, DateTime_t120  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDateTime_t120_m28846(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t120 , const MethodInfo*))Array_InternalArray__set_Item_TisDateTime_t120_m28846_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisDateTime_t120_m28846_gshared (Array_t * __this, int32_t ___index, DateTime_t120  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		DateTime_t120  L_6 = ___item;
		DateTime_t120  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (DateTime_t120 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118.h"
// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
struct Array_t;
struct IEnumerator_1_t4557;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t120_m28847_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t4010  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t4010 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t4010  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Decimal
#include "mscorlib_System_Decimal.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C" Decimal_t1065  Array_InternalArray__get_Item_TisDecimal_t1065_m28849_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1065_m28849(__this, ___index, method) (( Decimal_t1065  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1065_m28849_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" Decimal_t1065  Array_InternalArray__get_Item_TisDecimal_t1065_m28849_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t1065  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Decimal_t1065 *)(&V_0));
		Decimal_t1065  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850(__this, ___item, method) (( void (*) (Array_t *, Decimal_t1065 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1065_m28850_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1065 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1065_m28851_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1065  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_005c;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1065 *)(&V_2));
		Decimal_t1065  L_5 = ___item;
		Decimal_t1065  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		Decimal_t1065  L_8 = V_2;
		Decimal_t1065  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		Decimal_t1065  L_11 = V_2;
		Decimal_t1065  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_12);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_14 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_13);
		if (!L_14)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
struct Array_t;
struct DecimalU5BU5D_t2618;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852_gshared (Array_t * __this, DecimalU5BU5D_t2618* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852(__this, ___array, ___index, method) (( void (*) (Array_t *, DecimalU5BU5D_t2618*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1065_m28852_gshared (Array_t * __this, DecimalU5BU5D_t2618* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		DecimalU5BU5D_t2618* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2618* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2618* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		DecimalU5BU5D_t2618* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2618* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1065 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1065_m28853_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1065_m28854_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDecimal_t1065_m28854(__this, ___item, method) (( int32_t (*) (Array_t *, Decimal_t1065 , const MethodInfo*))Array_InternalArray__IndexOf_TisDecimal_t1065_m28854_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1065_m28854_gshared (Array_t * __this, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1065  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1065 *)(&V_2));
		Decimal_t1065  L_5 = ___item;
		Decimal_t1065  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		Decimal_t1065  L_8 = V_2;
		Decimal_t1065  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		Decimal_t1065  L_14 = ___item;
		Decimal_t1065  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDecimal_t1065_m28855_gshared (Array_t * __this, int32_t ___index, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDecimal_t1065_m28855(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1065 , const MethodInfo*))Array_InternalArray__Insert_TisDecimal_t1065_m28855_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisDecimal_t1065_m28855_gshared (Array_t * __this, int32_t ___index, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1065_m28857_gshared (Array_t * __this, int32_t ___index, Decimal_t1065  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDecimal_t1065_m28857(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1065 , const MethodInfo*))Array_InternalArray__set_Item_TisDecimal_t1065_m28857_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1065_m28857_gshared (Array_t * __this, int32_t ___index, Decimal_t1065  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		Decimal_t1065  L_6 = ___item;
		Decimal_t1065  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Decimal_t1065 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Decimal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119.h"
// System.Array/InternalEnumerator`1<System.Decimal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
struct Array_t;
struct IEnumerator_1_t4558;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1065_m28858_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t4011  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t4011 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t4011  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C" TimeSpan_t121  Array_InternalArray__get_Item_TisTimeSpan_t121_m28860_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t121_m28860(__this, ___index, method) (( TimeSpan_t121  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t121_m28860_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t121  Array_InternalArray__get_Item_TisTimeSpan_t121_m28860_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t121  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TimeSpan_t121 *)(&V_0));
		TimeSpan_t121  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861(__this, ___item, method) (( void (*) (Array_t *, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t121_m28861_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t121_m28862_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t121  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_005c;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t121 *)(&V_2));
		TimeSpan_t121  L_5 = ___item;
		TimeSpan_t121  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0041;
		}
	}
	{
		TimeSpan_t121  L_8 = V_2;
		TimeSpan_t121  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		TimeSpan_t121  L_11 = V_2;
		TimeSpan_t121  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_12);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_14 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_13);
		if (!L_14)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
struct Array_t;
struct TimeSpanU5BU5D_t2619;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863_gshared (Array_t * __this, TimeSpanU5BU5D_t2619* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863(__this, ___array, ___index, method) (( void (*) (Array_t *, TimeSpanU5BU5D_t2619*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t121_m28863_gshared (Array_t * __this, TimeSpanU5BU5D_t2619* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpanU5BU5D_t2619* L_0 = ___array;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_4 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2619* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m10165((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2619* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m10163((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t476 * L_11 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_11, (String_t*)(String_t*) &_stringLiteral1393, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_004c:
	{
		TimeSpanU5BU5D_t2619* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m9344((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_15 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0065:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1394, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1412 * L_18 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7010(L_18, (String_t*)(String_t*) &_stringLiteral591, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_007e:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2619* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m10163((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t121_m28864_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865(__this, ___item, method) (( int32_t (*) (Array_t *, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern TypeInfo* RankException_t2549_il2cpp_TypeInfo_var;
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t121_m28865_gshared (Array_t * __this, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4155);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t121  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m9344((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m10341(NULL /*static, unused*/, (String_t*)(String_t*) &_stringLiteral1392, /*hidden argument*/NULL);
		RankException_t2549 * L_2 = (RankException_t2549 *)il2cpp_codegen_object_new (RankException_t2549_il2cpp_TypeInfo_var);
		RankException__ctor_m13865(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0074;
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t121 *)(&V_2));
		TimeSpan_t121  L_5 = ___item;
		TimeSpan_t121  L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_6);
		if (L_7)
		{
			goto IL_0051;
		}
	}
	{
		TimeSpan_t121  L_8 = V_2;
		TimeSpan_t121  L_9 = L_8;
		Object_t * L_10 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_9);
		if (L_10)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_12 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_11+(int32_t)L_12));
	}

IL_0047:
	{
		NullCheck((Array_t *)__this);
		int32_t L_13 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0051:
	{
		TimeSpan_t121  L_14 = ___item;
		TimeSpan_t121  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_15);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_17 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_16);
		if (!L_17)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_18+(int32_t)L_19));
	}

IL_0070:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_23 = Array_GetLowerBound_m10165((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_23-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t121_m28866_gshared (Array_t * __this, int32_t ___index, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTimeSpan_t121_m28866(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__Insert_TisTimeSpan_t121_m28866_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t121_m28866_gshared (Array_t * __this, int32_t ___index, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral958, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t121_m28868_gshared (Array_t * __this, int32_t ___index, TimeSpan_t121  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTimeSpan_t121_m28868(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t121 , const MethodInfo*))Array_InternalArray__set_Item_TisTimeSpan_t121_m28868_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t121_m28868_gshared (Array_t * __this, int32_t ___index, TimeSpan_t121  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m9341((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		V_0 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)IsInst(__this, ObjectU5BU5D_t124_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t124* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t124* L_4 = V_0;
		int32_t L_5 = ___index;
		TimeSpan_t121  L_6 = ___item;
		TimeSpan_t121  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_0028:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TimeSpan_t121 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.TimeSpan>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120.h"
// System.Array/InternalEnumerator`1<System.TimeSpan>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
struct Array_t;
struct IEnumerator_1_t4559;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t121_m28869_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t4012  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t4012 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t4012  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
