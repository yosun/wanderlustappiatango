﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t644;

// System.Void Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::.ctor()
extern "C" void NullPremiumObjectFactory__ctor_m3018 (NullPremiumObjectFactory_t644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
