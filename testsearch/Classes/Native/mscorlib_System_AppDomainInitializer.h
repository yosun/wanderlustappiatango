﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t15;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AppDomainInitializer
struct  AppDomainInitializer_t2498  : public MulticastDelegate_t314
{
};
