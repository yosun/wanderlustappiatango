﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t2449;

// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m12889 (SuppressUnmanagedCodeSecurityAttribute_t2449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
