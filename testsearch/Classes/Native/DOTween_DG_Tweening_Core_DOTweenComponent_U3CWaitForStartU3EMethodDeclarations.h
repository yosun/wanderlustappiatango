﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
struct U3CWaitForStartU3Ed__a_t947;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::MoveNext()
extern "C" bool U3CWaitForStartU3Ed__a_MoveNext_m5321 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.IDisposable.Dispose()
extern "C" void U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::.ctor(System.Int32)
extern "C" void U3CWaitForStartU3Ed__a__ctor_m5325 (U3CWaitForStartU3Ed__a_t947 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
