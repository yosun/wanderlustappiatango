﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t0_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t0_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
// Metadata Definition ARVRModes/TheCurrentMode
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentModeMethodDeclarations.h"
static const MethodInfo* TheCurrentMode_t1_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference TheCurrentMode_t1_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TheCurrentMode_t1_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair TheCurrentMode_t1_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TheCurrentMode_t1_0_0_0;
extern const Il2CppType TheCurrentMode_t1_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
extern TypeInfo ARVRModes_t6_il2cpp_TypeInfo;
extern const Il2CppType ARVRModes_t6_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata TheCurrentMode_t1_DefinitionMetadata = 
{
	&ARVRModes_t6_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TheCurrentMode_t1_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TheCurrentMode_t1_VTable/* vtableMethods */
	, TheCurrentMode_t1_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TheCurrentMode"/* name */
	, ""/* namespaze */
	, TheCurrentMode_t1_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TheCurrentMode_t1_0_0_0/* byval_arg */
	, &TheCurrentMode_t1_1_0_0/* this_arg */
	, &TheCurrentMode_t1_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TheCurrentMode_t1)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TheCurrentMode_t1)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
// Metadata Definition ARVRModes
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModesMethodDeclarations.h"
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::.ctor()
extern const MethodInfo ARVRModes__ctor_m0_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ARVRModes__ctor_m0/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::.cctor()
extern const MethodInfo ARVRModes__cctor_m1_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ARVRModes__cctor_m1/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::Start()
extern const MethodInfo ARVRModes_Start_m2_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ARVRModes_Start_m2/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppType GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ARVRModes_t6_ARVRModes_FlipGameObjectArray_m3_ParameterInfos[] = 
{
	{"g", 0, 134217729, 0, &GameObjectU5BU5D_t5_0_0_0},
	{"flip", 1, 134217730, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::FlipGameObjectArray(UnityEngine.GameObject[],System.Boolean)
extern const MethodInfo ARVRModes_FlipGameObjectArray_m3_MethodInfo = 
{
	"FlipGameObjectArray"/* name */
	, (methodPointerType)&ARVRModes_FlipGameObjectArray_m3/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, ARVRModes_t6_ARVRModes_FlipGameObjectArray_m3_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SeeEntire()
extern const MethodInfo ARVRModes_SeeEntire_m4_MethodInfo = 
{
	"SeeEntire"/* name */
	, (methodPointerType)&ARVRModes_SeeEntire_m4/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SeeDoor()
extern const MethodInfo ARVRModes_SeeDoor_m5_MethodInfo = 
{
	"SeeDoor"/* name */
	, (methodPointerType)&ARVRModes_SeeDoor_m5/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchARToVR()
extern const MethodInfo ARVRModes_SwitchARToVR_m6_MethodInfo = 
{
	"SwitchARToVR"/* name */
	, (methodPointerType)&ARVRModes_SwitchARToVR_m6/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchVRToAR()
extern const MethodInfo ARVRModes_SwitchVRToAR_m7_MethodInfo = 
{
	"SwitchVRToAR"/* name */
	, (methodPointerType)&ARVRModes_SwitchVRToAR_m7/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchModes()
extern const MethodInfo ARVRModes_SwitchModes_m8_MethodInfo = 
{
	"SwitchModes"/* name */
	, (methodPointerType)&ARVRModes_SwitchModes_m8/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::Update()
extern const MethodInfo ARVRModes_Update_m9_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ARVRModes_Update_m9/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ARVRModes_t6_MethodInfos[] =
{
	&ARVRModes__ctor_m0_MethodInfo,
	&ARVRModes__cctor_m1_MethodInfo,
	&ARVRModes_Start_m2_MethodInfo,
	&ARVRModes_FlipGameObjectArray_m3_MethodInfo,
	&ARVRModes_SeeEntire_m4_MethodInfo,
	&ARVRModes_SeeDoor_m5_MethodInfo,
	&ARVRModes_SwitchARToVR_m6_MethodInfo,
	&ARVRModes_SwitchVRToAR_m7_MethodInfo,
	&ARVRModes_SwitchModes_m8_MethodInfo,
	&ARVRModes_Update_m9_MethodInfo,
	NULL
};
static const Il2CppType* ARVRModes_t6_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TheCurrentMode_t1_0_0_0,
};
extern const MethodInfo Object_Equals_m563_MethodInfo;
extern const MethodInfo Object_GetHashCode_m564_MethodInfo;
extern const MethodInfo Object_ToString_m565_MethodInfo;
static const Il2CppMethodReference ARVRModes_t6_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool ARVRModes_t6_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ARVRModes_t6_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct ARVRModes_t6;
const Il2CppTypeDefinitionMetadata ARVRModes_t6_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ARVRModes_t6_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, ARVRModes_t6_VTable/* vtableMethods */
	, ARVRModes_t6_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3/* fieldStart */

};
TypeInfo ARVRModes_t6_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ARVRModes"/* name */
	, ""/* namespaze */
	, ARVRModes_t6_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ARVRModes_t6_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ARVRModes_t6_0_0_0/* byval_arg */
	, &ARVRModes_t6_1_0_0/* this_arg */
	, &ARVRModes_t6_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ARVRModes_t6)/* instance_size */
	, sizeof (ARVRModes_t6)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ARVRModes_t6_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RealitySearch
#include "AssemblyU2DCSharp_RealitySearch.h"
// Metadata Definition RealitySearch
extern TypeInfo RealitySearch_t13_il2cpp_TypeInfo;
// RealitySearch
#include "AssemblyU2DCSharp_RealitySearchMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::.ctor()
extern const MethodInfo RealitySearch__ctor_m10_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealitySearch__ctor_m10/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::BuildFromLocalDB()
extern const MethodInfo RealitySearch_BuildFromLocalDB_m11_MethodInfo = 
{
	"BuildFromLocalDB"/* name */
	, (methodPointerType)&RealitySearch_BuildFromLocalDB_m11/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::SearchFromInputField()
extern const MethodInfo RealitySearch_SearchFromInputField_m12_MethodInfo = 
{
	"SearchFromInputField"/* name */
	, (methodPointerType)&RealitySearch_SearchFromInputField_m12/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RealitySearch_t13_RealitySearch_Search_m13_ParameterInfos[] = 
{
	{"tag", 0, 134217731, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::Search(System.String)
extern const MethodInfo RealitySearch_Search_m13_MethodInfo = 
{
	"Search"/* name */
	, (methodPointerType)&RealitySearch_Search_m13/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RealitySearch_t13_RealitySearch_Search_m13_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo RealitySearch_t13_RealitySearch_CreatePathFromCurrentTo_m14_ParameterInfos[] = 
{
	{"g", 0, 134217732, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::CreatePathFromCurrentTo(UnityEngine.GameObject)
extern const MethodInfo RealitySearch_CreatePathFromCurrentTo_m14_MethodInfo = 
{
	"CreatePathFromCurrentTo"/* name */
	, (methodPointerType)&RealitySearch_CreatePathFromCurrentTo_m14/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RealitySearch_t13_RealitySearch_CreatePathFromCurrentTo_m14_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Quaternion_t22_0_0_0;
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo RealitySearch_t13_RealitySearch_InstantiateFromPool_m15_ParameterInfos[] = 
{
	{"g", 0, 134217733, 0, &GameObject_t2_0_0_0},
	{"pos", 1, 134217734, 0, &Vector3_t14_0_0_0},
	{"rot", 2, 134217735, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t14_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject RealitySearch::InstantiateFromPool(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern const MethodInfo RealitySearch_InstantiateFromPool_m15_MethodInfo = 
{
	"InstantiateFromPool"/* name */
	, (methodPointerType)&RealitySearch_InstantiateFromPool_m15/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t14_Quaternion_t22/* invoker_method */
	, RealitySearch_t13_RealitySearch_InstantiateFromPool_m15_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo RealitySearch_t13_RealitySearch_CreatePool_m16_ParameterInfos[] = 
{
	{"num", 0, 134217736, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::CreatePool(System.Int32)
extern const MethodInfo RealitySearch_CreatePool_m16_MethodInfo = 
{
	"CreatePool"/* name */
	, (methodPointerType)&RealitySearch_CreatePool_m16/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, RealitySearch_t13_RealitySearch_CreatePool_m16_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::Awake()
extern const MethodInfo RealitySearch_Awake_m17_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&RealitySearch_Awake_m17/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void RealitySearch::Update()
extern const MethodInfo RealitySearch_Update_m18_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&RealitySearch_Update_m18/* method */
	, &RealitySearch_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RealitySearch_t13_MethodInfos[] =
{
	&RealitySearch__ctor_m10_MethodInfo,
	&RealitySearch_BuildFromLocalDB_m11_MethodInfo,
	&RealitySearch_SearchFromInputField_m12_MethodInfo,
	&RealitySearch_Search_m13_MethodInfo,
	&RealitySearch_CreatePathFromCurrentTo_m14_MethodInfo,
	&RealitySearch_InstantiateFromPool_m15_MethodInfo,
	&RealitySearch_CreatePool_m16_MethodInfo,
	&RealitySearch_Awake_m17_MethodInfo,
	&RealitySearch_Update_m18_MethodInfo,
	NULL
};
static const Il2CppMethodReference RealitySearch_t13_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool RealitySearch_t13_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RealitySearch_t13_0_0_0;
extern const Il2CppType RealitySearch_t13_1_0_0;
struct RealitySearch_t13;
const Il2CppTypeDefinitionMetadata RealitySearch_t13_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, RealitySearch_t13_VTable/* vtableMethods */
	, RealitySearch_t13_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 16/* fieldStart */

};
TypeInfo RealitySearch_t13_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealitySearch"/* name */
	, ""/* namespaze */
	, RealitySearch_t13_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RealitySearch_t13_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RealitySearch_t13_0_0_0/* byval_arg */
	, &RealitySearch_t13_1_0_0/* this_arg */
	, &RealitySearch_t13_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealitySearch_t13)/* instance_size */
	, sizeof (RealitySearch_t13)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// EachSearchableObject
#include "AssemblyU2DCSharp_EachSearchableObject.h"
// Metadata Definition EachSearchableObject
extern TypeInfo EachSearchableObject_t16_il2cpp_TypeInfo;
// EachSearchableObject
#include "AssemblyU2DCSharp_EachSearchableObjectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void EachSearchableObject::.ctor()
extern const MethodInfo EachSearchableObject__ctor_m19_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EachSearchableObject__ctor_m19/* method */
	, &EachSearchableObject_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EachSearchableObject_t16_MethodInfos[] =
{
	&EachSearchableObject__ctor_m19_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference EachSearchableObject_t16_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool EachSearchableObject_t16_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType EachSearchableObject_t16_0_0_0;
extern const Il2CppType EachSearchableObject_t16_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct EachSearchableObject_t16;
const Il2CppTypeDefinitionMetadata EachSearchableObject_t16_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EachSearchableObject_t16_VTable/* vtableMethods */
	, EachSearchableObject_t16_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 30/* fieldStart */

};
TypeInfo EachSearchableObject_t16_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "EachSearchableObject"/* name */
	, ""/* namespaze */
	, EachSearchableObject_t16_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EachSearchableObject_t16_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EachSearchableObject_t16_0_0_0/* byval_arg */
	, &EachSearchableObject_t16_1_0_0/* this_arg */
	, &EachSearchableObject_t16_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EachSearchableObject_t16)/* instance_size */
	, sizeof (EachSearchableObject_t16)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
// Metadata Definition AnimateTexture
extern TypeInfo AnimateTexture_t18_il2cpp_TypeInfo;
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTextureMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::.ctor()
extern const MethodInfo AnimateTexture__ctor_m20_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnimateTexture__ctor_m20/* method */
	, &AnimateTexture_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Awake()
extern const MethodInfo AnimateTexture_Awake_m21_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&AnimateTexture_Awake_m21/* method */
	, &AnimateTexture_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Update()
extern const MethodInfo AnimateTexture_Update_m22_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AnimateTexture_Update_m22/* method */
	, &AnimateTexture_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AnimateTexture_t18_MethodInfos[] =
{
	&AnimateTexture__ctor_m20_MethodInfo,
	&AnimateTexture_Awake_m21_MethodInfo,
	&AnimateTexture_Update_m22_MethodInfo,
	NULL
};
static const Il2CppMethodReference AnimateTexture_t18_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool AnimateTexture_t18_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AnimateTexture_t18_0_0_0;
extern const Il2CppType AnimateTexture_t18_1_0_0;
struct AnimateTexture_t18;
const Il2CppTypeDefinitionMetadata AnimateTexture_t18_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, AnimateTexture_t18_VTable/* vtableMethods */
	, AnimateTexture_t18_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 33/* fieldStart */

};
TypeInfo AnimateTexture_t18_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimateTexture"/* name */
	, ""/* namespaze */
	, AnimateTexture_t18_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AnimateTexture_t18_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimateTexture_t18_0_0_0/* byval_arg */
	, &AnimateTexture_t18_1_0_0/* this_arg */
	, &AnimateTexture_t18_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimateTexture_t18)/* instance_size */
	, sizeof (AnimateTexture_t18)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// FadeLoop
#include "AssemblyU2DCSharp_FadeLoop.h"
// Metadata Definition FadeLoop
extern TypeInfo FadeLoop_t20_il2cpp_TypeInfo;
// FadeLoop
#include "AssemblyU2DCSharp_FadeLoopMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void FadeLoop::.ctor()
extern const MethodInfo FadeLoop__ctor_m23_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FadeLoop__ctor_m23/* method */
	, &FadeLoop_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void FadeLoop::Update()
extern const MethodInfo FadeLoop_Update_m24_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FadeLoop_Update_m24/* method */
	, &FadeLoop_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FadeLoop_t20_MethodInfos[] =
{
	&FadeLoop__ctor_m23_MethodInfo,
	&FadeLoop_Update_m24_MethodInfo,
	NULL
};
static const Il2CppMethodReference FadeLoop_t20_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool FadeLoop_t20_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType FadeLoop_t20_0_0_0;
extern const Il2CppType FadeLoop_t20_1_0_0;
struct FadeLoop_t20;
const Il2CppTypeDefinitionMetadata FadeLoop_t20_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, FadeLoop_t20_VTable/* vtableMethods */
	, FadeLoop_t20_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 36/* fieldStart */

};
TypeInfo FadeLoop_t20_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "FadeLoop"/* name */
	, ""/* namespaze */
	, FadeLoop_t20_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FadeLoop_t20_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FadeLoop_t20_0_0_0/* byval_arg */
	, &FadeLoop_t20_1_0_0/* this_arg */
	, &FadeLoop_t20_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FadeLoop_t20)/* instance_size */
	, sizeof (FadeLoop_t20)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
// Metadata Definition Gyro2
extern TypeInfo Gyro2_t21_il2cpp_TypeInfo;
// Gyro2
#include "AssemblyU2DCSharp_Gyro2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::.ctor()
extern const MethodInfo Gyro2__ctor_m25_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Gyro2__ctor_m25/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::.cctor()
extern const MethodInfo Gyro2__cctor_m26_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Gyro2__cctor_m26/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Awake()
extern const MethodInfo Gyro2_Awake_m27_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Gyro2_Awake_m27/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Start()
extern const MethodInfo Gyro2_Start_m28_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Gyro2_Start_m28/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_ClampPosInRoom_m29_ParameterInfos[] = 
{
	{"p", 0, 134217737, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern const MethodInfo Gyro2_ClampPosInRoom_m29_MethodInfo = 
{
	"ClampPosInRoom"/* name */
	, (methodPointerType)&Gyro2_ClampPosInRoom_m29/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Vector3_t14/* invoker_method */
	, Gyro2_t21_Gyro2_ClampPosInRoom_m29_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_StationParent_m30_ParameterInfos[] = 
{
	{"pos", 0, 134217738, 0, &Vector3_t14_0_0_0},
	{"rot", 1, 134217739, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern const MethodInfo Gyro2_StationParent_m30_MethodInfo = 
{
	"StationParent"/* name */
	, (methodPointerType)&Gyro2_StationParent_m30/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14_Quaternion_t22/* invoker_method */
	, Gyro2_t21_Gyro2_StationParent_m30_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_StationParentShiftAngle_m31_ParameterInfos[] = 
{
	{"angle", 0, 134217740, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern const MethodInfo Gyro2_StationParentShiftAngle_m31_MethodInfo = 
{
	"StationParentShiftAngle"/* name */
	, (methodPointerType)&Gyro2_StationParentShiftAngle_m31/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Gyro2_t21_Gyro2_StationParentShiftAngle_m31_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_Init_m32_ParameterInfos[] = 
{
	{"t", 0, 134217741, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Init(UnityEngine.Transform)
extern const MethodInfo Gyro2_Init_m32_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&Gyro2_Init_m32/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Gyro2_t21_Gyro2_Init_m32_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::ResetStatic()
extern const MethodInfo Gyro2_ResetStatic_m33_MethodInfo = 
{
	"ResetStatic"/* name */
	, (methodPointerType)&Gyro2_ResetStatic_m33/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::ResetStaticSmoothly()
extern const MethodInfo Gyro2_ResetStaticSmoothly_m34_MethodInfo = 
{
	"ResetStaticSmoothly"/* name */
	, (methodPointerType)&Gyro2_ResetStaticSmoothly_m34/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Reset()
extern const MethodInfo Gyro2_Reset_m35_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Gyro2_Reset_m35/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_DoSwipeRotation_m36_ParameterInfos[] = 
{
	{"degree", 0, 134217742, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern const MethodInfo Gyro2_DoSwipeRotation_m36_MethodInfo = 
{
	"DoSwipeRotation"/* name */
	, (methodPointerType)&Gyro2_DoSwipeRotation_m36/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Gyro2_t21_Gyro2_DoSwipeRotation_m36_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::DisableSettingsButotn()
extern const MethodInfo Gyro2_DisableSettingsButotn_m37_MethodInfo = 
{
	"DisableSettingsButotn"/* name */
	, (methodPointerType)&Gyro2_DisableSettingsButotn_m37/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Update()
extern const MethodInfo Gyro2_Update_m38_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Gyro2_Update_m38/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::AssignCalibration()
extern const MethodInfo Gyro2_AssignCalibration_m39_MethodInfo = 
{
	"AssignCalibration"/* name */
	, (methodPointerType)&Gyro2_AssignCalibration_m39/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::AssignCamBaseRotation()
extern const MethodInfo Gyro2_AssignCamBaseRotation_m40_MethodInfo = 
{
	"AssignCamBaseRotation"/* name */
	, (methodPointerType)&Gyro2_AssignCamBaseRotation_m40/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Gyro2::G()
extern const MethodInfo Gyro2_G_m41_MethodInfo = 
{
	"G"/* name */
	, (methodPointerType)&Gyro2_G_m41/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo Gyro2_t21_Gyro2_ConvertRotation_m42_ParameterInfos[] = 
{
	{"q", 0, 134217743, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t22_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern const MethodInfo Gyro2_ConvertRotation_m42_MethodInfo = 
{
	"ConvertRotation"/* name */
	, (methodPointerType)&Gyro2_ConvertRotation_m42/* method */
	, &Gyro2_t21_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22_Quaternion_t22/* invoker_method */
	, Gyro2_t21_Gyro2_ConvertRotation_m42_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Gyro2_t21_MethodInfos[] =
{
	&Gyro2__ctor_m25_MethodInfo,
	&Gyro2__cctor_m26_MethodInfo,
	&Gyro2_Awake_m27_MethodInfo,
	&Gyro2_Start_m28_MethodInfo,
	&Gyro2_ClampPosInRoom_m29_MethodInfo,
	&Gyro2_StationParent_m30_MethodInfo,
	&Gyro2_StationParentShiftAngle_m31_MethodInfo,
	&Gyro2_Init_m32_MethodInfo,
	&Gyro2_ResetStatic_m33_MethodInfo,
	&Gyro2_ResetStaticSmoothly_m34_MethodInfo,
	&Gyro2_Reset_m35_MethodInfo,
	&Gyro2_DoSwipeRotation_m36_MethodInfo,
	&Gyro2_DisableSettingsButotn_m37_MethodInfo,
	&Gyro2_Update_m38_MethodInfo,
	&Gyro2_AssignCalibration_m39_MethodInfo,
	&Gyro2_AssignCamBaseRotation_m40_MethodInfo,
	&Gyro2_G_m41_MethodInfo,
	&Gyro2_ConvertRotation_m42_MethodInfo,
	NULL
};
static const Il2CppMethodReference Gyro2_t21_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool Gyro2_t21_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Gyro2_t21_0_0_0;
extern const Il2CppType Gyro2_t21_1_0_0;
struct Gyro2_t21;
const Il2CppTypeDefinitionMetadata Gyro2_t21_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Gyro2_t21_VTable/* vtableMethods */
	, Gyro2_t21_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 39/* fieldStart */

};
TypeInfo Gyro2_t21_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Gyro2"/* name */
	, ""/* namespaze */
	, Gyro2_t21_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Gyro2_t21_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Gyro2_t21_0_0_0/* byval_arg */
	, &Gyro2_t21_1_0_0/* this_arg */
	, &Gyro2_t21_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Gyro2_t21)/* instance_size */
	, sizeof (Gyro2_t21)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Gyro2_t21_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
// Metadata Definition GyroCameraYo
extern TypeInfo GyroCameraYo_t23_il2cpp_TypeInfo;
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::.ctor()
extern const MethodInfo GyroCameraYo__ctor_m43_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GyroCameraYo__ctor_m43/* method */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Start()
extern const MethodInfo GyroCameraYo_Start_m44_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&GyroCameraYo_Start_m44/* method */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Update()
extern const MethodInfo GyroCameraYo_Update_m45_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&GyroCameraYo_Update_m45/* method */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo GyroCameraYo_t23_GyroCameraYo_ConvertRotation_m46_ParameterInfos[] = 
{
	{"q", 0, 134217744, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t22_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern const MethodInfo GyroCameraYo_ConvertRotation_m46_MethodInfo = 
{
	"ConvertRotation"/* name */
	, (methodPointerType)&GyroCameraYo_ConvertRotation_m46/* method */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22_Quaternion_t22/* invoker_method */
	, GyroCameraYo_t23_GyroCameraYo_ConvertRotation_m46_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::GyroCam()
extern const MethodInfo GyroCameraYo_GyroCam_m47_MethodInfo = 
{
	"GyroCam"/* name */
	, (methodPointerType)&GyroCameraYo_GyroCam_m47/* method */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GyroCameraYo_t23_MethodInfos[] =
{
	&GyroCameraYo__ctor_m43_MethodInfo,
	&GyroCameraYo_Start_m44_MethodInfo,
	&GyroCameraYo_Update_m45_MethodInfo,
	&GyroCameraYo_ConvertRotation_m46_MethodInfo,
	&GyroCameraYo_GyroCam_m47_MethodInfo,
	NULL
};
static const Il2CppMethodReference GyroCameraYo_t23_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool GyroCameraYo_t23_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GyroCameraYo_t23_0_0_0;
extern const Il2CppType GyroCameraYo_t23_1_0_0;
struct GyroCameraYo_t23;
const Il2CppTypeDefinitionMetadata GyroCameraYo_t23_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, GyroCameraYo_t23_VTable/* vtableMethods */
	, GyroCameraYo_t23_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 49/* fieldStart */

};
TypeInfo GyroCameraYo_t23_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GyroCameraYo"/* name */
	, ""/* namespaze */
	, GyroCameraYo_t23_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GyroCameraYo_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GyroCameraYo_t23_0_0_0/* byval_arg */
	, &GyroCameraYo_t23_1_0_0/* this_arg */
	, &GyroCameraYo_t23_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GyroCameraYo_t23)/* instance_size */
	, sizeof (GyroCameraYo_t23)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Input2
#include "AssemblyU2DCSharp_Input2.h"
// Metadata Definition Input2
extern TypeInfo Input2_t24_il2cpp_TypeInfo;
// Input2
#include "AssemblyU2DCSharp_Input2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Input2::.ctor()
extern const MethodInfo Input2__ctor_m48_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Input2__ctor_m48/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Input2::.cctor()
extern const MethodInfo Input2__cctor_m49_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Input2__cctor_m49/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::IsPointerOverUI()
extern const MethodInfo Input2_IsPointerOverUI_m50_MethodInfo = 
{
	"IsPointerOverUI"/* name */
	, (methodPointerType)&Input2_IsPointerOverUI_m50/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::TouchBegin()
extern const MethodInfo Input2_TouchBegin_m51_MethodInfo = 
{
	"TouchBegin"/* name */
	, (methodPointerType)&Input2_TouchBegin_m51/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetFirstPoint()
extern const MethodInfo Input2_GetFirstPoint_m52_MethodInfo = 
{
	"GetFirstPoint"/* name */
	, (methodPointerType)&Input2_GetFirstPoint_m52/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern const MethodInfo Input2_GetSecondPoint_m53_MethodInfo = 
{
	"GetSecondPoint"/* name */
	, (methodPointerType)&Input2_GetSecondPoint_m53/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::GetPointerCount()
extern const MethodInfo Input2_GetPointerCount_m54_MethodInfo = 
{
	"GetPointerCount"/* name */
	, (methodPointerType)&Input2_GetPointerCount_m54/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Input2_t24_Input2_HasPointStarted_m55_ParameterInfos[] = 
{
	{"num", 0, 134217745, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointStarted(System.Int32)
extern const MethodInfo Input2_HasPointStarted_m55_MethodInfo = 
{
	"HasPointStarted"/* name */
	, (methodPointerType)&Input2_HasPointStarted_m55/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, Input2_t24_Input2_HasPointStarted_m55_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Input2_t24_Input2_HasPointEnded_m56_ParameterInfos[] = 
{
	{"num", 0, 134217746, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointEnded(System.Int32)
extern const MethodInfo Input2_HasPointEnded_m56_MethodInfo = 
{
	"HasPointEnded"/* name */
	, (methodPointerType)&Input2_HasPointEnded_m56/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, Input2_t24_Input2_HasPointEnded_m56_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Input2_t24_Input2_HasPointMoved_m57_ParameterInfos[] = 
{
	{"num", 0, 134217747, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointMoved(System.Int32)
extern const MethodInfo Input2_HasPointMoved_m57_MethodInfo = 
{
	"HasPointMoved"/* name */
	, (methodPointerType)&Input2_HasPointMoved_m57/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, Input2_t24_Input2_HasPointMoved_m57_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single Input2::Pinch()
extern const MethodInfo Input2_Pinch_m58_MethodInfo = 
{
	"Pinch"/* name */
	, (methodPointerType)&Input2_Pinch_m58/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single Input2::Twist()
extern const MethodInfo Input2_Twist_m59_MethodInfo = 
{
	"Twist"/* name */
	, (methodPointerType)&Input2_Twist_m59/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::TwistInt()
extern const MethodInfo Input2_TwistInt_m60_MethodInfo = 
{
	"TwistInt"/* name */
	, (methodPointerType)&Input2_TwistInt_m60/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Input2_t24_Input2_SwipeLeft_m61_ParameterInfos[] = 
{
	{"swipedir", 0, 134217748, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeLeft(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeLeft_m61_MethodInfo = 
{
	"SwipeLeft"/* name */
	, (methodPointerType)&Input2_SwipeLeft_m61/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19/* invoker_method */
	, Input2_t24_Input2_SwipeLeft_m61_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Input2_t24_Input2_SwipeRight_m62_ParameterInfos[] = 
{
	{"swipedir", 0, 134217749, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeRight(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeRight_m62_MethodInfo = 
{
	"SwipeRight"/* name */
	, (methodPointerType)&Input2_SwipeRight_m62/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19/* invoker_method */
	, Input2_t24_Input2_SwipeRight_m62_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Input2_t24_Input2_SwipeDown_m63_ParameterInfos[] = 
{
	{"swipedir", 0, 134217750, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeDown(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeDown_m63_MethodInfo = 
{
	"SwipeDown"/* name */
	, (methodPointerType)&Input2_SwipeDown_m63/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19/* invoker_method */
	, Input2_t24_Input2_SwipeDown_m63_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Input2_t24_Input2_SwipeUp_m64_ParameterInfos[] = 
{
	{"swipedir", 0, 134217751, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeUp(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeUp_m64_MethodInfo = 
{
	"SwipeUp"/* name */
	, (methodPointerType)&Input2_SwipeUp_m64/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19/* invoker_method */
	, Input2_t24_Input2_SwipeUp_m64_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::ContinuousSwipe()
extern const MethodInfo Input2_ContinuousSwipe_m65_MethodInfo = 
{
	"ContinuousSwipe"/* name */
	, (methodPointerType)&Input2_ContinuousSwipe_m65/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::Swipe()
extern const MethodInfo Input2_Swipe_m66_MethodInfo = 
{
	"Swipe"/* name */
	, (methodPointerType)&Input2_Swipe_m66/* method */
	, &Input2_t24_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Input2_t24_MethodInfos[] =
{
	&Input2__ctor_m48_MethodInfo,
	&Input2__cctor_m49_MethodInfo,
	&Input2_IsPointerOverUI_m50_MethodInfo,
	&Input2_TouchBegin_m51_MethodInfo,
	&Input2_GetFirstPoint_m52_MethodInfo,
	&Input2_GetSecondPoint_m53_MethodInfo,
	&Input2_GetPointerCount_m54_MethodInfo,
	&Input2_HasPointStarted_m55_MethodInfo,
	&Input2_HasPointEnded_m56_MethodInfo,
	&Input2_HasPointMoved_m57_MethodInfo,
	&Input2_Pinch_m58_MethodInfo,
	&Input2_Twist_m59_MethodInfo,
	&Input2_TwistInt_m60_MethodInfo,
	&Input2_SwipeLeft_m61_MethodInfo,
	&Input2_SwipeRight_m62_MethodInfo,
	&Input2_SwipeDown_m63_MethodInfo,
	&Input2_SwipeUp_m64_MethodInfo,
	&Input2_ContinuousSwipe_m65_MethodInfo,
	&Input2_Swipe_m66_MethodInfo,
	NULL
};
static const Il2CppMethodReference Input2_t24_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool Input2_t24_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Input2_t24_0_0_0;
extern const Il2CppType Input2_t24_1_0_0;
struct Input2_t24;
const Il2CppTypeDefinitionMetadata Input2_t24_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Input2_t24_VTable/* vtableMethods */
	, Input2_t24_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 53/* fieldStart */

};
TypeInfo Input2_t24_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Input2"/* name */
	, ""/* namespaze */
	, Input2_t24_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Input2_t24_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Input2_t24_0_0_0/* byval_arg */
	, &Input2_t24_1_0_0/* this_arg */
	, &Input2_t24_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Input2_t24)/* instance_size */
	, sizeof (Input2_t24)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Input2_t24_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
// Metadata Definition Mathf2
extern TypeInfo Mathf2_t25_il2cpp_TypeInfo;
// Mathf2
#include "AssemblyU2DCSharp_Mathf2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.ctor()
extern const MethodInfo Mathf2__ctor_m67_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mathf2__ctor_m67/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.cctor()
extern const MethodInfo Mathf2__cctor_m68_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Mathf2__cctor_m68/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RaycastHit_t102_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreenIgnore0_m69_MethodInfo = 
{
	"WhatDidWeHitCenterScreenIgnore0"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreenIgnore0_m69/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayerMask_t103_0_0_0;
extern const Il2CppType LayerMask_t103_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_WhatDidWeHitCenterScreen_m70_ParameterInfos[] = 
{
	{"lm", 0, 134217752, 0, &LayerMask_t103_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t102_LayerMask_t103 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreen_m70_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m70/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102_LayerMask_t103/* invoker_method */
	, Mathf2_t25_Mathf2_WhatDidWeHitCenterScreen_m70_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RaycastHit_t102 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreen_m71_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m71/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_WhatDidWeHit_m72_ParameterInfos[] = 
{
	{"v", 0, 134217753, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t102_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern const MethodInfo Mathf2_WhatDidWeHit_m72_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m72/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102_Vector2_t19/* invoker_method */
	, Mathf2_t25_Mathf2_WhatDidWeHit_m72_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_WhatDidWeHitCam_m73_ParameterInfos[] = 
{
	{"v", 0, 134217754, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t102_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCam(UnityEngine.Vector2)
extern const MethodInfo Mathf2_WhatDidWeHitCam_m73_MethodInfo = 
{
	"WhatDidWeHitCam"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCam_m73/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102_Vector2_t19/* invoker_method */
	, Mathf2_t25_Mathf2_WhatDidWeHitCam_m73_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType LayerMask_t103_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_WhatDidWeHit_m74_ParameterInfos[] = 
{
	{"v", 0, 134217755, 0, &Vector2_t19_0_0_0},
	{"lm", 1, 134217756, 0, &LayerMask_t103_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t102_Vector2_t19_LayerMask_t103 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHit_m74_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m74/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102_Vector2_t19_LayerMask_t103/* invoker_method */
	, Mathf2_t25_Mathf2_WhatDidWeHit_m74_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Ray_t104_0_0_0;
extern const Il2CppType Ray_t104_0_0_0;
extern const Il2CppType LayerMask_t103_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_WhatDidWeHit_m75_ParameterInfos[] = 
{
	{"ray", 0, 134217757, 0, &Ray_t104_0_0_0},
	{"lm", 1, 134217758, 0, &LayerMask_t103_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t102_Ray_t104_LayerMask_t103 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHit_m75_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m75/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t102_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t102_Ray_t104_LayerMask_t103/* invoker_method */
	, Mathf2_t25_Mathf2_WhatDidWeHit_m75_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_Pad0s_m76_ParameterInfos[] = 
{
	{"n", 0, 134217759, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::Pad0s(System.Int32)
extern const MethodInfo Mathf2_Pad0s_m76_MethodInfo = 
{
	"Pad0s"/* name */
	, (methodPointerType)&Mathf2_Pad0s_m76/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, Mathf2_t25_Mathf2_Pad0s_m76_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_Round10th_m77_ParameterInfos[] = 
{
	{"n", 0, 134217760, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::Round10th(System.Single)
extern const MethodInfo Mathf2_Round10th_m77_MethodInfo = 
{
	"Round10th"/* name */
	, (methodPointerType)&Mathf2_Round10th_m77/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112/* invoker_method */
	, Mathf2_t25_Mathf2_Round10th_m77_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GenUUID()
extern const MethodInfo Mathf2_GenUUID_m78_MethodInfo = 
{
	"GenUUID"/* name */
	, (methodPointerType)&Mathf2_GenUUID_m78/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_RoundVector3_m79_ParameterInfos[] = 
{
	{"v", 0, 134217761, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern const MethodInfo Mathf2_RoundVector3_m79_MethodInfo = 
{
	"RoundVector3"/* name */
	, (methodPointerType)&Mathf2_RoundVector3_m79/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Vector3_t14/* invoker_method */
	, Mathf2_t25_Mathf2_RoundVector3_m79_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::RandRot()
extern const MethodInfo Mathf2_RandRot_m80_MethodInfo = 
{
	"RandRot"/* name */
	, (methodPointerType)&Mathf2_RandRot_m80/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_round_m81_ParameterInfos[] = 
{
	{"f", 0, 134217762, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::round(System.Single)
extern const MethodInfo Mathf2_round_m81_MethodInfo = 
{
	"round"/* name */
	, (methodPointerType)&Mathf2_round_m81/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112/* invoker_method */
	, Mathf2_t25_Mathf2_round_m81_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Float_m82_ParameterInfos[] = 
{
	{"str", 0, 134217763, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::String2Float(System.String)
extern const MethodInfo Mathf2_String2Float_m82_MethodInfo = 
{
	"String2Float"/* name */
	, (methodPointerType)&Mathf2_String2Float_m82/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Float_m82_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_isNumeric_m83_ParameterInfos[] = 
{
	{"str", 0, 134217764, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mathf2::isNumeric(System.String)
extern const MethodInfo Mathf2_isNumeric_m83_MethodInfo = 
{
	"isNumeric"/* name */
	, (methodPointerType)&Mathf2_isNumeric_m83/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_isNumeric_m83_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Int_m84_ParameterInfos[] = 
{
	{"str", 0, 134217765, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Mathf2::String2Int(System.String)
extern const MethodInfo Mathf2_String2Int_m84_MethodInfo = 
{
	"String2Int"/* name */
	, (methodPointerType)&Mathf2_String2Int_m84/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Int_m84_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Color_m85_ParameterInfos[] = 
{
	{"str", 0, 134217766, 0, &String_t_0_0_0},
};
extern const Il2CppType Color_t98_0_0_0;
extern void* RuntimeInvoker_Color_t98_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color Mathf2::String2Color(System.String)
extern const MethodInfo Mathf2_String2Color_m85_MethodInfo = 
{
	"String2Color"/* name */
	, (methodPointerType)&Mathf2_String2Color_m85/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Color_t98_0_0_0/* return_type */
	, RuntimeInvoker_Color_t98_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Color_m85_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Vector3_m86_ParameterInfos[] = 
{
	{"str", 0, 134217767, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern const MethodInfo Mathf2_String2Vector3_m86_MethodInfo = 
{
	"String2Vector3"/* name */
	, (methodPointerType)&Mathf2_String2Vector3_m86/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Vector3_m86_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Vector2_m87_ParameterInfos[] = 
{
	{"str", 0, 134217768, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t19_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern const MethodInfo Mathf2_String2Vector2_m87_MethodInfo = 
{
	"String2Vector2"/* name */
	, (methodPointerType)&Mathf2_String2Vector2_m87/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Vector2_m87_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_RoundFraction_m88_ParameterInfos[] = 
{
	{"val", 0, 134217769, 0, &Single_t112_0_0_0},
	{"denominator", 1, 134217770, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern const MethodInfo Mathf2_RoundFraction_m88_MethodInfo = 
{
	"RoundFraction"/* name */
	, (methodPointerType)&Mathf2_RoundFraction_m88/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112/* invoker_method */
	, Mathf2_t25_Mathf2_RoundFraction_m88_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_String2Quat_m89_ParameterInfos[] = 
{
	{"str", 0, 134217771, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t22_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern const MethodInfo Mathf2_String2Quat_m89_MethodInfo = 
{
	"String2Quat"/* name */
	, (methodPointerType)&Mathf2_String2Quat_m89/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_String2Quat_m89_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_UnsignedVector3_m90_ParameterInfos[] = 
{
	{"v", 0, 134217772, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern const MethodInfo Mathf2_UnsignedVector3_m90_MethodInfo = 
{
	"UnsignedVector3"/* name */
	, (methodPointerType)&Mathf2_UnsignedVector3_m90/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Vector3_t14/* invoker_method */
	, Mathf2_t25_Mathf2_UnsignedVector3_m90_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetUnixTime()
extern const MethodInfo Mathf2_GetUnixTime_m91_MethodInfo = 
{
	"GetUnixTime"/* name */
	, (methodPointerType)&Mathf2_GetUnixTime_m91/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_GetPositionString_m92_ParameterInfos[] = 
{
	{"t", 0, 134217773, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetPositionString_m92_MethodInfo = 
{
	"GetPositionString"/* name */
	, (methodPointerType)&Mathf2_GetPositionString_m92/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_GetPositionString_m92_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_GetRotationString_m93_ParameterInfos[] = 
{
	{"t", 0, 134217774, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetRotationString_m93_MethodInfo = 
{
	"GetRotationString"/* name */
	, (methodPointerType)&Mathf2_GetRotationString_m93/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_GetRotationString_m93_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t25_Mathf2_GetScaleString_m94_ParameterInfos[] = 
{
	{"t", 0, 134217775, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetScaleString_m94_MethodInfo = 
{
	"GetScaleString"/* name */
	, (methodPointerType)&Mathf2_GetScaleString_m94/* method */
	, &Mathf2_t25_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t25_Mathf2_GetScaleString_m94_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mathf2_t25_MethodInfos[] =
{
	&Mathf2__ctor_m67_MethodInfo,
	&Mathf2__cctor_m68_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreenIgnore0_m69_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m70_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m71_MethodInfo,
	&Mathf2_WhatDidWeHit_m72_MethodInfo,
	&Mathf2_WhatDidWeHitCam_m73_MethodInfo,
	&Mathf2_WhatDidWeHit_m74_MethodInfo,
	&Mathf2_WhatDidWeHit_m75_MethodInfo,
	&Mathf2_Pad0s_m76_MethodInfo,
	&Mathf2_Round10th_m77_MethodInfo,
	&Mathf2_GenUUID_m78_MethodInfo,
	&Mathf2_RoundVector3_m79_MethodInfo,
	&Mathf2_RandRot_m80_MethodInfo,
	&Mathf2_round_m81_MethodInfo,
	&Mathf2_String2Float_m82_MethodInfo,
	&Mathf2_isNumeric_m83_MethodInfo,
	&Mathf2_String2Int_m84_MethodInfo,
	&Mathf2_String2Color_m85_MethodInfo,
	&Mathf2_String2Vector3_m86_MethodInfo,
	&Mathf2_String2Vector2_m87_MethodInfo,
	&Mathf2_RoundFraction_m88_MethodInfo,
	&Mathf2_String2Quat_m89_MethodInfo,
	&Mathf2_UnsignedVector3_m90_MethodInfo,
	&Mathf2_GetUnixTime_m91_MethodInfo,
	&Mathf2_GetPositionString_m92_MethodInfo,
	&Mathf2_GetRotationString_m93_MethodInfo,
	&Mathf2_GetScaleString_m94_MethodInfo,
	NULL
};
static const Il2CppMethodReference Mathf2_t25_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Mathf2_t25_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Mathf2_t25_0_0_0;
extern const Il2CppType Mathf2_t25_1_0_0;
struct Mathf2_t25;
const Il2CppTypeDefinitionMetadata Mathf2_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Mathf2_t25_VTable/* vtableMethods */
	, Mathf2_t25_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 60/* fieldStart */

};
TypeInfo Mathf2_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mathf2"/* name */
	, ""/* namespaze */
	, Mathf2_t25_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Mathf2_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mathf2_t25_0_0_0/* byval_arg */
	, &Mathf2_t25_1_0_0/* this_arg */
	, &Mathf2_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mathf2_t25)/* instance_size */
	, sizeof (Mathf2_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Mathf2_t25_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
// Metadata Definition Raycaster
extern TypeInfo Raycaster_t26_il2cpp_TypeInfo;
// Raycaster
#include "AssemblyU2DCSharp_RaycasterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::.ctor()
extern const MethodInfo Raycaster__ctor_m95_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Raycaster__ctor_m95/* method */
	, &Raycaster_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Raycaster_t26_Raycaster_RaycastIt_m96_ParameterInfos[] = 
{
	{"cam", 0, 134217776, 0, &Camera_t3_0_0_0},
	{"pos", 1, 134217777, 0, &Vector2_t19_0_0_0},
	{"distance", 2, 134217778, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Vector2_t19_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern const MethodInfo Raycaster_RaycastIt_m96_MethodInfo = 
{
	"RaycastIt"/* name */
	, (methodPointerType)&Raycaster_RaycastIt_m96/* method */
	, &Raycaster_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Vector2_t19_Single_t112/* invoker_method */
	, Raycaster_t26_Raycaster_RaycastIt_m96_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Raycaster_t26_MethodInfos[] =
{
	&Raycaster__ctor_m95_MethodInfo,
	&Raycaster_RaycastIt_m96_MethodInfo,
	NULL
};
static const Il2CppMethodReference Raycaster_t26_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool Raycaster_t26_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Raycaster_t26_0_0_0;
extern const Il2CppType Raycaster_t26_1_0_0;
struct Raycaster_t26;
const Il2CppTypeDefinitionMetadata Raycaster_t26_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Raycaster_t26_VTable/* vtableMethods */
	, Raycaster_t26_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Raycaster_t26_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Raycaster"/* name */
	, ""/* namespaze */
	, Raycaster_t26_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Raycaster_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Raycaster_t26_0_0_0/* byval_arg */
	, &Raycaster_t26_1_0_0/* this_arg */
	, &Raycaster_t26_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Raycaster_t26)/* instance_size */
	, sizeof (Raycaster_t26)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
// Metadata Definition SetRenderQueue
extern TypeInfo SetRenderQueue_t28_il2cpp_TypeInfo;
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueueMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::.ctor()
extern const MethodInfo SetRenderQueue__ctor_m97_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueue__ctor_m97/* method */
	, &SetRenderQueue_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::Awake()
extern const MethodInfo SetRenderQueue_Awake_m98_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueue_Awake_m98/* method */
	, &SetRenderQueue_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRenderQueue_t28_MethodInfos[] =
{
	&SetRenderQueue__ctor_m97_MethodInfo,
	&SetRenderQueue_Awake_m98_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRenderQueue_t28_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool SetRenderQueue_t28_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRenderQueue_t28_0_0_0;
extern const Il2CppType SetRenderQueue_t28_1_0_0;
struct SetRenderQueue_t28;
const Il2CppTypeDefinitionMetadata SetRenderQueue_t28_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRenderQueue_t28_VTable/* vtableMethods */
	, SetRenderQueue_t28_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 61/* fieldStart */

};
TypeInfo SetRenderQueue_t28_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueue"/* name */
	, ""/* namespaze */
	, SetRenderQueue_t28_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRenderQueue_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRenderQueue_t28_0_0_0/* byval_arg */
	, &SetRenderQueue_t28_1_0_0/* this_arg */
	, &SetRenderQueue_t28_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueue_t28)/* instance_size */
	, sizeof (SetRenderQueue_t28)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
// Metadata Definition SetRenderQueueChildren
extern TypeInfo SetRenderQueueChildren_t29_il2cpp_TypeInfo;
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildrenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::.ctor()
extern const MethodInfo SetRenderQueueChildren__ctor_m99_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueueChildren__ctor_m99/* method */
	, &SetRenderQueueChildren_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Start()
extern const MethodInfo SetRenderQueueChildren_Start_m100_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Start_m100/* method */
	, &SetRenderQueueChildren_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Awake()
extern const MethodInfo SetRenderQueueChildren_Awake_m101_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Awake_m101/* method */
	, &SetRenderQueueChildren_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRenderQueueChildren_t29_MethodInfos[] =
{
	&SetRenderQueueChildren__ctor_m99_MethodInfo,
	&SetRenderQueueChildren_Start_m100_MethodInfo,
	&SetRenderQueueChildren_Awake_m101_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRenderQueueChildren_t29_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool SetRenderQueueChildren_t29_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRenderQueueChildren_t29_0_0_0;
extern const Il2CppType SetRenderQueueChildren_t29_1_0_0;
struct SetRenderQueueChildren_t29;
const Il2CppTypeDefinitionMetadata SetRenderQueueChildren_t29_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRenderQueueChildren_t29_VTable/* vtableMethods */
	, SetRenderQueueChildren_t29_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 62/* fieldStart */

};
TypeInfo SetRenderQueueChildren_t29_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueueChildren"/* name */
	, ""/* namespaze */
	, SetRenderQueueChildren_t29_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRenderQueueChildren_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRenderQueueChildren_t29_0_0_0/* byval_arg */
	, &SetRenderQueueChildren_t29_1_0_0/* this_arg */
	, &SetRenderQueueChildren_t29_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueueChildren_t29)/* instance_size */
	, sizeof (SetRenderQueueChildren_t29)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
// Metadata Definition SetRotationManually
extern TypeInfo SetRotationManually_t32_il2cpp_TypeInfo;
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManuallyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::.ctor()
extern const MethodInfo SetRotationManually__ctor_m102_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRotationManually__ctor_m102/* method */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderX()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderX_m103_MethodInfo = 
{
	"UpdateRotationFromSliderX"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderX_m103/* method */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderY()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderY_m104_MethodInfo = 
{
	"UpdateRotationFromSliderY"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderY_m104/* method */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderZ()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderZ_m105_MethodInfo = 
{
	"UpdateRotationFromSliderZ"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderZ_m105/* method */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::Update()
extern const MethodInfo SetRotationManually_Update_m106_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SetRotationManually_Update_m106/* method */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRotationManually_t32_MethodInfos[] =
{
	&SetRotationManually__ctor_m102_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderX_m103_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderY_m104_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderZ_m105_MethodInfo,
	&SetRotationManually_Update_m106_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRotationManually_t32_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool SetRotationManually_t32_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRotationManually_t32_0_0_0;
extern const Il2CppType SetRotationManually_t32_1_0_0;
struct SetRotationManually_t32;
const Il2CppTypeDefinitionMetadata SetRotationManually_t32_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRotationManually_t32_VTable/* vtableMethods */
	, SetRotationManually_t32_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 63/* fieldStart */

};
TypeInfo SetRotationManually_t32_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRotationManually"/* name */
	, ""/* namespaze */
	, SetRotationManually_t32_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRotationManually_t32_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRotationManually_t32_0_0_0/* byval_arg */
	, &SetRotationManually_t32_1_0_0/* this_arg */
	, &SetRotationManually_t32_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRotationManually_t32)/* instance_size */
	, sizeof (SetRotationManually_t32)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
// Metadata Definition WorldLoop
extern TypeInfo WorldLoop_t33_il2cpp_TypeInfo;
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoopMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::.ctor()
extern const MethodInfo WorldLoop__ctor_m107_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldLoop__ctor_m107/* method */
	, &WorldLoop_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Awake()
extern const MethodInfo WorldLoop_Awake_m108_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldLoop_Awake_m108/* method */
	, &WorldLoop_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Start()
extern const MethodInfo WorldLoop_Start_m109_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WorldLoop_Start_m109/* method */
	, &WorldLoop_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Update()
extern const MethodInfo WorldLoop_Update_m110_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WorldLoop_Update_m110/* method */
	, &WorldLoop_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WorldLoop_t33_MethodInfos[] =
{
	&WorldLoop__ctor_m107_MethodInfo,
	&WorldLoop_Awake_m108_MethodInfo,
	&WorldLoop_Start_m109_MethodInfo,
	&WorldLoop_Update_m110_MethodInfo,
	NULL
};
static const Il2CppMethodReference WorldLoop_t33_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool WorldLoop_t33_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WorldLoop_t33_0_0_0;
extern const Il2CppType WorldLoop_t33_1_0_0;
struct WorldLoop_t33;
const Il2CppTypeDefinitionMetadata WorldLoop_t33_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WorldLoop_t33_VTable/* vtableMethods */
	, WorldLoop_t33_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 67/* fieldStart */

};
TypeInfo WorldLoop_t33_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldLoop"/* name */
	, ""/* namespaze */
	, WorldLoop_t33_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WorldLoop_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WorldLoop_t33_0_0_0/* byval_arg */
	, &WorldLoop_t33_1_0_0/* this_arg */
	, &WorldLoop_t33_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldLoop_t33)/* instance_size */
	, sizeof (WorldLoop_t33)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
// Metadata Definition WorldNav
extern TypeInfo WorldNav_t34_il2cpp_TypeInfo;
// WorldNav
#include "AssemblyU2DCSharp_WorldNavMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.ctor()
extern const MethodInfo WorldNav__ctor_m111_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldNav__ctor_m111/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.cctor()
extern const MethodInfo WorldNav__cctor_m112_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&WorldNav__cctor_m112/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Awake()
extern const MethodInfo WorldNav_Awake_m113_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldNav_Awake_m113/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Update()
extern const MethodInfo WorldNav_Update_m114_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WorldNav_Update_m114/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo WorldNav_t34_WorldNav_Walk_m115_ParameterInfos[] = 
{
	{"dir", 0, 134217779, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Walk(System.Int32)
extern const MethodInfo WorldNav_Walk_m115_MethodInfo = 
{
	"Walk"/* name */
	, (methodPointerType)&WorldNav_Walk_m115/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, WorldNav_t34_WorldNav_Walk_m115_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo WorldNav_t34_WorldNav_WalkDirTrue_m116_ParameterInfos[] = 
{
	{"dir", 0, 134217780, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern const MethodInfo WorldNav_WalkDirTrue_m116_MethodInfo = 
{
	"WalkDirTrue"/* name */
	, (methodPointerType)&WorldNav_WalkDirTrue_m116/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, WorldNav_t34_WorldNav_WalkDirTrue_m116_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo WorldNav_t34_WorldNav_WalkDirFalse_m117_ParameterInfos[] = 
{
	{"dir", 0, 134217781, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern const MethodInfo WorldNav_WalkDirFalse_m117_MethodInfo = 
{
	"WalkDirFalse"/* name */
	, (methodPointerType)&WorldNav_WalkDirFalse_m117/* method */
	, &WorldNav_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, WorldNav_t34_WorldNav_WalkDirFalse_m117_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WorldNav_t34_MethodInfos[] =
{
	&WorldNav__ctor_m111_MethodInfo,
	&WorldNav__cctor_m112_MethodInfo,
	&WorldNav_Awake_m113_MethodInfo,
	&WorldNav_Update_m114_MethodInfo,
	&WorldNav_Walk_m115_MethodInfo,
	&WorldNav_WalkDirTrue_m116_MethodInfo,
	&WorldNav_WalkDirFalse_m117_MethodInfo,
	NULL
};
static const Il2CppMethodReference WorldNav_t34_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool WorldNav_t34_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WorldNav_t34_0_0_0;
extern const Il2CppType WorldNav_t34_1_0_0;
struct WorldNav_t34;
const Il2CppTypeDefinitionMetadata WorldNav_t34_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WorldNav_t34_VTable/* vtableMethods */
	, WorldNav_t34_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 69/* fieldStart */

};
TypeInfo WorldNav_t34_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldNav"/* name */
	, ""/* namespaze */
	, WorldNav_t34_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WorldNav_t34_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WorldNav_t34_0_0_0/* byval_arg */
	, &WorldNav_t34_1_0_0/* this_arg */
	, &WorldNav_t34_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldNav_t34)/* instance_size */
	, sizeof (WorldNav_t34)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WorldNav_t34_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Basics
#include "AssemblyU2DCSharp_Basics.h"
// Metadata Definition Basics
extern TypeInfo Basics_t35_il2cpp_TypeInfo;
// Basics
#include "AssemblyU2DCSharp_BasicsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::.ctor()
extern const MethodInfo Basics__ctor_m118_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Basics__ctor_m118/* method */
	, &Basics_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::Start()
extern const MethodInfo Basics_Start_m119_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Basics_Start_m119/* method */
	, &Basics_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Basics::<Start>m__0()
extern const MethodInfo Basics_U3CStartU3Em__0_m120_MethodInfo = 
{
	"<Start>m__0"/* name */
	, (methodPointerType)&Basics_U3CStartU3Em__0_m120/* method */
	, &Basics_t35_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14/* invoker_method */
	, NULL/* parameters */
	, 5/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Basics_t35_Basics_U3CStartU3Em__1_m121_ParameterInfos[] = 
{
	{"x", 0, 134217782, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern const MethodInfo Basics_U3CStartU3Em__1_m121_MethodInfo = 
{
	"<Start>m__1"/* name */
	, (methodPointerType)&Basics_U3CStartU3Em__1_m121/* method */
	, &Basics_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14/* invoker_method */
	, Basics_t35_Basics_U3CStartU3Em__1_m121_ParameterInfos/* parameters */
	, 6/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Basics_t35_MethodInfos[] =
{
	&Basics__ctor_m118_MethodInfo,
	&Basics_Start_m119_MethodInfo,
	&Basics_U3CStartU3Em__0_m120_MethodInfo,
	&Basics_U3CStartU3Em__1_m121_MethodInfo,
	NULL
};
static const Il2CppMethodReference Basics_t35_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool Basics_t35_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Basics_t35_0_0_0;
extern const Il2CppType Basics_t35_1_0_0;
struct Basics_t35;
const Il2CppTypeDefinitionMetadata Basics_t35_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Basics_t35_VTable/* vtableMethods */
	, Basics_t35_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 76/* fieldStart */

};
TypeInfo Basics_t35_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Basics"/* name */
	, ""/* namespaze */
	, Basics_t35_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Basics_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Basics_t35_0_0_0/* byval_arg */
	, &Basics_t35_1_0_0/* this_arg */
	, &Basics_t35_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Basics_t35)/* instance_size */
	, sizeof (Basics_t35)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
// Metadata Definition Sequences
extern TypeInfo Sequences_t36_il2cpp_TypeInfo;
// Sequences
#include "AssemblyU2DCSharp_SequencesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Sequences::.ctor()
extern const MethodInfo Sequences__ctor_m122_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Sequences__ctor_m122/* method */
	, &Sequences_t36_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Sequences::Start()
extern const MethodInfo Sequences_Start_m123_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Sequences_Start_m123/* method */
	, &Sequences_t36_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Sequences_t36_MethodInfos[] =
{
	&Sequences__ctor_m122_MethodInfo,
	&Sequences_Start_m123_MethodInfo,
	NULL
};
static const Il2CppMethodReference Sequences_t36_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool Sequences_t36_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Sequences_t36_0_0_0;
extern const Il2CppType Sequences_t36_1_0_0;
struct Sequences_t36;
const Il2CppTypeDefinitionMetadata Sequences_t36_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Sequences_t36_VTable/* vtableMethods */
	, Sequences_t36_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 78/* fieldStart */

};
TypeInfo Sequences_t36_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sequences"/* name */
	, ""/* namespaze */
	, Sequences_t36_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Sequences_t36_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sequences_t36_0_0_0/* byval_arg */
	, &Sequences_t36_1_0_0/* this_arg */
	, &Sequences_t36_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sequences_t36)/* instance_size */
	, sizeof (Sequences_t36)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
// Metadata Definition OrbitCamera
extern TypeInfo OrbitCamera_t37_il2cpp_TypeInfo;
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::.ctor()
extern const MethodInfo OrbitCamera__ctor_m124_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrbitCamera__ctor_m124/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Start()
extern const MethodInfo OrbitCamera_Start_m125_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&OrbitCamera_Start_m125/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Update()
extern const MethodInfo OrbitCamera_Update_m126_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&OrbitCamera_Update_m126/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::LateUpdate()
extern const MethodInfo OrbitCamera_LateUpdate_m127_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&OrbitCamera_LateUpdate_m127/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::HandlePlayerInput()
extern const MethodInfo OrbitCamera_HandlePlayerInput_m128_MethodInfo = 
{
	"HandlePlayerInput"/* name */
	, (methodPointerType)&OrbitCamera_HandlePlayerInput_m128/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::CalculateDesiredPosition()
extern const MethodInfo OrbitCamera_CalculateDesiredPosition_m129_MethodInfo = 
{
	"CalculateDesiredPosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculateDesiredPosition_m129/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo OrbitCamera_t37_OrbitCamera_CalculatePosition_m130_ParameterInfos[] = 
{
	{"rotationX", 0, 134217783, 0, &Single_t112_0_0_0},
	{"rotationY", 1, 134217784, 0, &Single_t112_0_0_0},
	{"distance", 2, 134217785, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern const MethodInfo OrbitCamera_CalculatePosition_m130_MethodInfo = 
{
	"CalculatePosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculatePosition_m130/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Single_t112_Single_t112_Single_t112/* invoker_method */
	, OrbitCamera_t37_OrbitCamera_CalculatePosition_m130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::UpdatePosition()
extern const MethodInfo OrbitCamera_UpdatePosition_m131_MethodInfo = 
{
	"UpdatePosition"/* name */
	, (methodPointerType)&OrbitCamera_UpdatePosition_m131/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Reset()
extern const MethodInfo OrbitCamera_Reset_m132_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&OrbitCamera_Reset_m132/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo OrbitCamera_t37_OrbitCamera_ClampAngle_m133_ParameterInfos[] = 
{
	{"angle", 0, 134217786, 0, &Single_t112_0_0_0},
	{"min", 1, 134217787, 0, &Single_t112_0_0_0},
	{"max", 2, 134217788, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern const MethodInfo OrbitCamera_ClampAngle_m133_MethodInfo = 
{
	"ClampAngle"/* name */
	, (methodPointerType)&OrbitCamera_ClampAngle_m133/* method */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, OrbitCamera_t37_OrbitCamera_ClampAngle_m133_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrbitCamera_t37_MethodInfos[] =
{
	&OrbitCamera__ctor_m124_MethodInfo,
	&OrbitCamera_Start_m125_MethodInfo,
	&OrbitCamera_Update_m126_MethodInfo,
	&OrbitCamera_LateUpdate_m127_MethodInfo,
	&OrbitCamera_HandlePlayerInput_m128_MethodInfo,
	&OrbitCamera_CalculateDesiredPosition_m129_MethodInfo,
	&OrbitCamera_CalculatePosition_m130_MethodInfo,
	&OrbitCamera_UpdatePosition_m131_MethodInfo,
	&OrbitCamera_Reset_m132_MethodInfo,
	&OrbitCamera_ClampAngle_m133_MethodInfo,
	NULL
};
static const Il2CppMethodReference OrbitCamera_t37_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool OrbitCamera_t37_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OrbitCamera_t37_0_0_0;
extern const Il2CppType OrbitCamera_t37_1_0_0;
struct OrbitCamera_t37;
const Il2CppTypeDefinitionMetadata OrbitCamera_t37_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, OrbitCamera_t37_VTable/* vtableMethods */
	, OrbitCamera_t37_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 79/* fieldStart */

};
TypeInfo OrbitCamera_t37_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrbitCamera"/* name */
	, ""/* namespaze */
	, OrbitCamera_t37_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrbitCamera_t37_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrbitCamera_t37_0_0_0/* byval_arg */
	, &OrbitCamera_t37_1_0_0/* this_arg */
	, &OrbitCamera_t37_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrbitCamera_t37)/* instance_size */
	, sizeof (OrbitCamera_t37)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
// Metadata Definition TestParticles
extern TypeInfo TestParticles_t38_il2cpp_TypeInfo;
// TestParticles
#include "AssemblyU2DCSharp_TestParticlesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::.ctor()
extern const MethodInfo TestParticles__ctor_m134_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TestParticles__ctor_m134/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Start()
extern const MethodInfo TestParticles_Start_m135_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TestParticles_Start_m135/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Update()
extern const MethodInfo TestParticles_Update_m136_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TestParticles_Update_m136/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::OnGUI()
extern const MethodInfo TestParticles_OnGUI_m137_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&TestParticles_OnGUI_m137/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ShowParticle()
extern const MethodInfo TestParticles_ShowParticle_m138_MethodInfo = 
{
	"ShowParticle"/* name */
	, (methodPointerType)&TestParticles_ShowParticle_m138/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TestParticles_t38_TestParticles_ParticleInformationWindow_m139_ParameterInfos[] = 
{
	{"id", 0, 134217789, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern const MethodInfo TestParticles_ParticleInformationWindow_m139_MethodInfo = 
{
	"ParticleInformationWindow"/* name */
	, (methodPointerType)&TestParticles_ParticleInformationWindow_m139/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TestParticles_t38_TestParticles_ParticleInformationWindow_m139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TestParticles_t38_TestParticles_InfoWindow_m140_ParameterInfos[] = 
{
	{"id", 0, 134217790, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::InfoWindow(System.Int32)
extern const MethodInfo TestParticles_InfoWindow_m140_MethodInfo = 
{
	"InfoWindow"/* name */
	, (methodPointerType)&TestParticles_InfoWindow_m140/* method */
	, &TestParticles_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TestParticles_t38_TestParticles_InfoWindow_m140_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TestParticles_t38_MethodInfos[] =
{
	&TestParticles__ctor_m134_MethodInfo,
	&TestParticles_Start_m135_MethodInfo,
	&TestParticles_Update_m136_MethodInfo,
	&TestParticles_OnGUI_m137_MethodInfo,
	&TestParticles_ShowParticle_m138_MethodInfo,
	&TestParticles_ParticleInformationWindow_m139_MethodInfo,
	&TestParticles_InfoWindow_m140_MethodInfo,
	NULL
};
static const Il2CppMethodReference TestParticles_t38_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool TestParticles_t38_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TestParticles_t38_0_0_0;
extern const Il2CppType TestParticles_t38_1_0_0;
struct TestParticles_t38;
const Il2CppTypeDefinitionMetadata TestParticles_t38_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TestParticles_t38_VTable/* vtableMethods */
	, TestParticles_t38_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 101/* fieldStart */

};
TypeInfo TestParticles_t38_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TestParticles"/* name */
	, ""/* namespaze */
	, TestParticles_t38_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TestParticles_t38_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TestParticles_t38_0_0_0/* byval_arg */
	, &TestParticles_t38_1_0_0/* this_arg */
	, &TestParticles_t38_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TestParticles_t38)/* instance_size */
	, sizeof (TestParticles_t38)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
// Metadata Definition Vuforia.BackgroundPlaneBehaviour
extern TypeInfo BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo;
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern const MethodInfo BackgroundPlaneBehaviour__ctor_m141_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackgroundPlaneBehaviour__ctor_m141/* method */
	, &BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BackgroundPlaneBehaviour_t39_MethodInfos[] =
{
	&BackgroundPlaneBehaviour__ctor_m141_MethodInfo,
	NULL
};
extern const MethodInfo BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m569_MethodInfo;
static const Il2CppMethodReference BackgroundPlaneBehaviour_t39_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m569_MethodInfo,
};
static bool BackgroundPlaneBehaviour_t39_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVideoBackgroundEventHandler_t178_0_0_0;
static Il2CppInterfaceOffsetPair BackgroundPlaneBehaviour_t39_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t178_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BackgroundPlaneBehaviour_t39_0_0_0;
extern const Il2CppType BackgroundPlaneBehaviour_t39_1_0_0;
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t40_0_0_0;
struct BackgroundPlaneBehaviour_t39;
const Il2CppTypeDefinitionMetadata BackgroundPlaneBehaviour_t39_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BackgroundPlaneBehaviour_t39_InterfacesOffsets/* interfaceOffsets */
	, &BackgroundPlaneAbstractBehaviour_t40_0_0_0/* parent */
	, BackgroundPlaneBehaviour_t39_VTable/* vtableMethods */
	, BackgroundPlaneBehaviour_t39_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackgroundPlaneBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, BackgroundPlaneBehaviour_t39_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackgroundPlaneBehaviour_t39_0_0_0/* byval_arg */
	, &BackgroundPlaneBehaviour_t39_1_0_0/* this_arg */
	, &BackgroundPlaneBehaviour_t39_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackgroundPlaneBehaviour_t39)/* instance_size */
	, sizeof (BackgroundPlaneBehaviour_t39)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
// Metadata Definition Vuforia.CloudRecoBehaviour
extern TypeInfo CloudRecoBehaviour_t41_il2cpp_TypeInfo;
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern const MethodInfo CloudRecoBehaviour__ctor_m142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CloudRecoBehaviour__ctor_m142/* method */
	, &CloudRecoBehaviour_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CloudRecoBehaviour_t41_MethodInfos[] =
{
	&CloudRecoBehaviour__ctor_m142_MethodInfo,
	NULL
};
static const Il2CppMethodReference CloudRecoBehaviour_t41_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool CloudRecoBehaviour_t41_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CloudRecoBehaviour_t41_0_0_0;
extern const Il2CppType CloudRecoBehaviour_t41_1_0_0;
extern const Il2CppType CloudRecoAbstractBehaviour_t42_0_0_0;
struct CloudRecoBehaviour_t41;
const Il2CppTypeDefinitionMetadata CloudRecoBehaviour_t41_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CloudRecoAbstractBehaviour_t42_0_0_0/* parent */
	, CloudRecoBehaviour_t41_VTable/* vtableMethods */
	, CloudRecoBehaviour_t41_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CloudRecoBehaviour_t41_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CloudRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CloudRecoBehaviour_t41_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CloudRecoBehaviour_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CloudRecoBehaviour_t41_0_0_0/* byval_arg */
	, &CloudRecoBehaviour_t41_1_0_0/* this_arg */
	, &CloudRecoBehaviour_t41_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CloudRecoBehaviour_t41)/* instance_size */
	, sizeof (CloudRecoBehaviour_t41)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
// Metadata Definition Vuforia.CylinderTargetBehaviour
extern TypeInfo CylinderTargetBehaviour_t43_il2cpp_TypeInfo;
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern const MethodInfo CylinderTargetBehaviour__ctor_m143_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CylinderTargetBehaviour__ctor_m143/* method */
	, &CylinderTargetBehaviour_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CylinderTargetBehaviour_t43_MethodInfos[] =
{
	&CylinderTargetBehaviour__ctor_m143_MethodInfo,
	NULL
};
extern const MethodInfo TrackableBehaviour_get_TrackableName_m570_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo;
extern const MethodInfo TrackableBehaviour_get_Trackable_m573_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m581_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m582_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m583_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m584_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_OnTrackerUpdate_m586_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m587_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m588_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_CorrectScaleImpl_m589_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_OnDrawGizmos_m614_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m615_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m616_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m617_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m618_MethodInfo;
static const Il2CppMethodReference CylinderTargetBehaviour_t43_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m581_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m582_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m583_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m584_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m586_MethodInfo,
	&CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m587_MethodInfo,
	&CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m588_MethodInfo,
	&CylinderTargetAbstractBehaviour_CorrectScaleImpl_m589_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m614_MethodInfo,
	&CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m615_MethodInfo,
	&CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m616_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m617_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m618_MethodInfo,
};
static bool CylinderTargetBehaviour_t43_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorCylinderTargetBehaviour_t179_0_0_0;
extern const Il2CppType IEditorDataSetTrackableBehaviour_t180_0_0_0;
extern const Il2CppType IEditorTrackableBehaviour_t181_0_0_0;
extern const Il2CppType WorldCenterTrackableBehaviour_t182_0_0_0;
static Il2CppInterfaceOffsetPair CylinderTargetBehaviour_t43_InterfacesOffsets[] = 
{
	{ &IEditorCylinderTargetBehaviour_t179_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t180_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CylinderTargetBehaviour_t43_0_0_0;
extern const Il2CppType CylinderTargetBehaviour_t43_1_0_0;
extern const Il2CppType CylinderTargetAbstractBehaviour_t44_0_0_0;
struct CylinderTargetBehaviour_t43;
const Il2CppTypeDefinitionMetadata CylinderTargetBehaviour_t43_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CylinderTargetBehaviour_t43_InterfacesOffsets/* interfaceOffsets */
	, &CylinderTargetAbstractBehaviour_t44_0_0_0/* parent */
	, CylinderTargetBehaviour_t43_VTable/* vtableMethods */
	, CylinderTargetBehaviour_t43_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CylinderTargetBehaviour_t43_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CylinderTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CylinderTargetBehaviour_t43_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CylinderTargetBehaviour_t43_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CylinderTargetBehaviour_t43_0_0_0/* byval_arg */
	, &CylinderTargetBehaviour_t43_1_0_0/* this_arg */
	, &CylinderTargetBehaviour_t43_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CylinderTargetBehaviour_t43)/* instance_size */
	, sizeof (CylinderTargetBehaviour_t43)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 55/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
// Metadata Definition Vuforia.DataSetLoadBehaviour
extern TypeInfo DataSetLoadBehaviour_t45_il2cpp_TypeInfo;
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern const MethodInfo DataSetLoadBehaviour__ctor_m144_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DataSetLoadBehaviour__ctor_m144/* method */
	, &DataSetLoadBehaviour_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern const MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145_MethodInfo = 
{
	"AddOSSpecificExternalDatasetSearchDirs"/* name */
	, (methodPointerType)&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145/* method */
	, &DataSetLoadBehaviour_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DataSetLoadBehaviour_t45_MethodInfos[] =
{
	&DataSetLoadBehaviour__ctor_m144_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145_MethodInfo,
	NULL
};
extern const MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145_MethodInfo;
static const Il2CppMethodReference DataSetLoadBehaviour_t45_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145_MethodInfo,
};
static bool DataSetLoadBehaviour_t45_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DataSetLoadBehaviour_t45_0_0_0;
extern const Il2CppType DataSetLoadBehaviour_t45_1_0_0;
extern const Il2CppType DataSetLoadAbstractBehaviour_t46_0_0_0;
struct DataSetLoadBehaviour_t45;
const Il2CppTypeDefinitionMetadata DataSetLoadBehaviour_t45_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DataSetLoadAbstractBehaviour_t46_0_0_0/* parent */
	, DataSetLoadBehaviour_t45_VTable/* vtableMethods */
	, DataSetLoadBehaviour_t45_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DataSetLoadBehaviour_t45_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataSetLoadBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, DataSetLoadBehaviour_t45_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DataSetLoadBehaviour_t45_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DataSetLoadBehaviour_t45_0_0_0/* byval_arg */
	, &DataSetLoadBehaviour_t45_1_0_0/* this_arg */
	, &DataSetLoadBehaviour_t45_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DataSetLoadBehaviour_t45)/* instance_size */
	, sizeof (DataSetLoadBehaviour_t45)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
// Metadata Definition Vuforia.DefaultInitializationErrorHandler
extern TypeInfo DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo;
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern const MethodInfo DefaultInitializationErrorHandler__ctor_m146_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler__ctor_m146/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const MethodInfo DefaultInitializationErrorHandler_Awake_m147_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_Awake_m147/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern const MethodInfo DefaultInitializationErrorHandler_OnGUI_m148_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnGUI_m148/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const MethodInfo DefaultInitializationErrorHandler_OnDestroy_m149_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnDestroy_m149/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_DrawWindowContent_m150_ParameterInfos[] = 
{
	{"id", 0, 134217791, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern const MethodInfo DefaultInitializationErrorHandler_DrawWindowContent_m150_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_DrawWindowContent_m150/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_DrawWindowContent_m150_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InitError_t183_0_0_0;
extern const Il2CppType InitError_t183_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_SetErrorCode_m151_ParameterInfos[] = 
{
	{"errorCode", 0, 134217792, 0, &InitError_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern const MethodInfo DefaultInitializationErrorHandler_SetErrorCode_m151_MethodInfo = 
{
	"SetErrorCode"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorCode_m151/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_SetErrorCode_m151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_SetErrorOccurred_m152_ParameterInfos[] = 
{
	{"errorOccurred", 0, 134217793, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern const MethodInfo DefaultInitializationErrorHandler_SetErrorOccurred_m152_MethodInfo = 
{
	"SetErrorOccurred"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorOccurred_m152/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_SetErrorOccurred_m152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InitError_t183_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_OnQCARInitializationError_m153_ParameterInfos[] = 
{
	{"initError", 0, 134217794, 0, &InitError_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern const MethodInfo DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo = 
{
	"OnQCARInitializationError"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnQCARInitializationError_m153/* method */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DefaultInitializationErrorHandler_t47_DefaultInitializationErrorHandler_OnQCARInitializationError_m153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultInitializationErrorHandler_t47_MethodInfos[] =
{
	&DefaultInitializationErrorHandler__ctor_m146_MethodInfo,
	&DefaultInitializationErrorHandler_Awake_m147_MethodInfo,
	&DefaultInitializationErrorHandler_OnGUI_m148_MethodInfo,
	&DefaultInitializationErrorHandler_OnDestroy_m149_MethodInfo,
	&DefaultInitializationErrorHandler_DrawWindowContent_m150_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorCode_m151_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorOccurred_m152_MethodInfo,
	&DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultInitializationErrorHandler_t47_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool DefaultInitializationErrorHandler_t47_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultInitializationErrorHandler_t47_0_0_0;
extern const Il2CppType DefaultInitializationErrorHandler_t47_1_0_0;
struct DefaultInitializationErrorHandler_t47;
const Il2CppTypeDefinitionMetadata DefaultInitializationErrorHandler_t47_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultInitializationErrorHandler_t47_VTable/* vtableMethods */
	, DefaultInitializationErrorHandler_t47_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 115/* fieldStart */

};
TypeInfo DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultInitializationErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultInitializationErrorHandler_t47_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultInitializationErrorHandler_t47_0_0_0/* byval_arg */
	, &DefaultInitializationErrorHandler_t47_1_0_0/* this_arg */
	, &DefaultInitializationErrorHandler_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultInitializationErrorHandler_t47)/* instance_size */
	, sizeof (DefaultInitializationErrorHandler_t47)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
// Metadata Definition Vuforia.DefaultSmartTerrainEventHandler
extern TypeInfo DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo;
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern const MethodInfo DefaultSmartTerrainEventHandler__ctor_m154_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler__ctor_m154/* method */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern const MethodInfo DefaultSmartTerrainEventHandler_Start_m155_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_Start_m155/* method */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern const MethodInfo DefaultSmartTerrainEventHandler_OnDestroy_m156_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnDestroy_m156/* method */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Prop_t105_0_0_0;
extern const Il2CppType Prop_t105_0_0_0;
static const ParameterInfo DefaultSmartTerrainEventHandler_t51_DefaultSmartTerrainEventHandler_OnPropCreated_m157_ParameterInfos[] = 
{
	{"prop", 0, 134217795, 0, &Prop_t105_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern const MethodInfo DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo = 
{
	"OnPropCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnPropCreated_m157/* method */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t51_DefaultSmartTerrainEventHandler_OnPropCreated_m157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Surface_t106_0_0_0;
extern const Il2CppType Surface_t106_0_0_0;
static const ParameterInfo DefaultSmartTerrainEventHandler_t51_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_ParameterInfos[] = 
{
	{"surface", 0, 134217796, 0, &Surface_t106_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern const MethodInfo DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158/* method */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t51_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultSmartTerrainEventHandler_t51_MethodInfos[] =
{
	&DefaultSmartTerrainEventHandler__ctor_m154_MethodInfo,
	&DefaultSmartTerrainEventHandler_Start_m155_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnDestroy_m156_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultSmartTerrainEventHandler_t51_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool DefaultSmartTerrainEventHandler_t51_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultSmartTerrainEventHandler_t51_0_0_0;
extern const Il2CppType DefaultSmartTerrainEventHandler_t51_1_0_0;
struct DefaultSmartTerrainEventHandler_t51;
const Il2CppTypeDefinitionMetadata DefaultSmartTerrainEventHandler_t51_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultSmartTerrainEventHandler_t51_VTable/* vtableMethods */
	, DefaultSmartTerrainEventHandler_t51_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 118/* fieldStart */

};
TypeInfo DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultSmartTerrainEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultSmartTerrainEventHandler_t51_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultSmartTerrainEventHandler_t51_0_0_0/* byval_arg */
	, &DefaultSmartTerrainEventHandler_t51_1_0_0/* this_arg */
	, &DefaultSmartTerrainEventHandler_t51_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultSmartTerrainEventHandler_t51)/* instance_size */
	, sizeof (DefaultSmartTerrainEventHandler_t51)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
// Metadata Definition Vuforia.DefaultTrackableEventHandler
extern TypeInfo DefaultTrackableEventHandler_t53_il2cpp_TypeInfo;
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern const MethodInfo DefaultTrackableEventHandler__ctor_m159_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler__ctor_m159/* method */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo DefaultTrackableEventHandler_Start_m160_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_Start_m160/* method */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Status_t184_0_0_0;
extern const Il2CppType Status_t184_0_0_0;
extern const Il2CppType Status_t184_0_0_0;
static const ParameterInfo DefaultTrackableEventHandler_t53_DefaultTrackableEventHandler_OnTrackableStateChanged_m161_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217797, 0, &Status_t184_0_0_0},
	{"newStatus", 1, 134217798, 0, &Status_t184_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern const MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m161_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackableStateChanged_m161/* method */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, DefaultTrackableEventHandler_t53_DefaultTrackableEventHandler_OnTrackableStateChanged_m161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern const MethodInfo DefaultTrackableEventHandler_OnTrackingFound_m162_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingFound_m162/* method */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern const MethodInfo DefaultTrackableEventHandler_OnTrackingLost_m163_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingLost_m163/* method */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultTrackableEventHandler_t53_MethodInfos[] =
{
	&DefaultTrackableEventHandler__ctor_m159_MethodInfo,
	&DefaultTrackableEventHandler_Start_m160_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m161_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingFound_m162_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingLost_m163_MethodInfo,
	NULL
};
extern const MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m161_MethodInfo;
static const Il2CppMethodReference DefaultTrackableEventHandler_t53_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m161_MethodInfo,
};
static bool DefaultTrackableEventHandler_t53_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ITrackableEventHandler_t185_0_0_0;
static const Il2CppType* DefaultTrackableEventHandler_t53_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t185_0_0_0,
};
static Il2CppInterfaceOffsetPair DefaultTrackableEventHandler_t53_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t185_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultTrackableEventHandler_t53_0_0_0;
extern const Il2CppType DefaultTrackableEventHandler_t53_1_0_0;
struct DefaultTrackableEventHandler_t53;
const Il2CppTypeDefinitionMetadata DefaultTrackableEventHandler_t53_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DefaultTrackableEventHandler_t53_InterfacesTypeInfos/* implementedInterfaces */
	, DefaultTrackableEventHandler_t53_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultTrackableEventHandler_t53_VTable/* vtableMethods */
	, DefaultTrackableEventHandler_t53_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 121/* fieldStart */

};
TypeInfo DefaultTrackableEventHandler_t53_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultTrackableEventHandler_t53_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultTrackableEventHandler_t53_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultTrackableEventHandler_t53_0_0_0/* byval_arg */
	, &DefaultTrackableEventHandler_t53_1_0_0/* this_arg */
	, &DefaultTrackableEventHandler_t53_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultTrackableEventHandler_t53)/* instance_size */
	, sizeof (DefaultTrackableEventHandler_t53)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
// Metadata Definition Vuforia.GLErrorHandler
extern TypeInfo GLErrorHandler_t54_il2cpp_TypeInfo;
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.ctor()
extern const MethodInfo GLErrorHandler__ctor_m164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GLErrorHandler__ctor_m164/* method */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.cctor()
extern const MethodInfo GLErrorHandler__cctor_m165_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&GLErrorHandler__cctor_m165/* method */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo GLErrorHandler_t54_GLErrorHandler_SetError_m166_ParameterInfos[] = 
{
	{"errorText", 0, 134217799, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern const MethodInfo GLErrorHandler_SetError_m166_MethodInfo = 
{
	"SetError"/* name */
	, (methodPointerType)&GLErrorHandler_SetError_m166/* method */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, GLErrorHandler_t54_GLErrorHandler_SetError_m166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern const MethodInfo GLErrorHandler_OnGUI_m167_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&GLErrorHandler_OnGUI_m167/* method */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GLErrorHandler_t54_GLErrorHandler_DrawWindowContent_m168_ParameterInfos[] = 
{
	{"id", 0, 134217800, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern const MethodInfo GLErrorHandler_DrawWindowContent_m168_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&GLErrorHandler_DrawWindowContent_m168/* method */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GLErrorHandler_t54_GLErrorHandler_DrawWindowContent_m168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GLErrorHandler_t54_MethodInfos[] =
{
	&GLErrorHandler__ctor_m164_MethodInfo,
	&GLErrorHandler__cctor_m165_MethodInfo,
	&GLErrorHandler_SetError_m166_MethodInfo,
	&GLErrorHandler_OnGUI_m167_MethodInfo,
	&GLErrorHandler_DrawWindowContent_m168_MethodInfo,
	NULL
};
static const Il2CppMethodReference GLErrorHandler_t54_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool GLErrorHandler_t54_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GLErrorHandler_t54_0_0_0;
extern const Il2CppType GLErrorHandler_t54_1_0_0;
struct GLErrorHandler_t54;
const Il2CppTypeDefinitionMetadata GLErrorHandler_t54_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, GLErrorHandler_t54_VTable/* vtableMethods */
	, GLErrorHandler_t54_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 123/* fieldStart */

};
TypeInfo GLErrorHandler_t54_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GLErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, GLErrorHandler_t54_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GLErrorHandler_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GLErrorHandler_t54_0_0_0/* byval_arg */
	, &GLErrorHandler_t54_1_0_0/* this_arg */
	, &GLErrorHandler_t54_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GLErrorHandler_t54)/* instance_size */
	, sizeof (GLErrorHandler_t54)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GLErrorHandler_t54_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
// Metadata Definition Vuforia.HideExcessAreaBehaviour
extern TypeInfo HideExcessAreaBehaviour_t55_il2cpp_TypeInfo;
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern const MethodInfo HideExcessAreaBehaviour__ctor_m169_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideExcessAreaBehaviour__ctor_m169/* method */
	, &HideExcessAreaBehaviour_t55_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HideExcessAreaBehaviour_t55_MethodInfos[] =
{
	&HideExcessAreaBehaviour__ctor_m169_MethodInfo,
	NULL
};
static const Il2CppMethodReference HideExcessAreaBehaviour_t55_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool HideExcessAreaBehaviour_t55_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType HideExcessAreaBehaviour_t55_0_0_0;
extern const Il2CppType HideExcessAreaBehaviour_t55_1_0_0;
extern const Il2CppType HideExcessAreaAbstractBehaviour_t56_0_0_0;
struct HideExcessAreaBehaviour_t55;
const Il2CppTypeDefinitionMetadata HideExcessAreaBehaviour_t55_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &HideExcessAreaAbstractBehaviour_t56_0_0_0/* parent */
	, HideExcessAreaBehaviour_t55_VTable/* vtableMethods */
	, HideExcessAreaBehaviour_t55_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HideExcessAreaBehaviour_t55_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideExcessAreaBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, HideExcessAreaBehaviour_t55_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HideExcessAreaBehaviour_t55_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideExcessAreaBehaviour_t55_0_0_0/* byval_arg */
	, &HideExcessAreaBehaviour_t55_1_0_0/* this_arg */
	, &HideExcessAreaBehaviour_t55_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideExcessAreaBehaviour_t55)/* instance_size */
	, sizeof (HideExcessAreaBehaviour_t55)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
// Metadata Definition Vuforia.ImageTargetBehaviour
extern TypeInfo ImageTargetBehaviour_t57_il2cpp_TypeInfo;
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern const MethodInfo ImageTargetBehaviour__ctor_m170_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ImageTargetBehaviour__ctor_m170/* method */
	, &ImageTargetBehaviour_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ImageTargetBehaviour_t57_MethodInfos[] =
{
	&ImageTargetBehaviour__ctor_m170_MethodInfo,
	NULL
};
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m619_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m620_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m621_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m622_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m624_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_CorrectScaleImpl_m625_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m626_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m627_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m628_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m629_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m630_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m631_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_GetSize_m632_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_SetWidth_m633_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_SetHeight_m634_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m635_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m636_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m637_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m638_MethodInfo;
static const Il2CppMethodReference ImageTargetBehaviour_t57_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m619_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m620_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m621_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m622_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m586_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m624_MethodInfo,
	&ImageTargetAbstractBehaviour_CorrectScaleImpl_m625_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m614_MethodInfo,
	&ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m626_MethodInfo,
	&ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m627_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m628_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m629_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m630_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m631_MethodInfo,
	&ImageTargetAbstractBehaviour_GetSize_m632_MethodInfo,
	&ImageTargetAbstractBehaviour_SetWidth_m633_MethodInfo,
	&ImageTargetAbstractBehaviour_SetHeight_m634_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m635_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m636_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m637_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m638_MethodInfo,
};
static bool ImageTargetBehaviour_t57_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorImageTargetBehaviour_t186_0_0_0;
static Il2CppInterfaceOffsetPair ImageTargetBehaviour_t57_InterfacesOffsets[] = 
{
	{ &IEditorImageTargetBehaviour_t186_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t180_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ImageTargetBehaviour_t57_0_0_0;
extern const Il2CppType ImageTargetBehaviour_t57_1_0_0;
extern const Il2CppType ImageTargetAbstractBehaviour_t58_0_0_0;
struct ImageTargetBehaviour_t57;
const Il2CppTypeDefinitionMetadata ImageTargetBehaviour_t57_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ImageTargetBehaviour_t57_InterfacesOffsets/* interfaceOffsets */
	, &ImageTargetAbstractBehaviour_t58_0_0_0/* parent */
	, ImageTargetBehaviour_t57_VTable/* vtableMethods */
	, ImageTargetBehaviour_t57_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ImageTargetBehaviour_t57_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ImageTargetBehaviour_t57_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ImageTargetBehaviour_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ImageTargetBehaviour_t57_0_0_0/* byval_arg */
	, &ImageTargetBehaviour_t57_1_0_0/* this_arg */
	, &ImageTargetBehaviour_t57_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImageTargetBehaviour_t57)/* instance_size */
	, sizeof (ImageTargetBehaviour_t57)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 64/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
// Metadata Definition Vuforia.AndroidUnityPlayer
extern TypeInfo AndroidUnityPlayer_t59_il2cpp_TypeInfo;
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern const MethodInfo AndroidUnityPlayer__ctor_m171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AndroidUnityPlayer__ctor_m171/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m172_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibraries_m172/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern const MethodInfo AndroidUnityPlayer_InitializePlatform_m173_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializePlatform_m173/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AndroidUnityPlayer_t59_AndroidUnityPlayer_Start_m174_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217801, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_InitError_t183_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern const MethodInfo AndroidUnityPlayer_Start_m174_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Start_m174/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t183_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t183_Object_t/* invoker_method */
	, AndroidUnityPlayer_t59_AndroidUnityPlayer_Start_m174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern const MethodInfo AndroidUnityPlayer_Update_m175_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Update_m175/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern const MethodInfo AndroidUnityPlayer_OnPause_m176_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnPause_m176/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern const MethodInfo AndroidUnityPlayer_OnResume_m177_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnResume_m177/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern const MethodInfo AndroidUnityPlayer_OnDestroy_m178_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnDestroy_m178/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern const MethodInfo AndroidUnityPlayer_Dispose_m179_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Dispose_m179/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibrariesFromJava_m180_MethodInfo = 
{
	"LoadNativeLibrariesFromJava"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m180/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern const MethodInfo AndroidUnityPlayer_InitAndroidPlatform_m181_MethodInfo = 
{
	"InitAndroidPlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitAndroidPlatform_m181/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AndroidUnityPlayer_t59_AndroidUnityPlayer_InitQCAR_m182_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217802, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
extern const MethodInfo AndroidUnityPlayer_InitQCAR_m182_MethodInfo = 
{
	"InitQCAR"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitQCAR_m182/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, AndroidUnityPlayer_t59_AndroidUnityPlayer_InitQCAR_m182_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern const MethodInfo AndroidUnityPlayer_InitializeSurface_m183_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializeSurface_m183/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern const MethodInfo AndroidUnityPlayer_ResetUnityScreenOrientation_m184_MethodInfo = 
{
	"ResetUnityScreenOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_ResetUnityScreenOrientation_m184/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern const MethodInfo AndroidUnityPlayer_CheckOrientation_m185_MethodInfo = 
{
	"CheckOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_CheckOrientation_m185/* method */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AndroidUnityPlayer_t59_MethodInfos[] =
{
	&AndroidUnityPlayer__ctor_m171_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m172_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m173_MethodInfo,
	&AndroidUnityPlayer_Start_m174_MethodInfo,
	&AndroidUnityPlayer_Update_m175_MethodInfo,
	&AndroidUnityPlayer_OnPause_m176_MethodInfo,
	&AndroidUnityPlayer_OnResume_m177_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m178_MethodInfo,
	&AndroidUnityPlayer_Dispose_m179_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m180_MethodInfo,
	&AndroidUnityPlayer_InitAndroidPlatform_m181_MethodInfo,
	&AndroidUnityPlayer_InitQCAR_m182_MethodInfo,
	&AndroidUnityPlayer_InitializeSurface_m183_MethodInfo,
	&AndroidUnityPlayer_ResetUnityScreenOrientation_m184_MethodInfo,
	&AndroidUnityPlayer_CheckOrientation_m185_MethodInfo,
	NULL
};
extern const MethodInfo AndroidUnityPlayer_Dispose_m179_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m172_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_InitializePlatform_m173_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_Start_m174_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_Update_m175_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnPause_m176_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnResume_m177_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnDestroy_m178_MethodInfo;
static const Il2CppMethodReference AndroidUnityPlayer_t59_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&AndroidUnityPlayer_Dispose_m179_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m172_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m173_MethodInfo,
	&AndroidUnityPlayer_Start_m174_MethodInfo,
	&AndroidUnityPlayer_Update_m175_MethodInfo,
	&AndroidUnityPlayer_OnPause_m176_MethodInfo,
	&AndroidUnityPlayer_OnResume_m177_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m178_MethodInfo,
};
static bool AndroidUnityPlayer_t59_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t152_0_0_0;
extern const Il2CppType IUnityPlayer_t187_0_0_0;
static const Il2CppType* AndroidUnityPlayer_t59_InterfacesTypeInfos[] = 
{
	&IDisposable_t152_0_0_0,
	&IUnityPlayer_t187_0_0_0,
};
static Il2CppInterfaceOffsetPair AndroidUnityPlayer_t59_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
	{ &IUnityPlayer_t187_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AndroidUnityPlayer_t59_0_0_0;
extern const Il2CppType AndroidUnityPlayer_t59_1_0_0;
struct AndroidUnityPlayer_t59;
const Il2CppTypeDefinitionMetadata AndroidUnityPlayer_t59_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AndroidUnityPlayer_t59_InterfacesTypeInfos/* implementedInterfaces */
	, AndroidUnityPlayer_t59_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AndroidUnityPlayer_t59_VTable/* vtableMethods */
	, AndroidUnityPlayer_t59_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 126/* fieldStart */

};
TypeInfo AndroidUnityPlayer_t59_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AndroidUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, AndroidUnityPlayer_t59_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AndroidUnityPlayer_t59_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AndroidUnityPlayer_t59_0_0_0/* byval_arg */
	, &AndroidUnityPlayer_t59_1_0_0/* this_arg */
	, &AndroidUnityPlayer_t59_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AndroidUnityPlayer_t59)/* instance_size */
	, sizeof (AndroidUnityPlayer_t59)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
// Metadata Definition Vuforia.ComponentFactoryStarterBehaviour
extern TypeInfo ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo;
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern const MethodInfo ComponentFactoryStarterBehaviour__ctor_m186_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour__ctor_m186/* method */
	, &ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const MethodInfo ComponentFactoryStarterBehaviour_Awake_m187_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_Awake_m187/* method */
	, &ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern const MethodInfo ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m188_MethodInfo = 
{
	"SetBehaviourComponentFactory"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m188/* method */
	, &ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 7/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentFactoryStarterBehaviour_t60_MethodInfos[] =
{
	&ComponentFactoryStarterBehaviour__ctor_m186_MethodInfo,
	&ComponentFactoryStarterBehaviour_Awake_m187_MethodInfo,
	&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m188_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentFactoryStarterBehaviour_t60_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool ComponentFactoryStarterBehaviour_t60_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ComponentFactoryStarterBehaviour_t60_0_0_0;
extern const Il2CppType ComponentFactoryStarterBehaviour_t60_1_0_0;
struct ComponentFactoryStarterBehaviour_t60;
const Il2CppTypeDefinitionMetadata ComponentFactoryStarterBehaviour_t60_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, ComponentFactoryStarterBehaviour_t60_VTable/* vtableMethods */
	, ComponentFactoryStarterBehaviour_t60_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentFactoryStarterBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ComponentFactoryStarterBehaviour_t60_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentFactoryStarterBehaviour_t60_0_0_0/* byval_arg */
	, &ComponentFactoryStarterBehaviour_t60_1_0_0/* this_arg */
	, &ComponentFactoryStarterBehaviour_t60_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentFactoryStarterBehaviour_t60)/* instance_size */
	, sizeof (ComponentFactoryStarterBehaviour_t60)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
// Metadata Definition Vuforia.IOSUnityPlayer
extern TypeInfo IOSUnityPlayer_t61_il2cpp_TypeInfo;
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern const MethodInfo IOSUnityPlayer__ctor_m189_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IOSUnityPlayer__ctor_m189/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern const MethodInfo IOSUnityPlayer_LoadNativeLibraries_m190_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&IOSUnityPlayer_LoadNativeLibraries_m190/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern const MethodInfo IOSUnityPlayer_InitializePlatform_m191_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializePlatform_m191/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IOSUnityPlayer_t61_IOSUnityPlayer_Start_m192_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217803, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_InitError_t183_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern const MethodInfo IOSUnityPlayer_Start_m192_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&IOSUnityPlayer_Start_m192/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t183_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t183_Object_t/* invoker_method */
	, IOSUnityPlayer_t61_IOSUnityPlayer_Start_m192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Update()
extern const MethodInfo IOSUnityPlayer_Update_m193_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&IOSUnityPlayer_Update_m193/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern const MethodInfo IOSUnityPlayer_Dispose_m194_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&IOSUnityPlayer_Dispose_m194/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern const MethodInfo IOSUnityPlayer_OnPause_m195_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnPause_m195/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern const MethodInfo IOSUnityPlayer_OnResume_m196_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnResume_m196/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern const MethodInfo IOSUnityPlayer_OnDestroy_m197_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnDestroy_m197/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern const MethodInfo IOSUnityPlayer_InitializeSurface_m198_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializeSurface_m198/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern const MethodInfo IOSUnityPlayer_SetUnityScreenOrientation_m199_MethodInfo = 
{
	"SetUnityScreenOrientation"/* name */
	, (methodPointerType)&IOSUnityPlayer_SetUnityScreenOrientation_m199/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern const MethodInfo IOSUnityPlayer_setPlatFormNative_m200_MethodInfo = 
{
	"setPlatFormNative"/* name */
	, (methodPointerType)&IOSUnityPlayer_setPlatFormNative_m200/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IOSUnityPlayer_t61_IOSUnityPlayer_initQCARiOS_m201_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217804, 0, &Int32_t135_0_0_0},
	{"licenseKey", 1, 134217805, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern const MethodInfo IOSUnityPlayer_initQCARiOS_m201_MethodInfo = 
{
	"initQCARiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_initQCARiOS_m201/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135_Object_t/* invoker_method */
	, IOSUnityPlayer_t61_IOSUnityPlayer_initQCARiOS_m201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IOSUnityPlayer_t61_IOSUnityPlayer_setSurfaceOrientationiOS_m202_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217806, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern const MethodInfo IOSUnityPlayer_setSurfaceOrientationiOS_m202_MethodInfo = 
{
	"setSurfaceOrientationiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_setSurfaceOrientationiOS_m202/* method */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, IOSUnityPlayer_t61_IOSUnityPlayer_setSurfaceOrientationiOS_m202_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IOSUnityPlayer_t61_MethodInfos[] =
{
	&IOSUnityPlayer__ctor_m189_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m190_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m191_MethodInfo,
	&IOSUnityPlayer_Start_m192_MethodInfo,
	&IOSUnityPlayer_Update_m193_MethodInfo,
	&IOSUnityPlayer_Dispose_m194_MethodInfo,
	&IOSUnityPlayer_OnPause_m195_MethodInfo,
	&IOSUnityPlayer_OnResume_m196_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m197_MethodInfo,
	&IOSUnityPlayer_InitializeSurface_m198_MethodInfo,
	&IOSUnityPlayer_SetUnityScreenOrientation_m199_MethodInfo,
	&IOSUnityPlayer_setPlatFormNative_m200_MethodInfo,
	&IOSUnityPlayer_initQCARiOS_m201_MethodInfo,
	&IOSUnityPlayer_setSurfaceOrientationiOS_m202_MethodInfo,
	NULL
};
extern const MethodInfo IOSUnityPlayer_Dispose_m194_MethodInfo;
extern const MethodInfo IOSUnityPlayer_LoadNativeLibraries_m190_MethodInfo;
extern const MethodInfo IOSUnityPlayer_InitializePlatform_m191_MethodInfo;
extern const MethodInfo IOSUnityPlayer_Start_m192_MethodInfo;
extern const MethodInfo IOSUnityPlayer_Update_m193_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnPause_m195_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnResume_m196_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnDestroy_m197_MethodInfo;
static const Il2CppMethodReference IOSUnityPlayer_t61_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&IOSUnityPlayer_Dispose_m194_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m190_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m191_MethodInfo,
	&IOSUnityPlayer_Start_m192_MethodInfo,
	&IOSUnityPlayer_Update_m193_MethodInfo,
	&IOSUnityPlayer_OnPause_m195_MethodInfo,
	&IOSUnityPlayer_OnResume_m196_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m197_MethodInfo,
};
static bool IOSUnityPlayer_t61_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* IOSUnityPlayer_t61_InterfacesTypeInfos[] = 
{
	&IDisposable_t152_0_0_0,
	&IUnityPlayer_t187_0_0_0,
};
static Il2CppInterfaceOffsetPair IOSUnityPlayer_t61_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
	{ &IUnityPlayer_t187_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType IOSUnityPlayer_t61_0_0_0;
extern const Il2CppType IOSUnityPlayer_t61_1_0_0;
struct IOSUnityPlayer_t61;
const Il2CppTypeDefinitionMetadata IOSUnityPlayer_t61_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IOSUnityPlayer_t61_InterfacesTypeInfos/* implementedInterfaces */
	, IOSUnityPlayer_t61_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IOSUnityPlayer_t61_VTable/* vtableMethods */
	, IOSUnityPlayer_t61_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 132/* fieldStart */

};
TypeInfo IOSUnityPlayer_t61_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "IOSUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, IOSUnityPlayer_t61_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IOSUnityPlayer_t61_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IOSUnityPlayer_t61_0_0_0/* byval_arg */
	, &IOSUnityPlayer_t61_1_0_0/* this_arg */
	, &IOSUnityPlayer_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IOSUnityPlayer_t61)/* instance_size */
	, sizeof (IOSUnityPlayer_t61)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
// Metadata Definition Vuforia.VuforiaBehaviourComponentFactory
extern TypeInfo VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo;
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern const MethodInfo VuforiaBehaviourComponentFactory__ctor_m203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory__ctor_m203/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_ParameterInfos[] = 
{
	{"gameObject", 0, 134217807, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MaskOutAbstractBehaviour_t68_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_MethodInfo = 
{
	"AddMaskOutBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t68_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_ParameterInfos[] = 
{
	{"gameObject", 0, 134217808, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType VirtualButtonAbstractBehaviour_t94_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_MethodInfo = 
{
	"AddVirtualButtonBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t94_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_ParameterInfos[] = 
{
	{"gameObject", 0, 134217809, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType TurnOffAbstractBehaviour_t85_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_MethodInfo = 
{
	"AddTurnOffBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t85_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_ParameterInfos[] = 
{
	{"gameObject", 0, 134217810, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_MethodInfo = 
{
	"AddImageTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_ParameterInfos[] = 
{
	{"gameObject", 0, 134217811, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MarkerAbstractBehaviour_t66_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_MethodInfo = 
{
	"AddMarkerBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t66_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_ParameterInfos[] = 
{
	{"gameObject", 0, 134217812, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MultiTargetAbstractBehaviour_t70_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_MethodInfo = 
{
	"AddMultiTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_ParameterInfos[] = 
{
	{"gameObject", 0, 134217813, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_MethodInfo = 
{
	"AddCylinderTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_ParameterInfos[] = 
{
	{"gameObject", 0, 134217814, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType WordAbstractBehaviour_t101_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_MethodInfo = 
{
	"AddWordBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddWordBehaviour_m211/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t101_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_ParameterInfos[] = 
{
	{"gameObject", 0, 134217815, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType TextRecoAbstractBehaviour_t83_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_MethodInfo = 
{
	"AddTextRecoBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t83_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_ParameterInfos[] = 
{
	{"gameObject", 0, 134217816, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType ObjectTargetAbstractBehaviour_t72_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_MethodInfo = 
{
	"AddObjectTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213/* method */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t62_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VuforiaBehaviourComponentFactory_t62_MethodInfos[] =
{
	&VuforiaBehaviourComponentFactory__ctor_m203_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_MethodInfo,
	NULL
};
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_MethodInfo;
static const Il2CppMethodReference VuforiaBehaviourComponentFactory_t62_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m211_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213_MethodInfo,
};
static bool VuforiaBehaviourComponentFactory_t62_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IBehaviourComponentFactory_t188_0_0_0;
static const Il2CppType* VuforiaBehaviourComponentFactory_t62_InterfacesTypeInfos[] = 
{
	&IBehaviourComponentFactory_t188_0_0_0,
};
static Il2CppInterfaceOffsetPair VuforiaBehaviourComponentFactory_t62_InterfacesOffsets[] = 
{
	{ &IBehaviourComponentFactory_t188_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VuforiaBehaviourComponentFactory_t62_0_0_0;
extern const Il2CppType VuforiaBehaviourComponentFactory_t62_1_0_0;
struct VuforiaBehaviourComponentFactory_t62;
const Il2CppTypeDefinitionMetadata VuforiaBehaviourComponentFactory_t62_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, VuforiaBehaviourComponentFactory_t62_InterfacesTypeInfos/* implementedInterfaces */
	, VuforiaBehaviourComponentFactory_t62_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, VuforiaBehaviourComponentFactory_t62_VTable/* vtableMethods */
	, VuforiaBehaviourComponentFactory_t62_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VuforiaBehaviourComponentFactory"/* name */
	, "Vuforia"/* namespaze */
	, VuforiaBehaviourComponentFactory_t62_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VuforiaBehaviourComponentFactory_t62_0_0_0/* byval_arg */
	, &VuforiaBehaviourComponentFactory_t62_1_0_0/* this_arg */
	, &VuforiaBehaviourComponentFactory_t62_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VuforiaBehaviourComponentFactory_t62)/* instance_size */
	, sizeof (VuforiaBehaviourComponentFactory_t62)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
// Metadata Definition Vuforia.KeepAliveBehaviour
extern TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo;
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern const MethodInfo KeepAliveBehaviour__ctor_m214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&KeepAliveBehaviour__ctor_m214/* method */
	, &KeepAliveBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* KeepAliveBehaviour_t63_MethodInfos[] =
{
	&KeepAliveBehaviour__ctor_m214_MethodInfo,
	NULL
};
static const Il2CppMethodReference KeepAliveBehaviour_t63_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool KeepAliveBehaviour_t63_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType KeepAliveBehaviour_t63_0_0_0;
extern const Il2CppType KeepAliveBehaviour_t63_1_0_0;
extern const Il2CppType KeepAliveAbstractBehaviour_t64_0_0_0;
struct KeepAliveBehaviour_t63;
const Il2CppTypeDefinitionMetadata KeepAliveBehaviour_t63_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &KeepAliveAbstractBehaviour_t64_0_0_0/* parent */
	, KeepAliveBehaviour_t63_VTable/* vtableMethods */
	, KeepAliveBehaviour_t63_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeepAliveBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, KeepAliveBehaviour_t63_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &KeepAliveBehaviour_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 8/* custom_attributes_cache */
	, &KeepAliveBehaviour_t63_0_0_0/* byval_arg */
	, &KeepAliveBehaviour_t63_1_0_0/* this_arg */
	, &KeepAliveBehaviour_t63_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeepAliveBehaviour_t63)/* instance_size */
	, sizeof (KeepAliveBehaviour_t63)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Metadata Definition Vuforia.MarkerBehaviour
extern TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo;
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MarkerBehaviour::.ctor()
extern const MethodInfo MarkerBehaviour__ctor_m215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarkerBehaviour__ctor_m215/* method */
	, &MarkerBehaviour_t65_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MarkerBehaviour_t65_MethodInfos[] =
{
	&MarkerBehaviour__ctor_m215_MethodInfo,
	NULL
};
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m639_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m640_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m641_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m642_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_InternalUnregisterTrackable_m644_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_CorrectScaleImpl_m645_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m646_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m647_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m648_MethodInfo;
static const Il2CppMethodReference MarkerBehaviour_t65_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m639_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m640_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m641_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m642_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&MarkerAbstractBehaviour_InternalUnregisterTrackable_m644_MethodInfo,
	&MarkerAbstractBehaviour_CorrectScaleImpl_m645_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m646_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m647_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m648_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
};
static bool MarkerBehaviour_t65_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorMarkerBehaviour_t189_0_0_0;
static Il2CppInterfaceOffsetPair MarkerBehaviour_t65_InterfacesOffsets[] = 
{
	{ &IEditorMarkerBehaviour_t189_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 28},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MarkerBehaviour_t65_0_0_0;
extern const Il2CppType MarkerBehaviour_t65_1_0_0;
struct MarkerBehaviour_t65;
const Il2CppTypeDefinitionMetadata MarkerBehaviour_t65_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarkerBehaviour_t65_InterfacesOffsets/* interfaceOffsets */
	, &MarkerAbstractBehaviour_t66_0_0_0/* parent */
	, MarkerBehaviour_t65_VTable/* vtableMethods */
	, MarkerBehaviour_t65_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarkerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MarkerBehaviour_t65_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MarkerBehaviour_t65_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MarkerBehaviour_t65_0_0_0/* byval_arg */
	, &MarkerBehaviour_t65_1_0_0/* this_arg */
	, &MarkerBehaviour_t65_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarkerBehaviour_t65)/* instance_size */
	, sizeof (MarkerBehaviour_t65)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Metadata Definition Vuforia.MaskOutBehaviour
extern TypeInfo MaskOutBehaviour_t67_il2cpp_TypeInfo;
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern const MethodInfo MaskOutBehaviour__ctor_m216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MaskOutBehaviour__ctor_m216/* method */
	, &MaskOutBehaviour_t67_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::Start()
extern const MethodInfo MaskOutBehaviour_Start_m217_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&MaskOutBehaviour_Start_m217/* method */
	, &MaskOutBehaviour_t67_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MaskOutBehaviour_t67_MethodInfos[] =
{
	&MaskOutBehaviour__ctor_m216_MethodInfo,
	&MaskOutBehaviour_Start_m217_MethodInfo,
	NULL
};
static const Il2CppMethodReference MaskOutBehaviour_t67_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool MaskOutBehaviour_t67_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MaskOutBehaviour_t67_0_0_0;
extern const Il2CppType MaskOutBehaviour_t67_1_0_0;
struct MaskOutBehaviour_t67;
const Il2CppTypeDefinitionMetadata MaskOutBehaviour_t67_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MaskOutAbstractBehaviour_t68_0_0_0/* parent */
	, MaskOutBehaviour_t67_VTable/* vtableMethods */
	, MaskOutBehaviour_t67_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MaskOutBehaviour_t67_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MaskOutBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MaskOutBehaviour_t67_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MaskOutBehaviour_t67_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MaskOutBehaviour_t67_0_0_0/* byval_arg */
	, &MaskOutBehaviour_t67_1_0_0/* this_arg */
	, &MaskOutBehaviour_t67_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MaskOutBehaviour_t67)/* instance_size */
	, sizeof (MaskOutBehaviour_t67)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Metadata Definition Vuforia.MultiTargetBehaviour
extern TypeInfo MultiTargetBehaviour_t69_il2cpp_TypeInfo;
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern const MethodInfo MultiTargetBehaviour__ctor_m218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MultiTargetBehaviour__ctor_m218/* method */
	, &MultiTargetBehaviour_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MultiTargetBehaviour_t69_MethodInfos[] =
{
	&MultiTargetBehaviour__ctor_m218_MethodInfo,
	NULL
};
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m649_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m650_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m651_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m652_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m653_MethodInfo;
extern const MethodInfo TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m655_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m656_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m657_MethodInfo;
static const Il2CppMethodReference MultiTargetBehaviour_t69_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m649_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m650_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m651_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m652_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m586_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m653_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m614_MethodInfo,
	&MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m655_MethodInfo,
	&MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m656_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m657_MethodInfo,
};
static bool MultiTargetBehaviour_t69_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorMultiTargetBehaviour_t190_0_0_0;
static Il2CppInterfaceOffsetPair MultiTargetBehaviour_t69_InterfacesOffsets[] = 
{
	{ &IEditorMultiTargetBehaviour_t190_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t180_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MultiTargetBehaviour_t69_0_0_0;
extern const Il2CppType MultiTargetBehaviour_t69_1_0_0;
struct MultiTargetBehaviour_t69;
const Il2CppTypeDefinitionMetadata MultiTargetBehaviour_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MultiTargetBehaviour_t69_InterfacesOffsets/* interfaceOffsets */
	, &MultiTargetAbstractBehaviour_t70_0_0_0/* parent */
	, MultiTargetBehaviour_t69_VTable/* vtableMethods */
	, MultiTargetBehaviour_t69_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MultiTargetBehaviour_t69_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MultiTargetBehaviour_t69_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MultiTargetBehaviour_t69_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MultiTargetBehaviour_t69_0_0_0/* byval_arg */
	, &MultiTargetBehaviour_t69_1_0_0/* this_arg */
	, &MultiTargetBehaviour_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiTargetBehaviour_t69)/* instance_size */
	, sizeof (MultiTargetBehaviour_t69)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 54/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
// Metadata Definition Vuforia.ObjectTargetBehaviour
extern TypeInfo ObjectTargetBehaviour_t71_il2cpp_TypeInfo;
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern const MethodInfo ObjectTargetBehaviour__ctor_m219_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectTargetBehaviour__ctor_m219/* method */
	, &ObjectTargetBehaviour_t71_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectTargetBehaviour_t71_MethodInfos[] =
{
	&ObjectTargetBehaviour__ctor_m219_MethodInfo,
	NULL
};
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m658_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m659_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m660_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m661_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m662_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_CorrectScaleImpl_m663_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m664_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m665_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m666_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m667_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m668_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m669_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m670_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_GetSize_m671_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetLength_m672_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetWidth_m673_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetHeight_m674_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m675_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m676_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetBoundingBox_m677_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m678_MethodInfo;
static const Il2CppMethodReference ObjectTargetBehaviour_t71_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m658_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m659_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m660_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m661_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m586_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m662_MethodInfo,
	&ObjectTargetAbstractBehaviour_CorrectScaleImpl_m663_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m614_MethodInfo,
	&ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m664_MethodInfo,
	&ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m665_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m666_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m667_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m668_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m669_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m670_MethodInfo,
	&ObjectTargetAbstractBehaviour_GetSize_m671_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetLength_m672_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetWidth_m673_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetHeight_m674_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m675_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m676_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetBoundingBox_m677_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m678_MethodInfo,
};
static bool ObjectTargetBehaviour_t71_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorObjectTargetBehaviour_t191_0_0_0;
static Il2CppInterfaceOffsetPair ObjectTargetBehaviour_t71_InterfacesOffsets[] = 
{
	{ &IEditorObjectTargetBehaviour_t191_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t180_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ObjectTargetBehaviour_t71_0_0_0;
extern const Il2CppType ObjectTargetBehaviour_t71_1_0_0;
struct ObjectTargetBehaviour_t71;
const Il2CppTypeDefinitionMetadata ObjectTargetBehaviour_t71_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectTargetBehaviour_t71_InterfacesOffsets/* interfaceOffsets */
	, &ObjectTargetAbstractBehaviour_t72_0_0_0/* parent */
	, ObjectTargetBehaviour_t71_VTable/* vtableMethods */
	, ObjectTargetBehaviour_t71_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ObjectTargetBehaviour_t71_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ObjectTargetBehaviour_t71_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjectTargetBehaviour_t71_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectTargetBehaviour_t71_0_0_0/* byval_arg */
	, &ObjectTargetBehaviour_t71_1_0_0/* this_arg */
	, &ObjectTargetBehaviour_t71_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectTargetBehaviour_t71)/* instance_size */
	, sizeof (ObjectTargetBehaviour_t71)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 66/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Metadata Definition Vuforia.PropBehaviour
extern TypeInfo PropBehaviour_t49_il2cpp_TypeInfo;
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.PropBehaviour::.ctor()
extern const MethodInfo PropBehaviour__ctor_m220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropBehaviour__ctor_m220/* method */
	, &PropBehaviour_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropBehaviour_t49_MethodInfos[] =
{
	&PropBehaviour__ctor_m220_MethodInfo,
	NULL
};
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m679_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m680_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m681_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m682_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_InternalUnregisterTrackable_m683_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_UpdateMeshAndColliders_m684_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Start_m685_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m686_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m687_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m688_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m689_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m690_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m691_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m692_MethodInfo;
static const Il2CppMethodReference PropBehaviour_t49_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m679_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m680_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m681_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m682_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&PropAbstractBehaviour_InternalUnregisterTrackable_m683_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo,
	&PropAbstractBehaviour_UpdateMeshAndColliders_m684_MethodInfo,
	&PropAbstractBehaviour_Start_m685_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m686_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m687_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m688_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m689_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m690_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m691_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m692_MethodInfo,
};
static bool PropBehaviour_t49_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorPropBehaviour_t192_0_0_0;
static Il2CppInterfaceOffsetPair PropBehaviour_t49_InterfacesOffsets[] = 
{
	{ &IEditorPropBehaviour_t192_0_0_0, 27},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PropBehaviour_t49_0_0_0;
extern const Il2CppType PropBehaviour_t49_1_0_0;
extern const Il2CppType PropAbstractBehaviour_t73_0_0_0;
struct PropBehaviour_t49;
const Il2CppTypeDefinitionMetadata PropBehaviour_t49_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropBehaviour_t49_InterfacesOffsets/* interfaceOffsets */
	, &PropAbstractBehaviour_t73_0_0_0/* parent */
	, PropBehaviour_t49_VTable/* vtableMethods */
	, PropBehaviour_t49_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropBehaviour_t49_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, PropBehaviour_t49_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropBehaviour_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PropBehaviour_t49_0_0_0/* byval_arg */
	, &PropBehaviour_t49_1_0_0/* this_arg */
	, &PropBehaviour_t49_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropBehaviour_t49)/* instance_size */
	, sizeof (PropBehaviour_t49)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
// Metadata Definition Vuforia.QCARBehaviour
extern TypeInfo QCARBehaviour_t74_il2cpp_TypeInfo;
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::.ctor()
extern const MethodInfo QCARBehaviour__ctor_m221_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARBehaviour__ctor_m221/* method */
	, &QCARBehaviour_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::Awake()
extern const MethodInfo QCARBehaviour_Awake_m222_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&QCARBehaviour_Awake_m222/* method */
	, &QCARBehaviour_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QCARBehaviour_t74_MethodInfos[] =
{
	&QCARBehaviour__ctor_m221_MethodInfo,
	&QCARBehaviour_Awake_m222_MethodInfo,
	NULL
};
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m693_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m694_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m695_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m696_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m697_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m698_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m699_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m700_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m701_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m702_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m703_MethodInfo;
static const Il2CppMethodReference QCARBehaviour_t74_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m693_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m694_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m695_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m696_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m697_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m698_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m699_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m700_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m701_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m702_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m703_MethodInfo,
};
static bool QCARBehaviour_t74_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorQCARBehaviour_t193_0_0_0;
static Il2CppInterfaceOffsetPair QCARBehaviour_t74_InterfacesOffsets[] = 
{
	{ &IEditorQCARBehaviour_t193_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType QCARBehaviour_t74_0_0_0;
extern const Il2CppType QCARBehaviour_t74_1_0_0;
extern const Il2CppType QCARAbstractBehaviour_t75_0_0_0;
struct QCARBehaviour_t74;
const Il2CppTypeDefinitionMetadata QCARBehaviour_t74_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, QCARBehaviour_t74_InterfacesOffsets/* interfaceOffsets */
	, &QCARAbstractBehaviour_t75_0_0_0/* parent */
	, QCARBehaviour_t74_VTable/* vtableMethods */
	, QCARBehaviour_t74_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo QCARBehaviour_t74_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, QCARBehaviour_t74_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QCARBehaviour_t74_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QCARBehaviour_t74_0_0_0/* byval_arg */
	, &QCARBehaviour_t74_1_0_0/* this_arg */
	, &QCARBehaviour_t74_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARBehaviour_t74)/* instance_size */
	, sizeof (QCARBehaviour_t74)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// Metadata Definition Vuforia.ReconstructionBehaviour
extern TypeInfo ReconstructionBehaviour_t48_il2cpp_TypeInfo;
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern const MethodInfo ReconstructionBehaviour__ctor_m223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionBehaviour__ctor_m223/* method */
	, &ReconstructionBehaviour_t48_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReconstructionBehaviour_t48_MethodInfos[] =
{
	&ReconstructionBehaviour__ctor_m223_MethodInfo,
	NULL
};
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m704_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m705_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m706_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m707_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m708_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m709_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m710_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m711_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m712_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m713_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m714_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m715_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m716_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m717_MethodInfo;
static const Il2CppMethodReference ReconstructionBehaviour_t48_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m704_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m705_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m706_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m707_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m708_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m709_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m710_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m711_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m712_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m713_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m714_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m715_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m716_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m717_MethodInfo,
};
static bool ReconstructionBehaviour_t48_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorReconstructionBehaviour_t194_0_0_0;
static Il2CppInterfaceOffsetPair ReconstructionBehaviour_t48_InterfacesOffsets[] = 
{
	{ &IEditorReconstructionBehaviour_t194_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReconstructionBehaviour_t48_0_0_0;
extern const Il2CppType ReconstructionBehaviour_t48_1_0_0;
extern const Il2CppType ReconstructionAbstractBehaviour_t76_0_0_0;
struct ReconstructionBehaviour_t48;
const Il2CppTypeDefinitionMetadata ReconstructionBehaviour_t48_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReconstructionBehaviour_t48_InterfacesOffsets/* interfaceOffsets */
	, &ReconstructionAbstractBehaviour_t76_0_0_0/* parent */
	, ReconstructionBehaviour_t48_VTable/* vtableMethods */
	, ReconstructionBehaviour_t48_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ReconstructionBehaviour_t48_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionBehaviour_t48_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReconstructionBehaviour_t48_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReconstructionBehaviour_t48_0_0_0/* byval_arg */
	, &ReconstructionBehaviour_t48_1_0_0/* this_arg */
	, &ReconstructionBehaviour_t48_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionBehaviour_t48)/* instance_size */
	, sizeof (ReconstructionBehaviour_t48)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
// Metadata Definition Vuforia.ReconstructionFromTargetBehaviour
extern TypeInfo ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo;
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern const MethodInfo ReconstructionFromTargetBehaviour__ctor_m224_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionFromTargetBehaviour__ctor_m224/* method */
	, &ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReconstructionFromTargetBehaviour_t77_MethodInfos[] =
{
	&ReconstructionFromTargetBehaviour__ctor_m224_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReconstructionFromTargetBehaviour_t77_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool ReconstructionFromTargetBehaviour_t77_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReconstructionFromTargetBehaviour_t77_0_0_0;
extern const Il2CppType ReconstructionFromTargetBehaviour_t77_1_0_0;
extern const Il2CppType ReconstructionFromTargetAbstractBehaviour_t78_0_0_0;
struct ReconstructionFromTargetBehaviour_t77;
const Il2CppTypeDefinitionMetadata ReconstructionFromTargetBehaviour_t77_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ReconstructionFromTargetAbstractBehaviour_t78_0_0_0/* parent */
	, ReconstructionFromTargetBehaviour_t77_VTable/* vtableMethods */
	, ReconstructionFromTargetBehaviour_t77_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionFromTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionFromTargetBehaviour_t77_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReconstructionFromTargetBehaviour_t77_0_0_0/* byval_arg */
	, &ReconstructionFromTargetBehaviour_t77_1_0_0/* this_arg */
	, &ReconstructionFromTargetBehaviour_t77_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionFromTargetBehaviour_t77)/* instance_size */
	, sizeof (ReconstructionFromTargetBehaviour_t77)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
// Metadata Definition Vuforia.SmartTerrainTrackerBehaviour
extern TypeInfo SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo;
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern const MethodInfo SmartTerrainTrackerBehaviour__ctor_m225_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmartTerrainTrackerBehaviour__ctor_m225/* method */
	, &SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SmartTerrainTrackerBehaviour_t79_MethodInfos[] =
{
	&SmartTerrainTrackerBehaviour__ctor_m225_MethodInfo,
	NULL
};
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m718_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m719_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m720_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m721_MethodInfo;
static const Il2CppMethodReference SmartTerrainTrackerBehaviour_t79_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m718_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m719_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m720_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m721_MethodInfo,
};
static bool SmartTerrainTrackerBehaviour_t79_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorSmartTerrainTrackerBehaviour_t195_0_0_0;
static Il2CppInterfaceOffsetPair SmartTerrainTrackerBehaviour_t79_InterfacesOffsets[] = 
{
	{ &IEditorSmartTerrainTrackerBehaviour_t195_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SmartTerrainTrackerBehaviour_t79_0_0_0;
extern const Il2CppType SmartTerrainTrackerBehaviour_t79_1_0_0;
extern const Il2CppType SmartTerrainTrackerAbstractBehaviour_t80_0_0_0;
struct SmartTerrainTrackerBehaviour_t79;
const Il2CppTypeDefinitionMetadata SmartTerrainTrackerBehaviour_t79_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SmartTerrainTrackerBehaviour_t79_InterfacesOffsets/* interfaceOffsets */
	, &SmartTerrainTrackerAbstractBehaviour_t80_0_0_0/* parent */
	, SmartTerrainTrackerBehaviour_t79_VTable/* vtableMethods */
	, SmartTerrainTrackerBehaviour_t79_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmartTerrainTrackerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SmartTerrainTrackerBehaviour_t79_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmartTerrainTrackerBehaviour_t79_0_0_0/* byval_arg */
	, &SmartTerrainTrackerBehaviour_t79_1_0_0/* this_arg */
	, &SmartTerrainTrackerBehaviour_t79_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmartTerrainTrackerBehaviour_t79)/* instance_size */
	, sizeof (SmartTerrainTrackerBehaviour_t79)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Metadata Definition Vuforia.SurfaceBehaviour
extern TypeInfo SurfaceBehaviour_t50_il2cpp_TypeInfo;
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern const MethodInfo SurfaceBehaviour__ctor_m226_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SurfaceBehaviour__ctor_m226/* method */
	, &SurfaceBehaviour_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SurfaceBehaviour_t50_MethodInfos[] =
{
	&SurfaceBehaviour__ctor_m226_MethodInfo,
	NULL
};
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m722_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m723_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m724_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m725_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_InternalUnregisterTrackable_m726_MethodInfo;
extern const MethodInfo SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m727_MethodInfo;
extern const MethodInfo SmartTerrainTrackableBehaviour_Start_m728_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m729_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m730_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m731_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m732_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m733_MethodInfo;
static const Il2CppMethodReference SurfaceBehaviour_t50_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m722_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m723_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m724_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m725_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&SurfaceAbstractBehaviour_InternalUnregisterTrackable_m726_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo,
	&SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m727_MethodInfo,
	&SmartTerrainTrackableBehaviour_Start_m728_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m729_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m730_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m731_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m732_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m733_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
};
static bool SurfaceBehaviour_t50_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorSurfaceBehaviour_t196_0_0_0;
static Il2CppInterfaceOffsetPair SurfaceBehaviour_t50_InterfacesOffsets[] = 
{
	{ &IEditorSurfaceBehaviour_t196_0_0_0, 27},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t182_0_0_0, 32},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SurfaceBehaviour_t50_0_0_0;
extern const Il2CppType SurfaceBehaviour_t50_1_0_0;
extern const Il2CppType SurfaceAbstractBehaviour_t81_0_0_0;
struct SurfaceBehaviour_t50;
const Il2CppTypeDefinitionMetadata SurfaceBehaviour_t50_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SurfaceBehaviour_t50_InterfacesOffsets/* interfaceOffsets */
	, &SurfaceAbstractBehaviour_t81_0_0_0/* parent */
	, SurfaceBehaviour_t50_VTable/* vtableMethods */
	, SurfaceBehaviour_t50_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SurfaceBehaviour_t50_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceBehaviour_t50_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SurfaceBehaviour_t50_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SurfaceBehaviour_t50_0_0_0/* byval_arg */
	, &SurfaceBehaviour_t50_1_0_0/* this_arg */
	, &SurfaceBehaviour_t50_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceBehaviour_t50)/* instance_size */
	, sizeof (SurfaceBehaviour_t50)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Metadata Definition Vuforia.TextRecoBehaviour
extern TypeInfo TextRecoBehaviour_t82_il2cpp_TypeInfo;
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern const MethodInfo TextRecoBehaviour__ctor_m227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoBehaviour__ctor_m227/* method */
	, &TextRecoBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextRecoBehaviour_t82_MethodInfos[] =
{
	&TextRecoBehaviour__ctor_m227_MethodInfo,
	NULL
};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo;
static const Il2CppMethodReference TextRecoBehaviour_t82_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo,
};
static bool TextRecoBehaviour_t82_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorTextRecoBehaviour_t197_0_0_0;
static Il2CppInterfaceOffsetPair TextRecoBehaviour_t82_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t197_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TextRecoBehaviour_t82_0_0_0;
extern const Il2CppType TextRecoBehaviour_t82_1_0_0;
struct TextRecoBehaviour_t82;
const Il2CppTypeDefinitionMetadata TextRecoBehaviour_t82_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextRecoBehaviour_t82_InterfacesOffsets/* interfaceOffsets */
	, &TextRecoAbstractBehaviour_t83_0_0_0/* parent */
	, TextRecoBehaviour_t82_VTable/* vtableMethods */
	, TextRecoBehaviour_t82_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TextRecoBehaviour_t82_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoBehaviour_t82_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextRecoBehaviour_t82_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextRecoBehaviour_t82_0_0_0/* byval_arg */
	, &TextRecoBehaviour_t82_1_0_0/* this_arg */
	, &TextRecoBehaviour_t82_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoBehaviour_t82)/* instance_size */
	, sizeof (TextRecoBehaviour_t82)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Metadata Definition Vuforia.TurnOffBehaviour
extern TypeInfo TurnOffBehaviour_t84_il2cpp_TypeInfo;
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern const MethodInfo TurnOffBehaviour__ctor_m228_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffBehaviour__ctor_m228/* method */
	, &TurnOffBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern const MethodInfo TurnOffBehaviour_Awake_m229_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffBehaviour_Awake_m229/* method */
	, &TurnOffBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TurnOffBehaviour_t84_MethodInfos[] =
{
	&TurnOffBehaviour__ctor_m228_MethodInfo,
	&TurnOffBehaviour_Awake_m229_MethodInfo,
	NULL
};
static const Il2CppMethodReference TurnOffBehaviour_t84_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool TurnOffBehaviour_t84_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TurnOffBehaviour_t84_0_0_0;
extern const Il2CppType TurnOffBehaviour_t84_1_0_0;
struct TurnOffBehaviour_t84;
const Il2CppTypeDefinitionMetadata TurnOffBehaviour_t84_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TurnOffAbstractBehaviour_t85_0_0_0/* parent */
	, TurnOffBehaviour_t84_VTable/* vtableMethods */
	, TurnOffBehaviour_t84_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TurnOffBehaviour_t84_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffBehaviour_t84_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TurnOffBehaviour_t84_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TurnOffBehaviour_t84_0_0_0/* byval_arg */
	, &TurnOffBehaviour_t84_1_0_0/* this_arg */
	, &TurnOffBehaviour_t84_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffBehaviour_t84)/* instance_size */
	, sizeof (TurnOffBehaviour_t84)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
// Metadata Definition Vuforia.TurnOffWordBehaviour
extern TypeInfo TurnOffWordBehaviour_t86_il2cpp_TypeInfo;
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern const MethodInfo TurnOffWordBehaviour__ctor_m230_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffWordBehaviour__ctor_m230/* method */
	, &TurnOffWordBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern const MethodInfo TurnOffWordBehaviour_Awake_m231_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffWordBehaviour_Awake_m231/* method */
	, &TurnOffWordBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TurnOffWordBehaviour_t86_MethodInfos[] =
{
	&TurnOffWordBehaviour__ctor_m230_MethodInfo,
	&TurnOffWordBehaviour_Awake_m231_MethodInfo,
	NULL
};
static const Il2CppMethodReference TurnOffWordBehaviour_t86_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool TurnOffWordBehaviour_t86_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TurnOffWordBehaviour_t86_0_0_0;
extern const Il2CppType TurnOffWordBehaviour_t86_1_0_0;
struct TurnOffWordBehaviour_t86;
const Il2CppTypeDefinitionMetadata TurnOffWordBehaviour_t86_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TurnOffWordBehaviour_t86_VTable/* vtableMethods */
	, TurnOffWordBehaviour_t86_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TurnOffWordBehaviour_t86_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffWordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffWordBehaviour_t86_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TurnOffWordBehaviour_t86_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TurnOffWordBehaviour_t86_0_0_0/* byval_arg */
	, &TurnOffWordBehaviour_t86_1_0_0/* this_arg */
	, &TurnOffWordBehaviour_t86_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffWordBehaviour_t86)/* instance_size */
	, sizeof (TurnOffWordBehaviour_t86)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
// Metadata Definition Vuforia.UserDefinedTargetBuildingBehaviour
extern TypeInfo UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern const MethodInfo UserDefinedTargetBuildingBehaviour__ctor_m232_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingBehaviour__ctor_m232/* method */
	, &UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserDefinedTargetBuildingBehaviour_t87_MethodInfos[] =
{
	&UserDefinedTargetBuildingBehaviour__ctor_m232_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserDefinedTargetBuildingBehaviour_t87_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool UserDefinedTargetBuildingBehaviour_t87_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType UserDefinedTargetBuildingBehaviour_t87_0_0_0;
extern const Il2CppType UserDefinedTargetBuildingBehaviour_t87_1_0_0;
extern const Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t88_0_0_0;
struct UserDefinedTargetBuildingBehaviour_t87;
const Il2CppTypeDefinitionMetadata UserDefinedTargetBuildingBehaviour_t87_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_0_0_0/* parent */
	, UserDefinedTargetBuildingBehaviour_t87_VTable/* vtableMethods */
	, UserDefinedTargetBuildingBehaviour_t87_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingBehaviour_t87_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserDefinedTargetBuildingBehaviour_t87_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingBehaviour_t87_1_0_0/* this_arg */
	, &UserDefinedTargetBuildingBehaviour_t87_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingBehaviour_t87)/* instance_size */
	, sizeof (UserDefinedTargetBuildingBehaviour_t87)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
// Metadata Definition Vuforia.VideoBackgroundBehaviour
extern TypeInfo VideoBackgroundBehaviour_t89_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern const MethodInfo VideoBackgroundBehaviour__ctor_m233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundBehaviour__ctor_m233/* method */
	, &VideoBackgroundBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoBackgroundBehaviour_t89_MethodInfos[] =
{
	&VideoBackgroundBehaviour__ctor_m233_MethodInfo,
	NULL
};
static const Il2CppMethodReference VideoBackgroundBehaviour_t89_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool VideoBackgroundBehaviour_t89_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VideoBackgroundBehaviour_t89_0_0_0;
extern const Il2CppType VideoBackgroundBehaviour_t89_1_0_0;
extern const Il2CppType VideoBackgroundAbstractBehaviour_t90_0_0_0;
struct VideoBackgroundBehaviour_t89;
const Il2CppTypeDefinitionMetadata VideoBackgroundBehaviour_t89_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &VideoBackgroundAbstractBehaviour_t90_0_0_0/* parent */
	, VideoBackgroundBehaviour_t89_VTable/* vtableMethods */
	, VideoBackgroundBehaviour_t89_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VideoBackgroundBehaviour_t89_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundBehaviour_t89_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VideoBackgroundBehaviour_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 9/* custom_attributes_cache */
	, &VideoBackgroundBehaviour_t89_0_0_0/* byval_arg */
	, &VideoBackgroundBehaviour_t89_1_0_0/* this_arg */
	, &VideoBackgroundBehaviour_t89_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundBehaviour_t89)/* instance_size */
	, sizeof (VideoBackgroundBehaviour_t89)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
// Metadata Definition Vuforia.VideoTextureRenderer
extern TypeInfo VideoTextureRenderer_t91_il2cpp_TypeInfo;
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern const MethodInfo VideoTextureRenderer__ctor_m234_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRenderer__ctor_m234/* method */
	, &VideoTextureRenderer_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoTextureRenderer_t91_MethodInfos[] =
{
	&VideoTextureRenderer__ctor_m234_MethodInfo,
	NULL
};
extern const MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo;
static const Il2CppMethodReference VideoTextureRenderer_t91_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo,
};
static bool VideoTextureRenderer_t91_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VideoTextureRenderer_t91_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t178_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VideoTextureRenderer_t91_0_0_0;
extern const Il2CppType VideoTextureRenderer_t91_1_0_0;
extern const Il2CppType VideoTextureRendererAbstractBehaviour_t92_0_0_0;
struct VideoTextureRenderer_t91;
const Il2CppTypeDefinitionMetadata VideoTextureRenderer_t91_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VideoTextureRenderer_t91_InterfacesOffsets/* interfaceOffsets */
	, &VideoTextureRendererAbstractBehaviour_t92_0_0_0/* parent */
	, VideoTextureRenderer_t91_VTable/* vtableMethods */
	, VideoTextureRenderer_t91_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VideoTextureRenderer_t91_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRenderer"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRenderer_t91_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VideoTextureRenderer_t91_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VideoTextureRenderer_t91_0_0_0/* byval_arg */
	, &VideoTextureRenderer_t91_1_0_0/* this_arg */
	, &VideoTextureRenderer_t91_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRenderer_t91)/* instance_size */
	, sizeof (VideoTextureRenderer_t91)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Metadata Definition Vuforia.VirtualButtonBehaviour
extern TypeInfo VirtualButtonBehaviour_t93_il2cpp_TypeInfo;
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern const MethodInfo VirtualButtonBehaviour__ctor_m235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonBehaviour__ctor_m235/* method */
	, &VirtualButtonBehaviour_t93_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualButtonBehaviour_t93_MethodInfos[] =
{
	&VirtualButtonBehaviour__ctor_m235_MethodInfo,
	NULL
};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo;
static const Il2CppMethodReference VirtualButtonBehaviour_t93_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo,
};
static bool VirtualButtonBehaviour_t93_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorVirtualButtonBehaviour_t198_0_0_0;
static Il2CppInterfaceOffsetPair VirtualButtonBehaviour_t93_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t198_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VirtualButtonBehaviour_t93_0_0_0;
extern const Il2CppType VirtualButtonBehaviour_t93_1_0_0;
struct VirtualButtonBehaviour_t93;
const Il2CppTypeDefinitionMetadata VirtualButtonBehaviour_t93_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VirtualButtonBehaviour_t93_InterfacesOffsets/* interfaceOffsets */
	, &VirtualButtonAbstractBehaviour_t94_0_0_0/* parent */
	, VirtualButtonBehaviour_t93_VTable/* vtableMethods */
	, VirtualButtonBehaviour_t93_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VirtualButtonBehaviour_t93_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonBehaviour_t93_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VirtualButtonBehaviour_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualButtonBehaviour_t93_0_0_0/* byval_arg */
	, &VirtualButtonBehaviour_t93_1_0_0/* this_arg */
	, &VirtualButtonBehaviour_t93_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonBehaviour_t93)/* instance_size */
	, sizeof (VirtualButtonBehaviour_t93)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
// Metadata Definition Vuforia.WebCamBehaviour
extern TypeInfo WebCamBehaviour_t95_il2cpp_TypeInfo;
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern const MethodInfo WebCamBehaviour__ctor_m236_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamBehaviour__ctor_m236/* method */
	, &WebCamBehaviour_t95_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WebCamBehaviour_t95_MethodInfos[] =
{
	&WebCamBehaviour__ctor_m236_MethodInfo,
	NULL
};
static const Il2CppMethodReference WebCamBehaviour_t95_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool WebCamBehaviour_t95_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WebCamBehaviour_t95_0_0_0;
extern const Il2CppType WebCamBehaviour_t95_1_0_0;
extern const Il2CppType WebCamAbstractBehaviour_t96_0_0_0;
struct WebCamBehaviour_t95;
const Il2CppTypeDefinitionMetadata WebCamBehaviour_t95_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &WebCamAbstractBehaviour_t96_0_0_0/* parent */
	, WebCamBehaviour_t95_VTable/* vtableMethods */
	, WebCamBehaviour_t95_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WebCamBehaviour_t95_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamBehaviour_t95_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WebCamBehaviour_t95_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamBehaviour_t95_0_0_0/* byval_arg */
	, &WebCamBehaviour_t95_1_0_0/* this_arg */
	, &WebCamBehaviour_t95_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamBehaviour_t95)/* instance_size */
	, sizeof (WebCamBehaviour_t95)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
// Metadata Definition Vuforia.WireframeBehaviour
extern TypeInfo WireframeBehaviour_t97_il2cpp_TypeInfo;
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern const MethodInfo WireframeBehaviour__ctor_m237_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeBehaviour__ctor_m237/* method */
	, &WireframeBehaviour_t97_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern const MethodInfo WireframeBehaviour_CreateLineMaterial_m238_MethodInfo = 
{
	"CreateLineMaterial"/* name */
	, (methodPointerType)&WireframeBehaviour_CreateLineMaterial_m238/* method */
	, &WireframeBehaviour_t97_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern const MethodInfo WireframeBehaviour_OnRenderObject_m239_MethodInfo = 
{
	"OnRenderObject"/* name */
	, (methodPointerType)&WireframeBehaviour_OnRenderObject_m239/* method */
	, &WireframeBehaviour_t97_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo WireframeBehaviour_OnDrawGizmos_m240_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&WireframeBehaviour_OnDrawGizmos_m240/* method */
	, &WireframeBehaviour_t97_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WireframeBehaviour_t97_MethodInfos[] =
{
	&WireframeBehaviour__ctor_m237_MethodInfo,
	&WireframeBehaviour_CreateLineMaterial_m238_MethodInfo,
	&WireframeBehaviour_OnRenderObject_m239_MethodInfo,
	&WireframeBehaviour_OnDrawGizmos_m240_MethodInfo,
	NULL
};
static const Il2CppMethodReference WireframeBehaviour_t97_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool WireframeBehaviour_t97_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WireframeBehaviour_t97_0_0_0;
extern const Il2CppType WireframeBehaviour_t97_1_0_0;
struct WireframeBehaviour_t97;
const Il2CppTypeDefinitionMetadata WireframeBehaviour_t97_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WireframeBehaviour_t97_VTable/* vtableMethods */
	, WireframeBehaviour_t97_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 133/* fieldStart */

};
TypeInfo WireframeBehaviour_t97_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WireframeBehaviour_t97_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WireframeBehaviour_t97_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WireframeBehaviour_t97_0_0_0/* byval_arg */
	, &WireframeBehaviour_t97_1_0_0/* this_arg */
	, &WireframeBehaviour_t97_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeBehaviour_t97)/* instance_size */
	, sizeof (WireframeBehaviour_t97)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
// Metadata Definition Vuforia.WireframeTrackableEventHandler
extern TypeInfo WireframeTrackableEventHandler_t99_il2cpp_TypeInfo;
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern const MethodInfo WireframeTrackableEventHandler__ctor_m241_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler__ctor_m241/* method */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo WireframeTrackableEventHandler_Start_m242_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_Start_m242/* method */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Status_t184_0_0_0;
extern const Il2CppType Status_t184_0_0_0;
static const ParameterInfo WireframeTrackableEventHandler_t99_WireframeTrackableEventHandler_OnTrackableStateChanged_m243_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217817, 0, &Status_t184_0_0_0},
	{"newStatus", 1, 134217818, 0, &Status_t184_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern const MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m243_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackableStateChanged_m243/* method */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, WireframeTrackableEventHandler_t99_WireframeTrackableEventHandler_OnTrackableStateChanged_m243_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern const MethodInfo WireframeTrackableEventHandler_OnTrackingFound_m244_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingFound_m244/* method */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern const MethodInfo WireframeTrackableEventHandler_OnTrackingLost_m245_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingLost_m245/* method */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WireframeTrackableEventHandler_t99_MethodInfos[] =
{
	&WireframeTrackableEventHandler__ctor_m241_MethodInfo,
	&WireframeTrackableEventHandler_Start_m242_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m243_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingFound_m244_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingLost_m245_MethodInfo,
	NULL
};
extern const MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m243_MethodInfo;
static const Il2CppMethodReference WireframeTrackableEventHandler_t99_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m243_MethodInfo,
};
static bool WireframeTrackableEventHandler_t99_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WireframeTrackableEventHandler_t99_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t185_0_0_0,
};
static Il2CppInterfaceOffsetPair WireframeTrackableEventHandler_t99_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t185_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WireframeTrackableEventHandler_t99_0_0_0;
extern const Il2CppType WireframeTrackableEventHandler_t99_1_0_0;
struct WireframeTrackableEventHandler_t99;
const Il2CppTypeDefinitionMetadata WireframeTrackableEventHandler_t99_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WireframeTrackableEventHandler_t99_InterfacesTypeInfos/* implementedInterfaces */
	, WireframeTrackableEventHandler_t99_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WireframeTrackableEventHandler_t99_VTable/* vtableMethods */
	, WireframeTrackableEventHandler_t99_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 136/* fieldStart */

};
TypeInfo WireframeTrackableEventHandler_t99_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, WireframeTrackableEventHandler_t99_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WireframeTrackableEventHandler_t99_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WireframeTrackableEventHandler_t99_0_0_0/* byval_arg */
	, &WireframeTrackableEventHandler_t99_1_0_0/* this_arg */
	, &WireframeTrackableEventHandler_t99_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeTrackableEventHandler_t99)/* instance_size */
	, sizeof (WireframeTrackableEventHandler_t99)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Metadata Definition Vuforia.WordBehaviour
extern TypeInfo WordBehaviour_t100_il2cpp_TypeInfo;
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordBehaviour::.ctor()
extern const MethodInfo WordBehaviour__ctor_m246_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordBehaviour__ctor_m246/* method */
	, &WordBehaviour_t100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WordBehaviour_t100_MethodInfos[] =
{
	&WordBehaviour__ctor_m246_MethodInfo,
	NULL
};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo;
static const Il2CppMethodReference WordBehaviour_t100_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo,
};
static bool WordBehaviour_t100_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorWordBehaviour_t199_0_0_0;
static Il2CppInterfaceOffsetPair WordBehaviour_t100_InterfacesOffsets[] = 
{
	{ &IEditorWordBehaviour_t199_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WordBehaviour_t100_0_0_0;
extern const Il2CppType WordBehaviour_t100_1_0_0;
struct WordBehaviour_t100;
const Il2CppTypeDefinitionMetadata WordBehaviour_t100_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WordBehaviour_t100_InterfacesOffsets/* interfaceOffsets */
	, &WordAbstractBehaviour_t101_0_0_0/* parent */
	, WordBehaviour_t100_VTable/* vtableMethods */
	, WordBehaviour_t100_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WordBehaviour_t100_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordBehaviour_t100_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WordBehaviour_t100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WordBehaviour_t100_0_0_0/* byval_arg */
	, &WordBehaviour_t100_1_0_0/* this_arg */
	, &WordBehaviour_t100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordBehaviour_t100)/* instance_size */
	, sizeof (WordBehaviour_t100)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
