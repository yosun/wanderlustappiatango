﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t266;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Selectable>
struct  Comparison_1_t3333  : public MulticastDelegate_t314
{
};
