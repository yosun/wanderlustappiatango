﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueueChildren
struct SetRenderQueueChildren_t29;

// System.Void SetRenderQueueChildren::.ctor()
extern "C" void SetRenderQueueChildren__ctor_m99 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Start()
extern "C" void SetRenderQueueChildren_Start_m100 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Awake()
extern "C" void SetRenderQueueChildren_Awake_m101 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
