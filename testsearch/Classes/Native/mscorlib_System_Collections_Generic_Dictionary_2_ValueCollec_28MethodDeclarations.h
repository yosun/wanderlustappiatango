﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3124;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3113;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15070_gshared (Enumerator_t3124 * __this, Dictionary_2_t3113 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15070(__this, ___host, method) (( void (*) (Enumerator_t3124 *, Dictionary_2_t3113 *, const MethodInfo*))Enumerator__ctor_m15070_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared (Enumerator_t3124 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15071(__this, method) (( Object_t * (*) (Enumerator_t3124 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15072_gshared (Enumerator_t3124 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15072(__this, method) (( void (*) (Enumerator_t3124 *, const MethodInfo*))Enumerator_Dispose_m15072_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15073_gshared (Enumerator_t3124 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15073(__this, method) (( bool (*) (Enumerator_t3124 *, const MethodInfo*))Enumerator_MoveNext_m15073_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15074_gshared (Enumerator_t3124 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15074(__this, method) (( Object_t * (*) (Enumerator_t3124 *, const MethodInfo*))Enumerator_get_Current_m15074_gshared)(__this, method)
