﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t2151;
// System.Globalization.CompareInfo
struct CompareInfo_t1833;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct  Comparer_t2151  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t1833 * ___m_compareInfo_2;
};
struct Comparer_t2151_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t2151 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t2151 * ___DefaultInvariant_1;
};
