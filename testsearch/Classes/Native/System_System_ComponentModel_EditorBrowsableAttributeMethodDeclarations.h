﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_t1448;
// System.Object
struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"

// System.Void System.ComponentModel.EditorBrowsableAttribute::.ctor(System.ComponentModel.EditorBrowsableState)
extern "C" void EditorBrowsableAttribute__ctor_m7073 (EditorBrowsableAttribute_t1448 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EditorBrowsableState System.ComponentModel.EditorBrowsableAttribute::get_State()
extern "C" int32_t EditorBrowsableAttribute_get_State_m8536 (EditorBrowsableAttribute_t1448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EditorBrowsableAttribute::Equals(System.Object)
extern "C" bool EditorBrowsableAttribute_Equals_m8537 (EditorBrowsableAttribute_t1448 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EditorBrowsableAttribute::GetHashCode()
extern "C" int32_t EditorBrowsableAttribute_GetHashCode_m8538 (EditorBrowsableAttribute_t1448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
