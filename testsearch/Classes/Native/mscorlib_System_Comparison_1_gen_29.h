﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t571;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Trackable>
struct  Comparison_1_t3423  : public MulticastDelegate_t314
{
};
