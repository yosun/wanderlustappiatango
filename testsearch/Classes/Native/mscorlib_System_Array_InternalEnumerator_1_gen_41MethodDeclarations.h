﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>
struct InternalEnumerator_1_t3555;
// System.Object
struct Object_t;
// UnityEngine.MeshFilter
struct MeshFilter_t157;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m21809(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3555 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21810(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3555 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::Dispose()
#define InternalEnumerator_1_Dispose_m21811(__this, method) (( void (*) (InternalEnumerator_1_t3555 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21812(__this, method) (( bool (*) (InternalEnumerator_1_t3555 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::get_Current()
#define InternalEnumerator_1_get_Current_m21813(__this, method) (( MeshFilter_t157 * (*) (InternalEnumerator_1_t3555 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
