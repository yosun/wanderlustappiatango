﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UnityPlayer
struct UnityPlayer_t586;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t187;

// Vuforia.IUnityPlayer Vuforia.UnityPlayer::get_Instance()
extern "C" Object_t * UnityPlayer_get_Instance_m2744 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::SetImplementation(Vuforia.IUnityPlayer)
extern "C" void UnityPlayer_SetImplementation_m2745 (Object_t * __this /* static, unused */, Object_t * ___implementation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::.cctor()
extern "C" void UnityPlayer__cctor_m2746 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
