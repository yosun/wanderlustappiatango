﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2562;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t2497  : public MulticastDelegate_t314
{
};
