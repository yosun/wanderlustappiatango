﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>
struct  KeyValuePair_2_t3131 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::value
	GameObjectU5BU5D_t5* ___value_1;
};
