﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t357;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct  Func_2_t360  : public MulticastDelegate_t314
{
};
