﻿#pragma once
#include <stdint.h>
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<DG.Tweening.Core.ABSSequentiable>
struct  Comparison_1_t1064  : public MulticastDelegate_t314
{
};
