﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t3631;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22755(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3631 *, String_t*, ProfileData_t740 , const MethodInfo*))KeyValuePair_2__ctor_m22660_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m22756(__this, method) (( String_t* (*) (KeyValuePair_2_t3631 *, const MethodInfo*))KeyValuePair_2_get_Key_m22661_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22757(__this, ___value, method) (( void (*) (KeyValuePair_2_t3631 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m22662_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m22758(__this, method) (( ProfileData_t740  (*) (KeyValuePair_2_t3631 *, const MethodInfo*))KeyValuePair_2_get_Value_m22663_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22759(__this, ___value, method) (( void (*) (KeyValuePair_2_t3631 *, ProfileData_t740 , const MethodInfo*))KeyValuePair_2_set_Value_m22664_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m22760(__this, method) (( String_t* (*) (KeyValuePair_2_t3631 *, const MethodInfo*))KeyValuePair_2_ToString_m22665_gshared)(__this, method)
