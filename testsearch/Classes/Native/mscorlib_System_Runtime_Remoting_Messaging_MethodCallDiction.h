﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t15;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct  MethodCallDictionary_t2331  : public MethodDictionary_t2326
{
};
struct MethodCallDictionary_t2331_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t15* ___InternalKeys_6;
};
