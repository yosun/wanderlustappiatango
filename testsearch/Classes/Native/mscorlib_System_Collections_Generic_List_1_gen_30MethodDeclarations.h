﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t695;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t701;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t777;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordResult>
struct IEnumerator_1_t898;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.WordResult>
struct ICollection_1_t4203;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>
struct ReadOnlyCollection_1_t3499;
// Vuforia.WordResult[]
struct WordResultU5BU5D_t3494;
// System.Predicate`1<Vuforia.WordResult>
struct Predicate_1_t3500;
// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t3501;
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4514(__this, method) (( void (*) (List_1_t695 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20494(__this, ___collection, method) (( void (*) (List_1_t695 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Int32)
#define List_1__ctor_m20495(__this, ___capacity, method) (( void (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.cctor()
#define List_1__cctor_m20496(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20497(__this, method) (( Object_t* (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20498(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t695 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20499(__this, method) (( Object_t * (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20500(__this, ___item, method) (( int32_t (*) (List_1_t695 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20501(__this, ___item, method) (( bool (*) (List_1_t695 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20502(__this, ___item, method) (( int32_t (*) (List_1_t695 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20503(__this, ___index, ___item, method) (( void (*) (List_1_t695 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20504(__this, ___item, method) (( void (*) (List_1_t695 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20505(__this, method) (( bool (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20506(__this, method) (( bool (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20507(__this, method) (( Object_t * (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20508(__this, method) (( bool (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20509(__this, method) (( bool (*) (List_1_t695 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20510(__this, ___index, method) (( Object_t * (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20511(__this, ___index, ___value, method) (( void (*) (List_1_t695 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Add(T)
#define List_1_Add_m20512(__this, ___item, method) (( void (*) (List_1_t695 *, WordResult_t701 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20513(__this, ___newCount, method) (( void (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20514(__this, ___collection, method) (( void (*) (List_1_t695 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20515(__this, ___enumerable, method) (( void (*) (List_1_t695 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20516(__this, ___collection, method) (( void (*) (List_1_t695 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::AsReadOnly()
#define List_1_AsReadOnly_m20517(__this, method) (( ReadOnlyCollection_1_t3499 * (*) (List_1_t695 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Clear()
#define List_1_Clear_m20518(__this, method) (( void (*) (List_1_t695 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Contains(T)
#define List_1_Contains_m20519(__this, ___item, method) (( bool (*) (List_1_t695 *, WordResult_t701 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20520(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t695 *, WordResultU5BU5D_t3494*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::Find(System.Predicate`1<T>)
#define List_1_Find_m20521(__this, ___match, method) (( WordResult_t701 * (*) (List_1_t695 *, Predicate_1_t3500 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20522(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3500 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20523(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t695 *, int32_t, int32_t, Predicate_1_t3500 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.WordResult>::GetEnumerator()
#define List_1_GetEnumerator_m4504(__this, method) (( Enumerator_t846  (*) (List_1_t695 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::IndexOf(T)
#define List_1_IndexOf_m20524(__this, ___item, method) (( int32_t (*) (List_1_t695 *, WordResult_t701 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20525(__this, ___start, ___delta, method) (( void (*) (List_1_t695 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20526(__this, ___index, method) (( void (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Insert(System.Int32,T)
#define List_1_Insert_m20527(__this, ___index, ___item, method) (( void (*) (List_1_t695 *, int32_t, WordResult_t701 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20528(__this, ___collection, method) (( void (*) (List_1_t695 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Remove(T)
#define List_1_Remove_m20529(__this, ___item, method) (( bool (*) (List_1_t695 *, WordResult_t701 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20530(__this, ___match, method) (( int32_t (*) (List_1_t695 *, Predicate_1_t3500 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20531(__this, ___index, method) (( void (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Reverse()
#define List_1_Reverse_m20532(__this, method) (( void (*) (List_1_t695 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort()
#define List_1_Sort_m20533(__this, method) (( void (*) (List_1_t695 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20534(__this, ___comparison, method) (( void (*) (List_1_t695 *, Comparison_1_t3501 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::ToArray()
#define List_1_ToArray_m20535(__this, method) (( WordResultU5BU5D_t3494* (*) (List_1_t695 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::TrimExcess()
#define List_1_TrimExcess_m20536(__this, method) (( void (*) (List_1_t695 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Capacity()
#define List_1_get_Capacity_m20537(__this, method) (( int32_t (*) (List_1_t695 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20538(__this, ___value, method) (( void (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Count()
#define List_1_get_Count_m20539(__this, method) (( int32_t (*) (List_1_t695 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::get_Item(System.Int32)
#define List_1_get_Item_m20540(__this, ___index, method) (( WordResult_t701 * (*) (List_1_t695 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Item(System.Int32,T)
#define List_1_set_Item_m20541(__this, ___index, ___value, method) (( void (*) (List_1_t695 *, int32_t, WordResult_t701 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
