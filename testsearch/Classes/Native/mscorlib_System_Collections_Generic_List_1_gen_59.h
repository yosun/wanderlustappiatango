﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t3920;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct  List_1_t1349  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::_items
	PersistentCallU5BU5D_t3920* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::_version
	int32_t ____version_3;
};
struct List_1_t1349_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::EmptyArray
	PersistentCallU5BU5D_t3920* ___EmptyArray_4;
};
