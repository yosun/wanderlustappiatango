﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t3771;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1370;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t4349;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t467;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m25073_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25073(__this, method) (( void (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1__ctor_m25073_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25074_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25074(__this, method) (( bool (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25074_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25075_gshared (Collection_1_t3771 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25075(__this, ___array, ___index, method) (( void (*) (Collection_1_t3771 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25075_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25076_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25076(__this, method) (( Object_t * (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25076_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25077_gshared (Collection_1_t3771 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25077(__this, ___value, method) (( int32_t (*) (Collection_1_t3771 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25077_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25078_gshared (Collection_1_t3771 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25078(__this, ___value, method) (( bool (*) (Collection_1_t3771 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25078_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25079_gshared (Collection_1_t3771 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25079(__this, ___value, method) (( int32_t (*) (Collection_1_t3771 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25079_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25080_gshared (Collection_1_t3771 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25080(__this, ___index, ___value, method) (( void (*) (Collection_1_t3771 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25080_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25081_gshared (Collection_1_t3771 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25081(__this, ___value, method) (( void (*) (Collection_1_t3771 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25081_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25082_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25082(__this, method) (( bool (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25082_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25083_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25083(__this, method) (( Object_t * (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25083_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25084_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25084(__this, method) (( bool (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25084_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25085_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25085(__this, method) (( bool (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25085_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25086_gshared (Collection_1_t3771 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25086(__this, ___index, method) (( Object_t * (*) (Collection_1_t3771 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25086_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25087_gshared (Collection_1_t3771 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25087(__this, ___index, ___value, method) (( void (*) (Collection_1_t3771 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25087_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m25088_gshared (Collection_1_t3771 * __this, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_Add_m25088(__this, ___item, method) (( void (*) (Collection_1_t3771 *, UICharInfo_t466 , const MethodInfo*))Collection_1_Add_m25088_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m25089_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25089(__this, method) (( void (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_Clear_m25089_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m25090_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25090(__this, method) (( void (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_ClearItems_m25090_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m25091_gshared (Collection_1_t3771 * __this, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_Contains_m25091(__this, ___item, method) (( bool (*) (Collection_1_t3771 *, UICharInfo_t466 , const MethodInfo*))Collection_1_Contains_m25091_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25092_gshared (Collection_1_t3771 * __this, UICharInfoU5BU5D_t1370* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25092(__this, ___array, ___index, method) (( void (*) (Collection_1_t3771 *, UICharInfoU5BU5D_t1370*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25092_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25093_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25093(__this, method) (( Object_t* (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_GetEnumerator_m25093_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25094_gshared (Collection_1_t3771 * __this, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25094(__this, ___item, method) (( int32_t (*) (Collection_1_t3771 *, UICharInfo_t466 , const MethodInfo*))Collection_1_IndexOf_m25094_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25095_gshared (Collection_1_t3771 * __this, int32_t ___index, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_Insert_m25095(__this, ___index, ___item, method) (( void (*) (Collection_1_t3771 *, int32_t, UICharInfo_t466 , const MethodInfo*))Collection_1_Insert_m25095_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25096_gshared (Collection_1_t3771 * __this, int32_t ___index, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25096(__this, ___index, ___item, method) (( void (*) (Collection_1_t3771 *, int32_t, UICharInfo_t466 , const MethodInfo*))Collection_1_InsertItem_m25096_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m25097_gshared (Collection_1_t3771 * __this, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_Remove_m25097(__this, ___item, method) (( bool (*) (Collection_1_t3771 *, UICharInfo_t466 , const MethodInfo*))Collection_1_Remove_m25097_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25098_gshared (Collection_1_t3771 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25098(__this, ___index, method) (( void (*) (Collection_1_t3771 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25098_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25099_gshared (Collection_1_t3771 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25099(__this, ___index, method) (( void (*) (Collection_1_t3771 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25099_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25100_gshared (Collection_1_t3771 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25100(__this, method) (( int32_t (*) (Collection_1_t3771 *, const MethodInfo*))Collection_1_get_Count_m25100_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t466  Collection_1_get_Item_m25101_gshared (Collection_1_t3771 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25101(__this, ___index, method) (( UICharInfo_t466  (*) (Collection_1_t3771 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25101_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25102_gshared (Collection_1_t3771 * __this, int32_t ___index, UICharInfo_t466  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25102(__this, ___index, ___value, method) (( void (*) (Collection_1_t3771 *, int32_t, UICharInfo_t466 , const MethodInfo*))Collection_1_set_Item_m25102_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25103_gshared (Collection_1_t3771 * __this, int32_t ___index, UICharInfo_t466  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25103(__this, ___index, ___item, method) (( void (*) (Collection_1_t3771 *, int32_t, UICharInfo_t466 , const MethodInfo*))Collection_1_SetItem_m25103_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25104_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25104(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25104_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t466  Collection_1_ConvertItem_m25105_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25105(__this /* static, unused */, ___item, method) (( UICharInfo_t466  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25105_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25106_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25106(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25106_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25107_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25107(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25107_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25108_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25108(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25108_gshared)(__this /* static, unused */, ___list, method)
