﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t1291;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
struct  KeyValuePair_2_t1425 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::key
	Type_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::value
	SetDelegate_t1291 * ___value_1;
};
