﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3940;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3939;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t534;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m27095_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m27095(__this, method) (( void (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1__ctor_m27095_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m27096_gshared (LinkedList_1_t3940 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m27096(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3940 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))LinkedList_1__ctor_m27096_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27097_gshared (LinkedList_1_t3940 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27097(__this, ___value, method) (( void (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27097_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m27098_gshared (LinkedList_1_t3940 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m27098(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3940 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m27098_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27099_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27099(__this, method) (( Object_t* (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27099_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m27100_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m27100(__this, method) (( Object_t * (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m27100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27101_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27101(__this, method) (( bool (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27101_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m27102_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m27102(__this, method) (( bool (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m27102_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m27103_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m27103(__this, method) (( Object_t * (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m27103_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m27104_gshared (LinkedList_1_t3940 * __this, LinkedListNode_1_t3939 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m27104(__this, ___node, method) (( void (*) (LinkedList_1_t3940 *, LinkedListNode_1_t3939 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m27104_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t3939 * LinkedList_1_AddLast_m27105_gshared (LinkedList_1_t3940 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m27105(__this, ___value, method) (( LinkedListNode_1_t3939 * (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m27105_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m27106_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m27106(__this, method) (( void (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_Clear_m27106_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m27107_gshared (LinkedList_1_t3940 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m27107(__this, ___value, method) (( bool (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m27107_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m27108_gshared (LinkedList_1_t3940 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m27108(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3940 *, ObjectU5BU5D_t124*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m27108_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t3939 * LinkedList_1_Find_m27109_gshared (LinkedList_1_t3940 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m27109(__this, ___value, method) (( LinkedListNode_1_t3939 * (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m27109_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3941  LinkedList_1_GetEnumerator_m27110_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m27110(__this, method) (( Enumerator_t3941  (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_GetEnumerator_m27110_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m27111_gshared (LinkedList_1_t3940 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m27111(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3940 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))LinkedList_1_GetObjectData_m27111_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m27112_gshared (LinkedList_1_t3940 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m27112(__this, ___sender, method) (( void (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m27112_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m27113_gshared (LinkedList_1_t3940 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m27113(__this, ___value, method) (( bool (*) (LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m27113_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m27114_gshared (LinkedList_1_t3940 * __this, LinkedListNode_1_t3939 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m27114(__this, ___node, method) (( void (*) (LinkedList_1_t3940 *, LinkedListNode_1_t3939 *, const MethodInfo*))LinkedList_1_Remove_m27114_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m27115_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m27115(__this, method) (( void (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_RemoveLast_m27115_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m27116_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m27116(__this, method) (( int32_t (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_get_Count_m27116_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t3939 * LinkedList_1_get_First_m27117_gshared (LinkedList_1_t3940 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m27117(__this, method) (( LinkedListNode_1_t3939 * (*) (LinkedList_1_t3940 *, const MethodInfo*))LinkedList_1_get_First_m27117_gshared)(__this, method)
