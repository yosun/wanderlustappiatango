﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct KeyValuePair_2_t3399;
// Vuforia.Image
struct Image_t621;
// System.String
struct String_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18941(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3399 *, int32_t, Image_t621 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
#define KeyValuePair_2_get_Key_m18942(__this, method) (( int32_t (*) (KeyValuePair_2_t3399 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18943(__this, ___value, method) (( void (*) (KeyValuePair_2_t3399 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
#define KeyValuePair_2_get_Value_m18944(__this, method) (( Image_t621 * (*) (KeyValuePair_2_t3399 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18945(__this, ___value, method) (( void (*) (KeyValuePair_2_t3399 *, Image_t621 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToString()
#define KeyValuePair_2_ToString_m18946(__this, method) (( String_t* (*) (KeyValuePair_2_t3399 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
