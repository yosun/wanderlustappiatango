﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t3246;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16725_gshared (InternalEnumerator_1_t3246 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16725(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3246 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16725_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16726_gshared (InternalEnumerator_1_t3246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16726(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3246 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16726_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16727_gshared (InternalEnumerator_1_t3246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16727(__this, method) (( void (*) (InternalEnumerator_1_t3246 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16727_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16728_gshared (InternalEnumerator_1_t3246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16728(__this, method) (( bool (*) (InternalEnumerator_1_t3246 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16728_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t438  InternalEnumerator_1_get_Current_m16729_gshared (InternalEnumerator_1_t3246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16729(__this, method) (( RaycastHit2D_t438  (*) (InternalEnumerator_1_t3246 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16729_gshared)(__this, method)
