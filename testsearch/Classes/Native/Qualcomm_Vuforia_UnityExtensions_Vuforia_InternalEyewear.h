﻿#pragma once
#include <stdint.h>
// Vuforia.InternalEyewear
struct InternalEyewear_t593;
// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t566;
// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t568;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.InternalEyewear
struct  InternalEyewear_t593  : public Object_t
{
	// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::mProfileManager
	InternalEyewearCalibrationProfileManager_t566 * ___mProfileManager_1;
	// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::mCalibrator
	InternalEyewearUserCalibrator_t568 * ___mCalibrator_2;
};
struct InternalEyewear_t593_StaticFields{
	// Vuforia.InternalEyewear Vuforia.InternalEyewear::mInstance
	InternalEyewear_t593 * ___mInstance_0;
};
